namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenAttachmentsInProject : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "FileName", c => c.String());
            AddColumn("dbo.Projects", "FilePath", c => c.String());
            AddColumn("dbo.Projects", "ActualFileName", c => c.String());
            AddColumn("dbo.Projects", "AttachmentDescription", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Projects", "AttachmentDescription");
            DropColumn("dbo.Projects", "ActualFileName");
            DropColumn("dbo.Projects", "FilePath");
            DropColumn("dbo.Projects", "FileName");
        }
    }
}
