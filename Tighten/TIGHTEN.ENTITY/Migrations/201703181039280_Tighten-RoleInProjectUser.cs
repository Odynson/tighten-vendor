namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenRoleInProjectUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectUsers", "RoleId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectUsers", "RoleId");
        }
    }
}
