namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenInternalPermission_ForAdminLevelPermissions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InternalPermissions", "isAdminLevelPermission", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InternalPermissions", "isAdminLevelPermission");
        }
    }
}
