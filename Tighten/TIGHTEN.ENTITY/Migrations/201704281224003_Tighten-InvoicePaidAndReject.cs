namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenInvoicePaidAndReject : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceDetails", "TodoTimeLogIds", c => c.String());
            AddColumn("dbo.Invoices", "IsPaid", c => c.Boolean(nullable: false));
            AddColumn("dbo.Invoices", "PaidBy", c => c.String());
            AddColumn("dbo.Invoices", "IsRejected", c => c.Boolean(nullable: false));
            AddColumn("dbo.Invoices", "RejectedBy", c => c.String());
            AddColumn("dbo.Invoices", "RejectReason", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoices", "RejectReason");
            DropColumn("dbo.Invoices", "RejectedBy");
            DropColumn("dbo.Invoices", "IsRejected");
            DropColumn("dbo.Invoices", "PaidBy");
            DropColumn("dbo.Invoices", "IsPaid");
            DropColumn("dbo.InvoiceDetails", "TodoTimeLogIds");
        }
    }
}
