namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenconfirmationCodeCreatedDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserDetails", "EmailConfirmationCodeCreatedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserDetails", "EmailConfirmationCodeCreatedDate");
        }
    }
}
