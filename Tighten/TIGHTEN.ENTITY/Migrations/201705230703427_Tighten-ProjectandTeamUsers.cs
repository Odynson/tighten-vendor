namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenProjectandTeamUsers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectUsers", "IsActive", c => c.Boolean());
            AddColumn("dbo.ProjectUsers", "IsDeleted", c => c.Boolean());
            AddColumn("dbo.TeamUsers", "IsActive", c => c.Boolean());
            AddColumn("dbo.TeamUsers", "IsDeleted", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeamUsers", "IsDeleted");
            DropColumn("dbo.TeamUsers", "IsActive");
            DropColumn("dbo.ProjectUsers", "IsDeleted");
            DropColumn("dbo.ProjectUsers", "IsActive");
        }
    }
}
