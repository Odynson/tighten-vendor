namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenReporterInTodos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ToDoes", "ReporterId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ToDoes", "ReporterId");
        }
    }
}
