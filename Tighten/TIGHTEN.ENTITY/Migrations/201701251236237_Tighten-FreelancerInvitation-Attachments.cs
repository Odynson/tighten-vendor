namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenFreelancerInvitationAttachments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FreelancerInvitations", "FreelancerRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.FreelancerInvitations", "FileName", c => c.String());
            AddColumn("dbo.FreelancerInvitations", "ActualFileName", c => c.String());
            AddColumn("dbo.FreelancerInvitations", "FilePath", c => c.String());
            AddColumn("dbo.FreelancerInvitations", "AttachmentDescription", c => c.String());
            AddColumn("dbo.FreelancerInvitations", "Responsibilities", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FreelancerInvitations", "Responsibilities");
            DropColumn("dbo.FreelancerInvitations", "AttachmentDescription");
            DropColumn("dbo.FreelancerInvitations", "FilePath");
            DropColumn("dbo.FreelancerInvitations", "ActualFileName");
            DropColumn("dbo.FreelancerInvitations", "FileName");
            DropColumn("dbo.FreelancerInvitations", "FreelancerRate");
        }
    }
}
