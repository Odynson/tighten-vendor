namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenProjectFeedback : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectFeedbacks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ProjectId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        Feedback = c.String(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsSelected = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Projects", "Remarks", c => c.String());
            AddColumn("dbo.Roles", "Priority", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Roles", "Priority");
            DropColumn("dbo.Projects", "Remarks");
            DropTable("dbo.ProjectFeedbacks");
        }
    }
}
