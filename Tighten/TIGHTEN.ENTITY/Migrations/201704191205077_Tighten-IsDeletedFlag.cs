namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenIsDeletedFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentDetails", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Projects", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.ToDoes", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserDetails", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserEducationDetails", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserProfessionalDetails", "IsDeleted", c => c.Boolean(nullable: false));
            DropColumn("dbo.PaymentDetails", "IsActive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PaymentDetails", "IsActive", c => c.Boolean(nullable: false));
            DropColumn("dbo.UserProfessionalDetails", "IsDeleted");
            DropColumn("dbo.UserEducationDetails", "IsDeleted");
            DropColumn("dbo.UserDetails", "IsDeleted");
            DropColumn("dbo.ToDoes", "IsDeleted");
            DropColumn("dbo.Roles", "IsDeleted");
            DropColumn("dbo.Projects", "IsDeleted");
            DropColumn("dbo.PaymentDetails", "IsDeleted");
        }
    }
}
