namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenWidgets : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RolewiseWidgets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WidgetId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserWidgets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        CompanyId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        WidgetId = c.Int(nullable: false),
                        Sequence = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WidgetMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WidgetName = c.String(),
                        WidgetDescription = c.String(),
                        WidgetKey = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.WidgetMasters");
            DropTable("dbo.UserWidgets");
            DropTable("dbo.RolewiseWidgets");
        }
    }
}
