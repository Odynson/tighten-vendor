namespace TIGHTEN.ENTITY.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TightenProjectInvitationAttachments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectInvitationAttachments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        UserId = c.String(),
                        FileName = c.String(),
                        ActualFileName = c.String(),
                        FilePath = c.String(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProjectInvitationAttachments");
        }
    }
}
