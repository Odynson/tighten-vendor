﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TIGHTEN.ENTITY
{
    public class UserFeedback
    {

        /* Id is the unique Id of each row of UserFeedback table and also the Primary key  */
        public int Id { get; set; }

        /* CompanyId is the unique id of Company in Companies table to which this User Feedback is associated. */
        public int CompanyId { get; set; }


        /*Title is Title of Feedback  given by User */
        public string Title { get; set; }

        /* It is the Feedback Description */
        public string Description { get; set; }


        /* Flag for checking if the Feedback is Private or Public. If true its private otherwise public */
        public bool IsPrivate { get; set; }


        /* Flag for checking the Priority. If 1 it is high priority, if 2 it is medium priority, if 3 it is low priority */
        public int Priority { get; set; }


        /* CreatedBy consists the unique userid of User who added the Feedback */
        public string CreatedBy { get; set; }


        /* CreatedDate is the date time when the Feedback was added  */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }


        /*  it is a relationship between Todos table and TodoId */
        //[ForeignKey("CompanyId")]
        //public Company Companies { get; set; }




    }
}
