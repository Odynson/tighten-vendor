﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
   public class UserFeedbackComments
    {
        /* Id is the unique Id of each row of UserFeedbackComments table and also the Primary key  */
        public int Id { get; set; }

        /* UserFeedbackId is the unique id of UserFeedback in UserFeedback table to which this Comment is associated. */
        public int UserFeedbackId { get; set; }


        /* Comment is the string data commented by user */
        public string Comment { get; set; }


        /* CreatedBy consists the unique userid of User who added the Comment */
        public string CreatedBy { get; set; }

        /* CreatedDate is the date time when the Comment was added  */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }


        /*  it is a relationship between UserFeedback Table and UserFeedbackId */
        //[ForeignKey("UserFeedbackId")]
        //public UserFeedback UserFeedback { get; set; }


    }
}
