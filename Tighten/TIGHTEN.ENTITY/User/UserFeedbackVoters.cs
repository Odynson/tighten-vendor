﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class UserFeedbackVoters
    {

        /* Id is the unique Id of each row of UserFeedbackVoters table and also the Primary key  */
        public int Id { get; set; }

        /* UserFeedbackId is the unique id of UserFeedback in UserFeedback table to which this Voter is associated. */
        public int UserFeedbackId { get; set; }


        /* It is the userId of the Voter */
        public string VoterUserId { get; set; }


        /* CreatedDate is the date time when the Voter was added  */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }


        /*  it is a relationship between UserFeedback Table and UserFeedbackId */
        //[ForeignKey("UserFeedbackId")]
        //public UserFeedback UserFeedback { get; set; }


    }
}
