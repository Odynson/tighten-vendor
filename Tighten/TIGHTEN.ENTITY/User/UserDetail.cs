﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class UserDetail
    {
        /* Id is the unique id of each row in UserDetails Table and the primary key */
        public int Id { get; set; }
        /* UserId is the unique userid of the user which is created automatically by microsoft identity */
        public string UserId { get; set; }
        /* Email is the email address of the user for eg: abc@abccompany.com  */
        public string Email { get; set; }
        /*  FirstName is the first name of the user for eg : abc */
        public string FirstName { get; set; }
        /* LastName is the last name of the user for eg: def */
        public string LastName { get; set; }
        /* Gender is the gender/sex of the user for eg: if male Gender is 1, if female Gender is 2 , if dont prefer to answer it is 3 */
        public int? Gender { get; set; }
        /* IsSubscribed is the status to check if the user has subscribed through email or not */
        public bool? IsSubscribed { get; set; }
        /* CompanyId is the unique id of the company to which the user is associated */
        public int? CompanyId { get; set; }
        /* RoleId is the unique id of the Role to which the user is associated i.e User, Admin or Client*/
        public string RoleId { get; set; }
        /*  ParentUserId consits the unique user id of the user under whom this user is working. ParentuserId user is Admin of the company we can say*/
        public string ParentUserId { get; set; }
        /* ProfilePhoto is the name of image that the user has uploaded as his/her profile photo */
        public string ProfilePhoto { get; set; }
        /* SelectedTeamId consists the unique id of Team on which the user is currently working */
        public int? SelectedTeamId { get; set; }
        /* SendNotificationEmail is the status for checking whether the user wants the notification through Email or not */
        public bool? SendNotificationEmail { get; set; }
        /*  Address is address of the user for eg: house no #1 street abc city abc etc */
        public string Address { get; set; }
        /* PhoneNo is the cell/contact/phone number of the user for eg: 22222222 etc */
        public string PhoneNo { get; set; }
        /* Department is the department in company where the user is working for eg: IT department or Finance Department or Management Department etc */
        public string Department { get; set; }
        /* Designation is position of the user for eg: Software engineer, Tester , Team Leader etc */
        public string Designation { get; set; }
        /* Experience is the time period since the user is working in his/her field */
        public string Experience { get; set; }
        /* Expertise is the user field of expertise for eg: asp.net, mvc ,jquery, angular etc */
        public string Expertise { get; set; }
        /* It is the date of birth of the user */
        public DateTime? DateOfBirth { get; set; }
        /* It is about the user */
        public string AboutMe { get; set; }
        public bool? EmailConfirmed { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string EmailConfirmationCode { get; set; }
        public bool IsActive { get; set; }
        public decimal Rate { get; set; }

        /* This is to check User is Freelancer or Company */
        public bool IsFreelancer { get; set; }
        public string UserName { get; set; }
        public decimal? Availabilty { get; set; }
        public string Tags { get; set; }
        public string FavProjects { get; set; }
        public bool? IsProfilePublic { get; set; }

        public string CreditCard { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string CvvNumber { get; set; }
        public string CardHolderName { get; set; }
        public Nullable<bool> AllowReckringPayment { get; set; }

        public string StripeUserId { get; set; }
        public Nullable<bool> IsStripeAccountVerified { get; set; }
        public DateTime? EmailConfirmationCodeCreatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool Indefinitely { get; set; }
        public bool ValidUntil { get; set; }
        public DateTime ValidUntilDate { get; set; }


        public int VendorId { get; set; }
    }
}
