﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class UserCompanyRole
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public int? RoleId { get; set; }

        /* CompanyId is the unique id of the company to which the user is associated */
        public int? CompanyId { get; set; }
        public DateTime CreatedDate { get; set; }

        //[ForeignKey("UserId")]
        //public UserDetail UserDetails { get; set; }

        //[ForeignKey("RoleId")]
        //public Role Roles { get; set; }

        //[ForeignKey("Id")]
        //public Company Companies { get; set; }
    }
}
