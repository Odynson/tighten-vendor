﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class UserTag
    {

        /* Id is the unique id of each row in UserTags table and also it is the primary key */
        public int Id { get; set; }

        /* CommentId is the unique id of comment to which the user is tagged */
        [Required]
        public int CommentId { get; set; }

        /* UserId is the unique Id of user who is tagged into the comment*/
        public string UserId { get; set; }


        /*  It creates a relationship between ToDoComments table and this table. For eg. if we delete the comment then the related User tag entry
         will be deleted too 
         */
        //[ForeignKey("CommentId")]
        //public ToDoComment ToDoComments { get; set; }


    }
}
