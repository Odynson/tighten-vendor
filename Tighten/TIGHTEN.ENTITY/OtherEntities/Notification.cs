﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Notification
    {

        /* Id is the unique id of each row in Notification table and it is primary key */
        public int Id { get; set; }

        /* TeamId is the unique id of Team to which notification is associated */
        public int? TeamId { get; set; }

        /* ProjectId is the unique id of project  to which this notification is associated. */
        public int? ProjectId { get; set; }

        /* CalendarEventId is the unique id of Calendar Event  to which this notification is associated.  */
        public int? CalendarEventId { get; set; }
       
        /* TodoId is the unique id of todo in ToDo table to which this notification is associated. it contains the id of todo to which notification is associated */
        public int? TodoId { get; set; }

        /* It consits the unique id of comment of TodoComments table to which the notification is associated */
        public int? CommentId { get; set; }

        /* It consits the unique id of attachment of TodoAttachments table to which the notification is associated */
        public int? AttachmentId { get; set; }

        /* It consists the unique userid of user to which notification is associated  */
        public string NotifyingUserId { get; set; }

        /* It can contain the action to distinguish for eg whether it is todo assigned notification or edit notification or tagging notification or deleting notification etc */
        public string Action { get; set; }

        /* Name is the description of the notification which will be shown on UI for eg: You have been assigned a task etc */
        public string Name { get; set; }

        /* IsRead is the status whether the user has read the notification or not. 
         It is also used to highlight the unread notification and to filter the notification by read and unread filters */
        public bool IsRead { get; set; }

        /* It is status for checking which type of notification is this. Whether its Team notification, event notification, project notification, todo notification */
        public int Type { get; set; }

        /* It contains the unique userid of user who created the notification or responsible for notification by making some changes in software etc*/
        public string CreatedBy { get; set; }

        /* CreatedDate is the datetime when the notification was created */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }







    }
}
