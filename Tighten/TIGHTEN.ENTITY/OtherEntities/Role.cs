﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Role
    {
        //[Key]
        public int RoleId { get; set; }
        public string Name { get; set; }
        public  bool? isActive { get; set; }
        public string Description{ get; set; }
        public string CreatedBy { get; set; }
        /* CreatedDate consists the datetime when the company was created */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }
        /* ModifiedBy Consists the userid of the user who recently Updated the Company */
        public string ModifiedBy { get; set; }
        /*  ModifiedDate consists the datetime when the company was recently updated */
        [Column(TypeName = "datetime2")]
        public DateTime? ModifiedDate { get; set; }
        public int CompanyId { get; set; }
        public int Priority { get; set; }
        public bool IsDeleted { get; set; }
        public bool RoleVisibleToVendor { get; set; }
        public int Type { get; set; }

    }
}
