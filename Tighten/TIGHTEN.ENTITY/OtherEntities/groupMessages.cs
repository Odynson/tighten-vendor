﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class groupMessages
    {

        public int id { get; set; }

        [Required]
        public string Message { get; set; }
        public int Sender { get; set; }
        public DateTime SentDate { get; set; }
        public bool ISDeleted { get; set; }
        public int GroupID { get; set; }


    }
}
