﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Company
    {

        /* Id is the unique id of each row in company table and it will be set as Primary key by default  */
        public int Id { get; set; }
        /* Name is the name of the company for eg. abc softwares */
        public string Name { get; set; }
        /* Email is the email of the company for eg. abc@abc.com */
        public string Email { get; set; }
        /* PhoneNo is the contact number of the company */
        public string PhoneNo { get; set; }
        /* Fax is the fax number of the company */
        public string Fax { get; set; }
        /* Website is the web address of the company for eg. www.abc.com */
        public string Website { get; set; }
        /* Address is the address of the company for eg. IT park  etc */
        public string Address { get; set; }
        /* Description contains the basic information of the company for eg. what kind of company is this and all */
        public string Description { get; set; }
        /* Logo contains the logo of the Company */
        public string Logo { get; set; }
        /* CreatedBy Consists the userid of the user who created the Company */
        public string CreatedBy { get; set; }
        /* CreatedDate consists the datetime when the company was created */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }
        /* ModifiedBy Consists the userid of the user who recently Updated the Company */
        public string ModifiedBy { get; set; }
        /*  ModifiedDate consists the datetime when the company was recently updated */
        [Column(TypeName = "datetime2")]
        public DateTime? ModifiedDate { get; set; }


    }

}
