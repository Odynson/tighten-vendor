﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
   public class Media
    {

        public int ID { get; set; }

        //fields
        public string MediaName { get; set; }
        public bool ISDeleted { get; set; }
        public int SentBy { get; set; }
        public string MediaType { get; set; }

        //Relationships
        public int MessageID { get; set; }
        //what?
        public int MediaID { get; set; }
    }
}
