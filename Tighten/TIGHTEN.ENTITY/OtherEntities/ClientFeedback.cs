﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class ClientFeedback
    {

        /* Id is the unique Id of each row of ClientFeedback table and also the Primary key  */
        public int Id { get; set; }

        /* TodoId is the unique id of todo in ToDos table to which this Client Feedback is associated. */
        public int? TodoId { get; set; }

        /* ProjectId is the unique id of project in Projects table to which this ClientFeedback is associated. */
        public int? ProjectId { get; set; }


        /* Feedback is the feedback given by Client */
        public string Feedback { get; set; }

        /* Rating is the percentage given to particular todo or project */
        public int Rating { get; set; }

        /* Flag for checking if it is a Todo Feedback or Project Feedback */
        public bool IsTodoFeedback { get; set; }

        /* CreatedBy consists the unique userid of Client who added the Feedback */
        public string CreatedBy { get; set; }


        /* CreatedDate is the date time when the Feedback was added  */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }


        ///*  it is a relationship between Todos table and TodoId */
        //[ForeignKey("TodoId")]
        //public ToDo ToDos { get; set; }


        ///*  it is a relationship between Projects table and ProjectId */
        //[ForeignKey("ProjectId")]
        //public Project Projects { get; set; }


    }
}
