﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
   public class groupMembers
    {

        public int id { get; set; }

        //Fields
        public DateTime JoinedDate { get; set; }
        public int AddedBy { get; set; }
        //What do you mean by this record?
        public int ISGroupLeft { get; set; }

        //Relationships
        public int GroupID { get; set; }
        public int MemberID { get; set; }
    }
}
