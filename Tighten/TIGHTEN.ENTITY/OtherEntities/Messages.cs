﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
  public  class Messages
    {
         [Key]
        public int MessageID { get; set; }

        //fields
        public int Reciever { get; set; }
        public DateTime Date { get; set; }
        public bool ISDeleted { get; set; }

        //relationships?
        public int Message { get; set; }
        public int Sender { get; set; }

    }
}
