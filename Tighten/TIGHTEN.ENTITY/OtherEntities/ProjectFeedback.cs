﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class ProjectFeedback
    {
        /*  Id is the unique id of each row in ProjectFeedback table and it is the primary key*/
        public int Id { get; set; }

        /* UserId Consists the unique id of the user for whom this Feedback is created*/
        public string UserId { get; set; }

        /* ProjectId is the unique id of Project to which the ProjectFeedback is associated  */
        public int ProjectId { get; set; }

        /* CompanyId is the unique id of company to which the ProjectFeedback is associated  */
        public int CompanyId { get; set; }

        /* Feedback is the Feedback user  */
        public string Feedback { get; set; }

        /* CreatedBy Consists the userid of the user who created the ProjectFeedback */
        public string CreatedBy { get; set; }

        /* CreatedDate consists the datetime when the ProjectFeedback was created */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }

        /*  ModifiedDate consists the datetime when the ProjectFeedback was recently updated */
        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        public bool IsSelected { get; set; }

    }
}
