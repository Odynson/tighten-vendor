﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Team
    {
        /* Id is the unique id of each row in Teams Table and the primary key*/
        public int Id { get; set; }

        /* CompanyId is the unique id of company to which the Team is associated  */
        public int CompanyId { get; set; }

        /* TeamName is the name of the team for eg: ODesk Team etc*/
        public string TeamName { get; set; }

        /* TeamName is the name of the team for eg: ODesk Team etc*/
        public long TeamBudget { get; set; }

        /* IsDefault is true if it is the Default team of the Newly added User */
        public bool IsDefault { get; set; }

        /* Description is the description of the team for eg: it deals with all projects in Odesk */
        public string Description { get; set; }

        /* CreatedBy Consists the userid of the user who created the Team */
        public string CreatedBy { get; set; }

        /* ModifiedBy Consists the userid of the user who modified/updated the Team */
        public string ModifiedBy { get; set; }

        /* IsDeleted is status used when we dont want to parmanently delete the Team. we just change this status to true and we show the Teams where IsDeleted=false */
        public bool IsDeleted { get; set; }

        /* CreatedDate consists the datetime when the Team was created */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }


        /*  ModifiedDate consists the datetime when the Team was recently updated */
        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

    }
}
