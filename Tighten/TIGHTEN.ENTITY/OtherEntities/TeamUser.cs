﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class TeamUser
    {

        /* Id is the unique id of each row in TeamUsers Table and the primary key */
        public int Id { get; set; }

        /* UserId is unique id of user who is in the team */
        public string UserId { get; set; }

        /*  TeamId is the unique Id of team from Teams table to which the teamUser is associated*/
        public int TeamId { get; set; }

        /* CreatedDate consists the datetime when the TeamUser was added into the team*/
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }

        /* CreatedBy Consists the userid of the user who added the user in the Team */
        public string CreatedBy { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

        /*  it is a relationship between Teams table and this table. As section is associated with the TeamId i.e if the team will be deleted the teamusers associated with
    that team will be automatically deleted. 
    */
        //[ForeignKey("TeamId")]
        //public Team Teams { get; set; }

    }
}
