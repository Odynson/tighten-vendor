﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY 
{
    public class ClientTestimonial
    {

        /*  Id is the unique id of each row in ClientTestimonial table and it is the primary key*/
        public int Id { get; set; }

        /* ProjectId is the Id of the project against which client will give his feedback*/
        public int ProjectId { get; set; }
        public string UserId { get; set; }
        public int Rating { get; set; }
        public int CompanyId { get; set; }
        public string Feedback { get; set; }
        public string AdminId { get; set; }
        public bool IsSelected { get; set; }
    }
}
