﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Follower
    {

        /* Id is the unique Id of each row of follower table and also the Primary key  */
        public int Id { get; set; }

        [Required]
        /* TodoId is the unique id of todo in ToDos table to which this follower is added. it contains the id of todo to which follower is added */
        public int TodoId { get; set; }

        /* UserId is the unique id of user that is added as  follower  to this table for a particular task/todo */
        public string UserId { get; set; }
        
        /* CreatedBy consists the unique userid of user who added the follower to a particular todo */
        public string CreatedBy { get; set; }
       
        
        /* CreatedDate is the date time when the follower was added to a todo */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }


        /*  it is a relationship between Todos table and this table. As follower is associated with the todoid i.e if the todo will be deleted the followers associated with
         that todo will be automatically deleted 
         */
        //[ForeignKey("TodoId")]
        //public ToDo ToDos { get; set; }

    }
}
