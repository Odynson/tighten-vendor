﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Section
    {

        /* Id is the unique id of each row in Sections Table and the primary key*/
        public int Id { get; set; }


        [Required]
        /* ProjectId is the unique id of project to which this section is associated */
        public int ProjectId { get; set; }

        /*  SectionName is the name of the section for eg: High Priority, Low Cost etc */
        public string SectionName { get; set; }

        /* IsDefault is the status used for dintinguishing between No section and other sections. If IsDefault is True it means it is No Section. */
        public bool IsDefault { get; set; }

        /* ListOrder defines the order of Sections in the way they will be displayed under Projects */
        public int ListOrder { get; set; }

        /* CreatedBy Consists the userid of the user who created the section */
        public string CreatedBy { get; set; }

        /* ModifiedBy Consists the userid of the user who modified/updated the section */
        public string ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        /* CreatedDate consists the datetime when the section was created */
        public DateTime CreatedDate { get; set; }


        /*  ModifiedDate consists the datetime when the section was recently updated */
        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        /*  it is a relationship between Projects table and this table. As section is associated with the ProjectId i.e if the project will be deleted the sections associated with
     that project will be automatically deleted. 
     */
        //[ForeignKey("ProjectId")]
        //public Project Projects { get; set; }

    }
}
