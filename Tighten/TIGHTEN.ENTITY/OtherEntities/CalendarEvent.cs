﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class CalendarEvent
    {

        /* Id is the unique id of each row in CalendarEvents Table and the primary key*/
        public int Id { get; set; }


        [Required]
        /* TeamId is the unique id of Team to which this CalendarEvent is associated */
        public int TeamId { get; set; }
       

        /*  Title is the title of the CalendarEvent for eg: Meeting, Birthday Party etc */
        public string Title { get; set; }

        /*  Description is the Description of the CalendarEvent  */
        public string Description { get; set; }


        /* Url is the Url if a user wants to redirect to some other url on Title Click */
        public string Url { get; set; }

        /* AllDay is the status to check whether the CalendarEvent is AllDay or Not. Lets say if it is AllDay event then it will last whole Day.  */
        public bool AllDay { get; set; }


        [Column(TypeName = "datetime2")]
        /* Start Date consists the datetime when the Calendar Event will Start/Commence */
        public DateTime StartDate { get; set; }


        /*  EndDate consists the datetime when the Calendar Event will End/Finish */
        [Column(TypeName = "datetime2")]
        public DateTime? EndDate { get; set; }

        /* CreatedBy Consists the userid of the user who created the Calendar Event */
        public string CreatedBy { get; set; }

        /* ModifiedBy Consists the userid of the user who modified/updated the Calendar Event */
        public string ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        /* CreatedDate consists the datetime when the Calendar Event was created */
        public DateTime CreatedDate { get; set; }


        /*  ModifiedDate consists the datetime when the Calendar Event was recently updated */
        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        /*  it is a relationship between Teams table and this table. As CalendarEvent is associated with the TeamId i.e if the Team will be deleted the CalendarEvents associated with
     that Team will be automatically deleted. 
     */
        //[ForeignKey("TeamId")]
        //public Team Teams { get; set; }

    }
}
