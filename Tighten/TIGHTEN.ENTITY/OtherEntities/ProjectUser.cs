﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
   public class ProjectUser
    {

        /* Id is the unique id of each row in ProjectUsers Table and the primary key */
        public int Id { get; set; }

        /* UserId is unique id of user who is in the project */
        public string UserId { get; set; }

        /*  ProjectId is the unique Id of project from Projects table to which the projectUser is associated*/
        public int ProjectId { get; set; }

        /* CreatedDate consists the datetime when the ProjectUser was added into the Project*/
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }

        /* CreatedBy Consists the userid of the user who added the user in the Project */
        public string CreatedBy { get; set; }

        /*  it is a relationship between Projects table and this table. As section is associated with the ProjectId i.e if the project will be deleted the sections associated with
     that project will be automatically deleted. 
     */
        //[ForeignKey("ProjectId")]
        //public Project Projects { get; set; }

        public decimal? UserRate { get; set; }
        public int? WeeklyLimit { get; set; }
        public int? InvitationId { get; set; }

        /* RoleId is Id of user's role for particular project */
        public int RoleId { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

    }
}
