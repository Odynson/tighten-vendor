﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Groups
    {
        [Key]
        public int GroupID { get; set; }

        //fields
        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public DateTime CreatedDate { get; set; }
        public int TotalMembers { get; set; }
        public int IsActive { get; set; }
        public int MessagesCount { get; set; }

        //Relationships
        public int AdminID { get; set; }
    }
}
