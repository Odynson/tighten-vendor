﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Project
    {

        /*  Id is the unique id of each row in Projects table and it is the primary key*/
        public int Id { get; set; }

        /* Name is the name of the project for eg. Todo Project etc */
        public string Name { get; set; }

        /* Description is the description of the project for eg. It is PMS project */
        public string Description { get; set; }

		/* EstimatedHours is the Estimated Hours of the project for eg. 100 hrs etc */
        public int? EstimatedHours { get; set; }

        /* EstimatedBudget is the Estimated Budget of the project for eg. $1000 etc */
        public decimal? EstimatedBudget { get; set; }
        /* CreatedBy Consists the userid of the user who created the project */
        public string CreatedBy { get; set; }

        /* CreatedDate consists the datetime when the project was created */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }

        /*  ModifiedDate consists the datetime when the project was recently updated */
        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        /* CompanyId is the unique id of company to which the project is associated  */
        public int CompanyId { get; set; }

        /* TeamId is the unique of team to which the project is associated */
        public int TeamId { get; set; }


        /*  it is a relationship between Teams table and this table. As section is associated with the TeamId i.e if the team will be deleted the projects associated with
         that team will be automatically deleted. 
        */
        //[ForeignKey("TeamId")]
        //public Team Teams { get; set; }


        /*  IsComplete holds the value for project completion, wheather project is completed or not */
        public bool IsComplete { get; set; }

        /*  AccessLevel is the level of accessibility of project for users */
        public int? AccessLevel { get; set; }

        /* Remarks is the Remark of the project for eg. Good Work In Project etc */
        public string Remarks { get; set; }

        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string ActualFileName { get; set; }
        public string AttachmentDescription { get; set; }
        public bool IsDeleted { get; set; }

    }

}
