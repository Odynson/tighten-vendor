﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class UserEducationDetail
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string School { get; set; }
        public string DateAttendedFrom { get; set; }
        public string DateAttendedTo { get; set; }
        public string Degree { get; set; }
        public string EducationalStream { get; set; }
        public string Grade { get; set; }
        public string ActivitiesAndSocieties { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
    }
}
