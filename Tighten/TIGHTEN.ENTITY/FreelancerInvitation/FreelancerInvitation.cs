﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class FreelancerInvitation
    {
        [Key]
        public int Id { get; set; }
        public string FreelancerEmail { get; set; }
        public int CompanyId { get; set; }
        public string SentBy { get; set; }
        public int ProjectId { get; set; }
        public decimal? UserRate { get; set; }
        public int RoleId { get; set; }
        public string Activationkey { get; set; }
        public bool IskeyActive { get; set; }
        public DateTime SentDate { get; set; }
        public bool IsAccepted { get; set; }
        public bool? IsRejected { get; set; }
        public int? WeeklyLimit{ get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public string Remarks { get; set; }
        public bool? IsJobInvitation { get; set; }

        public decimal FreelancerRate { get; set; }
        public string FileName { get; set; }
        public string ActualFileName { get; set; }
        public string FilePath { get; set; }
        public string AttachmentDescription {get; set;}
        public string Responsibilities { get; set; }
        
            
    }
}
