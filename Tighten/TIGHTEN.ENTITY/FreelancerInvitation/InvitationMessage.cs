﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class InvitationMessage
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string SentBy { get; set; }
        public int InvitationId { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
        public DateTime CreatedDate { get; set; }
        
    }
}
