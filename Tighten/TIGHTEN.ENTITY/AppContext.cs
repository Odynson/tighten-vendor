﻿//using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
//using TIGHTEN.ENTITY.Migrations;
//using TIGHTEN.ENTITY;
//using TIGHTEN.ENTITY;
//using Tighten.Model;

namespace TIGHTEN.ENTITY
{
    public class AppContext : DbContext
    {
        public AppContext()
            : base("Tighten")
        {
        }

        //Db Sets
        public DbSet<Company> Companies { get; set; }
        public DbSet<UserDetail> UserDetails { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserCompanyRole> UserCompanyRoles { get; set; }
        public DbSet<Project> Projects { get; set; }

        public DbSet<ProjectUser> ProjectUsers { get; set; }
        public DbSet<ToDo> ToDos { get; set; }

        public DbSet<TodoNotifications> TodoNotifications { get; set; }
        public DbSet<SubToDo> SubToDos { get; set; }
        public DbSet<ToDoComment> ToDoComments { get; set; }
        public DbSet<ToDoAttachment> ToDoAttachments { get; set; }
        public DbSet<Follower> Followers { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<UserTag> UserTags { get; set; }
        public DbSet<FileTag> FileTags { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Team> Teams { get; set; }

        public DbSet<TeamUser> TeamUsers { get; set; }

        public DbSet<CalendarEvent> CalendarEvents { get; set; }

        public DbSet<Media> Media { get; set; }

        public DbSet<groupMessages> groupMessages { get; set; }

        public DbSet<groupMembers> groupMembers { get; set; }

        public DbSet<Groups> Groups { get; set; }

        public DbSet<Messages> Messages { get; set; }

        public DbSet<ChatUsers> ChatUsers { get; set; }

        public DbSet<ClientFeedback> ClientFeedback { get; set; }

        public DbSet<UserFeedback> UserFeedback { get; set; }

        public DbSet<UserFeedbackVoters> UserFeedbackVoters { get; set; }

        public DbSet<UserFeedbackComments> UserFeedbackComments { get; set; }
        public DbSet<TodoTimeLogs> TodoTimeLogs { get; set; }

        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceDetail> InvoiceDetails { get; set; }
        public DbSet<InvoiceForward> InvoiceForwards { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<FreelancerInvitation> FreelancerInvitations { get; set; }
        public DbSet<UserOnline> UserOnlines { get; set; }
        public DbSet<UserMessages> UserMessage { get; set; }
        public DbSet<TaskType> TaskTypes { get; set; }
        public DbSet<UserEducationDetail> UserEducationDetails { get; set; }
        public DbSet<UserProfessionalDetail> UserProfessionalDetails { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<ClientTestimonial> ClientTestimonials { get; set; }
        public DbSet<EducationalStream> EducationalStreams { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<SettingSection> SettingSections { get; set; }
        public DbSet<SettingConfig> SettingConfigs { get; set; }
        public DbSet<UserSettingConfig> UserSettingConfigs { get; set; }
        public DbSet<StripeAccountDetail> StripeAccountDetails { get; set; }
        public DbSet<InternalPermission> InternalPermissions { get; set; }
        public DbSet<PaymentDetail> PaymentDetails { get; set; }
        public DbSet<WidgetMaster> WidgetMasters { get; set; }
        public DbSet<RolewiseWidget> RolewiseWidgets { get; set; }
        public DbSet<UserWidget> UserWidgets { get; set; }
        public DbSet<ProjectFeedback> ProjectFeedbacks { get; set; }
        public DbSet<InvitationMessage> InvitationMessages { get; set; }
        public DbSet<ProjectInvitationAttachment> ProjectInvitationAttachments { get; set; }
        

        //static AppContext()
        //{
        //    // Set the database intializer which is run once during application start
        //    // This seeds the database with admin user credentials and admin role
        //    //  Database.SetInitializer<AppContext>(new ApplicationDbInitializer());
        //    // Database.SetInitializer<AppContext>(new CreateDatabaseIfNotExists<AppContext>());
        //    Database.SetInitializer<AppContext>(new MigrateDatabaseToLatestVersion<AppContext, Configuration>());
        //}

        //public static AppContext Create()
        //{
        //    return new AppContext();
        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
        }


    }
}