﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class UserOnline
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public DateTime LastOnlineTime { get; set; }
    }
}