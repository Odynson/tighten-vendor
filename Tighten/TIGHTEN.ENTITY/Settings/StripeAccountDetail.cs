﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class StripeAccountDetail
    {
        /* Id is the unique id of each row in StripeAccountDetails Table and the primary key */
        public int Id { get; set; }
        /* UserId is the unique userid of the user which is created automatically by microsoft identity */
        public string UserId { get; set; }
        /* Email is the email address of the user for eg: abc@abccompany.com  */
        public string Email { get; set; }
        /*  FirstName is the first name of the user for eg : abc */
        public string FirstName { get; set; }
        /* LastName is the last name of the user for eg: def */
        public string LastName { get; set; }

        public bool? Managed { get; set; }

        public string LegalEntityType { get; set; }

        public string LegalEntityFirstName { get; set; }

        public string LegalEntityLastName { get; set; }

        public string LegalEntityAddressLine1 { get; set; }

        public string LegalEntityAddressCity { get; set; }

        public string LegalEntityAddressState { get; set; }

        public string LegalEntityAddressPostalCode { get; set; }

        public string LegalEntityBirthDay { get; set; }

        public string LegalEntityBirthMonth { get; set; }

        public string LegalEntityBirthYear { get; set; }

        public string LegalEntitySSNLast4 { get; set; }

        public DateTime? TosAcceptanceDate { get; set; }

        public string TosAcceptanceIp { get; set; }

        public string ExternalBankAccountNumber { get; set; }

        public string ExternalBankCountry { get; set; }

        public string ExternalBankRoutingNumber { get; set; }


    }
}
