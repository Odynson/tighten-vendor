﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class SettingConfig
    {
        /*  Id is the unique id of each row in Notification table and it is the primary key*/
        public int Id { get; set; }

        /* Name is the name of the Notification for eg. Todo Notification etc */
        public string NotificationName { get; set; }

        /* Description is the description of the Notification for eg. It is Todo Notification */
        public string NotificationDescription { get; set; }

        /* Text is the Notificational Text of the Notification for eg. It is Todo Notification */
        public string NotificationText { get; set; }

        /* SettingSectionId is the Id of SettingsSection table  */
        public int SettingSectionId { get; set; }

        /* CreatedBy Consists the userid of the user who created the Notification */
        public string CreatedBy { get; set; }

        /* CreatedDate consists the datetime when the Notification was created */
        public DateTime? CreatedDate { get; set; }

        /*  IsActive holds the value for Active Notification, wheather Notification is Active or not */
        public bool? IsActive { get; set; }

        
    }
}
