﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY 
{
    public class UserSettingConfig
    {
        /*  Id is the unique id of each row in UserNotificationConfig table and it is the primary key*/
        public int Id { get; set; }

        /* UserId is unique id of user who is in the UserNotificationConfig */
        public string UserId { get; set; }

        /* CompanyId is the unique id of company to which the project is associated  */
        public int CompanyId { get; set; }

        /*  TeamId is the unique Id of Team from Team table to which the User is associated*/
        public string TodoId { get; set; }

        /*  ProjectId is the unique Id of project from Projects table to which the projectUser is associated*/
        public string ProjectId { get; set; }

        /*  Id is the unique id of each row in NotificationConfig table */
        public int NotificationId { get; set; }

        /*  IsActive holds the value for Active Notification in NotificationConfig, wheather Notification is Active or not */
        public bool IsActive { get; set; }

    }
}
