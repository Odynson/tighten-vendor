﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class SettingSection
    {
        /*  Id is the unique id of each row in SettingsSection table and it is the primary key*/
        public int Id { get; set; }

        /* SectionName is the name of the Section which will be used in SettingsConfig Table */
        public string SectionName { get; set; }

        /* SectionDescription is the description of the Section  */
        public string SectionDescription { get; set; }

        /* IsActive is the field to determine wheather the record is currently active or not   */
        public bool IsActive { get; set; }

    }
}
