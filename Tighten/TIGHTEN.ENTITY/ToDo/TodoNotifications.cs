﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
   public class TodoNotifications
    {

        /* Id is the unique id of each row in Notification table and it is primary key */
        public int Id { get; set; }

        /* TodoId is the unique id of todo in ToDo table to which this notification is associated. it contains the id of todo to which notification is associated */
        public int TodoId { get; set; }


        /* It consits the unique id of comment of TodoComments table to which the notification is associated */
        public int? CommentId { get; set; }

        /* It consits the unique id of attachment of TodoAttachments table to which the notification is associated */
        public int? AttachmentId { get; set; }

        /* It consists the unique userid of user to which notification is associated  */
        public string ConcernedUserId { get; set; }


        /* Name is the description of the notification which will be shown on UI for eg: You have been assigned a task etc */
        public string Description { get; set; }


        /* It is status for checking which type of notification is this. Whether its Team notification, event notification, project notification, todo notification */
        /*  Type 1 for Todos only, 2 for attachments , 3 for followers and 4 for comments */
        public int Type { get; set; }

        /* It contains the unique userid of user who created the notification or responsible for notification by making some changes in software etc*/
        public string CreatedBy { get; set; }

        /* CreatedDate is the datetime when the notification was created */
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }


        /*  it is a relationship between Todos table and this table. As notification is associated with the todoid i.e if the todo will be deleted the notification associated with
       that todo will be automatically deleted. Because on notification click we fetch the todo associated with that notification. 
       */
        //[ForeignKey("TodoId")]
        //public ToDo ToDos { get; set; }






    }
}
