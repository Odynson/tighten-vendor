﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class ToDo
    {
        /* Id is the unique of Todo in Todos table and the primary key */
        public int Id { get; set; }

        /* AssigneeId is the unique id of user to which the Todo/task is assigned */
        public string AssigneeId { get; set; }

        /* ReporterId is the unique id of user who assigned the Todo/task */
        public string ReporterId { get; set; }

        /* SectionId  contains the unique id of section in which the todo lies for eg. the task is in Low Priority section */
        public int SectionId { get; set; }

        /* Name is the name of the todo for eg: Make a logo for this project etc*/
        public string Name { get; set; }

        /*  Description is the description of todo for eg: logo should be square and use these kind of colors etc*/
        public string Description { get; set; }

        /* CreatedBy Consists the userid of the user who created the Todo  */
        public string CreatedBy { get; set; }

        /* ModifiedBy Consists the userid of the user who modified/updated the Todo */
        public string ModifiedBy { get; set; }

        /* IsDone is the status for whether the Todo is completed or not */
        public bool IsDone { get; set; }


        /* InProgress is the status for whether the Todo is in Progress or not */
        public bool InProgress { get; set; }

        /* IsRead is the flag for checking if the assignee has checked his indox or not */
        public bool IsRead { get; set; }

        /* ListOrder defines the order of Todos in the way they will be displayed under Sections */
        public int ListOrder { get; set; }

        /* DateOfCompletion consists the datetime when the Todo was completed*/
        [Column(TypeName = "datetime2")]
        public DateTime DateOfCompletion { get; set; }


        /* CreatedDate consists the datetime when the Todo was created*/
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }

        /* DeadlineDate consists the datetime when the Todo should be completed in other words we can say it is DUE Date*/
        [Column(TypeName = "datetime2")]
        public DateTime? DeadlineDate { get; set; }

        /*  ModifiedDate consists the datetime when the Todo was recently updated */
        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        /* It is a relationship between Sections table and this table. As todos are related to sections therefore we are required to make a relation. If we delete the section the todos under that section will be automatically deleted. */
        //[ForeignKey("SectionId")]
        //public Section Sections { get; set; }
        /*Time which is insrted when creating the new todo*/
        public string EstimatedTime { get; set; }
        /*Time which is logged by user to complete the todo*/
        public string LoggedTime { get; set; }
        /*
        1 = Phone Call
        2 = Research & Development
        3 =  Meeting
        4 = Admin Duties
        5 = PMS Task 
         */

        /*  TaskType are the type of the task which are at organization level  */
        public int TaskType { get; set; }
        public bool IsApproved { get; set; }
        public string ApprovedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? ApprovedDate { get; set; }
        public string Reason { get; set; }
        public bool? IsAdminModified { get; set; }

        /*  TodoTaskTypeId are the type of the task which are at Project level  */
        public string TodoTaskTypeId { get; set; }

        /* IsAvailableForInvoice is the status for whether the Todo is Is Available For Generating Invoice or not */
        public bool? IsAvailableForInvoice { get; set; }

        public bool IsDeleted { get; set; }
    }
}
