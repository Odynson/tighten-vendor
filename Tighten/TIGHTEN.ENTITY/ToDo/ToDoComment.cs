﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class ToDoComment
    {

        /* Id is the unique id of each row in ToDoComments Table and the primary key */
        public int Id { get; set; }


        /* ToDoId is the unique id of todo/task in ToDos table to which the comment is associated */
        [Required]
        public int ToDoId { get; set; }

        /* Description is the description of the comment for eg: Hey abc please do this task carefully */
        public string Description { get; set; }


        /* CreatedBy Consists the userid of the user who added the comment  */
        public string CreatedBy { get; set; }

        /* CreatedDate consists the datetime when the comment was added*/
        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }


        /* ModifiedDate consists the datetime when the comment was updated/modified*/
        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }


        /*  it is a relationship between Todos table and this table. As comment is associated with the todoid i.e if the todo will be deleted the comments associated with
            that todo will be automatically deleted.*/
        //[ForeignKey("ToDoId")]
        //public ToDo ToDos { get; set; }
    }
}
