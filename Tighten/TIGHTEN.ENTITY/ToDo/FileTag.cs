﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class FileTag
    {

        /* Id is unique id of each row in FileTag table and also the primary key */
        public int Id { get; set; }

        [Required]
        /* CommentId is the unique Id of comment in ToDoComment table to which the file has been tagged */
        public int CommentId { get; set; }

        /* FileName is the name of file that has been tagged into a comment */
        public string FileName { get; set; }

        /* OriginalName is the unique name of the file which is saved into our filesystem */
        public string OriginalName { get; set; }

        /* FilePath is the path of the file where it is located */
        public string FilePath { get; set; }

        /* IsGif is status for checking whether the file is gif or not */
        public bool IsGif { get; set; }

        /*  It creates a relationship between ToDoComments table and this table. For eg. if we delete the comment then the related file tag entry
         will be deleted too 
         */
        //[ForeignKey("CommentId")]
        //public ToDoComment ToDoComments { get; set; }

    }
}
