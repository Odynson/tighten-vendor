﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class TaskType
    {
        /* Id is the unique of TaskType in TaskTypes table and the primary key */
        public int Id { get; set; }
        /* Text is the field for type of task in TaskType table */
        public string Text { get; set; }
        /* Text is the field for type of task in TaskType table */
        public bool IsActive { get; set; }

    }
}
