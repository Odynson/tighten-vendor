﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class TodoTimeLogs
    {

        /* Id is unique id of each row in FileTag table and also the primary key */
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public int? TodoId{ get; set; }
        public string Memo { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime? StopDateTime { get; set; }
        public int Duration { get; set; }
        public bool IsAutomaticLogged { get; set; }
        public DateTime loggingTime { get; set; }
        public string StartBy { get; set; }
        public string StopBy { get; set; }
        public bool? IsInvoiceGenerated { get; set; }

        /*  relationship between this table and todoes table */
        //[ForeignKey("Id")]
        //public ToDo ToDo { get; set; }

    }
}
