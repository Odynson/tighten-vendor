﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class WidgetMaster
    {
        public int Id { get; set; }

        public string WidgetName { get; set; }

        public string WidgetDescription { get; set; }

        public string WidgetKey { get; set; }

        public bool IsActive { get; set; }

    }
}
