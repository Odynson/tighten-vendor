﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class UserWidget
    {

        public int Id { get; set; }

        public string UserId { get; set; }

        public int CompanyId { get; set; }

        public int RoleId { get; set; }

        public int WidgetId { get; set; }

        public int Sequence { get; set; }

        public bool IsActive { get; set; }


    }
}
