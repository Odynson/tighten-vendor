﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TIGHTEN.ENTITY 
{
    public class InternalPermission
    {
        [Key]
        public int Id { get; set; }
        public string PermissionName { get; set; }
        public int MenuId { get; set; }
        public int RoleId { get; set; }
        public int CompanyId { get; set; }
        public bool isAdminLevelPermission { get; set; }
        public bool? isActive { get; set; }
        public bool? isDeleted { get; set; }

    }
}
