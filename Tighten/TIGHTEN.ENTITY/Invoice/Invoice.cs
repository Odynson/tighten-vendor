﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public string UserId { get; set; }
        public int CompanyId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string SendTo { get; set; }
        public string Message { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string TotalHours { get; set; }
        public decimal TotalAmount { get; set; }
        public string ApprovedBy { get; set; }
        public bool InvoiceApprovedStatus { get; set; }
        public bool IsPaid { get; set; }
        public string PaidBy { get; set; }
        public bool IsRejected { get; set; }
        public string RejectedBy { get; set; }
        public string RejectReason { get; set; }
    }
}
