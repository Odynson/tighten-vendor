﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class InvoiceDetail
    {
        [Key]
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int TodoId { get; set; }
        public string TodoDescription { get; set; }
        public string TodoName { get; set; }
        public decimal Amount { get; set; }
        public string hours { get; set; }
        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }
        public int TodoType { get; set; }
        public string TodoTimeLogIds { get; set; }
    }
}
