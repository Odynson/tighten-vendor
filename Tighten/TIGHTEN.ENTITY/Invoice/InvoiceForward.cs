﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TIGHTEN.ENTITY
{
     public class InvoiceForward
    {

        [Key]
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public string UserId { get; set; }
        public int CompanyId { get; set; }
        public string ForwardTo { get; set; }
        public DateTime ForwardDate { get; set; }
        public bool IsCurrentlyActive { get; set; }


    }
}
