﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class ChatUsers
    {
        [Key]
        public int UserID { get; set; }

        //fields
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicturePath { get; set; }
        public bool ISOnline { get; set; }
        public bool ISDeleted { get; set; }

    }
}
