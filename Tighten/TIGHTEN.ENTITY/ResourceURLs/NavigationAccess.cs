﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class NavigationAccess
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int RoleId{ get; set; }
        public int NavigationId { get; set; }
        public bool IsActive { get; set; }

    }
}
