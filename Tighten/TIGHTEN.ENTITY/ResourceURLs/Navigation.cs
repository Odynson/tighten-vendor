﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Navigation
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; }
        public int MenuType{ get; set; }
        public bool IsParent { get; set; }
        public int ParentId { get; set; }
    }
}
