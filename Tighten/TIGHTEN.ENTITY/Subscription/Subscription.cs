﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY
{
    public class Subscription
    {
        /*  Id is the unique id of each row in Subscriptions table and it is the primary key*/
        public int Id { get; set; }

        /*  PackageID is the unique id of Package from Packages table to which Subscription is associated*/
        public int PackageID { get; set; }

        /*  CompanyId is the unique id of Company of User */
        public int CompanyId { get; set; }

        /* UserId is unique id of user */
        public string UserId { get; set; }
        /* StartDate is the date on which Subscription starts */
        public DateTime StartDate { get; set; }
        /* EndDate is the date on which Subscription ends or  expires */
        public DateTime EndDate { get; set; }
        /*  IsActive holds the value for Subscription,wheather Subscription is currently active or not */
        public bool IsActive { get; set; }
        /*  Discount is the Discount which is applied on the package */
        public decimal Discount { get; set; }
        /*  CouponCode is the code of coupon which is used for discount  */
        public string CouponCode { get; set; }

        public bool? IsExpiryMailReceived { get; set; }

        public bool? IsExpiryWarningMailReceived { get; set; }
    }

}
