﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY 
{
    public class Transaction
    {
        /*  Id is the unique id of each row in Transactions table and it is the primary key*/
        public int Id { get; set; }

        public int EventID { get; set; }

        /*  TransactionBy is the unique id of Subscription from Subscriptions table to which Transaction is associated*/
        public string TransactionType { get; set; }
        /*  TransactionBy is the unique id of user who make transaction */
        public string TransactionBy { get; set; }
        /*  TransactionBy is the unique id of user who make transaction */
        public string TransactionFor { get; set; }

        /* AmountPaid is the amount which was paid against package */
        public decimal AmountPaid { get; set; }
       
        /* TransactionDate is the date on which transaction is done */
        public DateTime TransactionDate { get; set; }

        /*  TransactionID holds the value for Transaction Id which get in response from payment request */
        public string TransactionID { get; set; }

        /*  StirpeTransactionID  holds the value for Stripe Transaction Id which get in response from payment gateways */
        public string StirpeTransactionID { get; set; }

    }

}
