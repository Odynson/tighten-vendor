﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.ENTITY 
{
    public class Package
    {
        /*  Id is the unique id of each row in Packages table and it is the primary key*/
        public int Id { get; set; }

        /* Name is the name of the Package for eg. trial Project etc */
        public string Name { get; set; }

        /* Description is the description of the Package for eg. It is trial project */
        public string Description { get; set; }

        /* Price is the Price of Package for eg. $100 etc */
        public decimal? Price { get; set; }

        /* DurationInDays is the duration of the active Package in days eg. 30,365 */
        public int DurationInDays { get; set; }

        /*  IsActive holds the value for Package,wheather Package is active or not */
        public bool IsActive { get; set; }

        /*  Ispromotional holds the value for Promotional Package,wheather Package is Promotional or not */
        public bool Ispromotional { get; set; }

        /*  Discount holds the value for provided Discount */
        public decimal Discount { get; set; }

    }

}
