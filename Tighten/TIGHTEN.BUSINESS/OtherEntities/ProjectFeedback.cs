﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class ProjectFeedback
    {
        TIGHTEN.DATA.ProjectFeedback feedback;
        /// <summary>
        ///Getting User's projects in which user is Member 
        /// </summary>
        /// <param name="ProjectId">It is unique Id of Project</param>
        /// <returns>List of User's Projects in it as an array</returns>
        public List<MODEL.ProjectFeedbackModel.ProjectFeedback> GetProjectByProjectId(int ProjectId, string UserId, int RoleId)
        {
            feedback = new TIGHTEN.DATA.ProjectFeedback();
            return feedback.GetProjectByProjectId(ProjectId, UserId, RoleId);
        }


        public int GetUserRolePriority(int ProjectId, string UserId, int RoleId)
        {
            feedback = new TIGHTEN.DATA.ProjectFeedback();
            return feedback.GetUserRolePriority(ProjectId, UserId, RoleId);
        }


        public void AddUpdateFeedback(MODEL.ProjectFeedbackModel.ProjectFeedback model)
        {
            feedback = new TIGHTEN.DATA.ProjectFeedback();
            feedback.AddUpdateFeedback(model);
            //return feedback.AddUpdateFeedback(model);
        }


        public void SaveProjectRemarks(MODEL.ProjectFeedbackModel.ProjectFeedback model)
        {
            feedback = new TIGHTEN.DATA.ProjectFeedback();
            feedback.SaveProjectRemarks(model);
        }


        public string MarkAsComplete(MODEL.ProjectFeedbackModel.ProjectFeedback model)
        {
            feedback = new TIGHTEN.DATA.ProjectFeedback();
            return feedback.MarkAsComplete(model);
        }



    }
}
