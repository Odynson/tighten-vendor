﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MODEL;

namespace TIGHTEN.BUSINESS
{
    public class ProjectUsers
    {
        TIGHTEN.DATA.ProjectUsers projectUsers;
        /// <summary>
        /// Getting ProjectMembers For a particular project who can access this project
        /// </summary>
        /// <param name="projectId">It is unique id of Project</param>
        /// <returns>Return the list of Project Members associated with project</returns>
        public List<MODEL.ProjectModel.ProjectMembers> getProjectMembers(int projectId)
        {
            projectUsers = new TIGHTEN.DATA.ProjectUsers();
            return projectUsers.getProjectMembers(projectId);
        }
        public object getNonProjectUsers(int projectId)
        {
            projectUsers = new TIGHTEN.DATA.ProjectUsers();
            return projectUsers.getNonProjectUsers(projectId);
        }
        public void AddProjectUser(String UserId, MODEL.TodoModel.ProjectUsers model)
        {
            projectUsers = new TIGHTEN.DATA.ProjectUsers();
            projectUsers.AddProjectUser(UserId, model);
        }
        public void DeleteProjectUser(int Id)
        {
            projectUsers = new TIGHTEN.DATA.ProjectUsers();
            projectUsers.DeleteProjectUser(Id);
        }
    }
}
