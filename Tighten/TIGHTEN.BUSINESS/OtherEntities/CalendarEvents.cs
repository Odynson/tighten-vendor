﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MODEL;

namespace TIGHTEN.BUSINESS
{
    public class CalendarEvents
    {
        TIGHTEN.DATA.CalendarEvents calendarEvents;
        /// <summary>
        /// It is a Function for getting Calendar Events for a particular Team
        /// </summary>
        /// <param name="TeamId">It is the TeamId for which we are getting Calendar Events</param>
        /// <returns>It returns the Calendar Events for a particular team</returns>
        public List<MODEL.TodoModel.CalendarEvents> GetCalendarEvents(int CompanyId, string UserId, int TeamId, int ProjectId)
        {
            calendarEvents = new TIGHTEN.DATA.CalendarEvents();
            return calendarEvents.GetCalendarEvents(CompanyId, UserId, TeamId, ProjectId);
        }
        /// <summary>
        /// It fetches details of SIngle Event
        /// </summary>
        /// <param name="CalendarEventId">It is unique id of event</param>
        /// <returns>it returns single event </returns>
        public MODEL.TodoModel.CalendarEvents GetSingleEvent(int GetSingleEvent)
        {
            calendarEvents = new TIGHTEN.DATA.CalendarEvents();
            return calendarEvents.GetSingleEvent(GetSingleEvent);
        }

        /// <summary>
        /// It is a Function for Saving Calendar Event for a particular Team
        /// </summary>
        /// <param name="UserId">It is the Unique Userid of logged in User</param>
        /// <param name="objModel">It is the model which consists all the properties of CalendarEvent Table fields being Saved</param>
        public string SaveCalendarEvent(MODEL.CalendarModel.CalendarEvents model)
        {
            calendarEvents = new TIGHTEN.DATA.CalendarEvents();
            return calendarEvents.SaveCalendarEvent(model);
        }
        /// <summary>
        /// It is a Function for Editing Calendar Event for a particular Team
        /// </summary>
        /// <param name="UserId">It is the Unique Userid of logged in User for updating ModifiedBy Field</param>
        /// <param name="objModel">It is the model which consists all the properties of CalendarEvent Table fields being Updated</param>

        public void EditCalendarEvent(MODEL.TodoModel.CalendarEvents objModel)
        {
            calendarEvents = new TIGHTEN.DATA.CalendarEvents();
            calendarEvents.EditCalendarEvent(objModel);
        }


        /// <summary>
        /// It is a Function for Editing Calendar Event for a particular Team
        /// </summary>
        /// <param name="UserId">It is the Unique Userid of logged in User for updating ModifiedBy Field</param>
        /// <param name="objModel">It is the model which consists all the properties of CalendarEvent Table fields being Updated</param>
        public void UpdateOnDrop(MODEL.CalendarModel.UpdateEventOnDropOrResize model)
        {
            calendarEvents = new TIGHTEN.DATA.CalendarEvents();
            calendarEvents.UpdateOnDrop(model);
        }


        /// <summary>
        /// It is a Function for Editing Calendar Event for a particular Team
        /// </summary>
        /// <param name="UserId">It is the Unique Userid of logged in User for updating ModifiedBy Field</param>
        /// <param name="objModel">It is the model which consists all the properties of CalendarEvent Table fields being Updated</param>
        public void UpdateOnResize(MODEL.CalendarModel.UpdateEventOnDropOrResize model)
        {
            calendarEvents = new TIGHTEN.DATA.CalendarEvents();
            calendarEvents.UpdateOnResize(model);
        }


        /// <summary>
        /// It is the Function for Deleting a particular Calendar Event
        /// </summary>
        /// <param name="CalendarEventId"> It is the unique Id of CalendarEvent which will be deleted</param>
        public void DeleteCalendarEvent(int CalendarEventId)
        {
            calendarEvents = new TIGHTEN.DATA.CalendarEvents();
            calendarEvents.DeleteCalendarEvent(CalendarEventId);
        }
    }
}
