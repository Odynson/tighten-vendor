﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA;

namespace TIGHTEN.BUSINESS
{
    public class Email
    {
        TIGHTEN.DATA.Email email;
        public async Task SendEmail(MODEL.TodoModel.Emails obj)
        {
            email = new TIGHTEN.DATA.Email();
            await email.SendEmail(obj);
        }
        public async Task sendAssigneeEmail(string destinationemail, string taskName, string emailbody, string taskstatus)
        {
            email = new TIGHTEN.DATA.Email();
            await email.sendAssigneeEmail(destinationemail,  taskName,  emailbody,  taskstatus);
        }
    }
}
