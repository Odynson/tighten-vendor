﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MODEL;

namespace TIGHTEN.BUSINESS
{
    public class Companies
    {
        TIGHTEN.DATA.Companies companies;
        /// <summary>
        /// It is a function for Updating Company Logo 
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged In User</param>
        /// <param name="Logo">Logoname of newly updated Logo</param>
        /// <param name="CompanyId">Unique Id of Company</param>
        /// <returns>Previous Logo of Company which we will delete</returns>
        public string UpdateCompanyLogo(string UserId, string Logo, int CompanyId)
        {
            companies = new TIGHTEN.DATA.Companies();
            return companies.UpdateCompanyLogo(UserId, Logo, CompanyId);
        }
        /// <summary>
        /// It is a function for Getting Particular Company's Details
        /// </summary>
        /// <param name="CompanyId">It is the unique Id of Company</param>
        /// <returns> List of Company Details like name, description, email etc</returns>
        public object GetCompanyDetails(int CompanyId)
        {
            companies = new TIGHTEN.DATA.Companies();
            return companies.GetCompanyDetails(CompanyId);
        }
        /// <summary>
        /// It is a function for Clearing/Removing Logo of Particular Company
        /// </summary>
        /// <param name="CompanyId">It is the unique Id of the Company</param>
        public void ClearCompanyLogo(int CompanyId)
        {
            companies = new TIGHTEN.DATA.Companies();
            companies.ClearCompanyLogo(CompanyId);
        }
        /// <summary>
        /// This function Updates the Company Details other than Logo
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void EditCompanyDetails(string UserId, MODEL.CompanyModel mod)
        {
            companies = new TIGHTEN.DATA.Companies();
            companies.EditCompanyDetails(UserId,  mod);
        }

        /// <summary>
        /// This function Updates the Company Name
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyName(MODEL.NewCompanyModel mod)
        {
            companies = new TIGHTEN.DATA.Companies();
            companies.UpdateCompanyName(mod);
        }


        /// <summary>
        /// This function Updates the Company Web Address
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyWebAddress(MODEL.NewCompanyModel mod)
        {
            companies = new TIGHTEN.DATA.Companies();
            companies.UpdateCompanyWebAddress(mod);
        }

        /// <summary>
        /// This function Updates the Company Phone No
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyPhoneNo(MODEL.NewCompanyModel mod)
        {
            companies = new TIGHTEN.DATA.Companies();
            companies.UpdateCompanyPhoneNo(mod);
        }


        /// <summary>
        /// This function Updates the Company Fax
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyFax(MODEL.NewCompanyModel mod)
        {
            companies = new TIGHTEN.DATA.Companies();
            companies.UpdateCompanyFax(mod);
        }


        /// <summary>
        /// This function Updates the Company Email
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyEmail(MODEL.NewCompanyModel mod)
        {
            companies = new TIGHTEN.DATA.Companies();
            companies.UpdateCompanyEmail(mod);
        }


        /// <summary>
        /// This function Updates the Company Address
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyAddress(MODEL.NewCompanyModel mod)
        {
            companies = new TIGHTEN.DATA.Companies();
            companies.UpdateCompanyAddress(mod);
        }


        /// <summary>
        /// This function Updates the Company About Organization
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyDescription(MODEL.NewCompanyModel mod)
        {
            companies = new TIGHTEN.DATA.Companies();
            companies.UpdateCompanyDescription(mod);
        }




    }
}
