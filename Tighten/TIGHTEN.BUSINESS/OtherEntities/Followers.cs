﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MODEL;

namespace TIGHTEN.BUSINESS
{
    public class Followers
    {
        TIGHTEN.DATA.Followers followers;
        public List<MODEL.FollowerModel.FollowerDetailsModel> getFollowers(int ToDoId)
        {
            followers = new TIGHTEN.DATA.Followers();
            return followers.getFollowers(ToDoId);
        }
        public object getUsers(int ToDoId, string UserId)
        {
            followers = new TIGHTEN.DATA.Followers();
            return followers.getUsers(ToDoId,UserId);
        }
        public void saveFollower(int ToDoId, string FollowerUserId, string UserId)
        {
            followers = new TIGHTEN.DATA.Followers();
            followers.saveFollower(ToDoId, FollowerUserId, UserId);
        }
        public void deleteFollower(int Id)
        {
            followers = new TIGHTEN.DATA.Followers();
            followers.deleteFollower(Id);
        }
        public void UpdateFollowers(string UserId, MODEL.FollowerModel.FollowerUpdateModel model,int CompanyId)
        {
            followers = new TIGHTEN.DATA.Followers();
            followers.UpdateFollowers(UserId, model, CompanyId);
        }
    }
}
