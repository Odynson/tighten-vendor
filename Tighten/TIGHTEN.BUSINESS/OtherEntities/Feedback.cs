﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class Feedback
    {
        TIGHTEN.DATA.Feedback feedback;
        /// <summary>
        ///Getting User's projects in which user is Member 
        /// </summary>
        /// <param name="ProjectId">It is unique Id of Project</param>
        /// <returns>List of User's Projects in it as an array</returns>
        public List<MODEL.FeedbackModel.Feedback> GetProjects(int CompanyId, string UserId)
        {
            feedback = new TIGHTEN.DATA.Feedback();
            return feedback.GetProjects(CompanyId, UserId);
        }



        public List<MODEL.FeedbackModel.FeedbackOfProject> ShowAllFeedbacksOfProject(int ProjectId, string UserId)
        {
            feedback = new TIGHTEN.DATA.Feedback();
            return feedback.ShowAllFeedbacksOfProject(ProjectId, UserId);
        }


        public void ShowOnProfile(MODEL.FeedbackModel.ShowOnProfileModel model)
        {
            feedback = new TIGHTEN.DATA.Feedback();
            feedback.ShowOnProfile(model);
        }



    }
}
