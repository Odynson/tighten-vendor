﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MODEL;

namespace TIGHTEN.BUSINESS
{
    public class Notifications
    {
        TIGHTEN.DATA.Notifications notifications;
        public List<MODEL.NotificationModel.NotificationDetailModel> GetNotifications(string Userid)
        {
            notifications = new TIGHTEN.DATA.Notifications();
            return notifications.GetNotifications(Userid);
        }
        public void SaveNotification(MODEL.TodoModel.Notifications obj, string Userid)
        {
            notifications = new TIGHTEN.DATA.Notifications();
            notifications.SaveNotification(obj, Userid);
        }

        public void UpdateNotification(int NotificationId)
        {
            notifications = new TIGHTEN.DATA.Notifications();
            notifications.UpdateNotification(NotificationId);
        }

        public string resetNotification(string UserId)
        {
            notifications = new TIGHTEN.DATA.Notifications();
            return notifications.resetNotification(UserId);
        }

    }
}
