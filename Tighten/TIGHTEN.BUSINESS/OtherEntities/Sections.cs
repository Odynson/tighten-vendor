﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MODEL;

namespace TIGHTEN.BUSINESS
{
    public class Sections
    {
        TIGHTEN.DATA.Sections sections;

        /// <summary>
        /// Getting Sections For a particular Project
        /// </summary>
        /// <param name="ProjectId">It is Unique id of the project</param>
        /// <returns>It returns the sections as a list</returns>
        public List<MODEL.TodoModel.SectionData> GetSectionForParticularProject(int ProjectId)
        {
            sections = new TIGHTEN.DATA.Sections();
            return sections.GetSectionForParticularProject(ProjectId);
        }
        public object GetUpdatableSections(int ProjectId)
        {
            sections = new TIGHTEN.DATA.Sections();
            return sections.GetUpdatableSections(ProjectId);
        }
        public int SaveSection(string UserId, MODEL.SectionModel.SectionAddModel obj)
        {
            sections = new TIGHTEN.DATA.Sections();
            return sections.SaveSection(UserId,obj);
        }
        public string UpdateSection(string UserId, MODEL.SectionModel.SectionUpdateModel obj)
        {
            sections = new TIGHTEN.DATA.Sections();
            return sections.UpdateSection(UserId, obj);
        }
        public void UpdateSectionOrder(List<MODEL.TodoModel.Sections> obj)
        {
            sections = new TIGHTEN.DATA.Sections();
            sections.UpdateSectionOrder(obj);
        }
        public string DeleteSection(int Id)
        {
            sections = new TIGHTEN.DATA.Sections();
            return sections.DeleteSection(Id);
        }
        public void UpdateTodosListOrderOrSectionInSection(List<MODEL.TodoModel.TodosData> obj)
        {
            sections = new TIGHTEN.DATA.Sections();
            sections.UpdateTodosListOrderOrSectionInSection(obj);
        }

    }
}
