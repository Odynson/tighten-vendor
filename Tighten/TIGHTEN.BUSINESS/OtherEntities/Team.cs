﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
//using MODEL;

namespace TIGHTEN.BUSINESS
{
    public class Team
    {
        TIGHTEN.DATA.Team team;
        /// <summary>
        /// Getting Teams with Users in which Logged in user is a team Member
        /// </summary>
        /// <param name="userId">It is the Unique Id of Logged In User</param>
        /// <returns>Returns the list of Teams with teammembers</returns>
        public List<MODEL.TeamModel.TeamWithMembers> getTeams(String userId,int CompanyId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.getTeams(userId, CompanyId);
        }

        /// <summary>
        /// Getting Teams with Users in which used is a team Member
        /// </summary>
        /// <param name="userId">It is the Unique Id of  User</param>
        /// <returns>Returns the list of Teams with teammembers</returns>
        public List<MODEL.TeamModel.TeamWithMembers> getUserTeams(String userId,int CompanyId, bool isOtherUser)
        {
            team = new TIGHTEN.DATA.Team();
            return team.getUserTeams(userId, CompanyId, isOtherUser);
        }

        /// <summary>
        /// Fetching Details of a Particular Team
        /// </summary>
        /// <param name="teamId">Unique of a particular team</param>
        /// <returns>return team details with team members</returns>
        public MODEL.TeamModel.TeamWithMembers getTeam(int teamId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.getTeam(teamId);
        }
        public MODEL.TeamModel.TeamWithMembers getTeamWithDesignation(int teamId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.getTeamWithDesignation(teamId);
        }

        /// <summary>
        /// Saving New Team with it Team Members
        /// </summary>
        /// <param name="UserId">Unique id of logged in User</param>
        /// <param name="model">It consists the values of all fields to be saved</param>
        public string saveTeam(String UserId, MODEL.TeamModel.Team model,int CompanyId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.saveTeam(UserId,model,CompanyId);
        }

        /// <summary>
        /// This function updates the existing Team
        /// </summary>
        /// <param name="UserId">Unique id of logged in user</param>
        /// <param name="model">It consists the updated values of Team</param>
        public string updateTeam(String UserId, MODEL.TeamModel.Team model,int CompanyId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.updateTeam(UserId, model,CompanyId);
        }

        /// <summary>
        /// This function fetches the Teams with team members for a particular Company
        /// </summary>
        /// <param name="companyId">It is the unique Id of Company</param>
        /// <returns>It returns Teams of a particular company with team members</returns>
        public List<TeamDataModel> getCompanyTeams(int CompanyId, string UserId,int PageIndex,int PageSizeSelected)
        {
            team = new TIGHTEN.DATA.Team();
             return team.getCompanyTeams(CompanyId, UserId, PageIndex, PageSizeSelected);
        }

        public List<TeamChartModel> getCompanyTeamsChart(int CompanyId, string UserId, int PageIndex, int PageSizeSelected,int ParentTeamId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.getCompanyTeamsChart(CompanyId, UserId, PageIndex, PageSizeSelected, ParentTeamId);
        }
        public string getCompanyName(int CompanyId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.getCompanyName(CompanyId);
        }
        /// <summary>
        /// This function fetches the Teams with team members for a particular Company
        /// </summary>
        /// <param name="companyId">It is the unique Id of Company</param>
        /// <returns>It returns Teams of a particular company with team members</returns>
        public List<MODEL.TeamModel.TeamFinancialManager> getAllTeamsForFinancialManager(int CompanyId, string UserId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.getAllTeamsForFinancialManager(CompanyId, UserId);
        }

        public string deleteTeam(int TeamId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.deleteTeam(TeamId);
        }

        public List<MODEL.UserRoleModel> GetAllDesignations()
        {
            team = new TIGHTEN.DATA.Team();
            return team.GetAllDesignation();
        }
        public List<MODEL.TeamModel.TeamWithMembers> GetAllTeams(int CompanyId,string UserId)
        { 
            team= new TIGHTEN.DATA.Team();
            return team.GetAllTeams(CompanyId, UserId);
        }
        public List<MODEL.UserRoleModel> GetTeamMembersByTeamId(int CompanyId, int TeamId)
        {
            team = new TIGHTEN.DATA.Team();
            return team.GetTeamMembersByTeamId(CompanyId, TeamId);
        }
    }
}
