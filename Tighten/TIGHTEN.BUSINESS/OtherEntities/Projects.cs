﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.MODEL.Vendor;
using static TIGHTEN.MODEL.ProjectModel;
//using MODEL;

namespace TIGHTEN.BUSINESS
{
    public class Projects
    {
        TIGHTEN.DATA.Projects projects;
        /// <summary>
        /// Getting User's Teams with projects in which user is Team Member and Project Member For MyTodos Section
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>List of User's Teams with Projects in it as an array</returns>
        public List<MODEL.MyTodosModel.Teams> getMyProjectsWithTeams(string UserId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.getMyProjectsWithTeams(UserId);
        }
        /// <summary>
        /// Getting List of Projects for a particular Team with its Project Members in which The user is a project Member
        /// </summary>
        /// <param name="userId">It is the unique id of logged in user</param>
        /// <param name="TeamId">It is the unique id of Team</param>
        /// <returns>returns the list of projects with projects members in it as an array</returns>
        public List<MODEL.ProjectModel.ProjectWithMembers> getProjects(String UserId, int TeamId, int CompanyId, bool IsFreelancer)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.getProjects(UserId, TeamId, CompanyId, IsFreelancer);
        }

        /// <summary>
        /// Getting User Project By Specific ProjectId
        /// </summary>
        /// <param name="ProjectId">It is unique id of Project</param>
        /// <returns> Single  Project Model </returns>
        public MODEL.ProjectModel.ProjectWithMembers GetProject(int ProjectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProject(ProjectId);
        }

        /// <summary>
        /// Getting All Projects in which User is involved/ProjectMember irrespective of Teams
        /// </summary>
        /// <param name="userId">It is unique id of user</param>
        /// <returns>List of Projects in which User is project Member</returns>
        public List<MODEL.ProjectModel.ProjectWithMembers> getUserProjects(string UserId, int CompanyId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.getUserProjects(UserId, CompanyId);
        }


        /// <summary>
        /// Getting All Projects in which User is involved/ProjectMember irrespective of Teams
        /// </summary>
        /// <param name="userId">It is unique id of user</param>
        /// <returns>List of Projects in which User is project Member</returns>
        public List<MODEL.ProjectModel.ProjectWithMembers> GetProjectsForSelectedTeam(int TeamId, int CompanyId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProjectsForSelectedTeam(TeamId, CompanyId);
        }


        /// <summary>
        /// Getting Projects with all project members of a particular Team that have been Created By User i.e owned by User
        /// </summary>
        /// <param name="userId">It is unique Id of User</param>
        /// <param name="teamId">It is unique Id of Team</param>
        /// <returns>List of Projects with ProjectMembers in it as an array</returns>
        public List<MODEL.ProjectModel.ProjectWithMembers> getUserOwnedProjects(string UserId, int TeamId, int RoleId, int CompanyId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.getUserOwnedProjects(UserId, TeamId, RoleId, CompanyId);
        }
        /// <summary>
        /// It saves the new Project with Project Members
        /// </summary>
        /// <param name="UserId">It is unique Id of User who is saving the new Project</param>
        /// <param name="model">Model consists the new values of project and Project members who will be accessing this Project</param>
        public string saveProject(string url, String UserId, MODEL.ProjectModel.Project model, int CompanyId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.saveProject(url, UserId, model, CompanyId);
        }
        /// <summary>
        /// It updates the Existing Project Fields
        /// </summary>
        /// <param name="UserId">It is unique id of User who is Updating The Project</param>
        /// <param name="model">Model consists the updated values of existing project and its project members</param>
        public string updateProject(string url, String UserId, MODEL.ProjectModel.Project model, int CompanyId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.updateProject(url, UserId, model, CompanyId);
        }
        /// <summary>
        /// It deletes the existing Project from Projects table
        /// </summary>
        /// <param name="Id">It is unique id of Project that will be Deleted</param>
        public string deleteProject(int Id, int CompanyId, bool IsFreelancer)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.deleteProject(Id, CompanyId, IsFreelancer);
        }


        /// <summary>
        /// It mark project as complete in Projects table
        /// </summary>
        /// <param name="Id">It is unique id of Project that will be Marked as Complete</param>
        public string ProjectComplete(int Id)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.ProjectComplete(Id);
        }



        /// <summary>
        /// It insert feedback for project ClientTestionial table
        /// </summary>

        public void InsertFeedback(MODEL.ClientTestimonialModel model)
        {
            projects = new TIGHTEN.DATA.Projects();
            projects.InsertFeedback(model);
        }

        public List<MODEL.ProjectModel.ProjectWithForFinancialManager> getAllProjectsForFinancialManager(int CompanyId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.getAllProjectsForFinancialManager(CompanyId);
        }


        public List<MODEL.ProjectModel.ProjectWithForFinancialManager> GetAllProjectsOfCompany(int CompanyId, int TeamId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetAllProjectsOfCompany(CompanyId, TeamId);
        }



        public List<MODEL.ProjectModel.AllFreelancersForCompanyModel> GetAllFreelancerOfProject(int ProjectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetAllFreelancerOfProject(ProjectId);
        }


        public string SaveProjectOptionsForFreelancer(MODEL.ProjectModel.ProjectOptionsForFreelancerModel model)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.SaveProjectOptionsForFreelancer(model);
        }


        public MODEL.ProjectModel.FreelancerListingModel GetFreelancerDetails(string UserId, int projectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetFreelancerDetails(UserId, projectId);
        }



        public string DeleteAttachmentFileFromProject(int projectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.DeleteAttachmentFileFromProject(projectId);
        }


        public HttpResponseMessage DownloadAttachmentFileFromProject(int projectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.DownloadAttachmentFileFromProject(projectId);
        }


        public List<MODEL.ProjectModel.FreelancerListingModel> GetInvitedFreelamcersOnlyNotAccepted(int projectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetInvitedFreelamcersOnlyNotAccepted(projectId);
        }


        public MODEL.ProjectModel.InternalUserDetailsModel GetInternalUserDetails(string UserId, int projectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetInternalUserDetails(UserId, projectId);
        }

        public string GetFileName(int projectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetFileName(projectId);
        }

        public int SaveNewProject(ProjectModel.NewCreateProjectModel obj)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.SaveNewProject(obj);

        }

        public int CheckProjectExist(int CompanyId, string ProjectName,int ProjectId)
        {
            int ResultId;
            projects = new TIGHTEN.DATA.Projects();
            return ResultId = projects.CheckProjectExist(CompanyId, ProjectName , ProjectId);

        }


        public List<vendorModel> GetProjectVendor(int CompanyId, int ProjectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProjectVendor(CompanyId, ProjectId);


        }

        public List<projectQuotedModel> GetQuotedProjectList(projectQuotedModel Modal)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetQuotedProjectList(Modal);


        }

        public List<VendorQuoteModal> GetQuotedSubmitbyVendor(string ProjectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetQuotedSubmitbyVendor(ProjectId);


        }

        public VendorQuoteModal GetProjectQuotedByVendor(int CompanyId, int ProjectId, int VendorId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProjectQuotedByVendor(CompanyId, ProjectId, VendorId);


        }

        public int saveReasonAcceptOrRejectQuote(ReasonAcceptRejectModal modal)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.saveReasonAcceptOrRejectQuote(modal);


        }
        

       public ProjectModel.EditNewProjectModel GetNewProjectForEdit(int ProjectId,int CompanyId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetNewProjectForEdit(ProjectId,CompanyId);


        }
        public int EditNewProject(ProjectModel.NewCreateProjectModel obj)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.EditNewProject(obj);

        }


        public int DeleteEditProjectFiles(MODEL.ProjectModel.ProjectFileModel model)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.DeleteEditProjectFiles(model);


        }

        public List<vendorModel> GetProjectAwardedForVendor(FilterProjectModal Modal)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProjectAwardedForVendor(Modal);


        }


        public List<vendorModel> GetProjectAwardedForcompanyProfile(FilterProjectModal Modal)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProjectAwardedForcompanyProfile(Modal);


        }

        public VendorQuoteModal GetProjectAwardedViewDetailCompanyProfile(int ProjectId, int VendorId,int ComapnyId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProjectAwardedViewDetailCompanyProfile(ProjectId, VendorId, ComapnyId);


        }

        public List<ProjectCompanyModal> CompanyListForAwardedProjectDropVendorProfile(int VendorId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.CompanyListForAwardedProjectDropVendorProfile(VendorId);
        }

        public ProjectCompleteModal GetVendorProjectforCompletion(int ProjectId, int CompanyId,int VendorId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetVendorProjectforCompletion(ProjectId, CompanyId, VendorId);


        }

        public ProjectCompleteModal CompletionVendorProject(ProjectCompleteModal modal)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.CompletionVendorProject(modal);


        }


        public List<ProjectVendorModal> GetVendorListForDrop(int VendorId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetVendorListForDrop(VendorId);


        }

        public List<FilterProjectByNameModal> GetProjectName(FilterProjectByNameModal Modal)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProjectName(Modal);


        }

        public List<CustomControlUIModel> GetCustomControlProjectCreation(int CompanyId)
        {

            projects = new TIGHTEN.DATA.Projects();
            return projects.GetCustomControlProjectCreation(CompanyId);


        }
        public List<vendorModel> getProjectVendorByCompIdId(int CompanyId, int Id)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.getProjectVendorByCompIdId(CompanyId, Id);


        }
        public List<vendorModel> getApprovedProjectByUserId(string UserId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.getApprovedProjectByUserId(UserId);


        }
        public List<vendorModel> GetApprovedBillableProjectByUserId(string UserId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.getApprovedBillableProjectByUserId(UserId);


        }
        public List<vendorModel> GetProjectMilestonesByProjectId(int ProjectId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProjectMilestonesByProjectId(ProjectId);


        }
        public List<MODEL.ProjectModel.ProjectWithForFinancialManager> GetProjectsOfCompany(int CompanyId)
        {
            projects = new TIGHTEN.DATA.Projects();
            return projects.GetProjectsOfCompany(CompanyId);
        }
    }
}
