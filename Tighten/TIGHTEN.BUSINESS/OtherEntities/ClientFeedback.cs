﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;


namespace TIGHTEN.BUSINESS
{
    public class ClientFeedback
    {
        TIGHTEN.DATA.ClientFeedback clientFeedback;
        /// <summary>
        /// This function fetches all Feedbacks which are associated to the logged in user
        /// </summary>
        /// <param name="UserId">It is the unique id of logged in User</param>
        /// <returns>List of Client Feedbacks associated to the user</returns>
        public List<ClientFeedbackModel.ClientFeedbackDetailModel> getClientFeedbacks(string UserId) {
            clientFeedback = new DATA.ClientFeedback();
            return clientFeedback.getClientFeedbacks(UserId);
        }
        /// <summary>
        /// It saves the Client Feedback
        /// </summary>
        /// <param name="UserId">Unique id of user</param>
        /// <param name="model">it consists the values to be saved</param>
        public void SaveClientFeedback(string UserId, ClientFeedbackModel.ClientFeedbackAddModel model)
        {
            clientFeedback = new DATA.ClientFeedback();
            clientFeedback.SaveClientFeedback(UserId, model);
        }
    }
}
