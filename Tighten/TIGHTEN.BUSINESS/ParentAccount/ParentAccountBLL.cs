﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA.ParentAccount;
using TIGHTEN.MODEL;
using TIGHTEN.MODEL.ParentAccount;
namespace TIGHTEN.BUSINESS.ParentAccount
{
    public class ParentAccountBLL
    {

        ParentAccountDAL objDal;
        public ParentAccountBLL()
        {
            objDal = new ParentAccountDAL();
        }
        public List<ParentAccountModel> GetParentAccountList(int CompanyId)
        {
            
            return objDal.GetParentAccountList(CompanyId);


        }
        public ParentAccountModel GetParentAccountById(int Id)
        {

            return objDal.GetParentAccountById(Id);


        }
        public string SaveNewParentAccount(ParentAccountModel saveObj)
        {
          
            return objDal.SaveNewParentAccount(saveObj);


        }
        public string deleteParentAccountDetail(int Id)
        {
            return objDal.deleteParentAccountDetail(Id);
        }
        public string updateParentAccountDetail(ParentAccountModel saveObj)
        {
            return objDal.updateParentAccountDetail(saveObj);
        }
        public List<ParentAccountModel> GetParentAccountForVendorList(int VendorId)
        {

            return objDal.GetParentAccountForVendorList(VendorId);


        }
    }
}
