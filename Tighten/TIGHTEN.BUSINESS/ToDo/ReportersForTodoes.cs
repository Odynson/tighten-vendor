﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class ReportersForTodoes
    {
        TIGHTEN.DATA.ReportersForTodoes ToDoReporters;

        public List<MODEL.ReportersForTodoesModel.ReportersDetail> getToDoReporters(string UserId, int CompanyId, int ProjectId, int RoleId)
        {
            ToDoReporters = new TIGHTEN.DATA.ReportersForTodoes();
            return ToDoReporters.getToDoReporters(UserId, CompanyId, ProjectId, RoleId);
        }


        //public async Task SendMailToSelectedReporters(MODEL.ReportersForTodoesModel.ReportersEmailData model)
        //{
        //    ToDoReporters = new TIGHTEN.DATA.ReportersForTodoes();
        //    await ToDoReporters.SendMailToSelectedReporters(model);
        //    //return true;
        //}


        public string SaveInvoiceDetails(MODEL.ReportersForTodoesModel.ReportersEmailData model)
        {
            ToDoReporters = new TIGHTEN.DATA.ReportersForTodoes();
            return ToDoReporters.SaveInvoiceDetails(model);
        }

        public MODEL.ReportersForTodoesModel.TodosForPdf_List getAllTodosForPreview(MODEL.ReportersForTodoesModel.TodosForPreview_RequiredParams model)
        {
            ToDoReporters = new TIGHTEN.DATA.ReportersForTodoes();
            return ToDoReporters.getAllTodosForPreview(model);
        }


    }
}
