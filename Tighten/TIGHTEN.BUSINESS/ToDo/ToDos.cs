﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class ToDos
    {
        DATA.ToDos todo;
        /// <summary>
        /// Getting All Todos for a particular Project
        /// </summary>
        /// <param name="ProjectId">It is unique Id of Project</param>
        /// <returns>Returns List Of Todos with Sections</returns>
        public List<TodoModel.SectionData> GetAllTodosWithSections(int ProjectId)
        {
            todo = new DATA.ToDos();
            return todo.GetAllTodosWithSections(ProjectId);
        }


        /// <summary>
        /// Getting All Todos for All Project
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>Returns List Of Todos with Sections for All Project</returns>
        public List<TodoModel.SectionData> TodosWithSectionsForAllProjects(int CompanyId)
        {
            todo = new DATA.ToDos();
            return todo.TodosWithSectionsForAllProjects(CompanyId);
        }


        /// <summary>
        /// This function fetches my Todos for MyTOdos Tab
        /// </summary>
        /// <param name="UserId">It is unique id of logged in user</param>
        /// <returns>returns todos in list</returns>
        public List<TodoModel.TodosData> getMyTodos(string UserId)
        {
            todo = new DATA.ToDos();
            return todo.getMyTodos(UserId);
        }

        /// <summary>
        /// This function fetches details of a particular Todo
        /// </summary>
        /// <param name="TodoId">It is unique id of Todo</param>
        /// <returns>returns todos in list</returns>
        public TodoModel.TodosData getTodo(int TodoId, string userId)
        {
            todo = new DATA.ToDos();
            return todo.getTodo(TodoId, userId);
        }

        public string getLoggedTime(int TodoId, string userId)
        {
            todo = new DATA.ToDos();
            return todo.getLoggedTime(TodoId, userId);
        }
        // stop the in progress todo 
        public string updateInProgressTodo(int TodoId, string userId)
        {
            todo = new DATA.ToDos();
            return todo.getLoggedTime(TodoId, userId);
        }

        public object getSingleToDo(int toDoId, string UserId)
        {
            todo = new DATA.ToDos();
            return todo.getSingleToDo(toDoId, UserId);
        }
        /*  This function is used for saving the new Todo*/
        public int saveToDo(TodosModel.TodoAddModel model, string UserId,int CompanyId)
        {
            todo = new DATA.ToDos();
            return todo.saveToDo(model, UserId, CompanyId);
        }


        ///*  This function is used for Send Mail for saving the new Todo*/
        //public async Task SendMailForSaveNewTodo(TodosModel.TodoAddModel model)
        //{
        //    todo = new DATA.ToDos();
        //    await todo.SendMailForSaveNewTodo(model);
        //}

        /*  This function is used for Send Mail for saving the new Todo*/
        public void ThreadForSendMailForSaveNewTodo(TodosModel.TodoAddModel model,string UserId, int CompanyId, int ProjectId)
        {
            todo = new DATA.ToDos();
            todo.ThreadForSendMailForSaveNewTodo(model, UserId, CompanyId, ProjectId);
        }

        /* This function is used for Editing Todo */

        public string editToDo(TodosModel.TodoUpdateModel model, string UserId)
        {
            todo = new DATA.ToDos();
            return todo.editToDo(model, UserId);
        }


        /* This function is used for getting Todo Tags */
        public List<TaskTypeForCreatingTodo> getTodoTags(string TaskTypeIds,int TodoId)
        {
            todo = new DATA.ToDos();
            return todo.getTodoTags(TaskTypeIds,TodoId);
        }


        /* This function is used for Send Mail For Start and Stop TodoProgress*/
        public async Task SendMailForStartStopTodoProgress(TodosModel.NewTodoUpdateModel obj, string ValueForSendMailTo)
        {
            todo = new DATA.ToDos();
            await todo.SendMailForStartStopTodoProgress(obj, ValueForSendMailTo);
        }

        /* This function is used for Threading For Send Mail For Start and Stop TodoProgress*/
        public void ThreadForSendMailForStartStopTodoProgress(string url, TodosModel.NewTodoUpdateModel obj, string ValueForSendMailTo, int CompanyId, int ProjectId)
        {
            todo = new DATA.ToDos();
            todo.ThreadForSendMailForStartStopTodoProgress(url, obj, ValueForSendMailTo, CompanyId, ProjectId);
        }

        public async Task updateToDo(TodoModel.ToDo model)
        {
            todo = new DATA.ToDos();
            await todo.updateToDo(model);
        }

        public void deleteToDo(int Id)
        {
            todo = new DATA.ToDos();
            todo.deleteToDo(Id);
        }

        public object getAssigneeUsers(string userId)
        {
            todo = new DATA.ToDos();
            return todo.getAssigneeUsers(userId);
        }

        public async Task sendAssigneeEmail(string destinationemail, string taskName)
        {
            todo = new DATA.ToDos();
            await todo.sendAssigneeEmail(destinationemail, taskName);
        }
        public TodoTimeLogModel getRunningTaskDetail(string UserId)
        {
            todo = new DATA.ToDos();
            return todo.getRunningTaskDetail(UserId);
        }


        public List<TodoDetailForNotification> getTodoDetailForNotification(int TodoId)
        {
            todo = new DATA.ToDos();
            return todo.getTodoDetailForNotification(TodoId);
        }

        public BothTaskTypeForSelectize getTaskTypeForCreatingTodo(int TodoId)
        {
            todo = new DATA.ToDos();
            return todo.getTaskTypeForCreatingTodo(TodoId);
        }


        public void SaveTodoDescription(TodosModel.TodoUpdateModel model)
        {
            todo = new DATA.ToDos();
            todo.SaveTodoDescription(model);
        }

        public void UpdateTodoName(TodosModel.TodoUpdateModel model)
        {
            todo = new DATA.ToDos();
            todo.UpdateTodoName(model);
        }

        public void UpdateTodoReporter(string UserId, TodosModel.ReporterUpdateModel model)
        {
            todo = new DATA.ToDos();
            todo.UpdateTodoReporter(UserId, model);
        }

        public void UpdateTodoDeadlineDate(TodosModel.TodoUpdateModel model)
        {
            todo = new DATA.ToDos();
            todo.UpdateTodoDeadlineDate(model);
        }

        public void UpdateTodoEstimatedTime(TodosModel.TodoUpdateModel model)
        {
            todo = new DATA.ToDos();
            todo.UpdateTodoEstimatedTime(model);
        }

        public void UpdateTodoTaskType(TodosModel.TodoUpdateModel model)
        {
            todo = new DATA.ToDos();
            todo.UpdateTodoTaskType(model);
        }



    }
}
