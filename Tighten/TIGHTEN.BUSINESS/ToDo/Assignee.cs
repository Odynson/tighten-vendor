﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class Assignee
    {
        DATA.Assignee assignee;
        public void UpdateAssignee(string UserId, AssigneeModel.AssigneeUpdateModel model)
        {
            assignee = new DATA.Assignee();
            assignee.UpdateAssignee(UserId, model);
        }
    }
}
