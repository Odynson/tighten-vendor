﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class ToDoComments
    {
        DATA.ToDoComments todoComments;
        public object getToDoComments(int toDoId, string UserId)
        {
            todoComments = new DATA.ToDoComments();
            return todoComments.getToDoComments(toDoId, UserId);
        }
        public void saveToDoComment(TodoModel.ToDoComment model)
        {
            todoComments = new DATA.ToDoComments();
            todoComments.saveToDoComment(model);
        }
        public void deleteToDoComment(int Id)
        {
            todoComments = new DATA.ToDoComments();
            todoComments.deleteToDoComment(Id);
        }
        public void deleteAllToDoComments(int Id)
        {
            todoComments = new DATA.ToDoComments();
            todoComments.deleteAllToDoComments(Id);
        }
    }
}
