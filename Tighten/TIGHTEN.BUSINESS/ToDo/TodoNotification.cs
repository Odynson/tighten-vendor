﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class TodoNotification
    {
        DATA.TodoNotification todoNotification;
        public List<TodoNotificationModel.TodoNotificationDetailModel> GetTodoNotifications(int TodoId)
        {
            todoNotification = new DATA.TodoNotification();
            return todoNotification.GetTodoNotifications(TodoId);
        }
    }
}
