﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class UserToDoApproval
    {
        DATA.UserToDoApproval todo;
        /// <summary>
        /// This function fetches my Todos for MyTOdos Tab
        /// </summary>
        /// <param name="UserId">It is unique id of logged in user</param>
        /// <returns>returns todos in list</returns>
        public List<TodoApproval> getTodos(TodoApprovalSearch search)
        {
            todo = new DATA.UserToDoApproval();
            return todo.getTodos(search);
        }

        
        public List<ProjectResult> UserProjects(int CompanyId, string UserId)
        {
            todo = new DATA.UserToDoApproval();
            return todo.UserProjects(CompanyId, UserId);
        }

        public string DeleteUserLoggedTimeSlot(int TodoDetailId)
        {
            todo = new DATA.UserToDoApproval();
            return todo.DeleteUserLoggedTimeSlot(TodoDetailId);
        }
        public string UpdateUserLoggedTimeSlot(string UserId,int TodoDetailId, int minutes)
        {
            todo = new DATA.UserToDoApproval();
            return todo.UpdateUserLoggedTimeSlot(UserId,TodoDetailId, minutes);
        }
        public List<ProjectResult> getUserProjectsForAllCompanies(string UserId, int CompanyId)
        {
            todo = new DATA.UserToDoApproval();
            return todo.getUserProjectsForAllCompanies(UserId, CompanyId);
        }

        public List<ProjectResult> getUserCompanies(string UserId)
        {
            todo = new DATA.UserToDoApproval();
            return todo.getUserCompanies(UserId);
        }

    }
}
