﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class UserFeedback
    {
        DATA.UserFeedback userFeedback;
        /// <summary>
        /// This method saves the User Feedback
        /// </summary>
        /// <param name="userId">It is the unique id of logged in User</param>
        /// <param name="model">This model consists all new values of fields to be saved</param>
        public void SaveUserFeedback(string userId, UserFeedbackModel.UserFeedbackAddModel model)
        {
            userFeedback = new DATA.UserFeedback();
            userFeedback.SaveUserFeedback(userId, model);
        }

        /// <summary>
        /// It gets the User Feedbacks
        /// </summary>
        /// <param name="userId">It is the unique id of logged in user</param>
        public List<UserFeedbackModel.UserFeedbackDetailsModel> GetUserFeedbacks(string userId)
        {
            userFeedback = new DATA.UserFeedback();
            return userFeedback.GetUserFeedbacks(userId);
        }

        /// <summary>
        /// This method Updates the User Feedback Voter
        /// </summary>
        /// <param name="userId">It is the unique id of logged in User</param>
        /// <param name="model">This model consists all new values of fields to be saved</param>
        public void UpdateUserFeedbackVoter(string userId, UserFeedbackModel.UserFeedbackVoterUpdateModel model)
        {
            userFeedback = new DATA.UserFeedback();
            userFeedback.UpdateUserFeedbackVoter(userId, model);
        }

        /// <summary>
        /// Fetching Comments for a particular User Feedback
        /// </summary>
        /// <param name="UserFeedbackId">It is Unique id of User Feedback</param>
        /// <returns>It returns list of Comments for a particular User Feedback</returns>
        public List<UserFeedbackModel.UserFeedbackComments> GetUserFeedbackComment(int UserFeedbackId)
        {
            userFeedback = new DATA.UserFeedback();
            return userFeedback.GetUserFeedbackComment(UserFeedbackId);
        }

        /// <summary>
        /// This method saves the User Feedback Comment for a particualr User Feedback
        /// </summary>
        /// <param name="userId">It is the unique id of logged in User</param>
        /// <param name="model">This model consists all new values of fields to be saved</param>
        public void SaveUserFeedbackComment(string userId, UserFeedbackModel.UserFeedbackCommentAddModel model)
        {
            userFeedback = new DATA.UserFeedback();
            userFeedback.SaveUserFeedbackComment(userId, model);
        }

    }
}
