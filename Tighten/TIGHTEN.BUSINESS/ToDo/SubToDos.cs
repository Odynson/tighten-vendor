﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
     public class SubToDos
    {
        DATA.SubToDos subToDos;
        public object getSubTodos(string userId, int ToDoId)
        {
            subToDos = new DATA.SubToDos();
            return subToDos.getSubTodos(userId, ToDoId);
        }
        public void saveSubTodo(string userId, TodoModel.SubToDo model)
        {
            subToDos = new DATA.SubToDos();
            subToDos.saveSubTodo(userId, model);
        }
        public void updateSubTodo(TodoModel.SubToDo model)
        {
            subToDos = new DATA.SubToDos();
            subToDos.updateSubTodo(model);
        }
        public void deleteSubTodo(int Id)
        {
            subToDos = new DATA.SubToDos();
            subToDos.deleteSubTodo(Id);
        }
    }
}
