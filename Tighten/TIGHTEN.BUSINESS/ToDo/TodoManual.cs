﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class TodoManual
    {
        DATA.TodoManual todoManual;
        /// <summary>
        /// This function fetches my Todos for MyTOdos Tab
        /// </summary>
        /// <param name="UserId">It is unique id of logged in user</param>
        /// <returns>returns todos in list</returns>
        public List<TodoManualModel.TodoAddModel> getMyTodos(string UserId,int RoleId)
        {
            todoManual = new DATA.TodoManual();
            return todoManual.getMyTodos(UserId, RoleId);
        }
        public int saveToDo(TodoManualModel.TodoAddModel model)
        {
            todoManual = new DATA.TodoManual();
            return todoManual.saveToDo(model);
        }
        //Upddate the data
        public string UpdateToDo(TodoManualModel.TodoAddModel model)
        {
            todoManual = new DATA.TodoManual();
            return todoManual.UpdateToDo(model);
        }

    }
}
