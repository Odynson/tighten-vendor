﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class FileTag
    {
        DATA.FileTag fileTag;
        public object GetFilesTagged(int CommentId)
        {
            fileTag = new DATA.FileTag();
            return fileTag.GetFilesTagged(CommentId);
        }
        public void SaveFilesTagged(int commentId, string FileName, string OriginalFileName, string FilePath, bool IsGif)
        {
            fileTag = new DATA.FileTag();
            fileTag.SaveFilesTagged(commentId, FileName, OriginalFileName, FilePath, IsGif);
        }
    }
}
