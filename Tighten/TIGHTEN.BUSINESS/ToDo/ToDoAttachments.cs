﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class ToDoAttachments
    {
        DATA.ToDoAttachments todoAttachments;
        /// <summary>
        /// This function fetches Attachments for particular Todo
        /// </summary>
        /// <param name="ToDoId">It is unique id of Todo</param>
        /// <param name="userId">It is unique id of logged in user</param>
        /// <returns>List of attachments related to particular Todo</returns>
        public List<AttachmentModel.AttachmentDetailsModel> getAttachments(int ToDoId, string userId)
        {
            todoAttachments = new DATA.ToDoAttachments();
            return todoAttachments.getAttachments(ToDoId, userId);
        }
        /// <summary>
        /// This funtion fetches attachments related to a Project
        /// </summary>
        /// <param name="ProjectId">It is unique Id of Project</param>
        /// <returns>List of attachemnts of a particular project</returns>
        public List<AttachmentModel.AttachmentDetailsModel> getAttachmentsForParticularProject(int ProjectId)
        {
            todoAttachments = new DATA.ToDoAttachments();
            return todoAttachments.getAttachmentsForParticularProject(ProjectId);
        }

        public string saveToDoAttachments(string AttachmentType, int ToDoId, string AttachmentName, string OriginalName, string userId,string SourceImage)
        {
            todoAttachments = new DATA.ToDoAttachments();
            return todoAttachments.saveToDoAttachments(AttachmentType, ToDoId, AttachmentName, OriginalName,userId, SourceImage);
        }
        public void saveCloudToDoAttachments(TodoModel.ToDoAttachments model, string UserId)
        {
            todoAttachments = new DATA.ToDoAttachments();
            todoAttachments.saveCloudToDoAttachments(model, UserId);
        }
        public string deleteAttachment(int Id)
        {
            todoAttachments = new DATA.ToDoAttachments();
            return todoAttachments.deleteAttachment(Id);
        }
        /// <summary>
        ///  It saves the DropBox files Link which the user has added from DropBox 
        /// </summary>
        ///  <param name="UserId">It is Unique Id of Logged In User</param>
        /// <param name="TodoId">It is Unique Id of Todo</param>
        /// <param name="model">It is list of files being selected with their names and link on dropbox</param>
        public void saveDropBoxFiles(string UserId, int TodoId, List<CloudFilesModel.DropBoxFilesModel> model)
        {
            todoAttachments = new DATA.ToDoAttachments();
            todoAttachments.saveDropBoxFiles(UserId, TodoId, model);
        }
        /// <summary>
        ///  It saves the Box files Link which the user has added from Box 
        /// </summary>
        ///  <param name="UserId">It is Unique Id of Logged In User</param>
        /// <param name="TodoId">It is Unique Id of Todo</param>
        /// <param name="model">It is list of files being selected with their names and url on box</param>
        public void saveBoxFiles(string UserId, int TodoId, List<CloudFilesModel.BoxFilesModel> model)
        {
            todoAttachments = new DATA.ToDoAttachments();
            todoAttachments.saveBoxFiles(UserId, TodoId, model);
        }
        /// <summary>
        ///  It saves the Google Drive files Link which the user has added from Google Drive 
        /// </summary>
        ///  <param name="UserId">It is Unique Id of Logged In User</param>
        /// <param name="TodoId">It is Unique Id of Todo</param>
        /// <param name="model">It is list of files being selected with their names and Url on Google Drive</param>
        public void saveGoogleDriveFiles(string UserId, int TodoId, List<CloudFilesModel.GoogleDriveFilesModel> model)
        {
            todoAttachments = new DATA.ToDoAttachments();
            todoAttachments.saveGoogleDriveFiles(UserId,TodoId, model);
        }

    }
}
