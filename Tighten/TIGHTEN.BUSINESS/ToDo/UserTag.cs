﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class UserTag
    {
        DATA.UserTag userTag;
        public object GetUsersTagged(int CommentId)
        {
            userTag = new DATA.UserTag();
            return userTag.GetUsersTagged(CommentId);
        }

        public void SaveUsersTagged(int commentId, string userId)
        {
            userTag = new DATA.UserTag();
            userTag.SaveUsersTagged(commentId,userId);
        }
    }
}
