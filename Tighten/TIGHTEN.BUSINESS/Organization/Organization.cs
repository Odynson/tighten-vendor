﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class Organization
    {
        public List<MODEL.OrganizationModel> GetAllOrganizations(string UserId)
        {
            DATA.Organization objDll = new DATA.Organization();
            return objDll.GetAllOrganizations(UserId);
        }

        public List<MODEL.OrganizationTeamMembersModel> GetOrganization(string UserId, int CompanyId)
        {
            DATA.Organization objDll = new DATA.Organization();
            return objDll.GetOrganization(UserId, CompanyId);
        }



         

    }
}
