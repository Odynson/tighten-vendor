﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class  Invoice
    {
        DATA.Invoice objDll;

        public MODEL.UserInvoice.InvoiceModelBothApprovedandNotApproved GetInvoice(MODEL.UserInvoice.InvoiceQuery model)
        {
            objDll = new DATA.Invoice();
            return objDll.GetInvoice(model);
        }

        public MODEL.ReportersForTodoesModel.TodosForPdf_List GetInvoiceDetail(MODEL.UserInvoice.AdminInvoiceDetailsRequiredParams model, string DefaultInvoiceNumber)
        {
            objDll = new DATA.Invoice();
            return objDll.GetInvoiceDetail(model, DefaultInvoiceNumber);
        }


        public string ChangeInvoiceStatus(MODEL.UserInvoice.InvoiceStatus model,int StripeApplicationFee)
        {
            objDll = new DATA.Invoice();
            return objDll.ChangeInvoiceStatus(model, StripeApplicationFee);
        }


        public List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> GetProjectMembersForUser(MODEL.UserInvoice.ProjectMembersToForwardInvoice_QueryParams model)
        {
            objDll = new DATA.Invoice();
            return objDll.GetProjectMembersForUser(model);
        }


        public string ForwardInvoice(MODEL.UserInvoice.ForwardInvoice model)
        {
            objDll = new DATA.Invoice();
            return objDll.ForwardInvoice(model);
        }


        public List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> getAllFreelancerForAdmin(int CompanyId)
        {
            objDll = new DATA.Invoice();
            return objDll.getAllFreelancerForAdmin(CompanyId);
        }


        public MODEL.UserInvoice.UserCardPaymentModel GetCardDetailForPayment(int PaymentDetailId, int CompanyId, int InvoiceId,int StripeApplicationFee)
        {
            objDll = new DATA.Invoice();
            return objDll.GetCardDetailForPayment(PaymentDetailId, CompanyId, InvoiceId, StripeApplicationFee);
        }



        public List<MODEL.PaymentDetailModel> GetAllCardsForPaymentDetail(int CompanyId)
        {
            objDll = new DATA.Invoice();
            return objDll.GetAllCardsForPaymentDetail(CompanyId);
        }


        public string ChangeInvoiceApprovedStatus(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            objDll = new DATA.Invoice();
            return objDll.ChangeInvoiceApprovedStatus(model);
        }

        public string RejectInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            objDll = new DATA.Invoice();
            return objDll.RejectInvoice(model);
        }

        public int SaveInvoiceByVendor(InvoiceModel model)
        {
            objDll = new DATA.Invoice();
            return objDll.SaveInvoiceByVendor(model);
        }


    }
}
