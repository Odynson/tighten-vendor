﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class ToDoApproval
    {
        DATA.ToDoApproval todo;
        /// <summary>
        /// This function fetches my Todos for MyTOdos Tab
        /// </summary>
        /// <param name="UserId">It is unique id of logged in user</param>
        /// <returns>returns todos in list</returns>
        public List<TodoApproval> getTodos(TodoApprovalSearch search)
        {
            todo = new DATA.ToDoApproval();
            return todo.getTodos(search);
        }

        public int ApproveTodo(TodoApprovalUpdate search)
        {
            todo = new DATA.ToDoApproval();
            return todo.ApproveTodo(search);
        }
        public List<CompanyUserResult> CompanyUsers(int CompanyId, int RoleId, string UserId)
        {
            todo = new DATA.ToDoApproval();
            return todo.CompanyUsers(CompanyId, RoleId, UserId);
        }
    }
}
