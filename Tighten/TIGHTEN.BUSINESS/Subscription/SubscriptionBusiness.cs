﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MODEL;
using TIGHTEN.DATA;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class SubscriptionBusiness
    {
        TIGHTEN.DATA.SubscriptionData subscription;
        public List<SubscriptionModel> GetUserSubscriptions(string UserId, int CompanyId)
        {
            subscription = new TIGHTEN.DATA.SubscriptionData();
            return subscription.GetUserSubscriptions(UserId, CompanyId);
        }

        public SubscriptionModel GetUserActiveSubscriptions(string UserId, int CompanyId)
        {
            subscription = new TIGHTEN.DATA.SubscriptionData();
            return subscription.GetUserActiveSubscriptions(UserId, CompanyId);
        }

        public int AddSubscription(PaymentModel userdetail)
        {
            subscription = new TIGHTEN.DATA.SubscriptionData();
            return subscription.AddSubscription(userdetail);
        }


        public string SubsctriptionPayment(SubsctriptionPaymentModel model)
        {
            subscription = new TIGHTEN.DATA.SubscriptionData();
            return subscription.SubsctriptionPayment(model);
        }


        public int UpdateSubscription(SubscriptionModel userdetail)
        {
            subscription = new TIGHTEN.DATA.SubscriptionData();
            return subscription.UpdateSubscription(userdetail);
        }
    }
}
