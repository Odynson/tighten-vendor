﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MODEL;
using TIGHTEN.DATA;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class TransactionBusiness
    {
        TIGHTEN.DATA.TransactionData transaction;

        public List<TransactionModel> GetTransactions(int SubscriptionID)
        {
            transaction = new TIGHTEN.DATA.TransactionData();
            return transaction.GetTransactions(SubscriptionID);
        }

        public TransactionModel GetTransactionSingleOrDefault(int SubscriptionID)
        {
            transaction = new TIGHTEN.DATA.TransactionData();
            return transaction.GetTransactionSingleOrDefault(SubscriptionID);
        }

        public string AddTransaction(TransactionModel model)
        {
            transaction = new TIGHTEN.DATA.TransactionData();
            return transaction.AddTransaction(model);
        }

        public string UpdateTransaction(TransactionModel model)
        {
            transaction = new TIGHTEN.DATA.TransactionData();
            return transaction.UpdateTransaction(model);
        }
    }
}
