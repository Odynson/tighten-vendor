﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class UserProfile
    {
        TIGHTEN.DATA.UserProfile objDll;


        /// <summary>
        ///  Getting User's Educational Details for User Profile From UserEducationalDetail Table
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Educational Details as an array</returns>
        public List<MODEL.UserProfileModel.UserEducationDetail> getUserEducationDetail(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserEducationDetail(UserId);
        }




        /// <summary>
        ///  Getting User's Professional Details for User Profile From UserProfessionalDetail Table
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Professional Details as an array</returns>
        public List<MODEL.UserProfileModel.UserProfessionalDetail> getUserProfessionalDetail(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserProfessionalDetail(UserId);
        }



        /// <summary>
        ///  Save User's Professional Details for User Profile in UserProfessionalDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Professional Details as its properties </param>
        /// <returns>It returns Id of new User's Professional Details entry in UserProfessionalDetail Table </returns>
        public int saveUserProfessionalDetail(MODEL.UserProfileModel.UserProfessionalDetail model)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.saveUserProfessionalDetail(model);
        }



        /// <summary>
        ///  Update User's Professional Details for User Profile in UserProfessionalDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Professional Details as its properties </param>
        /// <returns>It returns Id of updated User's Professional Details entry from UserProfessionalDetail Table </returns>
        public int updateUserProfessionalDetail(MODEL.UserProfileModel.UserProfessionalDetail model)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.updateUserProfessionalDetail(model);
        }



        /// <summary>
        ///  Save User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Education Details as its properties </param>
        /// <returns>It returns Id of new User's Education Details entry in UserEducationDetail Table </returns>
        public string saveUserEducationDetail(MODEL.UserProfileModel.UserEducationDetail model)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.saveUserEducationDetail(model);
        }



        /// <summary>
        ///  Update User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Education Details as its properties </param>
        /// <returns>It returns Id of updated User's Education Details entry from UserEducationDetail Table </returns>
        public int updateUserEducationDetail(MODEL.UserProfileModel.UserEducationDetail model)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.updateUserEducationDetail(model);
        }


        /// <summary>
        ///  Delete User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <param name="EducationDetailObjectId">It is an object which contain info for Education Details as its properties </param>
        /// <returns>It returns the confirmation message for User's Education Details entry from UserEducationDetail Table </returns>
        public void deleteUserEducationDetail(int EducationDetailObjectId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            objDll.deleteUserEducationDetail(EducationDetailObjectId);
        }



        /// <summary>
        ///  Delete User's Professional Details for User Profile in UserProfessionalDetail Table
        /// </summary>
        /// <param name="ProfessionalDetailObjectId">It is an object which contain info for Professional Details as its properties </param>
        /// <returns>It returns the confirmation message for User's Professional Details entry from UserProfessionalDetail Table </returns>
        public void deleteUserProfessionalDetail(int ProfessionalDetailObjectId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            objDll.deleteUserProfessionalDetail(ProfessionalDetailObjectId);
        }



        /// <summary>
        ///  Update User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Education Details as its properties </param>
        /// <returns>It returns Id of updated User's Education Details entry from UserEducationDetail Table </returns>
        public string updateUserHourlyRate(string UserId,decimal newHourlyRate)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.updateUserHourlyRate(UserId, newHourlyRate);
        }



        /// <summary>
        ///  Update User's Availabilty for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User </param>
        /// <returns>It returns the new Availabilty </returns>
        public string updateUserAvailabilty(string UserId, decimal AvailableHours)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.updateUserAvailabilty(UserId, AvailableHours);
        }


        /// <summary>
        ///  Getting User's Favourite Projects for User Profile From UserDetails Table
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Favourite Projects as an array</returns>
        public ProjectModel.UserFavouriteProjects getUserFavouriteProjects(string UserId )
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserFavouriteProjects(UserId );
        }



        /// <summary>
        ///  Getting User's Availabilty for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Availabilty</returns>
        public decimal getUserAvailabilty(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserAvailabilty(UserId);
        }


        /// <summary>
        ///  Save User's Favourite Projects for User Profile in UserDetails Table
        /// </summary>
        /// <param name="mpdel">It contains UserId (unique id for user) and ProjectId array</param>
        /// <returns>It returns success message</returns>
        public string saveUserFavouriteProjects(MODEL.UserProfileModel.SaveFavoriteProjectsModel model)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.saveUserFavouriteProjects(model);
        }


        /// <summary>
        ///  Getting User's Statistics for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Statistics as an array</returns>
        public UserProfileModel.UserStatistics getUserStatistics(string UserId,string CurrentUserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserStatistics(UserId, CurrentUserId);
        }



        /// <summary>
        ///  Getting User's Efficiency for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Efficiency as an array</returns>
        public MODEL.UserProfileModel.TodoEfficiencyMeter getUserEfficiency(string UserId,string CurrentUserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserEfficiency(UserId, CurrentUserId);
        }




        /// <summary>
        ///  Getting User's Efficiency for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Efficiency as an array</returns>
        public MODEL.UserProfileModel.TechnologyList getUserEfficiencyForStrongAndWeekTechnology(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserEfficiencyForStrongAndWeekTechnology(UserId);
        }




        /// <summary>
        ///  Getting User's Avg Rate for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Efficiency as an array</returns>
        public decimal getUserAvgRate(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserAvgRate(UserId);
        }



        /// <summary>
        ///  Getting User's Tags for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Tags as an array</returns>
        public MODEL.UserProfileModel.UserTags getUserTags(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserTags(UserId);
        }




        /// <summary>
        ///  Getting User's Skill Set Tags for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Skill Set Tags as an array</returns>
        public MODEL.UserProfileModel.UserTags getUserSkillSetTags(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserSkillSetTags(UserId);
        }




        /// <summary>
        ///  Getting User's Tags for Professional Details in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Tags for Professional Details as an array</returns>
        public MODEL.UserProfileModel.UserTagsForProfessionalDetail getTagsForProfessionalDetails(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getTagsForProfessionalDetails(UserId);
        }



        /// <summary>
        ///  Save User's Tags for User Profile in Tags Table
        /// </summary>
        /// <param name="mpdel">It contains UserId (unique id for user) and TagId array</param>
        /// <returns>It returns success message</returns>
        public string saveUserTags(MODEL.UserProfileModel.SaveTags model)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.saveUserTags(model);
        }



        /// <summary>
        ///  Getting User's Team And Project Count in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Team And Project Count </returns>
        public MODEL.UserProfileModel.UserTeamAndProjectCount getUserTeamAndProjectCount(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserTeamAndProjectCount(UserId);
        }



        /// <summary>
        ///  Getting User's Testimonials in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Testimonials </returns>
        public MODEL.UserProfileModel.UserTestimonials getUserTestimonials(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserTestimonials(UserId);
        }


        /// <summary>
        ///  Getting User's Testimonials in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Testimonials </returns>
        public List<MODEL.UserProfileModel.UserFeedbacks> getUserFeedbacks(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserFeedbacks(UserId);
        }



        /// <summary>
        ///  Getting Other User's Profile Status in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns Other User's Profile Status </returns>
        public bool getOtherUserProfileStatus(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getOtherUserProfileStatus(UserId);
        }




        /// <summary>
        ///  Getting User's Testimonials in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Testimonials </returns>
        public string saveUserTestimonials(MODEL.UserProfileModel.SaveTestimonial model)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.saveUserTestimonials(model);
        }




        /// <summary>
        ///  Change User's ProfileStatus in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's ProfileStatus </returns>
        public bool changeUserProfile(MODEL.UserProfileModel.USerProfileStatusModel model)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.changeUserProfile(model);
        }


        /// <summary>
        ///  Getting Other User's Educational Streams
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns All User's Educational Streams </returns>
        public MODEL.UserProfileModel.UserEducationalStremModel getEducationalStreams(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getEducationalStreams(UserId);
        }



        /// <summary>
        ///  Getting Other User's Email-Id
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns  User's Email-Id </returns>
        public string getUserEmailFromUserId(string UserId)
        {
            objDll = new TIGHTEN.DATA.UserProfile();
            return objDll.getUserEmailFromUserId(UserId);
        }


    }
}
