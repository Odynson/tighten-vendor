﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class UserInvoice
    {

        DATA.UserInvoice objDll;

        public MODEL.UserInvoice.InvoiceModelBothApprovedandNotApproved GetInvoice(MODEL.UserInvoice.InvoiceQuery model)
        {
            objDll = new DATA.UserInvoice();
            return objDll.GetInvoice(model);
        }


        public MODEL.ReportersForTodoesModel.TodosForPdf_List GetInvoiceDetail(MODEL.UserInvoice.AdminInvoiceDetailsRequiredParams model, string DefaultInvoiceNumber)
        {
            objDll = new DATA.UserInvoice();
            return objDll.GetInvoiceDetail(model, DefaultInvoiceNumber);
        }


        public List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> GetProjectMembersForUser(MODEL.UserInvoice.ProjectMembersToForwardInvoice_QueryParams model)
        {
            objDll = new DATA.UserInvoice();
            return objDll.GetProjectMembersForUser(model);
        }




        public string ForwardInvoice(MODEL.UserInvoice.ForwardInvoice model)
        {
            objDll = new DATA.UserInvoice();
            return objDll.ForwardInvoice(model);
        }





    }
}
