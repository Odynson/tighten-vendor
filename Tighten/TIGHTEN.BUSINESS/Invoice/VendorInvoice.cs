﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.MODEL.Vendor;
using TIGHTEN.UTILITIES;


namespace TIGHTEN.BUSINESS
{

    public class VendorInvoice
    {
        DATA.VendorInvoice objDll;

        public List<InvoiceModel> GetAllInvoiceDetailList(InvoiceModel model)
        {

            objDll = new DATA.VendorInvoice();
            return objDll.GetAllInvoiceDetailList(model);
        }

        public InvoiceRaiseToCompanyModal GetPreviewInvoicePaidOutstandingReject(int Id,int VendorId)
        {

            objDll = new DATA.VendorInvoice();
            return objDll.GetPreviewInvoicePaidOutstandingReject(Id, VendorId);
        }

        public int SaveInvoiceByVendor(InvoiceModel model)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.SaveInvoiceByVendor(model);
        }


        public List<InvoiceModel> GetAllVendorInvoiceList(VendorInvoiceRequestFilterModel modal)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetAllVendorInvoiceList(modal);
        }


        public InvoiceRaiseToProject GetPhaseAndResourceforProject(int ProjectId,int VendorId)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetPhaseAndResourceforProject(ProjectId, VendorId);


        }

        public InvoiceRaiseToCompanyModal GetVendorInvoicePreview(int CompanyId, int Id)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetVendorInvoicePreview(CompanyId, Id);


        }

        public string SaveApproveVendorInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.SaveApproveVendorInvoice(model);
        }

        public string RejectVendorInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.RejectVendorInvoice(model);
        }


        public MODEL.UserInvoice.UserCardPaymentModel GetCardDetailForVendorPayment(int PaymentDetailId, int CompanyId, int InvoiceId, int StripeApplicationFee)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetCardDetailForVendorPayment(PaymentDetailId, CompanyId, InvoiceId, StripeApplicationFee);
        }

        public List<InvoiceRaiseToCompanyModal> ProjectForInvoiceDrop(int CompanyId,int VendorId)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.ProjectForInvoiceDrop(CompanyId, VendorId);
        }

        public List<VendorCompanyModel> GetVendorForDrop(int CompanyId)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetVendorForDrop(CompanyId);
        }
        public List<CompanyModal> GetCompanyForDrop(int CompanyId)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetCompanyForDrop(CompanyId);
        }

        public string ChangeInvoiceStatus(MODEL.UserInvoice.InvoiceStatus model, int StripeApplicationFee)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.ChangeInvoiceStatus(model, StripeApplicationFee);
        }

        #region paging

        public List<InvoiceModel> GetAllVendorInvoiceListOutstanding(VendorInvoiceRequestFilterModel modal)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetAllVendorInvoiceListOutstanding(modal);
        }

        public List<InvoiceModel> GetAllVendorInvoiceListPaid(VendorInvoiceRequestFilterModel modal)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetAllVendorInvoiceListPaid(modal);
        }

        public List<InvoiceModel> GetAllVendorInvoiceListRejected(VendorInvoiceRequestFilterModel modal)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetAllVendorInvoiceListRejected(modal);
        }

        public List<InvoiceModel> GetAllInvoiceDetailListOutstanding(InvoiceModel model)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetAllInvoiceDetailListOutstanding(model);
        }
        public List<InvoiceModel> GetAllInvoiceDetailListPaid(InvoiceModel model)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetAllInvoiceDetailListPaid(model);
        }

        public List<InvoiceModel> GetAllInvoiceDetailListRejected(InvoiceModel model)
        {
            objDll = new DATA.VendorInvoice();
            return objDll.GetAllInvoiceDetailListRejected(model);
        }
        #endregion

    }
}
