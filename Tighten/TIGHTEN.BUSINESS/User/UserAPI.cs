﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
//using MODEL;
namespace TIGHTEN.BUSINESS
{
    public class UserAPI
    {
        DATA.UsersAPI userApi;

        /// <summary>
        /// Get all users
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public List<MODEL.UsersDetailsModel> getUsers(String UserId, bool checkEmailConfirmed, int CompanyId)
        {
            userApi = new DATA.UsersAPI();
            return userApi.getUsers(UserId, checkEmailConfirmed, CompanyId);
        }

        public List<MODEL.UsersDetailsModel> GetFreelancers(String UserId, bool checkEmailConfirmed, int CompanyId)
        {
            userApi = new DATA.UsersAPI();
            return userApi.GetFreelancers(UserId, checkEmailConfirmed, CompanyId);
        }

        public int GetActiveUsersCount(int CompanyId)
        {
            userApi = new DATA.UsersAPI();
            return userApi.getActiveUsersCount(CompanyId);
        }

        /// <summary>
        /// get user by user id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public MODEL.UserModel getUser(string UserId, int CompanyId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.getUser(UserId, CompanyId);
        }

        /// <summary>
        /// get username by user id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public string getUserName(string UserId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.getUserName(UserId);
        }

        /// <summary>
        /// get userEmail by user id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public string getUserEmail(string UserId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.getUserEmail(UserId);
        }

        /// <summary>
        /// get user by user id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public MODEL.CardModel getUserCardInfo(string UserId, int CompanyId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.getUserCardInfo(UserId, CompanyId);
        }

        /// <summary>
        /// get user by user id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public bool IsUserCardInfoExist(string UserId, int CompanyId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.IsUserCardInfoExist(UserId, CompanyId);
        }

        /// <summary>
        /// save the user detail
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="ParentUserId"></param>
        /// <param name="model"></param>
        public MODEL.UserModel saveUser(String UserId, String ParentUserId, MODEL.AddNewUserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.saveUser(UserId, ParentUserId, model);
        }

        public void UpdateUserRole(string ParentUserId, MODEL.UserRoleModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.UpdateUserRole(ParentUserId, model);
        }

        /// <summary>
        /// update user detail
        /// </summary>
        /// <param name="model"></param>
        public void updateUser(MODEL.UserModel model)
        {
            userApi = new DATA.UsersAPI();
            userApi.updateUser(model);
        }

        /// <summary>
        /// Update my profile
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="model"></param>
        public void updateMyProfile(string userid, MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.updateMyProfile(userid, model);
        }

        /// <summary>
        /// Update userprofile pic path
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public string updateUserProfilePhoto(string FileName, string UserId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.updateUserProfilePhoto(FileName, UserId);
        }

        /// <summary>
        /// clear teh profile pic
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public string clearProfilePhoto(string UserId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.clearProfilePhoto(UserId);
        }

        /// <summary>
        /// delete the user
        /// </summary>
        /// <param name="UserId"></param>
        public void DeleteUser(String UserId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.DeleteUser(UserId);
        }

        /// <summary>
        /// update the team selected
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="model"></param>
        public void updateSelectedTeam(String UserId, MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.updateSelectedTeam(UserId, model);
        }

        /// <summary>
        /// send email notification on update
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="model"></param>
        public void updateSendEmailNotification(String UserId, MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.updateSendEmailNotification(UserId, model);
        }

        /// <summary>
        /// send email notifcation on registration
        /// </summary>
        /// <param name="destinationEmail"></param>
        /// <param name="userId"></param>
        /// <param name="confirmEmailtoken"></param>
        /// <returns></returns>
        public async Task<string> sendMainAccountRegistrationCreationConfirmationEmail(string destinationEmail, string userId, string confirmEmailtoken)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return await userApi.sendMainAccountRegistrationCreationConfirmationEmail(destinationEmail, userId, confirmEmailtoken);
        }

        /// <summary>
        /// Send Account creation email 
        /// </summary>
        /// <param name="destinationEmail"></param>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> sendAccountCreationConfirmationEmail(string destinationEmail, string userId, string token)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return await userApi.sendAccountCreationConfirmationEmail(destinationEmail, userId, token);
        }

        /// <summary>
        /// send forgot password email
        /// </summary>
        /// <param name="destinationEmail"></param>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> sendForgetPasswordEmail(string destinationEmail, string userId, string token)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return await userApi.sendForgetPasswordEmail(destinationEmail, userId, token);
        }

        /// <summary>
        /// send email on change has been confirmed
        /// </summary>
        /// <param name="destinationEmail"></param>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> sendEmailChangeConfirmationEmail(string destinationEmail, string userId, string token)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return await userApi.sendEmailChangeConfirmationEmail(destinationEmail, userId, token);
        }

        /// <summary>
        /// Getting All Projects of User's company
        /// </summary>
        /// <param Name="CompanyId"> It is the unique id of the company whose projects will be fetched
        /// </param>
        public List<MODEL.ProjectModel.Project> GetAllProjectsOfCompany(MODEL.FreelancerInfoToSendInvitationModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.GetAllProjectsOfCompany(model);
        }

        public List<MODEL.MailSendToFreelancerOnInvitationSent> insertFreelancer(MODEL.FreelancerInvitationModel model, string url)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.insertFreelancer(model, url);
        }



        public void ThreadForSendMailToSendInvitationToFreelancer(string EmailOrHandle, List<MODEL.MailSendToFreelancerOnInvitationSent> mail)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.ThreadForSendMailToSendInvitationToFreelancer(EmailOrHandle, mail);
        }


        /// <summary>
        /// get user by user id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public MODEL.CompanyAndFreelancerModel GetCompanyAndFreelancerName(string invitationType, string EmailOrHandle, int CompanyId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.GetCompanyAndFreelancerName(invitationType, EmailOrHandle, CompanyId);
        }

        /// <summary>
        /// Update Users First Name. 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUserFirstName(MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.UpdateUserFirstName(model);
        }

        /// <summary>
        /// Update Users Last Name. 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUserLastName(MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.UpdateUserLastName(model);
        }

        /// <summary>
        /// Update Users Address. 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUserAddress(MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.UpdateUserAddress(model);
        }

        /// <summary>
        /// Update Users PhoneNo. 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUserPhoneNo(MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.UpdateUserPhoneNo(model);
        }

        /// <summary>
        /// Update Users Designation. 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUserDesignation(MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.UpdateUserDesignation(model);
        }

        /// <summary>
        /// Update Users About Me. 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUserAboutMe(MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.UpdateUserAboutMe(model);
        }

        /// <summary>
        /// Update Users Gender. 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUserGender(MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.UpdateUserGender(model);
        }

        /// <summary>
        /// Update Users Date Of Birth. 
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUserDateOfBirth(MODEL.UserModel model)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            userApi.UpdateUserDateOfBirth(model);
        }

        public bool UpdateGeneralCompanyToVendorCompany(string UserId, int CompanyId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.UpdateGeneralCompanyToVendorCompany(UserId, CompanyId);
        }

        public bool CheckIfCompanyIsVendor(int CompanyId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
             return userApi.CheckIfCompanyIsVendor(CompanyId);
        }



        public List<UserRoleModel> GetUsersForTeam(int CompanyId)
        {
            userApi = new TIGHTEN.DATA.UsersAPI();
            return userApi.GetUsersForTeam(CompanyId);
        }

    }
}
