﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MODEL;
using TIGHTEN.DATA;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class Account
    {
        TIGHTEN.DATA.Account account;
        public Account()
        {

        }
        public void saveRegistrationDetails(MODEL.RegisterBindingModel model, string userId)
        {
            account = new TIGHTEN.DATA.Account();
            //account.saveRegistrationDetails(model, userId);
        }
        public MODEL.UserModel Register(MODEL.RegisterBindingModel model)
        {
            account = new TIGHTEN.DATA.Account();
            return account.Register(model);
        }

        public bool chkUserNameExist(FreelancerInfoForRegistration model)
        {
            account = new TIGHTEN.DATA.Account();
            return account.chkUserNameExist(model);
        }

        public MODEL.UserModel Login(MODEL.Login model, string ipAddress, string userAgent)
        {
            account = new TIGHTEN.DATA.Account();
            return account.Login(model,ipAddress,userAgent);
        }

        public UserModel ConfirmRegistration(string userId, string code)
        {
            account = new TIGHTEN.DATA.Account();
            return account.ConfirmRegistration(userId, code);
        }
        public UserModel ForgotPassword(string Email)
        {
            account = new TIGHTEN.DATA.Account();
            return account.ForgotPassword(Email);
        }
        public bool setPassword(SetPasswordBindingModel model)
        {
            account = new TIGHTEN.DATA.Account();
            return account.setPassword(model);
        }
        public bool SetPasswordForLinkedInUser(SetPasswordBindingModel model)
        {
            account = new TIGHTEN.DATA.Account();
            return account.SetPasswordForLinkedInUser(model);
        }
        public bool chkPasswordLinkExpiry(SetPasswordBindingModel model)
        {
            account = new TIGHTEN.DATA.Account();
            return account.chkPasswordLinkExpiry(model);
        }
        public bool ChangePassword(string UserId, ChangePasswordModel model )
        {
            account = new TIGHTEN.DATA.Account();
            return account.ChangePassword(UserId, model);
        }

        public RegistrationMessageModa RegisterInvitationVendorCompany(VendorCompanyInvitationModal Modal,string url)
        {
           

            account = new TIGHTEN.DATA.Account();
            return account.RegisterInvitationVendorCompany(Modal, url);
           

        }

        public VendorCompanyInvitationModal GetVendorCompanyRecordForRegistration(VendorCompanyInvitationModal Modal, string url)
        {

            account = new TIGHTEN.DATA.Account();
            return account.GetVendorCompanyRecordForRegistration(Modal, url);
           
        }

        public VendorCompanyRegistrationMailModal RegistrationsVendorCompany(VendorCompanyRegistrationModal Modal)
        {

            account = new TIGHTEN.DATA.Account();
            return account.RegistrationsVendorCompany(Modal);

        }
    }
}
