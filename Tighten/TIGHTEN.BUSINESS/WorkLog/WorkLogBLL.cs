﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA.WorkLog;
using TIGHTEN.MODEL;
using TIGHTEN.MODEL.WorkLog;

namespace TIGHTEN.BUSINESS.WorkLog
{
    public class WorkLogBLL
    {
        WorkLogDAL objDal;

        public List<WorkLogDetailsModel> GetWorkLogList(string UserId)
        {
            objDal = new WorkLogDAL();
            return objDal.GetWorkLogList(UserId);


        }
        public string saveWorkLog(MODEL.WorkLog.WorkLogDetailsModel saveObj)
        {
            objDal = new WorkLogDAL();
            return objDal.saveWorkLog(saveObj);


        }
        public string editWorkLog(MODEL.WorkLog.WorkLogDetailsModel saveObj)
        {
            objDal = new WorkLogDAL();
            return objDal.editWorkLog(saveObj);


        }
        public WorkLogDetailsModel GetWorkLogById(int Id)
        {
            objDal = new WorkLogDAL();
            return objDal.GetWorkLogById(Id);


        }
        public string deleteWorkLogById(int Id)
        {
            objDal = new WorkLogDAL();
            return objDal.deleteWorkLogById(Id);


        }

        public WorkLogDetailsModel GetWorkLogEnteryList(int WorkLogId)
        {
            objDal = new WorkLogDAL();
            return objDal.GetWorkLogEnteryList(WorkLogId);


        }

        public string SaveWorkLogEntry(MODEL.WorkLog.WorkLogEnteriesModel saveObj)
        {
            objDal = new WorkLogDAL();
            return objDal.SaveWorkLogEntry(saveObj);


        }
        public WorkLogEnteriesModel GetWorkLogEntryById(int Id)
        {
            objDal = new WorkLogDAL();
            return objDal.GetWorkLogEntryById(Id);


        }
        public string editWorkLogEntry(MODEL.WorkLog.WorkLogEnteriesModel saveObj)
        {
            objDal = new WorkLogDAL();
            return objDal.editWorkLogEntry(saveObj);


        }
        public string deleteWorkLogEntryById(int Id)
        {
            objDal = new WorkLogDAL();
            return objDal.deleteWorkLogEntryById(Id);


        }
        public List<WorkLogDetailsModel> GetWorkLogListByProjectFromTo(string UserId, int ProjectId, string FromDate, string ToDate)
        {
            objDal = new WorkLogDAL();
            return objDal.GetWorkLogListByProjectFromTo(UserId, ProjectId, FromDate, ToDate);


        }
    }
}
