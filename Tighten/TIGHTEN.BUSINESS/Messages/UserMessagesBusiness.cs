﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    
    public class UserMessagesBusiness
    {
        DATA.UserMessagesData userMessages;

        /// <summary>
        /// get All user Messages
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public List<MODEL.UserMessagesModel> GetAllUserMessages()
        {
            userMessages = new DATA.UserMessagesData();
            return userMessages.GetAllUserMessages();
        }

        /// <summary>
        /// get User Messages by senderId and reciverId
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public List<MODEL.UserMessagesModel> GetUserMessages(string reciverId, string senderId)
        {
            userMessages = new DATA.UserMessagesData();
            return userMessages.GetUserMessages(reciverId, senderId);
        }

        /// <summary>
        /// Add or Update User Messages
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public void addUserMessages(MODEL.UserMessagesModel messageModel)
        {
            userMessages = new DATA.UserMessagesData();
            userMessages.AddUserMessages(messageModel);
        }

        /// <summary>
        /// Get and Update User Messages IsRead Status
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public List<MODEL.UserMessagesModel> getUpdateMessageIsReadStatus(MODEL.UserMessagesModel messageModel)
        {
            userMessages = new DATA.UserMessagesData();
            return userMessages.GetUpdateMessageIsReadStatus(messageModel);
        }

        /// <summary>
        /// Update User Messages IsRead Status
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public void updateMessageIsReadStatus(MODEL.UserMessagesModel messageModel)
        {
            userMessages = new DATA.UserMessagesData();
            userMessages.UpdateMessageIsReadStatus(messageModel.ReceiverId , messageModel.SenderId);
        }

        /// <summary>
        /// get sender User by UserId
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public List<MODEL.UserLeftDisplayModel> GetSenderUsers(string UserId)
        {
            userMessages = new DATA.UserMessagesData();
            return userMessages.GetSenderUsers(UserId);
        }

        /// <summary>
        /// get Messages Count
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public int GetMessagesCount(string UserId)
        {
            userMessages = new DATA.UserMessagesData();
            return userMessages.GetMessagesCount(UserId);
        }

        public List<MODEL.UserLeftDisplayModel> GetAllSenders(string SenderId,int CompanyId, string UId)
        {
            userMessages = new DATA.UserMessagesData();
            return userMessages.GetAllSenders(SenderId, CompanyId, UId);
        }

    }
}
