﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class UserOnlineBusiness
    {
        DATA.UserOnlineData userOnline;

        ///// <summary>
        ///// get online user by user id and dateDiff
        ///// </summary>
        ///// <param name="UserId"></param>
        ///// <returns></returns>
        //public List<TIGHTEN.ENTITY.UserOnline> getOnlineUser(string CompanyId)
        //{
        //    userOnline = new TIGHTEN.DATA.UserOnlineData();
        //    return userOnline.GetOnlineUser(CompanyId);
        //}

        /// <summary>
        /// get online user by user id and dateDiff
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public List<MODEL.UserOnlineModel> getAllOnlineUser()
        {
            userOnline = new TIGHTEN.DATA.UserOnlineData();
            return userOnline.GetAllOnlineUser();
        }


        /// <summary>
        /// Add or Update User online
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>

        public void addUpdateOnlineUser(String UserId)
        {
            userOnline = new DATA.UserOnlineData();
            userOnline.AddUpdateOnlineUser(UserId);
        }
    }
}
