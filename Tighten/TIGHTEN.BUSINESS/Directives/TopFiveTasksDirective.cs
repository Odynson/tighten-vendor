﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class TopFiveTasksDirective
    {
        TIGHTEN.DATA.TopFiveTasksDirective ObjDll;

        public List<MODEL.TopFiveTasksDirective> GetTopFiveTasks(string UserId, int CompanyId)
        {
            ObjDll = new TIGHTEN.DATA.TopFiveTasksDirective();
            return ObjDll.GetTopFiveTasks(UserId, CompanyId);
        }

    }
}
