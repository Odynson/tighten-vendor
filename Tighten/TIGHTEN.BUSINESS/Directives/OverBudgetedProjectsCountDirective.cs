﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class OverBudgetedProjectsCountDirective
    {

        TIGHTEN.DATA.OverBudgetedProjectsCountDirective Todo;

        /// <summary>
        /// It gets the All Projects
        /// </summary>
        /// <returns>It returns All the Projects </returns>
        public List<MODEL.OverBudgetedProjectsCountDirective> GetUserOverBudgetedProjects(string UserId, int CompanyId)
        {
            Todo = new TIGHTEN.DATA.OverBudgetedProjectsCountDirective();
            return Todo.GetUserOverBudgetedProjects(UserId, CompanyId);
        }



    }
}
