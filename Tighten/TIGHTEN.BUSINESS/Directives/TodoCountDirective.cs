﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class TodoCountDirective
    {
        TIGHTEN.DATA.TodoCountDirective Todo;

        /// <summary>
        /// It gets the All Completed Todos
        /// </summary>
        /// <returns>It returns All the Completed Todos </returns>
        public List<MODEL.TodoModel.TodosData> GetUserCompletedTodos(string UserId, int CompanyId)
        {
            Todo = new TIGHTEN.DATA.TodoCountDirective();
            return Todo.GetUserCompletedTodos(UserId, CompanyId);
        }



    }
}
