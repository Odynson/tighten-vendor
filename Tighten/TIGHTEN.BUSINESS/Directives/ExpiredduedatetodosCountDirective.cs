﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class ExpiredduedatetodosCountDirective
    {

        TIGHTEN.DATA.ExpiredduedatetodosCountDirective Todo;

        /// <summary>
        /// It gets the All Completed Todos
        /// </summary>
        /// <returns>It returns All the Completed Todos </returns>
        public List<MODEL.TodoModel.TodosData> GetUserExpiredduedatetodos(string UserId, int CompanyId)
        {
            Todo = new TIGHTEN.DATA.ExpiredduedatetodosCountDirective();
            return Todo.GetUserExpiredduedatetodos(UserId, CompanyId);
        }


    }
}
