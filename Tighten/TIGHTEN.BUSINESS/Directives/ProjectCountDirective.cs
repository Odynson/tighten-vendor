﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class ProjectCountDirective
    {
        TIGHTEN.DATA.ProjectCountDirective Project;

        /// <summary>
        /// It gets the All Projects
        /// </summary>
        /// <returns>It returns All the Projects </returns>
        public List<MODEL.ProjectModel.ProjectWithMembers> GetUserProjects(string UserId, int CompanyId)
        {
            Project = new TIGHTEN.DATA.ProjectCountDirective();
            return Project.GetUserProjects(UserId, CompanyId);
        }




    }
}
