﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class TeamCountDirective
    {
        TIGHTEN.DATA.TeamCountDirective team;


        /// <summary>
        /// This function fetches the Teams with team members for a particular Company
        /// </summary>
        /// <param name="companyId">It is the unique Id of Company</param>
        /// <returns>It returns Teams of a particular company with team members</returns>
        public List<MODEL.TeamModel.TeamWithMembers> getCompanyTeams(int CompanyId, string UserId)
        {
            team = new TIGHTEN.DATA.TeamCountDirective();
            return team.getCompanyTeams(CompanyId, UserId);
        }

    }
}
