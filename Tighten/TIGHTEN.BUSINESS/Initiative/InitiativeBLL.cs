﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA.Initiative;
using TIGHTEN.MODEL;
using TIGHTEN.MODEL.Initiative;

namespace TIGHTEN.BUSINESS.Initiative
{
    public class InitiativeBLL
    {
        InitiativeDAL objDal;
        public List<InitiativeModel> GetInitiativeList(int CompanyId)
        {
            objDal = new InitiativeDAL();
            return objDal.GetInitiativeList(CompanyId);


        }
        public List<initiativeVendorsModel> GetVendorProjectInitiativeList(int InitiativeId)
        {
            objDal = new InitiativeDAL();
            return objDal.GetVendorProjectInitiativeList(InitiativeId);


        }
        public int CheckInitiativeExist(int CompanyId, string InitiativeTitle, int InitiativeId)
        {
            int ResultId;
            objDal = new InitiativeDAL();
            return ResultId = objDal.CheckInitiativeExist(CompanyId, InitiativeTitle, InitiativeId);

        }
        public int SaveNewInitiative(InitiativeModel obj)
        {
            objDal = new InitiativeDAL();
            return objDal.SaveNewInitiative(obj);

        }

        public int DeleteVendorProjectInitiativeFromList(int Id)
        {
            objDal = new InitiativeDAL();
            return objDal.DeleteVendorProjectInitiativeFromList(Id);


        }
        public InitiativeModel GetInitiativeById(int InitiativeId)
        {
            objDal = new InitiativeDAL();
            return objDal.GetInitiativeById(InitiativeId);


        }
        public int DeleteInitiative(int Id)
        {
            objDal = new InitiativeDAL();
            return objDal.DeleteInitiative(Id);


        }

        public int EditInitiative(InitiativeModel obj)
        {
            objDal = new InitiativeDAL();
            return objDal.EditInitiative(obj);

        }
    }
}
