﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA.Vendor;
using TIGHTEN.MODEL.Vendor.VendorAdminModel;

namespace TIGHTEN.BUSINESS.Vendor
{
    public class UserManagementVendorAdminProfileBLL
    {
        UserManagementVendorAdminProfileDAL objDal;


        public UserManagementModel GetUserAndResourceVendorAdminProfile(int VendorCompanyId)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.GetUserAndResourceVendorAdminProfile(VendorCompanyId);

        }

        public List<RoleVendorAdminProfileModel> GetExistingUserRoleVendorAdminProfile(int VendorId)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.GetExistingUserRoleVendorAdminProfile(VendorId);

        }


        public ResourceStatisticsModel GetResourceVendorAdminProfile(ResourceRequestModel Model)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.GetResourceVendorAdminProfile(Model);

        }



        public int SaveExistingUserAdminProfile(SaveExistingUserModel Model)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.SaveExistingUserAdminProfile(Model);

        }

        public int InviteResourceAsVendor(SaveResourceAsVendorRequestModel Model, string url)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.InviteResourceAsVendor(Model, url);

        }

        public int ActiveInactiveUserVendorAdminProfile(SaveExistingUserModel Model)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.ActiveInactiveUserVendorAdminProfile(Model);

        }


 

        public List<ExistingUsersModel> GetUserForVendorAdminProfile(int VendorCompanyId, int PageIndex, int PageSizeSelected)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.GetUserForVendorAdminProfile(VendorCompanyId, PageIndex, PageSizeSelected);

        }

        public List<ResourceModel> GetResourcesForVendorAdminProfile(int VendorCompanyId, int PageIndex, int PageSizeSelected)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.GetResourcesForVendorAdminProfile(VendorCompanyId, PageIndex, PageSizeSelected);

        }
        #region invitenewuserlist
        public List<MODEL.Vendor.UserInvitationModal> GetAllExistingInternalUsers(int CompanyId, int PageIndex, int PageSizeSelected)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.GetAllExistingInternalUsers(CompanyId, PageIndex, PageSizeSelected);

        }
        public int ActiveInactiveInternalUsersVendorAdminProfile(MODEL.Vendor.UserInvitationModal Model)
        {
            objDal = new UserManagementVendorAdminProfileDAL();
            return objDal.ActiveInactiveInternalUsersVendorAdminProfile(Model);

        }

        #endregion
    }

}

