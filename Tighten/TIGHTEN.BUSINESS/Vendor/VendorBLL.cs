﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA.Vendor;
using TIGHTEN.MODEL;
using TIGHTEN.MODEL.Vendor;

namespace TIGHTEN.BUSINESS.Vendor
{
    public class VendorBLL
    {
        vendorDAL objDal;

        public List<vendorModel> GetCompanyVendorList(int CompanyId)
        {
            objDal = new vendorDAL();
           return  objDal.GetCompanyVendorList(CompanyId);


        }



         public VendorCompanyMail SaveVendor(VendorInvitationModal modal, string url)
        {
            objDal = new vendorDAL();
            return objDal.SaveVendor(modal,url);
        }

        public List<VendorInvitationsModal> GetInvitaionFromCompany(VendorInvitationsModal Modal)
        {
            objDal = new vendorDAL();
            return objDal.GetInvitaionFromCompany(Modal);



        }

        public VendorInvitationsModal AcceptRejectInvitaionAsVendor(VendorInvitationsModal modal)
        {
            objDal = new vendorDAL();
            return objDal.AcceptRejectInvitaionAsVendor(modal);



        }

        public List<vendorModel> GetMyVendor(VendorFilterModal model)
        {
            objDal = new vendorDAL();
            return objDal.GetMyVendor(model);



        }

        public ProjectModel.NewCreateProjectModel GetProjectAndVendorDetail(int ProjectId, int CompanyId)
        {
            objDal = new vendorDAL();
            return objDal.GetProjectAndVendorDetail(ProjectId, CompanyId);


        }
        public List<vendorModel> GetProjectTOQuote(vendorModel Modal)
        {
            objDal = new vendorDAL();
            return objDal.GetProjectTOQuote(Modal);


        }

        public VendorQuoteModal GetProjectDetailForVendor(RequestProjectDetailforVendorModal Modal)
        {
            objDal = new vendorDAL();
            return objDal.GetProjectDetailForVendor(Modal);


        }

        public List<UserRoleModel> GetVendorResourceRole()
        {
            objDal = new vendorDAL();
            return objDal.GetVendorResourceRole();


        }
        public int SaveQuoteAcceptByvendor(VendorQuoteModal modal)
        {

            objDal = new vendorDAL();
            return objDal.SaveQuoteAcceptByvendor(modal);
        }


        public int EditSaveQuoteAcceptByvendor(VendorQuoteModal modal)
        {

            objDal = new vendorDAL();
            return objDal.EditSaveQuoteAcceptByvendor(modal);
        }

        public int RejectQuoteByVendor(VendorInvitationsModal modal)
        {

            objDal = new vendorDAL();
            return objDal.RejectQuoteByVendor(modal);
        }
        public VendorQuoteModal GetAwardedProjectViewDetail(int ProjectId, int VendorId)
        {
            objDal = new vendorDAL();
            return objDal.GetAwardedProjectViewDetail( ProjectId, VendorId);


        }

        public InvoiceRaiseToProject GetSpecificProjectForInvoiceRaise(int ProjectId, int VendorId)
        {
            objDal = new vendorDAL();
            return objDal.GetSpecificProjectForInvoiceRaise(ProjectId, VendorId);


        }


        public List<InvoiceToMilestoneModal> GetMilestoneforProject(int Id)
        {
            objDal = new vendorDAL();
            return objDal.GetMilestoneforProject(Id);


        }

        public List<InvoiceRaiseToCompanyModal> ProjectForInvoiceDrop(int CompanyId)
        {
            objDal = new vendorDAL();
            return objDal.ProjectForInvoiceDrop(CompanyId);
        }


        public List<VendorFilterModal> GetCompanyVendorDropList(int CompanyId)
        {
            objDal = new vendorDAL();
            return objDal.GetCompanyVendorDropList(CompanyId);
        }
        public List<CompanyModal> GetCompanyForDrop(int CompanyId)
        {
            objDal = new vendorDAL();
            return objDal.GetCompanyForDrop(CompanyId);
        }
        public List<VendorFilterModal> GetVendorName(VendorFilterModal Modal)
        {
            objDal = new vendorDAL();
            return objDal.GetVendorName(Modal);
        }

        public List<VendorFilterModal> GetVendorCompanyName(VendorFilterModal Modal)
        {
            objDal = new vendorDAL();
            return objDal.GetVendorCompanyName(Modal);
        }

        public List<VendorDetailModal> GetVendorDetail(int CompanyId, int VendorId)
        {
            objDal = new vendorDAL();
            return objDal.GetVendorDetail(CompanyId,VendorId);
        }


        public List<VendorCompanyPocModal> GetVendorCompanyPoc(int VendorCompanyId)
        {
            objDal = new vendorDAL();
            return objDal.GetVendorCompanyPoc(VendorCompanyId);
        }

        public CompanyModel GetSelectedVendorDetail( int VendorId)
        {
            objDal = new vendorDAL();
            return objDal.GetSelectedVendorDetail( VendorId);
        }

        public string deleteVendorDetail(int Id)
        {
            objDal = new vendorDAL();
            return objDal.deleteVendorDetail(Id);
        }

        public CompanyModel GetSelectedVendorDetailById(int Id)
        {
            objDal = new vendorDAL();
            return objDal.GetSelectedVendorDetailById(Id);
        }

        public VendorCompanyMail UpdateVendor(VendorInvitationModal modal, string url)
        {
            objDal = new vendorDAL();
            return objDal.UpdateVendor(modal, url);
        }
        public bool ValidationEmailForPOC(string pocEmail, int CompanyId)
        {
            objDal = new vendorDAL();
            return objDal.ValidationEmailForPOC(pocEmail, CompanyId);
        }

        public List<CompanyModel> GetGeneralCompanyListOfVendor(int CompanyId)
        {
            objDal = new vendorDAL();
            return objDal.GetGeneralCompanyListOfVendor(CompanyId);


        }


        #region InviteNewUserFunctionality

        public List<UserRoleModel> GetNewUserRole(int CompanyId)
        {
            objDal = new vendorDAL();
            return objDal.GetNewUserRole(CompanyId);
        }

        public NewUserMail InviteUser(UserInvitationModal modal, string url)
        {
            objDal = new vendorDAL();
            return objDal.InviteUser(modal, url);
        }

        public List<UserRoleModel> GetAllNewUser(int CompanyId)
        {
            objDal = new vendorDAL();
            return objDal.GetAllNewUser(CompanyId);
        }

        public string GetCompanyEmailForInviteNewUser(string CompanyName)
        {
            objDal = new vendorDAL();
            return objDal.GetCompanyEmailForInviteNewUser(CompanyName);
        }

        public NewUserMail UpdateExistingInternalUsers(UserInvitationModal modal, string url)
        {
            objDal = new vendorDAL();
            return objDal.UpdateExistingInternalUsers(modal, url);
        }

        public List<CompanyModel> GetCompaniesForVenderCompanyName(string Name)
        {
            objDal = new vendorDAL();
            return objDal.GetCompaniesForVenderCompanyName(Name);
        }
        #endregion

    }
}
