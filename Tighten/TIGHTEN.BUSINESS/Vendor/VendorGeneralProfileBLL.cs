﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA.Vendor;
using TIGHTEN.MODEL.VendorGeneralProfile;

namespace TIGHTEN.BUSINESS.Vendor
{
    public class VendorGeneralProfileBLL
    {

        VendorGeneralProfileDAL ObjDAL;
        public List<ProjectVendorGeneralProfile> GetMyProject(FilterProjectListVendorGeneralProfile Model)
        {
            ObjDAL = new VendorGeneralProfileDAL();
            return ObjDAL.GetMyProject(Model);


        }


        public List<ProjectModel> GetProjectNameForFilter(FilterProjectListVendorGeneralProfile Model)
        {

            ObjDAL = new VendorGeneralProfileDAL();
            return ObjDAL.GetProjectNameForFilter(Model);


        }

        public List<WorkDiaryModel> GetWorkNoteForVendorGeneralUser(int ProjectId, string VendorGeneralUserId)
        {
            ObjDAL = new VendorGeneralProfileDAL();
            return ObjDAL.GetWorkNoteForVendorGeneralUser(ProjectId, VendorGeneralUserId);
        }

        
        public  int InsertNoteVendorGeneralUser(WorkDiaryModel Model)
        {

            ObjDAL = new VendorGeneralProfileDAL();
            return ObjDAL.InsertNoteVendorGeneralUser(Model);



        }

        public VendorGeneralProfProjectAwardedViewDetailModel GetMyProjectViewDetailVendorGeneralProfile(int ProjectId, string UserId)
        {

            ObjDAL = new VendorGeneralProfileDAL();
            return ObjDAL.GetMyProjectViewDetailVendorGeneralProfile(ProjectId, UserId);



        }

    }
}
