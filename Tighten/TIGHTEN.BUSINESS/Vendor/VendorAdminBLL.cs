﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA.Vendor;
using TIGHTEN.MODEL.Vendor;
using TIGHTEN.MODEL.Vendor.VendorAdminModel;
using TIGHTEN.MODEL.VendorAdminModal;

namespace TIGHTEN.BUSINESS.Vendor
{
    public class VendorAdminBLL
    {

        VendorAdminDAL objDal;

        public  List<AdminProfileVendorInvitationModel>  GetInvitationToVendor(FilterVendorInvitationModel Modal)
        {
             objDal = new VendorAdminDAL();
            return objDal.GetInvitationToVendor(Modal);

        }

        public List<CompanyModel> GetCompanyForInvitaionDropDown(int VendorCompanyId)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetCompanyForInvitaionDropDown(VendorCompanyId);

        }


        public List<VendorModel> GetPocsforDrops(int VendorCompanyId)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetPocsforDrops(VendorCompanyId);

        }

        public List<CompanyModel> GetCompanyForFilterVendorAdminProfile(int VendorCompanyId)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetCompanyForFilterVendorAdminProfile(VendorCompanyId);

        }

        public List<ProjectToQuotevendorModel> GetProjectToQuoteListVendorAdminProfile(ProjectToQuoteFilterModel Model)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetProjectToQuoteListVendorAdminProfile(Model);

        }


        public List<AutoCompleteProjectToQuoteModel> GetProjectByNameVendorAdminProfile(int VendorCompanyId, string ProjectName)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetProjectByNameVendorAdminProfile(VendorCompanyId,ProjectName);

        }


        public VendorQuoteViewDetailForAdminProfileModal GetProjectToQuoteViewDetailAdminProfile(ProjectToQuoteViewDetailRequestModel Model)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetProjectToQuoteViewDetailAdminProfile(Model);

        }
        public List<AWardedProjectAdminProfileModel> GetAwardedProjectListAdminProfile(AwardedProjectListRequestModel Model)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetAwardedProjectListAdminProfile(Model);

        }

        public ProjectAwardedViewDetailModel GetAwardedProjectViewDetailVendorAdminProfile(int ProjectId, int VendorId)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetAwardedProjectViewDetailVendorAdminProfile(ProjectId,VendorId);

        }

        public ProjectCompleteFeedBackModal GetProjectCompletionFeedBackVendorAdminProfile(int ProjectId, int VendorId)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetProjectCompletionFeedBackVendorAdminProfile(ProjectId, VendorId);

        }

        public List<InvoiceVendorAdminProfileModel> GetAllInvoiceListVendorAdminProfile(InvoiceFilterAdminProfileModel Model)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetAllInvoiceListVendorAdminProfile(Model);


        }


        public InvoicePreviewVendorAdminProfileModel InvoicePreviewVendorAdminProfile(int InvoiceId)
        {
            objDal = new VendorAdminDAL();
            return objDal.InvoicePreviewVendorAdminProfile(InvoiceId);


        }

        #region Paging

        public List<InvoiceVendorAdminProfileModel> GetAllInvoiceListVendorAdminProfileOutstanding(InvoiceFilterAdminProfileModel Model)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetAllInvoiceListVendorAdminProfileOutstanding(Model);


        }

        public List<InvoiceVendorAdminProfileModel> GetAllInvoiceListVendorAdminProfilePaid(InvoiceFilterAdminProfileModel Model)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetAllInvoiceListVendorAdminProfilePaid(Model);


        }

        public List<InvoiceVendorAdminProfileModel> GetAllInvoiceListVendorAdminProfileRejected(InvoiceFilterAdminProfileModel Model)
        {
            objDal = new VendorAdminDAL();
            return objDal.GetAllInvoiceListVendorAdminProfileRejected(Model);


        }
        #endregion


    }
}
