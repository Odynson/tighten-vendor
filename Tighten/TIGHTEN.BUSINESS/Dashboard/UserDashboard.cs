﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class UserDashboard
    {
        TIGHTEN.DATA.UserDashboard userDashboard;
        /// <summary>
        /// It gets the upcoming Todos of the user logged in. Whole deadline date is greater than present date
        /// </summary>
        /// <param name="UserId">It is the unique id of logged in user</param>
        /// <returns>Returns the list of Todos</returns>
        public List<MODEL.TodoModel.TodosData> getUserUpcomingTodos(string UserId)
        {
            userDashboard = new TIGHTEN.DATA.UserDashboard();
            return userDashboard.getUserUpcomingTodos(UserId);
        }
        /// It is a Function for getting Calendar Events for Logged In
        /// </summary>
        /// <param name="UserId">It is the unique id of user</param>
        /// <returns>It returns the Calendar Events for a particular User</returns>
        public List<MODEL.TodoModel.CalendarEvents> GetUserUpcomingEvents(string UserId)
        {
            userDashboard = new TIGHTEN.DATA.UserDashboard();
            return userDashboard.GetUserUpcomingEvents(UserId);
        }

        /// <summary>
        /// It gets the User T-Line with all fields
        /// </summary>
        /// <param name="UserId">It is the unique Id of User</param>
        /// <returns>It returns the T-Line Model</returns>
        public MODEL.T_LineModel GetUserTLine(string UserId)
        {
            userDashboard = new TIGHTEN.DATA.UserDashboard();
            return userDashboard.GetUserTLine(UserId);
        }


        /// <summary>
        /// It gets the All Packages
        /// </summary>
        /// <returns>It returns All the Packages </returns>
        public List<MODEL.PackageModel> GetAllPackages(int CompanyId)
        {
            userDashboard = new TIGHTEN.DATA.UserDashboard();
            return userDashboard.GetAllPackages(CompanyId);
        }


        /// <summary>
        /// Upgrading Packages
        /// </summary>
        /// <returns>It returns success msg </returns>
        public string UpgradePackage(MODEL.SubscriptionModel model)
        {
            userDashboard = new TIGHTEN.DATA.UserDashboard();
            return userDashboard.UpgradePackage(model);
        }


    }
}
