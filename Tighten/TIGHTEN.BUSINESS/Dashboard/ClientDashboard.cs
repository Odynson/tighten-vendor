﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class ClientDashboard
    {
        TIGHTEN.DATA.ClientDashboard clientDashboard;
        /// <summary>
        /// It gets the upcoming Todos for the Client
        /// </summary>
        /// <param name="UserId">It is the unique id of logged in user</param>
        /// <returns>Returns the list of Todos</returns>
        public List<MODEL.TodoModel.TodosData> getClientUpcomingTodos(string UserId)
        {
            clientDashboard = new TIGHTEN.DATA.ClientDashboard();
            return clientDashboard.getClientUpcomingTodos(UserId);
        }

        /// <summary>
        /// Getting projects in which Client is Involved
        /// </summary>
        /// <param name="userId">It is the unique id of Client</param>
        /// <returns>Returns the Client Projects</returns>
        public List<MODEL.ProjectModel.ProjectWithMembers> getClientProjects(String UserId)
        {
            clientDashboard = new TIGHTEN.DATA.ClientDashboard();
            return clientDashboard.getClientProjects(UserId);
        }
        /// <summary>
        /// Get Users of those projects in which Client Is Involved
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public List<MODEL.UserModel> getConcernedUsersOfClient(String UserId)
        {
            clientDashboard = new TIGHTEN.DATA.ClientDashboard();
            return clientDashboard.getConcernedUsersOfClient(UserId);
        }
    }
}
