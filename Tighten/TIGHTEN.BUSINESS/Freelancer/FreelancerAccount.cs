﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS 
{
    public class FreelancerAccount
    {
        DATA.FreelancerAccount objDLL;
        public MODEL.FreelancerInvitationModel getFreelancerInfoForRegistration(string key)
        {
            objDLL = new DATA.FreelancerAccount();
            return objDLL.getFreelancerInfoForRegistration(key);
        }


        public string RegisterFreelancer(MODEL.RegisterBindingModel model, MODEL.FreelancerInvitationModel freelancerInfo,string pictureUrl)
        {
            objDLL = new DATA.FreelancerAccount();
            return objDLL.RegisterFreelancer(model, freelancerInfo, pictureUrl);
        }

        public List<MODEL.FreelancerInfoToConfirmLink> getFreelancerInfo(string UserName,int CompanyId)
        {
            objDLL = new DATA.FreelancerAccount();
            return objDLL.getFreelancerInfo(UserName, CompanyId);
        }

        public List<MODEL.FreelancerInfoToConfirmLink> GetAllFreelancer()
        {
            objDLL = new DATA.FreelancerAccount();
            return objDLL.GetAllFreelancer();
        }

        public List<MODEL.CompaniesForFreelancer> getCompaniesForFreelancer(string UserId)
        {
            objDLL = new DATA.FreelancerAccount();
            return objDLL.getCompaniesForFreelancer(UserId);
        }

        public MODEL.FreelancerModel GetFreelancerRoleForCompany(string UserId, int CompanyId)
        {
            objDLL = new DATA.FreelancerAccount();
            return objDLL.GetFreelancerRoleForCompany(UserId, CompanyId);
        }

        public MODEL.confirmEmailOrHandleModel confirmEmailOrHandle(MODEL.FreelancerInfoToSendInvitationModel model)
        {
            objDLL = new DATA.FreelancerAccount();
            return objDLL.confirmEmailOrHandle(model);
        }

        public MODEL.FreelancerForProjectModel GetAllFreelancerInProject(int ProjectId)
        {
            objDLL = new DATA.FreelancerAccount();
            return objDLL.GetAllFreelancerInProject(ProjectId);
        }



    }
}
