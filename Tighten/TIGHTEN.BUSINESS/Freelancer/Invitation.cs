﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS 
{
    public class Invitation
    {
        DATA.Invitation objDll;
        public List<MODEL.InvitationNotificationModal> GetInvitationNotification(string UserId,string InvitaitonStatus)
        {
            objDll = new DATA.Invitation();
            return objDll.GetInvitationNotification(UserId, InvitaitonStatus);
        }

        public string AcceptInvitation(MODEL.InvitationNotificationModal model)
        {
            objDll = new DATA.Invitation();
            return objDll.AcceptInvitation(model);
        }


        public string RejectInvitation(MODEL.InvitationNotificationModal model)
        {
            objDll = new DATA.Invitation();
            return objDll.RejectInvitation(model);
        }

        public List<MODEL.InvitationMessagesModal> GetUserMessage(int FreelancerInvitationId, string UserId)
        {
            objDll = new DATA.Invitation();
            return objDll.GetUserMessage(FreelancerInvitationId, UserId);
        }

        public string SendMessgae(MODEL.InvitationMessagesModal model,string url)
        {
            objDll = new DATA.Invitation();
            return objDll.SendMessgae(model,url);
        }

        public MODEL.InvitationFreelancerInfoModal GetFreelancerInfo(int InvitationId)
        {
            objDll = new DATA.Invitation();
            return objDll.GetFreelancerInfo(InvitationId);
        }




    }
}
