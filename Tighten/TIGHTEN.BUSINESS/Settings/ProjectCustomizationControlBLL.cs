﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA;
using TIGHTEN.MODEL.Settings;

namespace TIGHTEN.BUSINESS
{

    public class ProjectCustomizationControlBLL
    {

        ProjectCustomizationControlDAL ObjDLL;
                
        public int InsertCustomControl(CustomControlModel Model)
        {
    
            ObjDLL = new DATA.ProjectCustomizationControlDAL();
            return ObjDLL.InsertCustomControl(Model);

        }


        public List<CustomControlModel> GetCustomControl(int CompanyId)
        {
             ObjDLL = new DATA.ProjectCustomizationControlDAL();
            return ObjDLL.GetCustomControl(CompanyId);


        }

        public int DeleteCustomControlOrActive(CustomControlDelete Model)
        {
            ObjDLL = new DATA.ProjectCustomizationControlDAL();
            return ObjDLL.DeleteCustomControlOrActive(Model);


        }

        public int UpdatePriority(MODEL.updatePriorityModel model)
        {

            ObjDLL = new DATA.ProjectCustomizationControlDAL();

            return ObjDLL.UpdatePriority(model);

        }



    }
}
