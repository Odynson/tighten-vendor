﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class AccountSettings
    {
        DATA.AccountSettings objDll;

        public MODEL.AccountSettingsModel GetAccountSettings(string UserId)
        {
            objDll = new DATA.AccountSettings();
            return objDll.GetAccountSettings(UserId);
        }

        public string UpdateAccountSettings(MODEL.AccountSettingsModel model)
        {
            objDll = new DATA.AccountSettings();
            return objDll.UpdateAccountSettings(model);
        }

        public void UpdateUserRoutingNumber(MODEL.AccountSettingsModel model)
        {
            objDll = new DATA.AccountSettings();
            objDll.UpdateUserRoutingNumber(model);
        }


        public string UpdateUserRoutingNumberVendorAdminProfile(MODEL.AccountSettingsModel model)
        {
            objDll = new DATA.AccountSettings();
              return   objDll.UpdateUserRoutingNumberVendorAdminProfile(model);
        }

        public string UpdateUserRoutingNumberVendorPOCsProfile(MODEL.AccountSettingsModel model)
        {
            objDll = new DATA.AccountSettings();
            return objDll.UpdateUserRoutingNumberVendorPOCsProfile(model);
        }

    }
}
