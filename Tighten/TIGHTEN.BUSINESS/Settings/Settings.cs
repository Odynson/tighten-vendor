﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA;

namespace TIGHTEN.BUSINESS 
{
    public class Settings
    {
        TIGHTEN.DATA.SettingsDAL objDll;

        public List<MODEL.GetUserNotificationConfigModel> GetAllEmailSettingsOption(string UserId, int CompanyId, int ProjectId,int RoleId)
        {
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.GetAllEmailSettingsOption(UserId, CompanyId, ProjectId, RoleId);
        }


        public string ChangeEmailSettingsOption(MODEL.GetUserNotificationConfigModel model)
        {
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.ChangeEmailSettingsOption(model);
        }


        public List<MODEL.GetProjectsForCurrentCompanyModel> GetProjectsForCurrentCompany(string UserId, int CompanyId)
        {
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.GetProjectsForCurrentCompany(UserId, CompanyId);
        }


        public MODEL.EmailSettingsModelForProjectLevel GetEmailSettingsForProjectLevel(string UserId, int CompanyId)
        {
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.GetEmailSettingsForProjectLevel(UserId, CompanyId);
        }


        public MODEL.EmailSettingsModelForProjectLevel ChangeAllProjectSetting(MODEL.ChangeSettingsModel model)
        {
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.ChangeAllProjectSetting(model);
        }



        public MODEL.EmailSettingsModelForTodoLevel GetEmailSettingsForTodoLevel(string UserId, int CompanyId)
        {
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.GetEmailSettingsForTodoLevel(UserId, CompanyId);
        }


        public MODEL.EmailSettingsModelForTodoLevel ChangeAllTodoSetting(MODEL.ChangeSettingsModel model)
        {
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.ChangeAllTodoSetting(model);
        }


        public string ChangeProjectSetting(MODEL.EmailSettingsModelForProjectLevel model)
        {
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.ChangeProjectSetting(model);
        }


        public string changeTodoSetting(MODEL.EmailSettingsModelForTodoLevel model)
        {
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.changeTodoSetting(model);
        }


        public MODEL.VendorMainSettingsModel ChangeVendorSetting(int CompanyId, bool IsVendor)
        {
     
            objDll = new TIGHTEN.DATA.SettingsDAL();
            return objDll.ChangeVendorSetting(CompanyId, IsVendor);





        }

        public MODEL.VendorMainSettingsModel GetVendorSetting(string UserId,int CompanyId)
        {

            objDll = new TIGHTEN.DATA.SettingsDAL();

            return objDll.GetVendorSetting(UserId, CompanyId);

        }

        public MODEL.VendorMainSettingsModel SaveUpdateVendorWorkingArea(MODEL.VendorMainSettingsModel model)
        {

            objDll = new TIGHTEN.DATA.SettingsDAL();

            return objDll.SaveUpdateVendorWorkingArea(model);

        }

        public MODEL.VendorMainSettingsModel GetVendorSelectedAreas(int CompanyId)
        {

            objDll = new TIGHTEN.DATA.SettingsDAL();

            return objDll.GetVendorSelectedAreas( CompanyId);

        }

    


    }
}
