﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA.CompanyAccount;
using TIGHTEN.MODEL;
using TIGHTEN.MODEL.CompanyAccount;
namespace TIGHTEN.BUSINESS.CompanyAccount
{
    public class CompanyAccountBLL
    {
        CompanyAccountDAL objDal;
        public List<AccountModel> GetCompanyAccountList(int CompanyId)
        {
            objDal = new CompanyAccountDAL();
            return objDal.GetCompanyAccountList(CompanyId);


        }
        public List<CompanyModel> GetVendorList(int CompanyId)
        {
            objDal = new CompanyAccountDAL();
            return objDal.GetVendorList(CompanyId);


        }
        public List<TIGHTEN.MODEL.Vendor.VendorPOCModel> GetVendorPOCList(int CompanyId, int VendorId)
        {
            objDal = new CompanyAccountDAL();
            return objDal.GetVendorPOCList(CompanyId,VendorId);


        }
        public string SaveAccountDetail(AccountModel obj,string url)
        {
            objDal = new CompanyAccountDAL();
            return objDal.SaveAccountDetail(obj,url);
        }
        public string saveAccountChildDetail(AccountModel obj,string msg)
        {
            objDal = new CompanyAccountDAL();
            return objDal.saveAccountChildDetail(obj,msg);
        }
        public int CheckAccountExist(int CompanyId, int TeamId, int VendorId)
        {
            int ResultId;
            objDal = new CompanyAccountDAL();
            return ResultId = objDal.CheckAccountExist(CompanyId, TeamId, VendorId);

        }
        public AccountModel GetCompanyAccount(int Id)
        {
            objDal = new CompanyAccountDAL();
            return objDal.GetCompanyAccount(Id);


        }

        public string deleteCompanyAccountDetail(int Id)
        {
            objDal = new CompanyAccountDAL();
            return objDal.deleteCompanyAccountDetail(Id);
        }
        public string updateCompanyAccountDetail(AccountModel saveObj,string url)
        {
            objDal = new CompanyAccountDAL();
            return objDal.updateCompanyAccountDetail(saveObj,url);
        }
        public AccountDocumentsModel getAccountDocDetail(int attachementId)
        {
            objDal = new CompanyAccountDAL();
            return objDal.getAccountDocDetail(attachementId);
        }
        public List<AccountModel> getAccountDetailByVendorId(int VendorId)
        {
            objDal = new CompanyAccountDAL();
            return objDal.getAccountDetailByVendorId(VendorId);
        }
        public List<AccountModel> getAccountDetailByCompanyId(int CompanyId,int VendorId)
        {
            objDal = new CompanyAccountDAL();
            return objDal.getAccountDetailByCompanyId(CompanyId,VendorId);
        }
        public List<AccountModel> getAccountDetailById(int Id)
        {
            objDal = new CompanyAccountDAL();
            return objDal.getAccountDetailById(Id);
        }
        public List<AccountModel> GetMyEnterpriseByCompanyName(AccountModel model)
        {
            objDal = new CompanyAccountDAL();
            return objDal.GetMyEnterpriseByCompanyName(model);
        }
        public List<AccountModel> GetCompanyAccByCompanyId(int companyId)
        {
            objDal = new CompanyAccountDAL();
            return objDal.GetCompanyAccByCompanyId(companyId);
        }
    }
}
