﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS 
{
    public class PaymentHistory
    {
        DATA.PaymentHistory objDll;

        public List<MODEL.PaymentHistoryModel> GetPaymentHistory(string UserId, bool isFreelancer, int TeamId, int CompanyId,string subscriptionType, int ProjectId, string FreelancerUserId)
        {
            objDll = new DATA.PaymentHistory();
            return objDll.GetPaymentHistory(UserId, isFreelancer, TeamId, CompanyId, subscriptionType, ProjectId, FreelancerUserId);
        }


        public List<MODEL.ProjectModel.AllFreelancersForCompanyModel> GetFreelancersForSelectedProject(int ProjectId, int CompanyId)
        {
            objDll = new DATA.PaymentHistory();
            return objDll.GetFreelancersForSelectedProject( ProjectId,  CompanyId);
        }



    }
}
