﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class PaymentDetail
    {
        DATA.PaymentDetail objDll;

        public List<MODEL.PaymentDetailModel> GetPaymentDetail(int CompanyId)
        {
            objDll = new DATA.PaymentDetail();
            return objDll.GetPaymentDetail(CompanyId);
        }


        public string insertPaymentDetail(MODEL.PaymentDetailModel model)
        {
            objDll = new DATA.PaymentDetail();
            return objDll.insertPaymentDetail(model);
        }


        public int updatePaymentDetail(MODEL.PaymentDetailModel model)
        {
            objDll = new DATA.PaymentDetail();
            return objDll.updatePaymentDetail(model);
        }


        public void deletePaymentDetail(int Id)
        {
            objDll = new DATA.PaymentDetail();
            objDll.deletePaymentDetail(Id);
        }


    }
}
