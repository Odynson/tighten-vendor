﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS 
{
    public class RolePermission
    {
        DATA.RolePermission objDll;


        public MODEL.RolePermissionModel.InternalPermissionsForCurrentUserModel GetInternalPermissionsForCurrentUser(int RoleId, int CompanyId)
        {
            objDll = new DATA.RolePermission();
            return objDll.GetInternalPermissionsForCurrentUser(RoleId, CompanyId);
        }



        public List<MODEL.RolePermissionModel.AllRolesForCompanyModel> GetAllRolesForCompany(int CompanyId)
        {
            objDll = new DATA.RolePermission();
            return objDll.GetAllRolesForCompany(CompanyId);
        }


        public List<MODEL.RolePermissionModel.PermissionForSelectedRole> GetPermissionForSelectedRole(int RoleId,int CompanyId)
        {
            objDll = new DATA.RolePermission();
            return objDll.GetPermissionForSelectedRole(RoleId, CompanyId);
        }


        public string savePermissionsForSelectedRole(MODEL.RolePermissionModel.SavePermissionForSelectedRole model)
        {
            objDll = new DATA.RolePermission();
            return objDll.savePermissionsForSelectedRole(model);
        }


        public List<MODEL.RolePermissionModel.AllInternalPermissionsModel> GetAllInternalPermissionsForPermission(int MenuId, int RoleId, int CompanyId)
        {
            objDll = new DATA.RolePermission();
            return objDll.GetAllInternalPermissionsForPermission(MenuId, RoleId, CompanyId);
        }


        public string SaveInternalPermissions(MODEL.RolePermissionModel.SaveInternalPermissionsModel model)
        {
            objDll = new DATA.RolePermission();
            return objDll.SaveInternalPermissions(model);
        }



        

    }
}
