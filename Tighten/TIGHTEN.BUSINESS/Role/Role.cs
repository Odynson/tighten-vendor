﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.BUSINESS
{
    public class Role
    {
        DATA.Role userApi;
        public List<MODEL.RoleModel> getRoles(int CompanyId, int RoleId)
        {
            userApi = new DATA.Role();
            return userApi.getRoles(CompanyId, RoleId);
        }

        public MODEL.SideMenuOptionRolewiseModel showSideMenuOptionRolewise(int RoleId,string UserId,int CompanyId)
        {
            userApi = new DATA.Role();
            return userApi.showSideMenuOptionRolewise(RoleId, UserId, CompanyId);
        }

        //public MODEL.SideMenuOptionRolewiseModel showSideMenuForVendorCompany(int RoleId, string UserId, int CompanyId)
        //{
        //    userApi = new DATA.Role();
        //    return userApi.showSideMenuForVendorCompany(RoleId, UserId, CompanyId);
        //}


    }
}
