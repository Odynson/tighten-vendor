﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.DATA;
using TIGHTEN.MODEL.RoleManageVendorAdminProfile;


namespace TIGHTEN.BUSINESS
{
  public  class RoleManageVendorAdminProfileBLL
    {
        RoleManageVendorAdminProfileDLL objDll;

        public List<RoleManageVendorAdminProfileModel> GetAllRolesForVendorAdminProfile(int VendorCompanyId)
        {
            objDll = new  RoleManageVendorAdminProfileDLL();

            return objDll.GetAllRolesForVendorAdminProfile(VendorCompanyId);

        }



        /// <summary>
        /// Saving New Role
        /// </summary>
        public string saveRole(MODEL.RoleManageVendorAdminProfile.RoleAddUpdateModel obj)
        {
            objDll = new DATA.RoleManageVendorAdminProfileDLL();
            return objDll.saveRole(obj);
        }


        public string updateRole(MODEL.RoleManageVendorAdminProfile.RoleAddUpdateModel obj)
        {
            objDll = new DATA.RoleManageVendorAdminProfileDLL();
            return objDll.updateRole(obj);
        }


        public string DeleteRole(int RoleId)
        {
            objDll = new DATA.RoleManageVendorAdminProfileDLL();
            return objDll.DeleteRole(RoleId);
        }

    }
}
