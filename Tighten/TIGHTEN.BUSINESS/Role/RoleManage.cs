﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;

namespace TIGHTEN.BUSINESS
{
    public class RoleManage
    {
        DATA.RoleManage objDll;


        public List<MODEL.RoleManageModel.AllRolesForCompany> GetAllRolesForCompany(int CompanyId)
        {
            objDll = new DATA.RoleManage();
            return objDll.GetAllRolesForCompany(CompanyId);
        }


        /// <summary>
        /// Saving New Role
        /// </summary>
        public string saveRole(RoleManageModel.RoleAddUpdateModel obj)
        {
            objDll = new DATA.RoleManage();
            return objDll.saveRole(obj);
        }


        /// <summary>
        /// This function updates the existing Team
        /// </summary>
        ///
        public string updateRole(RoleManageModel.RoleAddUpdateModel obj)
        {
            objDll = new DATA.RoleManage();
            return objDll.updateRole(obj);
        }


        public string deleteRole(int RoleId, int CompanyId)
        {
            objDll = new DATA.RoleManage();
            return objDll.deleteRole(RoleId, CompanyId);
        }

        public string activeInactiveRoles(int RoleId, int CompanyId)
        {
            objDll = new DATA.RoleManage();
            return objDll.activeInactiveRoles(RoleId, CompanyId);
        }

    }
}
