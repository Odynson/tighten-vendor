﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
namespace TIGHTEN.API
{
    public class SectionsApiController : ApiController
    {
        /// <summary>
        /// Declaration on SectionBusiness Logic
        /// </summary>
        Sections objSec;


        /// <summary>
        /// It fetches the Sections for a particular Project
        /// </summary>
        /// <param name="id">It is the unique project id</param>
        /// <returns>it returns the sections in an object</returns>
        public JsonResponse Get(int id)
        {
            try
            {
                objSec = new Sections();
                return ControllerExtensions.JsonCustomSuccess(objSec.GetSectionForParticularProject(id));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/SectionsAPI/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        // GET api/<controller>/5
        [HttpGet]
        [Route("api/SectionsAPI/GetSections/{id}")]
        public HttpResponseMessage GetSections(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, objSec.GetUpdatableSections(id));
        }


        [HttpPut]
        [Route("api/SectionsAPI/UpdateSectionTodos")]
        public JsonResponse UpdateSectionTodos(List<TodoModel.TodosData> obj)
        {
            try
            {
                objSec = new Sections();
                objSec.UpdateTodosListOrderOrSectionInSection(obj);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/SectionsAPI/UpdateSectionTodos => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }
            return ControllerExtensions.JsonSuccess();
        }

        [HttpPut]
        [Route("api/SectionsAPI/UpdateSectionOrder")]
        public JsonResponse UpdateSectionOrder(List<TodoModel.Sections> obj)
        {
            try
            {
                objSec = new Sections();
                objSec.UpdateSectionOrder(obj);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/SectionsAPI/UpdateSectionOrder => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }
            return ControllerExtensions.JsonSuccess();
        }

        // POST api/<controller>
        public JsonResponse Post(SectionModel.NewSectionAddModel obj)
        {
            string SectionId;
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objSec = new Sections();
                SectionId = Convert.ToString(objSec.SaveSection(obj.UserId, obj.SectionAddModel));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/SectionsAPI/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess(SectionId);
        }

        // PUT api/<controller>/5
        public JsonResponse Put(SectionModel.NewSectionUpdateModel obj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objSec = new Sections();
                return ControllerExtensions.JsonCustomSuccess(objSec.UpdateSection(obj.UserId, obj.SectionUpdateModel));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/SectionsAPI/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        // DELETE api/<controller>/5
        public JsonResponse Delete(int id)
        {
            try
            {
                objSec = new Sections();
                string msg = objSec.DeleteSection(id);
                if (msg == string.Empty)
                {
                    return ControllerExtensions.JsonError("Atleast one section is required !!");
                }
                else if (msg == "Can not delete !! Todos are still there in this section")
                {
                    return ControllerExtensions.JsonError("Can't delete !! Todos are still there in this section");
                }
                else
                {
                    return ControllerExtensions.JsonSuccess("Section Deleted Successfully !");
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/SectionsAPI/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

           

        }
    }
}