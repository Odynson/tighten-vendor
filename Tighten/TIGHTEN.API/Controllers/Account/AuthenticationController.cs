﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
    [EnableCors("*", "*", "*")]
    public class AuthenticationController : ApiController
    {
        [EnableCors("*", "*", "*")]
        [Route("api/authenticate")]
        [HttpPost]
        public JsonResponse Authenticate(MODEL.Login viewModel)
        {

            TIGHTEN.BUSINESS.Account account = new BUSINESS.Account();
            JsonResponse response = new JsonResponse();
            try
            {
                string ipAddress = HttpContext.Current.Request.UserHostAddress;
                string userAgent = Request.Headers.UserAgent.ToString();
                UserModel returnModel = account.Login(viewModel, ipAddress, userAgent);
                if (returnModel.isAccountActive)
                {
                    response.ResponseData = returnModel;
                    response.message = "Success";
                    response.success = true;
                }

                else if (!returnModel.IsActive)
                {
                    response.ResponseData = returnModel;
                    response.message = "Success";
                    response.success = true;
                    response.message = returnModel.AboutMe = "userNotActive";
                }


                else
                {
                    response.success = true;
                    response.message = returnModel.AboutMe == "1" ? "wrongPassword" : "userNotExist";

                }


                response.success = true;
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/authenticate => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                response.success = false;
                response.message = CustomException;// "Some error occured. Please try after some time";

            }
            return response;
        }



        [Route("api/Account/ChangePassword")]
        [HttpPost]
        public JsonResponse ChangePassword(NewChangePasswordModel model)
        {

            TIGHTEN.BUSINESS.Account account = new BUSINESS.Account();
            JsonResponse response = new JsonResponse();
            try
            {
                bool returnResult = account.ChangePassword(model.UserId, model.Model);
                if (returnResult)
                {
                    response.ResponseData = returnResult;
                    response.message = "Password changed sucessfully";
                    response.success = true;
                }
                else
                {
                    response.success = false;
                    response.message = " Your current password is wrong";
                }

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/ChangePassword => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                response.success = false;
                response.message = "Some Error occured";

            }
            return response;
        }
    }
}
