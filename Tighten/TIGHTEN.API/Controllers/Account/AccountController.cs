﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;
using TIGHTEN.MODEL;
using System.Web.Http.Routing;
using System.Configuration;
using System.Web.Http.Cors;
using System.Threading;
using System.Xml;
using System.IO;
using System.Web;
using System.Xml.Linq;

namespace TIGHTEN.API
{
    [EnableCors("*", "*", "*")]
    public class AccountController : ApiController
    {
        #region Local variables
        Account AccountBAL = null;
        #endregion

        #region Registration

        /// <summary>
        /// Registration Form
        /// </summary>
        /// <returns>Registration View</returns>

        [HttpPost]
        public JsonResponse Registration(MODEL.RegisterBindingModel model)
        {
            JsonResponse result = new JsonResponse();
            try
            {
                string Token = ConfigurationManager.AppSettings["RegistrationToken"].ToString();
                if (model.Token == Token)
                {
                    AccountBAL = new Account();
                    UserModel obj = AccountBAL.Register(model);
                    if (obj.userId == string.Empty)
                    {
                        result.success = true;
                        result.message = "This email address is in use, Please use a different email.";
                        result.errors = true;
                    }
                    else
                    {
                        //await sendMainAccountRegistrationCreationConfirmationEmail(obj.email, obj.userId, obj.EmailConfirmationCode);

                        HttpContext ctx = HttpContext.Current;
                        Thread childref = new Thread(new ThreadStart(() =>
                        {
                            HttpContext.Current = ctx;
                            sendMainAccountRegistrationCreationConfirmationEmail(obj.email, obj.userId, obj.EmailConfirmationCode, obj.firstName);
                        }
                        ));
                        childref.Start();


                        result.success = true;
                        result.message = "You have registered Successfully !! Please confirm your email address.";
                        result.errors = false;
                    }
                }
                else
                {
                    result.success = true;
                    result.message = "You are not allowed. Please contact the site administrator";
                    result.errors = true;
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.message = "Some error occured. Please try after some time";

                string CustomException = "Path : api/Account/Registration => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
            }
            return result;
        }

        [Route("api/Account/chkUserNameExist")]
        [HttpPost]
        public JsonResponse chkUserNameExist(FreelancerInfoForRegistration model)
        {
            JsonResponse result = new JsonResponse();
            try
            {
                AccountBAL = new Account();
                result.success = AccountBAL.chkUserNameExist(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/chkUserNameExist => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
            return result;
        }

        #endregion

        #region Registration AccountVerification

        /// <summary>
        ///Main Account Verification View
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="code"></param>
        /// <returns>Redirects to SetPassword screen on successful email confirmation</returns>
        [Route("api/Account/confirmEmail/{userId}/{code}")]
        [HttpGet]
        public JsonResponse ConfirmEmail(string userId, string code)
        {
            JsonResponse result = new JsonResponse();
            try
            {
                if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(code))
                {
                    AccountBAL = new Account();
                    UserModel obj = AccountBAL.ConfirmRegistration(userId, code);
                    if (!string.IsNullOrEmpty(obj.userId))
                    {
                        result.success = true;
                        result.message = "EmailConfirmed";
                        result.ResponseData = obj;
                    }
                    else
                    {
                        result.success = true;
                        result.message = "EmailConfirmationError";
                    }
                }
                else
                {
                    result.success = true;
                    result.message = "EmailConfirmationErrorNotAllowed";
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/confirmEmail => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                result.success = false;
                result.message = "error";
            }
            return result;
        }

        #endregion

        [Route("api/Account/ResetPassword")]
        [HttpPost]
        public JsonResponse ResetPassword(SetPasswordBindingModel model)
        {
            JsonResponse result = new JsonResponse();
            try
            {
                if (!string.IsNullOrEmpty(model.UserId) && !string.IsNullOrEmpty(model.Code))
                {
                    AccountBAL = new Account();
                    bool isConfirmed = AccountBAL.setPassword(model);
                    if (isConfirmed)
                    {
                        result.success = true;
                        result.message = "Thanks!! Your password changed sucessfully. Please goto login";
                    }
                    else
                    {
                        result.success = true;
                        result.message = "Link Expired. Please contact the site administrator.";
                    }
                }
                else
                {
                    result.success = true;
                    result.message = "This operation is not allowed. Please contact the site administrator";
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/ResetPassword => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                result.success = false;
                result.message = "Some error occured. Please try after some time";
            }
            return result;
        }


        [HttpPost]
        [Route("api/Account/SetPasswordForLinkedInUser")]
        public JsonResponse SetPasswordForLinkedInUser(SetPasswordBindingModel model)
        {
            JsonResponse result = new JsonResponse();
            try
            {
                if (!string.IsNullOrEmpty(model.UserId) && !string.IsNullOrEmpty(model.NewPassword))
                {
                    AccountBAL = new Account();
                    bool isConfirmed = AccountBAL.SetPasswordForLinkedInUser(model);
                    if (isConfirmed)
                    {
                        result.success = true;
                        result.message = "Thanks!! Your password is set sucessfully.";
                    }
                    else
                    {
                        result.success = true;
                        result.message = "Please try again after some time";
                    }
                }
                else
                {
                    result.success = true;
                    result.message = "This operation is not allowed. Please contact the site administrator";
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/SetPasswordForLinkedInUser => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                result.success = false;
                result.message = "Some error occured. Please try after some time";
            }
            return result;
        }


        [Route("api/Account/chkPasswordLinkExpiry")]
        [HttpPost]
        public JsonResponse chkPasswordLinkExpiry(SetPasswordBindingModel model)
        {
            JsonResponse result = new JsonResponse();

            try
            {
                if (!string.IsNullOrEmpty(model.UserId) && !string.IsNullOrEmpty(model.Code))
                {
                    AccountBAL = new Account();
                    bool isConfirmed = AccountBAL.chkPasswordLinkExpiry(model);
                    if (isConfirmed)
                    {
                        result.success = true;
                    }
                    else
                    {
                        result.success = false;
                        result.message = "Link Expired. Please contact the site administrator.";
                    }
                }
                else
                {
                    result.success = false;
                    result.message = "This operation is not allowed. Please contact the site administrator";
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/chkPasswordLinkExpiry => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                result.success = false;
                result.message = "error";
            }

            return result;
        }


        [Route("api/Account/ForgotPassword")]
        [HttpPost]
        public async Task<JsonResponse> ForgotPassword(MODEL.Login viewModel)
        {
            JsonResponse result = new JsonResponse();
            try
            {
                AccountBAL = new Account();
                UserModel model = AccountBAL.ForgotPassword(viewModel.Email);

                if (!string.IsNullOrEmpty(model.userId))
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        sendForgotPasswordEmail(viewModel.Email, model.userId, model.EmailConfirmationCode, model.firstName);
                    }
                    ));

                    childref.Start();
                    result.success = true;
                    result.message = "resetPassword";
                }
                else
                {
                    result.success = false;
                    result.message = "wrongEmailForResetPassword";
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/ForgotPassword => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                result.success = false;
                result.message = "error";
            }
            return result;
            //  return ControllerExtensions.JsonSuccess("An Email has been sent! The email will give you instructions to reset your password.");
        }


        public void sendMainAccountRegistrationCreationConfirmationEmail(string destinationEmail, string userId, string confirmEmailtoken, string FirstName)
        {
            var url = ConfigurationManager.AppSettings["WebURL"].ToString();
            var callbackUrl = url + "#/confirmemail/" + userId + "/" + confirmEmailtoken + "/0";

            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "NewUserEmailConfirmation")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var item in MailModule)
            {
                emailbody = item.mailbody;
                Subject = item.subject;
            }

            emailbody = emailbody.Replace("@@callbackUrl", callbackUrl).Replace("@@PersonName", FirstName);

            //String emailbody = "<p>You Have been registered as a new User</p>";
            //emailbody += "<p>Please confirm your account by clicking this link </p>";
            //emailbody += "<a href=\"" + callbackUrl + "\">link</a>";

            /*Send Email to user and inform about new account created*/
            EmailUtility.SendMailInThread(destinationEmail, Subject, emailbody);
        }

        public void sendForgotPasswordEmail(string destinationEmail, string userId, string confirmEmailtoken, string FirstName)
        {
            var url = ConfigurationManager.AppSettings["WebURL"].ToString();
            var callbackUrl = url + "#/setpassword/" + userId + "/" + confirmEmailtoken + "/reset";
            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "ForgotPasswordEmail")
                                 //select r;
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var item in MailModule)
            {
                emailbody = item.mailbody;
                Subject = item.subject;
            }

            emailbody = emailbody.Replace("@@callbackUrl", callbackUrl).Replace("@@personname", FirstName);

            //String emailbody = "<p>You Have been requested to reset the password</p>";
            //emailbody += "<p>Please reset your password by clicking this link </p>";
            //emailbody += "<a href=\"" + callbackUrl + "\">link</a>";

            /*Send Email to user and inform about new account created*/
            EmailUtility.SendMailInThread(destinationEmail, Subject, emailbody);

        }



        [HttpPost]
        [Route("api/Account/RegisterInvitationVendorCompany")]
        public JsonResponse RegisterInvitationVendorCompany(VendorCompanyInvitationModal Modal)
        {
            try
            {
                 string url = ConfigurationManager.AppSettings["WebURL"].ToString();

                AccountBAL = new Account();
                return ControllerExtensions.JsonCustomSuccess(AccountBAL.RegisterInvitationVendorCompany(Modal,url));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/RegisterInvitationVendorCompany/ => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/Account/GetVendorCompanyRecordForRegistration")]
        public JsonResponse GetVendorCompanyRecordForRegistration(VendorCompanyInvitationModal Modal)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();

                AccountBAL = new Account();
                return ControllerExtensions.JsonCustomSuccess(AccountBAL.GetVendorCompanyRecordForRegistration(Modal, url));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/GetVendorCompanyRecordForRegistration/ => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }
        [HttpPost]
        [Route("api/Account/RegistrationsVendorCompany")]
        public JsonResponse RegistrationsVendorCompany(VendorCompanyRegistrationModal Modal)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();

                AccountBAL = new Account();
                return ControllerExtensions.JsonCustomSuccess(AccountBAL.RegistrationsVendorCompany(Modal));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Account/RegistrationsVendorCompany/ => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

    }
}
