﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.MODEL;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;
namespace TIGHTEN.API
{
    public class NotificationsApiController : ApiController
    {

        Notifications objBll;
        // GET api/<controller>


        /// <summary>
        /// It fetches the User Notifications
        /// </summary>
        /// <returns>List of Notifications in Json format</returns>
        /// 
        [Route("api/NotificationsApi/{UserId}")]
        public JsonResponse Get(string UserId)
        {
            try
            {
                objBll = new Notifications();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetNotifications(UserId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/NotificationsApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        // GET api/<controller>/5

        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        public JsonResponse Post(TodoModel.Notifications obj,string userId)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {
                objBll = new Notifications();
                objBll.SaveNotification(obj, userId);


            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/NotificationsApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);
            }

            return ControllerExtensions.JsonSuccess();

        }

        // PUT api/<controller>/5
        public JsonResponse Put(int id)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {
                objBll.UpdateNotification(id);


            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/NotificationsApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }

            return ControllerExtensions.JsonSuccess();
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }


        [HttpPut]
        [Route("api/NotificationsApi/reset/{UserId}")]
        public JsonResponse reset(string UserId)
        {
            try
            {
                objBll = new Notifications();
                return ControllerExtensions.JsonCustomSuccess(objBll.resetNotification(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/NotificationsApi/reset => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError("Error", "error");
            }


        }



    }
}