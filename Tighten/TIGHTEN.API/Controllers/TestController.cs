﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
   // [EnableCors(origins: "http://example.com", headers: "*", methods: "*")]
    [EnableCors("*", "*", "*")]
    public class TestController : ApiController
    {
        // GET: Test
        [Route("api/test")]
        public IHttpActionResult Index()
        {
            return Ok(new { name="Test", token="test", success =true});
        }
    }
}