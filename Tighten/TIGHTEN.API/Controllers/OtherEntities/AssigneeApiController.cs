﻿using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

namespace TIGHTEN.API
{
    public class AssigneeApiController : ApiController
    {
        Assignee objBll;

        /// <summary>
        /// It updated the Assignee Of a particular Todo
        /// </summary>
        /// <param name="model">It consists the TodoId and New Assignee User id</param>
        /// <returns>Returns success or error</returns>
        public JsonResponse Put(AssigneeModel.NewAssigneeUpdateModel model)
        {
            try
            {
                objBll = new Assignee();
                objBll.UpdateAssignee(model.UserId, model.AssigneeUpdateModel);
                return ControllerExtensions.JsonSuccess("Assignee Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/AssigneeApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Problem Occured !");

            }

        }


    }
}