﻿using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;


namespace TIGHTEN.API
{
    public class ClientDashboardApiController : ApiController
    {

        ClientDashboard objBll;

        /// <summary>
        /// It gets the Upcoming Todos of Client Logged In i.e Todos which have deadline date greater than today
        /// </summary>
        /// <returns> Returns Upcoming Todos in Json Format</returns>
        /// 
        [Route("api/ClientDashboardApi/{userId}")]
        public JsonResponse Get(string userId)
        {
            try
            {
                objBll = new ClientDashboard();
                return ControllerExtensions.JsonCustomSuccess(objBll.getClientUpcomingTodos(userId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ClientDashboardApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// Getting Projects in which Client is involved
        /// </summary>
        /// <returns>return projects in json format</returns>
        [HttpGet]
        [Route("api/ClientDashboardApi/Projects/{userId}")]
        public JsonResponse Projects(string userId)
        {
            try
            {
                objBll = new ClientDashboard();
                return ControllerExtensions.JsonCustomSuccess(objBll.getClientProjects(userId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ClientDashboardApi/Projects => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        ///  Get Users of those projects in which Client Is Involved
        /// </summary>
        /// <returns>return Users in json format</returns>
        [HttpGet]
        [Route("api/ClientDashboardApi/Users/{userId}")]
        public JsonResponse Users(string userId)
        {
            try
            {
                objBll = new ClientDashboard();
                return ControllerExtensions.JsonCustomSuccess(objBll.getConcernedUsersOfClient(userId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ClientDashboardApi/Users => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}