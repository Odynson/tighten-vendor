﻿using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

namespace TIGHTEN.API
{
    public class UserDashboardApiController : ApiController
    {
        UserDashboard objBll;

        /// <summary>
        /// It gets the Upcoming Todos of User Logged In i.e Todos which have deadline date greater than today
        /// </summary>
        /// <returns> Returns Upcoming Todos in Json Format</returns>
        /// 
        [Route("api/UserDashboardApi/{UserId}")]
        public JsonResponse Get(string UserId)
        {
            try
            {
                objBll = new UserDashboard();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserUpcomingTodos(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserDashboardApi => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Getting User Calendar Events
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/UserDashboardApi/getUserUpcomingEvents/{UserId}")]
        public JsonResponse getUserUpcomingEvents(string UserId)
        {
            try
            {
                objBll = new UserDashboard();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserUpcomingEvents(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserDashboardApi/getUserUpcomingEvents => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        /// <summary>
        /// Getting User T-Line
        /// </summary>
        /// <param name="id">It is the Userid of User</param>
        /// <returns>Returns T-line values</returns>
        [HttpGet]
        [Route("api/UserDashboardApi/getUserTLine/{UserId}")]
        public JsonResponse getUserTLine(string UserId)
        {
            try
            {
                objBll = new UserDashboard();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserTLine(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserDashboardApi/getUserTLine => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }



        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }


        /// <summary>
        /// Getting All Packages
        /// </summary>
        /// <returns>Returns All Packages </returns>
        [HttpGet]
        [Route("api/UserDashboardApi/GetAllPackages/{CompanyId}")]
        public JsonResponse GetAllPackages(int CompanyId)
        {
            try
            {
                //int a = 0;
                //int b = 10 / a;

                objBll = new UserDashboard();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllPackages(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserDashboardApi/GetAllPackages => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// UpgradePackages
        /// </summary>
        /// <returns>Returns msg </returns>
        [HttpPost]
        [Route("api/UserDashboardApi/UpgradePackage")]
        public JsonResponse UpgradePackage(MODEL.SubscriptionModel model)
        {
            try
            {
                objBll = new UserDashboard();
                return ControllerExtensions.JsonCustomSuccess(objBll.UpgradePackage(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserDashboardApi/UpgradePackage => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


     

    }
}