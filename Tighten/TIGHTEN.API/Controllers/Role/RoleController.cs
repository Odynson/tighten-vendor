﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class RoleController : ApiController
    {
        Role objBLL = new Role();
        [Route("api/Role/GetRoles/{CompanyId}/{RoleId}")]
        public JsonResponse GetRoles(int CompanyId,int RoleId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.getRoles(CompanyId, RoleId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Role/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [Route("api/Role/{RoleId}/{UserId}/{CompanyId}")]
        public JsonResponse Get(int RoleId,string UserId,int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.showSideMenuOptionRolewise(RoleId, UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Role/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !"+ CustomException);
            }
        }

        //[HttpGet]
        //[Route("api/Role/showSideMenuForVendorCompany/{RoleId}/{UserId}/{CompanyId}")]
        //public JsonResponse showSideMenuForVendorCompany(int RoleId, string UserId, int CompanyId)
        //{
        //    try
        //    {
        //        return ControllerExtensions.JsonCustomSuccess(objBLL.showSideMenuForVendorCompany(RoleId, UserId, CompanyId));
        //    }
        //    catch (Exception ex)
        //    {
        //        string CustomException = "Path : api/Role/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

        //        return ControllerExtensions.JsonError("Some Error Occured !");
        //    }
        //}




    }
}
