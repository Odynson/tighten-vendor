﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class RoleManageApiController : ApiController
    {
        RoleManage objBLL = new RoleManage();


        [Route("api/RoleManageApi/{CompanyId}")]
        public JsonResponse Get(int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetAllRolesForCompany(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RoleManageApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        /// <summary>
        /// Saving New Role 
        /// </summary>
        /// <param name="model">It consists the values of all fields to be saved</param>
        /// <returns>It returns response whether it is success or error</returns>
        /// 
        [HttpPost]
        [Route("api/RoleManageApi")]
        public JsonResponse Post(RoleManageModel.RoleAddUpdateModel obj)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.saveRole(obj));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RoleManageApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }

        }




        /// <summary>
        /// This function updates the existing Team
        /// </summary>
        /// 
        [HttpPut]
        [Route("api/RoleManageApi")]
        public JsonResponse Put(RoleManageModel.RoleAddUpdateModel obj)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.updateRole(obj));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RoleManageApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        /// <summary>
        /// This function deletes the Particular Role of a Company
        /// </summary>
        /// <param name="id">It is the Unique Id of Role</param>
        /// <returns>It returns response whether it is success or error</returns>
        
        [Route("api/RoleManageApi/{RoleId}/{CompanyId}")]
        public JsonResponse Delete(int RoleId,int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess( objBLL.deleteRole(RoleId,CompanyId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RoleManageApi/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }



        [Route("api/RoleManageApi/activeInactiveRole/{RoleId}/{CompanyId}")]
        [HttpGet]
        public JsonResponse activeInactiveRole(int RoleId,int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.activeInactiveRoles (RoleId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/activeInactiveRole/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


    }
}
