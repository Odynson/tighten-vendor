﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class RolePermissionApiController : ApiController
    {
        RolePermission objBLL = new RolePermission();


        [Route("api/RolePermissionApi/GetInternalPermissionsForCurrentUser/{RoleId}/{CompanyId}")]
        public JsonResponse GetInternalPermissionsForCurrentUser(int RoleId, int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetInternalPermissionsForCurrentUser(RoleId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RolePermissionApi/GetInternalPermissionsForCurrentUser => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [Route("api/RolePermissionApi/{CompanyId}")]
        public JsonResponse Get(int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetAllRolesForCompany(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RolePermissionApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [Route("api/RolePermissionApi/GetPermissionForSelectedRole/{RoleId}/{CompanyId}")]
        public JsonResponse GetPermissionForSelectedRole(int RoleId,int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetPermissionForSelectedRole(RoleId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RolePermissionApi/GetPermissionForSelectedRole => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpPost]
        [Route("api/RolePermissionApi")]
        public JsonResponse Post(MODEL.RolePermissionModel.SavePermissionForSelectedRole model)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.savePermissionsForSelectedRole(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RolePermissionApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [Route("api/RolePermissionApi/GetAllInternalPermissionsForPermission/{MenuId}/{RoleId}/{CompanyId}")]
        public JsonResponse GetAllInternalPermissionsForPermission(int MenuId, int RoleId, int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetAllInternalPermissionsForPermission( MenuId,  RoleId,  CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RolePermissionApi/GetAllInternalPermissionsForPermission => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpPost]
        [Route("api/RolePermissionApi/SaveInternalPermissions")]
        public JsonResponse SaveInternalPermissions(MODEL.RolePermissionModel.SaveInternalPermissionsModel model)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.SaveInternalPermissions(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RolePermissionApi/SaveInternalPermissions => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



      
    }
}
