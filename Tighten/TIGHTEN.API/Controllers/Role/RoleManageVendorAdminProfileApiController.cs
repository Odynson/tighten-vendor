﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL.RoleManageVendorAdminProfile;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers.RoleManageVendorAdminProfileApiController
{
    public class RoleManageVendorAdminProfileApiController : ApiController
    {
        RoleManageVendorAdminProfileBLL objBLL = new RoleManageVendorAdminProfileBLL();
        // GET: api/RoleManageVendorAdminProfileApi

        [HttpGet]
        [Route("api/RoleManageVendorAdminProfileApi/{VendorCompanyId}")]
        public JsonResponse Get(int VendorCompanyId)
        {
            try
            {
            return ControllerExtensions.JsonCustomSuccess(objBLL.GetAllRolesForVendorAdminProfile( VendorCompanyId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/RoleManageVendorAdminProfileApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
         
        }


        [HttpPost]
        [Route("api/RoleManageVendorAdminProfileApi")]
        public JsonResponse Post(MODEL.RoleManageVendorAdminProfile.RoleAddUpdateModel obj)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.saveRole(obj));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RoleManageVendorAdminProfileApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }

        }

        [HttpPut]
        [Route("api/RoleManageVendorAdminProfileApi")]
        public JsonResponse Put(MODEL.RoleManageVendorAdminProfile.RoleAddUpdateModel obj)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.updateRole(obj));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RoleManageVendorAdminProfileApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpDelete]
        [Route("api/RoleManageVendorAdminProfileApi/{RoleId}")]
        public JsonResponse Delete(int RoleId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.DeleteRole(RoleId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/RoleManageVendorAdminProfileApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }
    }
}
