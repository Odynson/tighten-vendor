﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.MODEL;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
    public class CalendarEventsApiController : ApiController
    {
        /// <summary>
        ///  Declaration of CalendarEvents class Of Business Logic Layer through which we will access its functions.
        /// </summary>
        CalendarEvents objBLL;

        /// <summary>
        ///  This function is used to Get the Calendar Events for a Particular Team
        /// </summary>
        /// <param name="id">THis is the Unique TeamId related to which the Calendar Events will be Fetched</param>
        /// <returns>List of CalendarEvents in Json Format</returns>
        /// 

        //[HttpGet]
        //[Route("api/CalendarEventsApi/{id}/{userId}")]
        //public HttpResponseMessage Get(int id, string userId)
        //{
        //    objBLL = new CalendarEvents();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBLL.GetCalendarEvents(id, userId));
        //}


        [HttpGet]
        [Route("api/CalendarEventsApi/{CompanyId}/{UserId}/{TeamId}/{ProjectId}")]
        public JsonResponse Get(int CompanyId, string UserId, int TeamId, int ProjectId)
        {
            objBLL = new CalendarEvents();
            return ControllerExtensions.JsonCustomSuccess(objBLL.GetCalendarEvents(CompanyId, UserId, TeamId, ProjectId));
        }


        /// <summary>
        ///  This function is used to Get the Single Event
        /// </summary>
        /// <param name="id">THis is the Unique Event Id related to which the Calendar Event will be Fetched</param>
        /// <returns>Single Calendar Event in Json Format</returns>
        [HttpGet]
        [Route("api/CalendarEventsApi/GetEvent/{id}")]
        public JsonResponse GetEvent(int id)
        {
            try
            {
                objBLL = new CalendarEvents();

                return ControllerExtensions.JsonCustomSuccess(objBLL.GetSingleEvent(id));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/CalendarEventsApi/GetEvent => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        /// <summary>
        /// This function is used to Save the new Calendar Event
        /// </summary>
        /// <param name="obj">This is the model to which the values will be assigned. We will send the object from Javascript and it will be assigned to this model</param>

        public JsonResponse Post(MODEL.CalendarModel.CalendarEvents model)
        {
            try
            {
                objBLL = new CalendarEvents();
                return ControllerExtensions.JsonCustomSuccess(objBLL.SaveCalendarEvent(model));
            }
            catch(Exception ex)
            {
                string CustomException = "Path : api/CalendarEventsApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError("error");
            }
        }

        /// <summary>
        /// This function is used for Updating the CalendarEvent
        /// </summary>
        /// <param name="obj">It is the object which consists the updated values of Calendar Event</param>
        /// <returns> It returns JsonResponse i.e IsSuccess, Message, Error etc</returns>
        public JsonResponse Put(TodoModel.CalendarEvents obj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBLL = new CalendarEvents();
                objBLL.EditCalendarEvent(obj);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/CalendarEventsApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }
            return ControllerExtensions.JsonSuccess();
        }



        /// <summary>
        /// This function is used for Updating the CalendarEvent
        /// </summary>
        /// <param name="obj">It is the object which consists the updated values of Calendar Event</param>
        /// <returns> It returns JsonResponse i.e IsSuccess, Message, Error etc</returns>
        [HttpPut]
        [Route("api/CalendarEventsApi/UpdateOnDrop")]
        public JsonResponse UpdateOnDrop(MODEL.CalendarModel.UpdateEventOnDropOrResize model)
        {
            try
            {
                objBLL = new CalendarEvents();
                objBLL.UpdateOnDrop(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/CalendarEventsApi/UpdateOnDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }
            return ControllerExtensions.JsonSuccess();
        }


        /// <summary>
        /// This function is used for Updating the CalendarEvent
        /// </summary>
        /// <param name="obj">It is the object which consists the updated values of Calendar Event</param>
        /// <returns> It returns JsonResponse i.e IsSuccess, Message, Error etc</returns>
        [HttpPut]
        [Route("api/CalendarEventsApi/UpdateOnResize")]
        public JsonResponse UpdateOnResize(MODEL.CalendarModel.UpdateEventOnDropOrResize model)
        {
            try
            {
                objBLL = new CalendarEvents();
                objBLL.UpdateOnResize(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/CalendarEventsApi/UpdateOnResize => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }
            return ControllerExtensions.JsonSuccess();
        }


        /// <summary>
        /// This function is used to Delete Calendar Event
        /// </summary>
        /// <param name="id">it is the unique id of CalendarEvent which is being removed</param>
        /// <returns> It returns JsonResponse i.e IsSuccess, error etc </returns>
        public JsonResponse Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {

                objBLL = new CalendarEvents();
                objBLL.DeleteCalendarEvent(id);

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/CalendarEventsApi/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }

            return ControllerExtensions.JsonSuccess();
        }
    }
}