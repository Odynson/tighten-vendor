﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;
namespace TIGHTEN.API
{
    public class FilesController : ApiController
    {
        ToDoAttachments objAttach = new ToDoAttachments();
        [HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        public JsonResponse Upload()
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedAttachment"];
                int ToDoId = Convert.ToInt32(HttpContext.Current.Request.Params["ToDoId"]);
                string userId = Convert.ToString(HttpContext.Current.Request.Params["UserId"]);
                if (httpPostedFile != null)
                {
                    var fileName = httpPostedFile.FileName;
                    string strExtension = Path.GetExtension(fileName);
                    Guid guid = Guid.NewGuid();
                    string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;
                    // Validate the uploaded image(optional)

                    // Get the complete file path
                    //string path = ConfigurationManager.AppSettings["ImageServerPath"].ToString();
                    //path = path + "/TIGHTEN-WEB/Uploads/Attachments/ToDo";
                    //var fileSavePath = Path.Combine(path, FinalFilename);
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/Attachments/ToDo"), FinalFilename);

                    // Save the uploaded file to "Uploads" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    string msg = objAttach.saveToDoAttachments("Local", ToDoId, FinalFilename, fileName, userId, "computer.png");
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FilesApi/Upload => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("File Uploaded Successfully !");
        }

        [Route("api/FilesApi/BoxFiles")]
        [HttpPost]
        public JsonResponse BoxFiles(MODEL.ToDoAttachmentsForAllApis model)
        {
            try
            {
                var fileName = model.boxModel.name;
                string FinalFilename = model.boxModel.url;

                return ControllerExtensions.JsonCustomSuccess(objAttach.saveToDoAttachments("Box", model.ToDoId, FinalFilename, fileName, model.UserId, "box-icon.png"));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FilesApi/BoxFiles => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [Route("api/FilesApi/DropBoxFiles")]
        [HttpPost]
        public JsonResponse DropBoxFiles(MODEL.ToDoAttachmentsForAllApis model)
        {
            try
            {
                var fileName = model.DropBoxModel.name;
                string FinalFilename = model.DropBoxModel.link;

                return ControllerExtensions.JsonCustomSuccess(objAttach.saveToDoAttachments("DropBox", model.ToDoId, FinalFilename, fileName, model.UserId, "drop-box.png"));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FilesApi/DropBoxFiles => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [Route("api/FilesApi/GoogleDriveFiles")]
        [HttpPost]
        public JsonResponse GoogleDriveFiles(MODEL.ToDoAttachmentsForAllApis model)
        {
            try
            {
                var fileName = model.GoogleDriveModel.name;
                string FinalFilename = model.GoogleDriveModel.url;

                return ControllerExtensions.JsonCustomSuccess(objAttach.saveToDoAttachments("GoogleDrive", model.ToDoId, FinalFilename, fileName, model.UserId, "google-drive.png"));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FilesApi/GoogleDriveFiles => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }





    }
}