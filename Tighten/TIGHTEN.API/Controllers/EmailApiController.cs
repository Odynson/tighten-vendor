﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.MODEL;
using TIGHTEN.BUSINESS;
using System.Threading.Tasks;
namespace TIGHTEN.API
{
    public class EmailApiController : ApiController
    {



        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }
        
        // POST api/<controller>
        public async Task Post(TodoModel.Emails obj)
        {
            Email objEmail = new Email();
            await objEmail.SendEmail(obj);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}