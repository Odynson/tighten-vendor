﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
    public class UserProfileApiController : ApiController
    {
        UserProfile objBll;



        /// <summary>
        /// Getting User's Educational Details for User Profile From UserEducationalDetail Table
        /// </summary>
        /// <returns>It returns User's Educational Details as an array</returns>
        /// 
        [Route("api/UserProfileAPI/{UserId}")]
        public JsonResponse Get(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserEducationDetail(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Getting User's Professional Details for User Profile From UserProfessionalDetail Table
        /// </summary>
        /// <returns>It returns User's Professional Details as an array</returns>
        /// 
        [Route("api/UserProfileAPI/getUserProfessionalDetail/{UserId}")]
        public JsonResponse getUserProfessionalDetail(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserProfessionalDetail(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserProfessionalDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Save User's Professional Details for User Profile in UserProfessionalDetail Table
        /// </summary>
        /// <returns>It returns success or error msg </returns>
        /// 
        [HttpPost]
        [Route("api/UserProfileAPI/saveUserProfessionalDetail")]
        public JsonResponse saveUserProfessionalDetail(MODEL.UserProfileModel.UserProfessionalDetail model)
        {
            try
            {
                objBll = new UserProfile();
                int id = objBll.saveUserProfessionalDetail(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/saveUserProfessionalDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("professionalSaveDetailSuccess");
        }




        /// <summary>
        /// Update User's Professional Details for User Profile in UserProfessionalDetail Table
        /// </summary>
        /// <returns>It returns success or error msg </returns>
        /// 
        [HttpPut]
        [Route("api/UserProfileAPI/updateUserProfessionalDetail")]
        public JsonResponse Put(MODEL.UserProfileModel.UserProfessionalDetail model)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.updateUserProfessionalDetail(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/updateUserProfessionalDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Save User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <returns>It returns success or error msg </returns>
        /// 
        [HttpPost]
        [Route("api/UserProfileAPI/saveUserEducationDetail")]
        public JsonResponse saveUserEducationDetail(MODEL.UserProfileModel.UserEducationDetail model)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.saveUserEducationDetail(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/saveUserEducationDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Update User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <returns>It returns success or error msg </returns>
        /// 
        [HttpPut]
        [Route("api/UserProfileAPI/updateUserEducationDetail")]
        public JsonResponse updateUserEducationDetail(MODEL.UserProfileModel.UserEducationDetail model)
        {
            try
            {
                objBll = new UserProfile();
                int id = objBll.updateUserEducationDetail(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/updateUserEducationDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("EducationDetailUpdateSuccess");
        }



        /// <summary>
        /// Delete User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <returns>It returns success or error msg </returns>
        /// 
        [HttpDelete]
        [Route("api/UserProfileAPI/deleteUserEducationDetail/{EducationDetailObjectId}")]
        public JsonResponse deleteUserEducationDetail(int EducationDetailObjectId)
        {
            try
            {
                objBll = new UserProfile();
                objBll.deleteUserEducationDetail(EducationDetailObjectId);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/deleteUserEducationDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("educationDetailDeleted");
        }



        /// <summary>
        /// Delete User's Professional Details for User Profile in UserProfessionalDetail Table
        /// </summary>
        /// <returns>It returns success or error msg </returns>
        /// 
        [HttpDelete]
        [Route("api/UserProfileAPI/deleteUserProfessionalDetail/{ProfessionalDetailObjectId}")]
        public JsonResponse deleteUserProfessionalDetail(int ProfessionalDetailObjectId)
        {
            try
            {
                objBll = new UserProfile();
                objBll.deleteUserProfessionalDetail(ProfessionalDetailObjectId);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/deleteUserProfessionalDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("professionalDetailDeleted");
        }



        /// <summary>
        /// Update User's Hourly Rate 
        /// </summary>
        /// <returns>It returns the new Hourly Rate </returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/updateUserHourlyRate/{UserId}/{newHourlyRate}")]
        public JsonResponse updateUserHourlyRate(string UserId,decimal newHourlyRate)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.updateUserHourlyRate(UserId, newHourlyRate));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/updateUserHourlyRate => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Update User's Availabilty 
        /// </summary>
        /// <returns>It returns the new Availabilty </returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/updateUserAvailabilty/{UserId}/{AvailableHours}")]
        public JsonResponse updateUserAvailabilty(string UserId, decimal AvailableHours)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.updateUserAvailabilty(UserId, AvailableHours));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/updateUserAvailabilty => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Getting User's Favourite Projects for User Profile From UserDetails Table
        /// </summary>
        /// <returns>It returns User's Favourite Projects as an array</returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserFavouriteProjects/{UserId}")]
        public JsonResponse getUserFavouriteProjects(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserFavouriteProjects(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserFavouriteProjects => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Getting User's Availabilty for User Profile  
        /// </summary>
        /// <returns>It returns User's Availabilty</returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserAvailabilty/{UserId}")]
        public JsonResponse getUserAvailabilty(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserAvailabilty(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserAvailabilty => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Save User's Favourite Projects for User Profile In UserDetails Table
        /// </summary>
        /// <returns>It returns success message</returns>
        /// 
        [HttpPost]
        [Route("api/UserProfileAPI/saveUserFavouriteProjects")]
        public JsonResponse saveUserFavouriteProjects(MODEL.UserProfileModel.SaveFavoriteProjectsModel model)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.saveUserFavouriteProjects(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/saveUserFavouriteProjects => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        /// <summary>
        /// Getting User's Statistics for User Profile  
        /// </summary>
        /// <returns>It returns User's Statistics as an array</returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserStatistics/{UserId}/{CurrentUserId}")]
        public JsonResponse getUserStatistics(string UserId,string CurrentUserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserStatistics(UserId, CurrentUserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserStatistics => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Getting User's Efficiency for User Profile  
        /// </summary>
        /// <returns>It returns User's Efficiency as an array</returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserEfficiency/{UserId}/{CurrentUserId}")]
        public JsonResponse getUserEfficiency(string UserId,string CurrentUserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserEfficiency(UserId, CurrentUserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserEfficiency => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        

        /// <summary>
        /// Getting User's Efficiency for User Profile  
        /// </summary>
        /// <returns>It returns User's Efficiency as an array</returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserEfficiencyForStrongAndWeekTechnology/{UserId}")]
        public JsonResponse getUserEfficiencyForStrongAndWeekTechnology(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserEfficiencyForStrongAndWeekTechnology(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserEfficiencyForStrongAndWeekTechnology => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }





        /// <summary>
        /// Getting User's Avg Rate for User Profile  
        /// </summary>
        /// <returns>It returns User's Avg Rate as an array</returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserAvgRate/{UserId}")]
        public JsonResponse getUserAvgRate(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserAvgRate(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserAvgRate => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Getting User's Tags for User Profile  
        /// </summary>
        /// <returns>It returns User's Tags as an array</returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserTags/{UserId}")]
        public JsonResponse getUserTags(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserTags(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserTags => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Getting User's Skill Set Tags for User Profile  
        /// </summary>
        /// <returns>It returns User's Skill Set Tags as an array</returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserSkillSetTags/{UserId}")]
        public JsonResponse getUserSkillSetTags(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserSkillSetTags(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserSkillSetTags => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Getting User's Tags for Professional Details in User Profile  
        /// </summary>
        /// <returns>It returns User's Tags for Professional Details as an array</returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getTagsForProfessionalDetails/{UserId}")]
        public JsonResponse getTagsForProfessionalDetails(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTagsForProfessionalDetails(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getTagsForProfessionalDetails => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Save User's Tags for User Profile In Tags Table
        /// </summary>
        /// <returns>It returns success message</returns>
        /// 
        [HttpPost]
        [Route("api/UserProfileAPI/saveUserTags")]
        public JsonResponse saveUserTags(MODEL.UserProfileModel.SaveTags model)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.saveUserTags(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/saveUserTags => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Getting User's Team And Project Count in User Profile  
        /// </summary>
        /// <returns>It returns User's Team And Project Count </returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserTeamAndProjectCount/{UserId}")]
        public JsonResponse getUserTeamAndProjectCount(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserTeamAndProjectCount(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserTeamAndProjectCount => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }





        /// <summary>
        /// Getting User's Testimonials in User Profile  
        /// </summary>
        /// <returns>It returns User's Testimonials </returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserTestimonials/{UserId}")]
        public JsonResponse getUserTestimonials(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserTestimonials(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserTestimonials => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Getting User's Testimonials in User Profile  
        /// </summary>
        /// <returns>It returns User's Testimonials </returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserFeedbacks/{UserId}")]
        public JsonResponse getUserFeedbacks(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserFeedbacks(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserFeedbacks => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Getting Other User's Profile Status in User Profile  
        /// </summary>
        /// <returns>It returns Other User's Profile Status </returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getOtherUserProfileStatus/{UserId}")]
        public JsonResponse getOtherUserProfileStatus(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getOtherUserProfileStatus(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getOtherUserProfileStatus => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Getting User's Testimonials in User Profile  
        /// </summary>
        /// <returns>It returns User's Testimonials </returns>
        /// 
        [HttpPost]
        [Route("api/UserProfileAPI/saveUserTestimonials")]
        public JsonResponse saveUserTestimonials(MODEL.UserProfileModel.SaveTestimonial model)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.saveUserTestimonials(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/saveUserTestimonials => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Change User's ProfileStatus in User Profile  
        /// </summary>
        /// <returns>It returns User's ProfileStatus </returns>
        /// 
        [HttpPost]
        [Route("api/UserProfileAPI/changeUserProfile")]
        public JsonResponse changeUserProfile(MODEL.UserProfileModel.USerProfileStatusModel model)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.changeUserProfile(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/changeUserProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        /// <summary>
        /// Change User's Educational Streams
        /// </summary>
        /// <returns>It returns User's Educational Streams </returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getEducationalStreams/{UserId}")]
        public JsonResponse getEducationalStreams(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getEducationalStreams(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getEducationalStreams => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// Getting User's Email-Id in User Profile 
        /// </summary>
        /// <returns>It returns User's Email-Id </returns>
        /// 
        [HttpGet]
        [Route("api/UserProfileAPI/getUserEmailFromUserId/{UserId}")]
        public JsonResponse getUserEmailFromUserId(string UserId)
        {
            try
            {
                objBll = new UserProfile();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserEmailFromUserId(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserProfileAPI/getUserEmailFromUserId => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




    }
}
