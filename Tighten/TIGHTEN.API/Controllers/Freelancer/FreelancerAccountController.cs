﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API 
{
    public class FreelancerAccountController : ApiController
    {
        FreelancerAccount objBll;
        [HttpGet]
        [Route("api/FreelancerAccount/{key}")]
        public JsonResponse get(string key)
        {
            try
            {
                objBll = new FreelancerAccount();
                return ControllerExtensions.JsonCustomSuccess(objBll.getFreelancerInfoForRegistration(key));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FreelancerAccount => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpGet]
        [Route("api/FreelancerAccount/GetFreelancerRoleForCompany/{UserId}/{CompanyId}")]
        public JsonResponse GetFreelancerRoleForCompany(string UserId,int CompanyId)
        {
            try
            {
                objBll = new FreelancerAccount();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetFreelancerRoleForCompany(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FreelancerAccount/GetFreelancerRoleForCompany => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }

        [HttpPost]
        [Route("api/FreelancerAccount")]
        public JsonResponse Post(MODEL.InsertFreelancer ModelToInsert)
        {
            try
            {
                objBll = new FreelancerAccount();
                MODEL.RegisterBindingModel model = ModelToInsert.RegisterBindingModel;
                MODEL.FreelancerInvitationModel freelancerInfo = ModelToInsert.FreelancerInvitationModel;
                return ControllerExtensions.JsonCustomSuccess(objBll.RegisterFreelancer(model,freelancerInfo,ModelToInsert.pictureUrl));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FreelancerAccount => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }

        [Route("api/FreelancerAccount/getFreelancerInfo/{UserName}/{CompanyId}")]
        [HttpGet]
        public JsonResponse getFreelancerInfo(string UserName,int CompanyId)
        {
            try
            {
                objBll = new FreelancerAccount();
                return ControllerExtensions.JsonCustomSuccess(objBll.getFreelancerInfo(UserName, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FreelancerAccount/getFreelancerInfo => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [Route("api/FreelancerAccount/GetAllFreelancer")]
        [HttpGet]
        public JsonResponse Get()
        {
            try
            {
                objBll = new FreelancerAccount();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllFreelancer());
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FreelancerAccount/GetAllFreelancer => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpGet]
        [Route("api/FreelancerAccount/getCompaniesForFreelancer/{UserId}")]
        public JsonResponse getCompaniesForFreelancer(string UserId)
        {
            try
            {
                objBll = new FreelancerAccount();
                return ControllerExtensions.JsonCustomSuccess(objBll.getCompaniesForFreelancer(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FreelancerAccount/getCompaniesForFreelancer => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpPost]
        [Route("api/FreelancerAccount/confirmEmailOrHandle")]
        public JsonResponse confirmEmailOrHandle(MODEL.FreelancerInfoToSendInvitationModel model)
        {
            try
            {
                objBll = new FreelancerAccount();
                return ControllerExtensions.JsonCustomSuccess(objBll.confirmEmailOrHandle(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FreelancerAccount/confirmEmailOrHandle => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpGet]
        [Route("api/FreelancerAccount/GetAllFreelancerInProject/{ProjectId}")]
        public JsonResponse GetAllFreelancerInProject(int ProjectId)
        {
            try
            {
                objBll = new FreelancerAccount();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllFreelancerInProject(ProjectId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FreelancerAccount/GetAllFreelancerInProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }




    }
}
