﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers 
{
    public class InvitationApiController : ApiController
    {
        Invitation objBll;

        [HttpGet]
        [Route("api/InvitationApi/{UseriD}/{InvitaitonStatus}")]
        public JsonResponse Get(string UseriD,string InvitaitonStatus)
        {
            try
            {
                objBll = new Invitation();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInvitationNotification(UseriD, InvitaitonStatus));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvitationApi/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }


        }

        public JsonResponse Post(MODEL.InvitationNotificationModal model)
        {
            try
            {
                objBll = new Invitation();
                return ControllerExtensions.JsonCustomSuccess(objBll.AcceptInvitation(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvitationApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        public JsonResponse Put(MODEL.InvitationNotificationModal model)
        {
            try
            {
                objBll = new Invitation();
                return ControllerExtensions.JsonCustomSuccess(objBll.RejectInvitation(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvitationApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpGet]
        [Route("api/InvitationApi/GetUserMessage/{FreelancerInvitationId}/{UserId}")]
        public JsonResponse GetUserMessage(int FreelancerInvitationId , string UserId)
        {
            try
            {
                objBll = new Invitation();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserMessage(FreelancerInvitationId, UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvitationApi/GetUserMessage => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpPost]
        [Route("api/InvitationApi/SendMessgae")]
        public JsonResponse SendMessgae(MODEL.InvitationMessagesModal model)
        {
            try
            {
                objBll = new Invitation();
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                return ControllerExtensions.JsonCustomSuccess(objBll.SendMessgae(model,url));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvitationApi/SendMessgae => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }



        [HttpGet]
        [Route("api/InvitationApi/GetFreelancerInfo/{InvitationId}")]
        public JsonResponse GetFreelancerInfo(int InvitationId)
        {
            try
            {
                objBll = new Invitation();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetFreelancerInfo(InvitationId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvitationApi/GetFreelancerInfo => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }



    }
}
