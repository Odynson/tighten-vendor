﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
    public class TeamApiController : ApiController
    {


        Team objBll;


        /// <summary>
        /// Getting Teams In which The User is a Team Member
        /// </summary>
        /// <returns>returns the teams </returns>
        /// 
        [Route("api/TeamApi/getTeamByUserId/{userId}/{CompanyId}")]
        public JsonResponse Get(string userId, int CompanyId)
        {
            try
            {
                objBll = new Team();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTeams(userId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/TeamApi/getTeamByUserId => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }

        /// <summary>
        /// Getting Details of a particular Team
        /// </summary>
        /// <param name="id">It is the Unique Id of Team</param>
        /// <returns>It returns the details of Team In Json Format</returns>
        /// 
        //[Route("api/TeamApi/getTeamByU/{userId}")]
        public JsonResponse Get(int teamId, string userId)
        {
            try
            {
                objBll = new Team();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTeam(teamId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/TeamApi/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        /// <summary>
        /// It fetches Company Teams With Team Members
        /// </summary>
        /// <param name="id">It is unique Id of Team</param>
        /// <returns>It returns Teams is Json Format</returns>
        [HttpGet]
        [Route("api/teamapi/CompanyTeams/{CompanyId}/{UserId}/{PageIndex}/{PageSizeSelected}")]
        public JsonResponse CompanyTeams(int CompanyId, string UserId, int PageIndex, int PageSizeSelected)
        {
            try
            {
                objBll = new Team();
                 return ControllerExtensions.JsonCustomSuccess(objBll.getCompanyTeams(CompanyId, UserId, PageIndex, PageSizeSelected));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamapi/CompanyTeams => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !"+ CustomException);

            }

        }

        [HttpGet]
        [Route("api/teamapi/CompanyTeamsChart/{CompanyId}/{UserId}/{PageIndex}/{PageSizeSelected}/{ParentTeamId}")]
        public JsonResponse CompanyTeamsChart(int CompanyId, string UserId, int PageIndex, int PageSizeSelected,int ParentTeamId)
        {
            try
            {
                objBll = new Team();
                List<TeamChartModel> ReturnList = new List<TeamChartModel>();
                string ComapnyName = objBll.getCompanyName(CompanyId);
                ReturnList.Add(new TeamChartModel()
                {
                    title = CompanyId.ToString(),//2,
                    name = ComapnyName,//"Vintage Company",
                    children = objBll.getCompanyTeamsChart(CompanyId, UserId, PageIndex, PageSizeSelected, ParentTeamId),

                }
                    );
                return ControllerExtensions.JsonCustomSuccess(ReturnList);

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamapi/CompanyTeams => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }


        /// <summary>
        /// It fetches User Teams With Team Members in which user is a Team Member
        /// </summary>
        /// <param name="id">It is unique Id of User</param>
        /// <returns>It returns Teams is Json Format</returns>
        /// 
        [HttpGet]
        [Route("api/teamapi/UserTeams/{id}/{CompanyId}/{isOtherUser}")]
        public JsonResponse UserTeams(string id, int CompanyId, bool isOtherUser)
        {
            try
            {
                objBll = new Team();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserTeams(id, CompanyId, isOtherUser));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamapi/UserTeams => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }




        /// <summary>
        /// Saving New Team with it Team Members
        /// </summary>
        /// <param name="model">It consists the values of all fields to be saved</param>
        /// <returns>It returns response whether it is success or error</returns>
        /// 
        [HttpPost]
        //public JsonResponse Post(TeamModel.Team obj, string userId)
        public JsonResponse Post(NewModelForTeam obj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {
                objBll = new Team();
                return ControllerExtensions.JsonCustomSuccess(objBll.saveTeam(obj.UserId, obj.TeamModel, obj.CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamapi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }

        }


        /// <summary>
        /// This function updates the existing Team
        /// </summary>
        /// <param name="model">It consists the updated values of Team</param>
        /// <returns>It returns response whether it is success or error</returns>
        /// 
        [HttpPut]
        public JsonResponse Put(NewModelForTeam team)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new Team();
                //objBll.updateTeam(team.UserId, team.TeamModel, team.CompanyId);
                return ControllerExtensions.JsonCustomSuccess(objBll.updateTeam(team.UserId, team.TeamModel, team.CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamapi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError("Error","Some Error Occured !");
            }

            //return ControllerExtensions.JsonSuccess("Team Updated Successfully !");
        }


        /// <summary>
        /// This function deletes the Particular Team
        /// </summary>
        /// <param name="id">It is the Unique Id of Team</param>
        /// <returns>It returns response whether it is success or error</returns>
        public JsonResponse Delete(int id)
        {
            try
            {
                objBll = new Team();
                string msg = objBll.deleteTeam(id);
                if (msg == "TeamDeletedSuccessfully")
                {
                    return ControllerExtensions.JsonSuccess(msg);
                }
                else
                {
                    return ControllerExtensions.JsonError(msg);
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamapi/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }


        [HttpGet]
        [Route("api/teamapi/GetAllDesignations")]
        public JsonResponse GetAllDesignations()
        {
            try
            {
                objBll = new Team();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllDesignations());
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/VendorApi/GetAllDesignations => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/teamapi/GetAllTeams/{CompanyId}/{UserId}")]
        public JsonResponse GetAllTeams(int CompanyId,string UserId)
        {
            try
            {
                objBll = new Team();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllTeams(CompanyId,UserId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/VendorApi/GetAllTeams => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/teamapi/GetTeamMembersByTeamId/{CompanyId}/{TeamId}")]
        public JsonResponse GetTeamMembersByTeamId(int CompanyId, int TeamId)
        {
            try
            {
                objBll = new Team();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetTeamMembersByTeamId(CompanyId, TeamId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/VendorApi/GetTeamMembersByTeamId => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpGet]
        [Route("api/teamapi/CompanyNestedTeamsChart/{CompanyId}/{UserId}/{PageIndex}/{PageSizeSelected}/{ParentTeamId}")]
        public JsonResponse CompanyNestedTeamsChart(int CompanyId, string UserId, int PageIndex, int PageSizeSelected,int ParentTeamId)
        {
            try
            {
                objBll = new Team();
                List<TeamChartModel> ReturnList = new List<TeamChartModel>();
                TeamModel.TeamWithMembers teamObj= objBll.getTeam(ParentTeamId);
                if (teamObj != null)
                {
                    ReturnList.Add(new TeamChartModel()
                    {
                        title = ParentTeamId.ToString(),
                        name = teamObj.TeamName,
                        children = objBll.getCompanyTeamsChart(CompanyId, UserId, PageIndex, PageSizeSelected, ParentTeamId),

                    }

                    );
                }
                return ControllerExtensions.JsonCustomSuccess(ReturnList);

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamapi/CompanyNestedTeamsChart => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }
    }
}