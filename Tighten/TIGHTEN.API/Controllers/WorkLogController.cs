﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.BUSINESS.WorkLog;
using TIGHTEN.UTILITIES;
using TIGHTEN.MODEL;
using System.Web.Http.Routing;
using System.Configuration;
using System.Web.Http.Cors;
using System.Threading;
using System.Xml;
using System.IO;
using System.Web;
using System.Xml.Linq;

namespace TIGHTEN.API.Controllers
{
    [EnableCors("*", "*", "*")]
    public class WorkLogController : ApiController
    {

        WorkLogBLL objBll;
        // Post: api/VendorApi
        [HttpGet]
        [Route("api/WorkLogApi/GetWorkLogList/{UserId}")]
        public JsonResponse GetWorkLogList(string UserId)
        {

            try
            {
                objBll = new WorkLogBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetWorkLogList(UserId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/WorkLogApi/GetWorkLogList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpPost]
        [Route("api/WorkLogApi/SaveWorkLog")]
        public JsonResponse SaveWorkLog(MODEL.WorkLog.WorkLogDetailsModel model)
        {
            try
            {
                objBll = new WorkLogBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.saveWorkLog(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/saveWorkLog => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!"+CustomException);
            }
        }


        [HttpPost]
        [Route("api/WorkLogApi/EditWorkLog")]
        public JsonResponse EditWorkLog(MODEL.WorkLog.WorkLogDetailsModel model)
        {
            try
            {
                objBll = new WorkLogBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.editWorkLog(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/saveWorkLog => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!" + CustomException);
            }
        }

        [HttpGet]
        [Route("api/WorkLogApi/GetWorkLogById/{Id}/{UserId}")]
        public JsonResponse GetWorkLogById(int Id,string UserId)
        {

            try
            {
                objBll = new WorkLogBLL();
                MODEL.WorkLog.WorkLogDetailsModel WrkLog = new MODEL.WorkLog.WorkLogDetailsModel();
                WrkLog = objBll.GetWorkLogById(Id);
                Projects PobjBll = new Projects();
                if (WrkLog != null)
                {
                    WrkLog.WorkLogVendorList = PobjBll.getApprovedProjectByUserId(UserId);
                    WrkLog.ProjectMilestones = PobjBll.GetProjectMilestonesByProjectId(WrkLog.ProjectId);
                    WrkLog.CreatedOn = Convert.ToDateTime(WrkLog.CreatedOn).ToShortDateString();
                }
                return ControllerExtensions.JsonCustomSuccess(WrkLog);


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/WorkLogApi/GetWorkLogById => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpGet]
        [Route("api/WorkLogApi/DeleteWorkLogById/{Id}")]
        public JsonResponse DeleteWorkLogById(int Id)
        {

            try
            {
                objBll = new WorkLogBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.deleteWorkLogById(Id));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/WorkLogApi/DeleteWorkLogById => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpGet]
        [Route("api/WorkLogApi/getWorkLogEnteryList/{WorkLogId}")]
        public JsonResponse GetWorkLogEnteryList(int WorkLogId)
        {

            try
            {
                objBll = new WorkLogBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetWorkLogEnteryList(WorkLogId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/WorkLogApi/GetWorkLogEnteryList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpPost]
        [Route("api/WorkLogApi/SaveWorkLogEntry")]
        public JsonResponse SaveWorkLogEntry(MODEL.WorkLog.WorkLogEnteriesModel model)
        {
            try
            {
                objBll = new WorkLogBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.SaveWorkLogEntry(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/SaveWorkLogEntry => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!" + CustomException);
            }
        }



        [HttpGet]
        [Route("api/WorkLogApi/GetWorkLogEntryById/{Id}")]
        public JsonResponse GetWorkLogEntryById(int Id)
        {

            try
            {
                objBll = new WorkLogBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetWorkLogEntryById(Id));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/WorkLogApi/GetWorkLogEntryById => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }
        [HttpPost]
        [Route("api/WorkLogApi/EditWorkLogEntry")]
        public JsonResponse EditWorkLogEntry(MODEL.WorkLog.WorkLogEnteriesModel model)
        {
            try
            {
                objBll = new WorkLogBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.editWorkLogEntry(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/EditWorkLogEntry => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!" + CustomException);
            }
        }

        [HttpGet]
        [Route("api/WorkLogApi/DeleteWorkLogEntryById/{Id}")]
        public JsonResponse DeleteWorkLogEntryById(int Id)
        {

            try
            {
                objBll = new WorkLogBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.deleteWorkLogEntryById(Id));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/WorkLogApi/DeleteWorkLogEntryById => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpGet]
        [Route("api/WorkLogApi/GetWorkLogListByProjectFromTo/{UserId}/{ProjectId}/{FromDate}/{ToDate}")]
        public JsonResponse GetWorkLogListByProjectFromTo(string UserId,int ProjectId,string FromDate,string ToDate)
        {

            try
            {
                objBll = new WorkLogBLL();
                DateTime tdate = Convert.ToDateTime(ToDate).AddDays(1);
                return ControllerExtensions.JsonCustomSuccess(objBll.GetWorkLogListByProjectFromTo(UserId, ProjectId, FromDate, tdate.ToShortDateString()));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/WorkLogApi/GetWorkLogListByProjectFromTo => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }
    }
}
