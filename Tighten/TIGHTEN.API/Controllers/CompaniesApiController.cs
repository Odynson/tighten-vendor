﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.MODEL;
using System.Web.Mvc;
using System.Web;
using TIGHTEN.UTILITIES;
using TIGHTEN.BUSINESS;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Configuration;

namespace TIGHTEN.API
{
    public class CompaniesApiController : ApiController
    {

        Companies objBLL;

        /// <summary>
        /// This function Updates the Company Logo
        /// </summary>
        /// <returns>It returns Success or Error</returns>
        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/CompaniesAPI/UploadLogo")]
        public JsonResponse UploadProfile()
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }

                var httpPostedFile = HttpContext.Current.Request.Files["UploadedProfile"];
                int CompanyId = Convert.ToInt32(HttpContext.Current.Request.Params["Companyid"]);
                string UserId = Convert.ToString(HttpContext.Current.Request.Params["UserId"]);

                // Validate the uploaded image(optional)

                var fileName = httpPostedFile.FileName;
                string strExtension = Path.GetExtension(fileName).ToLower();
                if (strExtension == ".jpg" || strExtension == ".png" || strExtension == ".jpeg" || strExtension == ".bmp")
                {
                    objBLL = new Companies();
                    Guid guid = Guid.NewGuid();
                    string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;
                    // Get the complete file path
                    //string path=ConfigurationManager.AppSettings["ImageServerPath"].ToString(); 
                    //path = path+"/TIGHTEN-WEB/Uploads/CompanyLogos";
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/CompanyLogos"), FinalFilename);
                    //var fileSavePath = Path.Combine(path, FinalFilename);

                    // Save the uploaded file to "Uploads" folder
                    httpPostedFile.SaveAs(fileSavePath);


                    var PreviousPhoto = objBLL.UpdateCompanyLogo(UserId, FinalFilename, CompanyId);
                    if (PreviousPhoto != null)
                    {
                        File.Delete(HttpContext.Current.Server.MapPath("~/Uploads/CompanyLogos/") + PreviousPhoto);

                    }

                    return ControllerExtensions.JsonSuccess("Company Logo Updated Successfully");
                }
                else
                {

                    return ControllerExtensions.JsonError("You can only upload jpg,jpeg,png and bmp Files");

                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/UploadLogo => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }
        }

        private void GenerateIcon(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(20);//(image.Width * scaleFactor);
                var newHeight = (int)(20);//(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }

        private void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(50);//(image.Width * scaleFactor);
                var newHeight = (int)(50);//(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }



        /// <summary>
        /// This function fetches/gets the CompanyDetails in which loggedin user is concerned with
        /// </summary>
        /// <param name="id">It is the unique Id of Company in Companies Table</param>
        /// <returns>It returns the Company Details in Json/Object </returns>
        public JsonResponse Get(int id)
        {
            try
            {
                objBLL = new Companies();
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetCompanyDetails(id));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);

            }
        }

        // POST api/<controller>

        public JsonResponse Post(UserModel model)
        {

            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {

            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);

            }

            return ControllerExtensions.JsonSuccess();

        }



        /// <summary>
        /// This function Updates the Company Details
        /// </summary>
        /// <param name="mod">it is Model which consists the updated values for all fields like companyname, companyemail etc</param>
        /// <returns>It returns Success or Error</returns>
        public JsonResponse Put(NewCompanyModel mod)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {
                objBLL = new Companies();
                objBLL.EditCompanyDetails(mod.UserId, mod.Company);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);
            }

            return ControllerExtensions.JsonSuccess("Company Details Updated Successfully");
        }

        /// <summary>
        /// This function is used to remove/clear the Company Logo
        /// </summary>
        /// <param name="id">It is the unique Id of Company</param>
        /// <returns>It returns Success or Error</returns>
        public JsonResponse Delete(int id)
        {

            try
            {
                objBLL = new Companies();
                objBLL.ClearCompanyLogo(id);

            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);

            }

            return ControllerExtensions.JsonSuccess("Company Logo Cleared Successfully");
        }


        /// <summary>
        /// This function Updates the Company Name
        /// </summary>
        /// <param name="mod">it is Model which consists the updated values for all fields like companyname, companyemail etc</param>
        /// <returns>It returns Success or Error</returns>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/CompaniesApi/UpdateCompanyName")]
        public JsonResponse UpdateCompanyName(NewCompanyModel mod)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBLL = new Companies();
                objBLL.UpdateCompanyName(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/UpdateCompanyName => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);
            }

            return ControllerExtensions.JsonSuccess("Company Name Updated Successfully");
        }


        /// <summary>
        /// This function Updates the Company Web Address
        /// </summary>
        /// <param name="mod">it is Model which consists the updated values for all fields like companyname, companyemail etc</param>
        /// <returns>It returns Success or Error</returns>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/CompaniesApi/UpdateCompanyWebAddress")]
        public JsonResponse UpdateCompanyWebAddress(NewCompanyModel mod)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBLL = new Companies();
                objBLL.UpdateCompanyWebAddress(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/UpdateCompanyWebAddress => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);
            }

            return ControllerExtensions.JsonSuccess("Company Web Address Updated Successfully");
        }

        /// <summary>
        /// This function Updates the Company Phone No
        /// </summary>
        /// <param name="mod">it is Model which consists the updated values for all fields like companyname, companyemail etc</param>
        /// <returns>It returns Success or Error</returns>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/CompaniesApi/UpdateCompanyPhoneNo")]
        public JsonResponse UpdateCompanyPhoneNo(NewCompanyModel mod)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBLL = new Companies();
                objBLL.UpdateCompanyPhoneNo(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/UpdateCompanyPhoneNo => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);
            }

            return ControllerExtensions.JsonSuccess("Company Phone Number Updated Successfully");
        }

        /// <summary>
        /// This function Updates the Company Fax
        /// </summary>
        /// <param name="mod">it is Model which consists the updated values for all fields like companyname, companyemail etc</param>
        /// <returns>It returns Success or Error</returns>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/CompaniesApi/UpdateCompanyFax")]
        public JsonResponse UpdateCompanyFax(NewCompanyModel mod)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBLL = new Companies();
                objBLL.UpdateCompanyFax(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/UpdateCompanyFax => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);
            }

            return ControllerExtensions.JsonSuccess("Company Fax Updated Successfully");
        }

        /// <summary>
        /// This function Updates the Company Email
        /// </summary>
        /// <param name="mod">it is Model which consists the updated values for all fields like companyname, companyemail etc</param>
        /// <returns>It returns Success or Error</returns>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/CompaniesApi/UpdateCompanyEmail")]
        public JsonResponse UpdateCompanyEmail(NewCompanyModel mod)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBLL = new Companies();
                objBLL.UpdateCompanyEmail(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/UpdateCompanyEmail => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);
            }

            return ControllerExtensions.JsonSuccess("Company Email Updated Successfully");
        }

        /// <summary>
        /// This function Updates the Company Address
        /// </summary>
        /// <param name="mod">it is Model which consists the updated values for all fields like companyname, companyemail etc</param>
        /// <returns>It returns Success or Error</returns>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/CompaniesApi/UpdateCompanyAddress")]
        public JsonResponse UpdateCompanyAddress(NewCompanyModel mod)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBLL = new Companies();
                objBLL.UpdateCompanyAddress(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/UpdateCompanyAddress => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);
            }

            return ControllerExtensions.JsonSuccess("Company Address Updated Successfully");
        }


        /// <summary>
        /// This function Updates the Company About Organization
        /// </summary>
        /// <param name="mod">it is Model which consists the updated values for all fields like companyname, companyemail etc</param>
        /// <returns>It returns Success or Error</returns>
        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/CompaniesApi/UpdateCompanyDescription")]
        public JsonResponse UpdateCompanyDescription(NewCompanyModel mod)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBLL = new Companies();
                objBLL.UpdateCompanyDescription(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/CompaniesAPI/UpdateCompanyDescription => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.Message);
            }

            return ControllerExtensions.JsonSuccess("About Organization Updated Successfully");
        }





    }
}