﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
    public class ProjectFeedbackApiController : ApiController
    {

        ProjectFeedback objBll;

        /// <summary>
        /// Getting User's projects in which user is Member 
        /// </summary>
        /// <returns>It returns User's Projects in it as an array</returns>
        /// 
        [Route("api/ProjectFeedbackApi/{ProjectId}/{UserId}/{RoleId}")]
        public JsonResponse Get(int ProjectId,string UserId,int RoleId)
        {
            try
            {
                objBll = new ProjectFeedback();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectByProjectId(ProjectId, UserId, RoleId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectFeedbackApi/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/ProjectFeedbackApi/GetUserRolePriority/{ProjectId}/{UserId}/{RoleId}")]
        public JsonResponse GetUserRolePriority(int ProjectId, string UserId, int RoleId)
        {
            try
            {
                objBll = new ProjectFeedback();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserRolePriority(ProjectId, UserId, RoleId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectFeedbackApi/GetUserRolePriority => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [Route("api/ProjectFeedbackApi")]
        public JsonResponse Post(MODEL.ProjectFeedbackModel.ProjectFeedback model)
        {
            try
            {
                objBll = new ProjectFeedback();
                objBll.AddUpdateFeedback(model);
                //return ControllerExtensions.JsonCustomSuccess(objBll.AddUpdateFeedback(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectFeedbackApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("Feedback is submitted successfully");
        }


        [HttpPost]
        [Route("api/ProjectFeedbackApi/SaveProjectRemarks")]
        public JsonResponse SaveProjectRemarks(MODEL.ProjectFeedbackModel.ProjectFeedback model)
        {
            try
            {
                objBll = new ProjectFeedback();
                objBll.SaveProjectRemarks(model);
                //return ControllerExtensions.JsonCustomSuccess(objBll.SaveProjectRemarks(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectFeedbackApi/SaveProjectRemarks => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("Project Remarks submitted successfully");
        }



        [HttpPost]
        [Route("api/ProjectFeedbackApi/MarkAsComplete")]
        public JsonResponse MarkAsComplete(MODEL.ProjectFeedbackModel.ProjectFeedback model)
        {
            try
            {
                objBll = new ProjectFeedback();
                //objBll.MarkAsComplete(model);
                return ControllerExtensions.JsonCustomSuccess(objBll.MarkAsComplete(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectFeedbackApi/MarkAsComplete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            //return ControllerExtensions.JsonSuccess("Project marked as complete successfully");
        }




    }
}
