﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS.Vendor;
using TIGHTEN.MODEL.Vendor.VendorAdminModel;
using TIGHTEN.MODEL.VendorAdminModal;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers.Vendor
{
    public class VendorAdminProfileApiController : ApiController
    {
        VendorAdminBLL objBll;


        [HttpPost]
        [Route("api/VendorAdminProfileApi/GetInvitationToVendor")]
        public JsonResponse GetInvitationToVendor(FilterVendorInvitationModel Modal)
        {

            try
            {

                 objBll = new VendorAdminBLL();
                 return ControllerExtensions.JsonCustomSuccess(objBll.GetInvitationToVendor(Modal));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetInvitationToVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
         
        }


        [HttpGet]
        [Route("api/VendorAdminProfileApi/GetCompanyForInvitaionDropDown/{VendorCompanyId}")]
        public JsonResponse GetCompanyForInvitaionDropDown(int VendorCompanyId)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyForInvitaionDropDown(VendorCompanyId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetCompanyForInvitaionDropDown => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        [HttpGet]
        [Route("api/VendorAdminProfileApi/GetPocsforDrops/{VendorCompanyId}")]
        public JsonResponse GetPocsforDrops(int VendorCompanyId)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetPocsforDrops(VendorCompanyId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetPocsforDrops => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        [HttpGet]
        [Route("api/VendorAdminProfileApi/GetCompanyForFilterVendorAdminProfile/{VendorCompanyId}")]
        public JsonResponse GetCompanyForFilterVendorAdminProfile(int VendorCompanyId)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyForFilterVendorAdminProfile(VendorCompanyId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetCompanyForProjectToQuoteDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        [HttpPost]
        [Route("api/VendorAdminProfileApi/GetProjectToQuoteListVendorAdminProfile")]
        public JsonResponse GetProjectToQuoteListVendorAdminProfile(ProjectToQuoteFilterModel Model )
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectToQuoteListVendorAdminProfile(Model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetProjectToQuoteListVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        [HttpGet]
        [Route("api/VendorAdminProfileApi/GetProjectByNameVendorAdminProfile/{VendorCompanyId}/{ProjectName}")]
        public JsonResponse GetProjectByNameVendorAdminProfile( int VendorCompanyId, string ProjectName )
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectByNameVendorAdminProfile(VendorCompanyId,ProjectName));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetProjectByNameVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }
        

        [HttpPost]
        [Route("api/VendorAdminProfileApi/GetProjectToQuoteViewDetailAdminProfile")]
        public JsonResponse GetProjectToQuoteViewDetailAdminProfile(ProjectToQuoteViewDetailRequestModel Model)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectToQuoteViewDetailAdminProfile(Model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetProjectToQuoteViewDetailAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/VendorAdminProfileApi/GetAwardedProjectListAdminProfile")]
        public JsonResponse GetAwardedProjectListAdminProfile(AwardedProjectListRequestModel Model)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAwardedProjectListAdminProfile(Model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetAwardedProjectListAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }
        [HttpGet]
        [Route("api/VendorAdminProfileApi/GetAwardedProjectViewDetailVendorAdminProfile/{ProjectId}/{VendorId}")]
        public JsonResponse GetAwardedProjectViewDetailVendorAdminProfile(int ProjectId, int VendorId)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAwardedProjectViewDetailVendorAdminProfile(ProjectId,VendorId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetAwardedProjectViewDetailVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }
        [HttpGet]
        [Route("api/VendorAdminProfileApi/GetProjectCompletionFeedBackVendorAdminProfile/{ProjectId}/{VendorId}")]
        public JsonResponse GetProjectCompletionFeedBackVendorAdminProfile(int ProjectId, int VendorId)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectCompletionFeedBackVendorAdminProfile(ProjectId, VendorId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetProjectCompletionFeedBackVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }
        [HttpPost]
        [Route("api/VendorAdminProfileApi/GetAllInvoiceListvendorAdminProfile")]
        public JsonResponse GetAllInvoiceListVendorAdminProfile(InvoiceFilterAdminProfileModel Model)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllInvoiceListVendorAdminProfile(Model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetAllInvoiceListVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpGet]
        [Route("api/VendorAdminProfileApi/InvoicePreviewVendorAdminProfile/{InvoiceId}")]
        public JsonResponse InvoicePreviewVendorAdminProfile(int InvoiceId)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.InvoicePreviewVendorAdminProfile(InvoiceId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/InvoicePreviewVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        #region Paging

        [HttpPost]
        [Route("api/VendorAdminProfileApi/GetAllInvoiceListvendorAdminProfileOutstanding")]
        public JsonResponse GetAllInvoiceListVendorAdminProfileOutstanding(InvoiceFilterAdminProfileModel Model)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllInvoiceListVendorAdminProfileOutstanding(Model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetAllInvoiceListVendorAdminProfileOutstanding => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/VendorAdminProfileApi/GetAllInvoiceListvendorAdminProfilePaid")]
        public JsonResponse GetAllInvoiceListVendorAdminProfileOutstandingPaid(InvoiceFilterAdminProfileModel Model)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllInvoiceListVendorAdminProfilePaid(Model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetAllInvoiceListVendorAdminProfilePaid => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/VendorAdminProfileApi/GetAllInvoiceListvendorAdminProfileRejected")]
        public JsonResponse GetAllInvoiceListVendorAdminProfileOutstandingRejected(InvoiceFilterAdminProfileModel Model)
        {

            try
            {

                objBll = new VendorAdminBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllInvoiceListVendorAdminProfileRejected(Model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorAdminProfileApi/GetAllInvoiceListVendorAdminProfileRejected => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        #endregion



    }
}
