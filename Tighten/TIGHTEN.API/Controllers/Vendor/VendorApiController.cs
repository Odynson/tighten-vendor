﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.UTILITIES;
using TIGHTEN.BUSINESS.Vendor;
using static TIGHTEN.MODEL.ProjectModel;
using TIGHTEN.MODEL.Vendor;
using System.Configuration;
using System.Web;
using System.IO;
using TIGHTEN.BUSINESS;
using System.Net.Http.Headers;
using System.Web.Http.Cors;
using TIGHTEN.MODEL;
using System.Text;
namespace TIGHTEN.API.Controllers.Vendor
{
    [EnableCors("*", "*", "*")]
    public class VendorApiController : ApiController
    {

        VendorBLL objBll;
        // Post: api/VendorApi
        [HttpGet]
        [Route("api/VendorApi/GetCompanyVendorList/{CompanyId}")]
        public JsonResponse GetCompanyVendorList(int CompanyId)
        {

            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyVendorList(CompanyId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/GetCompanyVendorList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }




        public JsonResponse post(VendorInvitationModal modal)
        {

            JsonResponse result = new JsonResponse();
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new VendorBLL();
                
                return  ControllerExtensions.JsonCustomSuccess(objBll.SaveVendor(modal, url));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/VendorApi/GetInvitaionFromCompany/")]
        public JsonResponse GetInvitaionFromCompany(VendorInvitationsModal Modal)
        {

            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInvitaionFromCompany(Modal));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/GetInvitaionFromCompany => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/VendorApi/AcceptRejectInvitaionAsVendor/")]
        public JsonResponse AcceptRejectInvitaionAsVendor(VendorInvitationsModal modal)
        {

            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.AcceptRejectInvitaionAsVendor(modal));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/AcceptRejectInvitaionAsVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/VendorApi/GetMyVendor/")]
        public JsonResponse GetMyVendor(VendorFilterModal model)

        {
            try
            {
                  objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetMyVendor(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorApi/GetMyVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError(CustomException);
            }


        }
        [HttpGet]
        [Route("api/VendorApi/GetProjectAndVendorDetail/{ProjectId}/{CompanyId}")]
        public JsonResponse GetProjectAndVendorDetail(int ProjectId, int CompanyId)
        {

            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectAndVendorDetail(ProjectId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path:api/VendorApi/GetProjectAndVendorDetail => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/VendorApi/GetProjectTOQuote/")]
        public JsonResponse GetProjectTOQuote(vendorModel Modal)
        {

            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectTOQuote(Modal));
            }
            catch (Exception ex)
            {
                string CustomException = "Path:api/VendorApi/GetProjectTOQuote => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");

            }


        }
        [HttpPost]
        [Route("api/VendorApi/GetProjectDetailForVendor/")]
        public JsonResponse GetProjectDetailForVendor(RequestProjectDetailforVendorModal Modal )
        {

            try
            {

                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectDetailForVendor(Modal));

            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/VendorApi/GetProjectDetailForVendor => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/VendorApi/SaveQuoteAcceptByvendor")]
        public JsonResponse SaveQuoteAcceptByvendor(VendorQuoteModal modal)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.SaveQuoteAcceptByvendor(modal));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/SaveQuoteAcceptByvendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }



        [HttpGet]
        [Route("api/VendorApi/GetVendorResourceRole")]

        public JsonResponse GetVendorResourceRole()
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorResourceRole());
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/VendorApi/GetVendorResourceRole => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }



        }

        [HttpPost]
        [Route("api/VendorApi/GetFiledownload")]
        public HttpResponseMessage GetFiledownload(newProjectDocument modal)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            HttpResponseMessage Response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                objBll = new VendorBLL();

                var path = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/" + modal.FilePath);


                if (File.Exists(path)) //Not found then throw Exception

                {


                    //S2:Read File as Byte Array
                    byte[] fileData = File.ReadAllBytes(path);

                    if (fileData == null)
                        throw new HttpResponseException(HttpStatusCode.NotFound);
                    //S3:Set Response contents and MediaTypeHeaderValue
                    Response.Content = new ByteArrayContent(fileData);
                    Response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
                }
                else
                {


                }


            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetFiledownload => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

            }
            return Response;

        }


        [HttpPost]
        [Route("api/VendorApi/EditSaveQuoteAcceptByvendor")]
        public JsonResponse EditSaveQuoteAcceptByvendor(VendorQuoteModal modal)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.EditSaveQuoteAcceptByvendor(modal));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorApi/EditSaveQuoteAcceptByvendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        [HttpPost]
        [Route("api/VendorApi/RejectQuoteByVendor")]
        public JsonResponse RejectQuoteByVendor(VendorInvitationsModal modal)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.RejectQuoteByVendor(modal));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/RejectQuoteByVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpGet]
        [Route("api/VendorApi/GetAwardedProjectViewDetail/{ProjectId}/{VendorId}")]
        public JsonResponse GetAwardedProjectViewDetail(int ProjectId, int VendorId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAwardedProjectViewDetail(ProjectId, VendorId));

            }
            catch (Exception ex)
            {


                string CustomException = "Path : api/VendorApi/GetAwardedProjectViewDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpGet]
        [Route("api/VendorApi/GetSpecificProjectForInvoiceRaise/{ProjectId}/{VendorId}")]
        public JsonResponse GetSpecificProjectForInvoiceRaise(int ProjectId, int VendorId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetSpecificProjectForInvoiceRaise(ProjectId, VendorId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/GetSpecificProjectForInvoiceRaise => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");

            }
        }

        [HttpGet]
        [Route("api/VendorApi/GetMilestoneforProject/{Id}")]
        public JsonResponse GetMilestoneforProject(int Id)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetMilestoneforProject(Id));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/GetMilestoneforProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }
        [HttpGet]
        [Route("api/VendorApi/ProjectForInvoiceDrop/{CompanyId}")]
        public JsonResponse ProjectForInvoiceDrop(int CompanyId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.ProjectForInvoiceDrop(CompanyId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/ProjectForInvoiceDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }

        [HttpGet]
        [Route("api/VendorApi/GetCompanyVendorDropList/{CompanyId}")]
        public JsonResponse GetCompanyVendorDropList(int CompanyId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyVendorDropList(CompanyId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/ProjectForInvoiceDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }

        [HttpGet]
        [Route("api/VendorApi/GetCompanyForDrop/{CompanyId}")]
        public JsonResponse GetCompanyForDrop(int CompanyId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyForDrop(CompanyId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/ProjectForInvoiceDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");

            }
        }
        [HttpPost]
        [Route("api/VendorApi/GetVendorName/")]
        public JsonResponse GetVendorName(VendorFilterModal Modal)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorName(Modal));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/ProjectForInvoiceDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpGet]
        [Route("api/VendorApi/GetVendorDetail/{CompanyId}/{VendorId}")]
        public JsonResponse GetVendorDetail(int CompanyId, int VendorId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorDetail(CompanyId, VendorId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorApi/GetVendorDetail/ => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        [HttpGet]
        [Route("api/VendorApi/GetVendorCompanyPoc/{VendorCompanyId}")]
        public JsonResponse GetVendorCompanyPoc(int VendorCompanyId )
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorCompanyPoc(VendorCompanyId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorApi/GetVendorCompanyVendor/ => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        [HttpGet]
        [Route("api/VendorApi/GetSelectedVendorDetail/{VendorCompanyId}")]
        public JsonResponse GetSelectedVendorDetail( int VendorCompanyId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetSelectedVendorDetail(VendorCompanyId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorApi/GetSelectedVendorDetail/ => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        /// <summary>
        /// This function deletes the Particular Vendor Company of Company
        /// </summary>
        /// <param name="id">It is the Unique Id</param>
        /// <returns>It returns response whether it is success or error</returns>

        public JsonResponse Delete(int id)
        {
            try
            {
                objBll = new VendorBLL();
                string msg = objBll.deleteVendorDetail(id);
                if (msg == "VendorDetailDeletedSuccessfully")
                {
                    return ControllerExtensions.JsonSuccess(msg);
                }
                else
                {
                    return ControllerExtensions.JsonError(msg);
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorApi/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }


        [HttpGet]
        [Route("api/VendorApi/getSelectedVendorDetailById/{CVId}")]
        public JsonResponse getSelectedVendorDetailById(int CVId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetSelectedVendorDetailById(CVId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorApi/GetSelectedVendorDetail/ => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"+ CustomException);
            }

        }

        [HttpPost]
        [Route("api/VendorApi/GetVendorCompanyName/")]
        public JsonResponse GetVendorCompanyName(VendorFilterModal Modal)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorCompanyName(Modal));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/ProjectForInvoiceDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/VendorApi/updateVendor/")]
        public JsonResponse updateVendor(VendorInvitationModal modal)
        {

            JsonResponse result = new JsonResponse();
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new VendorBLL();

                return ControllerExtensions.JsonCustomSuccess(objBll.UpdateVendor(modal, url));
               

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !"+ CustomException);
            }

        }
        [HttpGet]
        [Route("api/VendorApi/ValidationEmailForPOC/{pocEmail}/{CompanyId}")]
        public JsonResponse ValidationEmailForPOC(string pocEmail,int CompanyId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.ValidationEmailForPOC(pocEmail, CompanyId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorApi/ValidationEmailForPOC/ => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        [HttpGet]
        [Route("api/VendorApi/GetGeneralCompanyListOfVendor/{CompanyId}")]
        public JsonResponse GetGeneralCompanyListOfVendor(int CompanyId)
        {

            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetGeneralCompanyListOfVendor(CompanyId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/GetGeneralCompanyListOfVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }


        #region InviteNewUserFunctionality

        [HttpGet]
        [Route("api/VendorApi/GetNewUserRole/{CompanyId}")]

        public JsonResponse GetNewUserRole(int CompanyId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetNewUserRole(CompanyId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/VendorApi/GetVendorResourceRole => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }



        }

        [HttpPost]
        [Route("api/VendorApi/InviteUser/")]
        public JsonResponse Post(UserInvitationModal modal)
        {

            JsonResponse result = new JsonResponse();
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new VendorBLL();
                return  ControllerExtensions.JsonCustomSuccess(objBll.InviteUser(modal, url));



            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                StringBuilder UserRoleIdList = new StringBuilder();
                StringBuilder UserRoleNameList = new StringBuilder();
                for (int i = 0; i < modal.UserRoleList.Count; i++)
                {
                    UserRoleIdList.Append(modal.UserRoleList[i].RoleId).Append(",");
                    UserRoleNameList.Append(modal.UserRoleList[i].Name).Append(",");

                }
                return ControllerExtensions.JsonError("Some Error Occured !");
                //return ControllerExtensions.JsonError(CustomException+ " UserId: " +modal.UserId+" FirstName: "+modal.FirstName+" LastName: "+modal.LastName+" CompanyName: "+modal.CompanyName+" Email: "+modal.Email+" CompanyId: "+modal.CompanyId+" JobProfileId: "+modal.JobProfileId+" EmailConfirmId: "+ EmailUtility.GetUniqueKey(8)+ " UserRoleIdList: " + UserRoleIdList);
            }

        }

        [HttpGet]
        [Route("api/VendorApi/GetAllNewUsers/{CompanyId}/{UserId}")]
        public JsonResponse GetAllNewUsers(int CompanyId,string UserId)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllNewUser(CompanyId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorApi/GetVendorDetail/ => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpGet]
        [Route("api/VendorApi/GetCompanyEmailForInviteNewUser/{CompanyName}")]
        public JsonResponse GetCompanyEmailForInviteNewUser(string CompanyName)
        {
            try
            {
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyEmailForInviteNewUser(CompanyName.Trim()));
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/VendorApi/GetCompanyEmailForInviteNewUser => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }



        }


        [HttpPost]
        [Route("api/VendorApi/UpdateExistingInternalUsers/")]
        public JsonResponse UpdateExistingInternalUsers(UserInvitationModal modal)
        {

            JsonResponse result = new JsonResponse();
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.UpdateExistingInternalUsers(modal, url));



            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpGet]
        [Route("api/VendorApi/GetCompaniesForVenderCompanyName/{Name}")]
        public JsonResponse GetCompaniesForVenderCompanyName(string Name="")
        {

            JsonResponse result = new JsonResponse();
            try
            {
                //string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new VendorBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompaniesForVenderCompanyName(Name));



            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/GetCompaniesForVenderCompanyName => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }
        #endregion
    }
}
