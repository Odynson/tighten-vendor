﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS.Vendor;
using TIGHTEN.MODEL.VendorGeneralProfile;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers.Vendor
{
    public class VendorGeneralProfileApiController : ApiController
    {
        // GET: api/VendorGeneralProfileApi
        VendorGeneralProfileBLL ObjBLL;



        [HttpPost]
        [Route("api/VendorGeneralProfileApi/GetMyProject")]
        public JsonResponse GetMyProject(FilterProjectListVendorGeneralProfile Model)
        {
            try
            {
                ObjBLL = new VendorGeneralProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(ObjBLL.GetMyProject(Model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorGeneralProfileApiController/GetMyProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpPost]
        [Route("api/VendorGeneralProfileApi/GetProjectNameForFilter")]
        public JsonResponse GetProjectNameForFilter(FilterProjectListVendorGeneralProfile Model)
        {
            try
            {
                ObjBLL = new VendorGeneralProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(ObjBLL.GetProjectNameForFilter(Model));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorGeneralProfileApi/GetProjectNameForFilter => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/VendorGeneralProfileApi/GetWorkNoteForVendorGeneralUser/{ProjectId}/{VendorGeneralUserId}")]

        public JsonResponse GetWorkNoteForVendorGeneralUser(int ProjectId, string VendorGeneralUserId)
        {
            try
            {
                ObjBLL = new VendorGeneralProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(ObjBLL.GetWorkNoteForVendorGeneralUser(ProjectId, VendorGeneralUserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorGeneralProfileApi/GetWorkNoteForVendorGeneralUser => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpPost]
        [Route("api/VendorGeneralProfileApi/InsertNoteVendorGeneralUser")]
        public JsonResponse InsertNoteVendorGeneralUser(WorkDiaryModel Model)
        {

            try
            {
                ObjBLL = new VendorGeneralProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(ObjBLL.InsertNoteVendorGeneralUser(Model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorGeneralProfileApi/InsertNoteVendorGeneralUser => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");


            }


        }

        [HttpGet]
        [Route("api/VendorGeneralProfileApi/GetMyProjectViewDetailVendorGeneralProfile/{ProjectId}/{UserId}")]

        public JsonResponse GetMyProjectViewDetailVendorGeneralProfile(int ProjectId, string UserId)
        {
              try
              {
                ObjBLL = new VendorGeneralProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(ObjBLL.GetMyProjectViewDetailVendorGeneralProfile(ProjectId, UserId));
            }
              catch (Exception ex)
              {

                string CustomException = "Path : api/VendorGeneralProfileApi/GetMyProjectViewDetailVendorGeneralProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

    }
}
