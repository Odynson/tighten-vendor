﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS.Vendor;
using TIGHTEN.MODEL.Vendor.VendorAdminModel;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers.Vendor
{
    public class UserManagementVendorAdminProfileApiController : ApiController
    {
        UserManagementVendorAdminProfileBLL objBll;

        [HttpGet]
        [Route("api/UserManagementVendorAdminProfileApi/GetUserAndResourceVendorAdminProfile/{VendorCompanyId}")]
        public JsonResponse GetUserAndResourceVendorAdminProfile(int VendorCompanyId)
        {
            try
            {
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserAndResourceVendorAdminProfile(VendorCompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/GetUserAndResourceVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }
        [HttpGet]
        [Route("api/UserManagementVendorAdminProfileApi/GetExistingUserRoleVendorAdminProfile/{VendorId}")]
        public JsonResponse GetExistingUserRoleVendorAdminProfile(int VendorId)
        {
            try
            {
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetExistingUserRoleVendorAdminProfile(VendorId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/GetExistingUserRoleVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpPost]
        [Route("api/UserManagementVendorAdminProfileApi/GetResourceVendorAdminProfile")]
        public JsonResponse GetResourceVendorAdminProfile(ResourceRequestModel Model)
        {
            try
            {
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetResourceVendorAdminProfile(Model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/GetResourceVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpPost]
        [Route("api/UserManagementVendorAdminProfileApi/SaveExistingUserAdminProfile")]
        public JsonResponse SaveExistingUserAdminProfile(SaveExistingUserModel Model)
        {
            try
            {
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.SaveExistingUserAdminProfile(Model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/GetResourceVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpPost]
        [Route("api/UserManagementVendorAdminProfileApi/InviteResourceAsVendor")]
        public JsonResponse InviteResourceAsVendor(SaveResourceAsVendorRequestModel Model)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.InviteResourceAsVendor(Model,url));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/InviteResourceAsVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpPost]
        [Route("api/UserManagementVendorAdminProfileApi/ActiveInactiveUserVendorAdminProfile")]
        public JsonResponse ActiveInactiveUserVendorAdminProfile(SaveExistingUserModel Model)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.ActiveInactiveUserVendorAdminProfile(Model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/InviteResourceAsVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }


        [HttpGet]
        [Route("api/UserManagementVendorAdminProfileApi/GetUserForVendorAdminProfile/{VendorCompanyId}/{PageIndex}/{PageSizeSelected}")]
        public JsonResponse GetUserForVendorAdminProfile(int VendorCompanyId,int PageIndex,int PageSizeSelected)
        {
            try
            {
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserForVendorAdminProfile(VendorCompanyId,PageIndex,PageSizeSelected));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/GetUserForVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpGet]
        [Route("api/UserManagementVendorAdminProfileApi/GetResourcesForVendorAdminProfile/{VendorCompanyId}/{PageIndex}/{PageSizeSelected}")]
        public JsonResponse GetResourcesForVendorAdminProfile(int VendorCompanyId, int PageIndex, int PageSizeSelected)
        {
            try
            {
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetResourcesForVendorAdminProfile(VendorCompanyId, PageIndex, PageSizeSelected));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/GetResourcesForVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        #region invitenewuserlist
        [HttpGet]
        [Route("api/UserManagementVendorAdminProfileApi/GetAllExistingInternalUsers/{CompanyId}/{PageIndex}/{PageSizeSelected}")]
        public JsonResponse GetAllExistingInternalUsers(int CompanyId, int PageIndex, int PageSizeSelected)
        {
            try
            {
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllExistingInternalUsers(CompanyId, PageIndex, PageSizeSelected));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/GetAllInternalUsers => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpPost]
        [Route("api/UserManagementVendorAdminProfileApi/ActiveInactiveInternalUsersVendorAdminProfile")]
        public JsonResponse ActiveInactiveInternalUsersVendorAdminProfile(MODEL.Vendor.UserInvitationModal Model)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                objBll = new UserManagementVendorAdminProfileBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.ActiveInactiveInternalUsersVendorAdminProfile(Model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserManagementVendorAdminProfileApi/InviteResourceAsVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }
        #endregion

    }
}
