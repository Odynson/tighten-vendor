﻿using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;



namespace TIGHTEN.API
{
    public class FollowerApiController : ApiController
    {

        Followers obj;
        // GET api/<controller>/5
        //public HttpResponseMessage Get()
        //{
        //    Followers obj = new Followers();

        //    return Request.CreateResponse(HttpStatusCode.OK, obj.getMyFollowedToDos(User.Identity.GetUserId()));
        //}


        /// <summary>
        /// Getting Followers of a particular Todo
        /// </summary>
        /// <param name="id">It is the Unique Id of Todo</param>
        /// <returns>returns the object with followers of todo</returns>
        public JsonResponse Get(int id)
        {
            try
            {
                obj = new Followers();
                return ControllerExtensions.JsonCustomSuccess(obj.getFollowers(id));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FollowerAPI/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }
        }


        // GET api/<controller>/5
        [HttpGet]
        [Route("api/FollowerAPI/GetUsers/{id}")]
        public HttpResponseMessage GetUsers(int id, string userId)
        {
            obj = new Followers();
            return Request.CreateResponse(HttpStatusCode.OK, obj.getUsers(id, userId));
        }


        // POST api/<controller>
        public void Post(TodoModel.Followers objk, string userId)
        {

            obj = new Followers();
            obj.saveFollower(objk.ToDoId, objk.UserId, userId);
        }



        /// <summary>
        /// Updating Followers for a Particular Todo
        /// </summary>
        /// <param name="obj">It is Follower Update Model which consists the TodoId and Followers User Id's as Array</param>
        public JsonResponse Put(FollowerModel.NewFollowerUpdateModel model)
        {
            try
            {
                obj = new Followers();
                obj.UpdateFollowers(model.UserId, model.FollowerUpdateModel, model.CompanyId);

                return ControllerExtensions.JsonSuccess("Followers Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FollowerAPI/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            obj = new Followers();
            obj.deleteFollower(id);
        }
    }
}