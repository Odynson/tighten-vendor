﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class PaymentHistoryApiController : ApiController
    {
        PaymentHistory objBll;

        [Route("api/PaymentHistoryApi/{UserId}/{isFreelancer}/{TeamId}/{CompanyId}/{subscriptionType}/{ProjectId}/{FreelancerUserId}")]
        public JsonResponse Get(string UserId, bool isFreelancer, int TeamId, int CompanyId,string subscriptionType, int ProjectId, string FreelancerUserId)
        {
            try
            {
                objBll = new PaymentHistory();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetPaymentHistory(UserId, isFreelancer, TeamId, CompanyId, subscriptionType, ProjectId, FreelancerUserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/PaymentHistoryApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/PaymentHistoryApi/GetFreelancersForSelectedProject/{ProjectId}/{CompanyId}")]
        public JsonResponse GetFreelancersForSelectedProject(int ProjectId, int CompanyId)
        {
            try
            {
                objBll = new PaymentHistory();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetFreelancersForSelectedProject( ProjectId,  CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/PaymentHistoryApi/GetFreelancersForSelectedProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




    }
}
