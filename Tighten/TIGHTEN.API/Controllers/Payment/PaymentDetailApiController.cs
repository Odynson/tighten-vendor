﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class PaymentDetailApiController : ApiController
    {
        PaymentDetail objBll;

        [Route("api/PaymentDetailApi/{CompanyId}")]
        public JsonResponse Get( int CompanyId)
        {
            try
            {
                objBll = new PaymentDetail();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetPaymentDetail(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/PaymentDetailApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpPost]
        [Route("api/PaymentDetailApi")]
        public JsonResponse Post(MODEL.PaymentDetailModel model)
        {
            try
            {
                objBll = new PaymentDetail();
                return ControllerExtensions.JsonCustomSuccess(objBll.insertPaymentDetail(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/PaymentDetailApi/insertPaymentDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpPut]
        [Route("api/PaymentDetailApi")]
        public JsonResponse Put(MODEL.PaymentDetailModel model)
        {
            try
            {
                objBll = new PaymentDetail();
                return ControllerExtensions.JsonCustomSuccess(objBll.updatePaymentDetail(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/PaymentDetailApi/updatePaymentDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpDelete]
        [Route("api/PaymentDetailApi/{Id}")]
        public JsonResponse Delete(int Id)
        {
            try
            {
                objBll = new PaymentDetail();
                objBll.deletePaymentDetail(Id);
                return ControllerExtensions.JsonSuccess("Payment Detail Deleted Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/PaymentDetailApi/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }






    }
}
