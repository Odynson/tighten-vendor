﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.AspNet.Identity.Owin;
//using Microsoft.Owin.Security;
//using Microsoft.Owin.Security.Cookies;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class subscriptionController : ApiController
    {
        SubscriptionBusiness objBLL = new SubscriptionBusiness();

        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/subscriptionsAPI/insertSubscription")]
        public JsonResponse AddSubscription(PaymentModel model)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.AddSubscription(model), "Success");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/subscriptionsAPI/insertSubscription => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }



        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/subscriptionsAPI/SubsctriptionPayment")]
        public JsonResponse SubsctriptionPayment(SubsctriptionPaymentModel model)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.SubsctriptionPayment(model), "Success");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/subscriptionsAPI/SubsctriptionPayment => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }




        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/subscriptionsAPI/updateSubscription")]
        public JsonResponse UpdateSubscription(SubscriptionModel model)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.UpdateSubscription(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/subscriptionsAPI/updateSubscription => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [System.Web.Http.HttpGet] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/subscriptionsAPI/GetUserSubscriptions/{UserId}/{CompanyId}")]
        public JsonResponse GetUserSubscriptions(string UserId, int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetUserSubscriptions(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/subscriptionsAPI/GetUserSubscriptions => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [System.Web.Http.HttpGet] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/subscriptionsAPI/GetUserActiveSubscriptions/{UserId}/{CompanyId}")]
        public JsonResponse GetUserActiveSubscriptions(string UserId, int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetUserActiveSubscriptions(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/subscriptionsAPI/GetUserActiveSubscriptions => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }
    }
}