﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.AspNet.Identity.Owin;
//using Microsoft.Owin.Security;
//using Microsoft.Owin.Security.Cookies;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class transactionsController : ApiController
    {
        TransactionBusiness objBLL = new TransactionBusiness();

        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/transactionsAPI/insertTransactions")]
        public JsonResponse insertTransactions(TransactionModel model)
        {
            try
            {
                model.TransactionDate = DateTime.Now;
                return ControllerExtensions.JsonCustomSuccess(objBLL.AddTransaction(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/transactionsAPI/insertTransactions => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/transactionsAPI/updateTransactions")]
        public JsonResponse updateTransactions(TransactionModel model)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.UpdateTransaction(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/transactionsAPI/updateTransactions => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [System.Web.Http.HttpGet] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/transactionsAPI/GetTransactions/{SubscriptionID}")]
        public JsonResponse GetTransactions(int SubscriptionID)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetTransactions(SubscriptionID));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/transactionsAPI/GetTransactions => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [System.Web.Http.HttpGet] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/transactionsAPI/GetTransactionSingle/{SubscriptionID}")]
        public JsonResponse GetTransactionSingleOrDefault(int SubscriptionID)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetTransactionSingleOrDefault(SubscriptionID));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/transactionsAPI/GetTransactionSingle => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }
    }
}