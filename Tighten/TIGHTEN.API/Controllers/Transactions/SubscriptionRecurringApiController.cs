﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;
using TIGHTEN.MODEL;

namespace TIGHTEN.API.Controllers
{
    public class SubscriptionRecurringApiController : ApiController
    {
        SubscriptionRecurring objBll;

        [HttpGet]
        public JsonResponse RecurringPayment()
        {
            try
            {
                objBll = new SubscriptionRecurring();
                return ControllerExtensions.JsonCustomSuccess(objBll.RecurringPayment());
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/RecurringPayment => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



    }
}
