﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
namespace TIGHTEN.API
{
    public class TeamDetailApiController : ApiController
    {
        Team objBll;
        [HttpGet]
        [Route("api/teamdetailapi/GetTeamDetail/{teamId}")]
        public JsonResponse GetTeamDetail(int teamId)
        {
            try
           {
                objBll = new Team();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTeamWithDesignation(teamId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamdetailapi/GetTeamDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured ! ");

            }

        }
    }
}