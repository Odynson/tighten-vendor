﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
    public class SubToDoApiController : ApiController
    {
        SubToDos objBll = new SubToDos();
        // GET api/<controller>
        public HttpResponseMessage Get(int id, string userId)
        {

            return Request.CreateResponse(HttpStatusCode.OK, objBll.getSubTodos(userId, id));
        }



        // POST api/<controller>
        public JsonResponse Post(TodoModel.SubToDo obj, string userId)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {

                objBll.saveSubTodo(userId, obj);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/SubToDoApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }

            return ControllerExtensions.JsonSuccess();

        }

        // PUT api/<controller>/5
        public JsonResponse Put(TodoModel.SubToDo obj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {

                objBll.updateSubTodo(obj);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/SubToDoApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }

            return ControllerExtensions.JsonSuccess();


        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            try
            {
                objBll.deleteSubTodo(id);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/SubToDoApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
            }
        }


    }
}