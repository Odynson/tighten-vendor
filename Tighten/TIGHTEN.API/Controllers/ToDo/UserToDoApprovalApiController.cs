﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class UserToDoApprovalApiController : ApiController
    {
        UserToDoApproval objBll;

        [HttpPost]
        /// <summary>
        /// It gets MyTodos
        /// </summary>
        /// <returns>returns Success or Error</returns>
        /// 
        [Route("api/userToDoApproval/GetToDo")]
        public JsonResponse GetToDo(TodoApprovalSearch search)
        {
            try
            {
                objBll = new UserToDoApproval();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTodos(search));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/userToDoApproval/GetToDo => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [Route("api/userToDoApproval/getUserProjects/{CompanyId}/{UserId}")]
        public JsonResponse getCompanyUsers(int CompanyId, string UserId)
        {
            try
            {
                objBll = new UserToDoApproval();
                return ControllerExtensions.JsonCustomSuccess(objBll.UserProjects(CompanyId, UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/userToDoApproval/getUserProjects => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        // DELETE api/<controller>/5
        [HttpPost]
        [Route("api/userToDoApproval/DeleteUserLoggedTimeSlot/{Id}")]
        public JsonResponse DeleteUserLoggedTimeSlot(int Id)
        {
            try
            {
                /*Delete ToDosDetail from todologged table */
                objBll = new UserToDoApproval();
                return ControllerExtensions.JsonCustomSuccess(objBll.DeleteUserLoggedTimeSlot(Id));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/userToDoApproval/DeleteUserLoggedTimeSlot => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
           // return ControllerExtensions.JsonSuccess("Todo Deleted Successfully !");
        }

        [HttpPost]
        [Route("api/userToDoApproval/UpdateUserLoggedTimeSlot/{UserId}/{Id}/{Minutes}")]
        public JsonResponse UpdateUserLoggedTimeSlot(string UserId,int Id, int Minutes)
        {
            try
            {
                /*Delete ToDosDetail from todologged table */
                objBll = new UserToDoApproval();
                return ControllerExtensions.JsonCustomSuccess(objBll.UpdateUserLoggedTimeSlot(UserId,Id, Minutes));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/userToDoApproval/UpdateUserLoggedTimeSlot => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/userToDoApproval/getUserProjectsForAllCompanies/{UserId}/{CompanyId}")]
        public JsonResponse getUserProjectsForAllCompanies(string UserId,int CompanyId)
        {
            try
            {
                objBll = new UserToDoApproval();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserProjectsForAllCompanies(UserId,CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/userToDoApproval/getUserProjectsForAllCompanies => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpGet]
        [Route("api/userToDoApproval/getUserCompanies/{UserId}")]
        public JsonResponse getUserCompanies(string UserId)
        {
            try
            {
                objBll = new UserToDoApproval();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserCompanies(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/userToDoApproval/getUserCompanies => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


    }
}
