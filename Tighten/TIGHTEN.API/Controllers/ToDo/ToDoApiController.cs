﻿using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Web;
using System.Configuration;

namespace TIGHTEN.API
{
    public class ToDoApiController : ApiController
    {
        ToDos objBll;


        /// <summary>
        /// It gets MyTodos
        /// </summary>
        /// <returns>returns Success or Error</returns>
        /// 
        [Route("api/ToDoApi/getMyTodos/{userId}")]
        public JsonResponse Get(string userId)
        {
            try
            {
                objBll = new ToDos();
                return ControllerExtensions.JsonCustomSuccess(objBll.getMyTodos(userId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoApi/getMyTodos => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// It fetches the Sections with todos in it as an array for a particular Project
        /// </summary>
        /// <param name="id">It is the unique project id</param>
        /// <returns>it returns the sections with todos in an object</returns>
        /// 
        [Route("api/ToDoApi/TodosWithSections/{id}/{userId}")]
        public JsonResponse Get(int id, string userId)
        {
            try
            {
                objBll = new ToDos();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllTodosWithSections(id));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoApi/TodosWithSections => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        /// <summary>
        /// It fetches the Sections  with todos in it as an array For All Projects
        /// </summary>
        /// <param name="UserId">It is the unique id of User</param>
        /// <returns>it returns the sections with todos For All Projects in an object</returns>
        /// 
        [HttpGet]
        [Route("api/ToDoApi/TodosWithSectionsForAllProjects/{CompanyId}")]
        public JsonResponse TodosWithSectionsForAllProjects(int CompanyId)
        {
            try
            {
                objBll = new ToDos();
                return ControllerExtensions.JsonCustomSuccess(objBll.TodosWithSectionsForAllProjects(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoApi/TodosWithSectionsForAllProjects => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        /// <summary>
        /// It fetches the details of a particular TOdo
        /// </summary>
        /// <param name="id">It is unique id of todo</param>
        /// <returns>returns the details of todo in json format</returns>
        [HttpGet]
        [Route("api/ToDoAPI/getTodo/{id}/{userId}")]
        public JsonResponse getTodo(int id, string userId)
        {
            try
            {
                objBll = new ToDos();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTodo(id, userId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/getTodo => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        [HttpGet]
        [Route("api/ToDoAPI/getLoggedTime/{id}/{userId}")]
        public JsonResponse getLoggedTime(int id, string userId)
        {
            try
            {
                objBll = new ToDos();
                return ControllerExtensions.JsonCustomSuccess(objBll.getLoggedTime(id, userId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/getLoggedTime => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }



        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/getCompletedToDos/{id}")]
        //public HttpResponseMessage getCompletedToDos(int id)
        //{
        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getCompletedToDos(id));
        //}

        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/getToDosInProgress")]
        //public HttpResponseMessage getToDosInProgress()
        //{
        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getToDosInProgress(User.Identity.GetUserId()));
        //}

        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/getPendingToDos")]
        //public HttpResponseMessage getPendingToDos()
        //{
        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getPendingToDos(User.Identity.GetUserId()));
        //}


        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/GetSingleToDo/{id}")]
        //public HttpResponseMessage GetSingleToDo(int id)
        //{
        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getSingleToDo(id, User.Identity.GetUserId()));
        //}


        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/GetMyUnassignedToDos")]
        //public HttpResponseMessage GetMyUnassignedToDos()
        //{
        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getMyUnassignedToDos(User.Identity.GetUserId()));
        //}


        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/GetMyAssignedToDos")]
        //public HttpResponseMessage GetMyAssignedToDos()
        //{
        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getMyAssignedToDos(User.Identity.GetUserId()));
        //}

        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/GetMyPersonalAssignedToDos")]
        //public HttpResponseMessage GetMyPersonalAssignedToDos()
        //{

        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getMyPersonalAssignedToDos(User.Identity.GetUserId()));
        //}


        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/GetMyInboxToDos")]
        //public HttpResponseMessage GetMyInboxToDos()
        //{
        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getMyInboxToDos(User.Identity.GetUserId()));
        //}

        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/GetMyInboxArchived")]
        //public HttpResponseMessage GetMyInboxArchived()
        //{
        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getMyInboxArchivedToDos(User.Identity.GetUserId()));
        //}

        //// GET api/<controller>/5
        //[HttpGet]
        //[Route("api/ToDoAPI/GetMyArchivedToDos")]
        //public HttpResponseMessage GetMyArchivedToDos()
        //{
        //    ToDos objBll = new ToDos();
        //    return Request.CreateResponse(HttpStatusCode.OK, objBll.getMyArchivedToDos(User.Identity.GetUserId()));
        //}


        // POST api/<controller>
        /* This function is called when we are saving Todo. TodoModel.ToDo is the model which is assigned the object which we have sent from
         angular. TodoModel is the parent class and ToDo is the child class
         
         */
        public JsonResponse Post(TodosModel.NewTodoAddModel obj)
        {

            string TodoId;
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new ToDos();
                TodoId = Convert.ToString(objBll.saveToDo(obj.TodoAddModel, obj.userId,obj.CompanyId));
                // THis check is for the todos which are not assigned to any user
                if (!string.IsNullOrEmpty(obj.TodoAddModel.AssigneeId))
                {
                    //await objBll.SendMailForSaveNewTodo(obj.TodoAddModel);

                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        objBll.ThreadForSendMailForSaveNewTodo(obj.TodoAddModel, obj.userId, obj.CompanyId, obj.ProjectId);
                    }
                    ));
                    childref.Start();
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess(TodoId);
        }

        // PUT api/<controller>/5

        public JsonResponse Put(TodosModel.NewTodoUpdateModel obj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new ToDos();
                string ValueForSendMailTo = objBll.editToDo(obj.TodoUpdateModel, obj.userId);

                //Creation of instance of Thread class  
                if (ValueForSendMailTo  != "TodoInProgess")
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                        //objBll.ThreadForSendMailForStartStopTodoProgress(url, obj, ValueForSendMailTo, obj.CompanyId, obj.ProjectId);
                    }
                    ));
                    childref.Start();
                }
                else
                {
                    return ControllerExtensions.JsonError("Todo is in progress");
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("Todo Updated Successfully !");
        }

        
        [HttpGet]
        [Route("api/ToDoAPI/getTodoTags/{TaskTypeIds}/{TodoId}")]
        public JsonResponse getTodoTags(string TaskTypeIds,int TodoId)
        {
            try
            {
                objBll = new ToDos();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTodoTags(TaskTypeIds,TodoId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/getTodoTags => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        // DELETE api/<controller>/5
        public JsonResponse Delete(int id)
        {
            try
            {
                /*Delete ToDos */
                objBll = new ToDos();
                objBll.deleteToDo(id);

                /*Delete ToDoComments for a Task */
                ToDoComments objBllComm = new ToDoComments();
                objBllComm.deleteAllToDoComments(id);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

            return ControllerExtensions.JsonSuccess("Todo Deleted Successfully !");
        }

        [HttpGet]
        [Route("api/todoapi/getAssigneeUsers/{userId}")]
        public HttpResponseMessage getAssigneeUsers(string userId)
        {
            ToDos objBll = new ToDos();
            return Request.CreateResponse(HttpStatusCode.OK, objBll.getAssigneeUsers(userId));
        }

        [HttpGet]
        [Route("api/ToDoAPI/getRunningTaskDetail/{userId}")]
        public JsonResponse getRunningTaskDetail(string userId)
        {
            try
            {
                objBll = new ToDos();
                return ControllerExtensions.JsonCustomSuccess(objBll.getRunningTaskDetail(userId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/getRunningTaskDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/ToDoAPI/getTodoDetailForNotification/{TodoId}")]
        public JsonResponse getTodoDetailForNotification(int TodoId)
        {
            try
            {
                objBll = new ToDos();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTodoDetailForNotification(TodoId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/getTodoDetailForNotification => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }

        }


        [HttpGet]
        [Route("api/ToDoAPI/getTaskTypeForCreatingTodo/{TodoId}")]
        public JsonResponse getTaskTypeForCreatingTodo(int TodoId)
        {
            try
            {
                objBll = new ToDos();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTaskTypeForCreatingTodo(TodoId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/getTaskTypeForCreatingTodo => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpPost]
        [Route("api/ToDoAPI/SaveTodoDescription")]
        public JsonResponse SaveTodoDescription(TodosModel.TodoUpdateModel model)
        {
            try
            {
                objBll = new ToDos();
                objBll.SaveTodoDescription(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/SaveTodoDescription => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
            return ControllerExtensions.JsonSuccess("Description updated successfully");
        }


        [HttpPost]
        [Route("api/ToDoAPI/UpdateTodoName")]
        public JsonResponse UpdateTodoName(TodosModel.TodoUpdateModel model)
        {
            try
            {
                objBll = new ToDos();
                objBll.UpdateTodoName(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/UpdateTodoName => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
            return ControllerExtensions.JsonSuccess("Todo Name updated successfully");
        }


        [HttpPost]
        [Route("api/ToDoAPI/UpdateTodoReporter")]
        public JsonResponse UpdateTodoReporter(TodosModel.NewReporterUpdateModel model)
        {
            try
            {
                objBll = new ToDos();
                objBll.UpdateTodoReporter(model.UserId, model.ReporterUpdateModel);
                return ControllerExtensions.JsonSuccess("Reporter Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/UpdateTodoReporter => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpPut]
        [Route("api/ToDoAPI/UpdateTodoDeadlineDate")]
        public JsonResponse UpdateTodoDeadlineDate(TodosModel.TodoUpdateModel model)
        {
            try
            {
                objBll = new ToDos();
                objBll.UpdateTodoDeadlineDate(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/UpdateTodoDeadlineDate => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
            return ControllerExtensions.JsonSuccess("Todo Deadline Date updated successfully");
        }

        [HttpPut]
        [Route("api/ToDoAPI/UpdateTodoEstimatedTime")]
        public JsonResponse UpdateTodoEstimatedTime(TodosModel.TodoUpdateModel model)
        {
            try
            {
                objBll = new ToDos();
                objBll.UpdateTodoEstimatedTime(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/UpdateTodoEstimatedTime => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
            return ControllerExtensions.JsonSuccess("Todo Estimated Time updated successfully");
        }


        [HttpPut]
        [Route("api/ToDoAPI/UpdateTodoTaskType")]
        public JsonResponse UpdateTodoTaskType(TodosModel.TodoUpdateModel model)
        {
            try
            {
                objBll = new ToDos();
                objBll.UpdateTodoTaskType(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAPI/UpdateTodoTaskType => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
            return ControllerExtensions.JsonSuccess("Todo Task Type updated successfully");
        }





    }
}