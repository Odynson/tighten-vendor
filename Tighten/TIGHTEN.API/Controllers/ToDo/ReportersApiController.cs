﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;
using TIGHTEN.MODEL;

namespace TIGHTEN.API.Controllers
{
    public class ReportersApiController : ApiController
    {
        ReportersForTodoes objReporter; 

        // GET: api/ReportersApi
        [Route("api/ReportersApi/{UserId}/{CompanyId}/{ProjectId}/{RoleId}")]
        public JsonResponse Get(string UserId ,int CompanyId,int ProjectId, int RoleId)
        {
            try
            {
                objReporter = new ReportersForTodoes();
                return ControllerExtensions.JsonCustomSuccess(objReporter.getToDoReporters(UserId, CompanyId, ProjectId, RoleId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ReportersApi/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }



        [HttpPost]
        [Route("api/ReportersApi/SaveInvoice")]
        public JsonResponse SaveInvoice(ReportersForTodoesModel.ReportersEmailData model)
        {

            try
            {
                objReporter = new ReportersForTodoes();
                return ControllerExtensions.JsonCustomSuccess(objReporter.SaveInvoiceDetails(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ReportersApi/SaveInvoice => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }


        //[HttpPost]
        //[Route("api/ReportersApi/SendMail")]
        //public JsonResponse SendMail(ReportersForTodoesModel.ReportersEmailData model)
        //{
        //    try
        //    {
        //        objReporter = new ReportersForTodoes();
        //        return ControllerExtensions.JsonCustomSuccess(objReporter.SendMailToSelectedReporters(model));
        //    }
        //    catch (Exception ex)
        //    {
        //        string CustomException = "Path : api/ReportersApi/SendMail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

        //        return ControllerExtensions.JsonError("Some Error Occured !");
        //    }
        //}




        [HttpPost]
        [Route("api/ReportersApi/getAllTodosForPreview")]
        public JsonResponse getAllTodosForPreview(ReportersForTodoesModel.TodosForPreview_RequiredParams model)
        {
            try
            {
                objReporter = new ReportersForTodoes();
                return ControllerExtensions.JsonCustomSuccess(objReporter.getAllTodosForPreview(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ReportersApi/getAllTodosForPreview => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




    }
}
