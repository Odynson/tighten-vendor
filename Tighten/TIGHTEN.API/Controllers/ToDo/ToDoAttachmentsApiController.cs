﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using System.IO;
using System.Web;
using TIGHTEN.UTILITIES;
namespace TIGHTEN.API
{
    public class ToDoAttachmentsApiController : ApiController
    {
        ToDoAttachments objBll;



        /// <summary>
        /// Getting Attachments for Particular Todo
        /// </summary>
        /// <param name="id">It is the Unique TodoId</param>
        /// <returns>returns attachments related to todo</returns>
        /// 
        [Route("api/ToDoAttachmentsAPI/{ToDoId}/{UserId}")]
        [HttpGet]
        public JsonResponse Get(int ToDoId, string UserId)
        {
            try
            {
                objBll = new ToDoAttachments();
                return ControllerExtensions.JsonCustomSuccess(objBll.getAttachments(ToDoId, UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAttachmentsAPI/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }




        /// <summary>
        /// Getting Attachments for whole Project
        /// </summary>
        /// <param name="id">it is ProjectId</param>
        /// <returns>return atttachments in json format related to project</returns>
        [HttpGet]
        [Route("api/ToDoAttachmentsAPI/Attachments/{id}")]
        public JsonResponse Attachments(int id)
        {
            try
            {
                objBll = new ToDoAttachments();
                return ControllerExtensions.JsonCustomSuccess(objBll.getAttachmentsForParticularProject(id));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAttachmentsAPI/Attachments => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }



        /// <summary>
        /// It saves the DropBox files Link which the user has added from DropBox
        /// </summary>
        /// <param name="id">It is Unique Id of Todo</param>
        /// <param name="model">It is list of files being selected with their names and link on dropbox</param>
        /// <returns>Success or Error</returns>
        [HttpPost]
        [Route("api/ToDoAttachmentsAPI/DropBoxFiles/{id}")]
        public JsonResponse DropBoxFiles(int id, List<CloudFilesModel.DropBoxFilesModel> model, string userId)
        {
            try
            {
                objBll = new ToDoAttachments();
                objBll.saveDropBoxFiles(userId, id, model);

                return ControllerExtensions.JsonSuccess("DropBox Files Saved Successfully !");

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAttachmentsAPI/DropBoxFiles => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }


        /// <summary>
        /// It saves the Box files Link which the user has added from Box
        /// </summary>
        /// <param name="id">It is Unique Id of Todo</param>
        /// <param name="model">It is list of files being selected with their names and Url on box</param>
        /// <returns>Success or Error</returns>
        [HttpPost]
        [Route("api/ToDoAttachmentsAPI/BoxFiles/{id}")]
        public JsonResponse BoxFiles(int id, List<CloudFilesModel.BoxFilesModel> model, string userId)
        {
            try
            {
                objBll = new ToDoAttachments();
                objBll.saveBoxFiles(userId, id, model);
                return ControllerExtensions.JsonSuccess("Box Files Saved Successfully !");

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAttachmentsAPI/BoxFiles => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }




        /// <summary>
        /// It saves the GoogleDrive files Link which the user has added from GoogleDrive
        /// </summary>
        /// <param name="id">It is Unique Id of Todo</param>
        /// <param name="model">It is list of files being selected with their names and Url on GoogleDrive</param>
        /// <returns>Success or Error</returns>
        [HttpPost]
        [Route("api/ToDoAttachmentsAPI/GoogleDriveFiles/{id}")]
        public JsonResponse GoogleDriveFiles(int id, List<CloudFilesModel.GoogleDriveFilesModel> model, string userId)
        {
            try
            {
                objBll = new ToDoAttachments();
                objBll.saveGoogleDriveFiles(userId, id, model);
                return ControllerExtensions.JsonSuccess("GoogleDrive Files Saved Successfully !");

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoAttachmentsAPI/GoogleDriveFiles => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }





        // POST api/<controller>
        public void Post(TodoModel.ToDoAttachments obj, string userId)
        {
            objBll = new ToDoAttachments();
            objBll.saveCloudToDoAttachments(obj, userId);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public JsonResponse Delete(int Id)
        {
            try
            {
                objBll = new ToDoAttachments();
                string attachmentName = objBll.deleteAttachment(Id);

                File.Delete(HttpContext.Current.Server.MapPath("~/Uploads/Attachments/ToDo/") + attachmentName);
                return ControllerExtensions.JsonCustomSuccess(attachmentName, "todoAttachmentDeleteSuccess");
            }

            catch(Exception ex)
            {
                string CustomException = "Path : api/ToDoAttachmentsAPI/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
            

        }
    }
}