﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers.ToDo
{
    public class TodoManualTimeController : ApiController
    {
        TodoManual objBll;

        /// <summary>
        /// It gets List of todos
        /// </summary>
        /// <returns>returns Success or Error</returns>
        /// 
        [Route("api/TodoManualTime/getMyTodos/{userId}/{RoleId}")]
        public JsonResponse Get(string userId, int RoleId)
        {
            try
            {
                objBll = new TodoManual();
                return ControllerExtensions.JsonCustomSuccess(objBll.getMyTodos(userId, RoleId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/TodoManualTime/getMyTodos => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        // POST: api/TodoManualTime
        public JsonResponse Post(MODEL.TodoManualModel.TodoAddModel obj)
        {
            string TodoId;
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new TodoManual();
                TodoId = Convert.ToString(objBll.saveToDo(obj));
            }
            catch( Exception ex)
            {
                string CustomException = "Path : api/TodoManualTime/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess(TodoId);
        }

        // PUT: api/TodoManualTime/5
        public JsonResponse Put(MODEL.TodoManualModel.TodoAddModel obj)
        {
            string TodoId;
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new TodoManual();
                TodoId = Convert.ToString(objBll.UpdateToDo(obj));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/TodoManualTime/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess(TodoId);
        }

        // DELETE: api/TodoManualTime/5
        public void Delete(int id)
        {
        }
    }
}
