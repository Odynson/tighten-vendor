﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.UTILITIES;
using TIGHTEN.BUSINESS;

namespace TIGHTEN.API
{
    public class TodoNotificationsApiController : ApiController
    {
        TodoNotification objBll;

        /// <summary>
        /// Fetching Notification for particular Todo i.e Todo Notifications
        /// </summary>
        /// <param name="id">Unique Id of Todo</param>
        /// <returns></returns>
        public JsonResponse Get(int id)
        {
            try
            {
                objBll = new TodoNotification();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetTodoNotifications(id));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/TodoNotificationsApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }


    }
}