﻿using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TIGHTEN.API
{
    public class ToDoCommentsApiController : ApiController
    {
        // GET api/<controller>/5
        [Route("api/ToDoCommentsApi/{TodoId}/{UserId}")]
        [HttpGet]
        public HttpResponseMessage Get(int TodoId,string UserId)
        {
            ToDoComments objBll = new ToDoComments();
            return Request.CreateResponse(HttpStatusCode.OK, objBll.getToDoComments(TodoId, UserId));
        }



        // POST api/<controller>
        public JsonResponse Post(TodoModel.NewToDoComment obj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                /*Add reference for user adding the comment*/
                obj.ToDoComment.CreatedBy = obj.UserId;

                ToDoComments objBll = new ToDoComments();
                objBll.saveToDoComment(obj.ToDoComment);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoCommentsApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError(ex.InnerException.Message);
            }

            return ControllerExtensions.JsonSuccess();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public JsonResponse Delete(int id)
        {
            try
            {
                ToDoComments objBll = new ToDoComments();

                objBll.deleteToDoComment(id);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoCommentsApi/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }

            return ControllerExtensions.JsonSuccess();

        }
    }
}