﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.AspNet.Identity.Owin;
//using Microsoft.Owin.Security;
//using Microsoft.Owin.Security.Cookies;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
    public class UserMessagesController : ApiController
    {

        UserAPI objBLL = new UserAPI();
        TIGHTEN.BUSINESS.UserOnlineBusiness userOnline = new BUSINESS.UserOnlineBusiness();
        TIGHTEN.BUSINESS.UserMessagesBusiness userMessage = new BUSINESS.UserMessagesBusiness();
        // GET: UserMessages
        [System.Web.Http.HttpGet] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/addUpdateUserOnline/{UserId}")]
        public JsonResponse addUpdateUserOnline(string UserId)
        {
            try
            {
                userOnline.addUpdateOnlineUser(UserId);
                return null;
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/addUpdateUserOnline => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        // GET: UserMessages
        [System.Web.Http.HttpGet] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/GetOnlineUser")]
        public JsonResponse getOnlineUser()
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(userOnline.getAllOnlineUser());
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/GetOnlineUser => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        // GET: UserMessages
        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/Messages/InsertUserMessages")]
        public JsonResponse insertUserMessages(MODEL.UserMessagesModel messageModel)
        {
            try
            {
                userMessage.addUserMessages(messageModel);
                return null;
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/Messages/InsertUserMessages => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        // GET: UserMessages
        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/Messages/GetAndUpdateMessageIsReadStatus")]
        public JsonResponse getUpdateMessageIsReadStatus(MODEL.UserMessagesModel messageModel)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(userMessage.getUpdateMessageIsReadStatus(messageModel));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/Messages/GetAndUpdateMessageIsReadStatus => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        // GET: UserMessages
        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/Messages/UpdateMessageIsReadStatus")]
        public JsonResponse updateMessageIsReadStatus(MODEL.UserMessagesModel messageModel)
        {
            try
            {
                userMessage.updateMessageIsReadStatus(messageModel);
                return null;
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/Messages/UpdateMessageIsReadStatus => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        // GET: UserMessages
        [System.Web.Http.HttpGet] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/Messages/GetUsersMessages/{reciverId}/{senderId}")]
        public JsonResponse getUserMessages(string reciverId, string senderId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(userMessage.GetUserMessages(reciverId, senderId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/Messages/GetUsersMessages => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [System.Web.Http.HttpGet] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/Messages/GetSenderUsers/{UserId}")]
        public JsonResponse GetSenderUsers(string UserId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(userMessage.GetSenderUsers(UserId).ToList());
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/Messages/GetSenderUsers => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [System.Web.Http.HttpGet] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/Messages/GetMessagesCount/{UserId}")]
        public JsonResponse GetMessagesCount(string UserId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(userMessage.GetMessagesCount(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/Messages/GetMessagesCount => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Messages/GetAllSenders/{SenderId}/{CompanyId}/{UId}")]
        public JsonResponse GetAllSenders(string SenderId,int CompanyId, string UId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(userMessage.GetAllSenders(SenderId, CompanyId, UId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/Messages/GetAllSenders => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }

    }
}