﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
    public class ProjectUsersApiController : ApiController
    {
        ProjectUsers objBll;
    

        /// <summary>
        /// Getting Project Members for particular Project
        /// </summary>
        /// <param name="id">It is unique Id of Project</param>
        /// <returns>Returns Project Members in Json Format</returns>
        public JsonResponse Get(int id)
        {
            try
            {
                objBll = new ProjectUsers();
                return ControllerExtensions.JsonCustomSuccess(objBll.getProjectMembers(id));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/projectusersapi/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        /* Fetching Users that are not in the team */

        [HttpGet]
        [Route("api/projectusersapi/getNonProjectUsers/{id}")]
        public HttpResponseMessage getNonProjectUsers(int id)
        {

            return Request.CreateResponse(HttpStatusCode.OK, objBll.getNonProjectUsers(id));
        }

        // POST api/<controller>
        public JsonResponse Post(TodoModel.ProjectUsers obj, string userId)
        {

            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {

                objBll.AddProjectUser(userId, obj);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/projectusersapi/Post=> Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }

            return ControllerExtensions.JsonSuccess();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {

            objBll.DeleteProjectUser(id);
        }
    }
}