﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class TeamFinancialManagerController : ApiController
    {
        Projects objBll;
        Team objBllTeam;

        //public JsonResponse Index(int CompanyId)
        //{
        //    try
        //    {
        //        objBll = new Projects();
        //        return ControllerExtensions.JsonCustomSuccess(objBll.getAllProjectsForFinancialManager(CompanyId));
        //    }
        //    catch (Exception ex)
        //    {
        //        return ControllerExtensions.JsonError("Some Error Occured!");
        //    }
        //}

        /// <summary>
        /// It fetches Company Teams With Team Members
        /// </summary>
        /// <param name="id">It is unique Id of Team</param>
        /// <returns>It returns Teams is Json Format</returns>
        [HttpGet]
        [Route("api/teamFinancialManagerApi/{CompanyId}/{UserId}")]
        public JsonResponse CompanyTeams(int CompanyId, string UserId)
        {
            try
            {
                objBllTeam = new Team();
                return ControllerExtensions.JsonCustomSuccess(objBllTeam.getAllTeamsForFinancialManager(CompanyId, UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamFinancialManagerApi/CompanyTeams => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }
    }
}