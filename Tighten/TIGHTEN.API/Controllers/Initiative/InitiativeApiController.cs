﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.UTILITIES;
using TIGHTEN.BUSINESS.Initiative;
//using static TIGHTEN.MODEL.Initiative.InitiativeModel;
using TIGHTEN.MODEL.Initiative;
using System.Configuration;
using System.Web;
using System.IO;
using TIGHTEN.BUSINESS;
using System.Net.Http.Headers;
using System.Web.Http.Cors;
using TIGHTEN.MODEL;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO.Compression;
namespace TIGHTEN.API.Controllers.Initiative
{
    [EnableCors("*", "*", "*")]
    public class InitiativeApiController : ApiController
    {
        InitiativeBLL objBll;
        [HttpGet]
        [Route("api/InitiativeApi/GetInitiativeList/{CompanyId}")]
        public JsonResponse GetInitiativeList(int CompanyId)
        {

            try
            {
                objBll = new InitiativeBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInitiativeList(CompanyId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/InitiativeApi/GetInitiativeList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }

        [HttpGet]
        [Route("api/InitiativeApi/GetVendorProjectInitiativeList/{InitiativeId}")]
        public JsonResponse GetVendorProjectInitiativeList(int InitiativeId)
        {

            try
            {
                objBll = new InitiativeBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorProjectInitiativeList(InitiativeId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/InitiativeApi/GetInitiativeList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }

        [HttpPost]
        [Route("api/InitiativeApi/CreateNewInitiative")]
        public async Task<JsonResponse> CreateNewInitiative()
        {
            var AbsRoot = "~/Uploads/Attachments/Initiative_Attachments/";
            string RelativeUrl = "Attachments/Initiative_Attachments/";

            var StoragePath = HttpContext.Current.Server.MapPath(AbsRoot);
            if (Request.Content.IsMimeMultipartContent())
            {
                objBll = new InitiativeBLL();
                try
                {
                    var InitModal = HttpContext.Current.Request.Params["model"];

                    if (!Directory.Exists(StoragePath + "Upload"))
                    {
                        Directory.CreateDirectory(StoragePath + "Upload");
                    }

                    InitiativeModel ObjInitModal = JsonConvert.DeserializeObject<InitiativeModel>(InitModal);
                    var streamProvider = new MultipartFormDataStreamProvider(Path.Combine(StoragePath, "Upload"));


                    int Initiativecheck = objBll.CheckInitiativeExist(ObjInitModal.enterpriseCompanyID, ObjInitModal.initiative_title, ObjInitModal.Id);


                    if (Initiativecheck == 1)
                    {


                        var formData = streamProvider.FormData;
                        await Request.Content.ReadAsMultipartAsync(streamProvider);
                        foreach (MultipartFileData fileData in streamProvider.FileData)
                        {
                            if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                            {

                                return ControllerExtensions.JsonCustomSuccess("This request is not properly formatted");
                                //  return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                            }
                            string ActualFileName = fileData.Headers.ContentDisposition.FileName;
                            if (ActualFileName.StartsWith("\"") && ActualFileName.EndsWith("\""))
                            {
                                ActualFileName = ActualFileName.Trim('"');
                            }
                            if (ActualFileName.Contains(@"/") || ActualFileName.Contains(@"\"))
                            {
                                ActualFileName = Path.GetFileName(ActualFileName);
                            }
                            string strExtension = Path.GetExtension(ActualFileName).ToLower();
                            Guid guid = Guid.NewGuid();
                            string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;

                            File.Move(fileData.LocalFileName, Path.Combine(StoragePath, FinalFilename));

                            string UploadFileName = JsonConvert.DeserializeObject<string>(fileData.Headers.ContentDisposition.Name);

                            if (!string.IsNullOrEmpty(UploadFileName) && UploadFileName == "InitiDocuments")
                            {
                                ObjInitModal.ActualFileName += ActualFileName + ",";
                                ObjInitModal.FileName += FinalFilename + ",";
                                ObjInitModal.FilePath += RelativeUrl + FinalFilename + ",";
                            }
                            //else
                            //{
                            //    ObjInitModal.vendorids = ActualFileName;
                            //    ObjInitModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].FileName = FinalFilename;
                            //    ObjInitModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].FilePath = RelativeUrl + FinalFilename;
                            //}
                        }



                        string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                        // var data = ControllerExtensions.JsonCustomSuccess(objBll.SaveNewProject(ObjProjectModal));
                        // Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                        //return Request.CreateResponse(HttpStatusCode.OK, data);

                        return ControllerExtensions.JsonCustomSuccess(objBll.SaveNewInitiative(ObjInitModal));
                    }
                    else
                    {
                        Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                        return ControllerExtensions.JsonCustomSuccess(-1);

                    }

                }
                catch (Exception ex)
                {
                    string CustomException = "Path : api/InitiativeApi/CreateNewInitiative => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                    return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
                }
            }
            else
            {
                return ControllerExtensions.JsonError("This request is not properly formatted");

            }
        }
        
        [HttpGet]
        [Route("api/InitiativeApi/DeleteVendorProjectInitiativeFromList/{Id}")]
        public JsonResponse DeleteVendorProjectInitiativeFromList(int Id)
        {

            try
            {
                objBll = new InitiativeBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.DeleteVendorProjectInitiativeFromList(Id));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/InitiativeApi/DeleteVendorProjectInitiativeFromList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }


        [HttpGet]
        [Route("api/InitiativeApi/GetInitiativeById/{InitiativeId}")]
        public JsonResponse GetInitiativeById(int InitiativeId)
        {

            try
            {
                objBll = new InitiativeBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInitiativeById(InitiativeId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/InitiativeApi/GetInitiativeById => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }

        [HttpGet]
        [Route("api/InitiativeApi/DeleteInitiative/{Id}")]
        public JsonResponse DeleteInitiative(int Id)
        {

            try
            {
                objBll = new InitiativeBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.DeleteInitiative(Id));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/InitiativeApi/DeleteVendorProjectInitiativeFromList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }

        [HttpPost]
        [Route("api/InitiativeApi/EditInitiative")]
        public async Task<JsonResponse> EditInitiative()
        {
            var AbsRoot = "~/Uploads/Attachments/Initiative_Attachments/";
            string RelativeUrl = "Attachments/Initiative_Attachments/";

            var StoragePath = HttpContext.Current.Server.MapPath(AbsRoot);
            if (Request.Content.IsMimeMultipartContent())
            {
                objBll = new InitiativeBLL();
                try
                {
                    var InitModal = HttpContext.Current.Request.Params["model"];

                    if (!Directory.Exists(StoragePath + "Upload"))
                    {
                        Directory.CreateDirectory(StoragePath + "Upload");
                    }
                    
                    InitiativeModel ObjInitModal = JsonConvert.DeserializeObject<InitiativeModel>(InitModal);
                    foreach (var Item in ObjInitModal.vendorProjectDocumentList)
                    {
                        string strExtension = Path.GetExtension(Item.ActualFileName).ToLower();
                        Guid guid = Guid.NewGuid();
                        string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;
                        ObjInitModal.ActualFileName += Item.ActualFileName + ",";
                        ObjInitModal.FileName += FinalFilename + ",";
                        ObjInitModal.FilePath += RelativeUrl + FinalFilename + ",";
                        ObjInitModal.DocListIsAdded += Item.IsAdded.ToString() + ",";
                        ObjInitModal.DocIsDeletedList += Item.IsDeleted.ToString() + ",";
                        ObjInitModal.DocIdList += Item.Id.ToString() + ",";
                    }
                    var streamProvider = new MultipartFormDataStreamProvider(Path.Combine(StoragePath, "Upload"));


                    int Initiativecheck = objBll.CheckInitiativeExist(ObjInitModal.enterpriseCompanyID, ObjInitModal.initiative_title, ObjInitModal.Id);


                    if (Initiativecheck == 1)
                    {


                        var formData = streamProvider.FormData;
                        await Request.Content.ReadAsMultipartAsync(streamProvider);
                        foreach (MultipartFileData fileData in streamProvider.FileData)
                        {
                            if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                            {

                                return ControllerExtensions.JsonCustomSuccess("This request is not properly formatted");
                                //  return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                            }
                            string ActualFileName = fileData.Headers.ContentDisposition.FileName;
                            if (ActualFileName.StartsWith("\"") && ActualFileName.EndsWith("\""))
                            {
                                ActualFileName = ActualFileName.Trim('"');
                            }
                            if (ActualFileName.Contains(@"/") || ActualFileName.Contains(@"\"))
                            {
                                ActualFileName = Path.GetFileName(ActualFileName);
                            }
                            string strExtension = Path.GetExtension(ActualFileName).ToLower();
                            Guid guid = Guid.NewGuid();
                            string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;

                            File.Move(fileData.LocalFileName, Path.Combine(StoragePath, FinalFilename));

                            string UploadFileName = JsonConvert.DeserializeObject<string>(fileData.Headers.ContentDisposition.Name);

                            //if (!string.IsNullOrEmpty(UploadFileName) && UploadFileName == "InitiDocuments")
                            //{
                            //    ObjInitModal.ActualFileName += ActualFileName + ",";
                            //    ObjInitModal.FileName += FinalFilename + ",";
                            //    ObjInitModal.FilePath += RelativeUrl + FinalFilename + ",";

                            //}
                            //else
                            //{
                            //    ObjInitModal.vendorids = ActualFileName;
                            //    ObjInitModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].FileName = FinalFilename;
                            //    ObjInitModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].FilePath = RelativeUrl + FinalFilename;
                            //}
                        }



                        string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                        // var data = ControllerExtensions.JsonCustomSuccess(objBll.SaveNewProject(ObjProjectModal));
                        // Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                        //return Request.CreateResponse(HttpStatusCode.OK, data);

                        return ControllerExtensions.JsonCustomSuccess(objBll.EditInitiative(ObjInitModal));
                    }
                    else
                    {
                        Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                        return ControllerExtensions.JsonCustomSuccess(-1);

                    }

                }
                catch (Exception ex)
                {
                    string CustomException = "Path : api/InitiativeApi/CreateNewInitiative => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                    return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
                }
            }
            else
            {
                return ControllerExtensions.JsonError("This request is not properly formatted");

            }
        }



    }
}
