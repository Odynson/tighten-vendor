﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.UTILITIES;
using TIGHTEN.BUSINESS.ParentAccount;
using static TIGHTEN.MODEL.ParentAccount.ParentAccountModel;
using TIGHTEN.MODEL.ParentAccount;
using System.Configuration;
using System.Web;
using System.IO;
using TIGHTEN.BUSINESS;
using System.Net.Http.Headers;
using System.Web.Http.Cors;
using TIGHTEN.MODEL;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO.Compression;

namespace TIGHTEN.API.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ParentAccountApiController : ApiController
    {
        ParentAccountBLL objBll;

        [HttpGet]
        [Route("api/ParentAccountApi/GetParentAccountList/{CompanyId}")]
        public JsonResponse GetParentAccountList(int CompanyId)
        {

            try
            {
                objBll = new ParentAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetParentAccountList(CompanyId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/ParentAccountApi/GetParentAccountList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }


        [HttpGet]
        [Route("api/ParentAccountApi/GetParentAccountById/{Id}")]
        public JsonResponse GetParentAccountById(int Id)
        {

            try
            {
                objBll = new ParentAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetParentAccountById(Id));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/ParentAccountApi/GetParentAccountById => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }

        [Route("api/ParentAccountApi/SaveNewParentAccount")]
        public async Task<JsonResponse> SaveNewParentAccount(ParentAccountModel model)
        {
            try
            {
                objBll = new ParentAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.SaveNewParentAccount(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ParentAccountApi/saveWorkLog => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!" + CustomException);
            }


        }

        public JsonResponse Delete(int id)
        {
            try
            {
                objBll = new ParentAccountBLL();
                
                string msg = objBll.deleteParentAccountDetail(id);
                if (msg == "ParentAccDetailDeletedSuccessfully")
                {
                    return ControllerExtensions.JsonSuccess(msg);
                }
                else
                {
                    return ControllerExtensions.JsonError(msg);
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ParentAccountApi/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [HttpPut]
        public JsonResponse Put(ParentAccountModel saveObj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new ParentAccountBLL();
                if (saveObj == null) saveObj = new ParentAccountModel();
                //string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                //objBll.updateTeam(team.UserId, team.TeamModel, team.CompanyId);
                return ControllerExtensions.JsonCustomSuccess(objBll.updateParentAccountDetail(saveObj));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ParentAccountApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError("Error", "Some Error Occured !" + CustomException);
            }

            //return ControllerExtensions.JsonSuccess("Team Updated Successfully !");
        }

        [HttpGet]
        [Route("api/ParentAccountApi/GetParentAccountForVendorList/{VendorId}")]
        public JsonResponse GetParentAccountForVendorList(int VendorId)
        {

            try
            {
                objBll = new ParentAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetParentAccountForVendorList(VendorId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/ParentAccountApi/GetParentAccountForVendorList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }

    }
}
