﻿using TIGHTEN.BUSINESS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using System.Configuration;
using System.Web;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TIGHTEN.API.Models;
using System.Collections.Specialized;
using System.Text;
using static TIGHTEN.MODEL.ProjectModel;
using TIGHTEN.MODEL.Vendor;
//using System.Net.Http.Headers;

namespace TIGHTEN.API
{
    public class ProjectsApiController : ApiController
    {

        Projects objBll;


        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/ProjectsApi/UploadFileForAttachments")]
        public JsonResponse UploadFileForAttachments()
        {
            try
            {
                ProjectModel.AttachmentFileModel model = new ProjectModel.AttachmentFileModel();

                if (!Request.Content.IsMimeMultipartContent())
                {
                    this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedFile"];
                int index = Convert.ToInt32(HttpContext.Current.Request.Params["index"]);

                var fileName = httpPostedFile.FileName;
                long filesize = httpPostedFile.ContentLength;
                if (filesize < 10485760)  // is lesser then 10mb
                {
                    string strExtension = Path.GetExtension(fileName).ToLower();
                    if (strExtension == ".jpg" || strExtension == ".png" || strExtension == ".jpeg" || strExtension == ".bmp" || strExtension == ".gif" || strExtension == ".doc" || strExtension == ".docx" || strExtension == ".xls" || strExtension == ".xlsx" || strExtension == ".txt" || strExtension == ".zip" || strExtension == ".pdf" || strExtension == ".ppt" || strExtension == ".ptx" || strExtension == ".rar")
                    {
                        Guid guid = Guid.NewGuid();
                        string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;

                        var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/Attachments/Mail_Attachments"), FinalFilename);

                        // Save the uploaded file to "Uploads/Attachments/Mail_Attachments" folder
                        httpPostedFile.SaveAs(fileSavePath);
                        //httpPostedFile.Dispose();


                        model.FileName = FinalFilename;
                        model.ActualFileName = fileName;
                        model.FilePath = fileSavePath;
                        model.Index = index;

                    }
                    else
                    {
                        return ControllerExtensions.JsonError("ExtensionMisMatch");
                    }
                }
                else
                {
                    return ControllerExtensions.JsonError("FileSizeExceeds");
                }

                return ControllerExtensions.JsonCustomSuccess(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/ProjectsApi/UploadFileForAttachments => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }





        /// <summary>
        /// Getting User's Teams with projects in which user is Team Member and Project Member For MyTodos Section
        /// </summary>
        /// <returns>It returns User's Teams with Projects in it as an array</returns>
        /// 
        [Route("api/ProjectsApi/{userId}")]
        public JsonResponse Get(string userId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.getMyProjectsWithTeams(userId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// Getting List of Projects for a particular Team with its Project Members in which The user is a project Member
        /// </summary>
        /// <param name="id">It the the unique TeamId</param>
        /// <returns>It returns all the projects with its project members in Json format</returns>
        /// 
        [HttpGet]
        [Route("api/ProjectsApi/getProjectsUsingTeamId/{UserId}/{TeamId}/{CompanyId}/{IsFreelancer}")]
        public JsonResponse Get(string UserId, int TeamId, int CompanyId, bool IsFreelancer)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.getProjects(UserId, TeamId, CompanyId, IsFreelancer));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/getProjectsUsingTeamId => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// Getting User Project By Specific ProjectId
        /// </summary>
        /// <returns>Single  Project Model</returns>
        [HttpGet]
        [Route("api/ProjectsApi/GetProject/{id}")]
        public JsonResponse GetProject(int id)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProject(id));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// Getting All Projects in which User is involved/ProjectMember irrespective of Teams
        /// </summary>
        /// <returns>List of Projects in which User is project Member</returns>
        [HttpGet]
        [Route("api/ProjectsApi/UserProjects/{id}/{CompanyId}")]
        public JsonResponse UserProjects(string id, int CompanyId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserProjects(id, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/UserProjects => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        /// <summary>
        /// Getting All Projects in Teams
        /// </summary>
        /// <returns>List of Projects for selected team</returns>
        [HttpGet]
        [Route("api/ProjectsApi/GetProjectsForSelectedTeam/{TeamId}/{CompanyId}")]
        public JsonResponse GetProjectsForSelectedTeam(int TeamId, int CompanyId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectsForSelectedTeam(TeamId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetProjectsForSelectedTeam => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        /// <summary>
        /// Getting Projects with all project members of a particular Team that have been Created By User i.e owned by User
        /// </summary>
        /// <param name="id">It is unique Id of Team</param>
        [HttpGet]
        [Route("api/ProjectsApi/UserOwnedProjects/{TeamId}/{userId}/{RoleId}/{CompanyId}")]
        public JsonResponse UserOwnedProjects(int TeamId, string userId, int RoleId, int CompanyId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.getUserOwnedProjects(userId, TeamId, RoleId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/UserOwnedProjects => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// It saves the new Project with Project Members
        /// </summary>
        /// <param name="model">Model consists the new values of project and Project members who will be accessing this Project</param>
        public JsonResponse Post(ProjectModel.NewModelForProject obj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new Projects();
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                return ControllerExtensions.JsonCustomSuccess(objBll.saveProject(url, obj.UserId, obj.ProjectModel, obj.CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }

        }

        /// <summary>
        /// It updates the Existing Project Fields
        /// </summary>
        /// <param name="model">Model consists the updated values of existing project and its project members</param>
        public JsonResponse Put(ProjectModel.NewModelForProject obj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new Projects();
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                return ControllerExtensions.JsonCustomSuccess(objBll.updateProject(url, obj.UserId, obj.ProjectModel, obj.CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError();
            }
            //return ControllerExtensions.JsonSuccess();
        }

        /// <summary>
        /// It deletes the existing Project from Projects table
        /// </summary>
        /// <param name="Id">It is unique id of Project that will be Deleted</param>
        /// 
        //[AcceptVerbs("OPTIONS")]
        [HttpDelete]
        [Route("api/ProjectsApi/{id}/{CompanyId}/{IsFreelancer}")]
        public JsonResponse Delete(int id, int CompanyId, bool IsFreelancer)
        {
            try
            {
                objBll = new Projects();
                string msg = objBll.deleteProject(id, CompanyId, IsFreelancer);
                if (msg == "ProjectDeletedSuccessfully")
                {
                    return ControllerExtensions.JsonSuccess(msg);
                }
                else
                {
                    return ControllerExtensions.JsonError(msg);
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }

        /// <summary>
        /// It mark the existing Project as complete (it can done only by Admin)
        /// </summary>
        /// <param name="Id">It is unique id of Project that will be Marked as Complete</param>
        /// 
        [HttpGet]
        [Route("api/ProjectsApi/ProjectComplete/{Id}")]
        public JsonResponse ProjectComplete(int Id)
        {
            try
            {
                objBll = new Projects();
                string ProjectStatus = objBll.ProjectComplete(Id);
                if (ProjectStatus == "ProjectCompleted")
                {
                    return ControllerExtensions.JsonCustomSuccess("Success", ProjectStatus);
                }
                else
                {
                    return ControllerExtensions.JsonCustomError("error", ProjectStatus);
                }

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/ProjectComplete => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }

        }

        /// <summary>
        /// It mark the existing Project as complete (it can done only by Admin)
        /// </summary>
        /// <param name="Id">It is unique id of Project that will be Marked as Complete</param>
        /// 
        [HttpPost]
        [Route("api/ProjectsApi/InsertFeedback")]
        public JsonResponse InsertFeedback(MODEL.ClientTestimonialModel model)
        {
            try
            {
                objBll = new Projects();
                objBll.InsertFeedback(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/InsertFeedback => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
            return ControllerExtensions.JsonSuccess("Feedback is saved successfully !");
        }


        /// <summary>
        /// get all Projects of company
        /// </summary>
        /// <param name="CompanyId">It is unique id of Project that will be Marked as Complete</param>
        /// 
        [HttpGet]
        [Route("api/ProjectsApi/GetAllProjectsOfCompany/{CompanyId}/{TeamId}")]
        public JsonResponse GetAllProjectsOfCompany(int CompanyId, int TeamId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllProjectsOfCompany(CompanyId, TeamId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetAllProjectsOfCompany => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }


        /// <summary>
        /// get all Projects of company
        /// </summary>
        /// <param name="CompanyId">It is unique id of Project that will be Marked as Complete</param>
        /// 
        [HttpGet]
        [Route("api/ProjectsApi/GetAllFreelancerOfProject/{ProjectId}")]
        public JsonResponse GetAllFreelancerOfProject(int ProjectId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllFreelancerOfProject(ProjectId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetAllFreelancerOfProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }



        /// <summary>
        /// get all Projects of company
        /// </summary>
        /// <param name=" "> </param>
        /// 
        [HttpPost]
        [Route("api/ProjectsApi/SaveProjectOptionsForFreelancer")]
        public JsonResponse SaveProjectOptionsForFreelancer(MODEL.ProjectModel.ProjectOptionsForFreelancerModel model)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.SaveProjectOptionsForFreelancer(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/SaveProjectOptionsForFreelancer => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }



        [HttpGet]
        [Route("api/ProjectsApi/GetFreelancerDetails/{UserId}/{projectId}")]
        public JsonResponse GetFreelancerDetails(string UserId, int projectId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetFreelancerDetails(UserId, projectId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetFreelancerDetails => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }




        [HttpGet]
        [Route("api/ProjectsApi/DeleteAttachmentFileFromProject/{projectId}")]
        public JsonResponse DeleteAttachmentFileFromProject(int projectId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.DeleteAttachmentFileFromProject(projectId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/DeleteAttachmentFileFromProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }


        [HttpGet]
        [Route("api/ProjectsApi/DownloadAttachmentFileFromProject/{projectId}")]
        public JsonResponse DownloadAttachmentFileFromProject(int projectId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.DownloadAttachmentFileFromProject(projectId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/DownloadAttachmentFileFromProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }



        [HttpGet]
        [Route("api/ProjectsApi/GetInvitedFreelamcersOnlyNotAccepted/{projectId}")]
        public JsonResponse GetInvitedFreelamcersOnlyNotAccepted(int projectId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInvitedFreelamcersOnlyNotAccepted(projectId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetInvitedFreelamcersOnlyNotAccepted => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }


        [HttpGet]
        [Route("api/ProjectsApi/GetInternalUserDetails/{UserId}/{projectId}")]
        public JsonResponse GetInternalUserDetails(string UserId, int projectId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInternalUserDetails(UserId, projectId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetInternalUserDetails => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }




        [HttpGet]
        [Route("api/ProjectsApi/DownloadProjectDocument/{projectId}")]
        public HttpResponseMessage DownloadProjectDocument(int projectId)
        {
            HttpResponseMessage Response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                objBll = new Projects();
                string FileName = objBll.GetFileName(projectId);

                var filePath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/Attachments/Mail_Attachments/" + FileName);

                if (!File.Exists(filePath)) //Not found then throw Exception
                    throw new HttpResponseException(HttpStatusCode.NotFound);

                //S2:Read File as Byte Array
                byte[] fileData = File.ReadAllBytes(filePath);

                if (fileData == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                //S3:Set Response contents and MediaTypeHeaderValue
                Response.Content = new ByteArrayContent(fileData);
                Response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

                return Response;
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/DownloadProjectDocument => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

            }
            return Response;
        }



        [HttpPost]
        [Route("api/ProjectsApi/DownloadFreelancerAttachments")]
        public HttpResponseMessage DownloadFreelancerAttachments(MODEL.ProjectModel.ProjectFileModel model)
        {
            try
            {
                //objBll = new Projects();
                //string FileName = objBll.GetFileName(projectId);

                var filePath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/Attachments/Mail_Attachments/" + model.FileName);

                if (!File.Exists(filePath)) //Not found then throw Exception
                    throw new HttpResponseException(HttpStatusCode.NotFound);

                HttpResponseMessage Response = new HttpResponseMessage(HttpStatusCode.OK);

                //S2:Read File as Byte Array
                byte[] fileData = File.ReadAllBytes(filePath);

                if (fileData == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                //S3:Set Response contents and MediaTypeHeaderValue
                Response.Content = new ByteArrayContent(fileData);
                Response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

                return Response;
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/DownloadFreelancerAttachments => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                throw;
            }
        }
        //[HttpPost]

        //[Route("api/ProjectsApi/CreateNewProject")]
        //public JsonResponse CreateNewProject(ProjectModel.NewCreateProjectModel obj)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return ControllerExtensions.JsonModelErrorAPI(this);
        //    }
        //    try
        //    {
        //        objBll = new Projects();
        //        string url = ConfigurationManager.AppSettings["WebURL"].ToString();
        //        return ControllerExtensions.JsonCustomSuccess(objBll.SaveNewProject(obj));
        //    }
        //    catch (Exception ex)
        //    {
        //        string CustomException = "Path : api/ProjectsApi/CreateNewProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

        //        return ControllerExtensions.JsonError("Some Error Occured!");
        //    }

        //}

        [HttpGet]
        [Route("api/ProjectsApi/GetProjectVendor/{CompanyId}/{ProjectId}")]
        public JsonResponse GetProjectVendor(int CompanyId, int ProjectId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectVendor(CompanyId, ProjectId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetProjectVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpPost]
        [Route("api/ProjectsApi/CreateNewProject")]
        public async Task<JsonResponse> CreateNewProject()
        {
            var AbsRoot = "~/Uploads/Attachments/Mail_Attachments/";
            string RelativeUrl = "Attachments/Mail_Attachments/";

            var StoragePath = HttpContext.Current.Server.MapPath(AbsRoot);
            if (Request.Content.IsMimeMultipartContent())
            {
                objBll = new Projects();
                try
                {
                    var ProjModal = HttpContext.Current.Request.Params["model"];

                    if (!Directory.Exists(StoragePath + "Upload"))
                    {
                        Directory.CreateDirectory(StoragePath + "Upload");
                    }

                    ProjectModel.NewCreateProjectModel ObjProjectModal = JsonConvert.DeserializeObject<ProjectModel.NewCreateProjectModel>(ProjModal);
                    var streamProvider = new MultipartFormDataStreamProvider(Path.Combine(StoragePath, "Upload"));


                    int Projectcheck = objBll.CheckProjectExist(ObjProjectModal.CompanyId, ObjProjectModal.ProjectName, ObjProjectModal.ProjectId);


                    if (Projectcheck == 1)
                    {


                        var formData = streamProvider.FormData;
                        await Request.Content.ReadAsMultipartAsync(streamProvider);
                        foreach (MultipartFileData fileData in streamProvider.FileData)
                        {
                            if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                            {

                                return ControllerExtensions.JsonCustomSuccess("This request is not properly formatted");
                                //  return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                            }
                            string ActualFileName = fileData.Headers.ContentDisposition.FileName;
                            if (ActualFileName.StartsWith("\"") && ActualFileName.EndsWith("\""))
                            {
                                ActualFileName = ActualFileName.Trim('"');
                            }
                            if (ActualFileName.Contains(@"/") || ActualFileName.Contains(@"\"))
                            {
                                ActualFileName = Path.GetFileName(ActualFileName);
                            }
                            string strExtension = Path.GetExtension(ActualFileName).ToLower();
                            Guid guid = Guid.NewGuid();
                            string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;

                            File.Move(fileData.LocalFileName, Path.Combine(StoragePath, FinalFilename));

                            string UploadFileName = JsonConvert.DeserializeObject<string>(fileData.Headers.ContentDisposition.Name);

                            if (!string.IsNullOrEmpty(UploadFileName) && UploadFileName == "Project")
                            {
                                ObjProjectModal.ActualFileName += ActualFileName + ",";
                                ObjProjectModal.FileName += FinalFilename + ",";
                                ObjProjectModal.FilePath += RelativeUrl + FinalFilename + ",";
                            }
                            else
                            {
                                string[] TempIndex = UploadFileName.Split('_');
                                int phaseIndex = Convert.ToInt32(TempIndex[1]);
                                int PhaseNameIndex = Convert.ToInt32(TempIndex[2]);
                                ObjProjectModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].ActualFileName = ActualFileName;
                                ObjProjectModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].FileName = FinalFilename;
                                ObjProjectModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].FilePath = RelativeUrl + FinalFilename;
                            }
                        }



                        string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                        // var data = ControllerExtensions.JsonCustomSuccess(objBll.SaveNewProject(ObjProjectModal));
                        // Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                        //return Request.CreateResponse(HttpStatusCode.OK, data);

                        return ControllerExtensions.JsonCustomSuccess(objBll.SaveNewProject(ObjProjectModal));
                    }
                    else
                    {
                        Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                        return ControllerExtensions.JsonCustomSuccess(-1);

                    }

                }
                catch (Exception ex)
                {
                    string CustomException = "Path : api/VendorApi/CreateNewProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                    return ControllerExtensions.JsonError("Some Error Occured !"+ CustomException);
                }
            }
            else
            {
                return ControllerExtensions.JsonError("This request is not properly formatted");

            }
        }


        [HttpPost]
        [Route("api/ProjectsApi/GetQuotedProjectList")]
        public JsonResponse GetQuotedProjectList(projectQuotedModel Modal)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetQuotedProjectList(Modal));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetQuotedProjectList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }
        [HttpGet]
        [Route("api/ProjectsApi/GetQuotedSubmitbyVendor/{ProjectId}")]
        public JsonResponse GetQuotedSubmitbyVendor(string ProjectId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetQuotedSubmitbyVendor(ProjectId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetQuotedSubmitbyVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpGet]
        [Route("api/ProjectsApi/GetProjectQuotedByVendor/{CompanyId}/{ProjectId}/{VendorId}")]
        public JsonResponse GetProjectQuotedByVendor(int CompanyId, int ProjectId, int VendorId)
        {

            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectQuotedByVendor(CompanyId, ProjectId, VendorId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/VendorApi/GetProjectQuotedByVendor => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpPost]
        [Route("api/ProjectsApi/saveReasonAcceptOrRejectQuote/")]
        public JsonResponse saveReasonAcceptOrRejectQuote(ReasonAcceptRejectModal modal)
        {

            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.saveReasonAcceptOrRejectQuote(modal));

            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/VendorApi/saveReasonAcceptOrRejectQuote => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }

        [HttpGet]
        [Route("api/ProjectsApi/GetNewProjectForEdit/{ProjectId}/{CompanyId}")]
        public JsonResponse GetNewProjectForEdit(int ProjectId, int CompanyId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetNewProjectForEdit(ProjectId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path:api/ProjectsApi/GetNewProjectForEdit => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }


        [HttpPost]
        [Route("api/ProjectsApi/EditNewProject")]
        public async Task<JsonResponse> EditNewProject()
        {
            var AbsRoot = "~/Uploads/Attachments/Mail_Attachments/";
            string RelativeUrl = "Attachments/Mail_Attachments/";

            var StoragePath = HttpContext.Current.Server.MapPath(AbsRoot);
            if (Request.Content.IsMimeMultipartContent())
            {
                objBll = new Projects();
                try
                {
                    var ProjModal = HttpContext.Current.Request.Params["model"];

                    if (!Directory.Exists(StoragePath + "Upload"))
                    {
                        Directory.CreateDirectory(StoragePath + "Upload");
                    }

                    ProjectModel.NewCreateProjectModel ObjProjectModal = JsonConvert.DeserializeObject<ProjectModel.NewCreateProjectModel>(ProjModal);
                    var streamProvider = new MultipartFormDataStreamProvider(Path.Combine(StoragePath, "Upload"));


                    int Projectcheck = objBll.CheckProjectExist(ObjProjectModal.CompanyId, ObjProjectModal.ProjectName, ObjProjectModal.ProjectId);


                    if (Projectcheck == 1)
                    {


                        var formData = streamProvider.FormData;
                        await Request.Content.ReadAsMultipartAsync(streamProvider);
                        foreach (MultipartFileData fileData in streamProvider.FileData)
                        {
                            if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                            {

                                return ControllerExtensions.JsonCustomSuccess("This request is not properly formatted");
                                //  return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                            }
                            string ActualFileName = fileData.Headers.ContentDisposition.FileName;
                            if (ActualFileName.StartsWith("\"") && ActualFileName.EndsWith("\""))
                            {
                                ActualFileName = ActualFileName.Trim('"');
                            }
                            if (ActualFileName.Contains(@"/") || ActualFileName.Contains(@"\"))
                            {
                                ActualFileName = Path.GetFileName(ActualFileName);
                            }
                            string strExtension = Path.GetExtension(ActualFileName).ToLower();
                            Guid guid = Guid.NewGuid();
                            string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;

                            File.Move(fileData.LocalFileName, Path.Combine(StoragePath, FinalFilename));

                            string UploadFileName = JsonConvert.DeserializeObject<string>(fileData.Headers.ContentDisposition.Name);

                            if (!string.IsNullOrEmpty(UploadFileName) && UploadFileName == "Project")
                            {
                                ObjProjectModal.ActualFileName += ActualFileName + ",";
                                ObjProjectModal.FileName += FinalFilename + ",";
                                ObjProjectModal.FilePath += RelativeUrl + FinalFilename + ",";
                            }
                            else
                            {
                                string[] TempIndex = UploadFileName.Split('_');
                                int phaseIndex = Convert.ToInt32(TempIndex[1]);
                                int PhaseNameIndex = Convert.ToInt32(TempIndex[2]);
                                if (ObjProjectModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].PhaseId == 0)
                                {
                                    ObjProjectModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].ActualFileName = ActualFileName;
                                    ObjProjectModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].FileName = FinalFilename;
                                    ObjProjectModal.PhaseList[phaseIndex].PhaseDocumentsName[PhaseNameIndex].FilePath = RelativeUrl + FinalFilename;
                                }
                            }
                        }



                        string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                        // var data = ControllerExtensions.JsonCustomSuccess(objBll.SaveNewProject(ObjProjectModal));
                        // Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                        //return Request.CreateResponse(HttpStatusCode.OK, data);

                        return ControllerExtensions.JsonCustomSuccess(objBll.EditNewProject(ObjProjectModal));
                    }
                    else
                    {
                        Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                        return ControllerExtensions.JsonCustomSuccess(-1);

                    }

                }
                catch (Exception ex)
                {
                    string CustomException = "Path : api/ProjectsApi/EditNewProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                    return ControllerExtensions.JsonError("Some Error Occured !"+CustomException);
                }
            }
            else
            {
                return ControllerExtensions.JsonError("This request is not properly formatted");

            }
        }


        [HttpPost]
        [Route("api/ProjectsApi/DeleteEditProjectFiles")]
        public JsonResponse DeleteEditProjectFiles(MODEL.ProjectModel.ProjectFileModel model)
        {
            objBll = new Projects();
            try
            {
                objBll = new Projects();
                var filePath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/Attachments/Mail_Attachments/" + model.FileName);
                var data = ControllerExtensions.JsonCustomSuccess(objBll.DeleteEditProjectFiles(model));
                bool exitst = File.Exists(filePath);
                if (File.Exists(filePath)) //Not found then throw Exception
                {

                    File.Delete(filePath);
                    objBll = new Projects();

                }
                return data;


            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/ProjectsApi/DeleteEditProjectFiles => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpPost]
        [Route("api/ProjectsApi/GetProjectAwardedForVendor")]
        public JsonResponse GetProjectAwardedForVendor(FilterProjectModal Modal)
        {

            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectAwardedForVendor(Modal));
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/ProjectsApi/GetProjectAwardedForVendor => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpPost]
        [Route("api/ProjectsApi/GetProjectAwardedForcompanyProfile/")]
        public JsonResponse GetProjectAwardedForcompanyProfile(FilterProjectModal Modal)
        {

            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectAwardedForcompanyProfile(Modal));
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/ProjectsApi/GetProjectAwardedForcompanyProfile => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpGet]
        [Route("api/ProjectsApi/GetProjectAwardedViewDetailCompanyProfile/{ProjectId}/{VendorId}/{ComapnyId}")]
        public JsonResponse GetProjectAwardedViewDetailCompanyProfile(int ProjectId, int VendorId, int ComapnyId)
        {

            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectAwardedViewDetailCompanyProfile(ProjectId, VendorId, ComapnyId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/ProjectsApi/GetProjectAwardedViewDetailCompanyProfile => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/ProjectsApi/GetVendorProjectforCompletion/{ProjectId}/{CompanyId}/{VendorId}")]
        public JsonResponse GetVendorProjectforCompletion(int ProjectId, int CompanyId, int VendorId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorProjectforCompletion(ProjectId, CompanyId, VendorId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/ProjectsApi/GetVendorProjectforCompletion => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");


            }
        }

        [HttpPost]
        [Route("api/ProjectsApi/CompletionVendorProject")]
        public JsonResponse CompletionVendorProject(ProjectCompleteModal modal)
        {

            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.CompletionVendorProject(modal));
            }
            catch (Exception ex)
            {
                string CustomException = "Path:api/ProjectsApi/CompletionProject => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpGet]
        [Route("api/ProjectsApi/CompanyListForAwardedProjectDropVendorProfile/{VendorId}")]
        public JsonResponse CompanyListForAwardedProjectDropVendorProfile(int VendorId)
        {

            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.CompanyListForAwardedProjectDropVendorProfile(VendorId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path:api/ProjectsApi/CompanyListForAwardedProjectDropVendorProfile => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }


        [HttpGet]
        [Route("api/ProjectsApi/GetVendorListForDrop/{VendorId}")]
        public JsonResponse GetVendorListForDrop(int VendorId)
        {

            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorListForDrop(VendorId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path:api/ProjectsApi/GetVendorListForDrop => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpPost]
        [Route("api/ProjectsApi/GetProjectName")]
        public JsonResponse GetProjectName(FilterProjectByNameModal Modal)
        {

            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectName(Modal));
            }
            catch (Exception ex)
            {
                string CustomException = "Path:api/ProjectsApi/GetVendorListForDrop => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }
        [HttpGet]
        [Route("api/ProjectsApi/GetCustomControlProjectCreation/{CompanyId}")]
        public JsonResponse GetCustomControlProjectCreation(int CompanyId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCustomControlProjectCreation(CompanyId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path:api/ProjectsApi/GetCustomControlProjectCreation => Error : " + ex.Message + ", InnerException :" + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }

        [HttpGet]
        [Route("api/ProjectsApi/GetProjectVendorByCompIdId/{CompanyId}/{Id}")]
        public JsonResponse GetProjectVendorByCompIdId(int CompanyId, int Id)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.getProjectVendorByCompIdId(CompanyId, Id));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetProjectVendorByCompIdId => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }


        [HttpGet]
        [Route("api/ProjectsApi/GetApprovedProjectByUserId/{UserId}")]
        public JsonResponse GetApprovedProjectByUserId(string UserId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.getApprovedProjectByUserId(UserId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetApprovedProjectByUserId => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpGet]
        [Route("api/ProjectsApi/GetProjectMilestonesByProjectId/{ProjectId}")]
        public JsonResponse GetProjectMilestonesByProjectId(int ProjectId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectMilestonesByProjectId(ProjectId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetProjectMilestonesByProjectId => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpGet]
        [Route("api/ProjectsApi/GetApprovedBillableProjectByUserId/{UserId}")]
        public JsonResponse GetApprovedBillableProjectByUserId(string UserId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetApprovedBillableProjectByUserId(UserId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetApprovedBillableProjectByUserId => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }
        [HttpGet]
        [Route("api/ProjectsApi/GetProjectsOfCompany/{CompanyId}")]
        public JsonResponse GetProjectsOfCompany(int CompanyId)
        {
            try
            {
                objBll = new Projects();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectsOfCompany(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectsApi/GetAllProjectsOfCompany => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured!");
            }
        }
    }


}