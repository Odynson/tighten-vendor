﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class UserInvoiceApiController : ApiController
    {
        UserInvoice objBll;

        [HttpPost]
        [Route("api/UserInvoiceApi")]
        public JsonResponse Post(MODEL.UserInvoice.InvoiceQuery model)
        {
            try
            {
                objBll = new UserInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInvoice(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserInvoiceApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpPost]
        [Route("api/UserInvoiceApi/GetInvoiceDetail")]
        public JsonResponse GetInvoiceDetail(MODEL.UserInvoice.AdminInvoiceDetailsRequiredParams model)
        {
            try
            {
                objBll = new UserInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInvoiceDetail(model, ConfigurationManager.AppSettings["DefaultInvoiceNumber"].ToString()));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserInvoiceApi/GetInvoiceDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }

        }

        [HttpPost]
        [Route("api/UserInvoiceApi/GetProjectMembersForUser")]
        public JsonResponse GetProjectMembersForUser(MODEL.UserInvoice.ProjectMembersToForwardInvoice_QueryParams model)
        {
            try
            {
                objBll = new UserInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectMembersForUser(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserInvoiceApi/GetProjectMembersForUser => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [Route("api/UserInvoiceApi/ForwardInvoice")]
        [HttpPost]
        public JsonResponse ForwardInvoice(MODEL.UserInvoice.ForwardInvoice model)
        {
            try
            {
                objBll = new UserInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.ForwardInvoice(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserInvoiceApi/ForwardInvoice => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured");
            }

        }




    }
}
