﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    [EnableCors("*", "*", "*")]
    public class VendorInvoiceApiController : ApiController
    {
        VendorInvoice objBll;

        [HttpPost]
        [Route("api/VendorInvoiceApi/GetAllInvoiceDetailList/")]
        public JsonResponse GetAllInvoiceDetailList(InvoiceModel model)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllInvoiceDetailList(model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorInvoiceApi/GetAllInvoiceDetailList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ; ;
            }
        }

        [HttpPut]
        [Route("api/VendorInvoiceApi")]
        public JsonResponse Put(MODEL.UserInvoice.InvoiceStatus model)
        {
            try
            {
                  objBll = new VendorInvoice();
                int StripeApplicationFee = Convert.ToInt32(ConfigurationManager.AppSettings["StripeApplicationFee"]);
                var mod = objBll.ChangeInvoiceStatus(model, StripeApplicationFee);
                //await sendInvoiceStatusChangeAndPayEmail(mod);
                return ControllerExtensions.JsonCustomSuccess(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }

        }

        [HttpGet]
        [Route("api/VendorInvoiceApi/GetPreviewInvoicePaidOutstandingReject/{Id}/{VendorId}")]
        public JsonResponse GetPreviewInvoicePaidOutstandingReject(int Id, int VendorId)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetPreviewInvoicePaidOutstandingReject(Id, VendorId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorInvoiceApi/GetAllInvoiceDetailList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ; ;
            }
        }

        [HttpPost]
        [Route("api/VendorInvoiceApi/SaveInvoiceByVendor")]
        public JsonResponse SaveInvoiceByVendor(InvoiceModel model)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.SaveInvoiceByVendor(model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorInvoiceApi/SaveInvoiceByVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured"+ CustomException);
            }


        }

        [HttpPost]
        [Route("api/VendorInvoiceApi/GetAllVendorInvoiceList")]
        public JsonResponse GetAllVendorInvoiceList(VendorInvoiceRequestFilterModel model)
        {

            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllVendorInvoiceList(model));

            }
            catch (Exception ex)
            {


                string CustomException = "Path : api/VendorInvoiceApi/GetInvoicePreviewForVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured");
            }


        }

        [HttpGet]
        [Route("api/VendorInvoiceApi/GetPhaseAndResourceforProject/{ProjectId}/{VendorId}")]
        public JsonResponse GetPhaseAndResourceforProject(int ProjectId, int VendorId)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetPhaseAndResourceforProject(ProjectId, VendorId));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/GetPhaseforProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ; ;
            }
        }

        [HttpGet]
        [Route("api/VendorInvoiceApi/GetVendorInvoicePreview/{Id}/{CompanyId}")]
        public JsonResponse GetVendorInvoicePreview(int Id, int CompanyId)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorInvoicePreview(CompanyId, Id));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorApi/GetPhaseforProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ; ;
            }
        }

        [HttpPost]
        [Route("api/VendorInvoiceApi/SaveApproveVendorInvoice")]
        public JsonResponse SaveApproveVendorInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            try
            {
                objBll = new VendorInvoice();
                var mod = objBll.SaveApproveVendorInvoice(model);
                return ControllerExtensions.JsonCustomSuccess(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorInvoiceApi/SaveApproveVendorInvoice => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }
        }

        [HttpPost]
        [Route("api/VendorInvoiceApi/RejectInvoice")]
        public JsonResponse RejectInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            try
            {
                objBll = new VendorInvoice();
                var mod = objBll.RejectVendorInvoice(model);
                return ControllerExtensions.JsonCustomSuccess(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorInvoiceApi/RejectInvoice => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }
        }

        [HttpGet]
        [Route("api/VendorInvoiceApi/GetCardDetailForVendorPayment/{PaymentDetailId}/{CompanyId}/{InvoiceId}")]
        public JsonResponse GetCardDetailForVendorPayment(int PaymentDetailId, int CompanyId, int InvoiceId)
        {
            try
            {
                objBll = new VendorInvoice();
                int StripeApplicationFee = Convert.ToInt32(ConfigurationManager.AppSettings["StripeApplicationFee"]);
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCardDetailForVendorPayment(PaymentDetailId, CompanyId, InvoiceId, StripeApplicationFee));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/VendorInvoiceApi/GetCardDetailForVendorPayment => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }

        [HttpGet]
        [Route("api/VendorInvoiceApi/ProjectForInvoiceDrop/{CompanyId}/{VendorId}")]
        public JsonResponse ProjectForInvoiceDrop(int CompanyId,int VendorId)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.ProjectForInvoiceDrop(CompanyId, VendorId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorInvoiceApi/ProjectForInvoiceDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ;

            }
        }

        [HttpGet]
        [Route("api/VendorInvoiceApi/GetVendorForDrop/{CompanyId}")]
        public JsonResponse GetVendorForDrop(int CompanyId)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorForDrop(CompanyId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorInvoiceApi/GetVendorForDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ;

            }

        }
      
        [HttpGet]
        [Route("api/VendorInvoiceApi/GetCompanyForDrop/{CompanyId}")]
        public JsonResponse GetCompanyForDrop(int CompanyId)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyForDrop(CompanyId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorInvoiceApi/GetCompanyForDrop => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ;

            }

        }

        #region Paging

        [HttpPost]
        [Route("api/VendorInvoiceApi/GetAllVendorInvoiceListOutstanding")]
        public JsonResponse GetAllVendorInvoiceListOutstanding(VendorInvoiceRequestFilterModel model)
        {

            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllVendorInvoiceListOutstanding(model));

            }
            catch (Exception ex)
            {


                string CustomException = "Path : api/VendorInvoiceApi/GetAllVendorInvoiceListOutstanding => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured");
            }


        }

        [HttpPost]
        [Route("api/VendorInvoiceApi/GetAllVendorInvoiceListPaid")]
        public JsonResponse GetAllVendorInvoiceListPaid(VendorInvoiceRequestFilterModel model)
        {

            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllVendorInvoiceListPaid(model));

            }
            catch (Exception ex)
            {


                string CustomException = "Path : api/VendorInvoiceApi/GetAllVendorInvoiceListPaid => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured");
            }


        }

        [HttpPost]
        [Route("api/VendorInvoiceApi/GetAllVendorInvoiceListRejected")]
        public JsonResponse GetAllVendorInvoiceListRejected(VendorInvoiceRequestFilterModel model)
        {

            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllVendorInvoiceListRejected(model));

            }
            catch (Exception ex)
            {


                string CustomException = "Path : api/VendorInvoiceApi/GetAllVendorInvoiceListRejected => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured");
            }


        }

        [HttpPost]
        [Route("api/VendorInvoiceApi/GetAllInvoiceDetailListOutstanding/")]
        public JsonResponse GetAllInvoiceDetailListOutstanding(InvoiceModel model)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllInvoiceDetailListOutstanding(model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorInvoiceApi/GetAllInvoiceDetailListOutstanding => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ; ;
            }
        }

        [HttpPost]
        [Route("api/VendorInvoiceApi/GetAllInvoiceDetailListPaid/")]
        public JsonResponse GetAllInvoiceDetailListPaid(InvoiceModel model)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllInvoiceDetailListPaid(model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorInvoiceApi/GetAllInvoiceDetailListPaid => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ; ;
            }
        }

        [HttpPost]
        [Route("api/VendorInvoiceApi/GetAllInvoiceDetailListRejected/")]
        public JsonResponse GetAllInvoiceDetailListRejected(InvoiceModel model)
        {
            try
            {
                objBll = new VendorInvoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllInvoiceDetailListRejected(model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/VendorInvoiceApi/GetAllInvoiceDetailListRejected => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !"); ; ;
            }
        }
        #endregion
    }
}