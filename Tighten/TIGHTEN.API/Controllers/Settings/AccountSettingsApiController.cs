﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class AccountSettingsApiController : ApiController
    {
        AccountSettings objBll;

        [HttpGet]
        [Route("api/AccountSettingsApi/{UserId}")]
        public JsonResponse Get(string UserId)
        {
            try
            {
                objBll = new AccountSettings();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAccountSettings(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/AccountSettingsApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpPost]
        [Route("api/AccountSettingsApi")]
        public JsonResponse Post(MODEL.AccountSettingsModel model)
        {
            try
            {
                objBll = new AccountSettings();
                return ControllerExtensions.JsonCustomSuccess(objBll.UpdateAccountSettings(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/AccountSettingsApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpPut]
        [Route("api/AccountSettingsApi/UpdateUserRoutingNumber")]
        public JsonResponse UpdateUserRoutingNumber(MODEL.AccountSettingsModel model)
        {
            try
            {
                objBll = new AccountSettings();
                objBll.UpdateUserRoutingNumber(model);
                return ControllerExtensions.JsonSuccess("User Routing Number updated successfully !!");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/AccountSettingsApi/UpdateUserRoutingNumber => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpPost]
        [Route("api/AccountSettingsApi/UpdateUserRoutingNumberVendorAdminProfile")]
        public JsonResponse UpdateUserRoutingNumberVendorAdminProfile(MODEL.AccountSettingsModel model)
        {
            try
            {
                objBll = new AccountSettings();
                return ControllerExtensions.JsonCustomSuccess(objBll.UpdateUserRoutingNumberVendorAdminProfile(model));
                
                
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/AccountSettingsApi/UpdateUserRoutingNumberVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpPost]
        [Route("api/AccountSettingsApi/UpdateUserRoutingNumberVendorPOCsProfile")]
        public JsonResponse UpdateUserRoutingNumberVendorPOCsProfile(MODEL.AccountSettingsModel model)
        {
            try
            {
                objBll = new AccountSettings();
                return ControllerExtensions.JsonCustomSuccess(objBll.UpdateUserRoutingNumberVendorPOCsProfile(model));


            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/AccountSettingsApi/UpdateUserRoutingNumberVendorAdminProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }
    }
}
