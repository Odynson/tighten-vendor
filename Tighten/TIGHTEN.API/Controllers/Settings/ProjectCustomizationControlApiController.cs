﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL.Settings;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers.Settings
{
    public class ProjectCustomizationControlApiController : ApiController
    {
        // GET: api/ProjectCustomizationControlApi

        ProjectCustomizationControlBLL OBjBLL;

        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }



        [Route("api/ProjectCustomizationControlApi")]
        public JsonResponse Post(CustomControlModel Model)
        {
            try
            {
                OBjBLL = new ProjectCustomizationControlBLL();
                return ControllerExtensions.JsonCustomSuccess(OBjBLL.InsertCustomControl(Model));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/ProjectCustomizationControlApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");
            }




        }
        [HttpGet]
        [Route("api/ProjectCustomizationControlApi/GetCustomControl/{CompanyId}")]
        public JsonResponse GetCustomControl(int CompanyId)
        {
            try
            {
                OBjBLL = new ProjectCustomizationControlBLL();
                return ControllerExtensions.JsonCustomSuccess(OBjBLL.GetCustomControl(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectCustomizationControlApi/GetCustomControl/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");

            }


        }

        [HttpPut]
        [Route("api/ProjectCustomizationControlApi/deleteCustomControl")]
        public JsonResponse deleteCustomControl(CustomControlDelete Model)
        {
            try
            {
                OBjBLL = new ProjectCustomizationControlBLL();
                return ControllerExtensions.JsonCustomSuccess(OBjBLL.DeleteCustomControlOrActive(Model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ProjectCustomizationControlApi/ProjectCustomizationControlApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured !");

            }


        }

        [HttpPost]
        [Route("api/ProjectCustomizationControlApi/UpdatePriority")]
        public JsonResponse UpdatePriority(MODEL.updatePriorityModel model)
        {
            try
            {

                OBjBLL = new ProjectCustomizationControlBLL();
                return ControllerExtensions.JsonCustomSuccess(OBjBLL.UpdatePriority(model));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/ProjectCustomizationControlApi/UpdatePriority => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


    }
}

