﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class SettingsApiController : ApiController
    {
        TIGHTEN.BUSINESS.Settings objBll;

        [Route("api/SettingsApi/{UserId}/{CompanyId}/{ProjectId}/{RoleId}")]
        public JsonResponse Get(string UserId, int CompanyId, int ProjectId, int RoleId)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllEmailSettingsOption(UserId, CompanyId, ProjectId, RoleId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/SettingsApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpPost]
        [Route("api/SettingsApi")]
        public JsonResponse Post(MODEL.GetUserNotificationConfigModel model)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.ChangeEmailSettingsOption(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/SettingsApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/SettingsApi/GetProjectsForCurrentCompany/{UserId}/{CompanyId}")]
        public JsonResponse GetProjectsForCurrentCompany(string UserId, int CompanyId)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectsForCurrentCompany(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/SettingsApi/GetProjectsForCurrentCompany => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpGet]
        [Route("api/SettingsApi/GetEmailSettingsForProjectLevel/{UserId}/{CompanyId}")]
        public JsonResponse GetEmailSettingsForProjectLevel(string UserId, int CompanyId)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetEmailSettingsForProjectLevel(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/SettingsApi/GetEmailSettingsForProjectLevel/GetEmailSettingsForProjectLevel => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpPost]
        [Route("api/SettingsApi/ChangeAllProjectSetting")]
        public JsonResponse ChangeAllProjectSetting(MODEL.ChangeSettingsModel model)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.ChangeAllProjectSetting(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/SettingsApi/ChangeAllProjectSetting => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpGet]
        [Route("api/SettingsApi/GetEmailSettingsForTodoLevel/{UserId}/{CompanyId}")]
        public JsonResponse GetEmailSettingsForTodoLevel(string UserId, int CompanyId)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetEmailSettingsForTodoLevel(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/SettingsApi/GetEmailSettingsForTodoLevel => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpPost]
        [Route("api/SettingsApi/ChangeAllTodoSetting")]
        public JsonResponse ChangeAllTodoSetting(MODEL.ChangeSettingsModel model)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.ChangeAllTodoSetting(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/SettingsApi/ChangeAllTodoSetting => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpPost]
        [Route("api/SettingsApi/ChangeProjectSetting")]
        public JsonResponse ChangeProjectSetting(MODEL.EmailSettingsModelForProjectLevel model)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.ChangeProjectSetting(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/SettingsApi/ChangeProjectSetting => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpPost]
        [Route("api/SettingsApi/changeTodoSetting")]
        public JsonResponse changeTodoSetting(MODEL.EmailSettingsModelForTodoLevel model)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.changeTodoSetting(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/SettingsApi/changeTodoSetting => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpPost]
        [Route("api/SettingsApi/ChangeVendorSetting/{CompanyId}/{IsVendor}")]
        public JsonResponse ChangeVendorSetting(int CompanyId, bool IsVendor)
        {
            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.ChangeVendorSetting(CompanyId, IsVendor));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : API/SettingsApi/ChangeVendorSetting/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

          

        }

        [HttpGet]
        [Route("api/SettingsApi/getVendorSetting/{UserId}/{CompanyId}")]
        public JsonResponse GetVendorSetting(string UserId, int CompanyId)
        {

            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorSetting(UserId, CompanyId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : API/SettingsApi/getVendorSetting/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException)); ;
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }

        [HttpPost]
        [Route("api/SettingsApi/saveUpdateVendorWorkingArea")]
       public JsonResponse SaveUpdateVendorWorkingArea(MODEL.VendorMainSettingsModel model )
        {
            try
                               {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.SaveUpdateVendorWorkingArea(model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : API/SettingsApi/saveUpdateVendorWorkingArea/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException)); ;
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }


        [HttpGet]
        [Route("api/SettingsApi/GetVendorSelectedAreas/{CompanyId}")]
        public JsonResponse GetVendorSelectedAreas(int CompanyId)
        {

            try
            {
                objBll = new TIGHTEN.BUSINESS.Settings();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorSelectedAreas( CompanyId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : API/SettingsApi/GetVendorSelectedAreas/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException)); ;
                return ControllerExtensions.JsonError("Some Error Occured !");
            }


        }





    }
}
