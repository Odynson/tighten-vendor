﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API
{
    public class FeedbackApiController : ApiController
    {
        Feedback objBll;

        /// <summary>
        /// Getting User's projects in which user is Member 
        /// </summary>
        /// <returns>It returns User's Projects in it as an array</returns>
        /// 
        [HttpGet]
        [Route("api/FeedbackApi/{CompanyId}/{UserId}")]
        public JsonResponse Get(int CompanyId, string UserId)
        {
            try
            {
                objBll = new Feedback();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjects(CompanyId, UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FeedbackApi/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [HttpGet]
        [Route("api/FeedbackApi/ShowAllFeedbacksOfProject/{ProjectId}/{UserId}")]
        public JsonResponse ShowAllFeedbacksOfProject(int ProjectId, string UserId)
        {
            try
            {
                objBll = new Feedback();
                return ControllerExtensions.JsonCustomSuccess(objBll.ShowAllFeedbacksOfProject(ProjectId, UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FeedbackApi/ShowAllFeedbacksOfProject => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpPost]
        [Route("api/FeedbackApi/ShowOnProfile")]
        public JsonResponse ShowOnProfile(MODEL.FeedbackModel.ShowOnProfileModel model )
        {
            try
            {
                objBll = new Feedback();
                objBll.ShowOnProfile(model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FeedbackApi/ShowOnProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("Feedbacks are marked to show or not on profile");
        }




    }
}
