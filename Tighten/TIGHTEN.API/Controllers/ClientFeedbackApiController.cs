﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using TIGHTEN.BUSINESS;

namespace TIGHTEN.API
{
    public class ClientFeedbackApiController : ApiController
    {
        ClientFeedback objBll;


        /// <summary>
        /// THis funtions fetches the Feedbacks associated to a user
        /// </summary>
        /// <returns>List of feedbacks in json format</returns>
        /// 
        [Route("api/ClientFeedbackApi/{userId}")]
        public JsonResponse Get(string userId)
        {
            try
            {
                objBll = new ClientFeedback();

                return ControllerExtensions.JsonCustomSuccess(objBll.getClientFeedbacks(userId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ClientFeedbackApi/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");

            }
        }

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        public JsonResponse Post(ClientFeedbackModel.ClientFeedbackAddModel model, string userId)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {
                objBll = new ClientFeedback();

                objBll.SaveClientFeedback(userId, model);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ClientFeedbackApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

            return ControllerExtensions.JsonSuccess("Feedback Saved Successfully !");
        }

        // PUT api/<controller>/5


        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}