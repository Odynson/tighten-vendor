﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class FreelancersHiredDirectiveAPIController : ApiController
    {
        FreelancersHiredDirective ObjBll;

        [HttpGet]
        [Route("api/FreelancersHiredDirectiveAPI/GetFreelancersHired/{UserId}/{CompanyId}")]
        public JsonResponse GetFreelancersHired(string UserId, int CompanyId)
        {
            try
            {
                ObjBll = new FreelancersHiredDirective();
                return ControllerExtensions.JsonCustomSuccess(ObjBll.GetFreelancersHired(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/FreelancersHiredDirectiveAPI/GetFreelancersHired => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



    }
}
