﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class PendingtodosCountDirectiveApiController : ApiController
    {

        PendingtodosCountDirective objBll;

        /// <summary>
        /// Getting All Projects 
        /// </summary>
        /// <returns>Returns All Projects </returns>
        [HttpGet]
        [Route("api/PendingtodosCountDirectiveApi/GetUserPendingtodos/{UserId}/{CompanyId}")]
        public JsonResponse GetUserCompletedTodos(string UserId, int CompanyId)
        {
            try
            {
                //int a = 0;
                //int b = 10 / a;

                objBll = new PendingtodosCountDirective();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserPendingtodos(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/PendingtodosCountDirectiveApi/GetUserPendingtodos => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


    }
}
