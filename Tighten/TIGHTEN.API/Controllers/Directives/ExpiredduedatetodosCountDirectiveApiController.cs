﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers.Directives
{
    public class ExpiredduedatetodosCountDirectiveApiController : ApiController
    {
        ExpiredduedatetodosCountDirective objBll;

        /// <summary>
        /// Getting All Projects 
        /// </summary>
        /// <returns>Returns All Projects </returns>
        [HttpGet]
        [Route("api/ExpiredduedatetodosCountDirectiveApi/GetUserExpiredduedatetodos/{UserId}/{CompanyId}")]
        public JsonResponse GetUserExpiredduedatetodos(string UserId, int CompanyId)
        {
            try
            {
                objBll = new ExpiredduedatetodosCountDirective();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserExpiredduedatetodos(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ExpiredduedatetodosCountDirectiveApi/GetUserExpiredduedatetodos => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



    }
}
