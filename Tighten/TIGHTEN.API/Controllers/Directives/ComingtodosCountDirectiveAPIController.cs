﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class ComingtodosCountDirectiveAPIController : ApiController
    {

        ComingtodosCountDirective objBll;

        /// <summary>
        /// Getting All Projects 
        /// </summary>
        /// <returns>Returns All Projects </returns>
        [HttpGet]
        [Route("api/ComingtodosCountDirectiveAPI/GetUserComingtodos/{UserId}/{CompanyId}")]
        public JsonResponse GetUserComingtodos(string UserId, int CompanyId)
        {
            try
            {
                //int a = 0;
                //int b = 10 / a;

                objBll = new ComingtodosCountDirective();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserComingtodos(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ComingtodosCountDirectiveAPI/GetUserComingtodos => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


    }
}
