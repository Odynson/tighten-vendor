﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class TopFiveTasksDirectiveAPIController : ApiController
    {
        TopFiveTasksDirective ObjBll;


        [Route("api/TopFiveTasksDirectiveAPI/GetTopFiveTasks/{UserId}/{CompanyId}")]
        public JsonResponse GetTopFiveTasks(string UserId, int CompanyId)
        {
            try
            {
                ObjBll = new TopFiveTasksDirective();
                return ControllerExtensions.JsonCustomSuccess(ObjBll.GetTopFiveTasks(UserId, CompanyId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/TopFiveTasksDirectiveAPI/GetTopFiveTasks => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

        }





    }
}
