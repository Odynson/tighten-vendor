﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class OverBudgetedProjectsCountDirectiveAPIController : ApiController
    {

        OverBudgetedProjectsCountDirective objBll;

        /// <summary>
        /// Getting All Projects 
        /// </summary>
        /// <returns>Returns All Projects </returns>
        [HttpGet]
        [Route("api/OverBudgetedProjectsCountDirectiveAPI/GetUserOverBudgetedProjects/{UserId}/{CompanyId}")]
        public JsonResponse GetUserOverBudgetedProjects(string UserId, int CompanyId)
        {
            try
            {
                //int a = 0;
                //int b = 10 / a;

                objBll = new OverBudgetedProjectsCountDirective();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserOverBudgetedProjects(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/OverBudgetedProjectsCountDirectiveAPI/GetUserOverBudgetedProjects => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


    }
}
