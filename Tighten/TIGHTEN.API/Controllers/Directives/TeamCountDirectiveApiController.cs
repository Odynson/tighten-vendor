﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class TeamCountDirectiveApiController : ApiController
    {

        TeamCountDirective objBll;

        /// <summary>
        /// It fetches Company Teams With Team Members
        /// </summary>
        /// <param name="id">It is unique Id of Team</param>
        /// <returns>It returns Teams is Json Format</returns>
        [HttpGet]
        [Route("api/TeamCountDirectiveAPI/CompanyTeams/{CompanyId}/{UserId}")]
        public JsonResponse CompanyTeams(int CompanyId, string UserId)
        {
            try
            {
                objBll = new TeamCountDirective();
                return ControllerExtensions.JsonCustomSuccess(objBll.getCompanyTeams(CompanyId, UserId));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/teamapi/CompanyTeams => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");

            }

        }





    }
}
