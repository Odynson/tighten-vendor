﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.UTILITIES;
using TIGHTEN.BUSINESS.CompanyAccount;
using static TIGHTEN.MODEL.CompanyAccount.AccountDocumentsModel;
using TIGHTEN.MODEL.CompanyAccount;
using System.Configuration;
using System.Web;
using System.IO;
using TIGHTEN.BUSINESS;
using System.Net.Http.Headers;
using System.Web.Http.Cors;
using TIGHTEN.MODEL;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO.Compression;
namespace TIGHTEN.API.Controllers.CompanyAccount
{
    [EnableCors("*", "*", "*")]
    public class CompanyAccountApiController : ApiController
    {
        CompanyAccountBLL objBll;
        [HttpGet]
        [Route("api/CompanyAccountApi/GetCompanyAccountList/{CompanyId}")]
        public JsonResponse GetCompanyAccountList(int CompanyId)
        {

            try
            {
                objBll = new CompanyAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyAccountList(CompanyId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/GetCompanyAccountList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !"+ CustomException);
            }


        }

        [HttpGet]
        [Route("api/CompanyAccountApi/GetVendorList/{CompanyId}")]
        public JsonResponse GetVendorList(int CompanyId)
        {

            try
            {
                objBll = new CompanyAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorList(CompanyId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/GetVendorList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }


        [HttpGet]
        [Route("api/CompanyAccountApi/GetVendorPOCList/{CompanyId}/{VendorId}")]
        public JsonResponse GetVendorPOCList(int CompanyId,int VendorId)
        {

            try
            {
                objBll = new CompanyAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetVendorPOCList(CompanyId, VendorId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/GetVendorList => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }
        [HttpPost]
        /// <summary>
        /// It saves the new Account
        /// </summary>
        /// <param name="model">Model consists the new values of Account, POCs and Documnets</param>
        [Route("api/CompanyAccountApi/SaveNewAccount")]
        public async Task<JsonResponse> SaveNewAccount()
        {
            AccountModel obj = new AccountModel();
            var AbsRoot = "~/Uploads/Attachments/Account_Attachments/";
            string RelativeUrl = "Attachments/Account_Attachments/";

            var StoragePath = HttpContext.Current.Server.MapPath(AbsRoot);
            if (Request.Content.IsMimeMultipartContent())
            {
                objBll = new CompanyAccountBLL();
                try
                {
                    var ProjModal = HttpContext.Current.Request.Params["model"];


                    string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                    AccountModel ObjAccountModal = JsonConvert.DeserializeObject<AccountModel>(ProjModal);
                    int Projectcheck = objBll.CheckAccountExist(ObjAccountModal.CompanyId, ObjAccountModal.TeamId, ObjAccountModal.VendorId);
                    string msg = objBll.SaveAccountDetail(ObjAccountModal, url);
                    if (msg != "-1")
                    {
                        int AccId = Convert.ToInt32(msg);
                        if (!Directory.Exists(StoragePath + "Upload/"+ AccId))
                        {
                            Directory.CreateDirectory(StoragePath + "Upload/"+ AccId);
                        }
                        
                        var streamProvider = new MultipartFormDataStreamProvider(Path.Combine(StoragePath, "Upload/"+ AccId));

                        StoragePath = StoragePath + "Upload/" + AccId;



                        if (Projectcheck == 1)
                        {


                            var formData = streamProvider.FormData;
                            await Request.Content.ReadAsMultipartAsync(streamProvider);
                            int index = 0;
                            foreach (MultipartFileData fileData in streamProvider.FileData)
                            {
                                if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                                {

                                    return ControllerExtensions.JsonCustomSuccess("This request is not properly formatted");
                                    //  return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                                }
                                string ActualFileName = fileData.Headers.ContentDisposition.FileName;
                                if (ActualFileName.StartsWith("\"") && ActualFileName.EndsWith("\""))
                                {
                                    ActualFileName = ActualFileName.Trim('"');
                                }
                                if (ActualFileName.Contains(@"/") || ActualFileName.Contains(@"\"))
                                {
                                    ActualFileName = Path.GetFileName(ActualFileName);
                                }
                                string strExtension = Path.GetExtension(ActualFileName).ToLower();
                                Guid guid = Guid.NewGuid();
                                string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;

                                File.Move(fileData.LocalFileName, Path.Combine(StoragePath, FinalFilename));
                                //ActualFileName = StoragePath + "Upload/" + AccId + "/"+FinalFilename;
                                string UploadFileName = JsonConvert.DeserializeObject<string>(fileData.Headers.ContentDisposition.Name);
                                if (ObjAccountModal.AccountDocumentList == null) ObjAccountModal.AccountDocumentList = new List<AccountDocumentsModel>();
                                if (!string.IsNullOrEmpty(UploadFileName) && UploadFileName == "Project")
                                {
                                    AccountDocumentsModel tempDocObj = new AccountDocumentsModel();
                                    tempDocObj.ActualFileName = ActualFileName;
                                    tempDocObj.Name = FinalFilename;
                                    tempDocObj.Path = RelativeUrl+"Upload/" + AccId + "/" + FinalFilename;
                                    ObjAccountModal.AccountDocumentList.Add(tempDocObj);
                                }
                                else
                                {
                                    string[] TempIndex = UploadFileName.Split('_');
                                    int phaseIndex = Convert.ToInt32(TempIndex[1]);
                                    int PhaseNameIndex = Convert.ToInt32(TempIndex[2]);
                                    ObjAccountModal.AccountDocumentList[index].ActualFileName = ActualFileName;
                                    ObjAccountModal.AccountDocumentList[index].Name = FinalFilename;
                                    ObjAccountModal.AccountDocumentList[index].Path = RelativeUrl + FinalFilename;
                                }
                                index++;
                            }



                            //string Emailurl = ConfigurationManager.AppSettings["WebURL"].ToString();
                            // var data = ControllerExtensions.JsonCustomSuccess(objBll.SaveNewProject(ObjAccountModal));
                            // Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                            //return Request.CreateResponse(HttpStatusCode.OK, data);

                            return ControllerExtensions.JsonCustomSuccess(objBll.saveAccountChildDetail(ObjAccountModal,msg));
                        }
                        else
                        {
                            Request.Content.Headers.Expires = new DateTimeOffset(DateTime.Now.AddSeconds(50000));
                            return ControllerExtensions.JsonCustomSuccess(-1);

                        }

                            

                        

                    }
                    return ControllerExtensions.JsonCustomSuccess(msg);

                }
                catch (Exception ex)
                {
                    string CustomException = "Path : api/CompanyAccountApi/SaveNewAccount => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                    return ControllerExtensions.JsonError("Some Error Occured !"+ CustomException);
                }
            }
            else
            {
                return ControllerExtensions.JsonError("This request is not properly formatted");

            }


            //if (!ModelState.IsValid)
            //{
            //    return ControllerExtensions.JsonModelErrorAPI(this);
            //}
            //try
            //{
            //    objBll = new CompanyAccountBLL();
            //    //string url = ConfigurationManager.AppSettings["WebURL"].ToString();
            //    return ControllerExtensions.JsonCustomSuccess(objBll.saveAccount(obj));
            //}
            //catch (Exception ex)
            //{
            //    string CustomException = "Path : api/CompanyAccountApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
            //    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

            //    return ControllerExtensions.JsonCustomError();
            //}

        }
        [HttpGet]
        [Route("api/CompanyAccountApi/DownloadDocs/{accid}")]
        public HttpResponseMessage DownloadDocs(int accid)
        {
            HttpResponseMessage Response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                var AbsRoot = "~/Uploads/Attachments/Account_Attachments/";
                string RelativeUrl = "Attachments/Account_Attachments/";

                var StoragePath = HttpContext.Current.Server.MapPath(AbsRoot);
                string targetfile = "";
                string destFile = "";
                string[] filePaths = Directory.GetFiles(StoragePath + "Upload/" + accid);
                if(!Directory.Exists(StoragePath + "Upload"))
                {
                    Directory.CreateDirectory(StoragePath + "Upload");
                }
                string zipPath = StoragePath + "Upload\\Docs.zip";
                if (File.Exists(zipPath))
                {
                    File.Delete(zipPath);
                }
                ZipArchive zip = ZipFile.Open(zipPath, ZipArchiveMode.Create);
                foreach (var filename in filePaths)
                {
                    zip.CreateEntryFromFile(targetfile = System.IO.Path.GetFullPath(filename), Path.GetFileName(filename), CompressionLevel.Optimal);

                }

                zip.Dispose();




                //S2:Read File as Byte Array
                byte[] fileData = File.ReadAllBytes(zipPath);

                if (fileData == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                //S3:Set Response contents and MediaTypeHeaderValue
                Response.Content = new ByteArrayContent(fileData);
                Response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "CompanyAccount_Attached_Documents_" + accid+".zip" };
                Response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/zip");

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/DownloadDocs => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

               // return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }
            return Response;

        }


        [HttpGet]
        [Route("api/CompanyAccountApi/GetCompanyAccount/{accid}")]
        public JsonResponse GetCompanyAccount(int accid)
        {

            try
            {
                objBll = new CompanyAccountBLL();
                
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyAccount(accid));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/GetCompanyAccount => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }
        public JsonResponse Delete(int id)
        {
            try
            {
                objBll = new CompanyAccountBLL();
                string msg = objBll.deleteCompanyAccountDetail(id);
                if (msg == "CompanyAccDetailDeletedSuccessfully")
                {
                    return ControllerExtensions.JsonSuccess(msg);
                }
                else
                {
                    return ControllerExtensions.JsonError(msg);
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/CompanyAccountApi/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        [HttpPut]
        public JsonResponse Put(AccountModel saveObj)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new CompanyAccountBLL();
                if (saveObj == null) saveObj = new AccountModel();
                string url = ConfigurationManager.AppSettings["WebURL"].ToString(); 
                //objBll.updateTeam(team.UserId, team.TeamModel, team.CompanyId);
                return ControllerExtensions.JsonCustomSuccess(objBll.updateCompanyAccountDetail(saveObj, url));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/CompanyAccountApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError("Error", "Some Error Occured !"+ CustomException);
            }

            //return ControllerExtensions.JsonSuccess("Team Updated Successfully !");
        }


        [HttpGet]
        [Route("api/CompanyAccountApi/GetSingleAttachmentownload/{attachementId}")]
        public HttpResponseMessage GetSingleAttachmentownload(int attachementId)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            HttpResponseMessage Response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                objBll = new CompanyAccountBLL();
                AccountDocumentsModel modal = objBll.getAccountDocDetail(attachementId);
                if (modal != null)
                {
                    var path = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/" + modal.Path);


                    if (File.Exists(path)) //Not found then throw Exception

                    {


                        //S2:Read File as Byte Array
                        byte[] fileData = File.ReadAllBytes(path);

                        if (fileData == null)
                            throw new HttpResponseException(HttpStatusCode.NotFound);
                        //S3:Set Response contents and MediaTypeHeaderValue
                        Response.Content = new ByteArrayContent(fileData);
                        Response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
                        Response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        //Response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue(System.Net.Mime.DispositionTypeNames.Inline);
                        Response.Content.Headers.ContentDisposition.FileName = modal.ActualFileName;
                    }
                    else
                    {


                    }
                }


            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/CompanyAccountApi/GetFiledownload => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

            }
            return Response;

        }
        [HttpGet]
        [Route("api/CompanyAccountApi/GetAttachedDocumentDetail/{attachementId}")]
        public JsonResponse GetAttachedDocumentDetail(int attachementId)
        {

            try
            {
                objBll = new CompanyAccountBLL();

                return ControllerExtensions.JsonCustomSuccess(objBll.getAccountDocDetail(attachementId));

            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/GetAttachedDocumentDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }
        [HttpGet]
        [Route("api/CompanyAccountApi/getAccountDetailByVendorId/{VendorId}")]
        public JsonResponse getAccountDetailByVendorId( int VendorId)
        {

            try
            {
                objBll = new CompanyAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.getAccountDetailByVendorId(VendorId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/getAccountDetailByVendorId => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }
        //[HttpPost]
        //[Route("api/CompanyAccountApi/getMyEnterprises/")]
        //public JsonResponse getMyEnterprises(AccountModel model)

        //{
        //    try
        //    {
        //        objBll = new CompanyAccountBLL();
        //        return ControllerExtensions.JsonCustomSuccess(objBll.GetMyVendor(model));
        //    }
        //    catch (Exception ex)
        //    {
        //        string CustomException = "Path : api/CompanyAccountApi/getMyEnterprises => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
        //        return ControllerExtensions.JsonError(CustomException);
        //    }


        //}

        [HttpGet]
        [Route("api/CompanyAccountApi/getAccountDetailByCompanyId/{CompanyId}/{VendorId}")]
        public JsonResponse getAccountDetailByCompanyId(int CompanyId,int VendorId)
        {

            try
            {
                objBll = new CompanyAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.getAccountDetailByCompanyId(CompanyId,VendorId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/getAccountDetailByVendorId => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }

        [HttpGet]
        [Route("api/CompanyAccountApi/getAccountDetailById/{Id}")]
        public JsonResponse getAccountDetailById(int Id)
        {

            try
            {
                objBll = new CompanyAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.getAccountDetailById(Id));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/getAccountDetailByVendorId => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }
        [HttpPost]
        [Route("api/CompanyAccountApi/GetMyEnterpriseByCompanyName/")]
        public JsonResponse GetMyEnterpriseByCompanyName(AccountModel model)

        {
            try
          {
                objBll = new CompanyAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetMyEnterpriseByCompanyName(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/CompanyAccountApi/GetMyEnterpriseByCompanyName => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError(CustomException);
            }


        }

        [HttpGet]
        [Route("api/CompanyAccountApi/GetCompanyAccByCompanyId/{CompanyId}")]
        public JsonResponse GetCompanyAccByCompanyId(int CompanyId)
        {

            try
            {
                objBll = new CompanyAccountBLL();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCompanyAccByCompanyId(CompanyId));


            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/CompanyAccountApi/GetCompanyAccByCompanyId => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !" + CustomException);
            }


        }
    }
}
