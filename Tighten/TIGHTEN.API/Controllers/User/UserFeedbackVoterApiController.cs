﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;


namespace TIGHTEN.API
{
    public class UserFeedbackVoterApiController : ApiController
    {
        UserFeedback objBll;
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        /// <summary>
        /// This method Updates the User Feedback Voter
        /// </summary>
        /// <param name="model">This model consists all new values of fields to be saved</param>
        /// <returns>Returns Success or Error</returns>
        public JsonResponse Put(UserFeedbackModel.NewUserFeedbackVoterUpdateModel model)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {
                objBll = new UserFeedback();
                objBll.UpdateUserFeedbackVoter(model.UserId, model.UserFeedbackVoterUpdateModel);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserFeedbackVoterApi/Put => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }


            return ControllerExtensions.JsonSuccess("Vote Updated Successfully !");


        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}