﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.AspNet.Identity.Owin;
//using Microsoft.Owin.Security;
//using Microsoft.Owin.Security.Cookies;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;


namespace TIGHTEN.API
{
    public class UsersAPIController : ApiController
    {
        UserAPI objBLL = new UserAPI();

        [System.Web.Http.HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/UsersAPI/UploadProfile")]
        public JsonResponse UploadProfile()
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedProfile"];
                string UserId = Convert.ToString(HttpContext.Current.Request.Params["UserId"]);
                // Validate the uploaded image(optional)
                var fileName = httpPostedFile.FileName;
                string strExtension = Path.GetExtension(fileName).ToLower();
                if (strExtension == ".jpg" || strExtension == ".png" || strExtension == ".jpeg" || strExtension == ".bmp")
                {
                    Guid guid = Guid.NewGuid();
                    string FinalFilename = guid.ToString().Substring(0, 8) + strExtension;

                    // Get the complete file path
                    //string path = ConfigurationManager.AppSettings["ImageServerPath"].ToString();
                    //path = path + "/TIGHTEN-WEB";
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/Profile/Original"), FinalFilename);

                    // Save the uploaded file to "Uploads" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    var thumbnailPath = HttpContext.Current.Server.MapPath("~/Uploads/Profile/Thumbnail/" + FinalFilename);
                    var iconPath = HttpContext.Current.Server.MapPath("~/Uploads/Profile/Icon/" + FinalFilename); ;
                    Stream strm = httpPostedFile.InputStream;
                    GenerateThumbnails(0.07, strm, thumbnailPath);
                    GenerateIcon(0.07, strm, iconPath);
                    var PreviousPhoto = objBLL.updateUserProfilePhoto(FinalFilename, UserId);
                    if (PreviousPhoto != null)
                    {
                        File.Delete(HttpContext.Current.Server.MapPath("~/Uploads/Profile/Original/") + PreviousPhoto);
                        File.Delete(HttpContext.Current.Server.MapPath("~/Uploads/Profile/Thumbnail/") + PreviousPhoto);
                        File.Delete(HttpContext.Current.Server.MapPath("~/Uploads/Profile/Icon/") + PreviousPhoto);
                    }
                    return ControllerExtensions.JsonSuccess("Profile Updated Successfully");
                }
                else
                {
                    return ControllerExtensions.JsonError("You can only upload jpg,jpeg,png and bmp Files");
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UploadProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError(ex.Message);
            }
        }

        private void GenerateIcon(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(50);//(image.Width * scaleFactor);
                var newHeight = (int)(50);//(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }

        private void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(100);//(image.Width * scaleFactor);
                var newHeight = (int)(100);//(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }

        /// <summary>
        /// It gets All users of a Particular Company
        /// </summary>
        /// <returns>It returns all users of company in  Json Format</returns>
        [Route("api/UsersAPI/getUsers/{UserId}/{checkEmailConfirmed}/{CompanyId}")]
        public JsonResponse Get(string UserId, bool checkEmailConfirmed, int CompanyId )
        {
            //var userRoles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.getUsers(UserId, checkEmailConfirmed, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/getUsers => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// It gets All freelancers of a Particular Company
        /// </summary>
        /// <returns>It returns all freelancers of company in  Json Format</returns>
        [Route("api/UsersAPI/getFreelancers/{UserId}/{checkEmailConfirmed}/{CompanyId}")]
        public JsonResponse GetFreelancers(string UserId, bool checkEmailConfirmed, int CompanyId)
        {
            //var userRoles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetFreelancers(UserId, checkEmailConfirmed, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/getFreelancers => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [Route("api/UsersAPI/getActiveUsersCount/{CompanyId}")]
        public JsonResponse GetActiveUsersCount(int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetActiveUsersCount(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/GetActiveUsersCount => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }
        //

        /// <summary>
        /// It gets the User Details
        /// </summary>
        /// <param name="id">It is temporary Id</param>
        /// <returns>It returns the details of User in Json Format</returns>
        [Route("api/UsersAPI/{UserId}/{CompanyId}")]
        public JsonResponse Get(string UserId, int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.getUser(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// It gets the User Details
        /// </summary>
        /// <param name="id">It is temporary Id</param>
        /// <returns>It returns the details of User in Json Format</returns>
        [Route("api/UsersAPI/UserCardInfo/{UserId}/{CompanyId}")]
        public JsonResponse getUserCardInfo(string UserId, int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.getUserCardInfo(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/getUserCardInfo => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// It gets the User Details
        /// </summary>
        /// <param name="id">It is temporary Id</param>
        /// <returns>It returns the details of User in Json Format</returns>
        [HttpGet]
        [Route("api/UsersAPI/IsUserCardInfoExist/{UserId}/{CompanyId}")]
        public JsonResponse IsUserCardInfoExist(string UserId, int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.IsUserCardInfoExist(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/IsUserCardInfoExist => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        // POST api/<controller>
        /// <summary>
        /// This method will be used to add new user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //TODO : needs to rewrite the logic again.

        [HttpPost]
        [Route("api/UsersAPI")]
        public async Task<JsonResponse> Post(NewAddNewUserModel model)
        {
            //NEW Flow :
            //1. save data into detail table (Role will be saved in the data layer)
            //2. If data is saved sucessfully then send the confirmation email to the user

            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                String UserId = Convert.ToString(Guid.NewGuid());
                /*Save User to User Detail Table*/
                UserModel userDetail = objBLL.saveUser(UserId, model.UserId, model.NewUserModel);
                if (!string.IsNullOrEmpty(userDetail.userId))
                {
                    /*Send Email Confirmation link - User will set password first time using the link*/
                    //await sendAccountCreationConfirmationEmail(userDetail);

                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForsendAccountCreationConfirmationEmail(userDetail);
                    }
                    ));
                    childref.Start();
                }
                else
                {
                    //return ControllerExtensions.JsonCustomError(userDetail.firstName +" " + userDetail.lastName + " (" + userDetail.email + ") already exist in " + userDetail.CompanyName);
                    return ControllerExtensions.JsonCustomError(userDetail);
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonCustomError("Some Error Occured");
            }
            return ControllerExtensions.JsonSuccess();
        }

        // GET api/<controller>/5
        [System.Web.Http.HttpPut] // This is from System.Web.Http, and not from System.Web.Mvc
        [System.Web.Http.Route("API/UsersAPI/UpdateUserRole")]
        //Used to update the role. 
        public JsonResponse UpdateUserRole(NewUserRoleModel model)
        {
            //FLOW :
            //1. Update Role of the user w.r.t to a perticular company
            try
            {
                objBLL.UpdateUserRole(model.UserId, model.UserRoleModel);
                return ControllerExtensions.JsonSuccess("User Role Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UpdateUserRole => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
           
            //if (!ModelState.IsValid)
            //{
            //    return ControllerExtensions.JsonModelErrorAPI(this);
            //}
            //try
            //{
            /*Update role of User in role Table*/
            //commented by kapil
            //var userRoles = await UserManager.GetRolesAsync(model.UserId);
            //for (int i = 0; i < userRoles.Count; i++)
            //{
            //    IdentityResult resultroleupdate = await UserManager.RemoveFromRoleAsync(model.UserId, userRoles[i]);
            //}
            //IdentityResult resultsroleupdate = await UserManager.AddToRoleAsync(model.UserId, model.UserRole);
            //if (resultsroleupdate.Succeeded)
            //{
            //    return ControllerExtensions.JsonSuccess("User Role Updated Successfully !");
            //}
            //else
            //{
            //    return ControllerExtensions.JsonError("Some Error Occured !");
            //}
            //return ControllerExtensions.JsonError("Some Error Occured !");
            //}
            //catch (Exception ex)
            //{
            //    return ControllerExtensions.JsonError("Some Error Occured !");
            //}
        }

        [HttpPost]
        [Route("api/UsersAPI/updateDefaultTeam")]
        public JsonResponse UpdateDefaultTeam(UserModel model)
        {
            if (model.updateflag == 2)
            {
                try
                {
                    objBLL.updateSelectedTeam(model.userId, model);
                    return ControllerExtensions.JsonSuccess();
                }
                catch (Exception ex)
                {
                    string CustomException = "Path : api/UsersAPI/updateDefaultTeam => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                    return ControllerExtensions.JsonError("Some Error Occured !");
                }
            }
            return ControllerExtensions.JsonSuccess();
        }

        [HttpPost]
        [Route("api/UsersAPI/updateUserProfile")]
        public JsonResponse UpdateUserProfile(UserModel model)
        {
            if (model.updateflag == 4)
            {
                try
                {
                    objBLL.updateMyProfile(model.userId, model);
                    return ControllerExtensions.JsonSuccess("Profile Updated Successfully !");
                }
                catch (Exception ex)
                {
                    string CustomException = "Path : api/UsersAPI/updateUserProfile => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                    return ControllerExtensions.JsonError("Some Error Occured !");
                }
            }
            return ControllerExtensions.JsonSuccess();
        }

        // PUT api/<controller>/5
        /// <summary>
        /// Update an user according to flag supplied
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<JsonResponse> Put(string id, UserModel model)
        {
            /* Flag 1 is used to Delete profile Photo*/
            //if (model.updateflag == 1)
            //{
            //var PreviousPhoto = objBLL.clearProfilePhoto(User.Identity.GetUserId());
            //File.Delete(HttpContext.Current.Server.MapPath("~/Uploads/Profile/Original/") + PreviousPhoto);
            //File.Delete(HttpContext.Current.Server.MapPath("~/Uploads/Profile/Thumbnail/") + PreviousPhoto);
            //File.Delete(HttpContext.Current.Server.MapPath("~/Uploads/Profile/Icon/") + PreviousPhoto);
            //    //return ControllerExtensions.JsonSuccess();
            //}
            ///* Flag 2 is used to update Selected Team*/
            //else if (model.updateflag == 2)
            //{
            //try
            //{
            //    objBLL.updateSelectedTeam(User.Identity.GetUserId(), model);
            //    return ControllerExtensions.JsonSuccess();
            //}
            //catch
            //{
            //    return ControllerExtensions.JsonError("Some Error Occured !");
            //}

            //  }

            /* Flag 3 is used to update Send Notification Email*/
            //else if (model.updateflag == 3)
            //{
            //    //objBLL.updateSendEmailNotification(User.Identity.GetUserId(), model);
            //    //return ControllerExtensions.JsonSuccess();
            //}

            /* Flag 4 is used to update My Profile*/
            //else if (model.updateflag == 4)
            //{
            //    if (!ModelState.IsValid)
            //    {
            //        //return ControllerExtensions.JsonModelErrorAPI(this);
            //    }

            //    try
            //    {



            //objBLL.updateMyProfile(User.Identity.GetUserId(), model);
            //return ControllerExtensions.JsonSuccess("Profile Updated Successfully !");
            //    }
            //    catch
            //    {


            //        return ControllerExtensions.JsonError("Some Error Occured !");
            //    }
            //}

            //else
            //{
            //    if (!ModelState.IsValid)
            //    {
            //        return ControllerExtensions.JsonModelErrorAPI(this);
            //    }

            //    try
            //    {

            //User userentity = (from e in dbcontext.Users where e.Id == model.userId select e).SingleOrDefault();

            ///* Check if email has been changed */
            //if (!userentity.Email.Equals(model.email))
            //{

            //    User user = new User();
            //    user.UserName = model.email;
            //    user.EmailConfirmed = false;
            //    user.Email = model.email;
            //    user.Id = model.userId;
            //    user.AccessFailedCount = userentity.AccessFailedCount; // this will not be required

            //    user.LockoutEnabled = userentity.LockoutEnabled;
            //    user.PasswordHash = userentity.PasswordHash;
            //    user.SecurityStamp = userentity.SecurityStamp;

            //    IdentityResult result = await UserManager.UpdateAsync(user);

            //    if (result.Succeeded)
            //    {

            //        objBLL.updateUser(model);

            //        string code = await UserManager.GenerateEmailConfirmationTokenAsync(id);

            //        code = HttpUtility.UrlEncode(code);

            //        /* Send Email Change Confirmation link */

            //        await objBLL.sendEmailChangeConfirmationEmail(model.email, id, code);


            //    }
            //    else
            //    {
            //        return ControllerExtensions.JsonIdentityError(result);
            //    }
            //}
            //else
            //{
            //    objBLL.updateUser(model);
            //}


            // }
            //catch (Exception ex)
            //{
            //    return ControllerExtensions.JsonCustomError(ex.Message);
            //    //throw ex;
            //}
            return ControllerExtensions.JsonSuccess();
            // }
        }

        // DELETE api/<controller>/5
        /// <summary>
        /// delete an user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpDelete]
        [Route("api/UsersAPI/{userId}")]
        public JsonResponse Delete(string userId)
        {
            try
            {
                objBLL.DeleteUser(userId);
                return ControllerExtensions.JsonSuccess();
            }
            catch(Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/delete => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }


            //FLOW :
            //1. Delete user from User detail table
            //2. internally delete the Role and other details of the user

            //try
            //{
            //    var user = await UserManager.FindByIdAsync(id);
            //    var logins = user.Logins;

            //    /*Find Role of Current User*/
            //    var rolesForUser = await UserManager.GetRolesAsync(id);

            //    if (rolesForUser.Count() > 0)
            //    {
            //        foreach (var item in rolesForUser)
            //        {
            //            // item should be the name of the role
            //            /*Delete role information of User*/
            //            var result = await UserManager.RemoveFromRoleAsync(user.Id, item);
            //        }
            //    }

            //    /*Delete user using Identity*/
            //    await UserManager.DeleteAsync(user);

            //    /*Delete Entry from UserDetails table*/
            //    objBLL.deleteUser(id);

            //}
            //catch { return ControllerExtensions.JsonCustomError(); }
            //return ControllerExtensions.JsonSuccess();
        }
       
        public void ThreadForsendAccountCreationConfirmationEmail(UserModel userDetail)
        {
            var url = ConfigurationManager.AppSettings["WebURL"].ToString();
            var callbackUrl = url + "#/confirmemail/" + userDetail.userId + "/" + userDetail.EmailConfirmationCode + "/1";

            //String emailbody = "<p>"+ userDetail.firstName + " wants to invite you to become a member of "+ userDetail.CompanyName + " on tighten </p>";
            //emailbody += "<p>Please confirm your account by clicking this link and set your new password</p>";
            //emailbody += "<a href=\"" + callbackUrl + "\">link</a>";


            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "AccountCreationConfirmationEmail")
                                 //select r;
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var item in MailModule)
            {
                emailbody = item.mailbody;
                Subject = item.subject;
            }

            emailbody = emailbody.Replace("@@Admin_CompanyName", userDetail.CompanyName).Replace("@@callbackUrl", callbackUrl);


            /*Send Email to user and inform about new account created*/
            //await EmailUtility.Send(userDetail.email, "New Account Created", emailbody);

            EmailUtility.SendMailInThread(userDetail.email, Subject, emailbody);

        }

        /// <summary>
        /// Getting All Projects of User's company
        /// </summary>
        /// <param Name="CompanyId"> It is the unique id of the company whose projects will be fetched
        /// </param>
        [HttpPost]
        [Route("api/UsersAPI/GetAllProjects")]
        public JsonResponse Get(MODEL.FreelancerInfoToSendInvitationModel model)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetAllProjectsOfCompany(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/GetAllProjects => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }

        /// <summary>
        /// Sending Invitation to freelancer and saving details to FreelancerInvitation Table
        /// </summary>
        [HttpPost]
        [Route("api/UsersAPI/insertFreelancer")]
        public JsonResponse insertFreelancer(MODEL.FreelancerInvitationModel model)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["WebURL"].ToString();
                List<MODEL.MailSendToFreelancerOnInvitationSent> mail = objBLL.insertFreelancer(model, url);
                if (mail.Count() > 0)
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        objBLL.ThreadForSendMailToSendInvitationToFreelancer(model.EmailOrHandle, mail);
                    }
                    ));
                    childref.Start();

                }
                else
                {
                    if (model.invitationType == "email")
                    {
                       if (model.ProjectIdList.Count() == 0 )
                        {
                            return ControllerExtensions.JsonError("freelancerAllreadyAttachedToCompanyWithoutAnyProject");
                        }
                        else
                        {
                            return ControllerExtensions.JsonError("freelancerEmailAddressAllreadyExist");
                        }
                    }
                    else
                    {
                         if (model.ProjectIdList.Count() == 0)
                        {
                            return ControllerExtensions.JsonError("freelancerAllreadyAttachedToCompanyWithoutAnyProject");
                        }
                        else
                        {
                            return ControllerExtensions.JsonError("freelancerProjectAllreadyAssigned");
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/insertFreelancer => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }

            return ControllerExtensions.JsonSuccess("InvitationSentToFreelancer");
        }





        /// <summary>
        /// It gets the User Details
        /// </summary>
        /// <param name="id">It is temporary Id</param>
        /// <returns>It returns the details of User in Json Format</returns>
        [HttpGet]
        [Route("api/UsersAPI/GetCompanyAndFreelancerName/{invitationType}/{EmailOrHandle}/{CompanyId}")]
        public JsonResponse GetCompanyAndFreelancerName(string invitationType, string EmailOrHandle, int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetCompanyAndFreelancerName(invitationType, EmailOrHandle, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/GetCompanyAndFreelancerName => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [System.Web.Http.HttpPut] 
        [System.Web.Http.Route("API/UsersAPI/UpdateUserFirstName")]
        //Used to update the Users First Name. 
        public JsonResponse UpdateUserFirstName(UserModel model)
        {
            try
            {
                objBLL.UpdateUserFirstName(model);
                return ControllerExtensions.JsonSuccess("User First Name Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UpdateUserFirstName => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("API/UsersAPI/UpdateUserLastName")]
        //Used to update the Users Last Name. 
        public JsonResponse UpdateUserLastName(UserModel model)
        {
            try
            {
                objBLL.UpdateUserLastName(model);
                return ControllerExtensions.JsonSuccess("User Last Name Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UpdateUserLastName => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("API/UsersAPI/UpdateUserAddress")]
        //Used to update the Users Address. 
        public JsonResponse UpdateUserAddress(UserModel model)
        {
            try
            {
                objBLL.UpdateUserAddress(model);
                return ControllerExtensions.JsonSuccess("User Address Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UpdateUserAddress => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("API/UsersAPI/UpdateUserPhoneNo")]
        //Used to update the Users Phone No. 
        public JsonResponse UpdateUserPhoneNo(UserModel model)
        {
            try
            {
                objBLL.UpdateUserPhoneNo(model);
                return ControllerExtensions.JsonSuccess("User Phone Number Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UpdateUserPhoneNo => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("API/UsersAPI/UpdateUserDesignation")]
        //Used to update the Users Designation. 
        public JsonResponse UpdateUserDesignation(UserModel model)
        {
            try
            {
                objBLL.UpdateUserDesignation(model);
                return ControllerExtensions.JsonSuccess("User Designation Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UpdateUserDesignation => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("API/UsersAPI/UpdateUserAboutMe")]
        //Used to update the Users About Me. 
        public JsonResponse UpdateUserAboutMe(UserModel model)
        {
            try
            {
                objBLL.UpdateUserAboutMe(model);
                return ControllerExtensions.JsonSuccess("User AboutMe Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UpdateUserAboutMe => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("API/UsersAPI/UpdateUserGender")]
        //Used to update the Users Gender. 
        public JsonResponse UpdateUserGender(UserModel model)
        {
            try
            {
                objBLL.UpdateUserGender(model);
                return ControllerExtensions.JsonSuccess("User Gender Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UpdateUserGender => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("API/UsersAPI/UpdateUserDateOfBirth")]
        //Used to update the Users Date Of Birth. 
        public JsonResponse UpdateUserDateOfBirth(UserModel model)
        {
            try
            {
                objBLL.UpdateUserDateOfBirth(model);
                return ControllerExtensions.JsonSuccess("User Date Of Birth Updated Successfully !");
            }
            catch (Exception ex)
            {
                string CustomException = "Path : API/UsersAPI/UpdateUserDateOfBirth => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpGet]
        [Route("api/UsersAPI/CheckIfCompanyIsVendor/{CompanyId}")]
        public JsonResponse CheckIfCompanyIsVendor(int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.CheckIfCompanyIsVendor(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/CheckIfCompanyIsVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }



        [HttpGet]
        [Route("api/UsersAPI/UpdateGeneralCompanyToVendorCompany/{UserId}/{CompanyId}")]
        public JsonResponse UpdateGeneralCompanyToVendorCompany(string UserId, int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.UpdateGeneralCompanyToVendorCompany(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/UpdateGeneralCompanyToVendorCompany => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured ! "  + CustomException);
            }
        }

        [HttpGet]
        [Route("api/UsersAPI/GetUsersForTeam/{CompanyId}")]
        public JsonResponse GetUsersForTeam(int CompanyId)
        {
            try
            {
                return ControllerExtensions.JsonCustomSuccess(objBLL.GetUsersForTeam(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UsersAPI/GetUsersForTeam => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }


    }
}