﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;


namespace TIGHTEN.API
{
    public class UserFeedbackApiController : ApiController
    {
        UserFeedback objBll;


        /// <summary>
        /// Getting User Feedbacks with Voters in it as an array
        /// </summary>
        /// <returns>It returns User Feedbacks in json format with Voters in it as an array</returns>
        /// 
        [Route("api/UserFeedbackApi/{userId}")]
        public JsonResponse Get(string userId)
        {
            try
            {
                objBll = new UserFeedback();

                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserFeedbacks(userId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserFeedbackApi/get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}



        /// <summary>
        /// This method saves the User Feedback
        /// </summary>
        /// <param name="model">This model consists all new values of fields to be saved</param>
        /// <returns>Returns Success or Error</returns>
        public JsonResponse Post(UserFeedbackModel.NewUserFeedbackAddModel model)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }
            try
            {
                objBll = new UserFeedback();
                objBll.SaveUserFeedback(model.UserId, model.UserFeedbackAddModel);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserFeedbackApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
            return ControllerExtensions.JsonSuccess("User Feedback Saved Successfully !");
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}