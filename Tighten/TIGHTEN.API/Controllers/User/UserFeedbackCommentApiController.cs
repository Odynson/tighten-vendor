﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
//using Microsoft.AspNet.Identity;
using TIGHTEN.UTILITIES;


namespace TIGHTEN.API
{
    public class UserFeedbackCommentApiController : ApiController
    {
        UserFeedback objBll;


        /// <summary>
        ///  Fetching Comments for a particular User Feedback
        /// </summary>
        /// <param name="id">It is unique id of User Feedback</param>
        /// <returns>It returns list of Comments for a particular User Feedback</returns>
        public JsonResponse Get(int id)
        {
            try
            {

                objBll = new UserFeedback();

                return ControllerExtensions.JsonCustomSuccess(objBll.GetUserFeedbackComment(id));

            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserFeedbackCommentApi/Get => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        /// <summary>
        /// This method saves the User Feedback Comment for a particualr User Feedback
        /// </summary>
        /// <param name="model">This model consists all new values of fields to be saved</param>
        /// <returns>Returns Success or Error</returns>
        public JsonResponse Post(UserFeedbackModel.NewUserFeedbackCommentAddModel model)
        {
            if (!ModelState.IsValid)
            {
                return ControllerExtensions.JsonModelErrorAPI(this);
            }

            try
            {
                objBll = new UserFeedback();
                objBll.SaveUserFeedbackComment(model.UserId, model.UserFeedbackCommentAddModel);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/UserFeedbackCommentApi/Post => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }

            return ControllerExtensions.JsonSuccess("Commented Successfully !");


        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}