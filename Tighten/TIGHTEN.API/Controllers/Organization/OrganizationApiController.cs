﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers 
{
    public class OrganizationApiController : ApiController
    {
        Organization objBll;

        [Route("api/OrganizationApi/{UserId}")]
        public JsonResponse Get(string UserId)
        {
            try
            {
                objBll = new Organization();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllOrganizations(UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/OrganizationApi => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }

        [HttpGet]
        [Route("api/OrganizationApi/GetOrganization/{UserId}/{CompanyId}")]
        public JsonResponse GetOrganization(string UserId,int CompanyId)
        {
            try
            {
                objBll = new Organization();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetOrganization(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/OrganizationApi/GetOrganization => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }
        [HttpGet]
        [Route("api/OrganizationApi/GetOrganizationTeams")]
        public JsonResponse GetOrganizationTeams()
        {
            try
            {
                objBll = new Organization();
                return null;
                //return ControllerExtensions.JsonCustomSuccess(objBll.GetOrganization(UserId, CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/OrganizationApi/GetOrganization => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }



    }
}
