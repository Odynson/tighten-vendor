﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class InvoiceApiController : ApiController
    {
        Invoice objBll;

        [HttpPost]
        [Route("api/InvoiceApi")]
        public JsonResponse Post(MODEL.UserInvoice.InvoiceQuery model)
        {
            try
            {
                objBll = new Invoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInvoice(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpPost]
        [Route("api/InvoiceApi/GetInvoiceDetail")]
        public JsonResponse GetInvoiceDetail(MODEL.UserInvoice.AdminInvoiceDetailsRequiredParams model)
        {
            try
            {
                objBll = new Invoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetInvoiceDetail(model, ConfigurationManager.AppSettings["DefaultInvoiceNumber"].ToString()));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi/GetInvoiceDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }

        }

        [HttpPut]
        [Route("api/InvoiceApi")]
        public JsonResponse Put(MODEL.UserInvoice.InvoiceStatus model)
        {
            try
            {
                objBll = new Invoice();
                int StripeApplicationFee = Convert.ToInt32(ConfigurationManager.AppSettings["StripeApplicationFee"]);
                var mod = objBll.ChangeInvoiceStatus(model, StripeApplicationFee);
                //await sendInvoiceStatusChangeAndPayEmail(mod);
                return ControllerExtensions.JsonCustomSuccess(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }

        }

        //public async System.Threading.Tasks.Task<string> sendInvoiceStatusChangeAndPayEmail(MODEL.InvoiceModel mod)
        //{
        //    UserAPI objBLL = new UserAPI();
        //    var url = System.Configuration.ConfigurationManager.AppSettings["WebURL"].ToString();
        //    string emailbody = "Dear " + objBLL.getUserName(mod.UserId) + " <br/>";
        //    emailbody += "<p>Invoice you have sent for amount $" + mod.TotalAmount + " has been approved and paid to you by " + objBLL.getUserName(mod.ApprovedBy) + " .</p>";
        //    await EmailUtility.Send(objBLL.getUserEmail(mod.UserId), "Invoice Approved and Paid", emailbody);
        //    return "true";
        //}

        [HttpPost]
        [Route("api/InvoiceApi/GetProjectMembersForUser")]
        public JsonResponse GetProjectMembersForUser(MODEL.UserInvoice.ProjectMembersToForwardInvoice_QueryParams model)
        {
            try
            {
                objBll = new Invoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetProjectMembersForUser(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi/GetProjectMembersForUser => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }



        [Route("api/InvoiceApi/ForwardInvoice")]
        [HttpPost]
        public JsonResponse ForwardInvoice(MODEL.UserInvoice.ForwardInvoice model)
        {
            try
            {
                objBll = new Invoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.ForwardInvoice(model));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi/ForwardInvoice => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }

        }

        [HttpGet]
        [Route("api/InvoiceApi/{CompanyId}")]
        public JsonResponse Get(int CompanyId)
        {
            try
            {
                objBll = new Invoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.getAllFreelancerForAdmin(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpGet]
        [Route("api/InvoiceApi/GetCardDetailForPayment/{PaymentDetailId}/{CompanyId}/{InvoiceId}")]
        public JsonResponse GetCardDetailForPayment(int PaymentDetailId, int CompanyId, int InvoiceId)
        {
            try
             {
                objBll = new Invoice();
                int StripeApplicationFee = Convert.ToInt32(ConfigurationManager.AppSettings["StripeApplicationFee"]);
                return ControllerExtensions.JsonCustomSuccess(objBll.GetCardDetailForPayment(PaymentDetailId, CompanyId, InvoiceId, StripeApplicationFee));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi/GetCardDetailForPayment => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpGet]
        [Route("api/InvoiceApi/GetAllCardsForPaymentDetail/{CompanyId}")]
        public JsonResponse GetAllCardsForPaymentDetail(int CompanyId)
        {
            try
            {
                objBll = new Invoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.GetAllCardsForPaymentDetail(CompanyId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi/GetAllCardsForPaymentDetail => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }


        [HttpPut]
        [Route("api/InvoiceApi/OnlyApproveInvoice")]
        public JsonResponse OnlyApproveInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            try
            {
                objBll = new Invoice();
                var mod = objBll.ChangeInvoiceApprovedStatus(model);
                return ControllerExtensions.JsonCustomSuccess(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi/OnlyApproveInvoice => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }
        }



        [HttpPut]
        [Route("api/InvoiceApi/RejectInvoice")]
        public JsonResponse RejectInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            try
            {
                objBll = new Invoice();
                var mod = objBll.RejectInvoice(model);
                return ControllerExtensions.JsonCustomSuccess(mod);
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/InvoiceApi/RejectInvoice => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured");
            }
        }

        [HttpPost]
        [Route("api/InvoiceApi/SaveInvoiceByVendor")]
        public JsonResponse SaveInvoiceByVendor(InvoiceModel model )
        {
            try
            {
                objBll = new Invoice();
                return ControllerExtensions.JsonCustomSuccess(objBll.SaveInvoiceByVendor(model));
            }
            catch (Exception ex)
            {

                string CustomException = "Path : api/InvoiceApi/SaveInvoiceByVendor => Error : " + ex.Message + " , InnerException : " + ex.InnerException;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));
                return ControllerExtensions.JsonError("Some Error Occured");
            }


        }




    }
}
