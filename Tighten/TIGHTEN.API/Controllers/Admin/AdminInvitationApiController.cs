﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class AdminInvitationApiController : ApiController
    {

        AdminInvitation objBll;

        [HttpGet]
        [Route("api/AdminInvitationApi/{CompanyId}/{PageIndex}/{PageSizeSelected}")]
        public JsonResponse Get(int CompanyId,int PageIndex,int PageSizeSelected)
        {
            try
            {
                objBll = new AdminInvitation();
                return ControllerExtensions.JsonCustomSuccess(objBll.getAllSentRequest(CompanyId,PageIndex,PageSizeSelected));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/AdminInvitationApi => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("error");
            }
        }







    }
}
