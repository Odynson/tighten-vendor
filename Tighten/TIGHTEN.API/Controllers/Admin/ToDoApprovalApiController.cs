﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TIGHTEN.BUSINESS;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.API.Controllers
{
    public class ToDoApprovalApiController : ApiController
    {
        ToDoApproval objBll;

        [HttpPost]
        /// <summary>
        /// It gets MyTodos
        /// </summary>
        /// <returns>returns Success or Error</returns>
        /// 
        [Route("api/ToDoApproval/GetToDo")]
        public JsonResponse GetToDo(TodoApprovalSearch search)
        {
            try
            {
                objBll = new ToDoApproval();
                return ControllerExtensions.JsonCustomSuccess(objBll.getTodos(search));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoApproval/GetToDo => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [HttpPost]
        /// <summary>
        /// It gets MyTodos
        /// </summary>
        /// <returns>returns Success or Error</returns>
        /// 
        [Route("api/ToDoApproval/ApproveTodo")]
        public JsonResponse ApproveTodo(TodoApprovalUpdate search)
        {
            try
            {
                objBll = new ToDoApproval();
                return ControllerExtensions.JsonCustomSuccess(objBll.ApproveTodo(search));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoApproval/ApproveTodo => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

        [Route("api/ToDoApproval/getCompanyUsers/{CompanyId}/{RoleId}/{UserId}")]
        public JsonResponse getCompanyUsers(int CompanyId, int RoleId, string UserId)
        {
            try
            {
                objBll = new ToDoApproval();
                return ControllerExtensions.JsonCustomSuccess(objBll.CompanyUsers(CompanyId, RoleId, UserId));
            }
            catch (Exception ex)
            {
                string CustomException = "Path : api/ToDoApproval/getCompanyUsers => Error : " + ex.Message + " , InnerException : " + ex.InnerException ;
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(CustomException));

                return ControllerExtensions.JsonError("Some Error Occured !");
            }
        }

    }
}
