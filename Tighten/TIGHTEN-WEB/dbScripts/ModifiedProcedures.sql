
ALTER PROCEDURE [dbo].[Usp_GetCompanyTeams]     
@CompanyId int,
@UserId  nvarchar(max),
@PageIndex int,
@PageSizeSelected  int
AS
BEGIN
SET NOCOUNT ON;
 Declare @sql nvarchar(Max);
 Declare @sql1 nvarchar(Max);
Create Table #TempTable
(
TeamId int,
TeamName Nvarchar(max),
TeamBudget   money,
TeamDescription Nvarchar(max) ,
CreatedDate DateTime

)
  
  Create Table #TempTable1
(
TeamId int,
MemberEmail Nvarchar(max),
MemberName   Nvarchar(max),
IsActive bit,
MemeberProfilePhoto Nvarchar(max),
MemberUserId int
)
--declare	@CompanyId int = 1
--declare	@UserId  nvarchar(max) = 'e99ec481-b83f-45ce-8715-1635445dda74'
declare @UserRole int
set  @UserRole =  (select usrcom.RoleId from UserDetails user1  inner JOin UserCompanyRoles usrcom on user1.UserId = usrcom.UserId where user1.UserId = @UserId  and user1.IsDeleted = 0
and usrcom.CompanyId = @CompanyId)
  if(@UserRole <> 1)
  begin 
-- ,m.CreatedDate
  set @sql='
  select m.Id as TeamId ,m.TeamName as TeamName, m.TeamBudget as TeamBudget,m.[Description] as TeamDescription
  from Teams m left join TeamUsers tu  on m.Id = tu.TeamId 
  where m.IsDefault = 0 and m.IsDeleted = 1 and m.IsDeleted = 0 and m.CompanyId = case when @CompanyId = 0 then m.CompanyId else @CompanyId end
  and( tu.UserId = @UserId or m.CreatedBy =  @UserId )
  group by m.Id,m.TeamName, m.TeamBudget, m.[Description]; '

  INSERT INTO #TempTable(TeamId,TeamName,TeamBudget,TeamDescription)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @UserId  nvarchar(max)',@CompanyId,@UserId	

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY TeamId) AS RNUMBER ,*,(SELECT COUNT(T.TeamId) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

  set @sql1='With CTETeams --create CTE for check value
  AS
  (
   select m.Id as TeamId ,m.TeamName as TeamName, m.TeamBudget as TeamBudget,m.[Description] as TeamDescription 
   from Teams m left join TeamUsers tu  on m.Id = tu.TeamId 
   where m.IsDefault = 0 and m.IsDeleted = 1 and m.IsDeleted = 0 and m.CompanyId = case when @CompanyId = 0 then m.CompanyId else @CompanyId end
   and( tu.UserId = @UserId or m.CreatedBy =  @UserId ) and m.IsDeleted = 0  
   group by m.Id,m.TeamName, m.TeamBudget, m.[Description]
   )
	select m1.TeamId as TeamId, m2.Email as MemberEmail,m2.FirstName as MemberName,m1.IsActive IsActive, 
	case when m2.ProfilePhoto is null then ''Uploads/Default/profile.png'' else ''Uploads/Profile/Icon/''+m2.ProfilePhoto end
	as MemberProfilePhoto, m2.UserId as MemberUserId
	from TeamUsers m1 inner join UserDetails m2 on m1.UserId = m2.UserId where m1.TeamId  in (select T.TeamId from CTETeams T)
	and m2.IsDeleted = 0 and m1.IsActive = 1 and m2.IsDeleted = 0    order by CreatedDate'

    INSERT INTO #TempTable1(TeamId,MemberEmail,MemberName,IsActive,MemeberProfilePhoto,MemberUserId)
    EXECUTE sp_executesql @sql1, N'@CompanyId int, @UserId  nvarchar(max)',@CompanyId,@UserId	
    SELECT * FROM #TempTable1

  end
  else
  Begin 
  set @sql='
  select m.Id as TeamId ,m.TeamName as TeamName, m.TeamBudget,m.[Description] TeamDescription ,m.CreatedDate from  Teams m 
  where m.IsDefault =0 and m.IsDeleted =0  and m.CompanyId = case when @CompanyId = 0 then m.CompanyId else @CompanyId end
  group by  m.Id ,m.TeamName, m.TeamBudget,m.[Description],m.CreatedDate;'

   INSERT INTO #TempTable(TeamId,TeamName,TeamBudget,TeamDescription,CreatedDate)
   EXECUTE sp_executesql @sql, N'@CompanyId int',@CompanyId	

   SELECT * From ( select ROW_NUMBER() OVER(ORDER BY TeamId) AS RNUMBER ,*,(SELECT COUNT(T.TeamId) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

    --create CTE for check value
  set @sql1='
  With CTETeams
  AS
  (
  select m.Id as TeamId ,m.TeamName as TeamName, m.TeamBudget,m.[Description] AS TeamDescription ,m.CreatedDate from  Teams m 
  where m.IsDefault =0 and m.IsDeleted =0  and m.CompanyId = case when @CompanyId = 0 then m.CompanyId else @CompanyId end
  group by  m.Id ,m.TeamName, m.TeamBudget,m.[Description],m.CreatedDate
  )
  select m1.TeamId as TeamId,  m2.Email as MemberEmail,m2.FirstName as MemberName,m1.IsActive as IsActive,

  case when m2.ProfilePhoto is null then ''Uploads/Default/profile.png'' else ''Uploads/Profile/Icon/''+m2.ProfilePhoto end

  as MemberProfilePhoto, m2.UserId as MemberUserId

  from TeamUsers m1 inner join UserDetails m2 on m1.UserId = m2.UserId where m1.TeamId in (select T.TeamId from CTETeams T )

  and m2.IsDeleted = 0 and m1.IsActive = 1 and m2.IsDeleted = 0  order by CreatedDate'

  


  INSERT INTO #TempTable1(TeamId,MemberEmail,MemberName,IsActive,MemeberProfilePhoto,MemberUserId)
  EXECUTE sp_executesql @sql1, N'@CompanyId int',@CompanyId
  end 
   SELECT * FROM #TempTable1

END
Go

-- =============================================

-- Author:		<Author,,Vinay Kumar>

-- Create date: <Create Date,,21/May/2017>

-- Description:	<Description,,This procedure used to get all the invoices of all freelancers for any particular company>

-- =============================================



ALTER PROCEDURE [dbo].[usp_GetAllSentRequestForAdmin]--[dbo].[usp_GetAllSentRequestForAdmin] 1,1,5
-- Add the parameters for the stored procedure here
@CompanyId int ,
@PageIndex int,
@PageSizeSelected  int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
Declare @sql nvarchar(Max);
-- Insert statements for procedure here
set @sql='select inv.Id as FreelancerInvitationId,inv.FreelancerEmail,inv.CompanyId,inv.SentBy,inv.ProjectId,case when inv.UserRate is null then 0 else inv.UserRate end as UserRate,
inv.RoleId, inv.Activationkey,inv.IskeyActive,inv.SentDate, inv.IsAccepted,case when inv.IsRejected is null then 0 else inv.IsRejected end as IsRejected,                                    
case when inv.WeeklyLimit is null then 0 else inv.WeeklyLimit end as WeeklyLimit, inv.JobTitle, inv.JobDescription,inv.Remarks,case when inv.IsJobInvitation is null 
then 0 else inv.IsJobInvitation end as IsJobInvitation,(select prjctt.Name from Projects prjctt where prjctt.Id = inv.ProjectId and prjctt.IsDeleted = 0) as ProjectName,         
(select rol.Name from Roles rol where rol.RoleId = inv.RoleId and rol.IsDeleted = 0) as RoleName,
(select ussr.FirstName from UserDetails ussr where ussr.UserId = inv.SentBy and ussr.IsDeleted = 0) as SenderName,
(select ussrr.UserId from UserDetails ussrr where ussrr.Email = inv.FreelancerEmail and ussrr.IsDeleted = 0) as FreelancerUserId,                      
(select ussrr.ProfilePhoto from UserDetails ussrr where ussrr.Email = inv.FreelancerEmail and ussrr.IsDeleted = 0) as FreelancerProfilePhoto
from FreelancerInvitations inv inner join Companies usrcom on inv.CompanyId = usrcom.Id  where usrcom.Id = @CompanyId'


Create Table #TempTable
(
FreelancerInvitationId int,
FreelancerEmail Nvarchar(max),
CompanyId   int,
SentBy Nvarchar(max) ,
ProjectId int,
UserRate  decimal(18, 2) ,
RoleId int,
Activationkey Nvarchar(max),
IskeyActive bit,
SentDate datetime,
IsAccepted bit,
IsRejected bit,
WeeklyLimit int,
JobTitle Nvarchar(max),
JobDescription Nvarchar(max),
Remarks Nvarchar(max),
IsJobInvitation bit,
ProjectName Nvarchar(max),
RoleName Nvarchar(max),
SenderName Nvarchar(max),
FreelancerUserId nvarchar(max),
FreelancerProfilePhoto nvarchar(max)
)
INSERT INTO #TempTable(FreelancerInvitationId ,
FreelancerEmail ,
CompanyId   ,
SentBy  ,
ProjectId ,
UserRate   ,
RoleId ,
Activationkey ,
IskeyActive ,
SentDate ,
IsAccepted ,
IsRejected ,
WeeklyLimit ,
JobTitle ,
JobDescription ,
Remarks,
IsJobInvitation ,
ProjectName ,
RoleName ,
SenderName ,
FreelancerUserId ,
FreelancerProfilePhoto)
EXECUTE sp_executesql @sql, N'@CompanyId int',@CompanyId
SELECT * From ( select ROW_NUMBER() OVER(ORDER BY FreelancerInvitationId) AS RNUMBER ,*,(SELECT COUNT(T.FreelancerInvitationId) FROM #TempTable T) TotalPageCount	
FROM #TempTable ) as TBL 
where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected



end 



GO

---25 April 2018------------------------------------------------------------------------------------------------------------



-- =============================================

-- Author:		<Author,RAKESH PARMAR,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================
ALTER PROCEDURE [dbo].[Usp_GetInvitaionFromCompany] --[Usp_GetInvitaionFromCompany]'0cffc4b6-429b-4b7b-8f27-9a4067ba88d3',null,0,0,1,1
-- Add the parameters for the stored procedure here
@UserId nvarchar(max),

@IsApproved int,

@Id int,

@CompanyId int,

@PageIndex int ,

@PageSize  int

AS

BEGIN

Declare @sql  NVARCHAR(MAX);



--Set @sql = N'select VI.Id as Id,VI.IsApproved IsApproved, C.Name as Name,VI.InvitationDate InvitationDate,C.Id as CompanyId 

--,(select Ud.Id from UserDetails Ud where Ud.UserId = @UserId) VendorId

--from Companies C inner join VendorInvitations VI on C.Id = VI.CompanyId  where VI.UserId = @UserId

--and VI.IsActive = 1 and VI.IsDeleted = 0 and VI.InvitationType = 0' 

CREATE TABLE #TempTable(

 Id int,

 IsApproved Bit,

 Name nvarchar(max),

 InvitationDate Datetime,

 CompanyId int,

 VendorId  int,
   )

Set @sql = N'select VI.Id as Id,VI.IsApproved IsApproved, C.Name as Name,VI.InvitationDate InvitationDate,C.Id as CompanyId 

,(select Ud.Id from UserDetails Ud where Ud.UserId = @UserId) VendorId

from Companies C inner join VendorInvitations VI on C.Id = VI.CompanyId  where VI.UserId = @UserId

and VI.IsActive = 1 and VI.IsDeleted = 0 and VI.InvitationType = 0 ' 

+ CASE WHEN @Id = 1 or @Id = 2 THEN N' and VI.IsApproved  = @IsApproved ' WHEN @Id = 3 THEN N' and VI.IsApproved Is Null ' ELSE N' ' END

+ CASE WHEN @CompanyId > 0 THEN N' and VI.CompanyId = @CompanyId' ELSE N' 'END

INSERT INTO #tempTable( 

 Id,

 IsApproved, 

 Name ,

 InvitationDate ,

 CompanyId ,

 VendorId 

 )


EXECUTE sp_executesql @Sql,N' @UserId Nvarchar(max),@IsApproved int,@Id int,@CompanyId int',@UserId,@IsApproved,@Id,@CompanyId

 SELECT RNUMBER, TBL.Id,TBL.IsApproved,TBL.Name,TBL.InvitationDate,TBL.CompanyId ,TBL.VendorId

 ,(SELECT Count(Tb.Id) as TotalCount FROM #TempTable Tb ) as TotalCount

   From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*

 ,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount	

 FROM #TempTable ) as TBL 

 where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSize + 1 AND @PageIndex * @PageSize

END
Go


---------------------26 April 2018------------------------------------------------------------




-- =============================================



-- Author:		<Author,,Name>



-- Create date: <Create Date,,>



-- Description:	<Description,,>



-- =============================================



CREATE PROCEDURE [dbo].[Usp_GetQuotedProjectForVendorProfile] 
-- Add the parameters for the stored procedure here
@ProjectId int,
@VendorId int,
@CompanyId int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
-----------------------------------ProjectVendorQuote---------------------------------------------------
SELECT ProjectId,VendorId ,Budget TotalBudget ,Hours TotalHours ,P.Description Description,P.StartDate ProjectStartDate
,P.EndDate  projectEndDate ,Pvq.IsActive ,QuoteSenton,P.Name,Pvq.Description AllDescription,P.IsComplete FROM   ProjectVendorQuote Pvq
inner join Projects P on P.Id = Pvq.ProjectId
where Pvq.ProjectId = @ProjectId and Pvq.VendorId = @VendorId 
-----------------------------------ProjectProjectInvitationAttachments---------------------------------------------------
select PIA.ProjectId, PIA.FileName,PIA.FilePath,PIA.ActualFileName  from dbo.ProjectInvitationAttachments PIA
inner join Projects P on P.Id = PIA.ProjectId where PIA.ProjectId = @ProjectId

-----------------------------------ProjectPhases---------------------------------------------------
SELECT Pph.Name Name,Pph.Description Description ,Pphq.Budget PhaseVendorBudget,Pphq.Hours PhaseVendorHour,Pph.Id PhaseId,Pphq.ProjectId
FROM   ProjectPhases Pph inner join dbo.ProjectPhaseQuotes Pphq on Pph.Id = Pphq.PhaseId
where VendorId = @VendorId and Pphq.ProjectId =  @ProjectId and Pphq.IsActive = 1
-----------------------------------PhaseAttachment---------------------------------------------------		
select Phd.PhaseId,Phd.ActualFileName,Phd.Path FilePath,Phd.Name FileName  from [dbo].[PhaseDocuments] Phd
inner join ProjectPhases Pph on Pph.Id = Phd.PhaseId
where ProjectId =  @ProjectId
-----------------------------------ProjectMileStoneQuotes---------------------------------------------------	
select Pms.Description Description,Pms.Name Name,Pmsq.Hours MilestoneVendorHour,Pmsq.Budget MilestoneVendorBudget,Pmsq.PhaseId PhaseId
,Pmsq.MileStoneId from dbo.ProjectMileStones Pms  
inner join dbo.ProjectMileStoneQuotes Pmsq  on Pmsq.MileStoneId =  Pms.Id
where Pmsq.VendorId = @VendorId and Pmsq.projectId = @ProjectId and Pmsq.IsDeleted = 0 and Pmsq.IsActive = 1
-----------------------------------ProjectProposedResourcesByVendor---------------------------------------------------	
SELECT Prv.Id,R.Name DesignationName,ProjectId,VendorId,FirstName ,LastName,Profile,HourlyRate HourlyRate,ExpectedHours,Prv.SpentHours
FROM   ProjectProposedResourcesByVendor Prv
inner join dbo.Roles R on R.RoleId = Prv.RoleId
where Prv.VendorId = @VendorId and Prv.projectId = @ProjectId

 -----------------------------------Project VendorProjectProgress----------------------------------------------
  select 



  (select Count(I.Id)  from Invoices I inner join



	VendorInvoiceDetails VId on I.Id = VId.InvoiceId where VId.ProjectId = @ProjectId  and VId.VendorId = @VendorId and I.IsRejected = 0 and I.IsPaid = 0



	and I.InvoiceNumber LIKE 'INV-V%'	



	)



	UpaidInvoice







,(select sum(CONVERT(INT ,I.TotalHours)) Hours from Invoices I inner join



VendorInvoiceDetails VId on I.Id = VId.InvoiceId where VId.ProjectId = @ProjectId  and VId.VendorId = @VendorId and I.IsRejected = 0



and I.InvoiceNumber LIKE 'INV-V%'	



)



TotalHours



,



(select sum(CONVERT(INT ,I.TotalHours)) Hours from Invoices I inner join



VendorInvoiceDetails VId on I.Id = VId.InvoiceId where VId.ProjectId = @ProjectId  and VId.VendorId = @VendorId and I.IsPaid = 1 and I.IsRejected = 0



and I.InvoiceNumber LIKE 'INV-V%'	



)



TotalPaidHour







,(select sum(I.TotalAmount) Cost from Invoices I inner join



VendorInvoiceDetails VId on I.Id = VId.InvoiceId where VId.ProjectId = @ProjectId  and VId.VendorId = @VendorId



and I.IsPaid = 1 and I.InvoiceNumber LIKE 'INV-V%'	) TotalCost







,I.Id,I.InvoiceNumber InvoiceNumber,I.TotalAmount InvoiceCost,I.TotalHours Hours,I.IsPaid IsPaid,I.InvoiceDate InvoiceDate,VId.ProjectId







from Invoices I inner join VendorInvoiceDetails VId on I.Id = VId.InvoiceId  where VId.ProjectId = @ProjectId and VId.VendorId = @VendorId



and I.IsRejected = 0  and I.InvoiceNumber LIKE 'INV-V%'	















if(@CompanyId = 0)



Begin











	 ---------------------------- Vendor Profile----Custom control UI getting------------------------------------------------



	 Declare  @MaicompanyId int







	select @MaicompanyId =  CompanyId from Projects where Id = @ProjectId



	



	



	



	



	SELECT  Id , CompanyId, FieldName, Watermark, Description, ControlType,[Values], IsActive,Description DescriptionCustomControl



     ,IsDeleted FROM ProjectCustomizationSettings Where CompanyId = @MaicompanyId and IsDeleted = 0 and IsActive = 1 ORDER BY Priority







	---------------------------------Custom control UI getting------------------------------------------------











	---------------------------------Custom control Value getting------------------------------------------------



	select Id, [values] CustomControlData from ProjectCustomValues where ProjectId = @ProjectId







	------------------------------------------------------------------------------------------------------







	end



	else







	begin



	  



		--------------------CompanyProfile Qoute get-Custom control UI getting------------------------------------------------







	SELECT  Id , CompanyId, FieldName, Watermark, Description, ControlType,[Values], IsActive,Description DescriptionCustomControl



     ,IsDeleted FROM ProjectCustomizationSettings Where CompanyId = @CompanyId and IsDeleted = 0 and IsActive = 1 ORDER BY Priority







	---------------------------------Custom control UI getting------------------------------------------------











	---------------------------------Custom control Value getting------------------------------------------------



	select Id, [values] CustomControlData from ProjectCustomValues where ProjectId = @ProjectId







	------------------------------------------------------------------------------------------------------



	end



	



END
Go



-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[Usp_GetQuotedProjectForCompany]-- [dbo].[Usp_GetQuotedProjectForCompany] 33,31,1

-- Add the parameters for the stored procedure here

@ProjectId int,

@VendorId int,

@CompanyId int

AS

BEGIN

-- SET NOCOUNT ON added to prevent extra result sets from

-- interfering with SELECT statements.

SET NOCOUNT ON;

-----------------------------------ProjectVendorQuote---------------------------------------------------

SELECT ProjectId,VendorId ,Budget TotalBudget ,Hours TotalHours,P.Description Description ,P.StartDate ProjectStartDate,P.EndDate  projectEndDate

,Pvq.IsActive,QuoteSenton,P.Name,Pvq.Description AllDescription,P.IsComplete,(ud.FirstName+''+ud.LastName) AS VendorPOCName

FROM   ProjectVendorQuote Pvq

inner join Projects P on P.Id = Pvq.ProjectId

inner join UserDetails ud on ud.Id=Pvq.VendorId 

where Pvq.ProjectId = @ProjectId and Pvq.VendorId = @VendorId 















			-----------------------------------ProjectProjectInvitationAttachments---------------------------------------------------















		select PIA.ProjectId, PIA.FileName,PIA.FilePath,PIA.ActualFileName  from dbo.ProjectInvitationAttachments PIA







			inner join Projects P on P.Id = PIA.ProjectId where PIA.ProjectId = @ProjectId







































			-----------------------------------ProjectPhases---------------------------------------------------







SELECT       







      Pph.Name Name







      ,Pph.Description Description







      ,Pphq.Budget PhaseVendorBudget







      ,Pphq.Hours PhaseVendorHour







	  ,Pph.Id PhaseId







	  ,Pphq.ProjectId







  FROM   ProjectPhases Pph







  inner join dbo.ProjectPhaseQuotes Pphq on Pph.Id = Pphq.PhaseId







  where VendorId = @VendorId and Pphq.ProjectId =  @ProjectId and Pphq.IsActive = 1







  















					-----------------------------------PhaseAttachment---------------------------------------------------		















select Phd.PhaseId,Phd.ActualFileName,Phd.Path FilePath,Phd.Name FileName  from [dbo].[PhaseDocuments] Phd







inner join ProjectPhases Pph on Pph.Id = Phd.PhaseId







where ProjectId =  @ProjectId















-----------------------------------ProjectMileStoneQuotes---------------------------------------------------	







	 







	 







select Pms.Description Description







  ,Pms.Name Name







 ,Pmsq.Hours MilestoneVendorHour







 ,Pmsq.Budget MilestoneVendorBudget







 ,Pmsq.PhaseId PhaseId







 ,Pmsq.MileStoneId







 







  from dbo.ProjectMileStones Pms  







inner join dbo.ProjectMileStoneQuotes Pmsq  on Pmsq.MileStoneId =  Pms.Id















where Pmsq.VendorId = @VendorId and Pmsq.projectId = @ProjectId and Pmsq.IsDeleted = 0 and Pmsq.IsActive = 1















-----------------------------------ProjectProposedResourcesByVendor---------------------------------------------------	















SELECT







     Prv.Id







	 ,R.Name DesignationName







      ,ProjectId







      ,VendorId







      ,FirstName







      ,LastName







      ,Profile







      ,HourlyRate HourlyRate







	  ,ExpectedHours







	  ,Prv.SpentHours







  FROM   ProjectProposedResourcesByVendor Prv







  inner join dbo.Roles R on R.RoleId = Prv.RoleId







  where Prv.VendorId = @VendorId and Prv.projectId = @ProjectId
  		--------------------CompanyProfile Qoute get-Custom control UI getting------------------------------------------------
	SELECT  Id , CompanyId, FieldName, Watermark, Description, ControlType,[Values], IsActive,Description DescriptionCustomControl
     ,IsDeleted FROM ProjectCustomizationSettings Where CompanyId = @CompanyId and IsDeleted = 0 and IsActive = 1 ORDER BY Priority

	---------------------------------Custom control UI getting------------------------------------------------
	---------------------------------Custom control Value getting------------------------------------------------
		select Id, [values] CustomControlData from ProjectCustomValues where ProjectId = @ProjectId
	------------------------------------------------------------------------------------------------------
END
GO











-- =============================================



-- Author:		<Author,,Rakesh Parmar>



-- Create date: <Create Date,,>



-- Description:	<Description,,>



-- =============================================



CREATE  PROCEDURE [dbo].[Usp_GetProjectDetailForVendorProfile]--[dbo].[Usp_GetProjectDetailForVendorProfile]  33,2,'ed45af68-f6de-48e2-8c9b-f76b86f0b585',31



	-- Add the parameters for the stored procedure here







@ProjectId int,



@CompanyId int,



@UserId Nvarchar(Max),



@VendorId int



	



AS



BEGIN



	-- SET NOCOUNT ON added to prevent extra result sets from



	-- interfering with SELECT statements.



	SET NOCOUNT ON;











-------------------------------------------- project Projects ------------------------------------------------------



				



				Declare @VendorUserId Nvarchar(max)



				set @VendorUserId  = @UserId



				--set @VendorUserId  = (select Ud.UserId from UserDetails Ud where Ud.CompanyId = @CompanyId)



			



				select Prj.Id ProjectId, Prj.Name Name,Prj.Description Description,



				prj.StartDate ProjectStartDate,Prj.EndDate projectEndDate



				,(select Pv.IsProjectUpdated from ProjectVendor Pv where Pv.VendorId = @VendorId and Pv.ProjectId = @ProjectId) IsProjectUpdated



				,Pvq.Description AllDescription



				,Pvq.Budget TotalBudget



				,Pvq.Hours TotalHours



				,VI.IsActive IsActive



				from Projects Prj



				inner join VendorInvitations VI on Prj.Id = VI.ProjectId 



				left join ProjectVendorQuote Pvq on Pvq.ProjectId = Prj.Id and Pvq.IsActive = 1 and Pvq.VendorId = @VendorId



				where  Prj.Id = @ProjectId  and VI.UserId = @UserId and VI.ProjectId = @ProjectId











-------------------------------------------- project ProjectInvitationAttachments ----------------------------------------------------------------







			   select Prj.Id ProjectId, Prj.Name ProjectName,Prj.Description Description,



				prj.StartDate ProjectStartDate,Prj.EndDate projectEndDate,PIA.FileName FileName,PIA.ActualFileName,PIA.FilePath  



				from Projects Prj



				inner join ProjectInvitationAttachments PIA on Prj.Id = PIA.ProjectId



				where  ProjectId = @ProjectId



				







				Declare @IsProjectEdited bit







			set	@IsProjectEdited = (select  Case When IsProjectUpdated Is Null  Then 0 Else 1 End  from ProjectVendor where ProjectId =  @ProjectId and VendorId = @VendorId)







				if(@IsProjectEdited = 0)



					Begin



			-----------------------------------------ProjectPhases  , PhaseDocuments and ProjectMileStones  first time -----------------------------------------------------



							select Pph.Id PhaseId, Pph.ProjectId ProjectId,Pph.Name Name,Pph.Description from ProjectPhases Pph



							where Pph.ProjectId = @ProjectId







							select Phd.PhaseId , phd.Name FileName,Phd.ActualFileName,Phd.Path FilePath from 



							PhaseDocuments Phd inner join ProjectPhases Pph on Phd.PhaseId = Pph.Id



							where Pph.ProjectId = @ProjectId











							select  PrjM.PhaseId,prjm.Id MilestoneId, PrjM.Name,PrjM.Description from ProjectMileStones PrjM 



							inner join  ProjectPhases PrjPh on PrjM.PhaseId = PrjPh.Id



							where PrjM.ProjectId = @ProjectId 











	                     select ProjectId



					      ,VendorId



					      ,FirstName



					      ,LastName



					      ,Profile



					      ,HourlyRate HourlyRate



					  FROM  ProjectProposedResourcesByVendor Prv



					  inner join dbo.Roles R on R.RoleId = Prv.RoleId



					  where Prv.VendorId = @VendorId and Prv.projectId = @ProjectId



					 end



				else



				     Begin







		 			-----------------------------------ProjectPhases---------------------------------------------------



	



						  



				SELECT



							  Pphq.Id Id      



						      ,Pph.Name Name



						      ,Pph.Description Description



						      ,Pphq.Budget PhaseVendorBudget



						      ,Pphq.Hours PhaseVendorHour



							  ,Pph.Id PhaseId



							  ,Pphq.ProjectId



						  FROM ProjectPhases Pph



						  left join dbo.ProjectPhaseQuotes Pphq on Pph.Id = Pphq.PhaseId and Pphq.IsDeleted = 0  and Pphq.VendorId = @VendorId



						  where  Pph.ProjectId =  @ProjectId 







						  -----------------------------------PhaseAttachment----------------------------------------------------------



						  	select Phd.PhaseId , phd.Name FileName,Phd.ActualFileName,Phd.Path FilePath from 



							PhaseDocuments Phd inner join ProjectPhases Pph on Phd.PhaseId = Pph.Id



							where Pph.ProjectId = @ProjectId 



							



-----------------------------------ProjectMileStoneQuotes---------------------------------------------------	



	 



	 						select 



						 Pmsq.Id Id 



						,Pms.Description Description



						  ,Pms.Name Name



						 ,Pmsq.Hours MilestoneVendorHour



						 ,Pmsq.Budget MilestoneVendorBudget



						 ,Pms.PhaseId PhaseId



						 ,Pms.Id MileStoneId



 



						  from dbo.ProjectMileStones Pms  



						left join dbo.ProjectMileStoneQuotes Pmsq  on Pmsq.MileStoneId =  Pms.Id and Pmsq.IsDeleted = 0 and Pmsq.VendorId = @VendorId







						where Pms.projectId = @ProjectId 











						



			-----------------------------------ProjectProposedResourcesByVendor---------------------------------------------------	



					     SELECT



						 Prv.Id Id



						 ,R.Name DesignationName



						 ,Prv.RoleId RoleId



						  ,ProjectId



					      ,VendorId



					      ,FirstName



					      ,LastName



					      ,Profile



					      ,HourlyRate HourlyRate



						  ,ExpectedHours



					  FROM  ProjectProposedResourcesByVendor Prv



					  inner join dbo.Roles R on R.RoleId = Prv.RoleId



					  where Prv.VendorId = @VendorId and Prv.projectId = @ProjectId 











					 end



			











	 











	 	 ---------------------------------Custom control UI getting------------------------------------------------



	 Declare  @MaicompanyId int







	select @MaicompanyId =  CompanyId from Projects where Id = @ProjectId



	



	



	



	



	SELECT  Id , CompanyId, FieldName, Watermark, Description, ControlType,[Values], IsActive,Description DescriptionCustomControl



     ,IsDeleted FROM ProjectCustomizationSettings Where CompanyId = @MaicompanyId and IsDeleted = 0 and IsActive = 1 ORDER BY Priority







	---------------------------------Custom control UI getting------------------------------------------------











	---------------------------------Custom control Value getting------------------------------------------------



	select Id, [values] CustomControlData from ProjectCustomValues where ProjectId = @ProjectId







	------------------------------------------------------------------------------------------------------

END
Go


------------------------------1 may 2018---------------------------------------
-- =============================================
-- Author:		<Author,,Rakesh Parmar>
-- Create date: <Create Date,,24/Oct/2017>
-- Description:	<Description,,This procedure used to get all the invoices of all freelancers for any particular company>
-- =============================================

ALTER Procedure [dbo].[Usp_GetAllVendorInvoiceList] 
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorCompanyId int, 
	@PageIndex int,
    @PageSizeSelected  int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
VendorName   Nvarchar(max) ,
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@VendorCompanyId = 0)
		Begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and  cast( I.FromDate as date) >=
				
			    CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END'
				 
		end
Else
		begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and I.FromDate >= 
					
					    CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END'

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,VendorName,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorCompanyId int',@CompanyId,@FromDate,@ToDate,@VendorCompanyId

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END
GO


-- =============================================
-- Author:		<Author,,Subia Rahman>
-- Create date: <Create Date,01/May/2018>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllInvoiceDetailList] 
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorId int, 
	@PageIndex int,
    @PageSizeSelected  int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@CompanyId = 0)
		Begin
		set @sql='select  I.Id, I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
                                inner join Companies c On c.Id = P.CompanyId
                                where Vid.VendorId = @VendorId'
				 
		end
Else
		begin
		set @sql='select  I.Id, I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
                                inner join Companies c On c.Id = P.CompanyId
                                where Vid.VendorId = @VendorId  and I.FromDate >= @FromDate and I.ToDate <=  @ToDate and
                                 I.CompanyId =  @CompanyId '

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorId int',@CompanyId,@FromDate,@ToDate,@VendorId

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END
GO



-- =============================================
-- Author:		<Author,RAKESH PARMAR,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER Procedure [dbo].[Usp_GetAllInvoiceListVendorAdminProfile]---[dbo].[Usp_GetAllInvoiceListVendorAdminProfile] 2,0,0,null,null,1,5
	-- Add the parameters for the stored procedure here
@VendorCompanyId int
,@CompanyId int
,@VendorId int
,@FromDate DateTime
,@ToDate DateTime
,@PageIndex int
,@PageSizeSelected  int

AS
BEGIN
Create Table #TempTable
(
InvoiceId int,
VendorName Nvarchar(max),
InvoiceNumber Nvarchar(max),
InvoiceDate DateTime ,
TotalHours Nvarchar(max),
TotalAmount decimal(18,2),
IsPaid bit,
IsRejected bit,
ProjectId int,
ProjectName nvarchar(max),
CompanyName nvarchar(max)
)
Declare @sql  NVARCHAR(MAX);
Set @sql = N'select  I.Id InvoiceId
                                ,CONCAT(Ud.FirstName + '' '',Ud.FirstName) VendorName
                                ,I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
								inner join UserDetails Ud on Ud.Id = Vid.VendorId
                                inner join Companies c On c.Id = P.CompanyId
                                where  Ud.CompanyId = @VendorCompanyId ' 
+ CASE WHEN @VendorId     > 0	THEN N' and Ud.Id = @VendorId ' ELSE N'  'END
+ CASE WHEN @FromDate  Is not Null 	THEN N' and I.FromDate >= @FromDate ' ELSE N'  'END
+ CASE WHEN @ToDate   Is not Null 	THEN N' and I.ToDate <=  @ToDate ' ELSE N'  'END
+ CASE WHEN @CompanyId   >  0 	THEN N' and c.Id =  @CompanyId ' ELSE N'  'END


INSERT INTO #tempTable(InvoiceId,VendorName,InvoiceNumber,InvoiceDate,TotalHours,TotalAmount,IsPaid,IsRejected,ProjectId,ProjectName,CompanyName)
EXECUTE sp_executesql @Sql,N'@VendorCompanyId int,@CompanyId int,@VendorId int,@FromDate DateTime,@ToDate DateTime,@PageIndex int,@PageSizeSelected  int',
@VendorCompanyId ,@CompanyId ,@VendorId ,@FromDate ,@ToDate ,@PageIndex ,@PageSizeSelected 

SELECT RNUMBER, TBL.InvoiceId,TBL.VendorName,TBL.InvoiceNumber,TBL.InvoiceDate,TBL.TotalHours ,TBL.TotalAmount,TBL.IsPaid,TBL.IsRejected,TBL.ProjectId,
TBL.ProjectName,TBL.CompanyName
,(SELECT Count(Tb.InvoiceId) as TotalCount FROM #TempTable Tb ) as TotalPageCount
 From ( select ROW_NUMBER() OVER(ORDER BY InvoiceId) AS RNUMBER ,*
FROM #TempTable ) as TBL 
where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

print(@sql)
--EXECUTE sp_executesql @Sql,N'@VendorCompanyId Int, @CompanyId Int,@VendorId Int,@FromDate DateTime,@ToDate DateTime',
--@VendorCompanyId,@CompanyId,@VendorId,@FromDate,@ToDate
End
GO



------------------------------------------------------3 May 2018------------------------------------------
-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,,03/May/2018>
-- Description:	<Description,,This procedure used to get all the rejected invoices of all freelancers for any particular company>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllVendorInvoiceListRejected] --[dbo].[Usp_GetAllVendorInvoiceListRejected] 1,null,null,0,1,5
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorCompanyId int, 
	@PageIndex int,
    @PageSizeSelected  int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET NOCOUNT ON;

Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
VendorName   Nvarchar(max) ,
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@VendorCompanyId = 0)
		Begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and  cast( I.FromDate as date) >= CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsRejected=1'
				 
		end
Else
		begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and I.FromDate >= 
					
					    CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsRejected=1'

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,VendorName,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorCompanyId int',@CompanyId,@FromDate,@ToDate,@VendorCompanyId

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END
GO



-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,,03/May/2018>
-- Description:	<Description,,This procedure used to get all the paid invoices of all freelancers for any particular company>
-- =============================================
CREATE Procedure [dbo].[Usp_GetAllVendorInvoiceListPaid] --[dbo].[[Usp_GetAllVendorInvoiceListPaid] 1,null,null,0,1,5
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorCompanyId int, 
	@PageIndex int,
    @PageSizeSelected  int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
VendorName   Nvarchar(max) ,
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@VendorCompanyId = 0)
		Begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and  cast( I.FromDate as date) >= CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsPaid=1'
				 
		end
Else
		begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and I.FromDate >= 
					
					    CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsPaid=1'

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,VendorName,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorCompanyId int',@CompanyId,@FromDate,@ToDate,@VendorCompanyId

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END
GO



-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,,03/May/2018>
-- Description:	<Description,,This procedure used to get all the outstanding invoices of all freelancers for any particular company>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllVendorInvoiceListOutstanding] --[dbo].[Usp_GetAllVendorInvoiceListOutstanding] 1,null,null,0,1,30
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorCompanyId int, 
	@PageIndex int,
    @PageSizeSelected  int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
VendorName   Nvarchar(max) ,
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@VendorCompanyId = 0)
		Begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and  cast( I.FromDate as date) >= CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsPaid=0'
				 
		end
Else
		begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and I.FromDate >= 
					
					    CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsPaid=0'

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,VendorName,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorCompanyId int',@CompanyId,@FromDate,@ToDate,@VendorCompanyId

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END



















					












					



































