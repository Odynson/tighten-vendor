------------------------------------------------------3 May 2018------------------------------------------
-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,,03/May/2018>
-- Description:	<Description,,This procedure used to get all the rejected invoices of all freelancers for any particular company>
-- =============================================

ALTER Procedure [dbo].[Usp_GetAllVendorInvoiceListRejected] --[dbo].[Usp_GetAllVendorInvoiceListRejected] 1,null,null,0,1,5
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorCompanyId int, 
	@PageIndex int,
    @PageSizeSelected  int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET NOCOUNT ON;

Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
VendorName   Nvarchar(max) ,
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@VendorCompanyId = 0)
		Begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and  cast( I.FromDate as date) >= CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsRejected=1'
				 
		end
Else
		begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and I.FromDate >= 
					
					    CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsRejected=1'

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,VendorName,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorCompanyId int',@CompanyId,@FromDate,@ToDate,@VendorCompanyId

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END
GO



-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,,03/May/2018>
-- Description:	<Description,,This procedure used to get all the paid invoices of all freelancers for any particular company>
-- =============================================
CREATE Procedure [dbo].[Usp_GetAllVendorInvoiceListPaid] --[dbo].[[Usp_GetAllVendorInvoiceListPaid] 1,null,null,0,1,5
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorCompanyId int, 
	@PageIndex int,
    @PageSizeSelected  int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
VendorName   Nvarchar(max) ,
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@VendorCompanyId = 0)
		Begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and  cast( I.FromDate as date) >= CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsPaid=1'
				 
		end
Else
		begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and I.FromDate >= 
					
					    CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsPaid=1'

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,VendorName,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorCompanyId int',@CompanyId,@FromDate,@ToDate,@VendorCompanyId

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END
GO



-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,,03/May/2018>
-- Description:	<Description,,This procedure used to get all the outstanding invoices of all freelancers for any particular company>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllVendorInvoiceListOutstanding] --[dbo].[[Usp_GetAllVendorInvoiceListOutstanding] 1,null,null,0,1,30
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorCompanyId int, 
	@PageIndex int,
    @PageSizeSelected  int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
VendorName   Nvarchar(max) ,
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@VendorCompanyId = 0)
		Begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and  cast( I.FromDate as date) >= CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsPaid=0'
				 
		end
Else
		begin
		set @sql='select I.Id,I.InvoiceNumber InvoiceNumber
				,CONCAT(ud.FirstName + '' '' ,ud.LastName) VendorName
				,I.InvoiceDate Invoicedate ,I.TotalHours TotalHours ,I.TotalAmount TotalAmount,I.IsPaid IsPaid,I.IsRejected IsRejected
				,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,c.Name CompanyName
				from UserDetails ud inner join  Invoices I on ud.UserId = I.UserId
				inner join Companies C on C.Id = ud.CompanyId
				inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
				inner join Projects p on p.Id = Vid.ProjectId
				where I.CompanyId = @CompanyId and I.FromDate >= 
					
					    CASE WHEN @FromDate Is null Then cast( I.FromDate as date) ELSE cast( @FromDate as date) END
			    and cast( I.ToDate as date)  <=	CASE WHEN @ToDate Is null Then cast( I.ToDate as date) ELSE cast( @ToDate as date) END and I.IsPaid=0'

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,VendorName,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorCompanyId int',@CompanyId,@FromDate,@ToDate,@VendorCompanyId

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END

GO


--------------------------------------------------------------------5 may 2018-----------------------------------------------------------------------
-- =============================================
-- Author:		<Author,,Subia Rahman>
-- Create date: <Create Date,05/May/2018>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllInvoiceDetailListOutstanding] --[dbo].[Usp_GetAllInvoiceDetailListOutstanding] 0,null,null,2,1,50
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorId int, 
	@PageIndex int,
    @PageSizeSelected  int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@CompanyId = 0)
		Begin
		set @sql='select  I.Id, I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
                                inner join Companies c On c.Id = P.CompanyId
                                where Vid.VendorId = @VendorId and I.IsPaid=0 and I.IsRejected=0'
				 
		end
Else
		begin
		set @sql='select  I.Id, I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
                                inner join Companies c On c.Id = P.CompanyId
                                where Vid.VendorId = @VendorId  and I.FromDate >= @FromDate and I.ToDate <=  @ToDate and
                                 I.CompanyId =  @CompanyId and I.IsPaid=0  and I.IsRejected=0'

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorId int',@CompanyId,@FromDate,@ToDate,@VendorId

  SELECT *  From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount FROM #TempTable ) as TBL 
  where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END
GO


-- =============================================
-- Author:		<Author,,Subia Rahman>
-- Create date: <Create Date,05/May/2018>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllInvoiceDetailListPaid] --[dbo].[Usp_GetAllInvoiceDetailListPaid] 0,null,null,2,1,50
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorId int, 
	@PageIndex int,
    @PageSizeSelected  int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@CompanyId = 0)
		Begin
		set @sql='select  I.Id, I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
                                inner join Companies c On c.Id = P.CompanyId
                                where Vid.VendorId = @VendorId and I.IsPaid=1'
				 
		end
Else
		begin
		set @sql='select  I.Id, I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
                                inner join Companies c On c.Id = P.CompanyId
                                where Vid.VendorId = @VendorId  and I.FromDate >= @FromDate and I.ToDate <=  @ToDate and
                                 I.CompanyId =  @CompanyId and I.IsPaid=1 '

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorId int',@CompanyId,@FromDate,@ToDate,@VendorId

  SELECT *  From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount FROM #TempTable ) as TBL 
  where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END
GO


-- =============================================
-- Author:		<Author,,Subia Rahman>
-- Create date: <Create Date,05/May/2018>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllInvoiceDetailListRejected] --[dbo].[Usp_GetAllInvoiceDetailListRejected] 0,null,null,2,1,50
	-- Add the parameters for the stored procedure here
	@CompanyId int ,
	@FromDate datetime,
	@ToDate datetime,
	@VendorId int, 
	@PageIndex int,
    @PageSizeSelected  int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
Declare @sql nvarchar(Max);
Create Table #TempTable
(
Id int,
InvoiceNumber Nvarchar(max),
InvoiceDate datetime,
TotalHours nvarchar(max),
TotalAmount decimal(18,2),
IsPAid bit,
IsRejected bit,
ProjectId int,
InvoiceId int,
ProjectName Nvarchar(max) ,
CompanyName Nvarchar(max) 
)

If(@CompanyId = 0)
		Begin
		set @sql='select  I.Id, I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
                                inner join Companies c On c.Id = P.CompanyId
                                where Vid.VendorId = @VendorId and I.IsRejected=1'
				 
		end
Else
		begin
		set @sql='select  I.Id, I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,Vid.InvoiceId InvoiceId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
                                inner join Companies c On c.Id = P.CompanyId
                                where Vid.VendorId = @VendorId  and I.FromDate >= @FromDate and I.ToDate <=  @ToDate and
                                 I.CompanyId =  @CompanyId and I.IsRejected=1 '

		end
  INSERT INTO #TempTable(Id,InvoiceNumber,InvoiceDate,TotalHours,TotalAmount,IsPAid,IsRejected,ProjectId,InvoiceId,ProjectName,CompanyName)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @FromDate DATETIME,@ToDate DATETIME,@VendorId int',@CompanyId,@FromDate,@ToDate,@VendorId

  SELECT *  From ( select ROW_NUMBER() OVER(ORDER BY Id) AS RNUMBER ,*,(SELECT COUNT(T.Id) FROM #TempTable T) TotalPageCount FROM #TempTable ) as TBL 
  where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END

GO

--------------------------------------------------7 may 2018-------------------------------------------------------------------

-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,7 may 2018,>
-- Description:	<Description,,>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllInvoiceListVendorAdminProfileOutstanding]---[dbo].[Usp_GetAllInvoiceListVendorAdminProfileOutstanding] 2,0,0,null,null,1,50
	-- Add the parameters for the stored procedure here
@VendorCompanyId int
,@CompanyId int
,@VendorId int
,@FromDate DateTime
,@ToDate DateTime
,@PageIndex int
,@PageSizeSelected  int

AS
BEGIN
Create Table #TempTable
(
InvoiceId int,
VendorName Nvarchar(max),
InvoiceNumber Nvarchar(max),
InvoiceDate DateTime ,
TotalHours Nvarchar(max),
TotalAmount decimal(18,2),
IsPaid bit,
IsRejected bit,
ProjectId int,
ProjectName nvarchar(max),
CompanyName nvarchar(max)
)
Declare @sql  NVARCHAR(MAX);
Set @sql = N'select  I.Id InvoiceId
                                ,CONCAT(Ud.FirstName + '' '',Ud.FirstName) VendorName
                                ,I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
								inner join UserDetails Ud on Ud.Id = Vid.VendorId
                                inner join Companies c On c.Id = P.CompanyId
                                where  Ud.CompanyId = @VendorCompanyId and I.IsPaid=0 and I.IsRejected=0' 
+ CASE WHEN @VendorId     > 0	THEN N' and Ud.Id = @VendorId ' ELSE N'  'END
+ CASE WHEN @FromDate  Is not Null 	THEN N' and I.FromDate >= @FromDate ' ELSE N'  'END
+ CASE WHEN @ToDate   Is not Null 	THEN N' and I.ToDate <=  @ToDate ' ELSE N'  'END
+ CASE WHEN @CompanyId   >  0 	THEN N' and c.Id =  @CompanyId ' ELSE N'  'END


INSERT INTO #tempTable(InvoiceId,VendorName,InvoiceNumber,InvoiceDate,TotalHours,TotalAmount,IsPaid,IsRejected,ProjectId,ProjectName,CompanyName)
EXECUTE sp_executesql @Sql,N'@VendorCompanyId int,@CompanyId int,@VendorId int,@FromDate DateTime,@ToDate DateTime,@PageIndex int,@PageSizeSelected  int',
@VendorCompanyId ,@CompanyId ,@VendorId ,@FromDate ,@ToDate ,@PageIndex ,@PageSizeSelected 

SELECT RNUMBER, TBL.InvoiceId,TBL.VendorName,TBL.InvoiceNumber,TBL.InvoiceDate,TBL.TotalHours ,TBL.TotalAmount,TBL.IsPaid,TBL.IsRejected,TBL.ProjectId,
TBL.ProjectName,TBL.CompanyName
,(SELECT Count(Tb.InvoiceId) as TotalCount FROM #TempTable Tb ) as TotalPageCount
 From ( select ROW_NUMBER() OVER(ORDER BY InvoiceId) AS RNUMBER ,*
FROM #TempTable ) as TBL 
where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected
End
GO

-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,7 may 2018,>
-- Description:	<Description,,>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllInvoiceListVendorAdminProfilePaid]---[dbo].[Usp_GetAllInvoiceListVendorAdminProfilePaid] 2,0,0,null,null,1,50
	-- Add the parameters for the stored procedure here
@VendorCompanyId int
,@CompanyId int
,@VendorId int
,@FromDate DateTime
,@ToDate DateTime
,@PageIndex int
,@PageSizeSelected  int

AS
BEGIN
Create Table #TempTable
(
InvoiceId int,
VendorName Nvarchar(max),
InvoiceNumber Nvarchar(max),
InvoiceDate DateTime ,
TotalHours Nvarchar(max),
TotalAmount decimal(18,2),
IsPaid bit,
IsRejected bit,
ProjectId int,
ProjectName nvarchar(max),
CompanyName nvarchar(max)
)
Declare @sql  NVARCHAR(MAX);
Set @sql = N'select  I.Id InvoiceId
                                ,CONCAT(Ud.FirstName + '' '',Ud.FirstName) VendorName
                                ,I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
								inner join UserDetails Ud on Ud.Id = Vid.VendorId
                                inner join Companies c On c.Id = P.CompanyId
                                where  Ud.CompanyId = @VendorCompanyId and I.IsPaid=1 and I.IsRejected=0' 
+ CASE WHEN @VendorId     > 0	THEN N' and Ud.Id = @VendorId ' ELSE N'  'END
+ CASE WHEN @FromDate  Is not Null 	THEN N' and I.FromDate >= @FromDate ' ELSE N'  'END
+ CASE WHEN @ToDate   Is not Null 	THEN N' and I.ToDate <=  @ToDate ' ELSE N'  'END
+ CASE WHEN @CompanyId   >  0 	THEN N' and c.Id =  @CompanyId ' ELSE N'  'END


INSERT INTO #tempTable(InvoiceId,VendorName,InvoiceNumber,InvoiceDate,TotalHours,TotalAmount,IsPaid,IsRejected,ProjectId,ProjectName,CompanyName)
EXECUTE sp_executesql @Sql,N'@VendorCompanyId int,@CompanyId int,@VendorId int,@FromDate DateTime,@ToDate DateTime,@PageIndex int,@PageSizeSelected  int',
@VendorCompanyId ,@CompanyId ,@VendorId ,@FromDate ,@ToDate ,@PageIndex ,@PageSizeSelected 

SELECT RNUMBER, TBL.InvoiceId,TBL.VendorName,TBL.InvoiceNumber,TBL.InvoiceDate,TBL.TotalHours ,TBL.TotalAmount,TBL.IsPaid,TBL.IsRejected,TBL.ProjectId,
TBL.ProjectName,TBL.CompanyName
,(SELECT Count(Tb.InvoiceId) as TotalCount FROM #TempTable Tb ) as TotalPageCount
 From ( select ROW_NUMBER() OVER(ORDER BY InvoiceId) AS RNUMBER ,*
FROM #TempTable ) as TBL 
where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected
End
GO

-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,7 may 2018,>
-- Description:	<Description,,>
-- =============================================

CREATE Procedure [dbo].[Usp_GetAllInvoiceListVendorAdminProfileRejected]---[dbo].[Usp_GetAllInvoiceListVendorAdminProfileRejected] 2,0,0,null,null,1,50
	-- Add the parameters for the stored procedure here
@VendorCompanyId int
,@CompanyId int
,@VendorId int
,@FromDate DateTime
,@ToDate DateTime
,@PageIndex int
,@PageSizeSelected  int

AS
BEGIN
Create Table #TempTable
(
InvoiceId int,
VendorName Nvarchar(max),
InvoiceNumber Nvarchar(max),
InvoiceDate DateTime ,
TotalHours Nvarchar(max),
TotalAmount decimal(18,2),
IsPaid bit,
IsRejected bit,
ProjectId int,
ProjectName nvarchar(max),
CompanyName nvarchar(max)
)
Declare @sql  NVARCHAR(MAX);
Set @sql = N'select  I.Id InvoiceId
                                ,CONCAT(Ud.FirstName + '' '',Ud.FirstName) VendorName
                                ,I.InvoiceNumber InvoiceNumber,I.InvoiceDate InvoiceDate,I.TotalHours TotalHours
                                ,(I.TotalAmount -	I.TightenCommission ) TotalAmount 
                                 ,I.IsPaid IsPaid,I.IsRejected IsRejected,Vid.ProjectId ProjectId,P.Name ProjectName,
                                c.Name CompanyName from 
                                Invoices I inner join  
                                VendorInvoiceDetails Vid on I.Id = Vid.InvoiceId
                                inner join Projects P on P.Id = Vid.ProjectId 
								inner join UserDetails Ud on Ud.Id = Vid.VendorId
                                inner join Companies c On c.Id = P.CompanyId
                                where  Ud.CompanyId = @VendorCompanyId and I.IsRejected=1 and I.IsPaid=0' 
+ CASE WHEN @VendorId     > 0	THEN N' and Ud.Id = @VendorId ' ELSE N'  'END
+ CASE WHEN @FromDate  Is not Null 	THEN N' and I.FromDate >= @FromDate ' ELSE N'  'END
+ CASE WHEN @ToDate   Is not Null 	THEN N' and I.ToDate <=  @ToDate ' ELSE N'  'END
+ CASE WHEN @CompanyId   >  0 	THEN N' and c.Id =  @CompanyId ' ELSE N'  'END


INSERT INTO #tempTable(InvoiceId,VendorName,InvoiceNumber,InvoiceDate,TotalHours,TotalAmount,IsPaid,IsRejected,ProjectId,ProjectName,CompanyName)
EXECUTE sp_executesql @Sql,N'@VendorCompanyId int,@CompanyId int,@VendorId int,@FromDate DateTime,@ToDate DateTime,@PageIndex int,@PageSizeSelected  int',
@VendorCompanyId ,@CompanyId ,@VendorId ,@FromDate ,@ToDate ,@PageIndex ,@PageSizeSelected 

SELECT RNUMBER, TBL.InvoiceId,TBL.VendorName,TBL.InvoiceNumber,TBL.InvoiceDate,TBL.TotalHours ,TBL.TotalAmount,TBL.IsPaid,TBL.IsRejected,TBL.ProjectId,
TBL.ProjectName,TBL.CompanyName
,(SELECT Count(Tb.InvoiceId) as TotalCount FROM #TempTable Tb ) as TotalPageCount
 From ( select ROW_NUMBER() OVER(ORDER BY InvoiceId) AS RNUMBER ,*
FROM #TempTable ) as TBL 
where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected
End
GO



CREATE PROCEDURE usp_GetUsersForVendorAdminProfile   --usp_GetUsersForVendorAdminProfile 2,1,10
@VendorCompanyId int,
@PageIndex int,
@PageSizeSelected  int
AS
BEGIN
SET NOCOUNT ON;
DECLARE @sql NVARCHAR(MAX);
Create Table #TempTable
(
VendorId INT,
FirstName NVARCHAR(MAX),
LastName NVARCHAR(MAX),
VendorName NVARCHAR(MAX),
Email NVARCHAR (MAX),
JoinDate DATETIME ,
RoleName NVARCHAR(MAX),
RoleId NVARCHAR(MAX),
Rating NUMERIC(18,2),
ProfilePhoto NVARCHAR(MAX),
IsActive BIT,
Password NVARCHAR(MAX),
Indefinitely BIT,
ValidUntil BIT,
ValidDate DATETIME
)
set @sql='select ud.Id VendorId,UD.FirstName,UD.LastName, CONCAT(UD.FirstName + '' '',UD.LastName) VendorName,UD.Email Email, UD.EmailConfirmationCodeCreatedDate JoinDate  , 
          R.Name RoleName,R.RoleId ,(select round(AVG(PV.Rating),0) from ProjectVendor PV where PV.VendorId = Ud.Id) Rating ,
		  ISNULL(''Uploads/Profile/Thumbnail/''+ud.ProfilePhoto,''Uploads/Default/profile.png'') ProfilePhoto
          ,UD.IsActive, UD.Password,IsNull(UD.Indefinitely,1) Indefinitely ,IsNull( UD.ValidUntil,0) ValidUntil,UD.ValidUntilDate
          from UserDetails UD  Inner join Roles R on R.RoleId = UD.RoleId where UD.CompanyId = @VendorCompanyId and UD.RoleId <> 1'

INSERT INTO #TempTable(VendorId ,FirstName ,LastName ,VendorName ,Email ,JoinDate ,RoleName ,RoleId ,Rating ,ProfilePhoto ,IsActive ,Password ,Indefinitely ,ValidUntil ,ValidDate)
EXECUTE sp_executesql @sql, N'@VendorCompanyId int',@VendorCompanyId	

SELECT *  From ( select ROW_NUMBER() OVER(ORDER BY VendorId) AS RNUMBER ,*,(SELECT COUNT(T.VendorId) FROM #TempTable T) TotalPageCount FROM #TempTable ) as TBL 
where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected         
END
GO


CREATE PROCEDURE usp_GetResourcesForVendorAdminProfile   --usp_GetResourcesForVendorAdminProfile 2,2,30
@VendorCompanyId int,
@PageIndex int,
@PageSizeSelected  int
AS
BEGIN
SET NOCOUNT ON;
DECLARE @sql NVARCHAR(MAX);
Create Table #TempTable
(
FirstName NVARCHAR(MAX),
LastName NVARCHAR(MAX),
Email NVARCHAR (MAX),
Profile NVARCHAR(MAX),
Password NVARCHAR(MAX)
)
set @sql='select Distinct  PPRV.FirstName FirstName, PPRV.LastName LastName, PPRV.Email Email, PPRV.Profile Profile,  UD.Password from ProjectProposedResourcesByVendor PPRV
inner join UserDetails UD on PPRV.VendorId = UD.Id where UD.CompanyId = @VendorCompanyId 
and PPRV.Email Not In (select Email  from UserDetails where CompanyId = @VendorCompanyId)'

INSERT INTO #TempTable(FirstName ,LastName ,Email ,Profile ,Password)
EXECUTE sp_executesql @sql, N'@VendorCompanyId int',@VendorCompanyId	

SELECT *  From ( select ROW_NUMBER() OVER(ORDER BY Email) AS RNUMBER ,*,(SELECT COUNT(T.Email) FROM #TempTable T) TotalPageCount FROM #TempTable ) as TBL 
where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected         

END 
GO



---------------------------------------------------------------8 MAY 2018-------------------------------------------------

CREATE PROCEDURE usp_GetMyProjectsVendorGeneralProfile  --usp_GetMyProjectsVendorGeneralProfile '031a76a6-937b-429f-9868-15dc5f57b2ce',0,1,1,5
@VendorGeneralUserId  NVARCHAR(MAX),
@ProjectId int,
@ProjectStatus int, 
@PageIndex int,
@PageSizeSelected int
AS
BEGIN
SET NOCOUNT ON;
DECLARE @sql NVARCHAR(MAX)
DECLARE @query2 NVARCHAR(MAX)
Create Table #TempTable
(
ProjectId int,
ProjectName NVARCHAR(MAX),
ProjectStartDate DATETIME,
HoursSpent INT,
HoursAllotted INT,
RoleAllotted NVARCHAR(MAX),
Rating NUMERIC(18,2),
VendorPoc NVARCHAR(MAX)
)


IF (@ProjectId <> 0)
     SET @query2 = ' and P.Id = @ProjectId'
ELSE IF (@ProjectId <> 0 AND (@ProjectStatus = 1 OR @ProjectStatus = 3))
     SET @query2 = ' and P.Id = @ProjectId and P.IsComplete = @ProjectStatus'
ELSE IF (@ProjectId = 0 AND (@ProjectStatus= 1 OR @ProjectStatus = 3))
     SET @query2 = ' and P.IsComplete = @ProjectStatus'
ELSE IF (@ProjectStatus = 3)
     SET @query2 = 'and P.IsComplete = @ProjectStatus';

SET @sql='select  P.Id ProjectId,P.Name ProjectName,PV.HiredOn ProjectStartDate
	      ,ISNULL(PPR.SpentHours,0 ) HoursSpent 
          ,PPR.ExpectedHours HoursAllotted,R.Name RoleAllotted,PPR.Rating  Rating
	      ,(select Concat(UD.FirstName + '' '' ,UD.LastName)  from userDetails UD where  UD.Id = PV.vendorId ) VendorPoc
          from ProjectProposedResourcesByVendor PPR
          Inner JOin Projects P on P.Id = PPR.ProjectId 
          Inner JOin ProjectVendor PV on PV.ProjectId = PPR.ProjectId and PV.VendorId = PPR.VendorId
          Inner Join Roles R on R.RoleId = PPR.RoleId
          inner join UserDetails UD on UD.Email = PPR.Email
          where UD.UserId = @VendorGeneralUserId ' + @query2

INSERT INTO #TempTable(ProjectId ,ProjectName ,ProjectStartDate ,HoursSpent ,HoursAllotted ,RoleAllotted ,Rating ,VendorPoc)
EXECUTE sp_executesql @sql, N'@VendorGeneralUserId NVARCHAR(MAX), @ProjectId  int,@ProjectStatus int',@VendorGeneralUserId,@ProjectId,@ProjectStatus	

SELECT * From ( select ROW_NUMBER() OVER(ORDER BY ProjectId) AS RNUMBER ,*,(SELECT COUNT(T.ProjectId) FROM #TempTable T) TotalPageCount FROM #TempTable ) as TBL 
where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

END
GO



--------------------------------------------------19 May 2018-----------------------------------------------------

ALTER PROCEDURE usp_EnableGeneralCompanyAsVendorCompany
@UserId NVARCHAR(MAX),
@CompanyId INT
AS
BEGIN
SET NOCOUNT ON;
DECLARE @Email NVARCHAR(MAX)

SET @Email=(SELECT Email FROM UserDetails WHERE CompanyId=@CompanyId)
UPDATE Companies SET IsVendor=1,IsVendorFirmRegistered=1,IsActive=1 WHERE Id=@CompanyId

--------------------------------------Stripe Detail Update----------------------------------------------------------	
IF EXISTS(SELECT NULL FROM StripeAccountDetails WHERE UserId=@UserId)
BEGIN
UPDATE StripeAccountDetails SET UserId=@UserId,Email=@Email,Managed=1 WHERE UserId=@UserId
END
ELSE
BEGIN
INSERT INTO StripeAccountDetails (UserId,Email,Managed) VALUES(@UserId,@Email,1)
END

-------------------- Inserting default permissions for all roles in Permission table   ------ 2 is use for VendorCompany Profile
DELETE FROM Permissions WHERE CompanyId=@CompanyId
insert into Permissions (UserRole,MenuId,CompanyId,isActive,isDeleted) select P.UserRole,P.MenuId,@CompanyId,1,0  from Permissions P where CompanyId = 2 

END 
GO



-------------------------------22 may 2018-----------------------------------------------------

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER Procedure [dbo].[Usp_GetQuotedProjectForCompany] 
	-- Add the parameters for the stored procedure here

@ProjectId int,
@VendorId int,
@CompanyId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-----------------------------------ProjectVendorQuote---------------------------------------------------

	SELECT
      ProjectId
      ,VendorId
      ,Budget TotalBudget
      ,Hours TotalHours
      ,P.Description Description
	  ,P.StartDate ProjectStartDate
	  ,P.EndDate  projectEndDate
      ,Pvq.IsActive
      ,QuoteSenton
	  ,P.Name
	  ,Pvq.Description AllDescription
	  ,P.IsComplete
		FROM Tighten.dbo.ProjectVendorQuote Pvq
		inner join Projects P on P.Id = Pvq.ProjectId
		where Pvq.ProjectId = @ProjectId and Pvq.VendorId = @VendorId 

			-----------------------------------ProjectProjectInvitationAttachments---------------------------------------------------

		select PIA.ProjectId, PIA.FileName,PIA.FilePath,PIA.ActualFileName  from dbo.ProjectInvitationAttachments PIA
			inner join Projects P on P.Id = PIA.ProjectId where PIA.ProjectId = @ProjectId




			-----------------------------------ProjectPhases---------------------------------------------------
SELECT       
      Pph.Name Name
      ,Pph.Description Description
      ,Pphq.Budget PhaseVendorBudget
      ,Pphq.Hours PhaseVendorHour
	  ,Pph.Id PhaseId
	  ,Pphq.ProjectId
  FROM ProjectPhases Pph
  inner join dbo.ProjectPhaseQuotes Pphq on Pph.Id = Pphq.PhaseId
  where VendorId = @VendorId and Pphq.ProjectId =  @ProjectId and Pphq.IsActive = 1
  

					-----------------------------------PhaseAttachment---------------------------------------------------		

select Phd.PhaseId,Phd.ActualFileName,Phd.Path FilePath,Phd.Name FileName  from [dbo].[PhaseDocuments] Phd
inner join ProjectPhases Pph on Pph.Id = Phd.PhaseId
where ProjectId =  @ProjectId

-----------------------------------ProjectMileStoneQuotes---------------------------------------------------	
	 
	 
select Pms.Description Description
  ,Pms.Name Name
 ,Pmsq.Hours MilestoneVendorHour
 ,Pmsq.Budget MilestoneVendorBudget
 ,Pmsq.PhaseId PhaseId
 ,Pmsq.MileStoneId
 
  from dbo.ProjectMileStones Pms  
inner join dbo.ProjectMileStoneQuotes Pmsq  on Pmsq.MileStoneId =  Pms.Id

where Pmsq.VendorId = @VendorId and Pmsq.projectId = @ProjectId and Pmsq.IsDeleted = 0 and Pmsq.IsActive = 1

-----------------------------------ProjectProposedResourcesByVendor---------------------------------------------------	

SELECT
     Prv.Id
	 ,R.Name DesignationName
      ,ProjectId
      ,VendorId
      ,FirstName
      ,LastName
      ,Profile
      ,HourlyRate HourlyRate
	  ,ExpectedHours
	  ,Prv.SpentHours
  FROM ProjectProposedResourcesByVendor Prv
  inner join dbo.Roles R on R.RoleId = Prv.RoleId
  where Prv.VendorId = @VendorId and Prv.projectId = @ProjectId



  		--------------------CompanyProfile Qoute get-Custom control UI getting------------------------------------------------

	SELECT  Id , CompanyId, FieldName, Watermark, Description, ControlType,[Values], IsActive,Description DescriptionCustomControl
     ,IsDeleted FROM ProjectCustomizationSettings Where CompanyId = @CompanyId and IsDeleted = 0 and IsActive = 1 ORDER BY Priority

	---------------------------------Custom control UI getting------------------------------------------------


	---------------------------------Custom control Value getting------------------------------------------------
	select Id, [values] CustomControlData from ProjectCustomValues where ProjectId = @ProjectId

	------------------------------------------------------------------------------------------------------
END

Go

---------------------------------23 May 2018--------------------------------------------------------------
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER Procedure [dbo].[Ups_GetAllRolesForCompany]
	-- Add the parameters for the stored procedure here
@CompanyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select  RoleId as RoleId,Name as RoleName,Description as RoleDescription,CompanyId as CompanyId,[Priority] as
	 RolePriority,isActive as isActive from Roles where (CompanyId = @CompanyId or CompanyId = 0 )  and IsDeleted = 0
	 --and isActive = 1
END
GO



ALTER PROCEDURE usp_ActiveInactiveRoles
@RoleId int ,
@CompanyId int
AS BEGIN
SET NOCOUNT ON;
DECLARE @IsActive bit
SET @IsActive=( SELECT IsActive FROM Roles WHERE CompanyId=@CompanyId AND RoleId=@RoleId)
UPDATE ROLES SET isActive= case when @IsActive =1 then 0 else 1 end WHERE CompanyId=@CompanyId AND RoleId=@RoleId 
END
GO

-- =============================================
-- Author:		<Author,,Rakesh Kumar>
-- Create date: <Create Date,,24/May/2017>
-- Description:	<Description,,This procedure used to get details of freelancer >
-- =============================================

ALTER Procedure [dbo].[Usp_RegistrationVendorCompany]  
	 @EmailDomain 	Nvarchar(MAX)	
	 ,@Email  Nvarchar(MAX)    
	 ,@FirstName Nvarchar(MAX) 
	 ,@LastName Nvarchar(MAX) 
	 ,@CompanyName Nvarchar(MAX) 
	 ,@Password Nvarchar(MAX) 
	 ,@Phone Nvarchar(MAX) 
	 ,@Fax Nvarchar(MAX) 
	 ,@Website Nvarchar(MAX) 
	 ,@Address Nvarchar(MAX) 
	 ,@Description Nvarchar(MAX) 
	 ,@UserId Nvarchar(MAX) 
	 ,@RegistrationConfirmationCode Nvarchar(max)
	

AS
BEGIN


	Declare @emaildomaincount varchar(max)
	Declare @NewCompanyId int
	Declare @IsAgainRegister int
 set  @IsAgainRegister = 	(select Count(c.Id) from Companies C where C.Email = 'NoEmail@'+ @EmailDomain 
 and C.IsVendor = 1 and C.IsVendorFirmRegistered = 1)
if(@IsAgainRegister = 0 )
	
	Begin
    set @emaildomaincount = (Select  top 1 Count(C.Id) from Companies 
    C where SUBSTRING(C.Email, CHARINDEX('@', C.Email) + 1, 100) = @EmailDomain  and C.IsVendor = 1)  
    
	if(@emaildomaincount = 0)
		begin

		insert into Companies (CreatedBy,CreatedDate,IsVendor,IsVendorFirmRegistered,Email,PhoneNo,Fax,
		Website,Address,Description,Name,IsActive) 
		values(@UserId,GetDAte(),1,1,@Email,@Phone,@Fax,@Website,@Address,@Description,@CompanyName,1)
	
		set	 @NewCompanyId = SCOPE_IDENTITY()

		Insert into UserDetails(UserId,Email,CompanyId,FirstName,LastName,Password,RoleId,Rate,IsFreelancer,IsDeleted,EmailConfirmationCode,EmailConfirmed,IsStripeAccountVerified) 
		values(@UserId,@Email,@NewCompanyId,@FirstName,@LastName,@Password,1,10,0,0,@RegistrationConfirmationCode,1,1)


	--------------------------------------Stripe Detail Update---------------------------------------	
		insert into StripeAccountDetails (UserId,Email,Managed) values(@UserId,@Email,1)
	

			-------------------- Inserting default permissions for all roles in Permission table   ----	--2 is use for VendorCompany Profile
		insert into Permissions (UserRole,Link,MenuId,CompanyId,isActive,isDeleted) 
        select P.UserRole,P.Link,P.MenuId,@NewCompanyId,1,0  from Permissions P where CompanyId = -2 

		  insert into UserCompanyRoles (CompanyId,RoleId,UserId,CreatedDate) values(@NewCompanyId,1,@UserId,getdate())


		 select c.Id as VendorCompanyId,ud.UserId,Ud.Email,ud.FirstName,ud.LastName, 0 as IsAlreadyRegistered, Concat(Ud.FirstName +' ',Ud.LastName) ReceiverName ,ud.UserId
			from UserDetails Ud
		 inner join Companies C on C.Id = Ud.CompanyId where C.Id = @NewCompanyId

	end


	--------------------------update company if POC exist----------------------------------------------------------------------------
		else
			begin

		set	@NewCompanyId	=	(Select  top 1 c.Id from Companies 
						        C where C.Email = 'NoEmail@'+ @EmailDomain 
								 and c.IsVendorFirmRegistered = 0 and IsVendor = 1)
								 
					
    

				update Companies set 
				     CreatedBy = @UserId
					,CreatedDate = GetDATE()
					,IsVendor = 1
					,IsVendorFirmRegistered = 1
					,Email =@Email
					,PhoneNo = @Phone
					,Fax = @Fax
					,Website = @Website
					,Address = @Address
					,Description = @Description
					,Name = @CompanyName where id = @NewCompanyId

	
					Insert into UserDetails(UserId,Email,CompanyId,FirstName,LastName,Password,RoleId,Rate,IsFreelancer,IsDeleted,EmailConfirmationCode,EmailConfirmed,IsStripeAccountVerified) 
					values(@UserId,@Email,@NewCompanyId,@FirstName,@LastName,@Password,1,10,0,0,@RegistrationConfirmationCode,1,1)
					
				
				--------------------------------------Stripe Detail Update---------------------------------------	
					insert into StripeAccountDetails (UserId,Email,Managed) values(@UserId,@Email,1)

		
					
				     insert into UserCompanyRoles (CompanyId,RoleId,UserId,CreatedDate) values(@NewCompanyId,1,@UserId,getdate())
					  --update UserCompanyRoles set 
					  --RoleId = 1  where CompanyId = @NewCompanyId


					 select c.Id as VendorCompanyId,ud.UserId,Ud.Email,ud.FirstName,ud.LastName, 0 as IsAlreadyRegistered,  Concat(Ud.FirstName +' ',Ud.LastName) ReceiverName ,ud.UserId
					from UserDetails Ud
					inner join Companies C on C.Id = Ud.CompanyId where Ud.UserId = @UserId
				

			end
		end

		else
		Begin

				select 1 as IsAlreadyRegistered ,'Againregister' as ReceiverName 
			

					end

End
GO

------------------------------------------------------25 May 2018-----------------------------------

-- =============================================
-- Author:		<Author,Subia Rahman>
-- Create date: <Create Date,25 May 2018>
-- Description:	<Description,Funcationality to register new user and save role chosen form frontend copied from [Usp_RegisterVendor] only roleid added as new parameter>
-- =============================================
ALTER Procedure [dbo].[Usp_RegisterNewUser]
 @NewUserUserId         nvarchar(max)	
,@UserId				nvarchar(max)
,@FirstName			    nvarchar(max)
,@LastName   			nvarchar(max)
,@CompanyName			nvarchar(max)
,@EmailAddress			nvarchar(max)
,@CompanyId             int 
,@MessageTextBox        Nvarchar(max)
,@RoleId				int
,@RoleName              Nvarchar(max)
,@EmailConfirmationCode Nvarchar(max)
AS
BEGIN
SET NOCOUNT ON;
if not  Exists(select  Email from UserDetails where Email = @EmailAddress)
  begin


  	Declare @receiver Nvarchar(max)
	Declare @venderId Nvarchar(max)
	Declare @VendorInvitationId int
	Declare @VendorCompanyId int = 0
	Declare @Id int = 0
	Declare @CompanyMailId varchar(max)
	Declare @DomainExist varchar(max)
	Declare @CompanyExist int

    set  @CompanyMailId =  'NoEmail' + SUBSTRING(@EmailAddress,CHARINDEX ('@',@EmailAddress),100)

    set  @DomainExist =   SUBSTRING(@EmailAddress,CHARINDEX ('@',@EmailAddress)+1,100)

    set @VendorCompanyId =    
		 (select  Isnull(C.Id,0) from Companies C where  SUBSTRING(C.Email, CHARINDEX('@', C.Email) + 1, 100) = @DomainExist 
		 and  LOWER(C.Name)  = LOWER(@CompanyName))

			if(@VendorCompanyId = 0 or @VendorCompanyId Is null )
			-- Same company should not exist then we insert new company and domain should not exists 
				Begin

				insert into Companies (Name,CreatedBy,IsVendor,CreatedDate,IsActive,Email) values(@CompanyName,@NewUserUserId,1,getdate(),1,@CompanyMailId)
				set  @VendorCompanyId = SCOPE_IDENTITY()


					
	-------------------- inserting master data from SettingConfigs to UserSettingConfigs to receive notification via email,this functionality added after process for registernewuser method modified ----------------
	insert into UserSettingConfigs (CompanyId, UserId, NotificationId, IsActive) 
	select @CompanyId as CompanyId,@UserId as UserId, setconfig.Id as NotificationId, 1 as IsActive from SettingConfigs as setconfig 
	where setconfig.IsActive = 1 

				-------------------- Inserting default permissions for all roles in Permission table   ----------------

						insert into Permissions (UserRole, Link, MenuId, CompanyId, isActive,isDeleted) 
						select per.UserRole ,per.Link, per.MenuId, @VendorCompanyId as CompanyId,1 as isActive,0 as isDeleted   from Permissions  as per 
						where per.CompanyId = -1 and per.isDeleted = 0

				End

				-- --------------Insert into userdetail-------------
				insert into UserDetails (UserId,Email,FirstName,LastName,Rate,IsFreelancer,EmailConfirmationCode,EmailConfirmed,CompanyId,EmailConfirmationCodeCreatedDate,RoleId) 
				   values(@NewUserUserId,@EmailAddress,@FirstName ,@LastName,10,0,@EmailConfirmationCode,0,@VendorCompanyId,GETDATE(),@RoleId)---earlier 2 was statically passed for vendor

				   set @receiver = SCOPE_IDENTITY()
	
				--------------------------------------Stripe Detail Update---------------------------------------	
				insert into StripeAccountDetails (UserId,Email,Managed) values(@NewUserUserId,@EmailAddress,1)


	  			insert into UserCompanyRoles (CompanyId,RoleId,UserId,CreatedDate) values(@receiver,@RoleId,@NewUserUserId,getdate())---earlier 2 was statically passed for vendor


				   insert into [dbo].[VendorInvitations]  (

				   UserId
				  ,Invitedby
				  ,CompanyId
				  ,ProjectId
				  ,[Message]
				  ,InvitationType
				  ,InvitationDate) values(@NewUserUserId,@UserId,@CompanyId,0,@MessageTextBox,0,getdate())

				  set @VendorInvitationId = SCOPE_IDENTITY()



				set @NewUserUserId = (select UserId from UserDetails where UserId = @NewUserUserId)
	   
	   				insert into [dbo].[VendorInvitationMessage] (
				   UserId
				  ,SendBy
				  ,InvitationId
				  ,[Message]
				  ,CreatedDate) values(@NewUserUserId,@UserId,@VendorInvitationId,@MessageTextBox,getdate())




		set @id = 1

		select @Id as Id,(FirstName + ' ' + LastName) Name ,Email  ,UserId,EmailConfirmationCode, 
		(select Name as VendorCompany from Companies where Id = @VendorCompanyId) VendorCompany ,
		(SELECT Top 1 Name  FROM Tighten.dbo.Companies where Id = @CompanyId) as SenderCompanyName,
		(select Top 1 [Message]   from [dbo].[VendorInvitations] V  where UserId = @NewUserUserId  )
		 Message,@RoleName AS RoleName
		from UserDetails where UserId = @NewUserUserId



 -- 	commit transaction t1
 --   End Try

	--	Begin Catch
	--	rollback transaction t1
	--	select 1
 --           --return
	--End Catch
	   
	end

 -------------------------------------------------------------if vendor already exists------------------------------------------------------------------------- 
 --- 
	else 

    begin

	 select  @NewUserUserId = Ud.UserId,@VendorCompanyId = c.Id from UserDetails Ud inner join Companies c on Ud.CompanyId = c.Id where Ud.Email = @EmailAddress


		if not exists(select * from VendorInvitations where UserId =  @NewUserUserId and CompanyId = @CompanyId)

				  begin
				
						insert into [dbo].[VendorInvitations]  (

					   UserId
					  ,Invitedby
					  ,CompanyId
					  ,ProjectId
					  ,[Message]
					  ,InvitationType
					  ,InvitationDate) values(@NewUserUserId,@UserId,@CompanyId,0,@MessageTextBox,0,getdate())

					  set @VendorInvitationId = SCOPE_IDENTITY()


							set @NewUserUserId = (select UserId from UserDetails where UserId = @NewUserUserId)
	   
	   				insert into [dbo].[VendorInvitationMessage] (
				   UserId
				  ,SendBy
				  ,InvitationId
				  ,[Message]
				  ,CreatedDate) values(@NewUserUserId,@UserId,@VendorInvitationId,@MessageTextBox,getdate())

				  set @id  =2

					select @Id as Id, (FirstName + ' ' + LastName) Name ,Email  ,UserId,EmailConfirmationCode, (select Name as VendorCompany from Companies 
					where Id = @VendorCompanyId) VendorCompany ,(SELECT Top 1 Name  FROM Tighten.dbo.Companies where Id = @CompanyId) as SenderCompanyName,
					(select Top 1 [Message]   from [dbo].[VendorInvitations] V  where UserId = @NewUserUserId  ) Message,@RoleName AS RoleName
					from UserDetails where UserId = @NewUserUserId


			end
			  else

			  begin
			 set  @Id = -1     -- if -1 You have already sent the Invitation
			  select @Id as Id

			  end


  End
     
End

GO

-------------------------------------------------26 May 2018----------------------------------------


ALTER Procedure [dbo].[Usp_GetCompanyTeams]    --[Usp_GetCompanyTeams] 1,'de151472-1568-47d8-9049-1166a9e64174',1,10
@CompanyId int,
@UserId  nvarchar(max),
@PageIndex int,
@PageSizeSelected  int
AS
BEGIN
SET NOCOUNT ON;
 Declare @sql nvarchar(Max);
 Declare @sql1 nvarchar(Max);
Create Table #TempTable
(
TeamId int,
TeamName Nvarchar(max),
TeamBudget   money,
TeamDescription Nvarchar(max) ,
CreatedDate DateTime,
ParentId int,
ParentTeamId int,
IsParent int
)
  
  Create Table #TempTable1
(
TeamId int,
MemberEmail Nvarchar(max),
MemberName   Nvarchar(max),
IsActive bit,
MemberProfilePhoto Nvarchar(max),
MemberUserId NVARCHAR(MAX),
MemberRoleId int,
MemberDesignationId int
)

declare @UserRole int
set  @UserRole =  (select usrcom.RoleId from UserDetails user1  inner JOin UserCompanyRoles usrcom on user1.UserId = usrcom.UserId where user1.UserId = @UserId  and user1.IsDeleted = 0
and usrcom.CompanyId = @CompanyId)
  if(@UserRole <> 1)
  begin 

  set @sql='
  select m.Id as TeamId ,m.TeamName as TeamName, m.TeamBudget as TeamBudget,m.[Description] as TeamDescription,m.CreatedDate AS CreatedDate,m.ParentId,m.ParentTeamId
  ,case when m.Id in(select ParentTeamId from Teams) then m.Id else 0 end AS IsParent
  from Teams m left join TeamUsers tu  on m.Id = tu.TeamId 
  where m.IsDefault = 0 and m.IsDeleted = 0 and m.CompanyId = case when @CompanyId = 0 then m.CompanyId else @CompanyId end
  and( tu.UserId = @UserId or m.CreatedBy =  @UserId )
  group by m.Id,m.TeamName, m.TeamBudget, m.[Description],m.CreatedDate,m.ParentId,m.ParentTeamId; '

  INSERT INTO #TempTable(TeamId,TeamName,TeamBudget,TeamDescription,CreatedDate,ParentId,ParentTeamId,IsParent)
  EXECUTE sp_executesql @sql, N'@CompanyId int, @UserId  nvarchar(max)',@CompanyId,@UserId	

   SELECT *
   From ( select ROW_NUMBER() OVER(ORDER BY TeamId) AS RNUMBER ,*,(SELECT COUNT(T.TeamId) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

  set @sql1='With CTETeams --create CTE for check value
  AS
  (
   select m.Id as TeamId ,m.TeamName as TeamName, m.TeamBudget as TeamBudget,m.[Description] as TeamDescription ,m.CreatedDate AS CreatedDate,m.ParentId,m.ParentTeamId
   ,case when m.Id in(select ParentTeamId from Teams) then m.Id else 0 end AS IsParent
   from Teams m left join TeamUsers tu  on m.Id = tu.TeamId 
   where m.IsDefault = 0 and m.IsDeleted = 1 and m.IsDeleted = 0 and m.CompanyId = case when @CompanyId = 0 then m.CompanyId else @CompanyId end
   and( tu.UserId = @UserId or m.CreatedBy =  @UserId ) and m.IsDeleted = 0  
   group by m.Id,m.TeamName, m.TeamBudget, m.[Description],m.ParentId,m.ParentTeamId
   )
	select m1.TeamId as TeamId, m2.Email as MemberEmail,m2.FirstName as MemberName,m1.IsActive IsActive, 
	case when m2.ProfilePhoto is null then ''Uploads/Default/profile.png'' else ''Uploads/Profile/Icon/''+m2.ProfilePhoto end
	as MemberProfilePhoto, m2.UserId as MemberUserId,m1.RoleId,m1.DesignationId
	from TeamUsers m1 inner join UserDetails m2 on m1.UserId = m2.UserId where m1.TeamId  in (select T.TeamId from CTETeams T)
	and m2.IsDeleted = 0 and m1.IsActive = 1 and m2.IsDeleted = 0    order by CreatedDate'

    INSERT INTO #TempTable1(TeamId,MemberEmail,MemberName,IsActive,MemberProfilePhoto,MemberUserId,MemberRoleId,MemberDesignationId)
    EXECUTE sp_executesql @sql1, N'@CompanyId int, @UserId  nvarchar(max)',@CompanyId,@UserId	
    SELECT * FROM #TempTable1

  end
  else
  Begin

  set @sql='
  select m.Id as TeamId ,m.TeamName as TeamName, m.TeamBudget,m.[Description] TeamDescription ,m.CreatedDate,m.ParentId,m.ParentTeamId 
  ,case when m.Id in(select ParentTeamId from Teams) then m.Id else 0 end AS IsParent 
  from  Teams m 
  where m.IsDefault =0 and m.IsDeleted =0  and m.CompanyId = case when @CompanyId = 0 then m.CompanyId else @CompanyId end
  group by  m.Id ,m.TeamName, m.TeamBudget,m.[Description],m.CreatedDate,m.ParentId,m.ParentTeamId;'

   INSERT INTO #TempTable(TeamId,TeamName,TeamBudget,TeamDescription,CreatedDate,ParentId,ParentTeamId,IsParent)
   EXECUTE sp_executesql @sql, N'@CompanyId int',@CompanyId	

   SELECT * From ( select ROW_NUMBER() OVER(ORDER BY TeamId) AS RNUMBER ,*,(SELECT COUNT(T.TeamId) FROM #TempTable T) TotalPageCount	
   FROM #TempTable ) as TBL 
   where RNUMBER BETWEEN ( @PageIndex - 1 ) * @PageSizeSelected + 1 AND @PageIndex * @PageSizeSelected

   --create CTE for check value
  set @sql1='
  With CTETeams
  AS
  (
  select m.Id as TeamId ,m.TeamName as TeamName, m.TeamBudget,m.[Description] AS TeamDescription ,m.CreatedDate,m.ParentId,m.ParentTeamId
  ,case when m.Id in(select ParentTeamId from Teams) then m.Id else 0 end AS IsParent 
  from  Teams m 
  where m.IsDefault =0 and m.IsDeleted =0  and m.CompanyId = case when @CompanyId = 0 then m.CompanyId else @CompanyId end
  group by  m.Id ,m.TeamName, m.TeamBudget,m.[Description],m.CreatedDate,m.ParentId,m.ParentTeamId
  )
  select m1.TeamId as TeamId,  m2.Email as MemberEmail,m2.FirstName as MemberName,m1.IsActive as IsActive,

  case when m2.ProfilePhoto is null then ''Uploads/Default/profile.png'' else ''Uploads/Profile/Icon/''+m2.ProfilePhoto end

  as MemberProfilePhoto, m2.UserId as MemberUserId,m1.RoleId,m1.DesignationId

  from TeamUsers m1 inner join UserDetails m2 on m1.UserId = m2.UserId where m1.TeamId in (select T.TeamId from CTETeams T )

  and m2.IsDeleted = 0 and m1.IsActive = 1 and m2.IsDeleted = 0  order by CreatedDate'

  


  INSERT INTO #TempTable1(TeamId,MemberEmail,MemberName,IsActive,MemberProfilePhoto,MemberUserId,MemberRoleId,MemberDesignationId)
  EXECUTE sp_executesql @sql1, N'@CompanyId int',@CompanyId
  end 
  SELECT * FROM #TempTable1

END
Go



-- Batch submitted through debugger: SQLQuery35.sql|7|0|C:\Users\yorkweb2\AppData\Local\Temp\~vs7540.sql
ALTER Procedure [dbo].[Usp_InsertTeam] --[dbo].[Usp_InsertTeam] 0,'de151472-1568-47d8-9049-1166a9e64174','Sub Team 1',100,'desc',1,'adbd2e8e-683f-4e26-a010-936fbaf42aef','','11',9
@TeamId  int null,
@UserId nvarchar(max) ,
@TeamName nvarchar(max),
@TeamBudget bigint,
@Description nvarchar(max),
@companyId int,
@UserIdList nvarchar(max),
@UserRoleIdList nvarchar(max),
@DesignationIdList nvarchar(max),
@ParentTeamId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	  declare @Id int
	  declare @RowCount int
	  declare @RowIndex int
	  declare @CountDuplicateRow int
	  declare @IsAdmin int
	  declare @ParentId int
	 -- declare @ParentTeamIdVarialble int=0
	  set @CountDuplicateRow = (select count(Id)  from Teams where TeamName = @TeamName and IsDeleted = 0 and CompanyId = @companyId )
	    
	Begin
	If (@CountDuplicateRow = 0 )
	begin

	set @IsAdmin=(select count(Id) from UserDetails WHERE UserId=@UserId AND (RoleId=1 or IsNull(RoleId, '') = ''))--check if userid is of admin RoleId=1 for vendororganisation and null for general company
	IF(@IsAdmin=1)
	BEGIN
	--SET @ParentTeamId=@ParentTeamIdVarialble
	SET @ParentId=(select Id from UserDetails WHERE UserId=@UserId AND (RoleId=1 or IsNull(RoleId, '') = ''))
	END
	 
	ELSE
	BEGIN
	SET @ParentId=(select Id from UserDetails WHERE UserId=@UserId)
	END

	insert  into Teams (TeamName,TeamBudget,[Description],CreatedBy,CreatedDate,CompanyId,ModifiedBy,ModifiedDate,IsDefault,IsDeleted,ParentId,ParentTeamId)
    values(@TeamName,@TeamBudget,@Description,@UserId,GetDate(),@companyId,@UserId,GETDATE(),0,0,@ParentId,@ParentTeamId)
    set @Id = Scope_Identity()
	set @RowIndex = 1
	set @RowCount =  (select count(*) from dbo.Split(@UserIdList,','))
	if((isnull(@UserIdList,''))<>'')
    Begin
      while(@RowCount >= @RowIndex )
			Begin

			insert into TeamUsers (UserId,CreatedBy,CreatedDate, TeamId,IsActive,IsDeleted,RoleId,DesignationId)
			values( cast((select Items from  dbo.Split(@UserIdList,',') as splitString where splitString.RowNumber = @RowIndex) as nvarchar(max)),
			@UserId,GetDate(),@Id,1,0, cast((select Items from  dbo.Split(@UserRoleIdList,',') as splitUSerRoleString where splitUSerRoleString.RowNumber = @RowIndex) as nvarchar(max)),
			 cast((select Items from  dbo.Split(@DesignationIdList,',') as splitDesignationString where splitDesignationString.RowNumber = @RowIndex) as nvarchar(max))
			)

			insert into Notifications (Name,CreatedBy,CreatedDate,IsRead,Type,TeamId, NotifyingUserId)
			values('has created a New Team:',@UserId,GetDate(),0,1,@Id, cast((select Items from  dbo.Split(@UserIdList,',') as splitString where splitString.RowNumber = @RowIndex) as nvarchar(max)))


			set @RowIndex = @RowIndex + 1
			End
			

	  select @Id as Id
	  end

	  else
	  begin

	  select 111

	  end 

	  		 end

	  else
	  begin
	  select -2
	  end
			

		
	End
END

Go

---------------------------------------------12 june 2018----------------------------------
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER Procedure [dbo].[Usp_InsertRole]
	-- Add the parameters for the stored procedure here
	@Name              nvarchar(max),
	@Description       nvarchar(max), 
	@CreatedBy		   nvarchar(max),
	@ModifiedBy		   nvarchar(max),	
	@CompanyId	       int ,
	@Priority		   int,
	@RoleVisibleToVendor bit,
	@Type               int	
	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   if not exists(select * from  Roles  where name  = @Name  and (CompanyId = @CompanyId or CompanyId = 0) and IsDeleted = 0 and Type=@Type) --and [Priority] = @Priority
   begin

   if exists(select * from  Roles  where  (CompanyId = @CompanyId or CompanyId = 0) and [Priority] = @Priority and IsDeleted = 0)
			 begin
			  select -1
			 end

			 else
			 begin			
			  --if type =3 means it is both the designation and role then it deletes the already existing role and designation with same name for that company and then adds new record

			 if(@Type=3) 
			 begin
			 update dbo.Roles set IsDeleted=1 WHERE name  = @Name  and CompanyId = @CompanyId 
			 end

			  insert into Roles( 
			   Name
			  ,isActive
			  ,[Description]
			  ,CreatedBy
			  ,CreatedDate
			  ,ModifiedBy
			  ,ModifiedDate
			  ,CompanyId
			  ,[Priority]
			  ,IsDeleted
			  ,[RoleVisibleToVendor]
			  ,[Type]
			  )
			  values(
			  @Name,
			  1
			  ,@Description
			  ,@CreatedBy
			  ,GetDate()
			  ,@ModifiedBy
			  ,GETDATE()
			  ,@CompanyId
			  ,@Priority
			  ,0
			  ,@RoleVisibleToVendor
			  ,@Type
			  )

			  select 1
			 end



   end
   else
   begin

   select -2
   end
END
Go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER Procedure [dbo].[Ups_GetAllRolesForCompany] 
	-- Add the parameters for the stored procedure here
@CompanyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select  RoleId as RoleId,Name as RoleName,Description as RoleDescription,CompanyId as CompanyId,[Priority] as
	 RolePriority,isActive as isActive,CASE Type WHEN 1 THEN 'Role' WHEN 2 THEN 'Designation' WHEN 3 THEN 'Role and Designation' END AS Type from Roles where (CompanyId = @CompanyId or CompanyId = 0 )  and IsDeleted = 0
	 --and isActive = 1
END
GO

----------------------------------------------------------------19 June 2018------------------------------
create procedure usp_DeleteRoles 
@CompanyId int,
@RoleId int
AS
BEGIN
SET NOCOUNT ON;
update dbo.Roles set IsDeleted=1 WHERE RoleId=@RoleId and CompanyId=@CompanyId
Select @@ROWCOUNT 

END 
GO



