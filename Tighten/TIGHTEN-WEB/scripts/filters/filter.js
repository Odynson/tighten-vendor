﻿define(['app'], function (app) {
    app
    //    .filter("reverse", function () {
    //    return function (input) {
    //        var str = "";
    //        for (var i = 0; i < input.length; i++) {
    //            str = input.charAt(i) + str;
    //        }
    //        return str;
    //    }
    //})
     .filter('getExtension', function () {
         return function (url) {

             return url.split('.').pop();

         };
     })
    //.filter('searchfilter', function () {
    //    return function (input, query) {

    //        var r = RegExp('(' + query + ')', 'gi');
    //        if (input != null)
    //            return input.replace(r, '<span class="filteredComment" >$&</span>');
    //    }
    //})
    .filter('unique', function () {

        return function (items, filterOn) {

            if (filterOn === false) {
                return items;
            }

            if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
                var hashCheck = {}, newItems = [];

                var extractValueToCompare = function (item) {
                    if (angular.isObject(item) && angular.isString(filterOn)) {
                        return item[filterOn];
                    } else {
                        return item;
                    }
                };

                angular.forEach(items, function (item) {
                    var valueToCheck, isDuplicate = false;

                    for (var i = 0; i < newItems.length; i++) {
                        if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if (!isDuplicate) {
                        newItems.push(item);
                    }

                });
                items = newItems;
            }
            return items;
        };
    });
    app.filter('searchfilter', function () {
        return function (input, query) {

            var r = RegExp('(' + query + ')', 'gi');
            if (input != null)
                return input.replace(r, '<span class="filteredComment" >$&</span>');
        }
    });

});