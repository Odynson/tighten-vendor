﻿define(['app'], function (app) {
    app
    .filter('searchfilter', function () {
        return function (input, query) {

            var r = RegExp('(' + query + ')', 'gi');
            if (input != null)
                return input.replace(r, '<span class="filteredComment" >$&</span>');
        }
    })


});