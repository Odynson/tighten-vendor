﻿/// <reference path="libs/datatable/plugins/bootstrap/angular-datatables.bootstrap.min.js" />
/// <reference path="libs/datatable/plugins/bootstrap/angular-datatables.bootstrap.min.js" />
/// <reference path="libs/angular/angular-ui.min.js" />
/// <reference path="libs/angular/angular-ui.min.js" />
/// <reference path="libs/angular/angular.min.js" />
/// <reference path="libs/angular/angular-ui.min.js" />
/// <reference path="libs/angular/angular-ui.min.js" />
/// <reference path="controllers/vendorAdminController/vendorAdminProfileInvitationController.js" />
/// <reference path="controllers/vendorAdminController/vendorAdminProfileInvitationController.js" />
/// <reference path="libs/ng-rateit.js" />
/// <reference path="libs/ng-rateit.js" />
/// <reference path="libs/angular-auto-complete.js" />
/// <reference path="libs/angular-auto-complete.js" />
/// <reference path="factories/vendorFactory.js" />
/// <reference path="factories/vendorFactory.js" />
/// <reference path="libs/datatable/angular-datatables.js" />
/// <reference path="libs/datatable/jquery.dataTables.min.js" />



require.createNode = function (config, moduleName) {
    var node = config.xhtml ?
        document.createElementNS('http://www.w3.org/1999/xhtml', 'html:script') :
        document.createElement('script');
    node.type = config.scriptType || 'text/javascript';
    node.charset = 'utf-8';
    node.async = true;

    if (moduleName == "dropBoxApi") {
        node.setAttribute("data-app-key", "dqa3b7ve1qmmcuf");
        node.setAttribute("id", "dropboxjs");
    }
    return node;
};

//////////////////////////////////////////////////////////
require.config({
    baseurl: '/scripts/',
    // this is for the development phase 
    // this is used for the versioning of the js files
    //  urlArgs: 'v=' + (new Date()).getTime(), // for  cashe in client side and comment to send in server

    waitSeconds: 200,
    paths: {
        'angular': 'libs/angular',
        'ngStorage': 'libs/ngStorage',
        'ngCookies': 'libs/angular-cookies',
        'ngAnimate': 'libs/angular-animate.min',
        'ngSanitize': 'libs/angular-sanitize',
        'ui-router': 'libs/angular-ui-router',
        'jquery': 'libs/jquery-1.10.2',
        'jquery-ui': 'libs/jquery-ui.min',
        'bootstrap': 'libs/bootstrap',
        'ui.bootstrap': 'libs/ui-bootstrap-tpls-0.14.1.min',
        'route': 'routes/routeConfig',
        'toaster': 'libs/angular-toaster/toaster',
        'jselectize': 'libs/Angular-Selectize/selectize',
        'selectize': 'libs/Angular-Selectize/angular-selectize',
        'angular-confirm': 'libs/Angular-Confirm/Scripts/angular-confirm',
        'textAngular-sanitize': 'libs/Angular-TextEditor/Scripts/textAngular-sanitize.min',
        'textAngular': 'libs/Angular-TextEditor/Scripts/textAngular.min',
        'angular-ui-tree': 'libs/angular-ui-tree/angular-ui-tree',
        'angularFileUpload': 'libs/angular-fileupload/angular-file-upload',
        'multiple-select': 'libs/select/isteven-multi-select',
        'ui-sortable': 'libs/sortable',
        'datatable': 'libs/datatable/jquery.dataTables.min',
        'ngbootstrap': 'libs/datatable/plugins/bootstrap/angular-datatables.bootstrap.min',
        'ngdatatables': 'libs/datatable/angular-datatables.min',
        //'JSZip': 'libs/jszip.min',
        
        //'angular':'libs/angular-0.10.5',
        //////////////////Related to calendar////////////////////
        'moment': 'libs/angular-calendar/moment.min',
        'calendar': 'libs/angular-calendar/calendar',
        'fullcalendar': 'libs/angular-calendar/fullcalendar.min',
        'gcal': 'libs/angular-calendar/gcal',
        'ngResource': 'libs/resource/angular-resource.min',
        ////////////////Related to plugins/////////////////////////
        'dropbox-picker': 'libs/dropbox-picker/dropbox-picker.min',
        'boxApi': 'libs/dropbox-picker/BoxApiJS/select',
        'dropBoxApi': 'libs/dropbox-picker/DropBoxApi/dropins',
        'lk-google-picker': 'libs/google-picker/google-picker',
        'googleApi': 'libs/google-picker/googleApi/api',

        'ngscroll': 'libs/jquery.slimscroll/jquery.slimscroll',
        'ng-tags': 'libs/ngTagsInput/ng-tags-input.min',
        'angularTrix': 'libs/richTextEditor/angular-trix',
        'trix': 'libs/richTextEditor/trix',
        'contextMenuApp': 'libs/contextMenu/contextMenu',
        'autoCompleteModule': 'libs/angular-auto-complete',
        'ngRateIt': 'libs/angular-rateit-master/src/ng-rateit',

        'angularTreeview': 'libs/treeViewSimpleAngularjs/angularTreeview',


        //'stripe': 'libs/Stripe/Stripe',
        //'loginFactory': 'factories/loginFactory',
        //'Base64Factory': 'factories/Base64Factory',
        'transactionsFactory': 'factories/transactionsFactory',
        'subscriptionFactory': 'factories/subscriptionFactory',
        'authenticationFactory': 'factories/authenticationFactory',
        'registrationFactory': 'factories/registrationFactory',
        'confirmEmailFactory': 'factories/confirmEmailFactory',
        'FreelancerAccountFactory': 'factories/FreelancerAccountFactory',
        'clientDashboardFactory': 'factories/clientDashboardFactory',
        'userDashboardFactory': 'factories/userDashboardFactory',
        'clientFeedbackFactory': 'factories/clientFeedbackFactory',
        'usersFactory': 'factories/usersFactory',
        'teamFactory': 'factories/teamFactory',
        'teamdetailFactory': 'factories/teamdetailFactory',
        'projectsFactory': 'factories/projectsFactory',
        'todosFactory': 'factories/todosFactory',
        'todoNotificationsFactory': 'factories/todoNotificationsFactory',
        'attachmentsFactory': 'factories/attachmentsFactory',
        'followersFactory': 'factories/followersFactory',
        'sectionsFactory': 'factories/sectionsFactory',
        'projectUsersFactory': 'factories/projectUsersFactory',
        'companiesFactory': 'factories/companiesFactory',
        'accountFactory': 'factories/accountFactory',
        'roleFactory': 'factories/roleFactory',
        'notificationFactory': 'factories/notificationFactory',
        'CalendarEventsFactory': 'factories/CalendarEventsFactory',
        'setPasswordFactory': 'factories/setPasswordFactory',
        'userFeedbackFactory': 'factories/userFeedbackFactory',
        'emailFactory': 'factories/emailFactory',
        'assigneeFactory': 'factories/assigneeFactory',
        'todoManualTrackingFactory': 'factories/todoManualTrackingFactory',
        'todoApprovalFactory': 'factories/todoApprovalFactory',
        'userTodoApprovalFactory': 'factories/userTodoApprovalFactory',
        'globalFactory': 'factories/globalFactory',

        'reportersFactory': 'factories/reportersFactory',
        'invoiceFactory': 'factories/invoiceFactory',
        'userInvoiceFactory': 'factories/userInvoiceFactory',
        'FreelancerInvitationFactory': 'factories/FreelancerInvitationFactory',
        'invitationFactory': 'factories/invitationFactory',
        'organizationFactory': 'factories/organizationFactory',
        'userMessagesFactory': 'factories/userMessagesFactory',
        'userProfileFactory': 'factories/userProfileFactory',
        'adminInvitationFactory': 'factories/adminInvitationFactory',
        'teamFinancialManagerFactory': 'factories/TeamFinancialManagerFactory',
        'settingsFactory': 'factories/settingsFactory',
        'accountSettingsFactory': 'factories/accountSettingsFactory',
        'userSubscriptionRecurringFactory': 'factories/userSubscriptionRecurringFactory',
        'paymentHistoryFactory': 'factories/paymentHistoryFactory',
        'rolePermissionFactory': 'factories/rolePermissionFactory',
        'roleManageFactory': 'factories/roleManageFactory',
        'paymentDetailFactory': 'factories/paymentDetailFactory',
        'projectFeedbackFactory': 'factories/projectFeedbackFactory',
        'feedbackFactory': 'factories/feedbackFactory',

        'commonFunctionService': 'factories/commonFunctionService',
        'sharedService': 'factories/sharedService',
        'utilityService': 'factories/utilityService',
        'vendorFactory': 'factories/vendorFactory',
        'companyAccountFactory': 'factories/companyAccountFactory',
        'initiativeFactory': 'factories/initiativeFactory',
        'vendorProjectFactory': 'factories/vendorProjectFactory',
        'vendorInvoiceFactory': 'factories/vendorInvoiceFactory',
        'vendorAdminProfileFactory': 'factories/vendorAdminFactory/vendorAdminProfileFactory',
        'userManagementFactory': 'factories/vendorAdminFactory/userManagementFactory',
        'roleVendorAdminProfileFactory': 'factories/vendorAdminFactory/roleVendorAdminProfileFactory',
        'vendorGeneralProfileFactory': 'factories/vendorGeneralUserFactory/vendorGeneralProfileFactory',
        'worklogFactory': 'factories/worklogFactory',
        'parentAccountFactory': 'factories/parentAccountFactory',


        'sideMenuController': 'controllers/sideMenuController',
        'headerMenuController': 'controllers/headerMenuController',
        'loginController': 'controllers/loginController',
        'registrationController': 'controllers/registrationController',
        'confirmEmailController': 'controllers/confirmEmailController',
        'ConfirmFreelancerController': 'controllers/ConfirmFreelancerController',
        'clientDashboardController': 'controllers/clientDashboardController',
        'userDashboardController': 'controllers/userDashboardController',
        'projectController': 'controllers/projectsController',
        'teamController': 'controllers/teamController',
        'teamdetailController': 'controllers/teamdetailController',
        'todoController': 'controllers/todoController',
        'companyController': 'controllers/companyController',
        'initiativeController': 'controllers/initiativeController',
        'globalController': 'controllers/globalController',
        'userProfileController': 'controllers/userProfileController',
        'userManageController': 'controllers/userManageController',
        'notificationController': 'controllers/notificationController',
        'feedbackController': 'controllers/feedbackController',
        'inboxController': 'controllers/inboxController',
        'setPasswordController': 'controllers/setPasswordController',
        'calendarController': 'controllers/calendarController',
        'todoManualTrackingController': 'controllers/todoManualTrackingController',
        'todoApprovalController': 'controllers/todoApprovalController',
        'userTodoApprovalController': 'controllers/userTodoApprovalController',
        'invoiceController': 'controllers/invoiceController',
        'userInvoiceController': 'controllers/userInvoiceController',
        'invitationController': 'controllers/invitationController',
        'organizationController': 'controllers/organizationController',
        'settingsController': 'controllers/settingsController',
        'adminInvitationController': 'controllers/adminInvitationController',
        'userSubscriptionController': 'controllers/userSubscriptionController',
        'teamFinancialManagerController': 'controllers/TeamFinancialManagerController',
        'accountSettingsController': 'controllers/accountSettingsController',
        'userSubscriptionRecurringController': 'controllers/userSubscriptionRecurringController',
        'paymentHistoryController': 'controllers/paymentHistoryController',
        'rolePermissionController': 'controllers/rolePermissionController',
        'roleManageController': 'controllers/roleManageController',
        'paymentDetailController': 'controllers/paymentDetailController',
        'projectFeedbackController': 'controllers/projectFeedbackController',
        'userFeedbackController': 'controllers/userFeedbackController',
        'addUpdateProjectController': 'controllers/addUpdateProjectController',
        'vendorCompanyProfileController': 'controllers/vendorCompanyProfileController',
        'GeneralCompaniesOfVendorConroller': 'controllers/GeneralCompaniesOfVendorConroller',
        'GenCompanyAccOfVendorController': 'controllers/GenCompanyAccOfVendorController',
        'vendorCompanyAccountController': 'controllers/vendorCompanyAccountController',
        'addVendorCompanyAccountController': 'controllers/addVendorCompanyAccountController',
        'CompanyAccountDetailController': 'controllers/CompanyAccountDetailController',
        'vendorForVendorProfileController': 'controllers/vendorForVendorProfileController',
        'vendorProjectController': 'controllers/vendorProjectController',
        'vendorProjectQuoteController': 'controllers/vendorProjectQuoteController',
        'quotedProjectAcceptOrRejectController': 'controllers/quotedProjectAcceptOrRejectController',
        'projectQuoteListForVendorProfileController': 'controllers/projectQuoteListForVendorProfileController',
        'editVendorProjectController': 'controllers/editVendorProjectController',
        'projectAwardController': 'controllers/projectAwardController',
        'projectAwardedViewDetailController': 'controllers/projectAwardedViewDetailController',
        'invoiceRaiseController': 'controllers/invoiceRaiseController',
        'invoiceListForVendorProfileController': 'controllers/invoiceListForVendorProfileController',
        'vendorAccountSettingsController': 'controllers/vendorAccountSettingsController',
        'vendorInvoiceListForCopanyProfileController': 'controllers/vendorInvoiceListForCopanyProfileController',
        'projectAwardForCompanyProfileController': 'controllers/projectAwardForCompanyProfileController',
        'projectAwardedViewDetailForCompanyProfileController': 'controllers/projectAwardedViewDetailForCompanyProfileController',
        'vendorProjectCompletionController': 'controllers/vendorProjectCompletionController',
        'inviteVendorCompanyRegistration': 'controllers/inviteVendorCompanyRegistration',
        'vendorCompanyRegistrationController': 'controllers/vendorCompanyRegistrationController',
        'vendorAdminProfileInvitationController': 'controllers/vendorAdminController/vendorAdminProfileInvitationController',
        'projectToQuoteListVendorAdminProfileController': 'controllers/vendorAdminController/projectToQuoteListVendorAdminProfileController',
        'projectToQuoteViewDetailByVendorAdminProfileController': 'controllers/vendorAdminController/projectToQuoteViewDetailByVendorAdminProfileController',
        'projectAwardedToVendorAdminProfileController': 'controllers/vendorAdminController/projectAwardedToVendorAdminProfileController',
        'projectAwardedViewDetailVendorAdminProfileController': 'controllers/vendorAdminController/projectAwardedViewDetailVendorAdminProfileController',
        'projectCompletionFeedBackVendorAdminProfileController': 'controllers/vendorAdminController/projectCompletionFeedBackVendorAdminProfileController',
        'invoiceListForVendorAdminProfileController': 'controllers/vendorAdminController/invoiceListForVendorAdminProfileController',
        'userManagementVendorAdminProfileController': 'controllers/vendorAdminController/userManagementVendorAdminProfileController',
        'vendorAdminProfileAccountSettingsController': 'controllers/vendorAdminController/vendorAdminProfileAccountSettingsController',
        'roleManageVendorAdminProfileController': 'controllers/vendorAdminController/roleManageVendorAdminProfileController',
        'myProjectVendorGeneralUserProfile': 'controllers/vendorGeneralUserController/myProjectVendorGeneralUserProfile',
        'ProjectCustomizationControlController': 'controllers/ProjectCustomizationControlController',
        'myProjectViewDetailVendorGeneralUserProfile': 'controllers/vendorGeneralUserController/myProjectViewDetailVendorGeneralUserProfile',
        'newUserController': 'controllers/newUserController',
        'orgChartDemoController': 'controllers/orgChartDemoController',
        'CompanyAccountFilePreviewController': 'controllers/CompanyAccountFilePreviewController',
        'worklogController': 'controllers/worklogController',
        'addNewWorkLogController': 'controllers/addNewWorkLogController',
        'editWorkLogController': 'controllers/editWorkLogController',
        'worklogEnteryController': 'controllers/worklogEnteryController',
        'addNewWorkLogEntryController': 'controllers/addNewWorkLogEntryController',
        'editWorkLogEntryController': 'controllers/editWorkLogEntryController',
        'parentAccountController': 'controllers/parentAccountController',
        'addParentAccountController': 'controllers/addParentAccountController',
        'editParentAccountController': 'controllers/editParentAccountController',
        'parentAccountForVendorController': 'controllers/parentAccountForVendorController',
        'addNewInitiativeController': 'controllers/addNewInitiativeController',
        'editInitiativeController': 'controllers/editInitiativeController',
        'filter': 'filters/filter',


        ////////////////Related to Directives/////////////////////////
        'appConstant': 'const/constant',

        'teamCountDirective': 'directives/teamCountDirective/teamCountDirective',

        'projectCountDirective': 'directives/projectCountDirective/projectCountDirective',
        'todoCountDirective': 'directives/todoCountDirective/todoCountDirective',

        'comingtodosCountDirective': 'directives/comingtodosCountDirective/comingtodosCountDirective',
        'expiredduedatetodosCountDirective': 'directives/expiredduedatetodosCountDirective/expiredduedatetodosCountDirective',
        'pendingtodosCountDirective': 'directives/pendingtodosCountDirective/pendingtodosCountDirective',

        'overbudgetedprojectsCountDirective': 'directives/overbudgetedprojectsCountDirective/overbudgetedprojectsCountDirective',
        'topFiveTasksDirective': 'directives/topFiveTasksDirective/topFiveTasksDirective',
        'freelancersHiredDirective': 'directives/freelancersHiredDirective/freelancersHiredDirective',
        'customControlRenderDirective': 'directives/customControlRenderDirective/customControlRenderDirective',
        'nestedTeamRenderDirective': 'directives/nestedTeamRenderDirective/nestedTeamRenderDirective',
        'orgchart': 'jquery.orgchart',


        'appDirective': 'directives/appDirective',
        'appConfig': 'routes/config',
        /*
        //'googleClient': '//apis.google.com/js/client',
        //'googleAPI': '//apis.google.com/js/api',
        //'dropboxSelect': 'https://app.box.com/js/static/select',
        //'dropboxAPI': 'https://www.dropbox.com/static/api/2/dropins'
        */
    },
    shim: {
        ngStorage: {
            deps: ['angular'],
            exports: 'angular'
        },
        ngCookies: {
            deps: ['angular'],
            exports: 'angular'
        },
        'ui-router': {
            deps: ['angular'],
            exports: 'angular'
        },
        angular: {
            exports: 'angular'
        },
        ngAnimate: {
            deps: ['angular'],
            exports: 'angular'
        },
        //'textAngular': {
        //    deps: ['angular', 'textAngular-sanitize']
        //},
        'ngSanitize': {
            deps: ['angular'],
            //exports: 'angular'
        },
        toaster: {
            deps: ['angular', 'ngAnimate'],
            exports: 'angular'
        },

        'ui.bootstrap': {
            deps: ['angular']
        },
        'angular-confirm': {
            deps: ['angular', 'ui.bootstrap'],
            exports: 'angular'
        },
        'angular-ui-tree': {
            deps: ['angular']
        },
        'ui-sortable': {
            deps: ['angular', 'jquery', 'jquery-ui'],
            exports: 'angular'
        },

        //'angular': {
        //    deps: ['angular'],
        //    exports: 'angular'
        //},

        'selectize': {
            deps: ['jquery', 'jselectize', 'angular']
        },
        bootstrap: {
            deps: ['jquery']
        },
        'angularFileUpload': {
            deps: ['angular']
        },
        'multiple-select': {
            deps: ['angular']
        },
        'fullcalendar': {
            deps: ['jquery', 'moment']
        },


        'calendar': {
            deps: ['angular', 'fullcalendar']
        },
        'dropbox-picker': {
            deps: ['angular'],
            exports: 'angular'
        },
        'lk-google-picker': {
            deps: ['angular'],
            exports: 'angular'
        },

        'todoController': {
            deps: ['dropbox-picker', 'boxApi', 'dropBoxApi', 'lk-google-picker', 'googleApi'],
            exports: 'angular'
        },
        'ngscroll': {
            deps: ['jquery']
        },
        'ng-tags': {
            deps: ['angular']
        },
        'contextMenuApp': {
            deps: ['angular']
        },

        'angularTrix': {
            deps: ['angular'],
            exports: 'angular'
        },
        'userProfileController': {
            deps: ['angularTrix', 'trix'],
            exports: 'angular'
        },


        'userDashboardController': {
            deps: ['teamCountDirective', 'projectCountDirective', 'todoCountDirective', 'comingtodosCountDirective', 'expiredduedatetodosCountDirective',
                'pendingtodosCountDirective', 'overbudgetedprojectsCountDirective', 'topFiveTasksDirective', 'freelancersHiredDirective'],
            exports: 'angular'
        },

        'teamController': {
            deps: ['nestedTeamRenderDirective'],
            exports: 'angular'
        },
        'teamdetailController': {
            deps: ['jquery'],
            exports: 'angular'
},
        'vendorProjectController': {
            deps: ['customControlRenderDirective'],
            exports: 'angular'
        },
        'editVendorProjectController': {
            deps: ['customControlRenderDirective'],
            exports: 'angular'
        },
        'projectAwardedViewDetailForCompanyProfileController': {
            deps: ['customControlRenderDirective'],
            exports: 'angular'
        },


        'vendorProjectQuoteController': {
            deps: ['customControlRenderDirective'],
            exports: 'angular'
        },
        'projectAwardedViewDetailController': {
            deps: ['customControlRenderDirective'],
            exports: 'angular'
        },
        'addVendorCompanyAccountController': {
            deps: ['customControlRenderDirective'],
            exports: 'angular'
        },
        'CompanyAccountDetailController': {
            deps: ['customControlRenderDirective'],
            exports: 'angular'
        },
        ngResource: {
            deps: ['angular', 'ngAnimate'],
            exports: 'angular'
        },

        'ngRateIt': {
            deps: ['jquery', 'angular'],
            "exports": 'angular'
        },
        'angularTreeview': {
            "deps": ['angular'],
            "exports": 'angular'
        },

        'datatable': {
            deps: ['jquery']
        },
        'ngbootstrap': {
            "deps": ['angular'],
            "exports": 'angular'
        },
        'ngdatatables': {
            "deps": ['jquery', 'angular', 'datatable', 'ngbootstrap', 'ngResource'],
            "exports": 'angular'
        },
        //'JSZip': {
        //    deps: ['jquery', 'angular'],
        //    "exports": 'angular'
        //},
       
        'orgchart': {
            deps: ['jquery']
        }
    },
    deps: ['app'],

});

require([
    'app',
    'ngCookies',
    'route',
    'jquery',
    'jquery-ui',
    'bootstrap',
    'ui.bootstrap',
    'ngAnimate',
    'jselectize',
    'ngSanitize',
    'toaster',
    'selectize',
    'angular-confirm',
    'angular-ui-tree',
    'ui-sortable',
    //'angular',
    'angularFileUpload',
    'multiple-select',
    'moment',
    'fullcalendar',
    'ng-tags',
    //'gcal',
    //'stripe',

    'calendar',
    'ngResource',
    //'ngSelectize',
    //'googleClient',
    //'googleAPI',
    //'dropboxSelect',
    //'dropboxAPI',
    //'Base64Factory'
    'transactionsFactory',
    'subscriptionFactory',
    'authenticationFactory',
    'registrationFactory',
    'confirmEmailFactory',
    'FreelancerAccountFactory',
    'clientDashboardFactory',
    //'userDashboardFactory',
    //'clientFeedbackFactory',
    'usersFactory',
    'teamFactory',
    'teamdetailFactory',
    'projectsFactory',
    'todosFactory',
    'todoNotificationsFactory',
    'attachmentsFactory',
    'followersFactory',
    'sectionsFactory',
    'projectUsersFactory',
    'companiesFactory',
    'accountFactory',
    'roleFactory',
    'notificationFactory',
    'CalendarEventsFactory',
    'setPasswordFactory',
    'emailFactory',
    'assigneeFactory',
    'globalFactory',

    'reportersFactory',
    'invoiceFactory',
    'userInvoiceFactory',
    'FreelancerInvitationFactory',
    'invitationFactory',
    'organizationFactory',
    'userMessagesFactory',
    'userProfileFactory',
    'adminInvitationFactory',
    'teamFinancialManagerFactory',
    'settingsFactory',
    'accountSettingsFactory',
    'userSubscriptionRecurringFactory',
    'paymentHistoryFactory',
    'rolePermissionFactory',
    'roleManageFactory',
    'paymentDetailFactory',
    'projectFeedbackFactory',
    'feedbackFactory',
    'vendorFactory',
    'companyAccountFactory',
    'initiativeFactory',
    'vendorProjectFactory',
    'vendorInvoiceFactory',
    'vendorAdminProfileFactory',
    'userManagementFactory',
    'roleVendorAdminProfileFactory',
    'vendorGeneralProfileFactory',
    'worklogFactory',
    'parentAccountFactory',


    'commonFunctionService',
    'sharedService',
    //'utilityService',

    'filter',
    'appDirective',
    'appConstant',
    'teamCountDirective',
    'projectCountDirective',
    'todoCountDirective',
    'comingtodosCountDirective',
    'expiredduedatetodosCountDirective',
    'pendingtodosCountDirective',
    'overbudgetedprojectsCountDirective',
    'topFiveTasksDirective',
    'freelancersHiredDirective',
    'customControlRenderDirective',
    'nestedTeamRenderDirective',



    'appConfig',
    'loginController',
    'registrationController',
    'confirmEmailController',
    'ConfirmFreelancerController',
    'clientDashboardController',
    'userDashboardController',
    'teamController',
    'teamdetailController',
    'projectController',
    'sideMenuController',
    'headerMenuController',
    'todoController',
    'companyController',
    'initiativeController',
    'globalController',
    'userProfileController',
    'userManageController',
    'notificationController',
    'feedbackController',
    'inboxController',
    'setPasswordController',
    'calendarController',
    'todoManualTrackingController',
    'todoApprovalController',
    'userTodoApprovalController',
    'invoiceController',
    'userInvoiceController',
    'invitationController',
    'organizationController',
    'settingsController',
    'adminInvitationController',
    'userSubscriptionController',
    'teamFinancialManagerController',
    'accountSettingsController',
    'userSubscriptionRecurringController',
    'paymentHistoryController',
    'rolePermissionController',
    'roleManageController',
    'paymentDetailController',
    'projectFeedbackController',
    'userFeedbackController',
    'addUpdateProjectController',
    'vendorCompanyProfileController',
    'GeneralCompaniesOfVendorConroller',
    'GenCompanyAccOfVendorController',
    'vendorCompanyAccountController',
    'addVendorCompanyAccountController',
    'vendorForVendorProfileController',
    'vendorProjectController',
    'vendorProjectQuoteController',
 
    'quotedProjectAcceptOrRejectController',
    'projectQuoteListForVendorProfileController',
    'editVendorProjectController',
    'projectAwardController',
    'projectAwardedViewDetailController',
    'invoiceRaiseController',
    'invoiceListForVendorProfileController',
    'vendorAccountSettingsController',
    'vendorInvoiceListForCopanyProfileController',
    'projectAwardForCompanyProfileController',
    'projectAwardedViewDetailForCompanyProfileController',
    'vendorProjectCompletionController',
    'inviteVendorCompanyRegistration',
    'vendorCompanyRegistrationController',
    'vendorAdminProfileInvitationController',
    'projectToQuoteListVendorAdminProfileController',
    'projectToQuoteViewDetailByVendorAdminProfileController',
    'projectAwardedToVendorAdminProfileController',
    'projectAwardedViewDetailVendorAdminProfileController',
    'projectCompletionFeedBackVendorAdminProfileController',
    'invoiceListForVendorAdminProfileController',
    'userManagementVendorAdminProfileController',
    'vendorAdminProfileAccountSettingsController',
    'roleManageVendorAdminProfileController',
    'myProjectVendorGeneralUserProfile',
    'ProjectCustomizationControlController',
    'CompanyAccountDetailController',
    'myProjectViewDetailVendorGeneralUserProfile',
    'newUserController',
    'orgChartDemoController',
    'CompanyAccountFilePreviewController',
    'worklogController',
    'addNewWorkLogController',
    'editWorkLogController',
    'worklogEnteryController',
    'addNewWorkLogEntryController',
    'editWorkLogEntryController',
    'parentAccountController',
    'addParentAccountController',
    'editParentAccountController',
    'parentAccountForVendorController',
    'addNewInitiativeController',
    'editInitiativeController',
    'dropbox-picker',
    'lk-google-picker',
    'angularTrix',
    'trix',
    'contextMenuApp',
    'ngscroll',
    'datatable',
    'ngdatatables',
    'angularTreeview',
    //'JSZip',
    
    'orgchart',

],

    function (app) {
        //bootstrapping app
        app.init();

    }
);