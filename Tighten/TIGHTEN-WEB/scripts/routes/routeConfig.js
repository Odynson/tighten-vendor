﻿/// <reference path="../../views/VendorGeneralProfile/MyProjectViewDetailVendorGeneralProfile.html" />
/// <reference path="../../views/project/QuotedProjectAcceptOrReject.html" />
/// <reference path="../../views/project/QuotedProjectAcceptOrReject.html" />
/// <reference path="../controllers/vendorController.js" />
/// <reference path="../controllers/vendorController.js" />


define(['app'], function (app) {


    app.config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/login");
        $stateProvider
        .state("about", {
            url: "/about",
            templateUrl: 'views/account/about.html',
            controller: 'aboutCtrl',
            title: 'About Us'

        })
        .state("login", {
            url: "/login",
            templateUrl: 'views/account/login.html',
            controller: 'loginController',
            title: 'Login'
        })
        .state("signup", {
            url: "/signup",
            templateUrl: 'views/account/signup.html',
            controller: 'registrationController',
            title: 'Register'
        })
        .state("confirmEmail", {
            url: "/confirmemail/:userId/:emailCode/:redirect",
            templateUrl: 'views/account/confirmEmail.html',
            controller: 'confirmEmailController',
            title: 'Confirm Email'
        })
        .state("setpassword", {
            url: "/setpassword/:userId/:emailCode/:formType",
            templateUrl: 'views/account/setpassword.html',
            controller: 'setPasswordController',
            title: 'Reset Password'
        })
        .state("subscription", {
            url: "/subscription",
            templateUrl: 'views/subscription/subscription.html',
            controller: 'userSubscriptionController'
        })
        .state("SubscriptionRecurring", {
            url: "/SubscriptionRecurring",
            templateUrl: 'views/subscription/SubscriptionRecurring.html',
            controller: 'userSubscriptionRecurringController'
        })

        .state("dashboard/u", {
            url: "/dashboard/u",
            templateUrl: 'views/dashboard/userDashboard.html',
            //view:{
            //    "menu": {
            //        template: "index.viewA"
            //    },
            //    "content": {
            //        template: "index.viewB"
            //    }
            //},
            controller: 'userDashboardController',
            title: 'welcome to tighten'

        })
        .state("dashboard/c", {
            url: "/dashboard/c",
            templateUrl: 'views/dashboard/clientDashboard.html',
            controller: 'clientDashboardController'
        })

        .state("/project", {
            url: "/project",
            templateUrl: 'views/project/project.html',
            controller: 'projectController',
            title: 'Manage Projects'
        })

        .state("/addupdateproject", {
            url: "/addupdateproject/:pid",
            templateUrl: 'views/project/addUpdateProject.html',
            controller: 'addUpdateProjectController',
            title: 'Add/Update Projects'
        })

        .state("/team", {
            url: "/team",
            templateUrl: 'views/team/team.html',
            controller: 'teamController',
            title: 'Manage Teams'
        })
        .state("/team/financial", {
            url: "/team/financial",
            templateUrl: 'views/team/TeamFinancialManager.html',
            controller: 'teamFinancialManagerController',
            title: 'Financial Manager'
        })
        .state("/todo", {
            url: "/todo/:pid/:tid",
            templateUrl: 'views/todo/todo.html',
            controller: 'todoController',
            title: 'Manage Teams'
        })
        .state("/mytodo", {
            url: "/mytodo",
            templateUrl: 'views/todo/mytodo.html',
            controller: 'todoController'
        })
        .state("/todo/manualtime", {
            url: "/todo/manualtime/:pid/:tid",
            templateUrl: 'views/todo/manualTimeTracking.html',
            controller: 'todoManualTrackingController'
        })
        .state("/company/profile", {
            url: "/company/profile/:uid",
            templateUrl: 'views/company/profile.html',
            controller: 'companyController'
        })

        .state("/user/profile", {
            url: "/user/profile/:uid",
            templateUrl: 'views/user/profile.html',
            controller: 'userProfileController'
        })
            //.state("/user/profile", {
        //    url: "/user/profile",
        //    templateUrl: 'views/user/profile.html',
        //    controller: 'userProfileController'
        //})
        .state("/user/manage", {
            url: "/user/manage",
            templateUrl: 'views/user/User.html',
            controller: 'userManageController'
        })
        .state("/notification/", {
            url: "/notification",
            templateUrl: 'views/notification/notification.html',
            controller: 'notificationController'
        })
        .state("/feedback/", {
            url: "/feedback",
            templateUrl: 'views/feedback/feedback.html',
            controller: 'feedbackController'
        })
        .state("/inbox/", {
            url: "/inbox/:userId",
            templateUrl: 'views/inbox/inbox.html',
            controller: 'inboxController'
        })
         .state("calendar", {
             url: "/calendar",
             templateUrl: 'views/calendar/calendar.html',
             controller: 'calendarController'
         })
            // for the approval screen for admin
        .state("/admin/todo/approval", {
            url: "/admin/todo/approval",
            templateUrl: 'views/admin/todoApproval.html',
            controller: 'todoApprovalController'
        })
         // for the approval screen for client
        .state("/todo/approval", {
            url: "/todo/approval",
            templateUrl: 'views/todo/todoApproval.html',
            controller: 'userTodoApprovalController'
        })
          // for the Invoice screen  of admin
        .state("/admin/invoice", {
            url: "/admin/invoice",
            templateUrl: 'views/admin/invoice.html',
            controller: 'invoiceController'
        })

        // for the Invoice screen of client
        .state("/invoice", {
            url: "/invoice",
            templateUrl: 'views/invoice/UserInvoice.html',
            controller: 'userInvoiceController'
        })

         // for the confirm freelancer screen 
        .state("/confirmfreelancer", {
            url: "/confirmfreelancer/:key",
            templateUrl: 'views/Freelancer/ConfirmFreelancer.html',
            controller: 'ConfirmFreelancerController'
        })

        // for Invitation screen of freelancer
        .state("/invitation", {
            url: "/invitation/:pid",
            templateUrl: 'views/Freelancer/invitation.html',
            controller: 'invitationController'
        })

        // for Invitation screen of admin
        .state("/admin/invitation", {
            url: "/admin/invitation",
            templateUrl: 'views/admin/adminInvitation.html',
            controller: 'adminInvitationController'
        })

        // for Organization screen of freelancer
        .state("/organization", {
            url: "/organization",
            templateUrl: 'views/Organization/Organization.html',
            controller: 'organizationController'
        })

         // for Settings screen 
        .state("/settings", {
            url: "/settings",
            templateUrl: 'views/Settings/Settings.html',
            controller: 'settingsController'
        })

        // for Account Settings screen 
        .state("/account/settings", {
            url: "/account/settings",
            templateUrl: 'views/Settings/accountsettings.html',
            controller: 'accountSettingsController'
        })

         // for Payment History screen 
        .state("/payment/history", {
            url: "/payment/history",
            templateUrl: 'views/payment/history.html',
            controller: 'paymentHistoryController'
        })

         // for Role Permission screen 
        .state("/role/permission", {
            url: "/role/permission",
            templateUrl: 'views/role/rolePermission.html',
            controller: 'rolePermissionController'
        })


         // for Role manage screen 
        .state("/role/manage", {
            url: "/role/manage",
            templateUrl: 'views/role/roleManage.html',
            controller: 'roleManageController'
            })

            // for New User Listing screen 
            .state("/role/manageusers", {
                url: "/role/manageusers",
                templateUrl: 'views/vendor/NewUserList.html',
                controller: 'newUserController'
            })

         // for Payment Detail screen 
        .state("/payment/detail", {
            url: "/payment/detail",
            templateUrl: 'views/payment/detail.html',
            controller: 'paymentDetailController'
        })

         // for Project Feedback screen ( to assign )
        .state("/project/feedback/test", {
            url: "/project/feedback/:pid",
            templateUrl: 'views/project/projectFeedback.html',
            controller: 'projectFeedbackController'
        })


        // Project Feedback screen for freelancer( to view )
        .state("/user/feedback", {
            url: "/user/feedback",
            templateUrl: 'views/user/feedback.html',
            controller: 'userFeedbackController'
        })


        //.state("/company/vendor", {
        //    url: "/company/vendor",
        //    templateUrl: 'views/vendor/VendorListing.html',
        //    controller: 'vendorController'
        //})
        .state("quoted/Project", {
            url: "/quoted/project",
            templateUrl: 'views/project/projetListing.html',
            controller: 'projectController'
        })

        .state("add/newproject", {
            url: "/add/newproject",
            templateUrl: "views/project/NewProject.html",
            controller: "vendorProjectController",
            title: "Manage Projects"
        })
            .state("company/invitation", {
                url: "/company/invitation",
                templateUrl: "views/vendor/InvitationbyCompany.html",
                controller: "vendorForVendorProfileController",
            })
        .state('/my/vendor', {
            url: "/my/vendor",
            templateUrl: "views/vendor/MyVendorsList.html",
            controller: "vendorCompanyProfileController",
        })
        .state('/project/toquote', {
            url: "/project/toquote",
            templateUrl: "views/vendor/ProjectQuoteListForVendorProfile.html",
            controller: "projectQuoteListForVendorProfileController"
        })
        .state('/viewDetail/quote', {
            url: "/viewDetail/quote/:pid",
            templateUrl: "views/vendor/ProjectViewDetailByVendor.html",
            controller: "vendorProjectQuoteController"
        })
           .state('/acceptReject/quotation', {
               url: "/acceptReject/quotation/:pid/:vid",
               templateUrl: "views/project/QuotedProjectAcceptOrReject.html",
               controller: "quotedProjectAcceptOrRejectController"
           })
        .state('/edit/project', {
            url: "/edit/project/:pid",
            templateUrl: "views/project/EditNewProject.html",
            controller: "editVendorProjectController"

        }).state('/project/awarded', {
            url: "/project/awarded",
            templateUrl: "views/vendor/ProjectAwardList.html",
            controller: "projectAwardController"

        })
        .state('/projectawarded/viewDetail', {
            url: "/projectawarded/viewDetail/:pid",
            templateUrl: "views/vendor/ProjectAwardedViewDetailList.html",
            controller: "projectAwardedViewDetailController"
        })
        .state('/raise/invoice', {
            url: "/raise/invoice",
            templateUrl: "views/invoice/VendorInvoiceRaise.html",
            controller: "invoiceRaiseController"
        })
        .state('/invoice/listview', {
            url: "/invoice/listview",
            templateUrl: "views/invoice/InvoiceListingForVendorProfile.html",
            controller: "invoiceListForVendorProfileController"

        })
            .state('/vendor/invoice', {
                url: "/vendor/invoice",
                templateUrl: "views/admin/vendorInvoiceForCompanyProfile.html",
                controller: "vendorInvoiceListForCopanyProfileController"
            })
             // for Account by vendor Settings screen 

            .state('/vendoraccount/settings', {
                url: "/vendoraccount/settings",
                templateUrl: 'views/Settings/Vendoraccountsettings.html',
                controller: 'vendorAccountSettingsController'
            })
        .state('/project/summary', {
            url: "/project/summary",
            templateUrl: 'views/project/ProjectAwardedListForCompanyProfile.html',
            controller: 'projectAwardForCompanyProfileController'
        })
        .state('project/summarydetail', {
            url: "/project/summarydetail/:pid/:vid",
            templateUrl: 'views/project/ProjectAwardedForCompanyProfileViewDetailList.html',
            controller: 'projectAwardedViewDetailForCompanyProfileController'
        })

        .state('project/completion/', {

            url: "/project/completion/:pid",
            templateUrl: 'views/project/completeVendorProject.html',
            controller: 'vendorProjectCompletionController'


        })
        .state('vendorcompay/invitation/', {
            url: "/vendorcompany/invitation",
            templateUrl: 'views/company/InvitationVendorCompany.html',
            controller: 'inviteVendorCompanyRegistration'


        }).state('registraion/vendorcompany/', {
            url: "/registration/vendorcompany/:userCode/:ConfirmationCode",
            templateUrl: 'views/account/VendorCompanySignup.html',
            controller: 'vendorCompanyRegistrationController'


        })
       .state('vendoradminprofile/invitation', {
           url: "/vendoradminprofile/invitation",
           templateUrl: 'views/vendorAdminProfile/InvitaionToVendor.html',
           controller: 'vendorAdminProfileInvitationController'


       })
         .state('vendoradminprofile/project/toquote', {
             url: "/vendoradminprofile/project/toquote",
             templateUrl: 'views/vendorAdminProfile/ProjectQuoteListForVendorAdminProfile.html',
             controller: 'projectToQuoteListVendorAdminProfileController'
         })
          .state('vendoradminprofile/viewDetail', {
              url: "/vendoradminprofile/viewDetail/toquote/:pid/:Vid",

              templateUrl: 'views/vendorAdminProfile/ProjectToQuoteViewDetailByVendorAdminProfile.html',
              controller: 'projectToQuoteViewDetailByVendorAdminProfileController'
          })

         .state('vendoradminprofile/project/awarded', {
             url: "/vendoradminprofile/project/awarded",
             templateUrl: 'views/vendorAdminProfile/ProjectAwardListVendorAdminProfile.html',
             controller: 'projectAwardedToVendorAdminProfileController'
         })
            .state('vendoradminprofile/project/awardedviewdetail', {
                url: "/vendoradminprofile/project/awardedviewdetail/:pid/:Vid",
                templateUrl: 'views/vendorAdminProfile/ProjectAwardedViewDetailAdminProfile.html',
                controller: 'projectAwardedViewDetailVendorAdminProfileController'
            })

            .state('vendoradminprofile/project/completion/', {
                url: "/vendoradminprofile/project/completionfeedback/:pid/:vid",
                templateUrl: 'views/vendorAdminProfile/ProjectCompletionFeedBackVendorAdminProfile.html',
                controller: 'projectCompletionFeedBackVendorAdminProfileController'


            })
             .state('vendoradminprofile/Invoices/', {
                 url: "/vendoradminprofile/Invoices",
                 templateUrl: "views/vendorAdminProfile/InvoiceListingVendorAdminProfile.html",
                 controller: "invoiceListForVendorAdminProfileController"


             })
         .state('vendoradminprofile/usermanagement/', {
             url: "/vendoradminprofile/usermanagement",
             templateUrl: "views/vendorAdminProfile/UserManagementVendorAdminProfile.html",
             controller: "userManagementVendorAdminProfileController"
         })
            .state('vendoradminprofile/accountsetting/', {
                url: "/vendoradminprofile/accountsetting",
                templateUrl: "views/vendorAdminProfile/AccountSettingVendorAdminProfile.html",
                controller: "vendorAdminProfileAccountSettingsController"
            })
        .state('vendoradminprofile/rolemanage/', {
            url: "/vendoradminprofile/rolemanage",
            templateUrl: "views/vendorAdminProfile/RoleManagementVendorAdminProfile.html",
            controller: "roleManageVendorAdminProfileController",

        })
         .state('vendorgeneralprofile/project/myprojects', {
             url: "/vendorgeneralprofile/project/myprojects",
             templateUrl: "views/VendorGeneralProfile/MyProjectVendorGeneralProfile.html",
             controller: "myProjectVendorGeneralUserProfile",

         })
       .state('project/customizationcontrol', {
           url: "/project/customizationcontrol",
           templateUrl: "views/Settings/ProjectCustomizationControl.html",
           controller: "ProjectCustomizationControlController",
       })
         .state('vendorgeneralProfile/awardedproject/viewdetail', {
                 url: "/vendorgeneralProfile/awardedproject/viewdetail/:pid",
                 templateUrl: "views/VendorGeneralProfile/MyProjectViewDetailVendorGeneralProfile.html",
                 controller: "myProjectViewDetailVendorGeneralUserProfile",
         })
        .state("OrgChartDemo", {
            url: "/OrgChartDemo/:parentTeamId",
            templateUrl: 'views/company/OrgChartDemo.html',
            controller: 'orgChartDemoController',
            title: 'Manage Teams'

        })
                .state("/team/teamdetail", {
                    url: "/team/teamdetail/:pid",
                    templateUrl: 'views/team/teamdetail.html',
                    controller: 'teamdetailController',
                    title: 'View Team Detail'
                })
         .state('/my/account', {
            url: "/my/account",
            templateUrl: "views/companyAccount/CompanyAccountList.html",
            controller: "vendorCompanyAccountController",
         })
         .state('/add/newaccount', {
             url: "/add/newaccount",
             templateUrl: "views/companyAccount/AddNewAccount.html",
             controller: "addVendorCompanyAccountController",
             title: "Add New Account"
         })
        .state('/account/detail', {
            url: "/account/detail/:accid/:frompage",
            templateUrl: "views/companyAccount/CompanyAccountDetailView.html",
            controller: "CompanyAccountDetailController",
            title: "Company Account Detail View"
        })
        .state('/account/attachement/preview', {
            url: "/account/attachement/preview/:attachid",
            templateUrl: "views/companyAccount/FilePreview.html",
            controller: "CompanyAccountFilePreviewController",
            title: "Company Account Attachement Preview"
        })
        ///My Enterprise CompanyList
        .state('/my/enterprisecompanies', {
            url: "/my/enterprisecompanies",
            templateUrl: "views/vendor/EnterpriseCompanyList.html",
            controller: "GeneralCompaniesOfVendorController",
        })
        .state('/my/enterpriseaccounts', {
            url: "/my/enterpriseaccounts",
            templateUrl: "views/companyAccount/EnterpriseCompanyAccList.html",
            controller: "GenCompanyAccOfVendorController",
        })
        .state("/Worklogs", {
            url: "/Worklogs",
            templateUrl: 'views/workLog/WorkLogListing.html',
            controller: 'worklogController'
        })
        .state("add/newWorklog", {
            url: "/add/newWorklog",
            templateUrl: "views/workLog/addNewWorkLog.html",
            controller: "addNewWorkLogController",
            title: "Add New Worklog"
        })
         .state("edit/Worklog", {
             url: "/edit/Worklog/:id",
             templateUrl: "views/workLog/editWorkLog.html",
             controller: "editWorkLogController",
             title: "Edit Worklog"
         })
         .state("/Worklogentries", {
             url: "/Worklogentries/:id",
            templateUrl: 'views/workLog/WorkLogEnteryListing.html',
            controller: 'worklogEnteryController'
         })
        .state("add/newWorklogEntry", {
            url: "/add/newWorklogEntry/:pid",
            templateUrl: "views/workLog/addNewWorkLogEntry.html",
            controller: "addNewWorkLogEntryController",
            title: "Add New Worklog Entry"
        })
         .state("edit/WorklogEntry", {
             url: "/edit/WorklogEntry/:id",
             templateUrl: "views/workLog/editWorkLogEntry.html",
             controller: "editWorkLogEntryController",
             title: "Edit Worklog"
         })
         .state('my/vendorparentaccount', {
             url: "/my/vendorparentaccount",
             templateUrl: "views/ParentAccount/ParentAccountList.html",
            controller: "parentAccountController",
         })
        .state('add/newparentaccount', {
            url: "/add/newparentaccount",
            templateUrl: "views/ParentAccount/AddNewParentAccount.html",
            controller: "addParentAccountController",
        })
        .state('/parentaccount/detail', {
            url: "/parentaccount/detail/:paccid",
            templateUrl: "views/ParentAccount/EditParentAccount.html",
            controller: "editParentAccountController",
            title: "Parent Account Detail View"
        })
         .state('my/enterpriseparentaccount', {
             url: "/my/enterpriseparentaccount",
             templateUrl: "views/ParentAccount/VendorParentAccontList.html",
             controller: "parentAccountForVendorController",
         })
        .state('/my/initiative', {
            url: "/my/initiative",
            templateUrl: "views/initiative/InitiativeList.html",
            controller: "initiativeController",
        })
        .state("add/newInitiative", {
            url: "/add/newinitiative",
            templateUrl: "views/initiative/addNewInitiative.html",
            controller: "addNewInitiativeController",
            title: "Add New Initiative"
        })
        .state("edit/Initiative", {
            url: "/edit/initiative/:pid",
            templateUrl: "views/initiative/editInitiative.html",
            controller: "editInitiativeController",
            title: "Edit Initiative"
        })

    });

});
