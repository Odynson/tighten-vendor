﻿define(['app'], function (app) {
    app.controller("clientDashboardController", function ($scope, $rootScope,apiURL, $timeout, $http, $location, sharedService, toaster, clientDashboardFactory, userDashboardFactory) {
        $scope.imgURL = apiURL.imageAddress;
        angular.element(document).ready(function () {
            // Calling the function

            $scope.$emit('showMenu', []);

            showClientDashboard();

        });

        /* Showing Client Dashboard*/
        $scope.showClientDashboard = function () {
            showClientDashboard();
        };
        function showClientDashboard() {
            //closeCentreSections();
            $scope.activeClientDashboardPanel = "Todo";
            $scope.selectedNavigation = 'ClientDashboard';
            $scope.showUserTLine = false;
            //$('#divClientDashboard').show();
            $scope.hideClientDashboardTodoDescriptionPanel = true;
            $scope.hideClientDashboardProjectDescriptionPanel = true;
            $scope.stretchClientDashboardTodoPanel = true;

            /*Getting Client Upcoming Todos*/
            clientDashboardFactory.getClientUpcomingTodos(sharedService.getUserId())
                .success(function (data) {
                    if (data.success) {
                        $scope.clientUpcomingTodosObject = data.ResponseData;
                        /* Getting Client Projects */
                        clientDashboardFactory.getClientProjects(sharedService.getUserId())
                            .success(function (data) {
                                if (data.success) {
                                    $scope.clientProjectsObject = data.ResponseData;
                                    /* Getting Client Concerned Users */
                                    clientDashboardFactory.getClientConcernedUsers(sharedService.getUserId())
                                        .success(function (data) {
                                            if (data.success) {
                                                $scope.clientConcernedUsersObject = data.ResponseData;
                                                viewUserTLine(sharedService.getUserId());
                                            }
                                            else {
                                                toaster.error("Error", data.message);
                                            }
                                        })
                                        .error(function () {
                                            toaster.error("Error", "Some Error Occured !");
                                        });
                                }
                                else {
                                    toaster.error("Error", data.message);
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured !");
                            });
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });



        }

        /* Viewing User T Line */
        $scope.viewUserTLine = function (userId) {
            viewUserTLine(sharedService.getUserId())
        };
        function viewUserTLine(userId) {

            /* Getting User T Line */
            userDashboardFactory.getUserTLine(userId)
                .success(function (data) {
                    if (data.success) {
                        $scope.userTLineObject = data.ResponseData;
                        $scope.showUserTLine = true;
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });
        }


        /* Hiding User T Line */
        $scope.hideUserTLine = function () {
            $scope.showUserTLine = false;
        }

        /* Showing Todo Description on Client Dashboard */
        $scope.showTodoDescription = function (todoObject) {
            $scope.stretchClientDashboardTodoPanel = false;
            $scope.todoDescriptionObject = todoObject;
            $scope.hideClientDashboardTodoDescriptionPanel = false;
        };

        /* Showing Project Description on Client Dashboard */
        $scope.showProjectDescription = function (projectObject) {
            $scope.stretchClientDashboardTodoPanel = false;
            $scope.projectDescriptionObject = projectObject;
            $scope.hideClientDashboardProjectDescriptionPanel = false;
        };

        /* Showing Todo Panel */
        $scope.showTodoPanel = function () {
            $scope.activeClientDashboardPanel = "Todo";
            $scope.hideClientDashboardTodoDescriptionPanel = true;
            $scope.hideClientDashboardProjectDescriptionPanel = true;
            $scope.stretchClientDashboardTodoPanel = true;
        };

        /* Showing Todo Panel */
        $scope.showProjectPanel = function () {
            $scope.activeClientDashboardPanel = "Project";
            $scope.hideClientDashboardTodoDescriptionPanel = true;
            $scope.hideClientDashboardProjectDescriptionPanel = true;
            $scope.stretchClientDashboardTodoPanel = true;
        };
        $scope.showClientFeedbackModal = function (IsTodoFeedback, projectId, todoId) {
            $scope.clientFeedbackAddUpdateModel = {};
            $scope.clientFeedbackAddUpdateModel.rating = 2;
            if (IsTodoFeedback) {
                $scope.clientFeedbackAddUpdateModel.isTodoFeedback = true;
                $scope.clientFeedbackAddUpdateModel.todoId = todoId;
            }
            else {
                $scope.clientFeedbackAddUpdateModel.isTodoFeedback = false;
                $scope.clientFeedbackAddUpdateModel.projectId = projectId;
            }
            $('#modalClientFeedback').modal('show');
        };
        $scope.myDataSource = {
            chart: {
                caption: "Deep Dhindsa",
                subCaption: "Last Five Months Growth",
                numberPrefix: "$",
                theme: "ocean"
            },
            data: [{
                label: "Nathan's Project",
                value: "2423423"
            },
            {
                label: "Garden Groove harbour",
                value: "730000"
            },
            {
                label: "Los Angeles Topanga",
                value: "590000"
            },
            {
                label: "Compton-Rancho Dom",
                value: "520000"
            },
            {
                label: "Daly City Serramonte",
                value: "330000"
            }]
        };
    });
});