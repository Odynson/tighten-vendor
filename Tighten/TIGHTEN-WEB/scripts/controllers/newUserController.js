﻿
define(["app"], function (app) {

    app.controller("newUserController", function ($scope, $rootScope, apiURL, $location, vendorFactory, sharedService, toaster, $http, $sce, teamFactory, $timeout, userManagementFactory) {
        var baseurl = apiURL.baseAddress;
        $scope.imgURL = apiURL.imageAddress;
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();


        $scope.myVendorList = [];  // my vendor for my vendor page
        $scope.companyVendorRespList = [];
        $scope.searchText = "";
        var dataModal = { CompanyId: CompanyId, Id: 0, IsApproved: null, VendorCompanyId: 0, VendorId: 0, Name: "", PageSize: 5, TotalPageCount: 0 }


        /*  NEW LISTING CODE STARTS*/
        $scope.vendorAdminProfileExistingInternalUsersLists = [];
        $scope.ExistingUserModel = { ExistingUserIndex: 0, VendorName: "", Rating: 0, Email: "", IsRated: true };//, Indefinitely: true, ValidUntil: false, ValidUntilDate: ""
        $scope.ExistingInternalUsersModel = {};
        $scope.jobProfileModal = {};
        $scope.Tags = {};
        $scope.Tags.Roles = [];
        $scope.openSaveRoleTags = false;
        var multiSelectPreviousData = [];
        var multiSelectCurrentData = [];

        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCountInternalUsers = 0;
        $scope.pageIndexInternalUsers = 1;
        $scope.pageSizeSelectedInternalUsers = 5;
        $scope.pagesizeListInternalUsers = [
            { PageSizeInternalUsers: 5 },
            { PageSizeInternalUsers: 10 },
            { PageSizeInternalUsers: 25 },
            { PageSizeInternalUsers: 50 },

        ];
        $scope.PageSizeInternalUsers = $scope.pagesizeListInternalUsers[0];
        $scope.totalPageCountInternalUsers = 0;
        $scope.changePageSize = function (pageSizeSelected) {
            $scope.pageSizeSelectedInternalUsers = pageSizeSelected.PageSizeInternalUsers;
            getAllExistingInternalUsers(CompanyId);
        }

        $scope.pageChanged = function (pageIndex) {
            $scope.pageIndexInternalUsers = pageIndex;
            getAllExistingInternalUsers(CompanyId);
        }

        /*Get all internal users invited from invite new users option*/
        function getAllExistingInternalUsers(VendorCompanyId) {
            $('#globalLoader').show();
            userManagementFactory.getAllExistingInternalUsers(VendorCompanyId, $scope.pageIndexInternalUsers, $scope.pageSizeSelectedInternalUsers)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    if (data.success && data.ResponseData != null) {
                        $scope.vendorAdminProfileExistingInternalUsersLists = data.ResponseData;
                        $scope.totalCountInternalUsers = data.ResponseData[0].TotalPageCount;

                    }
                    else {
                        toaster.error("Error", message.error);
                    }
                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        /*active inactive internal users  */
        $scope.activeInactiveUserVendorAdminProfile = function (item) {
            debugger
            $('#globalLoader').show();
            userManagementFactory.activeInactiveInternalUsersVendorAdminProfile(item)
                .success(function (data) {
                    getAllExistingInternalUsers(CompanyId);
                    $('#globalLoader').hide();
                }),
                Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })

        }

        /*  This function is used to edit Role Tags  */
        $scope.editRoleTags = function () {
            debugger;
            multiSelectCurrentData = $scope.Tags.Roles;
            for (var i = 0; i < multiSelectCurrentData.length; i++) {
                multiSelectPreviousData.push(multiSelectCurrentData[i]);
            }

            $scope.openSaveRoleTags = true;
            $timeout(function () {
                $('#RoleTags  .host .tags .input').focus();
            }, 100);
        }

        /*Get all roles of a signed in company i.e where type in (1,3) means its role or both */
        function getInternalUserRoleVendorAdminProfile(VendorCompanyId) {
            debugger;
            vendorFactory.GetNewUserRole(VendorCompanyId)
                .success(function (data) {
                    $scope.getUserRoleList = data.ResponseData;
                    $("#ExistingInternalUserVendorAdminProfileModal").modal('show')

                })
        }

        /*Get All Designation or Job Profiles  i.e where type in (2,3) means its job profile or both*/
        //function getAllDesignation() {
        //    $('#globalLoader').show();
        //    teamFactory.getAllDesignation()
        //        .success(function (data) {
        //            $scope.getAllDesignationList = data.ResponseData;
        //            $('#globalLoader').hide();

        //        })
        //}

        /* Open modal pop up for edit */
        $scope.openExistingUserDetailVendorAdminProfileModal = function (index, item) {
            debugger
            $scope.ExistingInternalUsersModel.InternalUserIndex - index;
            $scope.ExistingInternalUsersModel.Name = item.name;
            $scope.ExistingInternalUsersModel.Rating = item.Rating;
            $scope.ExistingInternalUsersModel.Email = item.Email;
            $scope.ExistingInternalUsersModel.ProfilePhoto = item.ProfilePhoto;
            $scope.ExistingInternalUsersModel.Id = item.Id;
            $scope.ExistingInternalUsersModel.Password = item.Password;
            $scope.ExistingInternalUsersModel.FirstName = item.FirstName;
            $scope.ExistingInternalUsersModel.LastName = item.LastName;
            $scope.ExistingInternalUsersModel.IsActive = item.IsActive;
            $scope.jobProfileModal.Name = item.JobProfileName;
            $scope.jobProfileModal.RoleId = item.JobProfileId;
            $scope.Tags.Roles = item.UserRoleList;
            $scope.editRoleTags();
            $scope.ExistingInternalUsersModel.JobProfileId = item.JobProfileId;
            $scope.ExistingInternalUsersModel.ValidUntilDate = item.ValidUntilDate;
            $scope.ExistingInternalUsersModel.Indefinitely = item.Indefinitely;
            $scope.ExistingInternalUsersModel.ValidUntil = item.ValidUntil;
            $scope.ExistingInternalUsersModel.UserId = item.UserId;
            $scope.ExistingInternalUsersModel.CompanyId = item.CompanyId;

            getInternalUserRoleVendorAdminProfile(CompanyId);
            getAllDesignation();

        }


        /*Set values for job profile on selection from drop down*/
        $scope.setJobProfileModal = function (jobProfileModal) {
            $scope.jobProfileModal.RoleId = jobProfileModal.RoleId;
            $scope.jobProfileModal.Name = jobProfileModal.Name;
        }

        /*Code to update internal user info*/
        $scope.UpdateExistingInternalUsers = function (isFirstName, IsLastName, IsEmail, InternalUserForm, ExistingInternalUsersModel) {

            debugger;
            if (isFirstName || IsLastName || IsEmail) {

                NewUserForm.FirstName.$dirty = true
                NewUserForm.LastName.$dirty = true
                NewUserForm.EmailAddress.$dirty = true
                return;
            }

            $scope.ExistingInternalUsersModel.JobProfileId = $scope.jobProfileModal.RoleId;
            $scope.ExistingInternalUsersModel.JobProfileName = $scope.jobProfileModal.Name;
            $scope.ExistingInternalUsersModel.UserRoleList = $scope.Tags.Roles;

            console.log($scope.ExistingInternalUsersModel);
            $('#globalLoader').show();
            vendorFactory.updateExistingInternalUsers($scope.ExistingInternalUsersModel)
                .success(function (data) {
                    $('#globalLoader').hide();
                    if (data.ResponseData != null) {
                        if (data.ResponseData.Id == -1) {

                            toaster.warning("Warning", message.InvitationError)
                            InternalUserForm.FirstName.$dirty = false;
                            InternalUserForm.LastName = false;
                        }
                        else {
                            toaster.success("Success", message.InvitationToUser);
                        }
                    }
                    else {
                        toaster.error("Error", message.InvitationError);
                    }
                    $scope.ExistingInternalUsersModel = {}
                    $scope.jobProfileModal = {};
                    $scope.userdata = {};
                    $('#ExistingInternalUserVendorAdminProfileModal').modal('hide');
                    getAllExistingInternalUsers(CompanyId);
                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })



        }

        $scope.inputType = 'password';
        /*Change type of password textbox*/
        $scope.changeType = function () {
            if ($scope.inputType == 'password') {
                $scope.inputType = 'text';
            }
            else {
                $scope.inputType = 'password';
            }
        }

        /*Date Functions Starts Here*/
        
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        var da = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
            console.log($scope.minDate);
        };

        $scope.toggleMin();

        $scope.maxDate = new Date(da.getFullYear(), 5, 22);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,

        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup2 = {
            opened: false
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        /*Date Functions Ends Here*/

        $scope.radioIndefinatly = function (radioType, ExistingUserModel) {
            if (radioType == 'Indefinitely') {
                ExistingUserModel.Indefinitely = true;
                ExistingUserModel.ValidUntil = false;
            }
            else {
                ExistingUserModel.Indefinitely = false;
                ExistingUserModel.ValidUntil = true;
            }
        }

        $scope.myVendorstatusList = [
            { IsApproved: null, status: '---Select All---', Id: 0 },
            { IsApproved: true, status: 'Accepted', Id: 1 },
            { IsApproved: false, status: 'Waiting for approval', Id: 2 },
        ]

        $scope.myvendorstatusModal = $scope.myVendorstatusList[0]

        /* Filter new user invited */
        $scope.FilterVendorByStatus = function (item) {
            debugger;

            dataModal.Id = item.Id;
            dataModal.IsApproved = item.IsApproved;
            getMyVendor();

        }

        /* NEW LISTING CODE ENDS HERE*/
     
        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');

            }
            $scope.email_address = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
            // var RoleId = sharedService.getRoleId()
            // getCompanyVendorList(CompanyId)
            //  dataModal.CompanyId = CompanyId;

            //   getCompanyVendorDrop(CompanyId);
            getAllExistingInternalUsers(CompanyId);


        })





        ////---------------------------getCompanyVendorList---------------------------------------------///////////

        function getCompanyVendorList(CompanyId) {

            vendorFactory.getCompanyVendorList(CompanyId).success(function (data) {
                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.companyVendorRespList = data.ResponseData;
                    }
                }
                else {
                    toaster.error("Error", data.message);
                }

            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }

        ///------------------------getInvitaionToVendorsList--------------------------------------------------------

        function getInvitaionToVendors(CompanyId, UserId) {
            debugger;
            vendorFactory.getInvitaionToVendors(CompanyId, UserId).success(function (data) {
                debugger;
                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.CompanyInvitaionRespList = data.ResponseData;
                    }
                }


            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }

        var IdForRejectUPdate;
        $scope.ReasonReject = "";
        var tempdata = {};

        $scope.modalAddNewvendors = function () {
            debugger;
            $('#modalAddNewvendors').modal('show');
        }
        function getProjectAndVendorDetail(ProjectId) {

            vendorFactory.getProjectAndVendorDetail(ProjectId, CompanyId)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.ProjectAndVendorDetailData = data.ResponseData;
                    }
                    $('#globalLoader').hide();

                }), Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })

        }
        ///////////////////////////////////////GetProjectToQuote for vendor Profile p ////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////Filtering////////////////////////////////////////////////////////////////////////////////////////////
        $scope.vendorDropModal = { CompanyId: 0, VendorId: 0, Id: 0, VendorCompanyId: 0, Name: "--Select All--" }

        function getCompanyVendorDrop(CompanyId) {
            debugger;
            $('#modalConfirmationCompleteProject').modal('show')
            vendorFactory.getCompanyVendorDropList(CompanyId)
                .success(function (data) {
                    if (data.success) {
                        debugger;
                        $scope.myVendorDropList = data.ResponseData;
                        $scope.myVendorDropList.push($scope.vendorDropModal);
                        getMyVendor();
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                , Error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                    $('#modalConfirmationCompleteProject').modal('hide');
                })

        }


        $scope.vendorInvitationModal = {

            firstName: "",
            lastName: "",
            companyName: "",
            emaiAddress: "",
            messageTextBox: "",

        }
        $scope.inviteVendor = function (isVendorFirstName, IsVendorCompanyNameInvalid, IsVendorEmailId, NewVendorForm, vendorInvitationModal) {

            debugger;
            if (isVendorFirstName || IsVendorCompanyNameInvalid || IsVendorEmailId) {

                NewVendorForm.VendorFirstName.$dirty = true
                NewVendorForm.companyName.$dirty = true
                NewVendorForm.VendorEmaiAddress.$dirty = true
                return;

            }

            vendorInvitationModal.companyName = sharedService.getShared().CompanyName;

            var data = {}
            data.FirstName = vendorInvitationModal.firstName
            data.LastName = vendorInvitationModal.lastName
            data.CompanyName = vendorInvitationModal.companyName
            data.EmaiAddress = vendorInvitationModal.emaiAddress
            data.MessageTextBox = vendorInvitationModal.messageTextBox
            data.UserId = sharedService.getUserId()
            data.CompanyId = sharedService.getCompanyId();

            $('#globalLoader').show();
            vendorFactory.inviteVendor(data)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    debugger;
                    if (data.ResponseData != null) {
                        if (data.ResponseData.Id == -1) {

                            toaster.warning("Warning", message.InvitationError)
                            NewVendorForm.VendorFirstName.$dirty = false;
                            NewVendorForm.companyName.$dirty = false;
                            NewVendorForm.VendorEmaiAddress.$dirty = false;
                            NewVendorForm.VendorEmaiAddress = false;
                            NewVendorForm.VendorMessageTextBox = false;
                            NewVendorForm.VendorLastName = false;
                        }
                        else {
                            toaster.success("Success", message.InvitationToVendor);
                        }
                    }
                    else {
                        toaster.error("Error", "Some Error Occured !");
                    }
                    $scope.vendorInvitationModal = {}
                    $('#modalAddNewvendors').modal('hide');
                    getMyVendor()//after Inviting show vendorlist
                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })



        }


        function getMyVendor() {
            debugger

            dataModal.CompanyId = CompanyId;
            $('#globalLoader').show();
            vendorFactory.getMyVendor(dataModal)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.myVendorList = data.ResponseData;
                    }
                    $('#globalLoader').hide();

                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })

        }


        $scope.FilterByVendorCompany = function (item) {
            dataModal.VendorCompanyId = item.VendorCompanyId;
            getMyVendor();

        }

        //$scope.myVendorstatusList = [
        //    { IsApproved: null, status: '---Select All---', Id: 0 },
        //    { IsApproved: true, status: 'Accepted', Id: 1 },
        //    { IsApproved: false, status: 'Rejected', Id: 2 },
        //    { IsApproved: null, status: 'Waiting for approval', Id: 3 },

        //]

        //$scope.myvendorstatusModal = $scope.myVendorstatusList[0]

        $scope.FilterVendorByStatus = function (item) {
            debugger;

            dataModal.Id = item.Id;
            dataModal.IsApproved = item.IsApproved;
            getMyVendor();

        }


        //////////////////////////////////////////--autoCompleteOptions---////////////////////////////////////////////////////
        var autoModal = {}
        $scope.autoCompleteOptions = {

            minimumChars: 3,
            dropdownWidth: '500px',
            dropdownHeight: '200px',
            data: function (term) {
                return $http.post(baseurl + 'VendorApi/GetVendorName/',
                    autoModal = { CompanyId: CompanyId, Name: term })
                    .then(function (data) {
                        debugger;
                        return data.data.ResponseData;
                    });
            },
            renderItem: function (item) {
                return {
                    value: item.Name,
                    label: $sce.trustAsHtml(
                        "<p class='auto-complete'>"
                        + item.Name +
                        "</p>")

                };

            },
            itemSelected: function (item) {
                debugger;
                dataModal.VendorId = item.item.VendorId;
                $scope.searchVendorName = item.item.Name;
                getMyVendor();
            }
        }


        $scope.vendorworkList = [];
        $scope.openVendorViewDetail = function (VendorId) {
            debugger;
            if (VendorId == 0) {

                $scope.vendorworkList = [];


                $('#modalVendorViewDetail').modal('show');
            }
            else {

                getVendorDetail(VendorId);
            }
        }

        function getVendorDetail(VendorId) {
            $('#globalLoader').show();

            vendorFactory.getVendorDetail(CompanyId, VendorId)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.vendorworkList = data.ResponseData;
                        $('#modalVendorViewDetail').modal('show');
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();
                    }

                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })
        }

        var isCtrlPressed;
        var isAPressed;
        /////////////////////////////empty text box project to quote companyProfile//////////////////////////////////////////////////////
        $scope.searchVendorNameInvitaion = function (searchVendorName, event) {
            debugger;
            if (searchVendorName != undefined && searchVendorName.length <= 1 && event.keyCode == 8) {
                dataModal.VendorId = 0
                dataModal.Name = "";
                getMyVendor();
            }
            // if key is pressed
            if (event.keyCode == 17 || isCtrlPressed) {
                isCtrlPressed = true;
            }
            else {
                isCtrlPressed = false;
            }

            // other key after the control
            if (isCtrlPressed) {
                if (event.keyCode == 97 || event.keyCode == 65) {
                    isAPressed = true;

                }
            }
            else {
                isAPressed = false;
            }


            if (isCtrlPressed && isAPressed) {
                if (event.keyCode == 46 || event.keyCode == 8) {
                    dataModal.VendorId = 0
                    dataModal.Name = "";
                    getMyVendor();
                    isCtrlPressed = false;
                    isAPressed = false;
                }

            }
            var text = window.getSelection().toString()
            if (text != "" && (event.keyCode == 8 || event.keyCode == 46)) {
                dataModal.VendorId = 0
                dataModal.Name = "";
                getMyVendor();
            }
        }


        $scope.cutSearchInvitaionByVendorName = function () {
            dataModal.VendorId = 0
            dataModal.Name = "";
            getMyVendor();

        }

        ////////////////////////////////////------------paging code/////////////////////////////////
        $scope.pagesizeList = [
            { PageSize: 5 },
            { PageSize: 10 },
            { PageSize: 25 },
            { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];

        //$scope.changePageSize = function (pageSizeSelected) {
        //    debugger;
        //    dataModal.PageSize = pageSizeSelected;
        //    getInvitationTovendor(dataModal)
        //}

        //$scope.pageChanged = function (pageIndex) {
        //    debugger;
        //    dataModal.PageIndex = pageIndex;
        //    getInvitationTovendor(dataModal)
        //}


        ////////////////////////////////////------------paging code/////////////////////////////////


        /*New functionality to invite users*/

        $scope.userdata = {};
        $scope.jobProfileModal = {};
        $scope.Tags = {};
        $scope.Tags.Roles = [];

        /*Open pop up to add new user and send invitation*/
        $scope.modalAddNewUsers = function () {
            $scope.companyDomain = "";
            $scope.userInvitationModal = {}
            $scope.userdata = {};
            $scope.Tags = {};
            $scope.Tags.Roles = [];
            getusersRole();
            getAllDesignation();
            $scope.GetCompanyEmailForInviteNewUser(sharedService.getShared().CompanyName);
            $("input[name='UserFirstName']").removeClass("inputError");
            $("input[name='UserLastName']").removeClass("inputError");
            $("input[name='UserEmailAddress']").removeClass("inputError");
            $("textarea[name='UserMessageTextBox']").removeClass("inputError");
            //$("select[name='jobProfile']").removeClass("inputError");
            //$("input[name='txtRole']").removeClass('inputError');

            multiSelectPreviousData = [];
            multiSelectCurrentData = [];

            $('#modalAddNewUsers').modal('show');
        }

        /*Code to get domain of company i.e. string after @ in companyemail by company name*/
        $scope.companyDomain = "";
        $scope.GetCompanyEmailForInviteNewUser = function (CompanyName) {
            $('#globalLoader').show();
            vendorFactory.GetCompanyEmailForInviteNewUser(CompanyName)
                .success(function (data) {
                    if (data.success && data.ResponseData != null) {
                        $scope.companyDomain = '@' + data.ResponseData.split('@')[1];
                    }
                    else {
                        $scope.companyDomain = '';

                    }
                    $('#globalLoader').hide();

                })
                ,
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })

        };

        /*Get all roles of a signed in company i.e where type in (1,3) means its role or both */
        function getusersRole() {
            vendorFactory.GetNewUserRole(sharedService.getCompanyId())
                .success(function (data) {
                    $scope.getUserRoleList = data.ResponseData;
                })
        }

        /*Code to add user,send invitation and save info*/
        $scope.inviteUser = function (isUserFirstName, IsUserLastNameInvalid, IsUserEmailId,IsUserEmailPattern, NewUserForm, userInvitationModal) {
           // alert(IsUserEmailPattern);
            debugger;
            if (isUserFirstName || IsUserLastNameInvalid || IsUserEmailId || IsUserEmailPattern) {

                NewUserForm.UserFirstName.$dirty = true
                NewUserForm.UserLastName.$dirty = true
                NewUserForm.UserEmailAddress.$dirty = true
                NewUserForm.RoleTags.$dirty = true

                return;

            }
            $scope.userdata.FirstName = userInvitationModal.firstName;
            $scope.userdata.LastName = userInvitationModal.lastName;
            $scope.userdata.CompanyName = sharedService.getShared().CompanyName;
            $scope.userdata.Email = userInvitationModal.emailAddress; //+ $scope.companyDomain;
            $scope.userdata.MessageTextBox = userInvitationModal.messageTextBox;
            $scope.userdata.UserId = sharedService.getUserId();
            $scope.userdata.CompanyId = sharedService.getCompanyId();
            $scope.userdata.UserRoleList = $scope.Tags.Roles;
            $scope.userdata.JobProfileId = $scope.jobProfileModal.RoleId;
            $scope.userdata.JobProfileName = $scope.jobProfileModal.Name;
            //$("select[name='jobProfile']").removeClass("inputError");

            $('#globalLoader').show();
            vendorFactory.inviteUser($scope.userdata)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    debugger;
                    if (data.ResponseData != null) {
                        if (data.ResponseData.Id == -1) {

                            toaster.warning("Warning", message.InvitationError)
                            NewUserForm.UserFirstName.$dirty = false;

                            NewUserForm.UserEmailAddress.$dirty = false;
                            NewUserForm.UserEmailAddress = false;
                            NewUserForm.UserMessageTextBox = false;
                            NewUserForm.UserLastName = false;
                            NewUserForm.companyName.$dirty = false;
                        }
                        else {
                            toaster.success("Success", message.InvitationToUser);
                        }
                    }
                    else {
                        toaster.error("Error", "Some Error Occured !");
                    }
                    $scope.userInvitationModal = {}
                    $scope.roleModal = {};
                    $scope.userdata = {};
                    $('#modalAddNewUsers').modal('hide');
                    // getMyVendor()    //after Inviting show vendorlist using old method to bind vendorlist no changes
                    getAllExistingInternalUsers(CompanyId);

                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })



        }

        /*Code to not allow user to  enter @ in Email address as it will be by default set to companies domain*/
        $scope.emailkeyUp = function (email) {
            //alert(email);
            debugger;
            //if (email.indexOf(' ') >= 0) {
            //    toaster.error("Space not allowed");
            //    //$scope.userInvitationModal.emailAddress = '';
            //}
            //if (email.indexOf("@") > -1) {
            //    toaster.error("Enter username only");
            //   // $scope.userInvitationModal.emailAddress = '';
            //}
            //if (email == '') {
            //    $('#UserEmailAddress').addClass('inputError');
            //}
            //else {
            //    $('#UserEmailAddress').removeClass('inputError');
            //}
        };

        /*Get All Designation or Job Profiles  i.e where type in (2,3) means its job profile or both*/
        function getAllDesignation() {
            $('#globalLoader').show();
            teamFactory.getAllDesignation()
                .success(function (data) {
                    $scope.getAllDesignationList = data.ResponseData;
                    $('#globalLoader').hide();

                })
        }

        //$scope.openSaveRoleTags = false;
        //var multiSelectPreviousData = [];
        //var multiSelectCurrentData = [];

        //var checkTagrol = false;
        /*  This function is used to edit Role Tags  */
        //$scope.editRoleTags = function () {
        //    debugger;
        //    multiSelectCurrentData = $scope.Tags.Roles;
        //    for (var i = 0; i < multiSelectCurrentData.length; i++) {
        //        multiSelectPreviousData.push(multiSelectCurrentData[i]);
        //        //checkTagrol = true;

        //    }

        //    $scope.openSaveRoleTags = true;
        //    $timeout(function () {
        //        $('#RoleTags  .host .tags .input').focus();
        //    }, 100);


        //}





    })


});

