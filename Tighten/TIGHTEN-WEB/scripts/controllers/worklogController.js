﻿
define(['app'], function (app) {
    app.controller("worklogController", function ($scope, $rootScope, $stateParams, apiURL, $timeout, $http, $location, sharedService, $confirm, toaster, worklogFactory) {
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();
        $scope.imgURL = apiURL.imageAddress;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }
        $scope.worklogList = [];
     
        $scope.getWorkLogList = function (UserId) {
            $('#globalLoader').show();
            //alert(UserId);
            worklogFactory.getWorkLogList(UserId)
            .success(function (data) {

                debugger;
                if (data.ResponseData != null) {
                    $scope.worklogList = data.ResponseData;
                    for (var i = 0; i < $scope.worklogList.length; i++) {
                        if ($scope.worklogList[i] != undefined && $scope.worklogList[i] != null && $scope.worklogList[i].CreatedOn != undefined && $scope.worklogList[i].CreatedOn != null) {
                            //$scope.AddNewWorkLogModel.CreatedDate = moment($scope.AddNewWorkLogModel.CreatedDate, "MM/DD/YYYY").toDate();
                            var datedet = $scope.worklogList[i].CreatedOn.split('/');
                            if (datedet.length == 3) {
                                var newdate = datedet[1] + "/" + datedet[0] + "/" + datedet[2];
                                $scope.worklogList[i].CreatedOn = newdate;
                            }
                        }
                    }
                    
                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }

        angular.element(document).ready(function () {
            debugger;
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
                $rootScope.activeTeam = [];
            }
            $scope.getWorkLogList(UserId);
        });

        $scope.open1 = function () {
            $scope.popup.opened1 = true;
            $scope.mindate = new Date();
        };





    });
});