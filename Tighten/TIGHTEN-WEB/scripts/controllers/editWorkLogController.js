﻿
define(['app'], function (app) {
    app.controller("editWorkLogController", function ($scope, $rootScope, $stateParams, apiURL, $timeout, $http, $location, sharedService, $confirm, toaster, worklogFactory, projectsFactory, $window) {
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();
        $scope.imgURL = apiURL.imageAddress;
        var WorkLogId = $stateParams.id;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }
        $scope.billableDropList = [
            { text: "Billable", value: "B" },
            { text: "Not-Billable", value: "NB" }
        ]
        $scope.worklogList = [];
        $scope.approvedProjectList = [];
        $scope.phaseMilestoneList = [];
        $scope.selectProjectObj = {};
        $scope.selectMilestoneObj = {};
        $scope.AddNewWorkLogModel = {};
        $scope.selectedBillableObj = {};
        $scope.popup = {
            opened1: false
        };
        $scope.selectedProjectChanged = function (ProjectObj) {
            debugger;
            //alert(ProjectObj.ProjectId);
            $scope.AddNewWorkLogModel.ProjectId = ProjectObj.ProjectId;
            $('#globalLoader').show();
            projectsFactory.getProjectMilestonesByProjectId(ProjectObj.ProjectId)
            .success(function (data) {
                $scope.phaseMilestoneList = [];

                debugger;
                if (data.ResponseData != null) {

                    $scope.phaseMilestoneList = data.ResponseData;

                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }

        $scope.selectedMilestoneChanged = function (MilestoneObj) {
            debugger;
            $('#globalLoader').show();
            //alert(ProjectObj.ProjectId);
            $scope.AddNewWorkLogModel.PhaseId = MilestoneObj.PhaseId;
            $scope.AddNewWorkLogModel.PhaseMilestoneId = MilestoneObj.MilestoneId;
            $('#globalLoader').hide();

        }
        $scope.selectedBillableChanged = function (BillableObj) {
            debugger;
            $('#globalLoader').show();
            //alert(ProjectObj.ProjectId);
            $scope.AddNewWorkLogModel.BillableStatus = BillableObj.text;
            $('#globalLoader').hide();

        }
        $scope.getWorkLogById = function (Id) {
            $('#globalLoader').show();
            //alert(Id);
            worklogFactory.getWorkLogById(Id,UserId)
            .success(function (data) {

                debugger;
                if (data.ResponseData != null) {
                    $scope.format = 'MM/dd/yyyy';
                    $scope.AddNewWorkLogModel = data.ResponseData;
                    $scope.approvedProjectList = $scope.AddNewWorkLogModel.WorkLogVendorList;
                    $scope.phaseMilestoneList = $scope.AddNewWorkLogModel.ProjectMilestones;
                    var CreateDate = $scope.AddNewWorkLogModel.CreatedOn.split('/');
                    if (CreateDate.length == 3) {
                        
                       
                        $scope.AddNewWorkLogModel.CreatedOn = CreateDate[1] + "/" + CreateDate[0] + "/" + CreateDate[2]
                    }
                    //alert(moment($scope.AddNewWorkLogModel.CreatedOn).format("MM/DD/YYYY"));
                    //$scope.AddNewWorkLogModel.CreatedOn = moment($scope.AddNewWorkLogModel.CreatedOn).format("MM/DD/YYYY");
                    for (var i = 0; i < $scope.approvedProjectList.length; i++) {
                        if ($scope.approvedProjectList[i].ProjectId == $scope.AddNewWorkLogModel.ProjectId) {

                            $scope.selectProjectObj = $scope.approvedProjectList[i];
                            break;
                        }
                    }
                    for (var i = 0; i < $scope.phaseMilestoneList.length; i++) {
                        //alert($scope.phaseMilestoneList[i].MilestoneId + " " + $scope.AddNewWorkLogModel.PhaseMilestoneId + " " + UserId);
                        if ($scope.phaseMilestoneList[i].MilestoneId == $scope.AddNewWorkLogModel.PhaseMilestoneId) {

                            $scope.selectMilestoneObj = $scope.phaseMilestoneList[i];
                            break;
                        }
                    }

                    for (var i = 0; i < $scope.billableDropList.length; i++) {
                        if ($scope.billableDropList[i].text == $scope.AddNewWorkLogModel.BillableStatus) {
                           
                            $scope.selectedBillableObj = $scope.billableDropList[i];
                            break;
                        }
                    }
                        
                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }

        $scope.getApprovedProjectByUserId = function (UserId) {
            $('#globalLoader').show();
            projectsFactory.getApprovedProjectByUserId(UserId)
            .success(function (data) {

                debugger;
                if (data.ResponseData != null) {
                    $scope.approvedProjectList = data.ResponseData;

                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }

        $scope.editWorkLog = function (IsWorkLogTitle, IsCreatedOn, NewWorkLogForm, AddNewWorkLogModel) {
            if (IsWorkLogTitle || IsCreatedOn || AddNewWorkLogModel.BillableStatus == undefined || AddNewWorkLogModel.BillableStatus == null || AddNewWorkLogModel.BillableStatus == "") {
                NewWorkLogForm.workLogTitle.$dirty = true;
                NewWorkLogForm.createdOn.$dirty = true;
                NewWorkLogForm.worklogBStatus.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                debugger;
                //alert(moment(NewWorkLogForm.createdOn.$viewValue).format("DD/MM/YYYY"));
                var model = {};
                model.Id = WorkLogId;
                model.WorkLogTitle = $scope.AddNewWorkLogModel.WorkLogTitle;
                model.WorkLogDescription = $scope.AddNewWorkLogModel.WorkLogDescription;
                model.CreatedOn = moment(NewWorkLogForm.createdOn.$viewValue).format("DD/MM/YYYY");
                //data.CreateOn = moment(NewWorkLogForm.createdOn.$viewValue).format("MM/DD/YYYY");
                model.CreatedUserId = UserId;
                model.BillableStatus = $scope.AddNewWorkLogModel.BillableStatus;
                model.ProjectId = $scope.AddNewWorkLogModel.ProjectId;
                model.PhaseId = $scope.AddNewWorkLogModel.PhaseId;
                model.PhaseMilestoneId = $scope.AddNewWorkLogModel.PhaseMilestoneId;

                worklogFactory.editWorkLog(model)
                     .success(function (data) {
                         debugger;
                         if (data.success == true) {
                             toaster.success("Success", data.ResponseData);
                             $('#globalLoader').hide();

                             NewWorkLogForm.workLogTitle.$dirty = false;
                             NewWorkLogForm.createdOn.$dirty = false;
                             NewWorkLogForm.worklogBStatus.$dirty = false;
                             $window.location.href = ('/#/Worklogs');
                         }
                         else {
                             toaster.warning("Warning", "Some Error Occured!");
                             $('#globalLoader').hide();
                         }
                     })
                     .error(function (data) {
                         toaster.error("Error", "Some Error Occured!");
                         $('#globalLoader').hide();
                     })
            }
        }

        $scope.deleteWorkLog = function (Id) {
           
            $confirm({ text: 'Are you sure you want to delete?', title: 'Delete it', ok: 'Yes', cancel: 'No' })

              .then(function () {
                  $('#globalLoader').show();
                  worklogFactory.deleteWorkLogById(Id)
                  .success(function (data) {

                      debugger;
                      if (data.ResponseData != null) {
                          toaster.success("Success", data.ResponseData);
                          $('#globalLoader').hide();
                          $window.location.href = ('/#/Worklogs');
                      }
                      else {
                          toaster.warning("Warning", "Some Error Occured!");
                          $('#globalLoader').hide();
                      }
              
                  })
                  .error(function (data) {
                      toaster.error("Error", "Some Error Occured!");
                      $('#globalLoader').hide();
                  })
              })
        }

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
                $rootScope.activeTeam = [];
                $scope.format = 'MM/dd/yyyy';
            }
            
            $scope.getApprovedProjectByUserId(UserId);
            $scope.getWorkLogById(WorkLogId);
        });


        $scope.open1 = function () {
            debugger;
            $scope.popup.opened1 = true;
            $scope.format = 'MM/dd/yyyy';
            $scope.mindate = new Date();
        };




    });
});