﻿
define(['app'], function (app) {
    //registrationFactory is the factory for the registration
    app.controller('vendorCompanyRegistrationController', function ($scope,apiURL, $rootScope, $timeout, toaster, registrationFactory, sharedService, $location, $stateParams, vendorFactory) {
        $rootScope.navbar = true;
        $scope.Type = "Company";
        $scope.registrationOption = "";
        $scope.registrationModel = {};

        $scope.imgURL = apiURL.imageAddress;

        var dataModal = {}

        dataModal.userCode = $stateParams.userCode

        angular.element(document).ready(function () {

            
            getInvitationsVendorCompanyRecord(dataModal);
        })



        $scope.proceedAhead =  function () {
    
            $location.path('/login');

        }




        function getInvitationsVendorCompanyRecord(dataModal) {
            $('#globalLoader').show();
            debugger;
            registrationFactory.getVendorCompanyRecord(dataModal)
            .success(function (data) {
                if (data.success)
                    if (data.ResponseData != null) {
                        toaster.success("Success", message.successdataLoad);
                        $('#globalLoader').hide();
                        $scope.registrationModel.Email = data.ResponseData.EmailId;
                    }
                    else {

                        $('#globalLoader').hide();
                    }
            })
            Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();

            })
        }


        var isValid = false;
        function removeClass() {
            $('#signin').removeClass('animate0 wobble');
        };
        // method called on register button click
        $scope.checkRegistrationValidation = function (event, registrationForm) {
            debugger
            //$('#globalLoader').show();
            event.preventDefault();
            $scope.dataLoading = true;

            if (registrationForm.FirstName.$invalid) {
                registrationForm.FirstName.$dirty = true;
                isvalid = false;
                $scope.FirstName_message = 'You must provide Full Name';
                return;
            }

            if (registrationForm.CompanyName.$invalid) {
                registrationForm.CompanyName.$dirty = true;
                isvalid = false;
                $scope.CompanyName_message = 'You must provide Company Name';
                return;
            }


            if (registrationForm.Password.$invalid) {
                registrationForm.Password.$dirty = true;
                isvalid = false;
                $scope.Password_message = 'You must provide Password';
                return;
            }


            if ($scope.registrationModel.TermsAndConditions != true) {
                isvalid = false;
                $scope.TermsAndConditions_message = 'You must agree to Terms and Conditions';
                return;
            }
   
                $('#signin').addClass('animate0 wobble');
                $timeout(removeClass, 1000);
            

                debugger;
                         
           
                var registrationData = {};
                registrationData.Email = $scope.registrationModel.Email;
                registrationData.FirstName = $scope.registrationModel.FirstName;
                registrationData.LastName = $scope.registrationModel.LastName;
                registrationData.CompanyName = $scope.registrationModel.CompanyName;
                registrationData.Password = $scope.registrationModel.Password;
                registrationData.Phone = $scope.registrationModel.Phone;
                registrationData.Fax = $scope.registrationModel.Fax;
                registrationData.Website = $scope.registrationModel.Website;
                registrationData.Address = $scope.registrationModel.Address;
                registrationData.Description = $scope.registrationModel.Description;


            registrationFactory.registerVendorCompany(registrationData)
                   .success(function (data) {
                       debugger;
                       if (data.success) {


                           if (data.ResponseData.IsAlreadyRegistered == true){
                               toaster.warning("warning", "This Company already  registered");
                           }
                           else if (data.ResponseData.IsAlreadyRegistered == false) {
                               getCompanyVendorForBlocker(data.ResponseData.VendorCompanyId);
                             
                           }
                     

                           $('#globalLoader').hide();
                       }
                   })
                   .error(function (data) {
                       toaster.error("Error", message[data.ResponseData]);
                       $('#globalLoader').hide();
                   })

                    $scope.dataLoading = false;

        };



        $scope.VendorBlockerList = []
        function getCompanyVendorForBlocker(VendorCompanyId) {
            $('#globalLoader').show();
            vendorFactory.getVendorCompanyPoc(VendorCompanyId)
            .success(function (data) {
                if (data.success) {
                    $scope.VendorBlockerList = data.ResponseData;
                    $('#BlockerVendorCompanyModalId').modal('show');// show poc of company
                    $('#globalLoader').hide();
                }
               
            }).error(function (data) {
                toaster.error("Error", message[data.ResponseData]);
                $('#globalLoader').hide();
            })

        }












        // 


        $scope.chkUserNameOnBlur = function (UserName) {

            $('#globalLoader').show();
            var data = { "UserName": UserName };
            registrationFactory.chkUserNameExist(data)
            .success(function (response) {
                if (response.success) {
                    $("input[name='UserName']").addClass("inputError");
                    $scope.isUserNameExist = true;
                    $('#globalLoader').hide();
                }
                else {
                    $("input[name='UserName']").removeClass("inputError");
                    $scope.isUserNameExist = false;
                    $('#globalLoader').hide();
                }
            })
            .error(function (data) {
                toaster.error("Error", message[data.ResponseData]);
                $('#globalLoader').hide();
            })
        };


        $scope.TTTTTTTTTTTTTTTTTTTTTcheckRegistrationValidation =    function (x) {
            debugger
            $('#globalLoader').show();
            vendorFactory.getVendorCompanyPoc(2)
            .success(function (data) {
                if (data.success) {
                    $scope.VendorBlockerList = data.ResponseData;
                    $('#BlockerVendorCompanyModalId').modal('show');// show poc of company
                    $('#globalLoader').hide();
                }

            }).error(function (data) {
                toaster.error("Error", message[data.ResponseData]);
                $('#globalLoader').hide();
            })

        }


    });
});