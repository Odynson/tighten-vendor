﻿define(['app',], function (app) {
    app.controller("teamdetailController", function ($scope, $rootScope, $stateParams, toaster, $http, apiURL, sharedService, teamdetailFactory, usersFactory, teamFactory,vendorFactory, $timeout, $confirm, $window) {
        var teamId = $stateParams.pid;
        $scope.TeamId = 0;
        $scope.TeamDetail = [];
        $scope.myTeamMember = {};
        $scope.TeamAdminDetail = {};
        $scope.TeamMembersList = [];
        $scope.designationgModalList = [];
        $scope.showMsgs = false;
        $scope.flag = 0;
        $scope.oc;
        $scope.disabledAnchor = false;
        $scope.showCenterLoader = false;
        $scope.openSaveRoleTags = false;
        $scope.teamAddUpdateModel = {};
        $scope.orgChartDataSourceModel = {};
        $scope.teamAddUpdateModel.teamMembers = [];
        $scope.AllInternalUsersForTeam = [];
        $scope.teamButton = false;
        $scope.hideShowTeamMembers = true;
        $scope.hideShowNestedTeams = false;
        $scope.PageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page. 
        $scope.CompanyTeamsObject = [];
        $scope.CompanyTeamsInTreeViewList = [];
        $scope.totalCount = 100;
        $scope.designationgModalList = [];
        $scope.IsTMActive = "";
        
        $scope.hideshowNestedTeamsTV = true;
        /* Showing Team Detail to see the information*/
        $scope.showTeamDetail = function () {

            $scope.showCenterLoader = true;
            
            $scope.getTeamDetail();

        };

         $scope.showTeamAddUpdateModal = function () {
            //debugger;
            $scope.showMsgs = false;
            $scope.flag = 0;
            //$scope.getAllUsers();
            $('#globalLoader').show();
            usersFactory.getUsersForTeam(sharedService.getCompanyId())
                .success(function (data) {
                   // debugger;
                    if (data.success && data.ResponseData != null) {
                        $scope.UsersObject = data.ResponseData;
                        $scope.AllInternalUsersForTeam = [];
                        $scope.myTeamMember.teamMembers = [];
                        $scope.myTeamMember.teamMembers = $scope.UsersObject;
                        $scope.teamButton = false;
                        getAllDesignation();
                        $scope.teamAddUpdateModel.teamName = $scope.TeamDetail.TeamName;
                        $scope.teamAddUpdateModel.teamBudget = $scope.TeamDetail.TeamBudget;
                        $scope.teamAddUpdateModel.teamDescription = $scope.TeamDetail.TeamDescription;
                        $scope.teamAddUpdateModel.teamId = $scope.TeamDetail.TeamId;
                        $scope.teamAddUpdateModel.TeamMembers = [];
                        var teamMembers = [];
                        //if ($scope.teamAddUpdateModel.TeamMembers.length > 0) {
                        for (var i = 0; i < $scope.TeamDetail.TeamMembers.length; i++) {
                            var newItemNo = $scope.columns.length + 1;
                           
                                $scope.columns.push({ 'colId': 'col' + newItemNo, 'name': $scope.TeamDetail.TeamMembers[i].MemberName, 'selected': $scope.TeamDetail.TeamMembers[i].MemberUserId, 'UserId': $scope.TeamDetail.TeamMembers[i].MemberUserId, 'radioSelected': true});
                           
                                teamMembers.push({ 'MemberUserId': $scope.TeamDetail.TeamMembers[i].MemberUserId, 'MemberDesignationId': $scope.TeamDetail.TeamMembers[i].MemberDesignationId, 'IsTeamLead': $scope.TeamDetail.TeamMembers[i].IsTeamLead, 'MemberName': $scope.TeamDetail.TeamMembers[i].MemberName, 'MemberProfilePhoto': $scope.TeamDetail.TeamMembers[i].MemberProfilePhoto, 'DesignationId': $scope.TeamDetail.TeamMembers[i].DesignationId, 'DesignationName': $scope.TeamDetail.TeamMembers[i].DesignationName, 'selected': $scope.TeamDetail.TeamMembers[i].MemberUserId });
                                $scope.AllInternalUsersForTeam.push({ 'UserId': $scope.TeamDetail.TeamMembers[i].MemberUserId, 'MemberDesignationId': $scope.TeamDetail.TeamMembers[i].MemberDesignationId, 'IsTeamLead': $scope.TeamDetail.TeamMembers[i].IsTeamLead, 'Name': $scope.TeamDetail.TeamMembers[i].MemberName, 'MemberProfilePhoto': $scope.TeamDetail.TeamMembers[i].MemberProfilePhoto, 'DesignationId': $scope.TeamDetail.TeamMembers[i].DesignationId, 'DesignationName': $scope.TeamDetail.TeamMembers[i].DesignationName });
                        }
                            if ($scope.TeamDetail.TeamMembers.length > 0) {
                                for (var i = 0; i < $scope.TeamDetail.TeamMembers.length; i++) {
                                    if ($scope.TeamDetail.TeamMembers[i].IsTeamLead) {
                                        $scope.columns[i].radioSelected = $scope.TeamDetail.TeamMembers[i].MemberUserId;
                                    }
                                    var RoleId = $scope.TeamDetail.TeamMembers[i].DesignationId;
                                    $scope.designationgModalList[i] = { "RoleId": (RoleId == undefined) ? '' : RoleId };
                                    //$scope.designationgModalList[i] = $scope.TeamDetail.TeamMembers[i].DesignationId;
                                }
                            }
                        
                        //}
                        // Already added team members list
                            $scope.teamAddUpdateModel.teamMembers = teamMembers;
                            if ($scope.TeamDetail.TeamMembers.length > 0) {
                                $scope.openSaveRoleTags = true;
                            }
                        $scope.showCenterLoader = false;
                        $scope.teamButton = true;
                        $('#modalTeamAddUpdate').modal('show');
                        $('#divTeamManagementSection').show();
                        $('#globalLoader').hide();
                    }
                }).error(function () {
                    toaster.error("Error", "Some error occured");
                    $('#globalLoader').hide();

                });
            $scope.tagRemoved = function (tag) {
                debugger;
                var index = -1;
                if($scope.columns.length>0){
                    for (var i = 0; i < $scope.columns.length; i++) {
                        if ($scope.columns[i].UserId == tag.UserId) {
                            //index = i;
                            $scope.removeColumn(i);
                            break;
                        }
                    }
                }
                
                
            }
             /* Function to reset radioSelected property of all radio buttons except the selected one*/
            $scope.toggleRadioSelection = function (item, index) {
                console.log('function called');
                debugger;

                for (var i = 0; i < $scope.teamAddUpdateModel.teamMembers.length; i++) {
                    //if (i != index) {
                    //    $scope.myTeamMember.teamMembers[i].radioSelected = "";
                    //}
                    //else {
                    //    $scope.myTeamMember.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].UserId;
                    //}
                    $scope.teamAddUpdateModel.teamMembers[i].radioSelected = '';
                    $scope.teamAddUpdateModel.teamMembers[i].IsTeamLead = false;
              
                    if ($scope.teamAddUpdateModel.teamMembers[i].MemberUserId == item.UserId) {
                        $scope.teamAddUpdateModel.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].UserId;
                        $scope.teamAddUpdateModel.teamMembers[i].IsTeamLead = true;
                        //break;
                    }
                    //else {
                    //    $scope.myTeamMember.teamMembers[i].radioSelected = "";

                    //}
                }
                console.log($scope.teamMembersNew);
            };
            $scope.removeColumn = function (index) {
                $scope.columns.splice(index, 1);
                $scope.teamAddUpdateModel.teamMembers.splice(index, 1);
                $scope.designationgModalList.splice(index, 1);
               // debugger;
                // if no rows left in the array create a blank array
                if ($scope.columns.length === 0 || $scope.columns.length == null) {
                    alert('no rec');
                    $scope.columns.push = [{ "colId": "col1" }];
                }

            }
            $scope.tagAdded = function (tag) {
                $scope.addNewColumn(tag);
            }
            $scope.addNewColumn = function (tag) {
                debugger;
                var newItemNo = $scope.columns.length + 1;
                if ($scope.columns == undefined || $scope.columns == null) {
                    $scope.columns = [];
                }
                $scope.columns.push({ 'colId': 'col' + newItemNo, 'name': tag.Name, 'selected': tag.UserId, 'UserId': tag.UserId });
                $scope.teamAddUpdateModel.teamMembers.push({ 'MemberUserId': tag.UserId, 'MemberName': tag.Name, 'selected': tag.UserId }); //'IsTeamLead': $scope.TeamDetail.TeamMembers[i].IsTeamLead,               
                $scope.designationgModalList[newItemNo] = { "RoleId": '' };
                angular.element(document.querySelector("#selectDesignationId" + '-' + newItemNo)).val('');
            };
            
            $scope.editTeamMembersTags = function () {
                debugger;
                multiSelectCurrentData = $scope.AllInternalUsersForTeam;
                for (var i = 0; i < multiSelectCurrentData.length; i++) {
                    multiSelectPreviousData.push(multiSelectCurrentData[i]);
                }

                $scope.openSaveRoleTags = true;
                $timeout(function () {
                    $('#TeamMemberTags  .host .tags .input').focus();
                }, 100);
            }

             //////////////////////// Update Team Detail///////////////////////////

            $scope.updateAllLevelTeam = function (isTeamNameInvalid, isTeamDescriptionInvalid, isTeamBudgetInvalid, teamAddUpdateForm, flag) {

                //debugger;
                //if (isTeamNameInvalid || isTeamDescriptionInvalid || isTeamBudgetInvalid) {
                //    teamAddUpdateForm.teamName.$dirty = true;
                //    teamAddUpdateForm.teamDescription.$dirty = true;
                //    teamAddUpdateForm.teamBudget.$dirty = true;
                //    return;
                //}
                if (teamAddUpdateForm.$valid) {
                    var teamAddUpdateModel = {
                        TeamId: 0, TeamName: "", TeamBudget: 0, TeamDescription: "", ParentId: 0,
                        ParentTeamId: 0, TeamMembers: []
                    };

                    teamAddUpdateModel.CompanyId = sharedService.getCompanyId();
                    teamAddUpdateModel.TeamName = $scope.teamAddUpdateModel.teamName;
                    teamAddUpdateModel.TeamBudget = $scope.teamAddUpdateModel.teamBudget;
                    teamAddUpdateModel.TeamDescription = $scope.teamAddUpdateModel.teamDescription;
                    teamAddUpdateModel.TeamId = $scope.teamAddUpdateModel.teamId;
                    teamAddUpdateModel.ParentTeamId = $scope.TeamDetail.ParentTeamId;


                    angular.forEach($scope.teamAddUpdateModel.teamMembers, function (value, key) {
                        // debugger;

                        if ($scope.teamAddUpdateModel.teamMembers[key].selected == $scope.teamAddUpdateModel.teamMembers[key].MemberUserId) {
                            var MemberUserId = $scope.teamAddUpdateModel.teamMembers[key].selected;
                            var DesignationId = angular.element(document.querySelector("#selectDesignationId" + '-' + key)).controller("ng-model").$modelValue.RoleId;
                            var IsTeamLead = $scope.teamAddUpdateModel.teamMembers[key].IsTeamLead;
                            var MemberRoleId = value.MemberRoleId != null ? value.MemberRoleId : 0;
                            IsTeamLead = (IsTeamLead == undefined || IsTeamLead == '') ? false : true;
                            teamAddUpdateModel.TeamMembers.push({ 'MemberUserId': MemberUserId, 'MemberDesignationId': DesignationId, 'MemberRoleId': MemberRoleId, 'IsTeamLead': IsTeamLead });
                        }
                    })


                    var NewTeamObj = { 'TeamModel': teamAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': sharedService.getCompanyId() }
                    $('#globalLoader').show();

                    teamFactory.updateTeam(NewTeamObj)
                        .success(function (data) {
                            if (data.success) {
                                debugger;

                                if (data.ResponseData != 'DuplicateTeam') {

                                    if (data.ResponseData == "TeamProjectsBudgetExceed") {
                                        toaster.warning("Warning", message["TeamProjectsBudgetExceed"]);
                                        $('#globalLoader').hide();
                                        return;
                                    }

                                    toaster.success("Success", data.ResponseData);
                                    $('#modalTeamAddUpdate').modal('hide');
                                    $('#modalSubTeamAddUpdate').modal('hide');
                                    // $scope.$emit('bindTeamDropdown', []);
                                    /* Getting Teams in which user is Team Member */

                                    //$scope.getCompanyTeams();
                                    $scope.TeamDetail = [];
                                    $scope.TeamAdminDetail = {};
                                    $scope.TeamMembersList = [];
                                    $scope.TeamId = teamId;

                                    $scope.showTeamDetail();
                                    $scope.Role = sharedService.getRoleId();
                                    $('#globalLoader').hide();
                                }
                            }
                            else {
                                toaster.error("Error", data.message);
                                $('#globalLoader').hide();
                            }

                            $scope.IsEditShowBtn = true;
                            $scope.IsEditTeam = false;

                        })
                }
                else {
                    $scope.showMsgs = true;
                }
            };

            

            //$scope.teamButton = false;
            $scope.teamAddUpdateModel = {};
            $scope.teamAddUpdateModel.teamMembers = [];
            //$scope.myTeamMember.teamMembers = [];
            //$scope.myTeamMember.teamMembers = $scope.UsersObject;
            //$('#teamName').removeClass('inputError');
            $("input[name='teamName']").removeClass("inputError");
            $("textarea[name='teamDescription']").removeClass("inputError");
            $("input[name='teamBudget']").removeClass("inputError");
            $scope.columns = [];
            $scope.teamMembersNew = [];
            $scope.openSaveRoleTags = false;
            var multiSelectPreviousData = [];
            var multiSelectCurrentData = [];
            //$('#modalTeamAddUpdate').modal('show');
        };

         $scope.editNestedTeamMembersTags = function () {
             debugger;
             multiSelectCurrentData = $scope.AllInternalUsersForTeam;
             for (var i = 0; i < multiSelectCurrentData.length; i++) {
                 multiSelectPreviousData.push(multiSelectCurrentData[i]);
             }

             $scope.openSaveRoleTags = true;
             $timeout(function () {
                 $('#TeamMemberTags  .host .tags .input').focus();
             }, 100);
         }
        /*Delete Team*/
         $scope.DeleteTeam = function () {
             var teamId = $scope.TeamDetail.TeamId;
             //alert(teamId);
             var index = -1;

             $confirm({ text: 'Are you sure you want to delete this Team?', title: 'Delete it', ok: 'Yes', cancel: 'No' })

                 .then(function () {
                     index = -1;
                     debugger;
                     //for (var i = 0; i < $scope.CompanyTeamsObject.length; i++) {
                     //    if ($scope.CompanyTeamsObject[i].TeamId == teamId) {

                     //        index = i;
                     //        break;
                     //    }
                     //    for (var j = 0; j < $scope.CompanyTeamsObject[i].SubTeamList.length; j++) {
                     //        if ($scope.CompanyTeamsObject[i].SubTeamList[j].TeamId == teamId) {
                     //            index = i;
                     //            break;
                     //        }
                     //    }
                     //}

                     $('#globalLoader').show();
                     teamFactory.deleteTeam(teamId)
                         .success(function (data) {
                             debugger;
                             if (data.success) {
                                 $('#globalLoader').hide();
                                 toaster.success("Success", message[data.message]);
                                 //$scope.isTeamEditDivOpend = false;
                                 //$scope.teamAddUpdateModel = {};
                                 //$scope.myTeamMember.teamMembers = [];
                                 //$scope.CompanyTeamsObject.splice(index, 1);
                                 //$scope.showTeamManagementSection();
                                 //$scope.Role = sharedService.getRoleId();
                                 ///* Getting Teams in which user is Team Member */
                                 //teamFactory.getLoggedInUserTeams(sharedService.getUserId(), $scope.selectedCompanyId)
                                 //    .success(function (data) {
                                 //        if (data.success) {
                                 //            $scope.TeamsObject = data.ResponseData;
                                 //            $scope.activeTeam = $scope.TeamsObject[0];
                                 //            // Updating the team dropdown
                                 //            $scope.$emit('bindTeamDropdown', []);
                                 //            $scope.$emit('bindUserDetail', []);
                                 //            $('#globalLoader').hide();
                                 //        }
                                 //        else {
                                 //            toaster.error("Error", data.message);
                                 //            $('#globalLoader').hide();
                                 //        }
                                 //    })
                                 //    .error(function () {
                                 //        toaster.error("Error", "Some Error Occured !");
                                 //        $('#globalLoader').hide();
                                 //    });
                                 
                                 $window.location.href = ('/#/OrgChartDemo/0');///'+teamId
                             }
                             else {
                                 if (data.message == "ProjectPresentInTeam") {
                                     toaster.warning("Warning", message[data.message]);
                                     $('#globalLoader').hide();
                                 }
                                 else {
                                     toaster.error("Error", data.message);
                                     $('#globalLoader').hide();
                                 }
                             }
                         })
                         .error(function () {
                             toaster.error("Error", "Some Error Occured!");
                             $('#globalLoader').hide();
                         });


                 })

         };


        /*Get All Designation*/
         function getAllDesignation() {
             $('#globalLoader').show();
             teamFactory.getAllDesignation()
                 .success(function (data) {
                    // debugger
                     $scope.getAllDesignationList = data.ResponseData;
                     $('#globalLoader').hide();

                     if ($scope.flag == 1) {
                         $scope.IsVisible = true;
                         angular.element(document.querySelector("#divMembers")).removeClass("ng-hide");
                         $scope.teamButton = false;
                         $scope.showCenterLoader = false;
                         /*
                          $timeout to schedule the changes to the scope in a future call stack. By providing a timeout period of 0ms,
                          this will occur as soon as possible and $timeout will ensure that the code will be called in a single $apply block.
                          */
                         //$scope.designationgModalList[0] = { "RoleId": '' };
                         //angular.element(document.querySelector("#selectTeamId")).val('');
                         $timeout(function () {
                             $('#globalLoader').hide();

                             $('#modalSubTeamAddUpdate').modal('show');
                         }, 1000)
                         $('#globalLoader').hide();
                     }
                 }).error(function () {
                     toaster.error("Error", "Some error occured");
                     $('#globalLoader').hide();

                 });
         }
      
        $scope.getTeamDetail = function () {
            debugger;
            /* Getting Company Team Detail with TeamId */
            teamdetailFactory.GetTeamDetail($scope.TeamId)

                .success(function (data) {
                    if (data.success) {
                        debugger;

                        $scope.TeamDetail = data.ResponseData;
                        var tempteamMembers = [];
                        for (var i = 0; i < $scope.TeamDetail.TeamMembers.length; i++) {
                            if ($scope.TeamDetail.TeamMembers[i].IsTeamLead == true) {
                                $scope.TeamAdminDetail.MemberUserId = $scope.TeamDetail.TeamMembers[i].MemberUserId;
                                $scope.TeamAdminDetail.MemberDesignationId = $scope.TeamDetail.TeamMembers[i].MemberDesignationId;
                                $scope.TeamAdminDetail.IsTeamLead = $scope.TeamDetail.TeamMembers[i].IsTeamLead;
                                $scope.TeamAdminDetail.MemberName = $scope.TeamDetail.TeamMembers[i].MemberName;
                                $scope.TeamAdminDetail.MemberProfilePhoto = $scope.TeamDetail.TeamMembers[i].MemberProfilePhoto;
                                $scope.TeamAdminDetail.DesignationId = $scope.TeamDetail.TeamMembers[i].DesignationId;
                                $scope.TeamAdminDetail.DesignationName = $scope.TeamDetail.TeamMembers[i].DesignationName;

                                //$scope.TeamAdminDetail.push({ 'MemberUserId': $scope.TeamDetail.TeamMembers[i].MemberUserId, 'MemberDesignationId': $scope.TeamDetail.TeamMembers[i].MemberDesignationId, 'IsTeamLead': $scope.TeamDetail.TeamMembers[i].IsTeamLead, 'MemberName': $scope.TeamDetail.TeamMembers[i].MemberName, 'MemberProfilePhoto': $scope.TeamDetail.TeamMembers[i].MemberProfilePhoto });
                                //break;
                            }
                            else {
                                $scope.TeamMembersList.push({ 'MemberUserId': $scope.TeamDetail.TeamMembers[i].MemberUserId, 'MemberDesignationId': $scope.TeamDetail.TeamMembers[i].MemberDesignationId, 'IsTeamLead': $scope.TeamDetail.TeamMembers[i].IsTeamLead, 'MemberName': $scope.TeamDetail.TeamMembers[i].MemberName, 'MemberProfilePhoto': $scope.TeamDetail.TeamMembers[i].MemberProfilePhoto, 'DesignationId': $scope.TeamDetail.TeamMembers[i].DesignationId, 'DesignationName': $scope.TeamDetail.TeamMembers[i].DesignationName });
                            }
                        }
                        //$scope.TeamAdminDetail = teamAdminMembers;
                    }
                    else {
                        debugger;
                        toaster.error("Error", data.message);
                    }
                })
                .error(function (e) {
                    debugger;
                    toaster.error("Error", "Some Error Occured");
                });
        };
        function ChangeTag(TagName) {
            $scope.IsTMActive = "";
            $scope.IsPDActive = "";
            $scope.IsFinActive = "";
            $scope.IsTTActive = "";
            if (TagName == "tmactive") {
                $scope.IsTMActive = "active";
            }
            else if (TagName == "pdactive") {
                $scope.IsPDActive = "active";
            }
            else if (TagName == "finactive") {
                $scope.IsFinActive = "active";
            }
            else if (TagName == "ttactive") {
                $scope.IsTTActive = "active";
            }
        }
        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');

            }
            $scope.TeamId = teamId;
            
            $scope.showTeamDetail();
            //$scope.ShowNestedTeamTreeView();
            if ($scope.orgChartDataSourceModel.name == null) {
                teamFactory.CompanyNestedTeamsChart(sharedService.getCompanyId(), sharedService.getUserId(), 1, 15, $scope.TeamId)
               .success(function (data) {
                   debugger;

                   if (data.success) {
                       if (data.ResponseData.length > 0) {
                           $scope.orgChartDataSourceModel = {};
                           $scope.orgChartDataSourceModel = data.ResponseData[0];
                           if ($scope.orgChartDataSourceModel.children.length > 0) {
                               $scope.oc = $('#chart-container').orgchart({
                                   'data': $scope.orgChartDataSourceModel,
                                   'nodeContent': 'title',
                                   'direction': 't2b',
                                   'pan': true,
                               });
                               $scope.oc.$chartContainer.on('click', '.node', function () {
                                   debugger;
                                   var $this = $(this);
                                   //alert($this.find('.title').text());
                                   //alert($this.find('.content').text());
                                   $window.location.href = ('/#/team/teamdetail/' + $this.find('.content').text().trim());
                                   $('#selected-node').val($this.find('.title').text()).data('node', $this);
                               });
                           }
                           else
                           {
                               $scope.hideshowNestedTeamsTV = false;
                           }
                       }
                       $('#globalLoader').hide();
                   }
                   else {
                       $('#globalLoader').hide();
                       toaster.error("Error", data.message);
                   }
               })
               .error(function () {
                   toaster.error("Error", "Some Error Occured !");
               });
            }
            ChangeTag("tmactive");
          //  $scope.Role = sharedService.getRoleId();

        });
        $scope.BackToTeam = function () {
            debugger;
            $window.location.href = "/#/OrgChartDemo/0";
        }
        function RefreshTreeView() {
            $('#globalLoader').show();
            //if ($scope.orgChartDataSourceModel.name == null) {
                teamFactory.CompanyNestedTeamsChart(sharedService.getCompanyId(), sharedService.getUserId(), 1, 15, $scope.TeamId)
               .success(function (data) {
                   debugger;
                   
                   if (data.success) {
                       if (data.ResponseData.length > 0) {
                           $scope.orgChartDataSourceModel = {};
                           $scope.orgChartDataSourceModel = data.ResponseData[0];
                           if ($scope.orgChartDataSourceModel.children.length>0){
                           $scope.oc.init({ 'data': $scope.orgChartDataSourceModel });
                           }
                           else {
                               $scope.hideshowNestedTeamsTV = false;
                           }
                       }
                   }
                   else {
                       toaster.error("Error", data.message);
                   }
                   $('#globalLoader').hide();
               })
               .error(function () {
                   $('#globalLoader').hide();
                   toaster.error("Error", "Some Error Occured !");
               });
            //}
        }
        $scope.ShowNestedTeamTreeView = function () {
            debugger;
            //alert($scope.TeamDetail.TeamId);

            $('#globalLoader').show();
            if ($scope.orgChartDataSourceModel.name == null ) {
                teamFactory.CompanyNestedTeamsChart(sharedService.getCompanyId(), sharedService.getUserId(), 1, 15, $scope.TeamId)
               .success(function (data) {
                   debugger;
                   
                   if (data.success) {
                       if (data.ResponseData.length > 0) {
                           $scope.orgChartDataSourceModel = {};
                           $scope.orgChartDataSourceModel = data.ResponseData[0];
                           $scope.oc = $('#chart-container').orgchart({
                               'data': $scope.orgChartDataSourceModel,
                               'nodeContent': 'title',
                               'direction': 't2b',
                               'pan': true,
                           });
                           $scope.oc.$chartContainer.on('click', '.node', function () {
                               debugger;
                               var $this = $(this);
                               //alert($this.find('.title').text());
                               //alert($this.find('.content').text());
                               $window.location.href = ('/#/team/teamdetail/' + $this.find('.content').text().trim());
                               $('#selected-node').val($this.find('.title').text()).data('node', $this);
                           });
                       }
                       $('#globalLoader').hide();
                   }
                   else {
                       $('#globalLoader').hide();
                       toaster.error("Error", data.message);
                   }
               })
               .error(function () {
                   toaster.error("Error", "Some Error Occured !");
               });
            }
        }
        $scope.ShowNestedTeamTab = function () {
            debugger;
            $scope.hideShowTeamMembers = false;
            $scope.hideShowNestedTeams = true;
            ChangeTag("ttactive");
            RefreshTreeView();
        }
        $scope.ShowTeamMemberTab = function () {
            $scope.hideShowTeamMembers = true;
            $scope.hideShowNestedTeams = false;
            ChangeTag("tmactive");
        }
        $scope.selectedCompanyId = sharedService.getCompanyId();
        $scope.AllInternalUsersForTeam = [];
        $scope.myTeamMember = {};
        $scope.columns = [];
        $scope.teamMembersNew = [];
        $scope.openSaveRoleTags = false;
        $scope.isTeamEditDivOpend = true;
        $scope.createNestedTeam = function () {

            //   getAllDesignation();
            $scope.showNestedMsgs = false;
            $scope.NestedTeamAddUpdateModel = {};
            $('#globalLoader').show();
            vendorFactory.GetAllNewUsers($scope.selectedCompanyId, sharedService.getUserId())
                .success(function (data) {
                    if (data.success && data.ResponseData != undefined) {
                        debugger;
                        getAllDesignation();
                        $scope.UsersObject = data.ResponseData;
                        $scope.AllInternalUsersForTeam = [];
                        $scope.myTeamMember.teamMembers = [];
                        $scope.myTeamMember.teamMembers = $scope.UsersObject;
                        $('#globalLoader').hide();
                    }
                    else {
                        $('#globalLoader').hide();
                    }
                });
            $scope.columns = [];
            $scope.teamMembersNew = [];
            $scope.openSaveRoleTags = false;
            var multiSelectPreviousData = [];
            var multiSelectCurrentData = [];
            $('#modalTeamNestedAdd').modal('show');

        }



        $scope.$watch('TeamTree.currentNode', function (newObj, oldObj) {
            debugger;
            if ($scope.TeamTree && angular.isObject($scope.TeamTree.currentNode)) {
                $scope.isTeamEditDivOpend = true;
                $scope.teamAddUpdateModel = {};
                $scope.myTeamMember.teamMembers = [] // empty team member
                $scope.teamAddUpdateModel.teamName = $scope.TeamTree.currentNode.TeamName;
                $scope.teamAddUpdateModel.teamBudget = $scope.TeamTree.currentNode.TeamBudget;
                $scope.teamAddUpdateModel.teamDescription = $scope.TeamTree.currentNode.TeamDescription;
                $scope.teamAddUpdateModel.teamId = $scope.TeamTree.currentNode.TeamId;
                $scope.teamAddUpdateModel.teamCreateDate = $filter('date')($scope.TeamTree.currentNode.TeamCreatedDate, "dd-MM-yyyy");// $scope.TeamTree.currentNode.TeamCreatedDate;
                $scope.myTeamMember.ParentTeamId = $scope.TeamTree.currentNode.ParentTeamId

                $scope.myTeamMember.teamMembers = $scope.TeamTree.currentNode.TopTeamMembersList;

                //var topTeamMemberList = [];
                var nestedTeamMemberList = [];
                //topTeamMemberList = $scope.TeamTree.currentNode.TopTeamMembersList
                nestedTeamMemberList = $scope.TeamTree.currentNode.TeamMembersList;

                RadiobuttonselectDy()
                function RadiobuttonselectDy() {
                    debugger
                    for (var i = 0; i < $scope.myTeamMember.teamMembers.length; i++) {
                        if ($scope.myTeamMember.teamMembers[i].IsTeamLead) {
                            $scope.myTeamMember.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].MemberUserId;
                        }

                        $scope.myTeamMember.teamMembers[i].selected = $scope.myTeamMember.teamMembers[i].MemberUserId;
                    }
                }

                if ($scope.myTeamMember.teamMembers.length > 0) {// && $scope.TeamTree.currentNode.ParentTeamId != 0
                    angular.forEach($scope.myTeamMember.teamMembers, function (value, key) {
                        for (var i = 0; i < nestedTeamMemberList.length; i++) {
                            debugger
                            // comparing the already added member in the list of all members
                            if ($scope.myTeamMember.teamMembers[key].MemberUserId == nestedTeamMemberList[i].MemberUserId) {
                                $scope.myTeamMember.teamMembers[key].selected = nestedTeamMemberList[i].MemberUserId;
                                $scope.myTeamMember.teamMembers[key].MemberDesignationId = nestedTeamMemberList[i].MemberDesignationId;
                                if ($scope.myTeamMember.teamMembers[i].IsTeamLead) {
                                    $scope.myTeamMember.teamMembers[key].radioSelected = nestedTeamMemberList[i].MemberUserId;
                                }

                                break;
                            }
                        }
                    })
                }

            }
        }, false);
        
        $scope.addNewNestedTeam = function (NestedTeamAddUpdateModel, teamAddUpdateForm) {
            debugger;
            if (teamAddUpdateForm.$valid) {
                var teamAddUpdateModel = {
                    TeamId: 0, TeamName: "", TeamBudget: 0, TeamDescription: "", ParentId: 0,
                    ParentTeamId: 0, TeamMembers: []
                };

                teamAddUpdateModel.TeamId = 0 // auto 
                teamAddUpdateModel.TeamName = NestedTeamAddUpdateModel.teamName;
                teamAddUpdateModel.TeamBudget = NestedTeamAddUpdateModel.teamBudget;
                teamAddUpdateModel.TeamDescription = NestedTeamAddUpdateModel.teamDescription;
                teamAddUpdateModel.ParentTeamId = teamId;//$scope.teamAddUpdateModel.teamId;



                angular.forEach($scope.teamMembersNew, function (value, key) {
                    debugger;

                    //if ($scope.myTeamMember.teamMembers[key].selected == $scope.myTeamMember.teamMembers[key].MemberUserId) {
                    var MemberUserId = $scope.teamMembersNew[key].selected;
                        var DesignationId = 0;
                        //alert(angular.element(document.querySelector("#selectDesignationId" + '-' + key)).controller("ng-model").$modelValue.RoleId);
                        //var DesignationId = angular.element(document.querySelector("#selectDesignationId" + '-' + key)).controller("ng-model").$modelValue.RoleId;
                        var IsTeamLead = $scope.teamMembersNew[key].radioSelected;

                    //var MemberRoleId = value.MemberRoleId != null ? value.MemberRoleId : 0;
                        var tempmemberroleid = $("#selectDesignationId-" + key).val();
                        var MemberRoleId = tempmemberroleid != undefined && tempmemberroleid != null ? tempmemberroleid : 0;
                        IsTeamLead = (IsTeamLead == undefined || IsTeamLead == '') ? false : true;
                        teamAddUpdateModel.TeamMembers.push({ 'MemberUserId': MemberUserId, 'MemberDesignationId': MemberRoleId, 'MemberRoleId': MemberRoleId, 'IsTeamLead': IsTeamLead });
                   // }
                })


                var NewTeamObj = { 'TeamModel': teamAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId }
                teamFactory.insertTeam(NewTeamObj)
                    .success(function (data) {
                        $('#globalLoader').hide();
                        $scope.getCompanyTeams();
                        $('#modalTeamNestedAdd').modal('hide');
                        teamAddUpdateModel = {};
                        $scope.columns = [];
                        $scope.myTeamMember.teamMembers = [] // empty team member
                        $scope.teamMembersNew = [];
                        $scope.designationgModalList = [];
                        RefreshTreeView();
                    })
                    .error(function (error) {
                        toaster.error("Error", "Some Error Occured!");
                        $('#globalLoader').hide();
                    });
            }
            else {
                $scope.showNestedMsgs = true;
            }

        }
        $scope.getCompanyTeams = function () {
            debugger
            /* Getting Company Teams with Users */
            teamFactory.getCompanyTeams($scope.selectedCompanyId, sharedService.getUserId(), $scope.PageIndex, $scope.pageSizeSelected)
                .success(function (data) {
                    if (data.success) {

                        $scope.CompanyTeamsObject = data.ResponseData;
                        $scope.CompanyTeamsInTreeViewList = data.ResponseData;
                        // $scope.CustomcontrolList = $scope.CompanyTeamsObject;
                        // $scope.locations = $scope.CompanyTeamsObject;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCount = data.ResponseData[0].TotalPageCount;
                        }
                        debugger

                        /* Getting Users Details in which all users are there so that they can be added for adding new team */
                        var checkEmailConfirmed = true;
                        //usersFactory.getUsers(sharedService.getUserId(), checkEmailConfirmed, $scope.selectedCompanyId)
                        //    .success(function (data) {
                        //        if (data.success) {
                        //            debugger;
                        //            $scope.UsersObject = data.ResponseData;
                        //            $scope.showCenterLoader = false;
                        //            $('#divTeamManagementSection').show();
                        //        }
                        //        else {
                        //            toaster.error("Error", data.message);
                        //        }
                        //    })
                        //    .error(function () {
                        //        toaster.error("Error", "Some error occured");
                        //    });
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured");
                });
        }
        $scope.tagAddedNested = function (tag) {
            $scope.addNewColumnNested(tag);
        }
        $scope.addNewColumnNested = function (tag) {
            var newItemNo = $scope.columns.length + 1;
            $scope.columns.push({ 'colId': 'col' + newItemNo, 'name': tag.Name, 'selected': tag.UserId, 'UserId': tag.UserId });
            $scope.teamMembersNew.push({ 'colId': 'col' + newItemNo, 'Description': tag.Description, 'Name': tag.Name, 'RoleId': tag.RoleId, 'UserId': tag.UserId, 'selected': tag.UserId });
        };
        $scope.tagRemovedNested = function (tag) {
            var index = $scope.columns.indexOf(tag);
            $scope.removeColumnNested(index);
        }
        $scope.removeColumnNested = function (index) {
            $scope.columns.splice(index, 1);
            // if no rows left in the array create a blank array
            if ($scope.columns.length() === 0 || $scope.columns.length() == null) {
                alert('no rec');
                $scope.columns.push = [{ "colId": "col1" }];
            }

        }
        /*Function to reset job profile dropdown when checkbox is un checked */
        $scope.toggleSelectionNested = function toggleSelection(item, index) {
            debugger;
            if (item.selected == '') {
                $scope.designationgModalList[index] = { "RoleId": '' };

            }
        };

        $scope.toggleRadioSelectionNested = function (item, index) {
            console.log('function called');
            debugger;

            for (var i = 0; i < $scope.myTeamMember.teamMembers.length; i++) {
                //if (i != index) {
                //    $scope.myTeamMember.teamMembers[i].radioSelected = "";
                //}
                //else {
                //    $scope.myTeamMember.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].UserId;
                //}
                if ($scope.myTeamMember.teamMembers[i].UserId == item.UserId) {
                    $scope.myTeamMember.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].UserId;
                    break;
                }
                //else {
                //    $scope.myTeamMember.teamMembers[i].radioSelected = "";

                //}
            }

            for (var i = 0; i < $scope.teamMembersNew.length; i++) {
                if ($scope.teamMembersNew[i].colId == item.colId) {
                    $scope.teamMembersNew[i].radioSelected = $scope.teamMembersNew[i].UserId;
                    //break;
                }
                else {
                    $scope.teamMembersNew[i].radioSelected = "";
                }
            }
            console.log($scope.teamMembersNew);
        };
        //$scope.tagRemoved = function (tag) {
        //    var index = $scope.columns.indexOf(tag);
        //    $scope.removeColumn(index);
        //}


        //$scope.removeColumn = function (index) {
        //    $scope.columns.splice(index, 1);
        //    // if no rows left in the array create a blank array
        //    if ($scope.columns.length() === 0 || $scope.columns.length() == null) {
        //        alert('no rec');
        //        $scope.columns.push = [{ "colId": "col1" }];
        //    }

        //}
    });
});