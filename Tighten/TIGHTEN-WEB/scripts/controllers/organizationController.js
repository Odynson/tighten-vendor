﻿define(['app', 'organizationFactory'], function (app) {

    app.controller("organizationController", function ($scope, $rootScope, $http, sharedService, toaster, apiURL, $location, organizationFactory) {

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.imgURL = apiURL.imageAddress;


        function showAllOrganizations() {

            $('#globalLoader').show();
            organizationFactory.getAllOrganizations(sharedService.getUserId())
            .success(function (response) {
                $scope.organizationObject = response.ResponseData;
                $('#globalLoader').hide();
            })
            .error(function (response) {
                toaster.error("Error", message[response.message]);
                $('#globalLoader').hide();
            })
        };


        $scope.goToCompanyProfile = function (companyId) {
            $location.path('/company/profile/' + companyId);
        };

        $scope.showCompanyDetails = function (item) {
            $('#globalLoader').show();
            //$scope.stretchCentreWindow = false;
            //$scope.hideRightWindow = false;

            organizationFactory.getOrganization(sharedService.getUserId(), item.CompanyId)
            .success(function (response) {

                $scope.organizationDescriptionObject = response.ResponseData;
                $scope.organizationDescriptionObject.CompanyName = item.CompanyName;
                $scope.organizationDescriptionObject.CompanyProfilePic = item.CompanyProfilePic;
                $scope.organizationDescriptionObject.TotalTime = item.TotalTime;
                $scope.organizationDescriptionObject.TotalAmount = item.TotalAmount;
                $scope.organizationDescriptionObject.CompanyAssignedOnDate = item.CompanyAssignedOnDate;
                $scope.organizationDescriptionObject.Role = item.Role;
                //$('#divOrganizationDescriptionSection').show();
                $('#divOrganizationDescriptionSection').modal('show');
                $('#globalLoader').hide();
            })
            .error(function (response) {
                toaster.error("Error", message[response.message]);
                $('#globalLoader').hide();
            })
        };


        $scope.CloseOrganizationDetail = function () {
            $('#divOrganizationDescriptionSection').modal('hide');
        }


        $scope.hideRightWindowFunction = function () {
            $scope.hideRightWindow = true;
            $scope.stretchCentreWindow = true;
        }


        angular.element(document).ready(function () {

            showAllOrganizations();
        });


    });

});