﻿
define(['app', 'invoiceFactory'], function (app) {

    app.controller("invoiceController", function ($scope, $rootScope, $timeout, $location, $stateParams, $http, sharedService, toaster, apiURL,stripSetting, invoiceFactory) {

        $scope.$emit('showMenu', []);
        $scope.imgURL = apiURL.imageAddress;
        $scope.Invoice = [];
        $scope.InvoiceList = [];
        $scope.AllTodosForPreviewObject = {};
        $scope.ProjectMember = {};
        $scope.isFreelancer = sharedService.getIsFreelancer();
        $scope.userCardDetail = {};
        $scope.allCardsForPaymentDetailObject = {};
        $scope.sectionName = "Invoice";
        $scope.Reject = {};
        //$scope.Permission = {};

        $scope.TaskStatus = {
            alltask: [
                { id: 0, task: 'All' },
                { id: 1, task: 'Approved' },
                { id: 2, task: 'Not-Processed' }
            ]
        };

        $scope.selectedStatus = '0';



        ///////////////////////*  region Stripe  Starts Here   */////////////////////////////

        //This is Publish Key For Stripe
        debugger;
        Stripe.setPublishableKey(stripSetting.stripKey);
  

        $scope.isSubsctriptionPaymentDone = false;



        $scope.PayNow = function (event, PaymentInfo) {
            debugger;
            // Request a token from Stripe:

            var isValid = $scope.checkPaymentValidation(event, PaymentInfo);
            if (isValid) {
                var $form = $('#payment-form');
                Stripe.card.createToken($form, stripeResponseHandler);
            }
        }


        function stripeResponseHandler(status, response) {
            debugger;
            // Grab the form:
            var $form = $('#payment-form');

            if (response.error) { // Problem!

                // Show the errors on the form:
                //$form.find('.payment-errors').text(response.error.message);
                toaster.error("Error", response.error.message);
                $form.find('.submit').prop('disabled', false); // Re-enable submission

            }
            else { // Token was created!

                // Get the token ID:
                var token = response.id;

                var data = { Token: response.id, Amount: $scope.Total };
                // calling method on server to complete the payment process
                subscriptionFactory.SubsctriptionPayment(data)
               .success(function (SubsctriptionPaymentData) {
                   if (SubsctriptionPaymentData.success) {
                       $scope.isSubsctriptionPaymentDone = true;
                       $scope.SubsctriptionPaymentStart();
                   }
               })
               .error(function () {
                   toaster.error("Error", "Some Error Occured !");
                   $('#globalLoader').hide();
               });


            }
        };


        /////////////*  region Stripe  End Here   */////////////////



        $scope.popup1 = {
            opened: false
        };
        $scope.popup2 = {
            opened: false
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.convertTo = function (arr, key, dayWise) {
            var groups = {};
            for (var i = 0; l = arr.length, i < l; i++) {
                var newkey = '';
                if (dayWise) {
                    var asfdf = arr[i][key];
                    //var weekday_value = $scope.weekdays[new Date(arr[i][key]).getDay()];
                    var date = new Date(arr[i][key]).toUTCString();
                    var newdate = date.substring(0, 16)
                    //arr[i][key] = [weekday_value] + ', ' + date;
                    newkey = newdate;
                }
                else {
                    //arr[i][key] = arr[i][key].toTimeString();
                    newkey = arr[i][key].toTimeString();
                }
                //groups[arr[i][key]] = groups[arr[i][key]] || [];
                //groups[arr[i][key]].push(arr[i]);
                groups[newkey] = groups[newkey] || [];
                groups[newkey].push(arr[i]);
            }
            return groups;
        };

        $scope.searchResult = function () {
            $('#globalLoader').show();
            var data = { "UserId": sharedService.getUserId(), "CompanyId": sharedService.getCompanyId(), "InvoiceStatus": $scope.selectedStatus, "FromDate": $scope.fromDate, "ToDate": $scope.toDate, "FreelancerUserId": $scope.selectedFreelancer, "RoleId": sharedService.getRoleId() };
            $scope.ShowInvoice(data)
        }

        $scope.FilterInvoiceByStatus = function (selectedStatus) {
            $('#globalLoader').show();
            var data = { "UserId": sharedService.getUserId(), "CompanyId": sharedService.getCompanyId(), "InvoiceStatus": $scope.selectedStatus, "FromDate": $scope.fromDate, "ToDate": $scope.toDate, "FreelancerUserId": $scope.selectedFreelancer, "RoleId": sharedService.getRoleId() };
            $scope.ShowInvoice(data)
        }

        $scope.getTotalAmountForPaidInvoice = function () {
            var total_Amount = 0;
            if ($scope.Invoice.list_Paid.length > 0) {
                for (var i = 0; i < $scope.Invoice.list_Paid.length; i++) {
                    var Inv = $scope.Invoice.list_Paid[i];
                    if (Inv.InvoiceApprovedStatus == true) {
                        total_Amount += (Inv.TotalAmount);
                    }
                }
            }
            return total_Amount;
        }

        $scope.getTotalAmountForNotPaidInvoice = function () {
            var total_Amount = 0;
            if ($scope.Invoice.list_NotPaid.length > 0) {
                for (var i = 0; i < $scope.Invoice.list_NotPaid.length; i++) {
                    var Inv = $scope.Invoice.list_NotPaid[i];
                    if (Inv.InvoiceApprovedStatus == false) {
                        total_Amount += (Inv.TotalAmount);
                    }
                }
            }
            return total_Amount;
        }

        $scope.getTotalAmountForRejectedInvoice = function () {
            var total_Amount = 0;
            if ($scope.Invoice.list_Rejected.length > 0) {
                for (var i = 0; i < $scope.Invoice.list_Rejected.length; i++) {
                    var Inv = $scope.Invoice.list_Rejected[i];
                    if (Inv.InvoiceApprovedStatus == true) {
                        total_Amount += (Inv.TotalAmount);
                    }
                }
            }
            return total_Amount;
        }

        $scope.ShowInvoice = function (data) {
            debugger;
            invoiceFactory.getAllInvoicesForUser(data)
            .success(function (result) {

                $scope.Invoice = result.ResponseData;
                invoiceFactory.getAllFreelancerForAdmin(sharedService.getCompanyId())
                 .success(function (result) {
                     $scope.Freelancer = result.ResponseData;
                     $('#globalLoader').hide();

                 })
                .error(function (result) {
                    $('#globalLoader').hide();
                    toaster.error("Error", message[response.message]);
                })
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message[response.message]);
            })
        };

        $scope.currentInvoiceIndex = -1;

        $scope.showInvoiceDetail = function (invoiceId, index, invoice) {
            $('#globalLoader').show();
            
            var data = { "invoiceId": invoiceId, "UserId": sharedService.getUserId(), "CompanyId": sharedService.getCompanyId() };
            $scope.currentInvoiceId = invoiceId
            $scope.currentInvoiceIndex = index;

            invoiceFactory.getInvoiceDetailForUser(data)
            .success(function (result) {
                
                $scope.AllTodosForPreviewObject = result.ResponseData;
                $scope.InvoicePaidStatusForPreview = invoice.IsPaid;
                $scope.InvoiceApprovedStatusForPreview = invoice.InvoiceApprovedStatus;
                $scope.InvoiceRejectStatusForPreview = invoice.IsRejected;
                $('#modalInvoicePreview').modal('show');
                $('#globalLoader').hide();
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message["error"]);
            })
        }

        //$scope.openApproveInvoice = function (InvoiceId, index) {
        //    $('#globalLoader').show();
        //    invoiceFactory.getCardDetailForPayment(sharedService.getUserId(),InvoiceId)
        //    .success(function (result) {
        //        $scope.userCardDetail = result.ResponseData;

        //        $scope.DataApproveInvoice = { "InvoiceId": InvoiceId, "UserId": sharedService.getUserId() };
        //        $scope.DataApproveInvoice.Index = index;
        //        $('#modalInvoicePreview').modal('hide');
        //        $('#modalStripePayment').modal('show');
        //        $('#globalLoader').hide();
        //    })
        //    .error(function () {
        //        $('#globalLoader').hide();
        //        toaster.error("Error", message["error"]);
        //    })


        //}




        $scope.openApproveInvoice = function (InvoiceId, index) {
            $('#globalLoader').show();
            invoiceFactory.getAllCardsForPaymentDetail(sharedService.getCompanyId())
            .success(function (resultAllCards) {
                $scope.allCardsForPaymentDetailObject.AllCards = resultAllCards.ResponseData;
                $scope.allCardsForPaymentDetailObject.InvoiceId = InvoiceId;
                $scope.allCardsForPaymentDetailObject.index = index;

                $('#modalInvoicePreview').modal('hide');
                $('#modalPaymentDetail').modal('show');
                $('#globalLoader').hide();
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message["error"]);
            })


        }


        $scope.selectPaymentDetail = function (InvoiceId, index) {
            $('#globalLoader').show();

            //var SelectedPaymentDetailId = 0;
            //angular.forEach($scope.allCardsForPaymentDetailObject.AllCards, function (obj) {
            //    if (obj.IsSelected == true) {
            //        SelectedPaymentDetailId = obj.Id;
            //    }
            //});

            invoiceFactory.getCardDetailForPayment(0, sharedService.getCompanyId(), InvoiceId)
            .success(function (result) {
                $scope.userCardDetail = result.ResponseData;

                $scope.DataApproveInvoice = { "InvoiceId": InvoiceId, "UserId": sharedService.getUserId() };
                $scope.DataApproveInvoice.Index = index;
                $('#modalInvoicePreview').modal('hide');
                $('#modalStripePayment').modal('show');
                $('#globalLoader').hide();
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message["error"]);
            })
        }



        $scope.clearAllCheckBoxes = function (accessid) {
            angular.forEach($scope.allCardsForPaymentDetailObject.AllCards, function (obj) {
                obj.IsSelected = false;
                if (obj.Id == accessid) {
                    obj.IsSelected = true;
                }
            });
        }


        $scope.approveInvoice = function () {
            $('#globalLoader').show();
            var $form = $('#payment-form');
            if ($scope.userCardDetail == null) {
                $('#globalLoader').hide();
                toaster.warning("Warning", "Please contact admin to set-up default Account");
                return;
            }
            //$form.find('.submit').prop('disabled', true);
            Stripe.card.createToken($form, stripeResponseHandler);
        }



        function stripeResponseHandler(status, response) {
            // Grab the form:
            var $form = $('#payment-form');
            if (response.error) {
                // Show the errors on the form:
                //$form.find('.payment-errors').text(response.error.message);
                toaster.error("Error", response.error.message);
                $('#globalLoader').hide();
            }
            else {
                // Token was created!

                // Get the token ID:
                $scope.DataApproveInvoice.Token = response.id;
                var data = $scope.DataApproveInvoice;
                var index = $scope.DataApproveInvoice.Index;

                invoiceFactory.approveInvoice(data)
                .success(function (response) {
                    $timeout(function () {
                        if (response.ResponseData == 'Payment successfull') {
                            toaster.success("Success", message.ApproveInvoiceSuccess);

                            //var asd = $scope.Invoice.list_NotApproved.splice([index], 1);
                            //asd[0].InvoiceApprovedStatus = true;
                            //$scope.Invoice.list_Approved.push(asd[0]);

                            var data = { "UserId": sharedService.getUserId(), "CompanyId": sharedService.getCompanyId(), "InvoiceStatus": $scope.selectedStatus, "FromDate": $scope.fromDate, "ToDate": $scope.toDate, "FreelancerUserId": $scope.selectedFreelancer, "RoleId": sharedService.getRoleId() };
                            $scope.ShowInvoice(data);
                            $('#modalStripePayment').modal('hide');
                            //$('#globalLoader').hide();
                        }
                        else {
                            $('#globalLoader').hide();
                            toaster.error("Error", message["error"]);
                            $('#modalInvoicePreview').modal('show');
                            $('#modalStripePayment').modal('hide');
                        }

                    }, 2000);
                })
                .error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message["error"]);
                    $('#modalInvoicePreview').modal('show');
                    $('#modalStripePayment').modal('hide');
                })


            }
        };

        $scope.onlyApproveInvoice = function (InvoiceId, index) {
            $('#globalLoader').show();
            var data = { 'InvoiceId': InvoiceId, 'UserId': sharedService.getUserId() };
            invoiceFactory.onlyApproveInvoice(data)
                .success(function (response) {
                    $('#modalInvoicePreview').modal('hide');
                    
                    $scope.Invoice.list_NotPaid[index].InvoiceApprovedStatus = true;
                    $('#globalLoader').hide();
                    toaster.success("Success", response.message);
                })
                .error(function (response) {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                })
        }

        
        $scope.openRejectInvoiceModal = function (InvoiceId, index) {
            
            $scope.Reject = { 'InvoiceId': InvoiceId, 'UserId': sharedService.getUserId() };
            $("input[name='Reason']").removeClass("inputError");
            $('#modalInvoicePreview').modal('hide');
            $('#modalInvoiceReject').modal('show');
        }


        $scope.rejectInvoice = function (IsReasonInvalid, addUpdateInvoiceForm) {

            if (IsReasonInvalid) {
                addUpdateInvoiceForm.Reason.$dirty = true;
                return;
            }

            $('#globalLoader').show();
            invoiceFactory.rejectInvoice($scope.Reject)
                .success(function (response) {
                    $('#modalInvoiceReject').modal('hide');
                    
                    // Load All Invoices again
                    var d = new Date();
                    d.setDate(d.getDate() - 7);
                    $scope.fromDate = d;
                    $scope.toDate = new Date();

                    var date = Date();
                    $scope.CurrentDate = date.substring(0, 3) + "," + date.substring(3, 15);

                    var data = { "UserId": sharedService.getUserId(), "CompanyId": sharedService.getCompanyId(), "InvoiceStatus": $scope.selectedStatus, "FromDate": $scope.fromDate, "ToDate": $scope.toDate, "FreelancerUserId": $scope.selectedFreelancer, "RoleId": sharedService.getRoleId() };
                    $scope.ShowInvoice(data);

                    toaster.success("Success", response.message);
                })
                .error(function (response) {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                })
        }

        $scope.showforwardInvoice = function (invoiceId) {
            $('#globalLoader').show();
            var data = { "invoiceId": invoiceId, "UserId": sharedService.getUserId() }
            invoiceFactory.getProjectMembersForUser(data)
            .success(function (response) {

                $scope.ProjectMembers = response.ResponseData;
                $scope.invoiceId = invoiceId;

                $("#modalForwardInvoice").modal("show");
                $('#globalLoader').hide();
            })
            .error(function (response) {
                $('#globalLoader').hide();
                toaster.error("Error", message[response.message]);
            })
        }

        $scope.forwardInvoice = function (ForwardInvoiceForm, isProjectMemberInvalid, invoiceId) {
            if (isProjectMemberInvalid) {
                ForwardInvoiceForm.ProjectMember.$dirty = true;
                return;
            }

            $('#globalLoader').show();
            var data = { "InvoiceId": invoiceId, "ForwardTo": $scope.ProjectMember.SelectedProjectMember, "UserId": sharedService.getUserId(), "CompanyId": sharedService.getCompanyId() }
            invoiceFactory.forwardInvoice(data)
            .success(function (data) {
                if (data != "") {

                    toaster.success("Success", message.ForwardInvoiceSuccess);
                    $("#modalForwardInvoice").modal("hide");
                    $('#globalLoader').hide();
                }
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message.error);
            })
        }

        $scope.FilterInvoiceByFreelancer = function (FreelancerUserId) {
            $('#globalLoader').show();
            var data = { "UserId": sharedService.getUserId(), "CompanyId": sharedService.getCompanyId(), "InvoiceStatus": $scope.selectedStatus, "FromDate": $scope.fromDate, "ToDate": $scope.toDate, "FreelancerUserId": $scope.selectedFreelancer, "RoleId": sharedService.getRoleId() };
            $scope.ShowInvoice(data)
        }

        $scope.CloseInvoicePreview = function () {
            $('#modalInvoicePreview').modal('hide');
        }

        angular.element(document).ready(function () {
            var d = new Date();
            d.setDate(d.getDate() - 7);
            $scope.fromDate = d;
            $scope.toDate = new Date();

            $('#globalLoader').show();
            var date = Date();
            $scope.CurrentDate = date.substring(0, 3) + "," + date.substring(3, 15);

            var data = { "UserId": sharedService.getUserId(), "CompanyId": sharedService.getCompanyId(), "InvoiceStatus": $scope.selectedStatus, "FromDate": $scope.fromDate, "ToDate": $scope.toDate, "FreelancerUserId": $scope.selectedFreelancer, "RoleId": sharedService.getRoleId() };
            $scope.ShowInvoice(data);

            //$scope.Permission = sharedService.getInternalPermissionInShared();
        })
    });
});

