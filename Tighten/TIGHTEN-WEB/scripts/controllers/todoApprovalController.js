﻿define(['app', 'todoApprovalFactory', 'utilityService'], function (app) {

    app.controller("todoApprovalController", function ($scope, $rootScope, $timeout, $location, $stateParams, $http, sharedService, toaster, apiURL, todoApprovalFactory, utilityService) {
        $scope.$emit('showMenu', []);
        $scope.imgURL = apiURL.imageAddress;
        $scope.TodosList = [];
        $scope.groupedTodoList = [];
        $scope.Reject = {};
        $scope.RejectObject = {};
        $scope.Assignees = [];




          $scope.TaskStatus = {
            alltask: [
                { id: 0, task: 'All' },
                { id: 1, task: 'Not Processed' },
                { id: 2, task: 'Approved' },
                { id: 3, task: 'Rejected' }
                
            ]
        };

        $scope.TaskType = {
            alltasktype: [
                {id:'1',type:'Phone Call'},
                { id: '2', type: 'Research & Development' },
                { id: '3', type: 'Meeting' },
                { id: '4', type: 'Admin Duties' },
                { id: '5', type: 'PMS Task' }
            ]
        };

        $scope.selectedTaskStatus = 'null';
        
        $scope.selectedTaskType = '5';
         


        $scope.showTodoFilteredByStatus = function (selectedTaskStatus) {
            $scope.groupedTodoList = [];
            search = { 'CompanyId': sharedService.getCompanyId(), 'AssigneeId': '', 'From': '', 'To': '', 'IsApproved': '', 'TaskStatus': $scope.selectedTaskStatus, 'TaskType': $scope.selectedTaskType };

            todoApprovalFactory.getAllTodos(search)
                   .success(function (data) {
                       $scope.TodosList = data.ResponseData;
                       angular.copy($scope.TodosList, $scope.groupedTodoList);
                       $scope.groupedTodoList = $scope.convertTo($scope.groupedTodoList, 'ModifiedDate', true);
                       $('#globalLoader').hide();
                       //getAssigneeList($scope.TodosList);
                       setTimeout(function () {
                           $scope.$apply();
                       }, 1000);
                   })
                   .error(function () {
                       $('#globalLoader').hide();
                       toaster.error("Error", "Some error occured");
                   });
        }


        $scope.showTodoFilteredByTaskType = function (selectedTaskType) {
            $scope.groupedTodoList = [];
            search = { 'CompanyId': sharedService.getCompanyId(), 'AssigneeId': '', 'From': '', 'To': '', 'IsApproved': '', 'TaskStatus': $scope.selectedTaskStatus, 'TaskType': $scope.selectedTaskType };
            //
            todoApprovalFactory.getAllTodos(search)
                   .success(function (data) {
                       $scope.TodosList = data.ResponseData;
                       angular.copy($scope.TodosList, $scope.groupedTodoList);
                       $scope.groupedTodoList = $scope.convertTo($scope.groupedTodoList, 'ModifiedDate', true);
                       $('#globalLoader').hide();
                       //getAssigneeList($scope.TodosList);
                       setTimeout(function () {
                           $scope.$apply();
                       }, 1000);
                   })
                   .error(function () {
                       $('#globalLoader').hide();
                       toaster.error("Error", "Some error occured");
                   });
        }

        $scope.getColor = function (approvedBy, isApproved) {
            if (approvedBy == null) {
                return ""
            }
            else if (isApproved) {
                return "green-border"
            }
            else {
                return "red-border";
            }
        }

        $scope.weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        $scope.convertTo = function (arr, key, dayWise) {
            var groups = {};
            for (var i = 0; l = arr.length, i < l; i++) {
                var newkey = '';
                if (dayWise) {
                    var weekday_value = $scope.weekdays[new Date(arr[i][key]).getDay()];
                    var date = new Date(arr[i][key]).toUTCString();
                    var newdate = date.substring(0, 16)
                    //arr[i][key] = [weekday_value] + ', ' + date;
                    newkey = newdate;
                }
                else {
                    //arr[i][key] = arr[i][key].toTimeString();
                    newkey = arr[i][key].toTimeString();
                }
                //groups[arr[i][key]] = groups[arr[i][key]] || [];
                //groups[arr[i][key]].push(arr[i]);
                groups[newkey] = groups[newkey] || [];
                groups[newkey].push(arr[i]);
            }
            return groups;
        };




        $scope.getDeveloper = function () {

        }
        // convert the minutes to time string
        $scope.ConvertMinutesToTimeString = function (minutes) {
            return utilityService.ConvertMinutesToFormatedTimeLogString(minutes);
        }
        // not using now
        var getAssigneeList = function (List) {

            List.forEach(function (column) {
                //
                var jsonData = {};
                var columnName = column.AssigneeName + " (" + column.AssigneeEmail + ")";
                jsonData.Name = columnName;
                jsonData.Value = column.AssigneeId;
                if ($scope.Assignees.indexOf(jsonData) === -1) {
                    $scope.Assignees.push(jsonData);
                }

            });
            //
        }

        $scope.approveTodo = function (key,parentIndex, todo, IsApproved) {
            $('#globalLoader').show();
            todo.IsApproved = true;
            search = { 'TodoId': todo.TodoId, 'ApproverId': sharedService.getUserId(), 'IsApproved': IsApproved };
            todoApprovalFactory.ApproveTodo(search)
                .success(function (result) {
                    if (result.ResponseData != '') {
                        //
                        $scope.groupedTodoList[key][parentIndex].ApprovedBy = sharedService.getUserId();
                        $scope.groupedTodoList[key][parentIndex].IsApproved = true;
                        toaster.success("Success", "Todo is approved");
                        $('#globalLoader').hide();
                    }
                })
                .error(function (error) {
                    $('#globalLoader').hide();
                    toaster.error("Error", "Some error occured");
                })
        }

        $scope.showRejectPopUp = function (key,parentIndex, todo, IsApproved) {
            $scope.RejectObject = todo;
            $scope.RejectObject.parentIndex = parentIndex;
            $scope.RejectObject.key = key;
            $scope.Reject = { 'TodoId': todo.TodoId, 'ApproverId': sharedService.getUserId(), 'IsApproved': IsApproved };
            $('#modalTodoReject').modal('show');
        }
        $scope.rejectTodo = function () {
            
            $('#globalLoader').show();
            $scope.RejectObject.IsApproved = false;
            todoApprovalFactory.ApproveTodo($scope.Reject)
                .success(function (result) {
                    if (result.ResponseData != '') {
                        $scope.groupedTodoList[$scope.RejectObject.key][$scope.RejectObject.parentIndex].ApprovedBy = sharedService.getUserId();
                        $scope.groupedTodoList[$scope.RejectObject.key][$scope.RejectObject.parentIndex].IsApproved = false;
                        $scope.groupedTodoList[$scope.RejectObject.key][$scope.RejectObject.parentIndex].Reason = $scope.Reject.Reason;
                        toaster.success("Success", "Todo is rejected");
                        $('#globalLoader').hide();
                    }
                    $('#modalTodoReject').modal('hide')
                })
                .error(function (error) {
                    $('#globalLoader').hide();
                    toaster.error("Error", "Some error occured");
                })
        }
        $scope.searchResult = function () {
            $('#globalLoader').show();
            var assignee = "";
            if ($scope.selectedAssignee)
                assignee = $scope.selectedAssignee.Value;

            search = { 'CompanyId': sharedService.getCompanyId(), 'AssigneeId': assignee, 'From': $scope.fromDate, 'To': $scope.toDate, 'IsApproved': '', 'RoleId': sharedService.getRoleId(), 'UserId': sharedService.getUserId() };
            
            // Get data first time with default filter
            todoApprovalFactory.getAllTodos(search)
                .success(function (data) {
                    $scope.TodosList = data.ResponseData;
                    $scope.groupedTodoList = [];
                    angular.copy($scope.TodosList, $scope.groupedTodoList);
                    $scope.groupedTodoList = $scope.convertTo($scope.groupedTodoList, 'ModifiedDate', true);
                    $('#globalLoader').hide();
                    //getAssigneeList($scope.TodosList);
                    setTimeout(function () {
                        $scope.$apply();
                    }, 1000);
                })
                .error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", "Some error occured");
                });
            /////////
        }

        ////////////Date Functions/////////////
        /************************/
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };
        var da = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
        };

        $scope.toggleMin();

        $scope.maxDate = new Date(da.getFullYear(), 5, 22);

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.open3 = function () {
            $scope.popup3.opened = true;
        };

        $scope.setDate = function (year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
        //$scope.init = new Date(da.getFullYear()-10, 5, 22);
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,

        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.popup3 = {
            opened: false
        };

        $scope.getDayClass = function (date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        };
        /***********************/

        // Page load
        angular.element(document).ready(function () {
            $('#globalLoader').show();
            var d = new Date();
            d.setDate(d.getDate() - 7);
            $scope.fromDate = d;// d.setDate(d.getDate() - 7);
            $scope.toDate = new Date();
            
            search = { 'CompanyId': sharedService.getCompanyId(), 'AssigneeId': '', 'From': '', 'To': '', 'IsApproved': '', 'RoleId': sharedService.getRoleId(), 'UserId': sharedService.getUserId() };
            todoApprovalFactory.getCompanyUsers(sharedService.getCompanyId(), sharedService.getRoleId(), sharedService.getUserId())
            .success(function (result) {
                // getting the list of the company users
                $scope.Assignees = result.ResponseData;

                // Get data first time with default filter
                todoApprovalFactory.getAllTodos(search)
                    .success(function (data) {
                        $scope.TodosList = data.ResponseData;
                        angular.copy($scope.TodosList, $scope.groupedTodoList);
                        $scope.groupedTodoList = $scope.convertTo($scope.groupedTodoList, 'ModifiedDate', true);
                        $('#globalLoader').hide();
                        //getAssigneeList($scope.TodosList);
                        setTimeout(function () {
                            $scope.$apply();
                        }, 1000);
                    })
                    .error(function () {
                        $('#globalLoader').hide();
                        toaster.error("Error", "Some error occured");
                    });
                /////////
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", "Some error occured");
            })



        });

});
    })