﻿
define(['app'], function (app) {
    app.controller("addNewWorkLogEntryController", function ($scope, $rootScope, $stateParams, apiURL, $timeout, $http, $location, sharedService, $confirm, toaster, worklogFactory, projectsFactory, $window) {
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();

        $scope.imgURL = apiURL.imageAddress;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }
        $scope.billableDropList = [
            { text: "Billable", value: "B" },
            { text: "Not-Billable", value: "NB" }
        ]
        $scope.worklogList = [];
        $scope.approvedProjectList = [];
        $scope.phaseMilestoneList = [];
        $scope.selectProjectObj = {};
        $scope.selectMilestoneObj = {};
        $scope.AddNewWorkLogModel = {};
        $scope.AddNewWorkLogModel.WorkLogId = $stateParams.pid;
        $scope.popup = {
            opened1: false
        };
        $scope.selectedProjectChanged = function (ProjectObj) {
            debugger;
            //alert(ProjectObj.ProjectId);
            $scope.AddNewWorkLogModel.ProjectId = ProjectObj.ProjectId;
            $('#globalLoader').show();
            projectsFactory.getProjectMilestonesByProjectId(ProjectObj.ProjectId)
            .success(function (data) {
                $scope.phaseMilestoneList = [];
                
                debugger;
                if (data.ResponseData != null) {

                        $scope.phaseMilestoneList = data.ResponseData;
                   
                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }

        $scope.selectedMilestoneChanged = function (MilestoneObj) {
            debugger;
            $('#globalLoader').show();
            //alert(ProjectObj.ProjectId);
            $scope.AddNewWorkLogModel.PhaseId = MilestoneObj.PhaseId;
            $scope.AddNewWorkLogModel.PhaseMilestoneId = MilestoneObj.MilestoneId;
            $('#globalLoader').hide();
            
        }
        $scope.selectedBillableChanged = function (BillableObj) {
            debugger;
            $('#globalLoader').show();
            //alert(ProjectObj.ProjectId);
            $scope.AddNewWorkLogModel.BillableStatus = BillableObj.text;
            $('#globalLoader').hide();

        }
        $scope.getWorkLogList = function (UserId) {
            $('#globalLoader').show();
            worklogFactory.getWorkLogList(UserId)
            .success(function (data) {

                debugger;
                if (data.ResponseData != null) {
                    if (data.ResponseData.length > 0) {
                        $scope.worklogList = data.ResponseData[0];
                    }
                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }

        $scope.getApprovedProjectByUserId = function (UserId) {
            $('#globalLoader').show();
            projectsFactory.getApprovedProjectByUserId(UserId)
            .success(function (data) {

                debugger;
                if (data.ResponseData != null) {
                        $scope.approvedProjectList = data.ResponseData;
                    
                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }

        $scope.saveWorkLogEntry = function (IsTimeSpent, IsCreatedDate, NewWorkLogForm, AddNewWorkLogModel) {
            if (IsTimeSpent || IsCreatedDate ) {
                NewWorkLogForm.timeSpent.$dirty = true;
                NewWorkLogForm.createdDate.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                debugger;
                //alert(moment(NewWorkLogForm.createdOn.$viewValue).format("DD/MM/YYYY"));
                var model = {};
                model.Description = $scope.AddNewWorkLogModel.Description;
                model.TimeSpent = $scope.AddNewWorkLogModel.TimeSpent;
                model.CreatedDate = moment(NewWorkLogForm.createdDate.$viewValue).format("DD/MM/YYYY");
                model.WorkLogId = $scope.AddNewWorkLogModel.WorkLogId;
                //data.CreateOn = moment(NewWorkLogForm.createdOn.$viewValue).format("MM/DD/YYYY");
                model.CreatedUserId = UserId;
                model.IsDeleted = false;
                worklogFactory.saveWorkLogEntry(model)
                     .success(function (data) {
                         debugger;
                         if (data.success == true) {
                             toaster.success("Success", data.ResponseData);
                             $('#globalLoader').hide();

                             NewWorkLogForm.timeSpent.$dirty = false;
                             NewWorkLogForm.createdDate.$dirty = false;
                             $window.location.href = ('/#/Worklogentries/' + $scope.AddNewWorkLogModel.WorkLogId);
                         }
                         else {
                             toaster.warning("Warning", "Some Error Occured!");
                             $('#globalLoader').hide();
                         }
                     })
                     .error(function (data) {
                         toaster.error("Error", "Some Error Occured!");
                         $('#globalLoader').hide();
                     })
            }
        }
        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
                $rootScope.activeTeam = [];
                $scope.format = 'MM/dd/yyyy';
            }
            //$scope.getWorkLogList(UserId);
            $scope.getApprovedProjectByUserId(UserId);
        });


        $scope.open1 = function () {
            debugger;
            $scope.popup.opened1 = true;
            $scope.format = 'MM/dd/yyyy';
            $scope.mindate = new Date();
            
        };




    });
});