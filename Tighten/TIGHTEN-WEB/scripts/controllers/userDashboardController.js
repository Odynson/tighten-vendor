﻿define(['app', 'userDashboardFactory', 'clientFeedbackFactory', 'usersFactory', 'accountFactory', 'subscriptionFactory', 'transactionsFactory'], function (app) {
    app.controller("userDashboardController", function ($scope, $rootScope, $cookieStore, apiURL,stripSetting, $timeout, $filter, $http, $location, sharedService, toaster, userDashboardFactory, clientFeedbackFactory, usersFactory, accountFactory, subscriptionFactory, transactionsFactory, invoiceFactory, setPasswordFactory) {
        $scope.imgURL = apiURL.imageAddress;

        $scope.$emit('showMenu', []);

        // closeCentreSections();
        $scope.showCenterLoader = true;
        $scope.CompanyObject = {};
        $rootScope.CompanyObject = {};
        $rootScope.TeamsObject = {};
        getUserDetail();
        $scope.cardType = "";
        $scope.PackageObject = {};
        $scope.PaymentData = {};
        $scope.allCardsForPaymentDetailObject = {};
        $scope.sectionName = "UserDashboard";
        

        $scope.teamCountDirectiveTitle = "Number Of Teams";
        $scope.projectCountDirectiveTitle = "Number Of Projects";
        $scope.todoCountDirectiveTitle = "Number Of Todos";
        $scope.pendingtodosCountDirectiveTitle = "Pending Todos";
        $scope.expiredduedatetodosCountDirectiveTitle = "Todos With Expired Due-Date";
        $scope.comingtodosCountDirectiveTitle = "Coming Todos In Next Week";
        $scope.overbudgetedprojectsCountDirectiveTitle = "Over Budgeted Projects";
        $scope.recentfreelancershiredCountDirectiveTitle = "Recent Freelancers Hired";
        $scope.top5tasksCountDirectiveTitle = "Top Five Tasks";

        $scope.refreshInterval = 300000;
        $scope.LoggedInFirstTimeByLinkedIn = {};



       

        var setPasswordObj = {};

        $scope.saveMypassword = function () {
            
                $('#globalLoader').show();
                
                if ($scope.LoggedInFirstTimeByLinkedIn.Password != '' && ($scope.LoggedInFirstTimeByLinkedIn.Password == $scope.LoggedInFirstTimeByLinkedIn.ConfirmPassword)) {
                    setPasswordObj.NewPassword = $scope.LoggedInFirstTimeByLinkedIn.Password;
                    setPasswordObj.UserId = sharedService.getUserId();
                    setPasswordObj.EmailId = sharedService.getEmailId();
                    checkEmailConfirmation(setPasswordObj);
                }
                else if ($scope.LoggedInFirstTimeByLinkedIn.Password == '') {
                 
                    toaster.warning("Warning", message["emptyPasswordField"]);
                    $('#globalLoader').hide();
                }
                else if ($scope.LoggedInFirstTimeByLinkedIn.Password != $scope.LoggedInFirstTimeByLinkedIn.ConfirmPassword) {
                    toaster.warning("Warning", message["passwordAndConfirmPasswordNotMatched"]);
                    $('#globalLoader').hide();
                }

        }


        // getting the parameters from the url
        var checkEmailConfirmation = function (setPasswordObj) {
            // email confirmtion service to save the user detail
            
            setPasswordFactory.setPasswordFromUserProfile(setPasswordObj, function (response) {
                
                toaster.success("Success", response.message);
                $("#modalLinkedInMessage").modal('hide');
                $('#globalLoader').hide();
                
            });
        };



        /*********************************  region Stripe  Starts Here   *********************************/

        //This is Publish Key For Stripe

        //Stripe.setPublishableKey('pk_test_vlfopRnE9Tff7fQBrEPagmeR');

        Stripe.setPublishableKey(stripSetting.stripKey);
        $scope.isSubsctriptionPaymentDone = false;



        $scope.PayNow = function () {
            $('#globalLoader').show();
            var $form = $('#payment-form');
            if ($scope.userCardDetail == null)
            {
                toaster.warning("Warning", "Please contact admin to set-up default Account");
                $('#globalLoader').hide();
                return false;
            }
            //$form.find('.submit').prop('disabled', true);
            Stripe.card.createToken($form, stripeResponseHandler);
        }


        function stripeResponseHandler(status, response) {
            // Grab the form:
            var $form = $('#payment-form');
            if (response.error) {
                // Show the errors on the form:
                //$form.find('.payment-errors').text(response.error.message);
                toaster.error("Error", response.error.message);
                $('#globalLoader').hide();
            }
            else {
                // Token was created!
                // Get the token ID:

                var data = { Token: response.id, Amount: $scope.Total, CompanyId: sharedService.getCompanyId() };
                // calling method on server to complete the payment process
                subscriptionFactory.SubsctriptionPayment(data)
               .success(function (SubsctriptionPaymentData) {
                   if (SubsctriptionPaymentData.success) {
                       $scope.isSubsctriptionPaymentDone = true;
                       $scope.SubsctriptionPaymentStart(SubsctriptionPaymentData.ResponseData);
                   }
               })
               .error(function () {
                   debugger;
                   toaster.error("Error", message["error"]);

                   $('#globalLoader').hide();
               });
            }
        };


        $scope.SubsctriptionPaymentStart = function (chargeId) {
            
            //var isValid = $scope.checkPaymentValidation(event, PaymentInfo);
            //if (isValid) {
            if ($scope.isSubsctriptionPaymentDone) {
                debugger;
                $('#modalInvoicePackage').modal('hide');
                //$('#globalLoader').show();
                $timeout(function () {
                    $scope.PaymentData.PackageId = $scope.PackageId;
                    $scope.PaymentData.CompanyId = sharedService.getCompanyId();
                    $scope.PaymentData.UserId = sharedService.getUserId();
                    var subscription = $scope.PaymentData
                    subscriptionFactory.insertSubscription(subscription)
                    
                        .success(function (dataSub) {
                            debugger;
                    var transactions = { 'EventID': dataSub.ResponseData, 'AmountPaid': $scope.Total, 'StirpeTransactionID': chargeId, 'TransactionBy': sharedService.getUserId(), 'TransactionType': 'Subscription', 'TransactionFor': sharedService.getUserId() }
                            transactionsFactory.insertTransaction(transactions)
                                .success(function (dataTran) {

                                    var obj = $cookieStore.get('sharedService')
                                    obj.IsSubscriptionsEnds = false
                                    $cookieStore.remove('sharedService');
                                    $cookieStore.put('sharedService', obj)
                                    $scope.transctionId = dataTran.ResponseData;
                                    $('#modalThankYou').modal('show');
                                    $('#globalLoader').hide();
                                });
                        });
                }, 5000);
            }
            else {
                $('#globalLoader').hide();
            }
            //}
        }




        /****************************  region Stripe  End Here   ************************/



        $scope.closePopUp = function () {
            $location.path('/dashboard/u');
        }



        function getUserDetail() {

            $scope.LoginUserRole = sharedService.getRoleId();
            function hideloader() {
                //closeCentreSections();
                //$('#divMasterLoader').hide();
                $scope.selectedNavigation = "MyDashboard";
                //$('#divMyDashboard').show();
                $scope.setOverFlowY = "auto !important";
            };

            /*This method is Comment Because some error throwing we ll resolve it
              12-Dec-17  for demo purpose  
            
            */



            //userDashboardFactory.getUserUpcomingTodos(sharedService.getUserId()).success(function (data) {
            //    if (data.success) {
            //        $scope.userUpcomingTodosObject = data.ResponseData;
            //        /* Getting User Upcoming Events */
            //        userDashboardFactory.getUserUpcomingEvents(sharedService.getUserId()).success(function (data) {
            //            if (data.success) {
            //                $scope.userUpcomingEventsObject = data.ResponseData;
            //                /* Getting User T Line */
            //                userDashboardFactory.getUserTLine(sharedService.getUserId()).success(function (data) {
            //                    if (data.success) {
            //                        $scope.userTLineObject = data.ResponseData;
            //                        /* Getting Client Feedbacks associated with the User  */
            //                        clientFeedbackFactory.getUserClientFeedbacks(sharedService.getUserId()).success(function (data) {
            //                            if (data.success) {
            //                                $scope.userClientFeedbacksObject = data.ResponseData;
            //                                $timeout(function () {
            //                                    $scope.$apply();
            //                                    $timeout(hideloader, 1000);
            //                                });
            //                            }
            //                            else {
            //                                debugger;
            //                                toaster.error("Error", data.message);
            //                            }
            //                        }).error(function () {
            //                            debugger;
            //                            toaster.error("Error", "Some Error Occured !");
            //                        });
            //                    }
            //                    else {
            //                        debugger;
            //                        toaster.error("Error", data.message);
            //                    }
            //                }).error(function () {
            //                    debugger;
            //                    toaster.error("Error", "Some Error Occured !");
            //                });
            //            }
            //            else {
            //                debugger;
            //                toaster.error("Error", data.message);
            //            }
            //        }).error(function () {
            //            debugger;
            //            toaster.error("Error", "Some Error Occured !");
            //        });
            //    }
            //    else {
            //        debugger;
            //        toaster.error("Error", data.message);
            //    }
            //}).error(function () {
            //    debugger;
            //    toaster.error("Error", "Some Error Occured !");
            //});

        }

        $scope.changeCardType = function () {
            if ($scope.PaymentData.CreditCard !== "") {
                if ($scope.PaymentData.CreditCard.startsWith("3")) {
                    $scope.cardType = "fa fa-cc-amex";
                }
                else if ($scope.PaymentData.CreditCard.startsWith("4")) {
                    $scope.cardType = "fa fa-cc-visa";
                }
                else if ($scope.PaymentData.CreditCard.startsWith("5")) {
                    $scope.cardType = "fa fa-cc-mastercard";
                }
                else if ($scope.PaymentData.CreditCard.startsWith("6")) {
                    $scope.cardType = "fa fa-cc-discover";
                }
                else {
                    $scope.cardType = "";
                }
            }
            else {
                $scope.cardType = "";
            }
        };

        $scope.showTodoDescription = function (todoObject) {
            $scope.hideRightWindow = false;
            //$scope.stretchClientDashboardTodoPanel = false;

            $scope.todoDescriptionObject = todoObject;
            $scope.hideClientDashboardTodoDescriptionPanel = false;

        }

        
        $scope.clearAllCheckBoxesUpgradePackage = function (accessid) {
            angular.forEach($scope.AllPackageObject, function (obj) {
                obj.IsActive = false;
                if (obj.Id == accessid) {
                    obj.IsActive = true;
                }
            });
        }

        $scope.filter = function (homes /*, thisp*/) {
            var res = [];
            $.map(homes, function (val, key) {
                if (val.Id > 1)
                    res.push(val);
            });
            return res;
        };

        $scope.PackageId = 0;
        $scope.CalculatePackageCost = [];
        $scope.openUpgradePackageModal = function () {
            debugger;
            $('#globalLoader').show();
            var UserId = sharedService.getUserId();
            userDashboardFactory.getAllPackages(sharedService.getCompanyId())
                .success(function (data) {
                    debugger;
                    data.ResponseData = $scope.filter(data.ResponseData)
                    $scope.AllPackageObject = data.ResponseData;
                    $('#modalUpgradePackage').modal('show');
                    $('#globalLoader').hide();
                })
                .error(function () {
                    debugger;
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                });
        }

        $scope.upgradePackage = function () {
            $('#modalUpgradePackage').modal('hide');
            $('#globalLoader').show();
            var SelectedPackage, Price, PackageName
            angular.forEach($scope.AllPackageObject, function (obj) {
                if (obj.IsActive == true) {
                    SelectedPackage = obj.Id;
                    Price = obj.Price;
                    PackageName = obj.Name;
                }
            });
            $scope.PackageId = SelectedPackage;
            $scope.PackageCostPerUser = [];
            $scope.CalculatePackageCost = [];
            usersFactory.getActiveUsersCount(sharedService.getCompanyId()).success(function (data) {
                
                var UsersCount = data.ResponseData;
                var Total = 0
                var Total = parseInt(UsersCount) * parseInt(Price)
                var PackageCost = { PackageId: SelectedPackage, Item: PackageName, UsersCount: UsersCount, PricePerUser: Price, SubTotal: Total }
                $scope.CalculatePackageCost.push(PackageCost);
                $scope.Total = Total;
                $('#modalCalculatePackageCost').modal('show');
                $('#globalLoader').hide();
            });
            //usersFactory.getUsers(sharedService.getUserId(), false, sharedService.getCompanyId())// here false is used for chkEmailConfirmed
            //    .success(function (data) {

            //        if (data.success) {
            //            $scope.UsersObject = data.ResponseData;
            //            $scope.filteredFreelancersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: true }, true);
            //            $scope.filteredUsersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: false }, true);

            //            var Total = 0
            //            angular.forEach($scope.filteredUsersObject, function (obj) {

            //                if (obj.IsEmailConfirmed == true) {
            //                    var PackageCost = { ProfilePhoto: 'http://localhost:55580/' + obj.ProfilePhoto, UserName: obj.FirstName + ' ' + obj.LastName, PricePerUser: Price }
            //                    $scope.PackageCostPerUser.push(PackageCost);
            //                }
            //            });
            //            var UsersCount = $scope.filteredUsersObject.length;
            //            var Total = 0
            //            var Total = parseInt(UsersCount) * parseInt(Price)
            //            var PackageCost = { PackageId: SelectedPackage, Item: PackageName, UsersCount: UsersCount, PricePerUser: Price, SubTotal: Total }
            //            $scope.CalculatePackageCost.push(PackageCost);
            //            var Total = parseInt(UsersCount) * parseInt(Price)
            //            $scope.Total = Total;
            //            $('#modalCalculatePackageCost').modal('show');
            //            $('#globalLoader').hide();
            //        }
            //    });
        }

        $scope.proceedToPayment = function () {
            $('#modalCalculatePackageCost').modal('hide');
            $('#globalLoader').show();
            var SelectedPackage, Price, PackageName
            angular.forEach($scope.AllPackageObject, function (obj) {
                if (obj.IsActive == true) {
                    SelectedPackage = obj.Id;
                    Price = obj.Price;
                    PackageName = obj.Name;
                }
            });
            $scope.PackageId = SelectedPackage;

            //invoiceFactory.getAllCardsForPaymentDetail(sharedService.getCompanyId())
            //.success(function (resultAllCards) {
            //    $scope.allCardsForPaymentDetailObject.AllCards = resultAllCards.ResponseData;
            //    $scope.allCardsForPaymentDetailObject.InvoiceId = "";
            //    $scope.allCardsForPaymentDetailObject.index = "";

            //    $('#modalPaymentDetail').modal('show');
            //    $('#globalLoader').hide();
            //})
            //.error(function () {
            //    $('#globalLoader').hide();
            //    toaster.error("Error", message["error"]);
            //})



            invoiceFactory.getCardDetailForPayment(0, sharedService.getCompanyId(), 0)
            .success(function (result) {
                
                $scope.userCardDetail = result.ResponseData;

                //$('#modalPaymentDetail').modal('hide');
                $('#modalInvoicePackage').modal('show');
                $('#globalLoader').hide();
            })
            .error(function () {
                $('#globalLoader').hide();
                debugger;
                toaster.error("Error", message["error"]);
            })

        }



        $scope.selectPaymentDetail = function (InvoiceId, index) {
            $('#globalLoader').show();

            var SelectedPaymentDetailId = 0;
            angular.forEach($scope.allCardsForPaymentDetailObject.AllCards, function (obj) {
                if (obj.IsSelected == true) {
                    SelectedPaymentDetailId = obj.Id;
                }
            });

            if (SelectedPaymentDetailId == 0) {
                toaster.warning("Warning", "Please select any of the payment detail option");
                $('#globalLoader').hide();
                return;
            }
            invoiceFactory.getCardDetailForPayment(SelectedPaymentDetailId, sharedService.getCompanyId(), 0)
            .success(function (result) {
                $scope.userCardDetail = result.ResponseData;


                $('#modalPaymentDetail').modal('hide');
                $('#modalInvoicePackage').modal('show');
                $('#globalLoader').hide();
            })
            .error(function () {
                debugger;
                $('#globalLoader').hide();
                toaster.error("Error", message["error"]);
            })
        }



        $scope.clearAllCheckBoxes = function (accessid) {
            angular.forEach($scope.allCardsForPaymentDetailObject.AllCards, function (obj) {
                obj.IsSelected = false;
                if (obj.Id == accessid) {
                    obj.IsSelected = true;
                }
            });
        }


        


        //$scope.PayNow = function (event, PaymentInfo) {
        //    
        //    var isValid = $scope.checkPaymentValidation(event, PaymentInfo);
        //    if (isValid) {
        //        $('#modalInvoicePackage').modal('hide');
        //        $('#globalLoader').show();
        //        $timeout(function () {
        //            $scope.PaymentData.PackageId = $scope.PackageId;
        //            $scope.PaymentData.CompanyId = sharedService.getCompanyId();
        //            $scope.PaymentData.UserId = sharedService.getUserId();
        //            var subscription = $scope.PaymentData
        //            subscriptionFactory.insertSubscription(subscription)
        //                .success(function (dataSub) {
        //                    var transactions = { 'SubscriptionID': dataSub.ResponseData, 'AmountPaid': $scope.Total, 'TransactionID': $scope.getRandomSpan() }
        //                    transactionsFactory.insertTransaction(transactions)
        //                        .success(function (dataTran) {
        //                            
        //                            var obj = $cookieStore.get('sharedService')
        //                            obj.IsSubscriptionsEnds = false
        //                            $cookieStore.remove('sharedService');
        //                            $cookieStore.put('sharedService', obj)
        //                            $scope.transctionId = transactions.TransactionID;
        //                            $('#modalThankYou').modal('show');
        //                            $('#globalLoader').hide();
        //                        });
        //                });
        //        }, 5000);
        //    }
        //}

        $scope.checkPaymentValidation = function (event, PaymentInfo) {

            var isvalid = true;
            if (PaymentInfo.CreditCard != undefined) {
                if (PaymentInfo.CreditCard.$invalid) {
                    PaymentInfo.CreditCard.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide Credit Card Number';
                    //return;
                }
            }
            if (PaymentInfo.ExpiryMonth != undefined) {
                if (PaymentInfo.ExpiryMonth.$invalid) {
                    PaymentInfo.ExpiryMonth.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide Expiry Month';
                    //return;
                }
            }
            if (PaymentInfo.ExpiryYear != undefined) {
                if (PaymentInfo.ExpiryYear.$invalid) {
                    PaymentInfo.ExpiryYear.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide Expiry Year';
                    //return;
                }
            }
            if (PaymentInfo.CvvNumber != undefined) {
                if (PaymentInfo.CvvNumber.$invalid) {
                    PaymentInfo.CvvNumber.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide Cvv Number';
                    //return;
                }
            }
            if (PaymentInfo.CardHolderName != undefined) {
                if (PaymentInfo.CardHolderName.$invalid) {
                    PaymentInfo.CardHolderName.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide CardHolder Name';
                    //return;
                }
            }

            return isvalid;
        };

        $scope.cancelUpgradePackage = function () {
            $('#modalUpgradePackage').modal('hide');
            $('#modalCalculatePackageCost').modal('hide');
            $('#modalInvoicePackage').modal('hide');
            $('#modalThankYou').modal('hide');
        }

        $scope.getYearsList = function (range) {
            var currentYear = new Date().getFullYear();
            var years = [];
            for (var i = 0; i < range; i++) {
                years.push(currentYear + i);
            }
            return years;
        };

        $scope.getRandomSpan = function () {
            //return Math.floor((Math.random() * 6) + 1);
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
            }
            return s4() + '-' + s4()
            //return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        };



        angular.element(document).ready(function () {
            
            var isloggedinfirsttimebylinkedin = sharedService.getIsLoggedInFirstTimeByLinkedIn();
            //var UserId = sharedService.getUserId();
            if (isloggedinfirsttimebylinkedin) {
                $scope.isLinkedInUsed = true;
                $scope.LoggedInFirstTimeByLinkedIn.EmailId = sharedService.getEmailId();
                $timeout(function () {
                    $('#modalLinkedInMessage').modal('show');
                }, 3000)
            }

        });


        

        //    $('#globalLoader').show();
        //    var SelectedPackage
        //    angular.forEach($scope.AllPackageObject, function (obj) {
        //        if (obj.IsActive == true) {
        //            SelectedPackage = obj.Id;
        //        }
        //    });
        //    if (SelectedPackage == undefined) {
        //        toaster.warning("Warning", "Please select Package to proceed");
        //        $('#globalLoader').hide();
        //        return;
        //    }

        //    $scope.PackageObject.UserId = sharedService.getUserId();
        //    $scope.PackageObject.PackageId = SelectedPackage;
        //    $scope.PackageObject.CompanyId = sharedService.getCompanyId();
        //    userDashboardFactory.upgradePackage($scope.PackageObject)
        //        .success(function (data) {
        //            toaster.success("Success", data.ResponseData);
        //            $('#globalLoader').hide();
        //            $('#modalUpgradePackage').modal('hide');
        //        })
        //        .error(function () {
        //            $('#globalLoader').hide();
        //            toaster.error("Error", "Some Error Occured !");
        //        });
        //}

        //function getUserDetail() {
        //    
        //    function hideloader() {
        //        //closeCentreSections();
        //        //$('#divMasterLoader').hide();
        //        $scope.selectedNavigation = "MyDashboard";
        //        //$('#divMyDashboard').show();
        //        $scope.setOverFlowY = "auto !important";
        //    };
        //    /*  Fetching User Details */
        //    usersFactory.getUser(sharedService.getUserId())
        //               .success(function (data) {
        //                   
        //                   if (data.success) {
        //                       //$scope.NotifyCount = data.ResponseData.NotificationCount == 0 ? null : data.ResponseData.NotificationCount;
        //                       //$scope.InboxCount = data.ResponseData.inboxCount == 0 ? null : data.ResponseData.inboxCount;
        //                       $scope.hideNotifyCount = data.ResponseData.NotificationCount == undefined ? 0 : data.ResponseData.NotificationCount == 0 ? false : true;
        //                       $scope.hideInboxCount = data.ResponseData.inboxCount == undefined ? 0 : data.ResponseData.inboxCount == 0 ? false : true;

        //                       $rootScope.NotificationCount = data.ResponseData.NotificationCount == undefined ? 0 : data.ResponseData.NotificationCount == null ? 0 : data.ResponseData.NotificationCount;
        //                       $rootScope.InboxCount = data.ResponseData.InboxCount == undefined ? 0 : data.ResponseData.InboxCount == null ? 0 : data.ResponseData.InboxCount;

        //                       // adding to the shared service
        //                       sharedService.setNotificationCount($rootScope.NotificationCount);
        //                       sharedService.setInboxCount($rootScope.InboxCount);

        //                       $scope.CompanyObject.companyId = data.ResponseData.CompanyId;
        //                       $scope.CompanyObject.companyName = data.ResponseData.CompanyName;

        //                       $rootScope.CompanyObject.companyId = data.ResponseData.CompanyId;
        //                       $rootScope.CompanyObject.companyName = data.ResponseData.CompanyName;uy

        //                       $scope.IsAdminLoggedIn = data.ResponseData.IsAdminLoggedIn;
        //                       $scope.SendNotification = data.ResponseData.SendNotificationEmail;

        //                       $scope.MyProfileObject = data.ResponseData;
        //                       $rootScope.MyProfileObject = data.ResponseData;

        //                       $scope.teamName = data.ResponseData.teamName;
        //                       $scope.TeamId = data.ResponseData.TeamId;
        //                       $timeout(function () {
        //                           $scope.$apply();
        //                           /* Fetching User Teams  */
        //                           teamFactory.getLoggedInUserTeams(sharedService.getUserId()).success(function (data) {
        //                               
        //                               if (data.success) {
        //                                   $scope.TeamsObject = data.ResponseData;
        //                                   $scope.activeTeam = $scope.TeamsObject[0];

        //                                   $rootScope.activeTeam = $scope.activeTeam;
        //                                   $rootScope.TeamsObject = $scope.TeamsObject;
        //                                   $timeout(function () {
        //                                       $scope.$apply();
        //                                       /* Getting User Projects for Selected Team */
        //                                       projectsFactory.getProjects(sharedService.getUserId(), $scope.activeTeam.TeamId).success(function (data) {
        //                                           
        //                                           if (data.success) {
        //                                               $scope.projects = data.ResponseData;
        //                                               /*Getting User Upcoming Todos*/
        //                                               userDashboardFactory.getUserUpcomingTodos(sharedService.getUserId()).success(function (data) {
        //                                                   
        //                                                   if (data.success) {
        //                                                       $scope.userUpcomingTodosObject = data.ResponseData;
        //                                                       /* Getting User Upcoming Events */
        //                                                       userDashboardFactory.getUserUpcomingEvents(sharedService.getUserId()).success(function (data) {
        //                                                           
        //                                                           if (data.success) {
        //                                                               $scope.userUpcomingEventsObject = data.ResponseData;
        //                                                               /* Getting User T Line */
        //                                                               userDashboardFactory.getUserTLine(sharedService.getUserId()).success(function (data) {
        //                                                                   
        //                                                                   if (data.success) {
        //                                                                       $scope.userTLineObject = data.ResponseData;
        //                                                                       /* Getting Client Feedbacks associated with the User  */
        //                                                                       clientFeedbackFactory.getUserClientFeedbacks(sharedService.getUserId()).success(function (data) {
        //                                                                           
        //                                                                           if (data.success) {
        //                                                                               $scope.userClientFeedbacksObject = data.ResponseData;
        //                                                                               $timeout(function () {
        //                                                                                   $scope.$apply();
        //                                                                                   $timeout(hideloader, 1000);
        //                                                                               });
        //                                                                           }
        //                                                                           else {
        //                                                                               toaster.error("Error", data.message);
        //                                                                           }
        //                                                                       }).error(function () {
        //                                                                           toaster.error("Error", "Some Error Occured !");
        //                                                                       });
        //                                                                   }
        //                                                                   else {
        //                                                                       toaster.error("Error", data.message);
        //                                                                   }
        //                                                               }).error(function () {
        //                                                                   toaster.error("Error", "Some Error Occured !");
        //                                                               });
        //                                                           }
        //                                                           else {
        //                                                               toaster.error("Error", data.message);
        //                                                           }
        //                                                       }).error(function () {
        //                                                           toaster.error("Error", "Some Error Occured !");
        //                                                       });
        //                                                   }
        //                                                   else {
        //                                                       toaster.error("Error", data.message);
        //                                                   }
        //                                               }).error(function () {
        //                                                   toaster.error("Error", "Some Error Occured !");
        //                                               });
        //                                           }
        //                                           else {
        //                                               toaster.error("Error", data.message);
        //                                           }
        //                                       }).error(function () {
        //                                           toaster.error("Error", "Some Error Occured !");
        //                                       });
        //                                   });
        //                               }
        //                               else {
        //                                   toaster.error("Error", data.message);
        //                               }
        //                           }).error(function () {
        //                               toaster.error("Error", "Some Error Occured!");
        //                           });
        //                       });
        //                   }
        //                   else {
        //                       toaster.error("Error", data.message);
        //                   }
        //               })
        //                .error(function () {
        //                    toaster.error("Error", "Some Error Occured !");
        //                });
        //}

        ///*Getting User Upcoming Todos*/
        //userDashboardFactory.getUserUpcomingTodos(sharedService.getUserId()).success(function (data) {
        //    
        //    if (data.success) {
        //        if (data.ResponseData == undefined)
        //            $scope.userUpcomingTodosObject = {};
        //        else
        //            $scope.userUpcomingTodosObject = data.ResponseData;
        //        /* Getting User Upcoming Events */
        //        userDashboardFactory.getUserUpcomingEvents(sharedService.getUserId()).success(function (data) {
        //            
        //            if (data.success) {
        //                if (data.ResponseData == undefined)
        //                    $scope.userUpcomingEventsObject == {}
        //                else
        //                    $scope.userUpcomingEventsObject = data.ResponseData;
        //                /* Getting User T Line */
        //                userDashboardFactory.getUserTLine(sharedService.getUserId()).success(function (data) {
        //                    
        //                    if (data.success) {
        //                        if (data.ResponseData == undefined)
        //                            $scope.userTLineObject = {};
        //                        else
        //                            $scope.userTLineObject = data.ResponseData;
        //                        /* Getting Client Feedbacks associated with the User  */
        //                        clientFeedbackFactory.getUserClientFeedbacks(sharedService.getUserId()).success(function (data) {
        //                            
        //                            if (data.success) {
        //                                if (data.ResponseData == undefined)
        //                                    $scope.userClientFeedbacksObject = {};
        //                                else
        //                                    $scope.userClientFeedbacksObject = data.ResponseData;
        //                                $scope.showCenterLoader = false;
        //                                //$('#divMyDashboard').show();
        //                            }
        //                            else {
        //                                toaster.error("Error", data.message);
        //                            }
        //                        }).error(function () {
        //                            toaster.error("Error", "Some Error Occured !");
        //                        });
        //                    }
        //                    else {
        //                        toaster.error("Error", data.message);
        //                    }
        //                }).error(function () {
        //                    toaster.error("Error", "Some Error Occured !");
        //                });
        //            }
        //            else {
        //                toaster.error("Error", data.message);
        //            }
        //        }).error(function () {
        //            toaster.error("Error", "Some Error Occured !");
        //        });
        //    }
        //    else {
        //        toaster.error("Error", data.message);
        //    }
        //}).error(function () {
        //    toaster.error("Error", "Some Error Occured !");
        //});

        //// convert the html Content
        //$scope.renderHtml = function (htmlCode) {
        //    return $sce.trustAsHtml(htmlCode);
        //};



    });
});