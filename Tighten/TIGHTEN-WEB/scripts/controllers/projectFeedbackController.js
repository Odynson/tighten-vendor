﻿
define(['app'], function (app) {
    app.controller("projectFeedbackController", function ($scope, $rootScope, $stateParams, apiURL, $timeout, $http, $location, sharedService, $confirm, toaster, projectFeedbackFactory) {

        $scope.imgURL = apiURL.imageAddress;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }

        $scope.RoleId = sharedService.getRoleId();


        $scope.showProjectFeedback = function (projectId) {
            $('#globalLoader').show();
            projectFeedbackFactory.getProjectByProjectId(projectId, sharedService.getUserId(), sharedService.getRoleId())
            .success(function (dataProjectByProjectId) {
                $scope.projectObject = {};
                if (dataProjectByProjectId.ResponseData != null) {
                    if (dataProjectByProjectId.ResponseData.length > 0) {
                        $scope.projectObject = dataProjectByProjectId.ResponseData[0];
                        $scope.ProjectRemarks = dataProjectByProjectId.ResponseData[0].ProjectRemarks;
                    }
                }
                $('#globalLoader').hide();

                    //projectFeedbackFactory.getUserRolePriority(projectId, sharedService.getUserId(), sharedService.getRoleId())
                    //    .success(function (dataRolePriority) {
                    //        $scope.currentUserRolePriority = dataRolePriority.ResponseData;
                    //        $('#globalLoader').hide();
                    //    })
                    //    .error(function (data) {
                    //        toaster.error("Error", "Some Error Occured!");
                    //        $('#globalLoader').hide();
                    //    })

            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }



        $scope.addUpdateFeedback = function () {
            $('#globalLoader').show();
            $scope.projectObject.CurrentUser = sharedService.getUserId();
            projectFeedbackFactory.addUpdateFeedback($scope.projectObject)
            .success(function (dataAddUpdateFeedback) {
                $scope.showProjectFeedback($scope.projectObject.ProjectId);
                toaster.success("Success", "Feedback to other users submitted successfully!");
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })

        }



        $scope.saveProjectRemarks = function () {
            $('#globalLoader').show();
            projectFeedbackFactory.saveProjectRemarks($scope.projectObject)
            .success(function (dataProjectRemarks) {
                toaster.success("Success", "Project Remarks submitted successfully!");
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }


        $scope.cancelProjectRemarks = function () {
            $scope.projectObject.ProjectRemarks = $scope.ProjectRemarks;
        }

        $scope.markAsComplete = function () {
            $('#globalLoader').show();
            projectFeedbackFactory.markAsComplete($scope.projectObject)
            .success(function (dataMarkAsComplete) {
                if (dataMarkAsComplete.ResponseData == "ProjectMarkedAsComplete") {
                    $scope.projectObject.ProjectCompleted = true;
                    toaster.success("Success", message["ProjectMarkedAsComplete"]);
                }
                else if (dataMarkAsComplete.ResponseData == "ProjectManagerNotCommented") {
                    toaster.warning("Warning", message["ProjectManagerNotCommented"]);
                }
                else {
                    toaster.error("Error", message["error"]);
                }
                $('#globalLoader').hide();

            })
            .error(function (data) {
                toaster.error("Error", message["error"]);
                $('#globalLoader').hide();
            })
        }

        $scope.cancelFeedback = function () {
            $location.path("/project");
        }


        angular.element(document).ready(function () {
            var projectId = $stateParams.pid;
            $scope.showProjectFeedback(projectId);
        });







    });
});