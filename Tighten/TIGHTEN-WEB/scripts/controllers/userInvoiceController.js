﻿
define(['app', 'userInvoiceFactory'], function (app) {


    app.controller("userInvoiceController", function ($scope, $rootScope, $timeout, $location, $stateParams, $http, sharedService, toaster, apiURL, userInvoiceFactory, userTodoApprovalFactory, reportersFactory) {

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.imgURL = apiURL.imageAddress;
        $scope.Invoice = [];
        //$scope.InvoiceList = [];
        $scope.InvoiceDetailList = [];
        $scope.ProjectMember = {};
        $scope.isFreelancer = sharedService.getIsFreelancer();
        $scope.selectedCompanyId = sharedService.getCompanyId();
        $scope.sendReportsModel = {};
        $scope.Reporter = {};


        // Company dropdown change event on left side menu for freelancer 
        $scope.$on('leftCompanyDropdownChange', function (event, data) {
            $scope.selectedCompanyChanged(data.CompanyId);
        });

        $scope.selectedCompanyChanged = function (selectedCompany) {

            $scope.selectedCompanyId = selectedCompany;
           
            var d = new Date();
            d.setDate(d.getDate() - 7);
            $scope.fromDate = d;
            $scope.toDate = new Date();

            $('#globalLoader').show();
            var date = Date();
            $scope.CurrentDate = date.substring(0, 3) + "," + date.substring(3, 15);

            var data = { "UserId": sharedService.getUserId(), "CompanyId": $scope.selectedCompanyId, "InvoiceStatus": $scope.selectedStatus, "InvoiceType": $scope.selectedType, "FromDate": $scope.fromDate, "ToDate": $scope.toDate };
            $scope.ShowInvoice(data);

        };

        $scope.TaskStatus = {
            alltask: [
                { id: 0, task: 'All' },
                { id: 1, task: 'Approved' },
                { id: 2, task: 'Not-Processed' }

            ]
        };

        $scope.InvoiceType = {
            allType: [
                { id: 1, Type: 'Sent' },
                { id: 2, Type: 'Received' }

            ]
        };

        $scope.selectedStatus = '0';
        $scope.selectedType = '0';
        

        ////////////Date Functions/////////////
        /************************/
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };
        var da = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
        };

        $scope.toggleMin();

        $scope.maxDate = new Date(da.getFullYear(), 5, 22);

        $scope.popup1 = {
            opened: false
        };
        $scope.popup2 = {
            opened: false
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.setDate = function (year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
        //$scope.init = new Date(da.getFullYear()-10, 5, 22);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,

        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.getDayClass = function (date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        };
        /***********************/

        $scope.openPopupSendReport_From = function () {
            $scope.popupSendReport_From.opened = true;
        };

        $scope.openPopupSendReport_To = function () {
            $scope.popupSendReport_To.opened = true;
        };

        $scope.popupSendReport_From = {
            opened: false
        };

        $scope.popupSendReport_To = {
            opened: false
        };


        $scope.convertTo = function (arr, key, dayWise) {
             
            var groups = {};
            for (var i = 0; l = arr.length, i < l; i++) {
                var newkey = '';
                if (dayWise) {
                    var asfdf = arr[i][key];
                    //var weekday_value = $scope.weekdays[new Date(arr[i][key]).getDay()];
                    var date = new Date(arr[i][key]).toUTCString();
                    var newdate = date.substring(0, 16)
                    //arr[i][key] = [weekday_value] + ', ' + date;
                    newkey = newdate;
                }
                else {
                    //arr[i][key] = arr[i][key].toTimeString();
                    newkey = arr[i][key].toTimeString();
                }
                //groups[arr[i][key]] = groups[arr[i][key]] || [];
                //groups[arr[i][key]].push(arr[i]);
                groups[newkey] = groups[newkey] || [];
                groups[newkey].push(arr[i]);
            }
            return groups;
        };


        $scope.searchResult = function () {
            $('#globalLoader').show();
            var data = { "UserId": sharedService.getUserId(), "CompanyId": $scope.selectedCompanyId, "InvoiceStatus": $scope.selectedStatus, "InvoiceType": $scope.selectedType, "FromDate": $scope.fromDate, "ToDate": $scope.toDate };
            $scope.ShowInvoice(data); 
        }

        $scope.FilterInvoiceByType = function (selectedType) {
            $('#globalLoader').show();
            var data = { "UserId": sharedService.getUserId(), "CompanyId": $scope.selectedCompanyId, "InvoiceStatus": $scope.selectedStatus, "InvoiceType": $scope.selectedType, "FromDate": $scope.fromDate, "ToDate": $scope.toDate };
            $scope.ShowInvoice(data);
        }


        $scope.FilterInvoiceByStatus = function (selectedStatus) {

            $('#globalLoader').show();
            var data = { "UserId": sharedService.getUserId(), "CompanyId": $scope.selectedCompanyId, "InvoiceStatus": selectedStatus, "InvoiceType": $scope.selectedType, "FromDate": $scope.fromDate, "ToDate": $scope.toDate };
            $scope.ShowInvoice(data);
        }


        //////////////////////////////////////////////////////getTotalAmountForPaidInvoice/////////////////////////////////////////////////////////////////
        $scope.getTotalAmountForPaidInvoice = function () {
             
            var total_Amount = 0;
            if ($scope.Invoice.list_Paid.length > 0) {
                for (var i = 0; i < $scope.Invoice.list_Paid.length; i++) {
                    var Inv = $scope.Invoice.list_Paid[i];
                    if (Inv.InvoiceApprovedStatus == true) {
                        total_Amount += (Inv.TotalAmount);
                    }
                }
            }
            return total_Amount;
        }
        //////////////////////////////////////////////////////getTotalAmountForNotPaidInvoice/////////////////////////////////////////////////////////////////
        
        $scope.getTotalAmountForNotPaidInvoice = function () {
            
            var total_Amount = 0;
            if ($scope.Invoice.list_NotPaid.length > 0) {
                for (var i = 0; i < $scope.Invoice.list_NotPaid.length; i++) {
                    var Inv = $scope.Invoice.list_NotPaid[i];
                    if (Inv.InvoiceApprovedStatus == false) {
                        total_Amount += (Inv.TotalAmount);
                    }
                }
            }
            return total_Amount;
        }

        $scope.getTotalAmountForNotPaidInvoice = function () {

            var total_Amount = 0;
            if ($scope.Invoice.list_NotPaid.length > 0) {
                for (var i = 0; i < $scope.Invoice.list_NotPaid.length; i++) {
                    var Inv = $scope.Invoice.list_NotPaid[i];
                    if (Inv.InvoiceApprovedStatus == false) {
                        total_Amount += (Inv.TotalAmount);
                    }
                }
            }
            return total_Amount;
        }





        $scope.ShowInvoice = function (data) {
            
            data.RoleId = sharedService.getRoleId();
            userInvoiceFactory.getAllInvoicesForUser(data)
            .success(function (result) {

                $scope.Invoice = result.ResponseData;
                $scope.role = sharedService.getRoleId() == 3 ? true : false ;
                //$scope.InvoiceList = [];
                //angular.copy($scope.Invoice, $scope.InvoiceList);
                //$scope.InvoiceList = $scope.convertTo($scope.InvoiceList, 'InvoiceDate', true);
                $('#globalLoader').hide();
                //setTimeout(function () {
                //    $scope.$apply();
                //}, 1000);

            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", "Some error occured");
            })

        };



        $scope.showInvoiceDetail = function (companyIdToShowInvoiceDetail, invoiceId, IsInvoiceForwardForDetails) {
            $('#globalLoader').show();
             
            var data = { "invoiceId": invoiceId, "UserId": sharedService.getUserId(), "CompanyId": companyIdToShowInvoiceDetail, "IsInvoiceForwardForDetails": IsInvoiceForwardForDetails };
            userInvoiceFactory.getInvoiceDetailForUser(data)
            .success(function (result) {
                $scope.AllTodosForPreviewObject = result.ResponseData;
              
                $('#modalInvoicePreview').modal('show');
                $('#globalLoader').hide();
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", "Some error occured");
            })

        }


        $scope.showforwardInvoice = function (invoiceId,CompanyId) {
            $('#globalLoader').show();
            var data = {"invoiceId":invoiceId  ,"UserId":sharedService.getUserId()}
            userInvoiceFactory.getProjectMembersForUser(data)
            .success(function (response) {

                $scope.ProjectMembers = response.ResponseData;
                $scope.invoiceId = invoiceId;
                $scope.CompanyId = CompanyId;

                $("#modalForwardInvoice").modal("show");
                $('#globalLoader').hide();
            })
            .error(function (response) {
                $('#globalLoader').hide();
                toaster.error("Error", message[response.message]);
            })
        }



        $scope.forwardInvoice = function (invoiceId, CompanyId) {
            $('#globalLoader').show();
            var data = { "InvoiceId": invoiceId, "ForwardTo": $scope.ProjectMember.SelectedProjectMember, "UserId": sharedService.getUserId(), "CompanyId": CompanyId }
            userInvoiceFactory.forwardInvoice(data)
            .success(function (data) {
                if (data != "") {
                    
                    var data = { "UserId": sharedService.getUserId(), "CompanyId": $scope.selectedCompanyId, "InvoiceStatus": $scope.selectedStatus, "InvoiceType": $scope.selectedType, "FromDate": $scope.fromDate, "ToDate": $scope.toDate };
                    $scope.ShowInvoice(data);

                    toaster.success("Success", message.ForwardInvoiceSuccess);
                    $("#modalForwardInvoice").modal("hide");
                    $('#globalLoader').hide();
                }
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message.error);
            })

        }


        //$scope.approveInvoice = function (InvoiceId) {
        //    $('#globalLoader').show();
        //    userInvoiceFactory.approveInvoice(InvoiceId)
        //    .success(function () {
        //        toaster.success("Success", "Invoice status changed Successfully");
        //        $('#globalLoader').hide();
        //    })
        //    .error(function () {
        //        $('#globalLoader').hide();
        //        toaster.error("Error", "Some error occured");
        //    })

        //}



        $scope.CloseInvoicePreview = function () {
            $('#modalSendReports').modal('show');
            $('#modalInvoicePreview').modal('hide');
        }


        angular.element(document).ready(function () {
            
            var d = new Date();
            d.setDate(d.getDate() - 7);
            $scope.fromDate = d;
            $scope.toDate = new Date();

            $('#globalLoader').show();
            var date = Date();
            $scope.CurrentDate = date.substring(0, 3) + "," + date.substring(3, 15);

            var data = { "UserId": sharedService.getUserId(), "CompanyId": $scope.selectedCompanyId, "InvoiceStatus": $scope.selectedStatus, "InvoiceType": $scope.selectedType, "FromDate": $scope.fromDate, "ToDate": $scope.toDate };
            $scope.ShowInvoice(data);
            
        })


        $scope.goToCompanyProfile = function (companyId) {
            $location.path('/company/profile/' + companyId);
        };


        $scope.ShowSendReportsModal = function () {

            $scope.sendReportsModel.ToDate = $scope.toDate;
            $scope.sendReportsModel.FromDate = $scope.fromDate;

            userTodoApprovalFactory.getUserCompanies(sharedService.getUserId())
           .success(function (data) {
               if (data.success) {
                   $scope.allCompaniesForUser = data.ResponseData;
                   $scope.sendReportsModel.Message = "";
                   $scope.sendReportsModel.companyId = "";
                   $scope.Reporter.projectId = 0;
                   $scope.Reporter.UserId = 0;
                   $('#modalSendReports').modal('show');
               }
               else {
                   toaster.error("Error", message[data.message]);
               }
           })
           .error(function (data) {
               toaster.error("Error", message[data.message]);
           })

        };

        $scope.userCompaniesChange = function (companyId) {

            if (companyId == "")
                companyId = 0;

            if ($scope.sendReportsModel.companyId != undefined || $scope.sendReportsModel.companyId != 0) {
                //$scope.validateMessage = "";
                $("#divCompany").removeClass('ddlerror');
            }

            $scope.selectedCompanyId = companyId;

            userTodoApprovalFactory.getUserProjectsForAllCompanies(sharedService.getUserId(), companyId)
             .success(function (data) {
                 if (data.success) {
                     $scope.allProjectsForUser = data.ResponseData;
                 }
                 else {
                     toaster.error("Error", message[data.message]);
                 }
             })
             .error(function (data) {
                 toaster.error("Error", message[data.message]);
             })

        }

        $scope.userProjectChange = function (companyId, projectId) {

            if (projectId == "" || projectId == null) {
                projectId = 0;
            }
            if (companyId == "" || companyId == null) {
                companyId = 0;
            }
            reportersFactory.getAllProjectUsersForTodoes(sharedService.getUserId(), companyId, projectId, sharedService.getRoleId())
                         .success(function (data) {

                             if (data.success) {
                                 $scope.projectUsersForTodoesObject = data.ResponseData;
                                 $timeout(function () {
                                     $scope.$apply();
                                     bindDropdown_TodoReporters();
                                     $scope.Reporter.UserId = 0;
                                 });

                             }
                             else {
                                 toaster.error("Error", data.message);
                             }
                         })
                         .error(function () {
                             toaster.error("Error", "Some Error Occured !");
                         });

            function bindDropdown_TodoReporters() {
                var oo = $scope.projectUsersForTodoesObject;
                var item = [], i = 0, folo = [];
                for (i = 0; i < oo.length; i++) {
                    var jsonItem = {};
                    jsonItem.Value = oo[i].UserId;
                    jsonItem.Text = oo[i].FirstName + " - " + oo[i].Email;
                    //jsonItem.ticked = false;
                    item.push(jsonItem);
                }
                $scope.newModel = item;
                $scope.output = [];
            }
        }

        $scope.reporterForInvoiceChange = function (reporterId) {
            if (reporterId != undefined || reporterId != 0) {
                //$scope.validateMessage = "";
                $("#divReporter").removeClass('ddlerror');
            }
        };

        $scope.ShowInvoicePreview = function () {

            if ($scope.sendReportsModel.companyId == undefined || $scope.sendReportsModel.companyId == 0) {
                toaster.warning("Warning", "Please select company");
                //$scope.validateMessage = "Please select company";
                $("#divCompany").addClass('ddlerror');
                return;
            }
            else if ($scope.Reporter.UserId == undefined || $scope.Reporter.UserId == 0) {
                toaster.warning("Warning", "Please select reporter");
                //$scope.validateMessage = "Please select reporter";
                $("#divReporter").addClass('ddlerror');
                return;
            }
            else {
                var data = { 'fromDate': $scope.sendReportsModel.FromDate, 'toDate': $scope.sendReportsModel.ToDate, 'userId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId, 'Message': $scope.sendReportsModel.Message, 'ReportersID': $scope.Reporter.UserId, 'ProjectId': $scope.Reporter.projectId }
                reportersFactory.getAllTodosForPreview(data)
                            .success(function (data) {
                                if (data.success) {

                                    $scope.AllTodosForPreviewObject = data.ResponseData;
                                }
                                else {
                                    toaster.error("Error", data.message);
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured !");
                            });

                $('#modalSendReports').modal('hide');
                $('#modalInvoicePreview').modal('show');
            }
        }

        $scope.SendReport = function () {

            if ($scope.sendReportsModel.companyId == undefined || $scope.sendReportsModel.companyId == 0) {
                toaster.warning("Warning", "Please select company");
                //$scope.validateMessage = "Please select company";
                $("#divCompany").addClass('ddlerror');
                return;
            }
            else if ($scope.Reporter.UserId == undefined || $scope.Reporter.UserId == 0) {
                toaster.warning("Warning", "Please select reporter");
                //$scope.validateMessage = "Please select reporter";
                $("#divReporter").addClass('ddlerror');
                return;
            }
            else {
                $('#globalLoader').show();

                var data = { 'fromDate': $scope.sendReportsModel.FromDate, 'toDate': $scope.sendReportsModel.ToDate, 'userId': sharedService.getUserId(), 'CompanyId': $scope.sendReportsModel.companyId, 'Message': $scope.sendReportsModel.Message, 'ReportersID': $scope.Reporter.UserId, 'ProjectId': $scope.Reporter.projectId }
                reportersFactory.getAllTodosForPreview(data)
                            .success(function (data) {
                                if (data.success) {

                                    $scope.AllTodosForPreviewObject = data.ResponseData;

                                    $scope.sendReportsModel.Reporters = $scope.Reporter.UserId;
                                    $scope.sendReportsModel.UserId = sharedService.getUserId();
                                    $scope.sendReportsModel.CompanyId = $scope.sendReportsModel.companyId;
                                    $scope.sendReportsModel.ProjectId = $scope.Reporter.projectId;
                                    $scope.sendReportsModel.Message = $scope.sendReportsModel.Message;
                                    $scope.sendReportsModel.AllTodosForPreview = $scope.AllTodosForPreviewObject;

                                    var data = $scope.sendReportsModel;

                                    reportersFactory.SaveInvoice(data)
                                          .success(function (response) {
                                              if (response.ResponseData == "Account Not Verified") {
                                                  toaster.warning({ title: "", body: "Please configure your account first" });
                                                  $('#globalLoader').hide();
                                              }
                                              else if (response.ResponseData == "") {
                                                  toaster.warning({ title: "", body: "There are no Tasks available for invoice" });
                                                  $('#globalLoader').hide();
                                              }
                                              else {
                                                  toaster.success("Success", "Invoice sent");
                                                  //reportersFactory.sendMailToProjectUsersForTodoes(data);
                                                  $('#modalSendReports').modal('hide');
                                                  $('#globalLoader').hide();

                                                  //Updating the invoice listing
                                                  var d = new Date();
                                                  d.setDate(d.getDate() - 7);
                                                  $scope.fromDate = d;
                                                  $scope.toDate = new Date();

                                                  $('#globalLoader').show();
                                                  var date = Date();
                                                  $scope.CurrentDate = date.substring(0, 3) + "," + date.substring(3, 15);

                                                  var data = { "UserId": sharedService.getUserId(), "CompanyId": $scope.selectedCompanyId, "InvoiceStatus": $scope.selectedStatus, "InvoiceType": $scope.selectedType, "FromDate": $scope.fromDate, "ToDate": $scope.toDate };
                                                  $scope.ShowInvoice(data);

                                              }
                                          })
                                          .error(function (response) {
                                              toaster.error("Error", "Some error occured");
                                              $('#globalLoader').hide();
                                          })
                                }
                                else {
                                    toaster.error("Error", data.message);
                                    $('#globalLoader').hide();
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured !");
                                $('#globalLoader').hide();
                            });
            }
        };





    });

});

