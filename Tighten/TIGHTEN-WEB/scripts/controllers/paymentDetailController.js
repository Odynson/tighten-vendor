﻿
define(['app', 'paymentDetailFactory'], function (app) {

    app.controller("paymentDetailController", function ($scope, $rootScope, $http, sharedService, $confirm, $timeout, toaster, apiURL, $location, paymentDetailFactory) {

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.imgURL = apiURL.imageAddress;


        $scope.getYearsList = function (range) {
            var currentYear = new Date().getFullYear();
            var years = [];
            for (var i = 0; i < range; i++) {
                years.push(currentYear + i);
            }
            return years;
        };



        /*SHowing Add Update Payment Detail Modal Pop Up*/
        $scope.showPaymentDetailAddUpdateModal = function () {
            $scope.paymentDetailButton = false;
            $scope.paymentDetailAddUpdateModel = {};

            //$("input[name='teamName']").removeClass("inputError");
            //$("textarea[name='teamDescription']").removeClass("inputError");
            //$("input[name='teamBudget']").removeClass("inputError");
            $('#modalPaymentDetailAddUpdate').modal('show');

        };


        /*Edit Payment Detail*/
        $scope.editPaymentDetail = function (paymentDetailObject) {

            $scope.paymentDetailButton = true;
            $scope.paymentDetailAddUpdateModel = {};
            $scope.paymentDetailAddUpdateModel = paymentDetailObject;

            $('#modalPaymentDetailAddUpdate').modal('show');
        };



        /*Add new Payment Detail */
        $scope.addNewPaymentDetail = function (isCardNameInvalid, isCreditCardNumberInvalid, isExpiryMonthInvalid, isExpiryYearInvalid, isCvvNumberInvalid, paymentDetailAddUpdateForm) {
            debugger;
            if (isCardNameInvalid || isCreditCardNumberInvalid || isExpiryMonthInvalid || isExpiryYearInvalid || isCvvNumberInvalid) {
                paymentDetailAddUpdateForm.CardName.$dirty = true;
                paymentDetailAddUpdateForm.CreditCardNumber.$dirty = true;
                paymentDetailAddUpdateForm.ExpiryMonth.$dirty = true;
                paymentDetailAddUpdateForm.ExpiryYear.$dirty = true;
                paymentDetailAddUpdateForm.CvvNumber.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                $scope.paymentDetailAddUpdateModel.CompanyId = sharedService.getCompanyId();
                $scope.paymentDetailAddUpdateModel.UserId = sharedService.getUserId();
                paymentDetailFactory.insertPaymentDetail($scope.paymentDetailAddUpdateModel)
                   .success(function (data) {
                       if (data.ResponseData != 'DuplicatePaymentDetail') {
                           if (data.ResponseData != 'DuplicateCardName') {
                               if (data.success) {
                                   $timeout(function () {
                                       $scope.$apply();
                                       /* Getting Payment Detail List */
                                       getPaymentDetail();
                                   });

                                   $timeout(function () {
                                       $scope.$apply();
                                       $('#modalPaymentDetailAddUpdate').modal('hide');
                                       $('#globalLoader').hide();
                                       toaster.success("Success", message[data.ResponseData]);

                                       paymentDetailAddUpdateForm.CardName.$dirty = false;
                                       paymentDetailAddUpdateForm.CreditCardNumber.$dirty = false;
                                       paymentDetailAddUpdateForm.ExpiryMonth.$dirty = false;
                                       paymentDetailAddUpdateForm.ExpiryYear.$dirty = false;
                                       paymentDetailAddUpdateForm.CvvNumber.$dirty = false;
                                   });
                               }
                               else {
                                   toaster.error("Error", message["error"]);
                                   $('#globalLoader').hide();
                               }
                           }
                           else {
                               toaster.warning("Warning", message["DuplicateCardName"]);
                               $('#globalLoader').hide();
                           }
                       }
                       else {
                           toaster.warning("Warning", message["DuplicatePaymentDetail"]);
                           $('#globalLoader').hide();
                       }
                   })
                    .error(function (error) {
                        toaster.error("Error", message["error"]);
                        $('#globalLoader').hide();
                    });
            }
        };





        /*Update Payment Detail*/
        $scope.updatePaymentDetail = function (isCardNameInvalid, isCreditCardNumberInvalid, isExpiryMonthInvalid, isExpiryYearInvalid, isCvvNumberInvalid, paymentDetailAddUpdateForm) {

            if (isCardNameInvalid || isCreditCardNumberInvalid || isExpiryMonthInvalid || isExpiryYearInvalid || isCvvNumberInvalid) {
                paymentDetailAddUpdateForm.CardName.$dirty = true;
                paymentDetailAddUpdateForm.CreditCardNumber.$dirty = true;
                paymentDetailAddUpdateForm.ExpiryMonth.$dirty = true;
                paymentDetailAddUpdateForm.ExpiryYear.$dirty = true;
                paymentDetailAddUpdateForm.CvvNumber.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                $scope.paymentDetailAddUpdateModel.CompanyId = sharedService.getCompanyId();
                $scope.paymentDetailAddUpdateModel.UserId = sharedService.getUserId();
                paymentDetailFactory.updatePaymentDetail($scope.paymentDetailAddUpdateModel)
                    .success(function (data) {
                        if (data.success) {
                            if (data.ResponseData != null) {
                                getPaymentDetail();
                                if (data.ResponseData == -1) {
                                    toaster.warning("Warning",message.creditcardDuplicateNumber);
                                }
                                else {
                                    toaster.success("Success", message.creditcardUpdateSuccessfully);
                                }

                                $('#modalPaymentDetailAddUpdate').modal('hide');
                                $('#globalLoader').hide();

                                paymentDetailAddUpdateForm.CardName.$dirty = false;
                                paymentDetailAddUpdateForm.CreditCardNumber.$dirty = false;
                                paymentDetailAddUpdateForm.ExpiryMonth.$dirty = false;
                                paymentDetailAddUpdateForm.ExpiryYear.$dirty = false;
                                paymentDetailAddUpdateForm.CvvNumber.$dirty = false;

                            }

                        }
                        else {
                            toaster.error("Error", message["error"]);
                            $('#globalLoader').hide();
                        }
                    })
                    .error(function () {
                        toaster.error("Error", message["error"]);
                        $('#globalLoader').hide();
                    });
            }
        };

        /*Delete Payment Detail*/
        $scope.deletePaymentDetail = function (paymentDetailId, index) {
            $confirm({ text: 'Are you sure you want to delete this Payment Detail ?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
               .then(function () {
                   $('#globalLoader').show();
                   paymentDetailFactory.deletePaymentDetail(paymentDetailId)
                       .success(function (data) {
                           if (data.success) {
                               toaster.success("Success", data.message);
                               $scope.paymentDetail.splice(index, 1);
                               $('#globalLoader').hide();
                           }
                           else {
                               toaster.error("Error", message["error"]);
                               $('#globalLoader').hide();
                           }
                       })
                       .error(function () {
                           toaster.error("Error", message["error"]);
                           $('#globalLoader').hide();
                       });
               })
        };


        // Checking wheather the input chaaracter is number or not 
        //$scope.isNumber = function (evnt) {
        //    if (evnt.keyCode > 47 && evnt.keyCode < 58 || evnt.charCode > 47 && evnt.charCode < 58) {
        //        return true;
        //    }
        //    else {
        //        evnt.preventDefault();
        //        return false;
        //    }
        //};


        function getPaymentDetail() {

            paymentDetailFactory.getpaymentDetail(sharedService.getCompanyId())
                .success(function (paymentDetailData) {
                    if (paymentDetailData.success) {
                        $scope.paymentDetail = paymentDetailData.ResponseData;
                    }
                    else {
                        toaster.error("Error", paymentDetailData.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });

        }


        angular.element(document).ready(function () {
            getPaymentDetail();
        });





    });

});
