﻿
define(['app',], function (app) {

    app.controller("teamController", function ($compile, $scope, $rootScope, apiURL, $timeout, $http, sharedService, toaster, $confirm, teamFactory, usersFactory, FreelancerAccountFactory, vendorFactory, $filter, $window) {
        window.Selectize = require('selectize');
        $scope.imgURL = apiURL.imageAddress;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        } 
        $scope.showMsgs = false;
        $scope.showNestedMsgs = false;
        $scope.CompanyTeamsObject = [];
        $scope.teamAddUpdateModel = {};
        $scope.teamAddUpdateModel.teamMembers = [];
        $scope.myTeamMember = {};
        $scope.Role = 0;
        $scope.isFreelancer = sharedService.getIsFreelancer();
        $scope.selectedCompanyId = sharedService.getCompanyId();
        //$scope.Permission = {};
        $scope.getAllDesignationList = {};
        $scope.designationgModalList = [];
        $scope.teamModal = {};
        // $scope.isChecked = false;
        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 100;  // Total number of items in all pages. initialize as a zero  
        $scope.PageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  

        $scope.editmode = 0;
        $scope.flag = 0;

        $scope.pagesizeList = [
            { PageSize: 5 },
            { PageSize: 10 },
            { PageSize: 25 },
            { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];
        $scope.totalPageCount = 0;
        $scope.AllInternalUsersForTeam = [];


        // Company dropdown change event on left side menu for freelancer 
        $scope.$on('leftCompanyDropdownChange', function (event, data) {

            $scope.selectedCompanyId = data.CompanyId;
            $scope.showTeamManagementSection();
        });

        $scope.selectedCompanyChanged = function (selectedCompany) {

            $scope.selectedCompanyId = selectedCompany;
            $scope.showTeamManagementSection();
        };

        $scope.getCompanyTeams = function () {
            debugger
            /* Getting Company Teams with Users */
            teamFactory.getCompanyTeams($scope.selectedCompanyId, sharedService.getUserId(), $scope.PageIndex, $scope.pageSizeSelected)
                .success(function (data) {
                    debugger;
                    if (data.success) {

                        $scope.CompanyTeamsObject = data.ResponseData;
                        $scope.CompanyTeamsInTreeViewList = data.ResponseData;
                        // $scope.CustomcontrolList = $scope.CompanyTeamsObject;
                        // $scope.locations = $scope.CompanyTeamsObject;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCount = data.ResponseData[0].TotalPageCount;
                        }
                        debugger

                        /* Getting Users Details in which all users are there so that they can be added for adding new team */
                        var checkEmailConfirmed = true;
                        //usersFactory.getUsers(sharedService.getUserId(), checkEmailConfirmed, $scope.selectedCompanyId)
                        //    .success(function (data) {
                        //        if (data.success) {
                        //            debugger;
                        //            $scope.UsersObject = data.ResponseData;
                        //            $scope.showCenterLoader = false;
                        //            $('#divTeamManagementSection').show();
                        //        }
                        //        else {
                        //            toaster.error("Error", data.message);
                        //        }
                        //    })
                        //    .error(function () {
                        //        toaster.error("Error", "Some error occured");
                        //    });
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured");
                });
        }

        /* Showing Team Management Section to see the information*/
        $scope.showTeamManagementSection = function () {

            //closeCentreSections();
            $scope.showCenterLoader = true;
            $scope.selectedProject = 0;
            $scope.inputSearchTeam = "";
            $scope.selectedTodo = 0;
            $scope.selectedNavigation = "TeamManagementSection";
            $scope.getCompanyTeams();

        };

        /*SHowing Add Update Team Modal Pop Up*/
        $scope.showTeamAddUpdateModal = function () {
            debugger;
            $scope.showMsgs = false;
            $scope.flag = 0;
            //$scope.getAllUsers();
            $('#globalLoader').show();
            usersFactory.getUsersForTeam(sharedService.getCompanyId())
                .success(function (data) {
                    debugger;
                    if (data.success && data.ResponseData != null) {
                        $scope.UsersObject = data.ResponseData;
                        $scope.AllInternalUsersForTeam = [];
                        $scope.myTeamMember.teamMembers = [];
                        $scope.myTeamMember.teamMembers = $scope.UsersObject;
                        $scope.teamButton = false;
                        getAllDesignation();
                        $scope.showCenterLoader = false;
                        $('#modalTeamAddUpdate').modal('show');
                        $('#divTeamManagementSection').show();
                        $('#globalLoader').hide();
                    }
                }).error(function () {
                    toaster.error("Error", "Some error occured");
                    $('#globalLoader').hide();

                });

            //$scope.teamButton = false;
            $scope.teamAddUpdateModel = {};
            $scope.teamAddUpdateModel.teamMembers = [];
            //$scope.myTeamMember.teamMembers = [];
            //$scope.myTeamMember.teamMembers = $scope.UsersObject;
            //$('#teamName').removeClass('inputError');
            $("input[name='teamName']").removeClass("inputError");
            $("textarea[name='teamDescription']").removeClass("inputError");
            $("input[name='teamBudget']").removeClass("inputError");
            $scope.columns = [];
            $scope.teamMembersNew = [];
            $scope.openSaveRoleTags = false;
            var multiSelectPreviousData = [];
            var multiSelectCurrentData = [];
            //$('#modalTeamAddUpdate').modal('show');
        };

        /*Add new Parent Team (when flag=0) and Child Team(when flag=1) */
        $scope.addNewTeam = function (isTeamNameInvalid, isTeamDescriptionInvalid, isTeamBudgetInvalid, teamAddUpdateForm, flag, teamAddForm) {
            debugger;
          
            //alert(teamAddForm.$valid);
           // !isTeamNameInvalid && !isTeamDescriptionInvalid && !isTeamBudgetInvalid && $scope.myTeamMember.teamMembers.length > 0
            if (teamAddForm.$valid) {
                $scope.myTeamMember.teamMembers = $scope.teamMembersNew;
                /* when adding child team it's budget should be greater than parent team*/
                if (flag == 1) {
                    if ($scope.parentTeamBudget < $scope.teamAddUpdateModel.teamBudget) {
                        toaster.warning("Parent team budget should be greater than sub team budget. ");
                        return;
                    }
                }
                if ($scope.myTeamMember.teamMembers.length == 0) {
                    toaster.warning("Atleast one member is required in  a team. ");
                    return;
                }
                $scope.MemberUserId = {};
                $scope.RoleId = {};
                $scope.MemberName = {};

                if (flag == 0) {
                    $scope.teamAddUpdateModel.ParentTeamId = 0;
                }
                if ($scope.myTeamMember.teamMembers.length > 0) {

                    angular.forEach($scope.myTeamMember.teamMembers, function (value, key) {
                        if ($scope.myTeamMember.teamMembers[key].selected == $scope.myTeamMember.teamMembers[key].UserId) {
                            debugger;
                            $scope.MemberUserId = $scope.myTeamMember.teamMembers[key].selected;
                            $scope.DesignationId = angular.element(document.querySelector("#selectDesignationId" + '-' + key)).controller("ng-model").$modelValue.RoleId;
                            var IsTeamLead = $scope.myTeamMember.teamMembers[key].radioSelected;
                            IsTeamLead = (IsTeamLead == undefined || IsTeamLead == '') ? false : true;
                            $scope.teamAddUpdateModel.teamMembers.push({ 'MemberUserId': $scope.MemberUserId, 'MemberDesignationId': $scope.DesignationId, 'IsTeamLead': IsTeamLead });
                        }
                    });
                }
                console.log($scope.teamAddUpdateModel);

                if (isTeamNameInvalid || isTeamDescriptionInvalid || isTeamBudgetInvalid) {
                    //alert("test")
                    //teamAddForm.teamName.$dirty = true;
                    //teamAddForm.teamDescription.$dirty = true;
                    //teamAddForm.teamBudget.$dirty = true;
                }
                else {

                    $('#globalLoader').show();

                    var NewTeamObj = { 'TeamModel': $scope.teamAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId }
                    teamFactory.insertTeam(NewTeamObj)
                        .success(function (data) {

                            if (data.ResponseData != 'DuplicateTeam') {
                                $scope.$emit('showMenu', []);
                                if (data.success) {
                                    $timeout(function () {
                                        $scope.$apply();

                                        /* Getting Teams in which user is Team Member */
                                        teamFactory.getLoggedInUserTeams(sharedService.getUserId(), $scope.selectedCompanyId)
                                            .success(function (data) {
                                                if (data.success) {
                                                    $scope.TeamsObject = data.ResponseData;
                                                    //$scope.CompanyTeamsObject = data.responseData;
                                                    $scope.activeTeam = $scope.TeamsObject[0];
                                                    // Updating the team dropdown
                                                    $scope.$emit('bindTeamDropdown', []);
                                                }
                                                else {
                                                    toaster.error("Error", data.message);
                                                }

                                            })
                                            .error(function () {
                                                toaster.error("Error", "Some Error Occured !");
                                            });
                                    });

                                    $timeout(function () {
                                        $scope.$apply();
                                        $scope.getCompanyTeams();
                                    });

                                    $timeout(function () {
                                        $scope.$apply();
                                        $('#modalTeamAddUpdate').modal('hide');
                                        $('#modalSubTeamAddUpdate').modal('hide');
                                        $('#globalLoader').hide();
                                        toaster.success("Success", message[data.ResponseData]);

                                        teamAddUpdateForm.teamName.$dirty = false;
                                        teamAddUpdateForm.teamDescription.$dirty = false;
                                        teamAddUpdateForm.teamBudget.$dirty = false;
                                        $scope.teamAddUpdateModel.teamMembers = [];
                                    });
                                }
                                else {
                                    toaster.error("Error", data.message);
                                    $('#globalLoader').hide();
                                }
                            }
                            else {
                                toaster.warning("Warning", message[data.ResponseData]);
                                $('#globalLoader').hide();
                            }

                        })
                        .error(function (error) {
                            toaster.error("Error", "Some Error Occured!");
                            $('#globalLoader').hide();
                        });
                }
            }
            else {
                $scope.showMsgs = true;
            }
        };


        /*Edit Team*/
        $scope.editTeam = function (teamObject, flag) {
            $scope.editmode = 1;
            $scope.flag = flag;
            $scope.teamButton = true;
            $scope.teamAddUpdateModel = {};
            $scope.myTeamMember.teamName = teamObject.TeamName;
            $scope.myTeamMember.teamBudget = teamObject.TeamBudget;
            $scope.myTeamMember.teamDescription = teamObject.TeamDescription;
            $scope.myTeamMember.teamId = teamObject.TeamId;
            $scope.teamAddUpdateModel.teamName = teamObject.TeamName;
            $scope.teamAddUpdateModel.teamBudget = teamObject.TeamBudget;
            $scope.teamAddUpdateModel.teamDescription = teamObject.TeamDescription;
            $scope.teamAddUpdateModel.teamId = teamObject.TeamId;

            var teamMembers = [];
            if (teamObject.TeamMembers.length > 0) {
                for (var i = 0; i < teamObject.TeamMembers.length; i++) {
                    teamMembers.push({ 'MemberUserId': teamObject.TeamMembers[i].MemberUserId, 'MemberDesignationId': teamObject.TeamMembers[i].MemberDesignationId, 'IsTeamLead': teamObject.TeamMembers[i].IsTeamLead });
                }
            }
            // Already added team members list
            $scope.teamAddUpdateModel.teamMembers = teamMembers;

            if ($scope.flag == 0) {
                $scope.teamAddUpdateModel.ParentTeamId = 0;

                // calling  method to get all the team memberes
                $scope.getAllUsers();

                // adding all the team members
                //$scope.myTeamMember.teamMembers = $scope.UsersObject;

            }
            if ($scope.flag == 1) {
                //gets all teams that are parent 
                getAllTeams();
                angular.element(document.querySelector("#selectTeamId")).val($scope.teamModal.TeamId);
                $scope.teamAddUpdateModel.parentTeamId = $scope.teamModal.TeamId;
            }
            debugger;



        };


        /*Update Team*/
        $scope.updateTeam = function (isTeamNameInvalid, isTeamDescriptionInvalid, isTeamBudgetInvalid, teamAddUpdateForm, flag) {
            
            $scope.MemberUserId = {};
            $scope.RoleId = {};
            $scope.DesignationId = {};
            $scope.MemberName = {};
            //  $scope.teamAddUpdateModel.teamMembers = '';
            $scope.teamAddUpdateModel.teamMembers = [];
            if ($scope.myTeamMember.teamMembers.length == 0) {
                toaster.warning("Atleast one member is required in  a team. ");
                return;
            }
            if ($scope.myTeamMember.teamMembers.length > 0) {
                angular.forEach($scope.myTeamMember.teamMembers, function (value, key) {
                    if ($scope.myTeamMember.teamMembers[key].selected == $scope.myTeamMember.teamMembers[key].UserId) {

                        $scope.MemberUserId = $scope.myTeamMember.teamMembers[key].selected;
                        $scope.DesignationId = angular.element(document.querySelector("#selectDesignationId" + '-' + key)).val();

                        var IsTeamLead = $scope.myTeamMember.teamMembers[key].radioSelected;
                        IsTeamLead = (IsTeamLead == undefined || IsTeamLead == '') ? false : true;
                        $scope.teamAddUpdateModel.teamMembers.push({ 'MemberUserId': $scope.MemberUserId, 'MemberDesignationId': $scope.DesignationId, 'IsTeamLead': IsTeamLead });
                    }


                });
            }
            
            if (isTeamNameInvalid || isTeamDescriptionInvalid || isTeamBudgetInvalid) {
                teamAddUpdateForm.teamName.$dirty = true;
                teamAddUpdateForm.teamDescription.$dirty = true;
                teamAddUpdateForm.teamBudget.$dirty = true;
            }
            else {
                var NewTeamObj = { 'TeamModel': $scope.teamAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId }
                $('#globalLoader').show();

                teamFactory.updateTeam(NewTeamObj)
                    .success(function (data) {
                        if (data.success) {
                            if (data.ResponseData != 'DuplicateTeam') {

                                if (data.ResponseData == "TeamProjectsBudgetExceed") {
                                    toaster.warning("Warning", message["TeamProjectsBudgetExceed"]);
                                    $('#globalLoader').hide();
                                    return;
                                }

                                toaster.success("Success", data.ResponseData);
                                $('#modalTeamAddUpdate').modal('hide');
                                $('#modalSubTeamAddUpdate').modal('hide');

                                // $scope.$emit('bindTeamDropdown', []);
                                /* Getting Teams in which user is Team Member */
                                teamFactory.getLoggedInUserTeams(sharedService.getUserId(), $scope.selectedCompanyId)
                                    .success(function (data) {
                                        if (data.success) {

                                            $scope.TeamsObject = data.ResponseData;
                                            $scope.activeTeam = $scope.TeamsObject[0];
                                        }
                                        else {
                                            toaster.error("Error", data.message);
                                        }
                                    })
                                    .error(function () {
                                        toaster.error("Error", "Some Error Occured !");
                                    });

                                /* Getting Company Teams with Users */
                                $scope.getCompanyTeams();
                                $('#globalLoader').hide();
                            }
                            else {
                                toaster.warning("Warning", message[data.ResponseData]);
                                $('#globalLoader').hide();
                            }
                        }
                        else {
                            toaster.error("Error", data.message);
                            $('#globalLoader').hide();
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();
                    });
            }
        };

        /*Delete Team*/
        $scope.DeleteTeam = function (teamId) {
            //alert(teamId + " " + index)
            var index = -1;
           
            $confirm({ text: 'Are you sure you want to delete this Team?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
           
                .then(function () {
                    index = -1;
                    debugger;
                    for (var i = 0; i < $scope.CompanyTeamsObject.length; i++) {
                        if ($scope.CompanyTeamsObject[i].TeamId == teamId) {
                            
                            index = i;
                            break;
                        }
                        for (var j = 0; j < $scope.CompanyTeamsObject[i].SubTeamList.length; j++) {
                            if ($scope.CompanyTeamsObject[i].SubTeamList[j].TeamId == teamId) {
                                index = i;
                                break;
                            }
                        }
                    }
                   
                        $('#globalLoader').show();
                        teamFactory.deleteTeam(teamId)
                            .success(function (data) {
                                debugger;
                                if (data.success) {
                                    
                                    toaster.success("Success", message[data.message]);
                                    $scope.isTeamEditDivOpend = false;
                                    $scope.teamAddUpdateModel = {};
                                    $scope.myTeamMember.teamMembers = [];
                                    $scope.CompanyTeamsObject.splice(index, 1);
                                    $scope.showTeamManagementSection();
                                    $scope.Role = sharedService.getRoleId();
                                    /* Getting Teams in which user is Team Member */
                                    teamFactory.getLoggedInUserTeams(sharedService.getUserId(), $scope.selectedCompanyId)
                                        .success(function (data) {
                                            if (data.success) {
                                                $scope.TeamsObject = data.ResponseData;
                                                $scope.activeTeam = $scope.TeamsObject[0];
                                                // Updating the team dropdown
                                                $scope.$emit('bindTeamDropdown', []);
                                                $scope.$emit('bindUserDetail', []);
                                                $('#globalLoader').hide();
                                            }
                                            else {
                                                toaster.error("Error", data.message);
                                                $('#globalLoader').hide();
                                            }
                                        })
                                        .error(function () {
                                            toaster.error("Error", "Some Error Occured !");
                                            $('#globalLoader').hide();
                                        });
                                }
                                else {
                                    if (data.message == "ProjectPresentInTeam") {
                                        toaster.warning("Warning", message[data.message]);
                                        $('#globalLoader').hide();
                                    }
                                    else {
                                        toaster.error("Error", data.message);
                                        $('#globalLoader').hide();
                                    }
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured!");
                                $('#globalLoader').hide();
                            });
                    

                    })
                   
        };
        /*View Team Detail*/
        $scope.ViewTeamDetail = function (teamId) {
            $window.location.href = ('/#/team/teamdetail/' + teamId);///'+teamId
        };
        ///*CHange Selected Team */
        //$scope.changeSelectedTeam = function (activeTeamObject) {

        //    closeCentreSections();
        //    $('#divTeam').show();
        //    $('#globalLoader').show();
        //    $scope.activeTeam = activeTeamObject;
        //    $scope.teamName = activeTeamObject.teamName;
        //    $scope.Users = {};
        //    $scope.Users.teamId = activeTeamObject.teamId;
        //    $scope.Users.updateflag = 2;
        //    usersFactory.updateUser($scope.Users).success(function () {

        //getProjects();

        //        $timeout(function () {
        //            $scope.$apply();
        //            $('#globalLoader').hide();
        //        });
        //    });
        //};

        /* Configuration of selectize for adding teammembers from Team Add Update Modal */
        //$scope.teamMembersConfig = {
        //    create: false,
        //    // maxItems: 1,
        //    required: true,
        //    plugins: ['remove_button'],
        //    valueField: 'Value',
        //    labelField: 'Text',
        //    // delimiter: '|',
        //    placeholder: 'Pick Team Members...',
        //    render: {
        //        option: function (item, escape) {

        //            var caption = item.Text;
        //            var iconByType = "fa fa-compress";
        //            if (item.IsFreelancer == true) {
        //                iconByType = "fa fa-expand";
        //            }

        //            return '<div>' +
        //                '<span >  <i class="' + iconByType + '" aria-hidden="true"></i>   </span> ' +
        //                (caption ? '<span>' + escape(caption) + '</span>' : '') +
        //                '</div>';


        //            //var MemberProfilePhoto = "<img title='" + item.FirstName + "' ng-src='" + $scope.imgURL + item.ProfilePhoto + "'>";
        //            //return '<div>' +
        //            //'<span >  <i class="' + iconByType + '" aria-hidden="true"></i>   </span> ' + MemberProfilePhoto +
        //            //(caption ? '<span>' + escape(caption) + '</span>' : '') +
        //            //'</div>';


        //        }
        //    }

        //}

        angular.element(document).ready(function () {

            $scope.showTeamManagementSection();
            $scope.Role = sharedService.getRoleId();

        });

        /*Function to reset job profile dropdown when checkbox is un checked */
        $scope.toggleSelection = function toggleSelection(item, index) {
            debugger;
            if (item.selected == '') {
                $scope.designationgModalList[index] = { "RoleId": '' };

            }
        };

        /* Function to reset radioSelected property of all radio buttons except the selected one*/
        $scope.toggleRadioSelection = function (item, index) {
            console.log('function called');
            debugger;

            for (var i = 0; i < $scope.myTeamMember.teamMembers.length; i++) {
                //if (i != index) {
                //    $scope.myTeamMember.teamMembers[i].radioSelected = "";
                //}
                //else {
                //    $scope.myTeamMember.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].UserId;
                //}
                if ($scope.myTeamMember.teamMembers[i].UserId == item.UserId) {
                    $scope.myTeamMember.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].UserId;
                    break;
                }
                //else {
                //    $scope.myTeamMember.teamMembers[i].radioSelected = "";

                //}
            }
           
            for (var i = 0; i < $scope.teamMembersNew.length; i++) {
                if ($scope.teamMembersNew[i].colId == item.colId) {
                    $scope.teamMembersNew[i].radioSelected = $scope.teamMembersNew[i].UserId;
                    break;
                }
                //else {
                //    $scope.teamMembersNew[i].radioSelected = "";
                //}
            }
            console.log($scope.teamMembersNew);
        };

        $scope.changePageSize = function (pageSizeSelected) {
            $scope.pageSizeSelected = pageSizeSelected.PageSize;
            $scope.showTeamManagementSection();
        }

        $scope.pageChanged = function (pageIndex) {
            $scope.PageIndex = pageIndex;
            $scope.showTeamManagementSection();
        }

        /*Get All Users  */
        $scope.getAllUsers = function () {
            $('#globalLoader').show();
            vendorFactory.GetAllNewUsers($scope.selectedCompanyId, sharedService.getUserId())
                .success(function (data) {
                    if (data.success && data.ResponseData != undefined) {
                        debugger;
                        $scope.UsersObject = data.ResponseData;
                        $scope.AllInternalUsersForTeam = data.ResponseData;
                        $scope.myTeamMember.teamMembers = [];
                        $scope.myTeamMember.teamMembers = $scope.UsersObject;

                        $scope.teamButton = false;
                        getAllDesignation();
                        $scope.showCenterLoader = false;

                        $('#modalTeamAddUpdate').modal('show');
                        $('#divTeamManagementSection').show();

                        $('#globalLoader').hide();

                        /*If in edit mode for parent team*/
                        if ($scope.editmode = 1) {
                            if ($scope.myTeamMember.teamMembers.length > 0) {
                                angular.forEach($scope.myTeamMember.teamMembers, function (value, key) {

                                    for (var i = 0; i < $scope.teamAddUpdateModel.teamMembers.length; i++) {
                                        debugger
                                        // comparing the already added member in the list of all members
                                        if ($scope.teamAddUpdateModel.teamMembers[i].MemberUserId == $scope.myTeamMember.teamMembers[key].UserId) {
                                            $scope.myTeamMember.teamMembers[key].selected = $scope.teamAddUpdateModel.teamMembers[i].MemberUserId;
                                            $scope.myTeamMember.teamMembers[key].designationSelected = $scope.teamAddUpdateModel.teamMembers[i].MemberDesignationId;
                                            if ($scope.teamAddUpdateModel.teamMembers[i].IsTeamLead) {
                                                $scope.myTeamMember.teamMembers[key].radioSelected = $scope.teamAddUpdateModel.teamMembers[i].MemberUserId;
                                            }

                                            break;
                                        }
                                    }


                                    //var IsTeamLead = $scope.myTeamMember.teamMembers[key].radioSelected;
                                    //IsTeamLead = (IsTeamLead == undefined) ? false : true;
                                  
                                    var RoleId = $scope.myTeamMember.teamMembers[key].designationSelected;
                                    $scope.designationgModalList[key] = { "RoleId": (RoleId == undefined) ? '' : RoleId };
                                });
                                $scope.$apply();
                            }

                            /*
                             $timeout to schedule the changes to the scope in a future call stack. By providing a timeout period of 0ms,
                             this will occur as soon as possible and $timeout will ensure that the code will be called in a single $apply block.
                             */

                            //  setTimeout(function () {
                            $timeout(function () {
                                if ($scope.flag == 0) {  /*0 means update team 1 means update sub team*/
                                    $('#modalTeamAddUpdate').modal('show');
                                }
                                else {
                                    $('#modalSubTeamAddUpdate').modal('show');

                                }
                                // }, 2000)
                            }, 0)
                        }

                    }
                    else {
                        toaster.error("Error", data.message);
                        $('#globalLoader').hide();

                    }
                })
                .error(function () {
                    toaster.error("Error", "Some error occured");
                    $('#globalLoader').hide();

                });
        }

        /*Get All Designation*/
        function getAllDesignation() {
            $('#globalLoader').show();
            teamFactory.getAllDesignation()
                .success(function (data) {
                    debugger
                    $scope.getAllDesignationList = data.ResponseData;
                    $('#globalLoader').hide();

                    if ($scope.flag == 1) {
                        $scope.IsVisible = true;
                        angular.element(document.querySelector("#divMembers")).removeClass("ng-hide");
                        $scope.teamButton = false;
                        $scope.showCenterLoader = false;
                        /*
                         $timeout to schedule the changes to the scope in a future call stack. By providing a timeout period of 0ms,
                         this will occur as soon as possible and $timeout will ensure that the code will be called in a single $apply block.
                         */
                        //$scope.designationgModalList[0] = { "RoleId": '' };
                        //angular.element(document.querySelector("#selectTeamId")).val('');
                        $timeout(function () {
                            $('#globalLoader').hide();

                            $('#modalSubTeamAddUpdate').modal('show');
                        }, 1000)
                        $('#globalLoader').hide();
                    }
                }).error(function () {
                    toaster.error("Error", "Some error occured");
                    $('#globalLoader').hide();

                });
        }

        /*Get All Roles by company id */
        function getusersRole() {
            $('#globalLoader').show();
            vendorFactory.GetNewUserRole($scope.selectedCompanyId)
                .success(function (data) {
                    $scope.getUserRoleList = data.ResponseData;
                    $('#globalLoader').hide();

                })
        }

        $scope.parentTeamBudget = {};
        /*Showing Child Team Add Update Modal Pop Up*/
        $scope.showChildTeamAddUpdateModal = function (teamId, parentTeamBudget) {
            debugger;
            $scope.flag = 1;
            $scope.teamAddUpdateModel = {};
            $scope.teamAddUpdateModel.teamMembers = [];
            $scope.parentTeamBudget = parentTeamBudget;
            $scope.teamAddUpdateModel.ParentTeamId = teamId;
            $scope.getTeamMembersByTeamId();
            $("input[name='teamName']").removeClass("inputError");
            $("textarea[name='teamDescription']").removeClass("inputError");
            $("input[name='teamBudget']").removeClass("inputError");
        };

        /*Get teamMembers of selected parent team for adding a sub team*/
        $scope.getTeamMembersByTeamId = function () {
            debugger;
            if ($scope.teamAddUpdateModel.ParentTeamId != undefined) {
                $('#globalLoader').show();
                teamFactory.getTeamMembersByTeamId(sharedService.getCompanyId(), $scope.teamAddUpdateModel.ParentTeamId)
                    .success(function (data) {
                        if (data.success && data.ResponseData != undefined) {
                            debugger;
                            getAllDesignation();

                            $scope.UsersObject = data.ResponseData;
                            $scope.myTeamMember.teamMembers = [];
                            $scope.myTeamMember.teamMembers = $scope.UsersObject;
                            $scope.getAllDesignationList = {};

                            //getAllTeams();
                            //$scope.designationgModalList[0] = { "RoleId": '' };
                            // angular.element(document.querySelector("#selectTeamId")).val('');
                            //$scope.IsVisible = false;


                            //$('#globalLoader').hide();
                        }
                        else {
                            toaster.error("Error", data.message);
                            $scope.IsVisible = false;
                            angular.element(document.querySelector("#divMembers")).addClass("ng-hide");
                            $('#globalLoader').hide();

                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some error occured");
                        $scope.IsVisible = false;
                        angular.element(document.querySelector("#divMembers")).addClass("ng-hide");

                    });
            }
            else {
                toaster.error("Please select team");
                $scope.IsVisible = true;
                angular.element(document.querySelector("#divMembers")).addClass("ng-hide");

            }

        }

        ///*Get All Teams CompanyId and UserId Wise */
        //function getAllTeams() {
        //    $('#globalLoader').show();
        //    teamFactory.getAllTeams(sharedService.getCompanyId(), sharedService.getUserId())
        //        .success(function (data) {
        //            $scope.getAllTeamList = data.ResponseData;
        //            $('#globalLoader').hide();

        //        })
        //}

        ///*Get teamMembers of selected parent team for adding a sub team*/
        //$scope.getTeamMembersByTeamId = function (teamModal) {
        //    if (angular.isDefined(teamModal)) {
        //        $('#globalLoader').show();
        //        $scope.teamModal.TeamId = teamModal.TeamId;
        //        teamFactory.getTeamMembersByTeamId(sharedService.getCompanyId(), $scope.teamModal.TeamId)
        //            .success(function (data) {
        //                if (data.success) {
        //                    debugger;
        //                    $scope.IsVisible = true;
        //                    angular.element(document.querySelector("#divMembers")).removeClass("ng-hide");
        //                    $scope.UsersObject = data.ResponseData;
        //                    $scope.myTeamMember.teamMembers = data.ResponseData;
        //                    getAllDesignation();
        //                    $scope.showCenterLoader = false;
        //                }
        //                else {
        //                    toaster.error("Error", data.message);
        //                    $scope.IsVisible = false;
        //                    angular.element(document.querySelector("#divMembers")).addClass("ng-hide");

        //                }
        //            })
        //            .error(function () {
        //                toaster.error("Error", "Some error occured");
        //                $scope.IsVisible = false;
        //                angular.element(document.querySelector("#divMembers")).addClass("ng-hide");

        //            });
        //    }
        //    else {
        //        toaster.error("Please select team");
        //        $scope.IsVisible = true;
        //        angular.element(document.querySelector("#divMembers")).addClass("ng-hide");

        //    }

        //}

        /*Code to show sub team div corresponding to parent team div*/
        $scope.activeParentIndex;
        $scope.showKids = function (index, ParentTeamId) {
            debugger;
            $scope.activeParentIndex = index;
            $scope.teamAddUpdateModel.ParentTeamId = ParentTeamId;
            $scope.isShowing(index);
            getCustomControl(ParentTeamId);
            $scope.getTeamMembersByTeamId();
            //$scope.teamModal.TeamId = ParentTeamId;
            //$scope.getTeamMembersByTeamId($scope.teamModal);
        };

        $scope.isShowing = function (index) {
            debugger;
            return $scope.activeParentIndex === index;
        };

        /*Set type whether its a role,designation or both*/
        //$scope.handleRadioClick = function (item) {
        //    $scope.roleAddUpdateModel.Type = item.value;
        //};


        /*work for tree view starts here*/

        /* Function to reset radioSelected property of all radio buttons except the selected one*/
        $scope.memberToggleRadioSelection = function (item, index) {
            debugger;
            for (var i = 0; i < $scope.myTeamMember.teamMembers.length; i++) {
                if (i == index) {
                    $scope.myTeamMember.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].MemberUserId;
                }
                //if ($scope.myTeamMember.teamMembers[i].UserId == item.UserId) {
                //    $scope.myTeamMember.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].UserId;
                //    break;
                //}
                //else {
                //    $scope.myTeamMember.teamMembers[i].radioSelected = "";

                //}
            }

        };


        $scope.CompanyTeamsInTreeViewList = []

        //test tree model 1

        getAllDesignation()//designation for edit team

        $scope.$watch('TeamTree.currentNode', function (newObj, oldObj) {
            //debugger;
            if ($scope.TeamTree && angular.isObject($scope.TeamTree.currentNode)) {
                $scope.isTeamEditDivOpend = true;
                $scope.teamAddUpdateModel = {};
                $scope.myTeamMember.teamMembers = [] // empty team member
                $scope.teamAddUpdateModel.teamName = $scope.TeamTree.currentNode.TeamName;
                $scope.teamAddUpdateModel.teamBudget = $scope.TeamTree.currentNode.TeamBudget;
                $scope.teamAddUpdateModel.teamDescription = $scope.TeamTree.currentNode.TeamDescription;
                $scope.teamAddUpdateModel.teamId = $scope.TeamTree.currentNode.TeamId;
                $scope.teamAddUpdateModel.teamCreateDate = $filter('date')($scope.TeamTree.currentNode.TeamCreatedDate, "dd-MM-yyyy");// $scope.TeamTree.currentNode.TeamCreatedDate;
                $scope.myTeamMember.ParentTeamId = $scope.TeamTree.currentNode.ParentTeamId

                $scope.myTeamMember.teamMembers = $scope.TeamTree.currentNode.TopTeamMembersList;

                //var topTeamMemberList = [];
                var nestedTeamMemberList = [];
                //topTeamMemberList = $scope.TeamTree.currentNode.TopTeamMembersList
                nestedTeamMemberList = $scope.TeamTree.currentNode.TeamMembersList;

                RadiobuttonselectDy()
                function RadiobuttonselectDy() {
                    debugger
                    for (var i = 0; i < $scope.myTeamMember.teamMembers.length; i++) {
                        if ($scope.myTeamMember.teamMembers[i].IsTeamLead) {
                            $scope.myTeamMember.teamMembers[i].radioSelected = $scope.myTeamMember.teamMembers[i].MemberUserId;
                        }

                        $scope.myTeamMember.teamMembers[i].selected = $scope.myTeamMember.teamMembers[i].MemberUserId;
                    }
                }

                if ($scope.myTeamMember.teamMembers.length > 0) {// && $scope.TeamTree.currentNode.ParentTeamId != 0
                    angular.forEach($scope.myTeamMember.teamMembers, function (value, key) {
                        for (var i = 0; i < nestedTeamMemberList.length; i++) {
                            debugger
                            // comparing the already added member in the list of all members
                            if ($scope.myTeamMember.teamMembers[key].MemberUserId == nestedTeamMemberList[i].MemberUserId) {
                                $scope.myTeamMember.teamMembers[key].selected = nestedTeamMemberList[i].MemberUserId;
                                $scope.myTeamMember.teamMembers[key].MemberDesignationId = nestedTeamMemberList[i].MemberDesignationId;
                                if ($scope.myTeamMember.teamMembers[i].IsTeamLead) {
                                    $scope.myTeamMember.teamMembers[key].radioSelected = nestedTeamMemberList[i].MemberUserId;
                                }

                                break;
                            }
                        }
                    })
                }

            }
        }, false);


        $scope.isTeamEditDivOpend = false;
        $scope.IsEditTeam = false;
        $scope.IsEditShowBtn = true;
        $scope.EditTeamEnable = function () {
            $scope.IsEditTeam = true;
            $scope.IsEditShowBtn = false;
            if ($scope.disabledAnchor) { return false; }
        }

        $scope.createNestedTeam = function () {

            //   getAllDesignation();
            $scope.showNestedMsgs = false;
            $scope.NestedTeamAddUpdateModel = {};
            $('#globalLoader').show();
            vendorFactory.GetAllNewUsers($scope.selectedCompanyId, sharedService.getUserId())
                .success(function (data) {
                    if (data.success && data.ResponseData != undefined) {
                        debugger;
                        $scope.UsersObject = data.ResponseData;
                        $scope.AllInternalUsersForTeam = [];
                        $scope.myTeamMember.teamMembers = [];
                        $scope.myTeamMember.teamMembers = $scope.UsersObject;
                        $('#globalLoader').hide();
                    }
                    else {
                        $('#globalLoader').hide();
                    }
                });
            $scope.columns = [];
            $scope.teamMembersNew = [];
            $scope.openSaveRoleTags = false;
            var multiSelectPreviousData = [];
            var multiSelectCurrentData = [];
            $('#modalTeamNestedAdd').modal('show');

        }

        $scope.addNewNestedTeam = function (NestedTeamAddUpdateModel,teamAddUpdateForm) {
            debugger;
            if (teamAddUpdateForm.$valid) {
                var teamAddUpdateModel = {
                    TeamId: 0, TeamName: "", TeamBudget: 0, TeamDescription: "", ParentId: 0,
                    ParentTeamId: 0, TeamMembers: []
                };

                teamAddUpdateModel.TeamId = 0 // auto 
                teamAddUpdateModel.TeamName = NestedTeamAddUpdateModel.teamName;
                teamAddUpdateModel.TeamBudget = NestedTeamAddUpdateModel.teamBudget;
                teamAddUpdateModel.TeamDescription = NestedTeamAddUpdateModel.teamDescription;
                teamAddUpdateModel.ParentTeamId = $scope.teamAddUpdateModel.teamId;



                angular.forEach($scope.myTeamMember.teamMembers, function (value, key) {
                    debugger;

                    if ($scope.myTeamMember.teamMembers[key].selected == $scope.myTeamMember.teamMembers[key].MemberUserId) {
                        var MemberUserId = $scope.myTeamMember.teamMembers[key].selected;
                        var DesignationId = 0;
                        //alert(angular.element(document.querySelector("#selectDesignationId" + '-' + key)).controller("ng-model").$modelValue.RoleId);
                        //var DesignationId = angular.element(document.querySelector("#selectDesignationId" + '-' + key)).controller("ng-model").$modelValue.RoleId;
                        var IsTeamLead = $scope.myTeamMember.teamMembers[key].radioSelected;
                        var MemberRoleId = value.MemberRoleId != null ? value.MemberRoleId : 0;
                        IsTeamLead = (IsTeamLead == undefined || IsTeamLead == '') ? false : true;
                        teamAddUpdateModel.TeamMembers.push({ 'MemberUserId': MemberUserId, 'MemberDesignationId': DesignationId, 'MemberRoleId': MemberRoleId, 'IsTeamLead': IsTeamLead });
                    }
                })


                var NewTeamObj = { 'TeamModel': teamAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId }
                teamFactory.insertTeam(NewTeamObj)
                    .success(function (data) {
                        $('#globalLoader').hide();
                        $scope.getCompanyTeams();
                        $('#modalTeamNestedAdd').modal('hide');
                        teamAddUpdateModel = {};
                        $scope.myTeamMember.teamMembers = [] // empty team member
                    })
                    .error(function (error) {
                        toaster.error("Error", "Some Error Occured!");
                        $('#globalLoader').hide();
                    });
            }
            else {
                $scope.showNestedMsgs = true;
            }

        }

        $scope.updateAllLevelTeam = function () {


            var teamAddUpdateModel = {
                TeamId: 0, TeamName: "", TeamBudget: 0, TeamDescription: "", ParentId: 0,
                ParentTeamId: 0, TeamMembers: []
            };


            teamAddUpdateModel.TeamName = $scope.teamAddUpdateModel.teamName;
            teamAddUpdateModel.TeamBudget = $scope.teamAddUpdateModel.teamBudget;
            teamAddUpdateModel.TeamDescription = $scope.teamAddUpdateModel.teamDescription;
            teamAddUpdateModel.TeamId = $scope.teamAddUpdateModel.teamId;
            teamAddUpdateModel.ParentTeamId = $scope.myTeamMember.ParentTeamId;


            angular.forEach($scope.myTeamMember.teamMembers, function (value, key) {
                debugger;

                if ($scope.myTeamMember.teamMembers[key].selected == $scope.myTeamMember.teamMembers[key].MemberUserId) {
                    var MemberUserId = $scope.myTeamMember.teamMembers[key].selected;
                    var DesignationId = angular.element(document.querySelector("#selectDesignationId" + '-' + key)).controller("ng-model").$modelValue.RoleId;
                    var IsTeamLead = $scope.myTeamMember.teamMembers[key].radioSelected;
                    var MemberRoleId = value.MemberRoleId != null ? value.MemberRoleId : 0;
                    IsTeamLead = (IsTeamLead == undefined || IsTeamLead == '') ? false : true;
                    teamAddUpdateModel.TeamMembers.push({ 'MemberUserId': MemberUserId, 'MemberDesignationId': DesignationId, 'MemberRoleId': MemberRoleId, 'IsTeamLead': IsTeamLead });
                }
            })


            var NewTeamObj = { 'TeamModel': teamAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId }
            $('#globalLoader').show();

            teamFactory.updateTeam(NewTeamObj)
                .success(function (data) {
                    if (data.success) {


                        if (data.ResponseData != 'DuplicateTeam') {

                            if (data.ResponseData == "TeamProjectsBudgetExceed") {
                                toaster.warning("Warning", message["TeamProjectsBudgetExceed"]);
                                $('#globalLoader').hide();
                                return;
                            }

                            toaster.success("Success", data.ResponseData);
                            $('#modalTeamAddUpdate').modal('hide');
                            $('#modalSubTeamAddUpdate').modal('hide');
                            // $scope.$emit('bindTeamDropdown', []);
                            /* Getting Teams in which user is Team Member */

                            $scope.getCompanyTeams();
                            $('#globalLoader').hide();
                        }
                    }
                    else {
                        toaster.error("Error", data.message);
                        $('#globalLoader').hide();
                    }

                    $scope.IsEditShowBtn = true;
                    $scope.IsEditTeam = false;

                })
        }

        /*work for tree view ends here*/

        /* code for selectize for team users starts*/
        $scope.openSaveRoleTags = false;
        var multiSelectPreviousData = [];
        var multiSelectCurrentData = [];


        /*  This function is used to edit team members Tags  */
        $scope.editTeamMembersTags = function () {
            debugger;
            multiSelectCurrentData = $scope.AllInternalUsersForTeam;
            for (var i = 0; i < multiSelectCurrentData.length; i++) {
                multiSelectPreviousData.push(multiSelectCurrentData[i]);
            }

            $scope.openSaveRoleTags = true;
            $timeout(function () {
                $('#TeamMemberTags  .host .tags .input').focus();
            }, 100);
        }
        $scope.teamMembersNew = [];

        $scope.tagAdded = function (tag) {
            $scope.addNewColumn(tag);
        }

        $scope.tagRemoved = function (tag) {
            var index = $scope.columns.indexOf(tag);
            $scope.removeColumn(index);
        }
  
        $scope.columns = [];

        $scope.addNewColumn = function (tag) {
            var newItemNo = $scope.columns.length + 1;
            $scope.columns.push({ 'colId': 'col' + newItemNo, 'name': tag.Name, 'selected': tag.UserId, 'UserId': tag.UserId });
            $scope.teamMembersNew.push({ 'colId': 'col' + newItemNo, 'Description': tag.Description, 'Name': tag.Name, 'RoleId': tag.RoleId, 'UserId': tag.UserId, 'selected': tag.UserId });
        };

        $scope.removeColumn = function (index) {
            $scope.columns.splice(index, 1);
            // if no rows left in the array create a blank array
            if ($scope.columns.length() === 0 || $scope.columns.length() == null) {
                alert('no rec');
                $scope.columns.push = [{ "colId": "col1" }];
            }

        }
        /* code for selectize for team users ends*/

    });







});
