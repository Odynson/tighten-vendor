﻿
define(['app'], function (app) {
    app.controller("quotedProjectAcceptOrRejectController", function ($scope, sharedService, $http, $rootScope, $location, toaster, $stateParams, projectsFactory, apiURL) {

        $scope.getProjectTOQuoteList = [];
        var CompanyId = sharedService.getCompanyId();
        var ProjectId = 0;

        var ProjectId = $stateParams.pid;
        var VendorId = $stateParams.vid;

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }

        if (ProjectId != 0) {

            getProjectQuotedByVendor(CompanyId, ProjectId, VendorId)
        }



        //////////////////////////////////////////////getProjectDetailForVendor///////////////////////////////////////////////////////////////////////
        function getProjectQuotedByVendor(CompanyId, ProjectId, VendorId) {
            debugger;
            $('#globalLoader').show();
            projectsFactory.getProjectQuotedByVendor(CompanyId, ProjectId, VendorId)

            .success(function (data) {
                debugger;
                $scope.getProjectDetailList = data.ResponseData;
                toaster.success("Success", message.successdataLoad);
                //------getting custom control area ---------//
                if (data.ResponseData != null) {
                    if (data.ResponseData.CustomControlValueModel != null) {
                        $scope.customcontrolmodel = JSON.parse(data.ResponseData.CustomControlValueModel.CustomControlData);


                    }
                }
                //-----------------Custom cuntrol is not use by Company---------------------------------------///

                $scope.CustomcontrolList = data.ResponseData.CustomControlUIList;
                $scope.CustomDirectiveReady = true;


                $('#globalLoader').hide();
            }),
            Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();
            })

        }


        var IsAcceptOrReject = -1;
        $scope.ReasonHead = ""
        $scope.acceptByvendor = function (index) {
            debugger;
            $scope.ReasonHead = " "
            IsAcceptOrReject = 1;
            $('#modalConfirmAcceptRejectQuote').modal('show')

        }


        $scope.rejectByvendor = function (index) {
            debugger;
            $scope.ReasonHead = "Enter the reason of rejection "
            IsAcceptOrReject = 0;
            $('#modalConfirmAcceptRejectQuote').modal('show')

        }
        $scope.saveReasonAcceptOrReject = function (Item) {
            debugger;

            var data = {}
            data.IsAcceptOrReject = IsAcceptOrReject;
            data.RejectOrAccept = Item.RejectOrAccept;
            data.ProjectId = ProjectId;
            data.VendorId = VendorId;
            data.CompanyId = CompanyId;
            data.ProjectName = $scope.getProjectDetailList.Name

            $('#globalLoader').show();
            projectsFactory.saveReasonAcceptOrRejct(data).success(function (data) {
                if (data.success) {
                    toaster.success("Success", "Data save Scuccessfully.");
                    $location.path('/quoted/project');

                } else {

                    toaster.error("error", message.error);
                }

                $('#globalLoader').hide();

            }), Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();
            })
        }




        //////////////////////////////////////////////////////modalMilestoneDetail///////////////////////////////////////////////////////////////////////
        $scope.PhaseIndex
        $scope.modalMilestoneAcceptReject = function (index) {
            debugger;
            $scope.PhaseIndex = index;
            $('#modalshowMilestoneAcceptOrReject').modal('show')

        }

        $scope.downloadVendorFile = function (FilePath, FileName, ActualFileName) {
            debugger;
            $('#globalLoader').show();

            var url = apiURL.baseAddress + "VendorApi/";

            $http({
                method: 'Post',
                url: url + "GetFiledownload",
                data: { FileName: FileName, FilePath: FilePath },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                headers = headers();
                debugger;
                //var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", ActualFileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    $('#globalLoader').hide();

                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    console.log(ex);
                    $('#globalLoader').hide();
                }
            }).error(function (data, status) {
                console.log(data);
                $('#globalLoader').hide();
            });
        };



    })

})