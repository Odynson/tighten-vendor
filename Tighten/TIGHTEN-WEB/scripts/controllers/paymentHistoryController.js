﻿
define(['app', 'paymentHistoryFactory'], function (app) {

    app.controller("paymentHistoryController", function ($scope, $rootScope, $http, sharedService, toaster, apiURL, $location, paymentHistoryFactory, teamFactory, projectsFactory) {

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.imgURL = apiURL.imageAddress;
        $scope.isFreelancer = sharedService.getIsFreelancer();
        $scope.selectedCompanyId = sharedService.getCompanyId();
        $scope.selectedTeam = 0;
        $scope.selectedProject = 0;
        $scope.selectedFreelancer = 0;
        $scope.selectedCompanymodel = 0;

        //$scope.Role = sharedService.getRoleId();

        //if ($rootScope.activeTeam.TeamId == undefined) {
        //    $rootScope.activeTeam.TeamId = sharedService.getDefaultTeamId();
        //}

        //$scope.selectedTeamId = $rootScope.activeTeam.TeamId;


        $scope.paymentHistory = [];
        $scope.Type = 'All';

        // Company dropdown change event on left side menu for freelancer 
        $scope.$on('leftCompanyDropdownChange', function (event, data) {

            $scope.selectedCompanyId = data.CompanyId;
            getPaymentHistory('All');
        });

         

        $scope.selectedCompanyChanged = function (selectedCompany) {

            $scope.selectedCompanyId = selectedCompany;
            getPaymentHistory('All');
        };


        $scope.selectedTypeChanged = function (type) {

            if (type == "Invoice") {
                subscriptionType = "Invoice";
                bindTeams(sharedService.getCompanyId());
            }
            getPaymentHistory(type);
        }


        $scope.selectedTeamChanged = function (selectedTeam) {
            
            bindProjects(selectedTeam);
        };


        $scope.selectedProjectChanged = function (selectedProject) {

            bindFreelancers(selectedProject);
        };


        $scope.selectedFreelancerChanged = function (selectedFreelancer) {

            getPaymentHistory($scope.Type);
        };



        function bindTeams(compId) {

            teamFactory.getCompanyTeams(compId, sharedService.getUserId())
                .success(function (data) {
                if (data.success) {
                    $scope.TeamsObjectForFreelancer = data.ResponseData;
                    $scope.selectedTeam = 0;
                }
                else {
                    toaster.error("Error", data.message);
                }
            }).error(function () {
                toaster.error("Error", "Some Error Occured!");
            });
        };


        function bindProjects(TeamId) {

            projectsFactory.getProjectsForSelectedTeam(TeamId, sharedService.getCompanyId())
                .success(function (Projectsdata) {
                    if (Projectsdata.success) {
                        $scope.ProjectsObject = Projectsdata.ResponseData;
                        getPaymentHistory($scope.Type);
                    }
                    else {
                        toaster.error("Error", Projectsdata.message);
                    }
                }).error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        };



        function bindFreelancers(ProjectId) {

            paymentHistoryFactory.getFreelancersForSelectedProject(ProjectId, sharedService.getCompanyId())
                .success(function (Freelancersdata) {
                    if (Freelancersdata.success) {
                        $scope.FreelancersObject = Freelancersdata.ResponseData;
                        getPaymentHistory($scope.Type);
                    }
                    else {
                        toaster.error("Error", Freelancersdata.message);
                    }
                }).error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        };


        function getPaymentHistory(subscriptionType) {
            $('#globalLoader').show();
            paymentHistoryFactory.getpaymentHistory(sharedService.getUserId(), $scope.isFreelancer, $scope.selectedTeam, $scope.selectedCompanyId, subscriptionType, $scope.selectedProject, $scope.selectedFreelancer)
                .success(function (data) {
                    if (data.success) {
                        $scope.paymentHistory = data.ResponseData;
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", data.message);
                        $('#globalLoader').hide();
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                });
        }

        angular.element(document).ready(function () {

            getPaymentHistory($scope.Type);
        });


    });

});
