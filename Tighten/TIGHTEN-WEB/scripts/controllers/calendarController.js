﻿
define(['app'], function (app) {

    app.controller("calendarController", function ($scope, $rootScope, apiURL, $timeout, $filter, $http, sharedService, toaster, $confirm, uiCalendarConfig, CalendarEventsFactory) {

        $scope.imgURL = apiURL.imageAddress;
        $scope.addBtnVisible = true;

        $scope.events = [];
        $scope.calendarEventModel = {};
        $scope.allDayEvent = {events : [
            { id: '0', isAllDay: 'No' },
            { id: '1', isAllDay: 'Yes' }
        ]
        };


        $scope.popup1 = {
            opened: false
        };
        $scope.popup2 = {
            opened: false
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,

        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();


        /*increasing and decreasing range of Hours and minutes*/
        $scope.hstep = 1;
        $scope.mstep = 5;

        /*shows hours and date in 24hr format or in 12hr */
        $scope.ismeridian = true;
        $scope.toggleMode = function () {
            $scope.ismeridian = !$scope.ismeridian;
        };

        /*update hours and minutes for From Date */
        $scope.timeFromChanged = function () {
            
            var d_form = $scope.calendarEventModel.eventDateFrom;
            var dd_form = $scope.calendarEventModel.eventTimeFrom;
            var timeFromHours = $filter('date')(dd_form, "hh");
            var timeFromMinutes = $filter('date')(dd_form, "mm");
            d_form.setHours(timeFromHours);
            d_form.setMinutes(timeFromMinutes)
            $scope.calendarEventModel.eventDateFrom = d_form;
        };

        
        /*update hours and minutes For To Date*/
        $scope.timeToChanged = function () {
            
            var d_To = $scope.calendarEventModel.eventDateTo;
            var dd_To = $scope.calendarEventModel.eventTimeTo;
            var timeToHours = $filter('date')(dd_To, "hh");
            var timeToMinutes = $filter('date')(dd_To, "mm");
            d_To.setHours(timeToHours);
            d_To.setMinutes(timeToMinutes)
            $scope.calendarEventModel.eventDateTo = d_To;
        };


        /* event source that pulls from google.com */
        $scope.eventSource = {
            //url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            //className: 'gcal-event',           // an option!
            //currentTimezone: 'America/Chicago' // an option!
        };
        /* event source that calls a function on every view switch */
        $scope.eventsF = function (start, end, timezone, callback) {
            var s = new Date(start).getTime() / 1000;
            var e = new Date(end).getTime() / 1000;
            var m = new Date(start).getMonth();
            var events = [{ title: 'Feed Me ' + m, start: s + (50000), end: s + (100000), allDay: false, className: ['customFeed'] }];
            callback(events);
        };

        


       

        $scope.showCalendar = function () {

            $scope.events = [
                           //{ Title: 'All Day Event', Start: new Date(y, m, 1) },
                           //{ title: 'Long Event', start: new Date(y, m, d - 5), end: new Date(y, m, d - 2) },
                           //{ id: 999, title: 'Repeating Event', start: new Date(y, m, d - 3, 16, 0), allDay: false },
                           //{ id: 999, title: 'Repeating Event', start: new Date(y, m, d + 4, 16, 0), allDay: false },
                           //{ title: 'Birthday Party', start: new Date(y, m, d + 1, 19, 0), end: new Date(y, m, d + 1, 22, 30), allDay: false },
                           //{ title: 'Click for Google', start: new Date(y, m, 28), end: new Date(y, m, 29), url: 'http://google.com/' },
                           //{ AllDay: false, ClassName: null, CreatedDate: "0001-01-01T00:00:00", CreatorName: null, CreatorProfilePhoto: null, Description: null, Editable: false, End: "2016-06-20T09:00:00", Id: 0, Start: "2016-06-20T00:00:00", TeamId: 0, Title: "first", TodoId: 0, Type: null, Url: null}
            ];

            CalendarEventsFactory.getCalendarEvents(sharedService.getCompanyId(), sharedService.getUserId(), 2, 0)
            .success(function (data) {
                
                if (data.ResponseData.length > 0) {
                    for (var i = 0; i < data.ResponseData.length ; i++) {
                        var asd = { 'id': data.ResponseData[i]._id, 'title': data.ResponseData[i].Title, 'start': data.ResponseData[i].Start, 'end': data.ResponseData[i].End };
                        $scope.events.push(asd);
                    }
                }
                //$scope.events = data.ResponseData;
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured !");
            })

            $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
        };


        $scope.AddCalendarEvent = function () {
            
            $scope.calendarEventModel.UserId = sharedService.getUserId();
            $scope.calendarEventModel.TeamId = sharedService.getDefaultTeamId();
            $scope.calendarEventModel.CompanyId = sharedService.getCompanyId();
            CalendarEventsFactory.insertCalendarEvent($scope.calendarEventModel)
            .success(function (data) {
                $scope.calendarEventModel = {};
                $scope.showCalendar();
                toaster.success("Success", "New event is saved");
                $('#modalCalendarEvent').modal('hide');
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured !");
            })
        };


        $scope.UpdateCalendarEvent = function () {
            
            $scope.calendarEventModel.UserId = sharedService.getUserId();
            $scope.calendarEventModel.TeamId = sharedService.getDefaultTeamId();
            $scope.calendarEventModel.CompanyId = sharedService.getCompanyId();
            CalendarEventsFactory.updateCalendarEvent($scope.calendarEventModel)
            .success(function (data) {
                $scope.showCalendar();
                toaster.success("Success", "Event is updated Successfully !");
                $('#modalCalendarEvent').modal('hide');
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured !");
            })
        };


        $scope.dayClick = function (date, jsEvent, view) {
            
            $scope.addBtnVisible = true;
            //$scope.calendarEventModel = {};
            $scope.calendarEventModel.AllDay = 0;
            $scope.calendarEventModel.eventDateFrom = date._d;
            $('#modalCalendarEvent').modal('show');
        };

        $scope.OnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
            
            
            var data = { 'EventId': event._id, 'eventDateFrom': event.start._d, 'eventDateTo': event.end._d, 'UserId': sharedService.getUserId() };
            CalendarEventsFactory.updateCalendarEventOnResize(data)
            .success(function (data) {
                $scope.showCalendar();
                toaster.success("Success", "Event is updated Successfully !");
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured !");
            })

        };


        $scope.OnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {

            
            var startdate = event.start._d;
            var data = { 'EventId': event._id, 'eventDateFrom': event.start._d, 'eventDateTo': event.end._d , 'UserId': sharedService.getUserId() };
            CalendarEventsFactory.updateCalendarEventOnDrop(data)
            .success(function (data) {
                $scope.showCalendar();
                toaster.success("Success", "Event is updated Successfully !");
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured !");
            })
        };


        $scope.EventClick = function (event, delta, revertFunc, jsEvent, ui, view) {
            
            $scope.calendarEventModel = {};
            CalendarEventsFactory.getSingleCalendarEvent(event.id)
            .success(function (data) {
                $scope.calendarEventModel = data.ResponseData;
                $scope.calendarEventModel.AllDay = data.ResponseData.AllDay;
                $scope.calendarEventModel.eventDateFrom = data.ResponseData.Start;
                $scope.calendarEventModel.eventDateTo = data.ResponseData.End;
                $scope.calendarEventModel.eventTimeFrom = data.ResponseData.Start;
                $scope.calendarEventModel.eventTimeTo = data.ResponseData.End;

                $scope.addBtnVisible = false;
                $('#modalCalendarEvent').modal('show');
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured !");
            })

        };



        /* config object */
        $scope.uiConfig = {
            calendar: {
                height: 700,
                editable: true,
                weekends: true,
                header: {
                    left: 'today prev,next',
                    center: 'title',
                    right: 'month agendaWeek agendaDay '
                },
                dayClick: $scope.dayClick,
                eventClick: $scope.EventClick,
                eventDrop: $scope.OnDrop,
                eventResize: $scope.OnResize,
                eventRender: $scope.eventRender,
                viewRender: $scope.renderView,


            }
        };



        /* Change Calendar Weekend Option */
        $scope.calendarWeekendsFunction = function (status) {

            $timeout(function () {
                $scope.$apply();
                $scope.uiConfig.calendar.weekends = status;
            });
            $timeout(function () {
                $scope.$apply();
            });

        };




        /////////////////////////////////////////////
        /////////////////////////////////////////////
        angular.element(document).ready(function () {
            $scope.showCalendar();
        });

    });
});