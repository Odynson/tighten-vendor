﻿//var loginMod = angular.module("loginModule", []);

define(['app', 'ngCookies'], function (app) {
    app.controller("loginController", function ($scope, $rootScope, $timeout, $http, $cookies, $cookieStore, $location, $interval, toaster, AuthenticationFactory, FreelancerAccountFactory, rolePermissionFactory, sharedService, apiURL, userMessagesFactory) {
        $scope.email = '';
        $scope.password = '';
        $scope.Email = '';

        $('#headerMenu').hide();
        $('#sideMenu').parent().parent().hide();



        //$rootScope.navbar = true;
        AuthenticationFactory.ClearCredentials();

        /* on ForgotPassword Click */
        $scope.forgotPassword = function () {

            $('#modalForgotPassword').modal('show');
        };


        $scope.Dismiss = function (event) {
            event.preventDefault();
            $scope.signinForm.email.$dirty = false;
            $scope.signinForm.password.$dirty = false;
            $('#emailSentConfirmation').hide();
            $("#signin").addClass('animate0 flipInY').show();
            $timeout(removeClass, 700);
            function removeClass() {
                $('#signin').removeClass('animate0 flipInY');

            };
        };

        $scope.checkForgotPasswordValidation = function (event, isemailInvalid) {

            if (isemailInvalid) {
                event.preventDefault();
                $('#forget').addClass('animate0 wobble');
                $scope.forgotpasswordForm.Email.$dirty = true;
                $timeout(removeClass, 1000);
            }
            else {
                $('#globalLoader').show();
                event.preventDefault();
                $http.post(apiURL.baseAddress + "Account/ForgotPassword", { "Email": $scope.Email }).success(function (data) {
                    if (data.success) {
                        $scope.Email = '';
                        //$scope.forgotPasswordMessage = '';
                        $('#forget').hide();
                        $scope.resetPasswordConfirmation = message.resetPassword;
                        $("#emailSentConfirmation").addClass('animate0 flipInY').fadeIn(3000);
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.warning("Warning", message[data.message]);
                        $('#globalLoader').hide();
                        //$scope.forgotPasswordMessage = message[data.message];
                    }
                }).error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message[data.message]);
                    //$scope.forgotPasswordMessage = message["error"];
                });
            }

            function removeClass() {
                $('#forget').removeClass('animate0 wobble');
            };
        };

        $scope.ForgotPassword = function (event) {
            $scope.Email = '';
            event.preventDefault();
            $('#signin').hide();
            $scope.forgotpasswordForm.Email.$dirty = false;
            $("#forget").addClass('animate0 flipInY').show();

            $timeout(removeClass, 1000);
            function removeClass() {
                $('#forget').removeClass('animate0 flipInY');

            };
        };


        $scope.GoToSignIn = function () {

            $('#forget').hide();
            $scope.signinForm.email.$dirty = false;
            $scope.signinForm.password.$dirty = false;
            $("#signin").addClass('animate0 flipInY').show();

            $timeout(removeClass, 1000);
            function removeClass() {
                $('#signin').removeClass('animate0 flipInY');

            };
        };



        //using
        $scope.login = function (event, loginForm) {
            debugger;
            var loginObject = {};
            loginObject.Email = $scope.email;
            loginObject.Password = $scope.password;
            loginObject.IsLoginWithLinedIn = false;
            $scope.dataLoading = true;
            var isValidate = $scope.checkloginValidation(event, loginForm)
            // checking if the form is valid or not
            if (isValidate) {
                AuthenticationFactory.ClearCredentials();
                AuthenticationFactory.Login(loginObject, function (response) {
                    debugger;
                    if (response.success && response.message == "Success") {
                        if (response.ResponseData.isFreelancer == true) {
                            FreelancerAccountFactory.getCompaniesForFreelancer(response.ResponseData.userId)
                            .success(function (freelancerData) {
                                if (freelancerData.ResponseData.length == 0) {
                                    response.ResponseData.CompanyId = 0;

                                    AuthenticationFactory.SetCredentials(response.ResponseData);
                                    $cookieStore.put('sharedService', response.ResponseData);
                                    sharedService.setShared(response.ResponseData);

                                    //if (response.ResponseData.RoleId==5)
                                    //    $location.path('/dashboard/c');
                                    //else
                                    // Call getMessagesCount on global controller 
                                    $scope.$emit('getMessagesCount', []);
                                    $location.path('/dashboard/u');
                                }
                                else {
                                    response.ResponseData.CompanyId = freelancerData.ResponseData[0].CompanyId;
                                    FreelancerAccountFactory.GetFreelancerRoleForCompany(response.ResponseData.userId, response.ResponseData.CompanyId)
                                                .success(function (FreelancerAccountData) {
                                                    response.ResponseData.RoleId = FreelancerAccountData.ResponseData.RoleId;
                                                    AuthenticationFactory.SetCredentials(response.ResponseData);
                                                    $cookieStore.put('sharedService', response.ResponseData);
                                                    sharedService.setShared(response.ResponseData);

                                                    //if (response.ResponseData.RoleId==5)
                                                    //    $location.path('/dashboard/c');
                                                    //else
                                                    // Call getMessagesCount on global controller 
                                                    $scope.$emit('getMessagesCount', []);
                                                    $location.path('/dashboard/u');
                                                })
                                                .error(function () {
                                                    toaster.error("Error", "Some Error Occured !");
                                                })
                                }
                            })
                            .error(function (data) {
                                toaster.error("Error", "Some Error Occured !");
                            })
                        }
                        else {

                            AuthenticationFactory.SetCredentials(response.ResponseData);
                            $cookieStore.put('sharedService', response.ResponseData);
                            sharedService.setShared(response.ResponseData);

                            //if (response.ResponseData.RoleId==5)
                            //    $location.path('/dashboard/c');
                            //else
                            // Call getMessagesCount on global controller 
                            $scope.$emit('getMessagesCount', []);

                            if (response.ResponseData.IsSubscriptionsEnds == false) {
                                $location.path('/dashboard/u');
                            }
                            else {
                                debugger;
                                if (response.ResponseData.RoleId == 1) {
                                    $location.path('/subscription');
                                }
                                else {
                                    toaster.warning("Warning", "Please contact your admin to login");
                                    $scope.error = "Please contact your admin to login";
                                    $scope.dataLoading = false;
                                }

                            }
                        }

                        if ($scope.checkboxRememberMe == true && response.message == "Success") {
                            $cookieStore.put('rem', response.ResponseData.email);
                        }

                        //Get Internal Permissions For Current User 
                        rolePermissionFactory.getInternalPermissionsForCurrentUser(response.ResponseData.RoleId, response.ResponseData.CompanyId)
                           .success(function (permissionResponse) {
                               sharedService.setInternalPermissionInShared(permissionResponse.ResponseData);
                           })
                           .error(function () {
                               toaster.error("Error", "Some Error Occured!");
                           })

                        
                        $rootScope.timeout = $interval(function () {
                            userMessagesFactory.getMessagesCount(sharedService.getUserId())
                                 .success(function (data) {

                                     $rootScope.InboxCount = data.ResponseData == undefined ? 0 : data.ResponseData == null ? 0 : data.ResponseData;
                                     sharedService.setNewMessagesCount($rootScope.InboxCount);
                                     $scope.InboxCount = $rootScope.InboxCount;
                                 });
                        }, 5000);


                        //else if ($scope.checkboxRememberMe == false && response.message == "Success") {
                        //    $cookieStore.put('rem', " ");
                        //}
                        //$rootScope.RoleId = response.ResponseData.RoleId;
                        //$rootScope.CompanyId = response.ResponseData.CompanyId;
                        //$rootScope.Token = response.ResponseData.Token;
                        //$rootScope.email = response.ResponseData.email;
                        //$rootScope.firstName = response.ResponseData.firstName;
                        //$rootScope.userId = response.ResponseData.userId;

                        $rootScope.headerMenu = false;
                        $rootScope.showSideMenu = false;
                        $('#headerMenu').show();
                        $('#sideMenu').show();
                    }
                    else {
                        toaster.warning(message[response.message]);
                        $scope.error = response.message;
                        $scope.dataLoading = false;
                    }
                });
            }
            else {
                $scope.dataLoading = false;
            }

        };


        //using
        $scope.checkloginValidation = function (event, loginForm) {

            if (loginForm.email.$invalid && loginForm.password.$invalid) {
                event.preventDefault();
                $('#signin').addClass('animate0 wobble');
                $scope.signinForm.email.$dirty = true;
                $scope.signinForm.password.$dirty = true;
                $timeout(removeClass, 1000);
                return false;
            }
            else if (loginForm.email.$invalid && !loginForm.password.$invalid) {
                event.preventDefault();
                $('#signin').addClass('animate0 wobble');
                $scope.signinForm.email.$dirty = true;
                $timeout(removeClass, 1000);
                return false;
            }
            else if (!loginForm.email.$invalid && loginForm.password.$invalid) {
                event.preventDefault();
                $('#signin').addClass('animate0 wobble');
                $scope.signinForm.password.$dirty = true;
                $timeout(removeClass, 1000);
                return false;
            }
            else {
                return true;
            }


            function removeClass() {
                $('#signin').removeClass('animate0 wobble');

            };

        };


        $scope.linkedInAuthentication = function () {
            IN.User.authorize(callbackFunction);
        }

        function callbackFunction() {
            IN.API.Profile("me").fields("first-name", "last-name", "email-address", "picture-url").result(onSuccess).error(onError);
        }

        function onSuccess(data) {
            debugger;
            $('#globalLoader').show();
            var loginObject = {};
            loginObject.Email = data.values[0].emailAddress;
            loginObject.Password = '';
            loginObject.IsLoginWithLinedIn = true;

            AuthenticationFactory.ClearCredentials();
            AuthenticationFactory.Login(loginObject, function (response) {
                if (response.success && response.message == "Success") {
                    if (response.ResponseData.isFreelancer == true) {
                        FreelancerAccountFactory.getCompaniesForFreelancer(response.ResponseData.userId)
                        .success(function (freelancerData) {
                            if (freelancerData.ResponseData.length == 0) {
                                response.ResponseData.CompanyId = 0;

                                AuthenticationFactory.SetCredentials(response.ResponseData);
                                $cookieStore.put('sharedService', response.ResponseData);
                                sharedService.setShared(response.ResponseData);

                                //if (response.ResponseData.RoleId==5)
                                //    $location.path('/dashboard/c');
                                //else
                                // Call getMessagesCount on global controller 
                                $scope.$emit('getMessagesCount', []);
                                $location.path('/dashboard/u');
                                $('#globalLoader').hide();
                                IN.User.logout();
                            }
                            else {
                                response.ResponseData.CompanyId = freelancerData.ResponseData[0].CompanyId;
                                FreelancerAccountFactory.GetFreelancerRoleForCompany(response.ResponseData.userId, response.ResponseData.CompanyId)
                                            .success(function (FreelancerAccountData) {
                                                response.ResponseData.RoleId = FreelancerAccountData.ResponseData.RoleId;
                                                AuthenticationFactory.SetCredentials(response.ResponseData);
                                                $cookieStore.put('sharedService', response.ResponseData);
                                                sharedService.setShared(response.ResponseData);

                                                //if (response.ResponseData.RoleId==5)
                                                //    $location.path('/dashboard/c');
                                                //else
                                                // Call getMessagesCount on global controller 
                                                $scope.$emit('getMessagesCount', []);
                                                $location.path('/dashboard/u');
                                                $('#globalLoader').hide();
                                                IN.User.logout();
                                            })
                                            .error(function () {
                                                toaster.error("Error", "Some Error Occured !");
                                                $('#globalLoader').hide();
                                                IN.User.logout();
                                            })
                            }
                        })
                        .error(function (data) {
                            toaster.error("Error", "Some Error Occured !");
                            $('#globalLoader').hide();
                            IN.User.logout();
                        })
                    }
                    else {

                        AuthenticationFactory.SetCredentials(response.ResponseData);
                        $cookieStore.put('sharedService', response.ResponseData);
                        sharedService.setShared(response.ResponseData);

                        //if (response.ResponseData.RoleId==5)
                        //    $location.path('/dashboard/c');
                        //else
                        // Call getMessagesCount on global controller 
                        $scope.$emit('getMessagesCount', []);

                        if (response.ResponseData.IsSubscriptionsEnds == false) {
                            $location.path('/dashboard/u');
                        }
                        else {
                            if (response.ResponseData.RoleId == 1) {
                                $location.path('/subscription');
                            }
                            else {

                                if (response.ResponseData.IsActive == false) {
                                    toaster.warning("Warning", message.userNotActive);
                                    $scope.error = "Please contact your admin to login";
                                    $scope.dataLoading = false;

                                }
                                else {
                                    toaster.warning("Warning", "Please contact your admin to login");
                                    $scope.error = "Please contact your admin to login";
                                    $scope.dataLoading = false;

                                }
                             
                            }
                        }

                        $('#globalLoader').hide();
                        IN.User.logout();
                    }

                    //Get Internal Permissions For Current User 
                    rolePermissionFactory.getInternalPermissionsForCurrentUser(response.ResponseData.RoleId, response.ResponseData.CompanyId)
                       .success(function (permissionResponse) {
                           sharedService.setInternalPermissionInShared(permissionResponse.ResponseData);
                       })
                       .error(function () {
                           toaster.error("Error", "Some Error Occured!");
                       })



                    $rootScope.headerMenu = false;
                    $rootScope.showSideMenu = false;
                    $('#headerMenu').show();
                    $('#sideMenu').show();
                }
                else {
                    toaster.warning(message[response.message]);
                    $scope.error = response.message;
                    $('#globalLoader').hide();
                    IN.User.logout();
                }
            });

            

        }

        // Handle an error response from the API call
        function onError(error) {
            toaster.error("Error", message["error"]);
        }




        /*Email Key Up Event */
        $scope.emailkeyUp = function () {
            if ($scope.email == '') {
                $('#Email').addClass('inputError');
            }
            else {
                $('#Email').removeClass('inputError');
            }
        };

        /*Password Key Up Event */
        $scope.passwordkeyUp = function () {
            if ($scope.password == '' || $scope.password == undefined) {
                $('#Password').addClass('inputError');
            }
            else {
                $('#Password').removeClass('inputError');
            }
        };

        $scope.test = function () {
            var loginObject = {};
            loginObject.Email = $scope.email;
            loginObject.Password = $scope.password;
            $scope.dataLoading = true;
            //AuthenticationFactory.ClearCredentials();

            //AuthenticationFactory.test(loginObject, function (response) {
            //    if (response.success) {
            //        AuthenticationFactory.SetCredentials(response.email, response.token);
            //        $location.path('/signup');
            //    } else {
            //        $scope.error = response.message;
            //        $scope.dataLoading = false;
            //    }
            //});
            $rootScope.navbar = true;

        };

        $scope.clickRememberMe = function myfunction() {
            $scope.checkboxRememberMe = !$scope.checkboxRememberMe;
        }


        angular.element(document).ready(function () {

            $("input[name='email']").focus();
            if ($cookieStore.get('rem') != undefined) {
                $scope.email = $cookieStore.get('rem');
            }

            if ($cookieStore.get('sharedService') != undefined) {

                var sharedServiceForlogin = $cookieStore.get('sharedService');
                if (!$.isEmptyObject(sharedServiceForlogin)) {
                    AuthenticationFactory.SetCredentials(sharedServiceForlogin);
                    //$cookieStore.put('sharedService', sharedServiceForlogin);
                    //sharedService.setShared(sharedServiceForlogin);

                    $scope.$emit('getMessagesCount', []);
                    $location.path('/dashboard/u');

                    $rootScope.headerMenu = false;
                    $rootScope.showSideMenu = false;
                    $('#headerMenu').show();
                    $('#sideMenu').show();

                }


            }

        })



    });
});