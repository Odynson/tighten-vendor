﻿define(['app'], function () {

    app.controller('ProjectCustomizationControlController', function ($scope, $rootScope, apiURL, $timeout, toaster, $http, settingsFactory, sharedService) {

        $scope.isTextbox = false;
        $scope.isDropRadio = false;
        $scope.isCheckbox = false;
        var CompanyId = sharedService.getCompanyId();
        $scope.customControlModel = { controlName: "", fieldName: "", description: "", controlValue: "", watermark: "", priorityControlValue: "" }
        $scope.ConpanyCustomControlList = []


        function init() {

            $(".droppable-area1, .droppable-area2").sortable({
                opacity: 0.6,
                update: function (event, ui) {
                    debugger;
                    var first = ui.item; // First element to swap
                    var second = ui.item.prev();
                    var dataModel = { FirstCustomControlId: 0, SecondCustomControlId: 0, FirstPriorityId: 0, SecondPriorityId: 0 }

                    debugger;
                    dataModel.FirstCustomControlId = JSON.parse(first[0].id).CustomControlId
                    dataModel.SecondCustomControlId = JSON.parse(second[0].id).CustomControlId
                    dataModel.FirstPriorityId = JSON.parse(first[0].id).PriorityControlValue
                    dataModel.SecondPriorityId = JSON.parse(second[0].id).PriorityControlValue
                    updatePriority(dataModel);

                }
            });
        }
        $(init);


        ////////////////////method for Udate drag and drop priority////////////////////////////////////////
        function updatePriority(dataModel) {
            settingsFactory.updatePriority(dataModel)
                .success(function (data) {
                    toaster.success("Success", message.saveSccessFullyComman);
                    getCustomControl(CompanyId)
                })

        } Error(function () {
            toaster.error("Error", message.error)
            $('#globalLoader').hide();
        })


        ////////////////////method for Udate drag and drop priority////////////////////////////////////////


        $scope.inputcontroldrop = [{ Id: 0, text: '----select----' },
        { Id: 1, text: 'TextBox' },
        { Id: 2, text: 'Dropdown' },
        { Id: 3, text: 'MultiLine Textbox' },
        { Id: 4, text: 'Radio buttons' },
        { Id: 5, text: 'Checkbox' },]

        $scope.customControlModel.controlName = $scope.inputcontroldrop[0];

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }

            getCustomControl(CompanyId)
        })

        $scope.customControlChange = function (controlName) {
            if (controlName.Id == 1 || controlName.Id == 3) {
                $scope.isTextbox = true;
                $scope.isDropRadio = false;
                $scope.isCheckbox = false;
            }
            else if (controlName.Id == 2 || controlName.Id == 4) {
                $scope.isDropRadio = true;
                $scope.isTextbox = false;
                $scope.isCheckbox = false;
            }
            else if (controlName.Id == 5) {
                $scope.isCheckbox = true;
                $scope.isTextbox = false;
                $scope.isDropRadio = false;
            }
            else {

                $scope.isTextbox = false;
                $scope.isDropRadio = false;
                $scope.isCheckbox = false;
            }

        }
        ///////////////////////////////////// --get custom control list//////////////////////////////////////////////////////////
        function getCustomControl(CompanyId) {
            debugger;
            $('#globalLoader').show();
            settingsFactory.GetCustomControl(CompanyId)
                .success(function (data) {
                    $('#globalLoader').hide();
                    $scope.ConpanyCustomControlList = data.ResponseData;
                    toaster.success("Success", message.successdataLoad);


                }), Error(function () {
                    toaster.error("Error", message.error)
                    $('#globalLoader').hide();
                })

        }
        ///////////////////////////////////// --Save Button code//////////////////////////////////////////////////////////
        $scope.savecustomcontrol = function (customControlModel) {
            debugger;
            var dataModel = {};
            dataModel.CompanyId = CompanyId;
            dataModel.ControlType = customControlModel.controlName.text;
            dataModel.FieldName = customControlModel.fieldName;
            dataModel.Description = customControlModel.description;
            if (customControlModel.controlName.text == 'Dropdown' || customControlModel.controlName.text == 'Radio buttons') {
                dataModel.ControlValue = commaSeparate(customControlModel.controlValue); // control value or watemark  one use
            }
            dataModel.Watermark = customControlModel.watermark; //
            dataModel.PriorityControlValue = customControlModel.priorityControlValue
            $('#globalLoader').show();
            settingsFactory.savecustomcontrol(dataModel)
                .success(function (data) {
                    $('#globalLoader').hide();
                    toaster.success("Success", message.saveSccessFullyComman);
                    getCustomControl(CompanyId)
                    $scope.customControlModel = {};
                    $scope.customControlModel.controlName = $scope.inputcontroldrop[0];

                }), Error(function () {
                    toaster.error("Error", message.error)
                    $('#globalLoader').hide();
                })

        }
        function commaSeparate(value) {
            $scope.newValues = '';
            $scope.data = value.replace(/ /g, '');
            var myData = $scope.data.split(',')
            for (var i = 0; myData.length > i; i++) {
                debugger;
                if (myData[i] != "") {
                    $scope.newValues += myData[i] + ',';
                }
            }
            var returnval = $scope.newValues.replace(/,\s*$/, "");
            return returnval;
        }




        ///////////////////////////////////// --Delete Button code//////////////////////////////////////////////////////////
        $scope.deleteCustomControl = function (CustomControlId) {
            debugger;
            dataModel = { CustomControlId: 0, IsActive: false, IsInActiveOrDelete: 1 }

            dataModel.CustomControlId = CustomControlId;
            settingsFactory.deleteCustomControl(dataModel)
                .success(function (data) {
                    toaster.success("Success", message.saveSccessFullyComman);
                    getCustomControl(CompanyId)
                })

        }, Error(function () {
            toaster.error("Error", message.error)
            $('#globalLoader').hide();
        })

        ///////////////////////////////////// --Active InActive Button code//////////////////////////////////////////////////////////
        $scope.inactiveCustomControl = function (item) {
            debugger;
            dataModel = { CustomControlId: 0, IsActive: false, IsInActiveOrDelete: 2 }

            dataModel.CustomControlId = item.CustomControlId;
            dataModel.IsActive = item.IsActive == false ? true : false; // active InActive button code
            settingsFactory.deleteCustomControl(dataModel)
                .success(function (data) {
                    toaster.success("Success", message.saveSccessFullyComman);
                    getCustomControl(CompanyId)
                })

        }, Error(function () {
            toaster.error("Error", message.error)
            $('#globalLoader').hide();
        })
    })

})