﻿
define(["app"], function (app) {

    app.controller("vendorController", function ($scope, $rootScope, apiURL, $location, vendorFactory, sharedService, toaster) {
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();
        $scope.myVendorList = [];  // my vendor for my vendor page
        $scope.companyVendorRespList = [];

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
      
            }

          var RoleId =   sharedService.getRoleId()

            getCompanyVendorList(CompanyId)
            
            getInvitaionToVendors(CompanyId,UserId)

            if (RoleId == 1) {
                debugger;
                getMyVendor(CompanyId)
            }


        })

    
        ////---------------------------getCompanyVendorList---------------------------------------------///////////

        function getCompanyVendorList(CompanyId) {
          
            vendorFactory.getCompanyVendorList(CompanyId).success(function (data) {
                debugger;
                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.companyVendorRespList = data.ResponseData;

                    }
                }
                else {
                    toaster.error("Error", data.message);
                }

            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }

        
        ///------------------------getInvitaionToVendorsList--------------------------------------------------------
       
        function getInvitaionToVendors(CompanyId,UserId) {
            debugger;
            vendorFactory.getInvitaionToVendors(CompanyId,UserId).success(function (data) {
                debugger;
                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.CompanyInvitaionRespList = data.ResponseData;
                    }
                }
      

            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }


        var IdForRejectUPdate;
        $scope.ReasonReject = "";
        ///------------------------popUpfor Rejection--------------------------------------------------------
 
        $scope.openAcceptRejectModel = function (item, IsApproved) {
            debugger;
            IdForRejectUPdate = item.Id;
            $('#modalAcceptReject').modal('show');

        }
        ///---------------------------------acceptRejectInvitaionByModal----------------------------------------------------------------------
        var tempdata = {};
        $scope.acceptRejectInvitaionByModal = function (ReasonReject, IsApproved) {
            debugger;
            var data = {}

            data.IsApproved = IsApproved;
            data.ReasonReject = ReasonReject;
            data.Id = IdForRejectUPdate;
            data.VendorId = CompanyId; // this company is now vendor id for company vendor
            $('#globalLoader').show();
            vendorFactory.acceptRejectInvitaionAsVendor(data)
                .success(function (data) {

                    if (data.ResponseData != null) {
                        //toaster.success("Success", message.InvitationAccepted);

                    }
                    else {
                        toaster.warning("Warning", message.error)
                    }
                    $('#modalAcceptReject').modal('hide');
                    $scope.ReasonReject = "";
                    //getInvitaionToVendors(CompanyId, UserId)
           
             
                }), Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                
                })

            $('#globalLoader').hide();
        }


        ///------------------------openPopforAcceptReject--------------------------------------------------------

        $scope.acceptRejectInvitaionAsVendor = function (item, IsApproved) {
            debugger;
            var data = {}

            data.CompanyId = item.CompanyId;
            data.Name = item.Name;
            data.VendorId = CompanyId; // this company is now vendor id for company vendor
            data.IsApproved = IsApproved;
            data.Id = item.Id;

            $('#globalLoader').show();
            vendorFactory.acceptRejectInvitaionAsVendor(data)
                .success(function (data) {

                    if (data.ResponseData != null)
                    {
                        toaster.success("Success", message.InvitationAccepted + data.ResponseData.Name);
                        
                    }
                    else {
                        toaster.warning("Warning", message.error)
                    }
                     getInvitaionToVendors(CompanyId, UserId)
                    
                    $('#globalLoader').hide();
                }), Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })


        }


        $scope.modalAddNewvendors = function () {
            debugger;
            $('#modalAddNewvendors').modal('show');


        }

        ///////--------------------send invitation to vendor-------------------------------------------------------//////

        /*Email Key Up Event */
        $scope.emailkeyUp = function () {
            if ($scope.email == '') {
                $('#Email').addClass('inputError');
            }
            else {
                $('#Email').removeClass('inputError');
            }
        };

             $scope.vendorInvitationModal = {

                firstName: "",
                lastName: "",
                companyName: "",
                emaiAddress: "",
                messageTextBox: "",


            }
             $scope.inviteVendor = function (isVendorFirstNameInvalid, IsVendorCompanyNameInvalid, IsVendorEmailId, NewVendorForm, vendorInvitationModal) {

            //debugger;
            //if (!isVendorFirstNameInvalid || !IsVendorCompanyNameInvalid || !IsVendorEmailId) {

            //    NewVendorForm.firstName.$dirty = true
            //    NewVendorForm.companyName.$dirty = true
            //    NewVendorForm.emailAddress.$dirty = true

            //}

          //  else {

                var data = {}
                data.FirstName = vendorInvitationModal.firstName
                data.LastName = vendorInvitationModal.lastName
                data.CompanyName = vendorInvitationModal.companyName
                data.EmaiAddress = vendorInvitationModal.emaiAddress
                data.MessageTextBox = vendorInvitationModal.messageTextBox
                data.UserId = sharedService.getUserId()
                data.CompanyId = sharedService.getCompanyId();

                $('#globalLoader').show();
                vendorFactory.inviteVendor(data)
                .success(function (data) {
                    $('#globalLoader').hide();

                    if (data.ResponseData.Id == -1) {

                        toaster.warning("Warning", message.InvitationError)
                    }
                    else {
                        toaster.success("Success", message.InvitationToVendor);
                    }
                    $scope.vendorInvitationModal = {}
                    $('#modalAddNewvendors').modal('hide');
                    getMyVendor(CompanyId) //after Inviting show vendorlist
                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })


          //  }
        }


        function getMyVendor(CompanyId) {
            debugger
            
            $('#globalLoader').show();
            vendorFactory.getMyVendor(CompanyId)
            .success(function (data) {
                if (data.ResponseData != null) {
                    $scope.myVendorList = data.ResponseData;
                }
                $('#globalLoader').hide();

            }),
               Error(function (data, status, headers, config) {
                   toaster.error("Error", "Some Error Occured !");
                   $('#globalLoader').hide();
               })

        }




        function getProjectAndVendorDetail(ProjectId) {

            vendorFactory.getProjectAndVendorDetail(ProjectId, CompanyId)
            .success(function (data) {
                if (data.ResponseData != null) {
                    $scope.ProjectAndVendorDetailData = data.ResponseData;
                }
                $('#globalLoader').hide();

            }),Error(function (data, status, headers, config) {
                toaster.error("Error", "Some Error Occured !");
                $('#globalLoader').hide();
            })

        }


 
        ///////////////////////////////////////GetProjectToQuote for vendor Profile p ////////////////////////////////////////////////////////////////
    


        



    })


});

