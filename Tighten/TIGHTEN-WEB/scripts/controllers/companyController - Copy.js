﻿define(['app', 'companiesFactory', 'teamFactory'], function (app, companiesFactory, teamFactory) {
    app.controller("companyController", function ($scope, $rootScope, $timeout, $http, $location, sharedService, toaster, companiesFactory, teamFactory,$upload,apiURL
        )

    {
        var baseurl = apiURL.baseAddress;
        $scope.imgURL = apiURL.imageAddress;
        /* Showing Company profile to see the information*/
        $scope.showCompanyProfile = function () {
            //closeCentreSections();
            $scope.showCenterLoader = true;
            $scope.selectedProject = 0;
            $scope.selectedTodo = 0;
            $scope.selectedNavigation = "CompanyProfile";
            /* Getting Company Details */
            companiesFactory.getCompanyDetails(sharedService.getCompanyId())
                .success(function (data) {
                    //
                    if (data.success) {
                        $scope.CompanyObject = data.ResponseData[0];
                        /* Getting Company Teams with Users */
                        teamFactory.getCompanyTeams(sharedService.getCompanyId())
                            .success(function (data) {
                                if (data.success) {
                                    $scope.CompanyTeamsObject = data.ResponseData;
                                    $scope.showCenterLoader = false;
                                    $('#divCompanyProfile').show();
                                }
                                else {
                                    toaster.error("Error", data.message);
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured");
                            });
                    }
                    else {
                        toaster.error("Error", "Some Error Occured");
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured");
                });
        };


        /* Opening Company Edit Modal pop up */
        $scope.editCompanyProfile = function () {
            $scope.CompanyEditObject = {};
            $scope.CompanyEditObject.CompanyId = $scope.CompanyObject.CompanyId;
            $scope.CompanyEditObject.CompanyName = $scope.CompanyObject.CompanyName;
            $scope.CompanyEditObject.CompanyEmail = $scope.CompanyObject.CompanyEmail;
            $scope.CompanyEditObject.CompanyWebsite = $scope.CompanyObject.CompanyWebsite;
            $scope.CompanyEditObject.CompanyPhoneNo = $scope.CompanyObject.CompanyPhoneNo;
            $scope.CompanyEditObject.CompanyFax = $scope.CompanyObject.CompanyFax;
            $scope.CompanyEditObject.CompanyAddress = $scope.CompanyObject.CompanyAddress;
            $scope.CompanyEditObject.CompanyDescription = $scope.CompanyObject.CompanyDescription;
            $('#modalEditCompanyProfile').modal('show');
        };

        /* Update Company Details */
        $scope.updateCompanyProfile = function () {
            var NewCompanyEditObject = { 'Company': $scope.CompanyEditObject, 'UserId': sharedService.getUserId() }
            companiesFactory.updateCompanyDetails(NewCompanyEditObject)
                .success(function (data) {
                    if (data.success) {
                        $scope.CompanyObject.CompanyName = $scope.CompanyEditObject.CompanyName;
                        $scope.CompanyObject.CompanyEmail = $scope.CompanyEditObject.CompanyEmail;
                        $scope.CompanyObject.CompanyWebsite = $scope.CompanyEditObject.CompanyWebsite;
                        $scope.CompanyObject.CompanyPhoneNo = $scope.CompanyEditObject.CompanyPhoneNo;
                        $scope.CompanyObject.CompanyFax = $scope.CompanyEditObject.CompanyFax;
                        $scope.CompanyObject.CompanyAddress = $scope.CompanyEditObject.CompanyAddress;
                        $scope.CompanyObject.CompanyDescription = $scope.CompanyEditObject.CompanyDescription;
                        // Updating the company prfile in the left menu
                        $scope.$emit('bindCompanyDetail', []);
                        toaster.success("Success", data.message);
                        $('#modalEditCompanyProfile').modal('hide');
                    }
                    else {
                        if (data.message == undefined) {
                            toaster.error("Error", data.errors.toString());
                        }
                        else {
                            toaster.error("Error", data.message);
                        }
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured");
                });
        };

        /* Updating Company Logo*/
        $scope.updateCompanyLogo = function ($files) {
            $('#globalLoader').show();
            $scope.showCompanyLogoLoader = true;
            //$files: an array of files selected, each file has name, size, and type.
            for (var i = 0; i < $files.length; i++) {
                var $file = $files[i];
                (function (index) {

                    //$scope.upload[index] = 
                         $upload.upload({
                        url: baseurl + "CompaniesAPI/UploadLogo", // webapi url
                        method: "POST",
                        data: { UploadedProfile: $file, Companyid: sharedService.getCompanyId(), UserId: sharedService .getUserId()},
                        file: $file
                    })
                        .progress(function (evt) {
                            // get upload percentage
                            $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                        })
                        .success(function (data) {
                            // file is uploaded successfully
                            if (data.success) {
                                angular.forEach(
                                 angular.element("input[type='file']"),
                                         function (inputElem) {
                                             angular.element(inputElem).val(null);
                                         });
                                $timeout(function () {
                                    $scope.$apply();
                                    companiesFactory.getCompanyDetails(sharedService.getCompanyId())
                                        .success(function (companydata) {
                                            if (companydata.success) {
                                                $scope.CompanyObject = companydata.ResponseData[0];
                                                $timeout(function () {
                                                    $scope.$apply();
                                                    $scope.showCompanyLogoLoader = false;
                                                    toaster.success("Success", data.message);
                                                });
                                            }
                                            else {
                                                $scope.showCompanyLogoLoader = false;
                                                toaster.error("Error", "Some Error Occured");
                                            }
                                        }).error(function () {
                                            $scope.showCompanyLogoLoader = false;
                                            toaster.error("Error", "Some error occured");
                                        });
                                });
                            }
                            else {
                                angular.forEach(
                                 angular.element("input[type='file']"),
                                         function (inputElem) {
                                             angular.element(inputElem).val(null);
                                         });
                                $timeout(function () {
                                    $scope.$apply();
                                    $scope.showCompanyLogoLoader = false;
                                    toaster.error("Error", data.message);
                                });
                            }
                        })
                        .error(function (data, status, headers, config) {
                            // file failed to upload
                            console.log(data);
                        });
                })(i);
            }

            $('#globalLoader').hide();

        }

        /* check Path of company Profile Pick*/
        $scope.checkCompanyProfilePic = function (obj) {
            if ((obj != 'Uploads/CompanyLogos/') && (obj != null)) {
                return true;
            }
            else {
                return false;
            }
        }
        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu', []);
            }
            $scope.showCompanyProfile();
            $scope.Role = sharedService.getRoleId();
        });
    });
});