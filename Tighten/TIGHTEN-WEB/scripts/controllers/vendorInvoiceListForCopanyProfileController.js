﻿define(['app'], function () {

    app.controller('vendorInvoiceListForCopanyProfileController', function ($scope, invoiceFactory, subscriptionFactory, $location, $timeout, sharedService, vendorInvoiceFactory, invoiceFactory, paymentDetailFactory, toaster, $rootScope, $stateParams, $location, apiURL, stripSetting, $http) {
        $scope.imgURL = apiURL.imageAddress;
        var CompanyId;
        var UserId;
        var InvoiceId;
        $scope.TotalOutstandingAmount = 0;
        $scope.TotalPaidAmount = 0;
        $scope.TotalRejectedAmount = 0
        $scope.filterVendorInvocieModal = { fromDate: "", toDate: "", VendorCompanyId: "" }
        $scope.VendorinvoiceList = [];
        $scope.VendorinvoiceListOutstanding = [];
        $scope.VendorinvoiceListPaid = [];
        $scope.VendorinvoiceListRejected = [];
        $scope.vedorInvoicePreview = {}
        $scope.format = 'MM/dd/yyyy';

        $scope.popup = {
            opened1: false,
            opened2: false
        };

        $scope.open1 = function () {
            debugger;
            $scope.popup.opened1 = true;
            $scope.mindate = new Date();
        };

        $scope.open2 = function () {

            $scope.popup.opened2 = true;
            $scope.mindate1 = new Date();
        };
        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCountOutstanding = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.totalCountPaid = 0;
        $scope.totalCountRejected = 0;

        $scope.pageIndexOutstanding = 1;   // Current page number. First page is 1.-->  
        $scope.pageIndexPaid = 1;
        $scope.pageIndexRejected = 1;

        $scope.pageSizeSelectedOutstanding = 5; // Maximum number of items per page. 
        $scope.pageSizeSelectedPaid = 5;
        $scope.pageSizeSelectedRejected = 5;


        $scope.pagesizeListOutstanding = [
            { PageSizeOutstanding: 5 },
            { PageSizeOutstanding: 10 },
            { PageSizeOutstanding: 25 },
            { PageSizeOutstanding: 50 },

        ];
        $scope.PageSizeOutstanding = $scope.pagesizeListOutstanding[0];


        $scope.pagesizeListPaid = [
            { PageSizePaid: 5 },
            { PageSizePaid: 10 },
            { PageSizePaid: 25 },
            { PageSizePaid: 50 },

        ];
        $scope.PageSizePaid = $scope.pagesizeListPaid[0];


        $scope.pagesizeListRejected = [
            { PageSizeRejected: 5 },
            { PageSizeRejected: 10 },
            { PageSizeRejected: 25 },
            { PageSizeRejected: 50 },

        ];
        $scope.PageSizeRejected = $scope.pagesizeListRejected[0];



        $scope.totalPageCountOutstanding = 0;
        $scope.totalPageCountPaid = 0;
        $scope.totalPageCountRejected = 0;

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }

            $scope.filterVendorInvocieModal.toDate = new Date();

            $scope.getYearsList = function (range) {
                var currentYear = new Date().getFullYear();
                var years = [];
                for (var i = 0; i < range; i++) {
                    years.push(currentYear + i);
                }
                return years;
            };

            CompanyId = sharedService.getCompanyId();
            UserId = sharedService.getUserId();
            //getAllInvoiceDetailList(CompanyId)
            GetAllInvoiceDetailListOutstanding(CompanyId)
            GetAllInvoiceDetailListPaid(CompanyId)
            GetAllInvoiceDetailListRejected(CompanyId)

            GetVendorForDrop(CompanyId) ///  vendor for filter project


        })


        ////////////////////////////////////getAllInvoiceDetailList//////////////////////////////////////////////////////////////

        function getTotalAmountForNotPaidInvoice() {
            debugger
            var total_Amount = 0;
            if ($scope.VendorinvoiceListOutstanding.length > 0) {
                for (var i = 0; i < $scope.VendorinvoiceListOutstanding.length; i++) {
                    var Inv = $scope.VendorinvoiceListOutstanding[i];
                    if (Inv.IsPaid == false && Inv.IsRejected == false) {
                        //$scope.TotalOutstandingAmount += Inv.TotalAmount;
                        $scope.TotalOutstandingAmount = (parseFloat($scope.TotalOutstandingAmount) + parseFloat(Inv.TotalAmount)).toFixed(2);;

                    }
                }
            }

        }

        ////////////////////////////////////getTotalAmountForPaidInvoice//////////////////////////////////////////////////////////////
        function getTotalAmountForPaidInvoice() {

            var total_Amount = 0;
            if ($scope.VendorinvoiceListPaid.length > 0) {
                for (var i = 0; i < $scope.VendorinvoiceListPaid.length; i++) {
                    var Inv = $scope.VendorinvoiceListPaid[i];
                    if (Inv.IsPaid == true && Inv.IsRejected == false) {
                      //  $scope.TotalPaidAmount += (Inv.TotalAmount);
                        $scope.TotalPaidAmount = (parseFloat($scope.TotalPaidAmount) + parseFloat(Inv.TotalAmount)).toFixed(2);;

                    }
                }
            }

        }

        ////////////////////////////////////getTotalAmountForPaidInvoice//////////////////////////////////////////////////////////////
        function getTotalAmountRejectedForPaidInvoice() {
            debugger

            if ($scope.VendorinvoiceListRejected.length > 0) {
                for (var i = 0; i < $scope.VendorinvoiceListRejected.length; i++) {
                    var Inv = $scope.VendorinvoiceListRejected[i];
                    if (Inv.IsRejected == true) {
                     //   $scope.TotalRejectedAmount += (Inv.TotalAmount);
                        $scope.TotalRejectedAmount = (parseFloat($scope.TotalRejectedAmount) + parseFloat(Inv.TotalAmount)).toFixed(2);;

                    }
                }
            }

        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $scope.FilterInvoiceByVendor = function (selectedVendor) {
            debugger;
            $scope.filterVendorInvocieModal.VendorCompanyId = selectedVendor;
            // getAllInvoiceDetailList(CompanyId)
            GetAllInvoiceDetailListOutstanding(CompanyId)
            GetAllInvoiceDetailListPaid(CompanyId)
            GetAllInvoiceDetailListRejected(CompanyId)
        }

        $scope.SearchVendorInvoiceCompanyProfile = function () {
            //   getAllInvoiceDetailList(CompanyId)
            GetAllInvoiceDetailListOutstanding(CompanyId)
            GetAllInvoiceDetailListPaid(CompanyId)
            GetAllInvoiceDetailListRejected(CompanyId)
        }
        ////////////////////////////////////getAllInvoiceDetailList//////////////////////////////////////////////////////////////
        function getAllInvoiceDetailList(CompanyId) {
            $scope.TotalOutstandingAmount = 0;
            $scope.TotalPaidAmount = 0;
            $scope.TotalRejectedAmount = 0

            var dataModal = {}
            dataModal.CompanyId = CompanyId;
            dataModal.FromDate = $scope.filterVendorInvocieModal.fromDate;
            dataModal.VendorId = $scope.filterVendorInvocieModal.vendorId;
            dataModal.VendorCompanyId = $scope.filterVendorInvocieModal.VendorCompanyId;
            dataModal.PageSizeSelected = $scope.pageSizeSelected;
            dataModal.PageIndex = $scope.pageIndex;
            vendorInvoiceFactory.getAllVendorInvoiceList(dataModal)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        debugger;

                        $scope.VendorinvoiceList = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCount = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountForNotPaidInvoice();
                        getTotalAmountForPaidInvoice();
                        getTotalAmountRejectedForPaidInvoice();

                        GetVendorForDrop(CompanyId) ///  vendor for filter project
                    }
                    else {
                        toaster.error("Error", message.error);

                    }
                }), Error(function () {

                    toaster.error("Error", message.error);
                })
        }

        ////////////////////////////////////showVendorInvoicePreviewInCompany//////////////////////////////////////////////////////////////
        $scope.showVendorInvoicePreviewInCompany = function (Id) {
            debugger;
            $('#globalLoader').show();
            vendorInvoiceFactory.getPreviewInvoicePaidOutstandingReject(Id, CompanyId)
                .success(function (data) {
                    debugger;
                    if (data.success) {
                        if (data.ResponseData != null)
                            $scope.vedorInvoicePaidOutstandRejectPreview = data.ResponseData;
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    }
                    $('#VendorInvoicePreviewForCompanyProfileModal').modal('show');
                }),
                Error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                })

        }
        ////////////////////////////////////approveVendorInvoice//////////////////////////////////////////////////////////////
        $scope.approveVendorInvoice = function (InvoiceId, index) {
            $('#globalLoader').show();
            debugger;
            var data = { 'InvoiceId': InvoiceId, 'UserId': UserId };
            vendorInvoiceFactory.approvedVendorInvoice(data)
                .success(function (response) {
                    $('#VendorInvoicePreviewForCompanyProfileModal').modal('hide');
                    $('#globalLoader').hide();
                    toaster.success("Success", message.VendorInvoiceApproved);
                    // getAllInvoiceDetailList(CompanyId)
                    GetAllInvoiceDetailListOutstanding(CompanyId)
                    GetAllInvoiceDetailListPaid(CompanyId)
                    GetAllInvoiceDetailListRejected(CompanyId)
                })
                .error(function (response) {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                })
        }


        ////////////////////////////////////rejectVendorInvoice//////////////////////////////////////////////////////////////
        $scope.rejectVendorInvoice = function (InvoiceId, index) {

            $scope.Reject = { 'InvoiceId': InvoiceId, 'UserId': sharedService.getUserId() };
            $("input[name='Reason']").removeClass("inputError");
            $('#VendorInvoicePreviewForCompanyProfileModal').modal('hide');
            $('#modalInvoiceReject').modal('show');
        }

        ////////////////////////////////////rejectInvoice//////////////////////////////////////////////////////////////

        $scope.rejectInvoice = function (IsReasonInvalid, addUpdateInvoiceForm) {

            if (IsReasonInvalid) {
                addUpdateInvoiceForm.Reason.$dirty = true;
                return;
            }

            $('#globalLoader').show();
            vendorInvoiceFactory.rejectInvoice($scope.Reject)
                .success(function (response) {
                    $('#modalInvoiceReject').modal('hide');

                    // Load All Invoices again
                    var d = new Date();
                    d.setDate(d.getDate() - 7);
                    $scope.fromDate = d;
                    $scope.toDate = new Date();
                    $('#globalLoader').hide();
                    // getAllInvoiceDetailList(CompanyId);
                    GetAllInvoiceDetailListOutstanding(CompanyId)
                    GetAllInvoiceDetailListPaid(CompanyId)
                    GetAllInvoiceDetailListRejected(CompanyId)
                    //var date = Date();
                    //$scope.CurrentDate = date.substring(0, 3) + "," + date.substring(3, 15);

                    //var data = { "UserId": UserId, "CompanyId": CompanyId, "InvoiceStatus": $scope.selectedStatus, "FromDate": $scope.fromDate, "ToDate": $scope.toDate, "FreelancerUserId": $scope.selectedFreelancer, "RoleId": sharedService.getRoleId() };
                    //$scope.ShowInvoice(data);
                    //toaster.success("Success", response.VendorInvoiceRejected);
                })
                .error(function (response) {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                })
        }
        $scope.DataApproveInvoice = { InvoiceId: 0, UserId: "", Token: "" };
        $scope.selectVendorPaymentDetail = function (InvoiceId, index) {
            //    $('#globalLoader').show();
            debugger;


            vendorInvoiceFactory.getCardDetailForVendorPayment(0, CompanyId, InvoiceId)
                .success(function (result) {
                    $scope.userCardDetail = result.ResponseData;

                    $scope.DataApproveInvoice = { InvoiceId: InvoiceId, UserId: UserId, Token: "" };

                    $('#VendorInvoicePreviewForCompanyProfileModal').modal('hide');
                    $('#modalInvoicePreview').modal('hide');
                    $('#VendorStripePaymentModal').modal('show');
                    $('#globalLoader').hide();
                })
                .error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message["error"]);
                })
        }

        function GetVendorForDrop(CompanyId) {
            debugger;
            vendorInvoiceFactory.GetVendorForDrop(CompanyId)
                .success(function (data) {
                    if (data.success) {
                        $scope.invoiceVendorList = data.ResponseData;
                    }
                    else {
                        toaster.error("Error", message.error);

                        $('#globalLoader').hide();
                    }

                })
                , Error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })
        }

        ///////////////////////*  region Stripe  Starts Here   */////////////////////////////

        $scope.isSubsctriptionPaymentDone = false;

        $scope.approveAndPaymentVenodrInvoice = function () {
            debugger;
            $('#globalLoader').show();
            var $form = $('#payment-form');
            if ($scope.userCardDetail == null) {
                $('#globalLoader').hide();
                toaster.warning("Warning", "Please contact admin to set-up default Account");
                return;
            }
            Stripe.setPublishableKey(stripSetting.stripKey);
            //$form.find('.submit').prop('disabled', true);
            Stripe.card.createToken($form, stripeResponseHandler);
        }


        /////////////*  region Stripe   Here   */////////////////
        function stripeResponseHandler(status, response) {
            // Grab the form:
            var $form = $('#payment-form');
            if (response.error) {
                // Show the errors on the form:
                //$form.find('.payment-errors').text(response.error.message);
                toaster.error("Error", response.error.message);
                $('#globalLoader').hide();
            }
            else {
                // Token was created!

                // Get the token ID:
                $scope.DataApproveInvoice.Token = response.id;
                var data = $scope.DataApproveInvoice;
                var index = $scope.DataApproveInvoice.Index;
                // this factory called for invoice payment
                vendorInvoiceFactory.approveInvoice(data)
                    .success(function (response) {
                        debugger;
                        $timeout(function () {
                            if (response.ResponseData == 'Payment successfull') {
                                toaster.success("Success", message.ApproveInvoiceSuccess);
                                $('#VendorStripePaymentModal').modal('hide');
                                getAllInvoiceDetailList(CompanyId)
                                $('#globalLoader').hide();
                            }
                            else {

                                toaster.error("Error", message["error"]);
                                $('#VendorStripePaymentModal').modal('show');
                                $('#globalLoader').hide();
                            }


                        }, 2000);
                    })
                    .error(function () {
                        $('#globalLoader').hide();
                        toaster.error("Error", message["error"]);
                        $('#VendorStripePaymentModal').modal('show');
                    })


            }
        };
        /////////////*  region Stripe  End Here   */////////////////




        ////////////////////////////////////------------paging code/////////////////////////////////

        $scope.changePageSize = function (pageSizeSelected, param) {
            debugger;
            if (param == 'Outstanding') {
                $scope.pageSizeSelectedOutstanding = pageSizeSelected.PageSizeOutstanding;
                GetAllInvoiceDetailListOutstanding(CompanyId)

            }
            else if (param == 'Paid') {
                $scope.pageSizeSelectedPaid = pageSizeSelected.PageSizePaid;
                GetAllInvoiceDetailListPaid(CompanyId)
            }
            else if (param == 'Rejected') {
                $scope.pageSizeSelectedRejected = pageSizeSelected.PageSizeRejected;
                GetAllInvoiceDetailListRejected(CompanyId)

            }
        }

        $scope.pageChanged = function (pageIndex, param) {
            debugger;
            if (param == 'Outstanding') {
                $scope.pageIndexOutstanding = pageIndex;
                GetAllInvoiceDetailListOutstanding(CompanyId)

            }
            else if (param == 'Paid') {
                $scope.pageIndexPaid = pageIndex;
                GetAllInvoiceDetailListPaid(CompanyId)
            }
            else if (param == 'Rejected') {
                $scope.pageIndexRejected = pageIndex;
                GetAllInvoiceDetailListRejected(CompanyId)

            }

        }
        ////////////////////////////////////------------paging code/////////////////////////////////
        function GetAllInvoiceDetailListOutstanding(CompanyId) {
            $scope.TotalOutstandingAmount = 0;
            var dataModal = {}
            dataModal.CompanyId = CompanyId;
            dataModal.FromDate = $scope.filterVendorInvocieModal.fromDate;
            dataModal.VendorId = $scope.filterVendorInvocieModal.vendorId;
            dataModal.VendorCompanyId = $scope.filterVendorInvocieModal.VendorCompanyId;
            dataModal.PageSizeSelected = $scope.pageSizeSelectedOutstanding;
            dataModal.PageIndex = $scope.pageIndexOutstanding;
            $('#globalLoader').show();

            vendorInvoiceFactory.getAllVendorInvoiceListOutstanding(dataModal)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.VendorinvoiceListOutstanding = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCountOutstanding = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountForNotPaidInvoice();
                        $('#globalLoader').hide();

                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();

                    }
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })

        }

        function GetAllInvoiceDetailListPaid(CompanyId) {
            $scope.TotalPaidAmount = 0;
            var dataModal = {}
            dataModal.CompanyId = CompanyId;
            dataModal.FromDate = $scope.filterVendorInvocieModal.fromDate;
            dataModal.VendorId = $scope.filterVendorInvocieModal.vendorId;
            dataModal.VendorCompanyId = $scope.filterVendorInvocieModal.VendorCompanyId;
            dataModal.PageSizeSelected = $scope.pageSizeSelectedPaid;
            dataModal.PageIndex = $scope.pageIndexPaid;
            $('#globalLoader').show();
            vendorInvoiceFactory.getAllVendorInvoiceListPaid(dataModal)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.VendorinvoiceListPaid = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCountPaid = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountForPaidInvoice();
                        $('#globalLoader').hide();

                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();

                    }
                }), Error(function () {

                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })

        }

        function GetAllInvoiceDetailListRejected(CompanyId) {
            debugger
            $scope.TotalRejectedAmount = 0
            var dataModal = {}
            dataModal.CompanyId = CompanyId;
            dataModal.FromDate = $scope.filterVendorInvocieModal.fromDate;
            dataModal.VendorId = $scope.filterVendorInvocieModal.vendorId;
            dataModal.VendorCompanyId = $scope.filterVendorInvocieModal.VendorCompanyId;
            dataModal.PageSizeSelected = $scope.pageSizeSelectedRejected;
            dataModal.PageIndex = $scope.pageIndexRejected;
            $('#globalLoader').show();

            vendorInvoiceFactory.getAllVendorInvoiceListRejected(dataModal)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.VendorinvoiceListRejected = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCountRejected = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountRejectedForPaidInvoice();
                        $('#globalLoader').hide();

                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();

                    }
                }), Error(function () {

                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })

        }




    })

})
