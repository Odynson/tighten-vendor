﻿
define(['app'], function () {


    app.controller("inviteVendorCompanyRegistration", function ($scope, sharedService, $rootScope, registrationFactory,toaster) {

        var CompanyId;
        var UserId;

        angular.element(document).ready(function () {
            CompanyId = sharedService.getCompanyId();
            UserId = sharedService.getUserId();

        })

  

        var isNotValidMail = true;

        $scope.registerInvitationVendorCompany = function (vendorCompanyMailId) {
            debugger;
            $scope.Email_message  = '';
            if (vendorCompanyMailId == undefined || vendorCompanyMailId == '') {
                $scope.Email_message = 'You must provide Email Address';
                return;
            }
            if (isNotValidMail) {
                $scope.Email_message = 'You must provide Valid Email Address';
                return;
            }
            var dataModal = {
                CompanyId: CompanyId,
                UserId: UserId,
                EmailId: vendorCompanyMailId
            }
          

            $('#globalLoader').show();
            registrationFactory.registerInvitationVendorCompany(dataModal)
            .success(function (data) {
                if (data.success) {
                    debugger;
                 

                    if (data.ResponseData.Id == 1) {
                        toaster.success("Success", "Registration Mail send Successfully");
                     
                    }
                    if (data.ResponseData.Id == 2) {
                        toaster.warning("warning", "Invitation already send");
                     
                    }
                    if (data.ResponseData.Id == 3) {
                        toaster.warning("warning", "somebody else have registered this vendor firm with " + data.ResponseData.EmailId);
                    
                    }

                    if (data.ResponseData.Id == 0) {
                        toaster.warning("warning", "somebody else have registered this vendor firm with " + data.ResponseData.EmailId);

                    }

                    $('#globalLoader').hide();
                
                }
            
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })


        }


        $scope.checkMailValid = function (vendorCompanyMailId) {
            debugger;
            $scope.Email_message = '';
            if (vendorCompanyMailId != undefined) {
                var newemail = vendorCompanyMailId.split("@");
                if (newemail.length > 1) {
                    isNotValidMail = false;
                }
                else {
                    isNotValidMail =  true;
                   
                }
            }
        }


    })
})