﻿define(['app'], function () {

    app.controller("vendorProjectQuoteController", function ($scope,$location, sharedService, vendorFactory, toaster, $rootScope, $stateParams, $location, apiURL, $http) {

        var CompanyId = sharedService.getCompanyId();
        var ProjectId = 0;
        var UserId = 0;
        var VendorId = 0
        UserId = sharedService.getUserId();
        $scope.IsQuateSendForProject = false;
        $scope.IsNotDeletedByCompany = true;
        $scope.QuoteReasonReject = "";
        

        $scope.getProjectDetailForVendorQuoteList = {

            VendorId: CompanyId,
            TotalBudget: 0,
            TotalHours: 0,
            AllDescription: '',
            ProjectPhaseList: [],
            ProjectMileStoneList: [],
            ResourcesList: []
        }

        $scope.resourceModal = {

            roleId: "",
            firstName: "",
            lastname: "",
            profile: "",
            hourlyRate: "",
            ExpectedHours: ""

        }

        // custom control
        $scope.CustomDirectiveReady = true;
        $scope.ProjectIsEdit = true;
        $scope.customcontrolmodel = {};



        angular.element(document).ready(function () {

             ProjectId = $stateParams.pid;
             UserId = sharedService.getUserId();
             VendorId = sharedService.getVendorId();
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }

           

            if (ProjectId != 0) {
                debugger;
                var dataModal = { UserId: UserId, CompanyId: CompanyId, ProjectId: ProjectId, VendorId: VendorId }
                getProjectDetailForVendor(dataModal)
            }

            $scope.formatDate = function (date) {
                var dateOut = new Date(date);
                return dateOut;
            };

        })



        //////////////////////////////////////////////getProjectDetailForVendor///////////////////////////////////////////////////////////////////////
        function getProjectDetailForVendor(dataModal) {
            debugger;
            $('#globalLoader').show();
            vendorFactory.getProjectDetailForVendor(dataModal)
            .success(function (data) {
                debugger
                $('#globalLoader').hide();
                if (data.ResponseData.IsProjectUpdated == null )
                {
                    $scope.IsQuateSendForProject = true;
                }
                if (data.ResponseData.IsActive == false) {
                    $scope.IsQuateSendForProject = false;
                    $scope.IsNotDeletedByCompany = false;
                }
                toaster.success("Success", message.successdataLoad);
                $scope.getProjectDetailForVendorQuoteList = data.ResponseData;
                $scope.getProjectDetailList1 = $scope.getProjectDetailForVendorQuoteList;
            


             for (var i = 0; i < $scope.getProjectDetailForVendorQuoteList.ResourcesList.length; i++) {
                    $scope.getProjectDetailForVendorQuoteList.ResourcesList[i].PercentageFromTotal = (parseFloat($scope.getProjectDetailForVendorQuoteList.ResourcesList[i].ExpectedHours).toFixed(2) / parseFloat($scope.getProjectDetailForVendorQuoteList.TotalHours).toFixed(2)) * 100
             }

                //------getting custom control area ---------//
             if (data.ResponseData.CustomControlValueModel != null) {
                 $scope.customcontrolmodel = JSON.parse(data.ResponseData.CustomControlValueModel.CustomControlData);


             }
                //-----------------Custom cuntrol is not use by Company---------------------------------------///
           
             $scope.CustomcontrolList = data.ResponseData.CustomControlUIList;
             $scope.CustomDirectiveReady = true;


             
                getVendorResourceRole()// calling vendor role
            }),
            Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();
            })

        }

        //////////////////////////////////////////////////////modalMilestoneDetail///////////////////////////////////////////////////////////////////////
        $scope.PhaseIndex
        $scope.modalMilestoneDetail = function (index) {

            $scope.PhaseIndex = index;
            $('#modalshowMilestoneDetail').modal('show')

        }




        //////////////////////////////////////////////////////getVendorResourceRole///////////////////////////////////////////////////////////////////////

        function getVendorResourceRole() {
            vendorFactory.getVendorResourceRole()
            .success(function (data) {
                $scope.getVendorResourceRoleList = data.ResponseData;
            })
        }
        //////////////////////////////////////////////////////addPhaseAndMilestoneBudgetCost ///////////////////////////////////////////////////////////////////////
        $scope.addBudgetCost = function (index, type) {
            debugger;
            $scope.Hourdisable = false;
            var phaseBudget = 0;
            var milestoneBudget = 0;
            var individualPhaseBudget = 0;
            $scope.getProjectDetailForVendorQuoteList.TotalBudget = 0;
            // looping on the Phase

            for (var i = 0; i < $scope.getProjectDetailForVendorQuoteList.PhaseList.length; i++) {
                individualPhaseBudget = 0;
                for (var j = 0; j < $scope.getProjectDetailForVendorQuoteList.PhaseList[i].MileStone.length; j++) {
                    if ($scope.getProjectDetailForVendorQuoteList.PhaseList[i].MileStone[j].MilestoneVendorBudget != undefined && $scope.getProjectDetailForVendorQuoteList.PhaseList[i].MileStone[j].MilestoneVendorBudget != null) {

                        milestoneBudget += parseFloat($scope.getProjectDetailForVendorQuoteList.PhaseList[i].MileStone[j].MilestoneVendorBudget);
                    }

                }
                if (milestoneBudget > 0) {
                    var tempphaseBudget = 0;
                    if ($scope.getProjectDetailForVendorQuoteList.PhaseList[index].PhaseVendorBudget != undefined) {
                        tempphaseBudget += parseFloat($scope.getProjectDetailForVendorQuoteList.PhaseList[index].PhaseVendorBudget);
                    }

                    if (type == 'm') {
                        $scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorBudget = milestoneBudget;
                        milestoneBudget = 0;
                    }
                    else if (type == 'p' && tempphaseBudget < milestoneBudget) {
                        {
                            $scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorBudget = milestoneBudget;
                            milestoneBudget = 0;
                            toaster.warning("Warning", "Phase budget cannot be less than milstone budget");
                        }

                    }
                    // $scope.Hourdisable = true
                }
                if ($scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorBudget != undefined && $scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorBudget != null) {
                    phaseBudget += parseFloat($scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorBudget);
                }
            }


            $scope.getProjectDetailForVendorQuoteList.TotalBudget = phaseBudget;
        }


        //////////////////////////////////////////////////////addPhaseAndMilestoneHourCost///////////////////////////////////////////////////////////////////////
        $scope.addHourCost = function (index, type) {

            var phaseHour = 0;
            var milestoneHour = 0;
            $scope.getProjectDetailForVendorQuoteList.TotalHours = 0;

            for (var i = 0; i < $scope.getProjectDetailForVendorQuoteList.PhaseList.length; i++) {

                //if ($scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorHour != undefined && $scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorHour != null) {
                //    $scope.getProjectDetailForVendorQuoteList.TotalHours += parseFloat($scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorHour);
                //}

                for (var j = 0; j < $scope.getProjectDetailForVendorQuoteList.PhaseList[i].MileStone.length; j++) {
                    if ($scope.getProjectDetailForVendorQuoteList.PhaseList[i].MileStone[j].MilestoneVendorHour != undefined && $scope.getProjectDetailForVendorQuoteList.PhaseList[i].MileStone[j].MilestoneVendorHour != null) {
                        milestoneHour += parseFloat($scope.getProjectDetailForVendorQuoteList.PhaseList[i].MileStone[j].MilestoneVendorHour);
                    }
                }
                if (milestoneHour > 0) {
                    var tempPhaseHour;

                    if ($scope.getProjectDetailForVendorQuoteList.PhaseList[index].PhaseVendorHour != undefined && $scope.getProjectDetailForVendorQuoteList.PhaseList[index].PhaseVendorHour != null) {
                        tempPhaseHour = parseFloat($scope.getProjectDetailForVendorQuoteList.PhaseList[index].PhaseVendorHour);
                    }
                    if (type == 'm') {
                        $scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorHour = milestoneHour;
                        milestoneHour = 0;
                    }
                    else if (type == 'p' && tempPhaseHour < milestoneHour) {
                        $scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorHour = milestoneHour;
                        milestoneHour = 0;
                        toaster.warning("Warning", "Phase hour cannot be less than milstone hour");

                    }
                }

                if ($scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorHour != undefined && $scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorHour != null) {
                    phaseHour += parseFloat($scope.getProjectDetailForVendorQuoteList.PhaseList[i].PhaseVendorHour);
                }
                $scope.getProjectDetailForVendorQuoteList.TotalHours = phaseHour;
            }
        }
        //////////////////////////////////////////////////////changeVendor///////////////////////////////////////////////////////////////////////

        $scope.roleModal = {}
        $scope.changeVendor = function (roleModal) {

            $scope.roleModal.RoleId = roleModal.RoleId;
            $scope.roleModal.DesignationName = roleModal.Name;
        }
        //////////////////////////////////////////////////////addResource///////////////////////////////////////////////////////////////////////
        var TotalExpectedHours = 0
        $scope.addResource = function (item) {
            debugger
            var data = {};

            data.ExpectedHours = 0
            data.PercentageFromTotal = 0
            if (item != null && item != undefined) {
                data.RoleId = $scope.roleModal.RoleId;
                data.DesignationName = $scope.roleModal.DesignationName;
                data.FirstName = item.firstName;
                data.LastName = item.lastname;//check spelling
                data.Profile = item.profile;
                data.HourlyRate = item.hourlyRate;
                data.ExpectedHours = parseFloat(item.expectedHours);
                data.PercentageFromTotal = parseFloat(item.expectedHours);
                $scope.getProjectDetailForVendorQuoteList.ResourcesList.push(data);

                $scope.getProjectDetailForVendorQuoteList.TotalHours;

                for (var i = 0; i < $scope.getProjectDetailForVendorQuoteList.ResourcesList.length; i++) {
                    $scope.getProjectDetailForVendorQuoteList.ResourcesList[i].PercentageFromTotal = (parseFloat($scope.getProjectDetailForVendorQuoteList.ResourcesList[i].ExpectedHours).toFixed(2) / parseFloat($scope.getProjectDetailForVendorQuoteList.TotalHours).toFixed(2)) * 100
                    parseFloat($scope.getProjectDetailForVendorQuoteList.ResourcesList[i].PercentageFromTotal).toFixed(2)
                }

                data = {};
                $scope.resourceModal = {};
                roleModal = {}


                $scope.percentagefromTotal;

            }
        }


        ///////////////////////////////////////removeResource////////////////////////////////////////////////////////////////////
        $scope.removeResource = function (index, item) {
            debugger;
            $scope.getProjectDetailForVendorQuoteList.ResourcesList.splice(index, 1)
            
            for (var i = 0; i < $scope.getProjectDetailForVendorQuoteList.ResourcesList.length; i++) {
                $scope.getProjectDetailForVendorQuoteList.ResourcesList[i].PercentageFromTotal = (parseFloat($scope.getProjectDetailForVendorQuoteList.ResourcesList[i].ExpectedHours).toFixed(2) / parseFloat($scope.getProjectDetailForVendorQuoteList.TotalHours).toFixed(2)) * 100
                parseFloat($scope.getProjectDetailForVendorQuoteList.ResourcesList[i].PercentageFromTotal).toFixed(2)
            }


        }


        $scope.editQuoteForProject = function () {
            $scope.IsQuateSendForProject = true;
        }

        ///////////////////////////////////////removeResource////////////////////////////////////////////////////////////////////
        $scope.rejectQuoteByVendor = function (index, item) {
            debugger;
  
            $("#modalAcceptRejectQuote").modal('show')
          

        }

        ///////////////////////////////////////////////////////////////////////////////////////

        $scope.saveRejectQuoteByVendor = function (QuoteReasonReject, ReasonReject, QuoteRejectReasonByvendorForm) {
            debugger
            $('#globalLoader').show();
            if(ReasonReject)
            {
                QuoteRejectReasonByvendorForm.QuoteReasonReject.$dirty = true;
            }
            else {
                var data = {}
                data.ReasonReject = QuoteReasonReject;
                data.VendorId = CompanyId;
                data.ProjectId = ProjectId;
                data.UserId = UserId;
                vendorFactory.saveRejectQuoteByVendor(data)
                .success(function (data) {
                    $("#modalAcceptRejectQuote").modal('hide')
                    $scope.QuoteReasonReject = "";

                    if(data.ResponseData == 1)
                    {
                        toaster.success("Success", message.successdataLoad);
                        $location.path('project/toquote');
                    }
                    else {
                        toaster.error("Error", message.error)
                    }
                    $('#globalLoader').hide();
                }), Error(function () {
                    toaster.error("Error", message.error)
                    $('#globalLoader').hide();
                })
            }

        }
      


        ///////////////////////////////////////saveQuoteAcceptByvendor////////////////////////////////////////////////////////////////////
        $scope.saveQuoteAcceptByvendor = function () {

            debugger;
            $('#globalLoader').show();

            $scope.getProjectDetailForVendorQuoteList.VendorId = CompanyId;
            $scope.getProjectDetailForVendorQuoteList.TotalBudget = $scope.getProjectDetailForVendorQuoteList.TotalBudget;
            $scope.getProjectDetailForVendorQuoteList.TotalHours = $scope.getProjectDetailForVendorQuoteList.TotalHours
            $scope.getProjectDetailForVendorQuoteList.AllDescription = $scope.getProjectDetailForVendorQuoteList.AllDescription
            $scope.getProjectDetailForVendorQuoteList.ResourcesList1 = $scope.getProjectDetailForVendorQuoteList.ResourcesList;
            $scope.getProjectDetailForVendorQuoteList.UserId = UserId;

            var data = {}
            data = JSON.stringify($scope.getProjectDetailForVendorQuoteList);

          
            vendorFactory.saveQuoteAcceptByvendor(data)
            .success(function (data) {
                debugger;
                if (data.ResponseData == 1) {
                    toaster.success("Success", message.QuoteAcceptByvendorSuccess);
                    $location.path('/project/toquote');
                }
                else {
                    toaster.error("Error", message.error)

                }
                $('#globalLoader').hide();
            }), Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();
            })
        }



        

        ///////////////////////////////////////EditSaveQuoteAcceptByvendor////////////////////////////////////////////////////////////////////
        $scope.editSaveQuoteAcceptByvendor = function () {

            debugger;
              $('#globalLoader').show();
            //$timeout
              $scope.getProjectDetailForVendorQuoteList.VendorId = VendorId;
           $scope.getProjectDetailForVendorQuoteList.TotalBudget;
           $scope.getProjectDetailForVendorQuoteList.TotalHours
           $scope.getProjectDetailForVendorQuoteList.AllDescription
           $scope.getProjectDetailForVendorQuoteList.ResourcesList1 =   $scope.getProjectDetailForVendorQuoteList.ResourcesList;

           var data = {}
         
           data = JSON.stringify($scope.getProjectDetailForVendorQuoteList);

            vendorFactory.editSaveQuoteAcceptByvendor(data)
            .success(function (data) {
                debugger;
                if (data.ResponseData == 1) {
                    toaster.success("Success", message.QuoteAcceptByvendorSuccess);
                    $location.path('/project/toquote');
                }
                else {
                    toaster.error("Error", message.error)

                }
                $('#globalLoader').hide();
            }), Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();
            })
        }



        $scope.cancelQuoteAcceptByvendor = function () {
            $location.path('/project/quotefor');
        }


        $scope.downloadVendorFile = function (FilePath, FileName, ActualFileName) {
            
            $('#globalLoader').show();
            var url = apiURL.baseAddress + "VendorApi/";

            $http({
                method: 'Post',
                url: url + "GetFiledownload/",
                data: { FileName: FileName, FilePath: FilePath },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                headers = headers();
                debugger;
                //var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", ActualFileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
          
                }

                $('#globalLoader').hide();
            }).error(function (data) {
         
                $('#globalLoader').hide();
            });
        };

    })

})