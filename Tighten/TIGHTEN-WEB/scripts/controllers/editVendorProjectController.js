﻿


define(['app'], function () {

    app.controller("editVendorProjectController", function ($scope, $timeout, sharedService, toaster, $rootScope, vendorProjectFactory, $stateParams, $filter, apiURL, $http, $location, companyAccountFactory, projectsFactory) {


        var ProjectId = $stateParams.pid;
        var CompanyId = sharedService.getCompanyId()
        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }
            $scope.format = 'MM/dd/yyyy';
            getNewProjectForEdit(ProjectId, CompanyId)
        })


        $scope.hideProjectInfo = true;
        $scope.hideShowRisk = false;
        $scope.hideShowPhaseMile = false;
        $scope.hideShowVendorInfo = false;
        $scope.editPhasebuttonShow = false;
        $scope.editMilestonebuttonShow = false;
        $scope.ProjectIsEdit = true;
        $scope.myFirstCurrent = 'current';
        $scope.mySecondcCurrent = '';
        $scope.myThirdCurrent = ''
        $scope.myForthCurrent = '';
        $scope.mySeverityList = [{ Text: "Normal", Value: "Normal" }, { Text: "High", Value: "High" }];
        $scope.myStatusList = [{ Text: "Open", Value: "Open" }, { Text: "Resolved", Value: "Resolved" }];

        $scope.AddNewProjectModel = {
            ProjectName: "",
            ProjectStartDate: "",
            ProjectEndDate: "",
            projectBudget: "",
            ProjectDescription: "",
            UserId: sharedService.getUserId(),
            CompanyId: CompanyId,
            PhaseList: [{ Name: "", Budget: "", Description: "", MileStone: [], PhaseDocumentsName: [] }],
            ProjectDocList: [],
            VendorList: [],
            RiskList:[]

        }
        $scope.Phase = { Name: "", Budget: "", Description: "", PhaseDocumentsName: [], MileStone: [] }
        $scope.phaseModel = {};
        $scope.milestoneModal = {};
        $scope.MileStoneModel = { Name: "", Description: "", EstimatedHours: "" }
        $scope.PhaseDocumentAttachedView = [];
        $scope.myProjectVendor = [];
        $scope.popup = { opened1: false,opened2: false};

        // custom control
        $scope.CustomDirectiveReady = false;
        $scope.customcontrolmodel = {};


        var customControlId = 0


        $scope.open1 = function () {
            $scope.popup.opened1 = true;
            $scope.mindate = new Date();
        };

        $scope.open2 = function () {
            $scope.popup.opened2 = true;
            $scope.mindate1 = new Date($scope.AddNewProjectModel.projectStartDate);
        };

        //////////////////////////////enable text box////////////////////////////////////
        $scope.editNewProject = function () {
            $scope.ProjectIsEdit = false;
        }


        ////////////////////////////// Opern milestone PopUpFor Edit//////////////////////////////////////////////////////////
        $scope.openMilestoneEdit = function (index, item) {
            debugger
            $scope.PhaseIndexForMilestone = index
            $scope.phaseNameONMile = item.Name
            $('#modalEditAndAddNewMileStone').modal('show');


        }


        function getProjectVendorByCompIdId(CompanyId, AccId) {
            debugger;
            projectsFactory.getProjectVendorByCompIdId(CompanyId, AccId)
.success(function (data) {
    $scope.TempVendorList = [];
    debugger;
    $scope.TempVendorList = $scope.AddNewProjectModel.VendorPOCList;
    $scope.AddNewProjectModel.VendorPOCList = data.ResponseData;
    //getCustomControl(CompanyId);

}).error(function (data, status, headers, config) {
    toaster.error("Error", "Some Error Occured !");
    $('#globalLoader').hide();
});

        }
        getProjectVendor(CompanyId, ProjectId)
        function getProjectVendor(CompanyId, ProjectId) {
            debugger
            projectsFactory.getProjectVendor(CompanyId, ProjectId)
        .success(function (data) {
            $scope.AddNewProjectModel.VendorPOCList = data.ResponseData;
           // $scope.AddNewProjectModel.VendorList = data.ResponseData;
           // getCustomControl(CompanyId);

        }).error(function (data, status, headers, config) {
            toaster.error("Error", "Some Error Occured !");
            $('#globalLoader').hide();
        });

        }

        getAccounts(CompanyId);
        function getAccounts(CompanyId) {
            debugger
            companyAccountFactory.GetCompanyAccByCompanyId(CompanyId)
        .success(function (data) {
            debugger;
            $scope.AddNewProjectModel.CompanyAccList = data.ResponseData;
            // getCustomControl(CompanyId);

        }).error(function (data, status, headers, config) {
            toaster.error("Error", "Some Error Occured !");
            $('#globalLoader').hide();
        });

        }

        $scope.TempVendorList = [];
        /////////////////////////////////////changeVendor Add Vendor for Project////////////////////////////////////////////////////////////
        $scope.changeAccount = function (item, x) {

            debugger;
            if (item != null) {
                var currentSelected = $filter('filter')($scope.AddNewProjectModel.CompanyAccList, item)[0];
                for (var i = 0; i < $scope.AddNewProjectModel.CompanyAccList.length; i++) {
                    if ($scope.AddNewProjectModel.CompanyAccList[i].VendorId == currentSelected.VendorId) {
                        $scope.AddNewProjectModel.CompanyAccList[i].IsAdded = true;
                        //break;
                    }
                }
                getProjectVendorByCompIdId(CompanyId, currentSelected.Id);
                $scope.SelectedAcc = currentSelected;
                // currentSelected.IsAdded = true;
            }

        }

        function getNewProjectForEdit(ProjectId, CompanyId) {
            debugger;
            $('#globalLoader').show();
            //alert(CompanyId);
            vendorProjectFactory.getNewProjectForEdit(ProjectId, CompanyId)
                .success(function (data) {
                    debugger;
                    if (data.success) {

                        var getEditProjectList;
                        getEditProjectList = data.ResponseData;
                        //$filter('date')(getEditProjectList.ProjectStartDate, "MM/dd/yyyy");
                        $scope.AddNewProjectModel.projectName = getEditProjectList.ProjectName;
                        $scope.AddNewProjectModel.RiskList = getEditProjectList.RiskList;
                        $scope.AddNewProjectModel.projectStartDate = moment(new Date(getEditProjectList.ProjectStartDate)).format("MM/DD/YYYY");
                        $scope.AddNewProjectModel.projectEndDate = moment(new Date(getEditProjectList.ProjectEndDate)).format("MM/DD/YYYY");
                        $scope.AddNewProjectModel.projectBudget = getEditProjectList.ProjectBudget;
                        $scope.AddNewProjectModel.projectDescription = getEditProjectList.ProjectDescription;
                        $scope.AddNewProjectModel.IsQuoteSent = getEditProjectList.IsQuoteSent;
                        $scope.AddNewProjectModel.PhaseList = getEditProjectList.PhaseList;
                        $scope.AddNewProjectModel.ProjectDocList = getEditProjectList.ProjectDocList;
                        $scope.AddNewProjectModel.VendorList = getEditProjectList.VendorList;

                      
                        for (var i = 0; i < $scope.AddNewProjectModel.CompanyAccList.length; i++) {
                            for (var j = 0; j < $scope.AddNewProjectModel.VendorList.length; j++) {
                                if ($scope.AddNewProjectModel.CompanyAccList[i].Id == $scope.AddNewProjectModel.VendorList[j].Id) {
                                    $scope.AddNewProjectModel.CompanyAccList[i].IsAdded = true;
                                    //break;
                                }
                            }
                        }
                        debugger;
                        for (var i = 0; i < $scope.AddNewProjectModel.RiskList.length; i++) {
                           
                                
                            $scope.AddNewProjectModel.RiskList[i].Createdon = moment(new Date($scope.AddNewProjectModel.RiskList[i].Createdon)).format("MM/DD/YYYY");

                            $scope.AddNewProjectModel.RiskList[i].IsAdded = false;
                            for (var j = 0; j < $scope.AddNewProjectModel.RiskList[i].RiskComments.length; j++) {
                                if ($scope.AddNewProjectModel.RiskList[i].RiskComments[j] != undefined && $scope.AddNewProjectModel.RiskList[i].RiskComments[j] != null) {
                                    $scope.AddNewProjectModel.RiskList[i].RiskComments[j].IsAdded = false;
                                    $scope.AddNewProjectModel.RiskList[i].RiskComments[j].UserName = sharedService.getNameId() + " " + sharedService.getLastNameId();
                                }
                            }
                        }

                        for (var i = 0; i < $scope.AddNewProjectModel.VendorPOCList.length; i++) {
                            //for (var j = 0; j < $scope.AddNewProjectModel.VendorList.length; j++) {
                                //if ($scope.AddNewProjectModel.VendorPOCList[i].VendorId == $scope.AddNewProjectModel.VendorList[j].VendorId) {
                            $scope.AddNewProjectModel.VendorPOCList[i].IsAdded = $scope.AddNewProjectModel.VendorList[i].IsAdded;
                                    //break;
                                //}
                            //}
                        }
                        //------getting custom control area ---------//

                        if (getEditProjectList.CustomControlValueModel != null) {
                            $scope.customcontrolmodel = JSON.parse(getEditProjectList.CustomControlValueModel.CustomControlData);
                            customControlId = getEditProjectList.CustomControlValueModel.Id;
                         
                        }
                        //-----------------Custom cuntrol is not use by Company---------------------------------------///
                      
                        $scope.CustomcontrolList = getEditProjectList.CustomControlUIList;
                        debugger;
                        if ($scope.CustomcontrolList != null && $scope.CustomcontrolList.length > 0) {
                            $scope.CustomDirectiveReady = true;
                        }
                        else
                        {
                            $scope.CustomDirectiveReady = false;
                        }
                        toaster.success("Success", message.successdataLoad);
                      
                    }
                    else {
                        toaster.success("error", message.error);
                    }

                    $('#globalLoader').hide();
                }),
                Error(function (data, status, headers, config) {
                    $('#globalLoader').hide();
                    toaster.error("error", message.error);

                });
        }
        //////////////////////////////////////////////nextFromProjectWizEdit//////////////////////////////////////////////////////
        $scope.nextFromProjectWizEdit = function (isVendorFirstNameInvalid, NewProjectToVendorForm, projectAddUpdateModel) {
            if (isVendorFirstNameInvalid) {
                NewProjectToVendorForm.projectName.$dirty = true
            }
            else {
                $scope.AddNewProjectModel.ProjectName = projectAddUpdateModel.projectName
                $scope.AddNewProjectModel.projectStartDate = $filter('date')(projectAddUpdateModel.projectStartDate, "MM/dd/yyyy");
                $scope.AddNewProjectModel.projectEndDate = $filter('date')(projectAddUpdateModel.projectEndDate, "MM/dd/yyyy");
                $scope.AddNewProjectModel.projectBudget = projectAddUpdateModel.projectBudget;
                $scope.hideProjectInfo = false;
                $scope.hideShowRisk = true;
                $scope.hideShowPhaseMile = false;
                $scope.hideShowVendorInfo = false;
            }
            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = 'current';
            $scope.myThirdCurrent = '';
            $scope.myForthCurrent = '';

        }

        //////////////////////////////////////////////nextFromPhaseToVendor//////////////////////////////////////////////////////
        $scope.nextFromPhaseToVendorEdit = function () {
            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = ''
            $scope.myForthCurrent = 'current';
            $scope.hideProjectInfo = false;
            $scope.hideShowEdit = false;    // 
            $scope.hideShowPhaseMile = false;
            $scope.hideShowVendorInfo = true;
            $scope.editPhasebuttonShow = false; // update button hide
        }

        ////////////////////////////////////////previousFromRisktoProject/////////////////////////////////////////////////////////////

        $scope.previousFromRisktoProject = function () {
            debugger;

            //  class for wizard
            $scope.myFirstCurrent = 'current';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = '';
            $scope.myForthCurrent = '';
            $scope.hideProjectInfo = true;
            $scope.hideShowRisk = false;
            $scope.hideShowPhaseMile = false;    // 
            $scope.hideShowVendorInfo = false;

            $scope.editPhasebuttonShow = false; // update button hide
        }

        //////////////////////////////////////////////nextFromRiskToPhase//////////////////////////////////////////////////////
        $scope.nextFromRiskToPhase = function () {
            debugger;

            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = 'current';
            $scope.myForthCurrent = '';
            $scope.hideProjectInfo = false;
            $scope.hideShowRisk = false;
            $scope.hideShowPhaseMile = true;    // 
            $scope.hideShowVendorInfo = false;

            $scope.editPhasebuttonShow = false; // update button hide
        }

        

        //////////////////////////////////////////////previousFromVendortoProjectEdit//////////////////////////////////////////////////////
        $scope.previousFromVendortoProjectEdit = function () {
            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = 'current';
            $scope.myForthCurrent = '';
            $scope.hideProjectInfo = false;
            $scope.hideShowEdit = false;
            $scope.hideShowPhaseMile = true;    // 
            $scope.hideShowVendorInfo = false;

            $scope.editPhasebuttonShow = false; // update button hide
        }
        /////////////////////////////////////previousFromProjectWiz///////////////////////////////////////////////////////////
        $scope.previousFromPhaseToRiskEdit = function () {
            debugger;


            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = 'current';
            $scope.myThirdCurrent = '';
            $scope.myForthCurrent = '';

            $scope.hideProjectInfo = false;
            $scope.hideShowRisk = true;
            $scope.hideShowPhaseMile = false;    // 
            $scope.hideShowVendorInfo = false;
            $scope.editPhasebuttonShow = false; // update button hide
        }
        /////////////////////////////////////previousFromProjectWiz///////////////////////////////////////////////////////////
        $scope.previousFromPhaseToProjectEdit = function () {
            //  class for wizard
            $scope.myFirstCurrent = 'current';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = ''
            $scope.myForthCurrent = '';
            $scope.hideProjectInfo = true;
            $scope.hideShowPhaseMile = false;    // 
            $scope.hideShowVendorInfo = false;
            $scope.editPhasebuttonShow = false; // update button hide
        }

        $scope.openMilestone = function (index, item) {
            $scope.PhaseIndexForMilestone = index
            $scope.phaseNameONMile = item.Name
            $('#modalAddNewMileStone').modal('show');

        }

        /////////////////////////////////////addNewPhaseEditProject///////////////////////////////////////////////////////////
        $scope.addNewPhaseEditProject = function (IsNamevalid, Phaseform, phase) {
            if (IsNamevalid) {
                Phaseform.phaseName.$dirty = true;
            }
            else {
                var data = { Name: "", Budget: "", Description: "", PhaseDocumentAttached: [], MileStone: [], PhaseDocumentsName: [] };
                data.Name = phase.name;
                data.Budget = phase.budget;
                data.Description = phase.description;
                data.PhaseDocumentsName = $scope.PhaseDocumentAttachedView;
                data.CreateOn = moment(new Date()).format("MM/DD/YYYY");
                $scope.AddNewProjectModel.PhaseList.push(data);
                $scope.PhaseDocumentAttachedView = [];
                $scope.phaseModel = {};
                Phaseform.phaseName.$dirty = false;
                var input = $("#fileInput");
                clearInput(input);
                function clearInput(input) {
                    input = input.val('').clone(true);
                };

            }
        }

        /////////////////////////////////////editPhaseEditProject///////////////////////////////////////////////////////////
        $scope.editPhaseEditProject = function (index, item) {
            $scope.phaseModel.name = item.Name;
            $scope.phaseModel.description = item.Description;
            $scope.phaseModel.budget = item.Budget;
            $scope.createOn = item.CreateOn;
            $scope.PhaseDocumentAttachedView = item.PhaseDocumentsName;

            for (var i = 0; i < item.PhaseDocumentsName.length; i++) {
                $scope.PhaseDocumentAttachedView[i].name = item.PhaseDocumentsName[i].ActualFileName;

            }
            $scope.phaseModel.PhaseIndex = index;

            $scope.editPhasebuttonShow = true
        }

        //////////////////////////////////////////////////updatePhaseEditProject///////////////////////////////////////////////////////////////////////////////////////////////////

        $scope.updatePhaseEditProject = function (IsNamevalid, Phaseform, phase) {
            if (IsNamevalid) {
                Phaseform.phaseName.$dirty = true;
            }
            else {

                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].Name = phase.name
                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].Budget = phase.budget;
                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].Description = phase.description;
                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].CreateOn = $scope.createOn;
                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].PhaseDocumentsName = $scope.PhaseDocumentAttachedView;
                $scope.PhaseDocumentAttachedView = [];
                $scope.phaseModel = {};
                Phaseform.phaseName.$dirty = false;

                $scope.editPhasebuttonShow = false;

            }
        }
        ////////////////////////////////////////deletePhaseConfirm////////////////////////////////////////////////////////////////////////////
        var PhasecurrentIndex
        $scope.deletePhaseConfirm = function (index, item) {
            $('#modalConfirmDeletePhase').modal('show')
            $scope.phaseNameONMile = item.Name
            PhasecurrentIndex = index;
        }
        ////////////////////////////////////////deletePhase////////////////////////////////////////////////////////////////////////////

        $scope.deletePhase = function (index, item) {
            $scope.AddNewProjectModel.PhaseList.splice(PhasecurrentIndex, 1)
            $('#modalConfirmDeletePhase').modal('hide')
        }

        ///////////////////////////////removePhaseAttachmentsEditProject///////////////////////////////////////////////////////////////////////////////////////
        $scope.removePhaseAttachmentsEditProject = function (index, Id, FileName) {
            var parentIndex = $scope.phaseModel.PhaseIndex;
            data = {};

            data.Id = Id
            data.ProjectId = 0
            data.FileName = FileName;

            if (Id != undefined) {
                deleteEditProjectPhaseFiles(data, parentIndex, index)

            }
            else {

                if (parentIndex != undefined) {
                    $scope.AddNewProjectModel.PhaseList[parentIndex].PhaseDocumentsName.splice(index, 1);
                }
                $scope.PhaseDocumentAttachedView.splice(index, 1);
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        function deleteEditProjectPhaseFiles(data, parentIndex, index) {
            $('#globalLoader').show();
            return vendorProjectFactory.deleteEditProjectFiles(data)
          .success(function myfunction(data) {
              if (data.success) {
                  if (data.ResponseData == 1) {
                      toaster.success("Success", message.FileDeleted);

                      if (parentIndex != 'undefined') {
                          $scope.AddNewProjectModel.PhaseList[parentIndex].PhaseDocumentsName.splice(index, 1);
                      }
                      $scope.PhaseDocumentAttachedView.splice(index, 1);

                  }
                  else {
                      toaster.error("Error", message.error);
                  }
                  $('#globalLoader').hide();

              }
              

          }).
          error(function (data, status, headers, config) {
              toaster.error("Error", message.error);
              $('#globalLoader').hide();

          })


        }


        /////////////////////////////////////addMilestone///////////////////////////////////////////////////////////
        $scope.PhaseIndexForMilestone;
        $scope.openMilestoneEditProject = function (index, item) {
            $scope.PhaseIndexForMilestone = index
            $scope.phaseNameONMile = item.Name
            $('#modalEditAndAddNewMileStone').modal('show');
        }



        ////////////////////////////////////////////////openMilestoneEditProject///////////////////////////////////////////////////////////////////////////
        $scope.addNewMilestoneEditProject = function (IsVendorNameValid, AddNewMilestone, milestoneModal) {
            if (IsVendorNameValid) {

                AddNewMilestone.milestoneName.$dirty = true
            }
            else {
                var data = {}
                data.Name = milestoneModal.name;
                data.CreateOn = moment(new Date()).format("MM/DD/YYYY");
                data.Description = milestoneModal.description;
                data.Budget = milestoneModal.budget;
                $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone.push(data);
                $scope.milestoneModal = {};
                AddNewMilestone.milestoneName.$dirty = false;

            }
        }

        ////////////////////////////////////////////////editMilesonte////////////////////////////////////////////////////////////////////////////
        var mileStoneIndex;
        $scope.editMilesonte = function (index, item) {
            $scope.milestoneModal.name = item.Name;
            $scope.milestoneModal.description = item.Description;
            $scope.milestoneModal.budget = item.Budget
            mileStoneIndex = index;
            $scope.editMilestonebuttonShow = true;
        }

        //////////////////////////////////////////////////////updateMilestone/////////////////////////////////////////////////////////////////////////////////////////////

        $scope.updateMilestoneEditProject = function (IsVendorNameValid, EditNewMilestone, milestoneModal) {
            if (IsVendorNameValid) {
                EditNewMilestone.milestoneName.$dirty = true;
            }
            else {
                $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone[mileStoneIndex].Name = milestoneModal.name;
                $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone[mileStoneIndex].Description = milestoneModal.description;
                $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone[mileStoneIndex].Budget = milestoneModal.budget
                $scope.milestoneModal = {};
                EditNewMilestone.milestoneName.$dirty = false;
                $scope.editMilestonebuttonShow = false;

            }
        }

        ////////////////////////////////////////////////confirm////////////////////////////////////////////////////////////////////////////

        var currentMileIndex
        $scope.deleteConfirmMilestone = function (index, item) {
            $('#modalConfirmDeleteMilestone').modal('show')
            currentMileIndex = index;
        }

        ///////////////////////////////delete///////////////////////////////////////
        $scope.deleteMilestone = function () {
            $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone.splice(currentMileIndex, 1);
            $('#modalConfirmDeleteMilestone').modal('hide')
            $scope.editMilestonebuttonShow = false;
        }


        /////////////////////////////selectFileForProjectEdit//////////////////////////////////////////////////
        var PhaseIndex;
        $scope.selectFileForEditProject = function ($files, index) {
            for (var i = 0; i < $files.length; i++) {

                if ($files[0].size < 10485760) {
                    $scope.AddNewProjectModel.ProjectDocList.push($files[i])
                    var l = $scope.AddNewProjectModel.ProjectDocList.length - 1;
                    $scope.AddNewProjectModel.ProjectDocList[l].ActualFileName = $files[i].name;
                }
                else {
                    toaster.error("Error", message.FileSizeExceeds);
                }
            }
        }
        //////////////////////////////////////////////removeProjectAttachments/////////////////////////////////////////////////////

        $scope.removeProjectAttachmentsEditProject = function (index, img) {
            data = {};

            data.Id = img.Id
            data.ProjectId = 1
            data.FileName = img.FileName;

            if (img.Id != undefined) {
                deleteEditProjectfiles(data, index) //delete file form server
            }
            else {
                $scope.AddNewProjectModel.ProjectDocList.splice(index, 1)
            }
        }


        ///////////////////////////////////////////////deleteEditProjectfiles////////////////////////////////////////////////////////////////
        function deleteEditProjectfiles(data, index) {
            $('#globalLoader').show();
            return vendorProjectFactory.deleteEditProjectFiles(data)
          .success(function myfunction(data) {
              if (data.success) {
                  if (data.ResponseData == 1) {
                      toaster.success("Success", message.FileDeleted);
                      $scope.AddNewProjectModel.ProjectDocList.splice(index, 1)
                  }
                  else {
                      toaster.error("Error", message.error);
                  }

              }
              return data.ResponseData

          }).
          error(function (data, status, headers, config) {
              toaster.error("Error", message.error);
              $('#globalLoader').hide();

          })


        }

        /////////////////////////////////////////////////openModalRemoveVendor///////////////////////////////////////////////////////////////////////////////////////////

        $scope.currentDelVendorIndex = {};
        $scope.openModalRemoveVendor = function (obj) {
            debugger;
            $scope.currentDelVendorIndex = obj;
            $('#modalConfirmDeleteVendor').modal('show');

        }

        /////////////////////////////////////////////////removeVendor///////////////////////////////////////////////////////////////////////////////////////////

        //$scope.removeVendor = function () {
        //    debugger;

        //    var currentSelected = $filter('filter')($scope.AddNewProjectModel.VendorList, $scope.currentDelVendorIndex)[0];
        //    currentSelected.IsAdded = false;
        //    $('#modalConfirmDeleteVendor').modal('hide');
        //}
        $scope.removeVendor = function (item) {
            debugger;
            var currentSelected = $filter('filter')($scope.AddNewProjectModel.VendorList, $scope.currentDelVendorIndex)[0];
            currentSelected.IsAdded = false;
            for (var i = 0; i < $scope.AddNewProjectModel.VendorPOCList.length; i++) {
                if ($scope.AddNewProjectModel.VendorPOCList[i].VendorId == currentSelected.VendorId) {
                    $scope.AddNewProjectModel.VendorPOCList[i].IsAdded = false;
                    break;
                }
            }
            for (var i = 0; i < $scope.AddNewProjectModel.CompanyAccList.length; i++) {
                if ($scope.AddNewProjectModel.CompanyAccList[i].VendorId == currentSelected.VendorCompanyId) {
                    $scope.AddNewProjectModel.CompanyAccList[i].IsAdded = false;
                    //break;
                }
            }
            $('#modalConfirmDeleteVendor').modal('hide');
        }

        /////////////////////////////changeVendor//////////////////////////////////////////////////

        $scope.changeVendor = function (item, x) {
            debugger;
            if (item != null) {
                var currentSelected = $filter('filter')($scope.AddNewProjectModel.VendorList, item)[0];
                currentSelected.IsAdded = true;
            }
        }
        $scope.SelectedAcc = {};
        $scope.changeVendor = function (item, x) {

            debugger;
            if (item != null) {
                var currentAccSelected = $filter('filter')($scope.AddNewProjectModel.CompanyAccList, $scope.SelectedAcc)[0];
                if (currentAccSelected != null && currentAccSelected.Id != null) {
                    var currentSelected = $filter('filter')($scope.AddNewProjectModel.VendorPOCList, item)[0];
                    for (var i = 0; i < $scope.AddNewProjectModel.VendorList.length; i++) {
                        if ($scope.AddNewProjectModel.VendorList[i].VendorId == item.VendorId) {
                            $scope.AddNewProjectModel.VendorList[i].IsAdded = true;
                            break;
                        }
                    }
                    currentSelected.IsAdded = true;

                    currentAccSelected.IsAdded = true;

                    $scope.SelectedAcc = {};
                }
            }

        }




        /////////////////////////////selectFileForDocumentPhase1//////////////////////////////////////////////////
        var PhaseIndex;
        $scope.selectFileForDocumentPhaseEdit = function ($files, index) {
            debugger;


            for (var i = 0; i < $files.length; i++) {

                if ($files[0].size < 10485760) {
                    $scope.PhaseDocumentAttachedView.push($files[i])
                    var l = $scope.PhaseDocumentAttachedView.length - 1;
                    $scope.PhaseDocumentAttachedView[l].ActualFileName = $files[i].name;

                }
                else {
                    toaster.error("Error", message.FileSizeExceeds);
                }
            }
        }
        ////////////////////////////////////////deletePhase////////////////////////////////////////////////////////////////////////////
        var PhasecurrentIndex
        $scope.deletePhaseConfirmdEditProject = function (index, item) {
            debugger;
            $('#modalConfirmDeletePhase').modal('show')
            $scope.phaseNameONMile = item.Name
            PhasecurrentIndex = index;
        }
        ////////////////////////////////////////downloadProjectPhaseFileEdit////////////////////////////////////////////////////////////////////////////
        $scope.downloadProjectPhaseFileEdit = function (FilePath, FileName, ActualFileName) {
            debugger;
            $('#globalLoader').show();
            var url = apiURL.baseAddress + "VendorApi/";

            $http({
                method: 'Post',
                url: url + "GetFiledownload/",
                data: { FileName: FileName, FilePath: FilePath },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                headers = headers();
                debugger;
                //var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", ActualFileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    console.log(ex);
                }

                $('#globalLoader').hide();
            }).error(function (data) {
                console.log(data);
                $('#globalLoader').hide();
            });
        };



        //    testing with form data
        $scope.SavenewEditProject = function (modal, IsQuoteSent) {

            debugger;
            var j = 0
            for (var i =0; i < $scope.AddNewProjectModel.VendorList.length;i++ )
            {
                if($scope.AddNewProjectModel.VendorList[i].IsAdded == false)
                {
                    j++
                }
                if ($scope.AddNewProjectModel.VendorList.length == j) {
                    toaster.warning("Warning", message.notSelectedVenodr);
                    return;
                }
            }

             $('#globalLoader').show();
            $scope.AddNewProjectModel.ProjectId = ProjectId;
            $scope.AddNewProjectModel.ProjectName = modal.projectName;
            $scope.AddNewProjectModel.ProjectStartDate = modal.projectStartDate;
            $scope.AddNewProjectModel.ProjectEndDate = modal.projectEndDate
            $scope.AddNewProjectModel.ProjectBudget = modal.projectBudget == "" ? 0 : modal.projectBudget;
            $scope.AddNewProjectModel.ProjectDescription = modal.projectDescription;
            $scope.AddNewProjectModel.IsQuoteSent = IsQuoteSent;
            $scope.AddNewProjectModel.CustomControlData = JSON.stringify($scope.customcontrolmodel);
            $scope.AddNewProjectModel.CustomControlId = customControlId;

            var projecturl = apiURL.baseAddress + "ProjectsApi/";
            $scope.jsonData = $scope.AddNewProjectModel;;
            $http({
                method: 'POST',
                url: projecturl + "EditNewProject",
                headers: { 'Content-Type': undefined },

                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("model", angular.toJson(data.model));

                    //for (l = 0; l < $scope.AddNewProjectModel.ProjectDocList.length; l++) {
                    //    if ($scope.AddNewProjectModel.ProjectDocList[l].File) {
                    //        formData.append("Project", $scope.AddNewProjectModel.ProjectDocList[l]);
                    //    }

                    //}

                    for (l = 0; l < $scope.AddNewProjectModel.ProjectDocList.length; l++) {

                        var File = $scope.AddNewProjectModel.ProjectDocList[l]
                        if (File.size > 0) {
                            formData.append("Project", $scope.AddNewProjectModel.ProjectDocList[l]);
                        }

                    }

                    for (i = 0; i < $scope.AddNewProjectModel.PhaseList.length; i++) {
                        for (l = 0; l < $scope.AddNewProjectModel.PhaseList[i].PhaseDocumentsName.length; l++) {
                            formData.append("Phase_" + i + "_" + l, $scope.AddNewProjectModel.PhaseList[i].PhaseDocumentsName[l]);
                        }
                    }

                    return formData;
                },

                data: { model: $scope.jsonData, files: $scope.files }
            }).
                success(function (data, status, headers, config) {
                    debugger;
                    $('#globalLoader').hide();

                    if (data.success) {
                        if (data.ResponseData == 1) {
                            toaster.success("Success", message.NewProjectUpdateSuccesfully);
                            $location.path('/quoted/project');
                        }
                        else if (data.ResponseData == 2) {
                            toaster.success("Success", message.NewProjectUpdateSuccesfully);
                            $location.path('/quoted/project');
                        }

                        else if (data.ResponseData == -1) {
                            toaster.warning("Warning", message.DuplicateProject);
                        }

                    }
                    else {
                        toaster.error("error", message.error);
                    }

                }).
                error(function (data, status, headers, config) {
                    $('#globalLoader').hide();

                });


        }
        ////////////////////////////////////////OpenRiskModal/////////////////////////////////////////////////////////////

        $scope.OpenRiskModal = function () {
            debugger;
            $scope.riskModel = {};
            $('#modalAddNewRisk').modal('show');
        }

        ////////////////////////////////////////AddNewRisk/////////////////////////////////////////////////////////////

        $scope.addNewRisk = function (IsRiskTitleInvalid, IsSeverityInvalid, IsStatusInvalid, RiskForm, RiskModel) {
            debugger;
            if (IsRiskTitleInvalid || IsSeverityInvalid || IsStatusInvalid) {
                RiskForm.riskTitle.$dirty = true;
                RiskForm.severity.$dirty = true;
                RiskForm.status.$dirty = true;
            }
            else {
                var data = { RiskTitle: "", Description: "", Severity: "", Status: "", IsDeleted: false, Createdon: "", NoOfComments: 0, RiskComments: [] };
                data.Id = 0;
                data.RiskTitle = RiskModel.RiskTitle;
                data.Description = RiskModel.Description;
                data.Severity = RiskModel.Severity.Value;
                data.Status = RiskModel.Status.Value;
                data.Createdon = moment(new Date()).format("MM/DD/YYYY");
                data.IsAdded = true;
                data.IsDeleted = false;
                $scope.AddNewProjectModel.RiskList.push(data);

                $scope.riskModel = {};
                RiskForm.riskTitle.$dirty = false;
                RiskForm.severity.$dirty = false;
                RiskForm.status.$dirty = false;
                $('#modalAddNewRisk').modal('hide');

            }
            //$('#modalAddNewRisk').modal('show');
        }

        //////////////////////////////////////////////////updateRisk///////////////////////////////////////////////////////////////////////////////////////////////////

        $scope.updateRisk = function (IsRiskTitleInvalid, IsSeverityInvalid, IsStatusInvalid, RiskForm, RiskModel) {

            debugger;
            if (IsRiskTitleInvalid || IsSeverityInvalid || IsStatusInvalid) {
                RiskForm.riskTitle.$dirty = true;
                RiskForm.severity.$dirty = true;
                RiskForm.status.$dirty = true;
            }
            else {



                $scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].RiskTitle = RiskModel.RiskTitle
                $scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].Description = RiskModel.Description;
                $scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].Severity = RiskModel.Severity.Value;
                $scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].Status = RiskModel.Status.Value;

                $scope.riskModel = {};
                RiskForm.riskTitle.$dirty = false;
                RiskForm.severity.$dirty = false;
                RiskForm.status.$dirty = false;
                $('#modalAddNewRisk').modal('hide');

            }


        }

        $scope.deleteRiskConfirm = function (index, item) {
            debugger;
            $('#modalConfirmDeleteRisk').modal('show')
            $scope.riskTitleONRisk = item.RiskTitle;
            $scope.RiskcurrentIndex = index;
        }
        $scope.deleteRisk = function (index, item) {
            debugger;
            if ($scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].IsAdded == true) {
                $scope.AddNewProjectModel.RiskList.splice($scope.RiskcurrentIndex, 1)
            }
            else {
                $scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].IsDeleted = true;
            }

            $('#modalConfirmDeleteRisk').modal('hide')

        }
        /////////////////////////////////////editRisk///////////////////////////////////////////////////////////
        $scope.editRisk = function (index, item) {
            debugger;
            $scope.riskModel.RiskTitle = item.RiskTitle;
            $scope.riskModel.Description = item.Description;
            $scope.riskModel.Severity = { Text: item.Severity, Value: item.Severity };
            $scope.riskModel.Status = { Text: item.Status, Value: item.Status };
            $scope.RiskcurrentIndex = index;
            $('#modalAddNewRisk').modal('show');
            $scope.editRiskbuttonShow = true
        }

        /////////////////////////////////////addMilestone///////////////////////////////////////////////////////////
        $scope.RiskIndexForComment;
        $scope.hideShowRiskComment = false;
        $scope.openRiskComment = function (index, item) {
            debugger
            $scope.RiskIndexForComment = index;
            $scope.riskTitleONComment = item.RiskTitle;
            $scope.descriptionOnComment = item.Description;
            $scope.severityOnComment = item.Severity;
            $scope.statusOnComment = item.Status;
            $scope.riskCommentModal = {};
            $scope.hideShowRiskComment = true;
            $('#modalEditAndAddNewRiskComment').modal('show');


        }
        /////////////////////////////////////addMilestone///////////////////////////////////////////////////////////
        $scope.RiskIndexForComment;
        $scope.openRiskCommentEdit = function (index, item) {

            debugger;
            $scope.RiskIndexForComment = index;
            $scope.riskTitleONComment = item.RiskTitle;
            $scope.descriptionOnComment = item.Description;
            $scope.severityOnComment = item.Severity;
            $scope.statusOnComment = item.Status;
            $scope.riskCommentModal = {};
            $scope.hideShowRiskComment = true;
            $('#modalEditAndAddNewRiskComment').modal('show');
        }
    })

})
