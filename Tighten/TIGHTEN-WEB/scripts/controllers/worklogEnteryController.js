﻿
define(['app'], function (app) {
    app.controller("worklogEnteryController", function ($scope, $rootScope, $stateParams, apiURL, $timeout, $http, $location, sharedService, $confirm, toaster, worklogFactory) {
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();
        var WorkLogId = $stateParams.id;
        $scope.imgURL = apiURL.imageAddress;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }
        $scope.worklogEnteryList = [];
     
        $scope.getWorkLogEnteryList = function (WorkLogId) {
            $('#globalLoader').show();
            //alert(UserId);
            worklogFactory.getWorkLogEnteryList(WorkLogId)
            .success(function (data) {

                debugger;
                if (data.ResponseData != null) {
                    $scope.worklogEnteryList = data.ResponseData;
                    for (var i = 0; i < $scope.worklogEnteryList.length; i++) {
                        if ($scope.worklogEnteryList[i] != undefined && $scope.worklogEnteryList[i] != null && $scope.worklogEnteryList[i].CreatedDate != undefined && $scope.worklogEnteryList[i].CreatedDate != null) {
                            //$scope.AddNewWorkLogModel.CreatedDate = moment($scope.AddNewWorkLogModel.CreatedDate, "MM/DD/YYYY").toDate();
                            var datedet = $scope.worklogEnteryList[i].CreatedDate.split('/');
                            if (datedet.length == 3) {
                                var newdate = datedet[1] + "/" + datedet[0] + "/" + datedet[2];
                                $scope.worklogEnteryList[i].CreatedDate = newdate;
                            }
                        }
                    }
                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
                $rootScope.activeTeam = [];
            }
            $scope.getWorkLogEnteryList(WorkLogId);
        });

        $scope.open1 = function () {
            $scope.popup.opened1 = true;
            $scope.mindate = new Date();
        };





    });
});