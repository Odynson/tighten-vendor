﻿
define(["app"], function (app) {

    app.controller("GeneralCompaniesOfVendorController", function ($scope, $rootScope, apiURL, $location, vendorFactory, sharedService, toaster, $http, $sce, teamFactory, $timeout, $confirm,$window) {
        var baseurl = apiURL.baseAddress;
        var weburl = apiURL.webAddress;//"http://localhost:54485/#/";
        $scope.imgURL = apiURL.imageAddress;
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();
        $scope.myEnterpriseList = [];  // my vendor for my vendor page
        $scope.companyVendorRespList = [];
        $scope.searchText = "";
        $scope.IsSaveVendor = true;
        $scope.IsEditVendor = true;
        $scope.IsViewVendor = true;
        $scope.IsManagePOC = false;
        $scope.getVenderCompanyList = [];
        $scope.Tags = {};
        $scope.Tags.Companies = [];
        $scope.openSaveCompanyTags = true;
        var dataModal = { CompanyId: CompanyId, Id: 0, IsApproved: null, VendorCompanyId: 0, VendorId: 0, Name: "", PageSize: 5, TotalPageCount: 0 }
        var dataCompanyModal = { CompanyId: CompanyId, Id: 0, IsApproved: null, VendorCompanyId: 0, VendorId: 0, CompanyName: "", PageSize: 5, TotalPageCount: 0 }
        $scope.maxSize = 2;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 1; // Maximum number of items per page.  
        $scope.email_address = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
        $scope.getVenderCompanyPOCList = [];
        $scope.DeletedVenderCompanyPOCList = [];
        $scope.vendorInvitationModal = {

            firstName: "",
            lastName: "",
            companyName: "",
            emaiAddress: "",
            messageTextBox: "",
            URL: "",
            vendorPhoneNo: "",
            CompanyIsVendorFirmRegistered: false,
            VendorCompanyIsVendorFirmRegistered: false,
            CId: 0,
            VendorPOCList: []
        }
        var that = this;
        that.colorName = null;
        
      

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');

            }

            var RoleId = sharedService.getRoleId()
            getMyEnterpriseCompanies(CompanyId)
            dataModal.CompanyId = CompanyId;

            //getCompanyVendorDrop(CompanyId);


        })

        ////---------------------------getCompanyVendorList---------------------------------------------///////////

        function getMyEnterpriseCompanies(CompanyId) {
            debugger;
            vendorFactory.getMyEnterpriseCompanies(CompanyId).success(function (data) {
                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.myEnterpriseList = data.ResponseData;
                    }
                }
                else {
                    toaster.error("Error", data.message);
                }

            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }

        ///------------------------getInvitaionToVendorsList--------------------------------------------------------

        function getInvitaionToVendors(CompanyId, UserId) {
            debugger;
            vendorFactory.getInvitaionToVendors(CompanyId, UserId).success(function (data) {
                debugger;
                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.CompanyInvitaionRespList = data.ResponseData;
                    }
                }


            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }

        var IdForRejectUPdate;
        $scope.ReasonReject = "";
        var tempdata = {};

        $scope.modalAddNewvendors = function () {
            debugger;
            //alert("Hello");
            $scope.Tags = {};
            $scope.Tags.Companies = [];
            $scope.IsSaveVendor = true;
            $scope.IsEditVendor = false;
            $scope.IsManagePOC = false;
            $scope.IsViewVendor = false;
            $('#modalAddNewvendors').modal('show');
            getCompanyListForVenderCompanyName();
        }
        $scope.EditVendorDetail = function (Id) {
            debugger;
            //alert(Id.toString());
            //$scope.Tags = {};
            //$scope.Tags.Companies = [];
            $scope.vendorInvitationModal = {

                firstName: "",
                lastName: "",
                companyName: "",
                emaiAddress: "",
                messageTextBox: "",
                URL: "",
                vendorPhoneNo: "",
                CompanyIsVendorFirmRegistered: false,
                VendorCompanyIsVendorFirmRegistered: false,
                CId: 0,
                VendorPOCList:[]
            }
            $scope.getVenderCompanyPOCList = [];
            $scope.IsSaveVendor = false;
            $scope.IsEditVendor = true;
            $scope.IsManagePOC = true;
            $scope.IsViewVendor = false;
            getVendorCompanyDetailById(Id);

            //getCompanyListForVenderCompanyName();
            
        }

        $scope.ViewEnterpriseDetail = function (Id) {
            debugger;
            $window.open(weburl + "company/profile/" + Id, '_blank');
            ////alert(Id.toString());
            ////$scope.Tags = {};
            ////$scope.Tags.Companies = [];
            //$scope.vendorInvitationModal = {

            //    firstName: "",
            //    lastName: "",
            //    companyName: "",
            //    emaiAddress: "",
            //    messageTextBox: "",
            //    URL: "",
            //    vendorPhoneNo: "",
            //    CompanyIsVendorFirmRegistered: false,
            //    VendorCompanyIsVendorFirmRegistered: false,
            //    CId: 0,
            //    VendorPOCList:[]
            //}
            //$scope.getVenderCompanyPOCList = [];
            //$scope.IsSaveVendor = false;
            //$scope.IsEditVendor = true;
            //$scope.IsManagePOC = true;
            //$scope.IsViewVendor = false;
            //getVendorCompanyDetailById(Id);

            ////getCompanyListForVenderCompanyName();

            /////////For View VendorDetail////////////////////////////////

            //////alert(Id.toString());

            ////$scope.IsSaveVendor = false;
            ////$scope.IsEditVendor = false;
            ////$scope.IsManagePOC = false;
            ////$scope.IsViewVendor = true;
            ////getVendorCompanyDetailById(Id);
            //////getCompanyListForVenderCompanyName();
          
        }
        $scope.ChangeableVendorDetail = function (Id) {
           

            $scope.IsSaveVendor = false;
            $scope.IsEditVendor = true;
            $scope.ManagePOC = true;
            $scope.IsViewVendor = false;
        }
        $scope.editRoleTags = function () {
            debugger;
            multiSelectCurrentData = $scope.Tags.Companies;
            for (var i = 0; i < multiSelectCurrentData.length; i++) {
                multiSelectPreviousData.push(multiSelectCurrentData[i]);
            }

            $scope.openSaveCompanyTags = true;
            $timeout(function () {
                $('#CompanyTags  .host .tags .input').focus();
            }, 100);
        }
        /*Get all roles of a signed in company i.e where type in (1,3) means its role or both */
        function getCompanyListForVenderCompanyName(val) {
            debugger;
            //alert(val);
            $scope.vendorInvitationModal = {

                firstName: "",
                lastName: "",
                companyName: "",
                emaiAddress: "",
                messageTextBox: "",
                URL: "",
                vendorPhoneNo: "",
                CompanyIsVendorFirmRegistered: false,
                VendorCompanyIsVendorFirmRegistered: false,
                CId: 0,
                VendorPOCList:[]
            }
            if (val != undefined && val != null && val != '') {
                vendorFactory.GetCompaniesForVenderCompanyName()
                    .success(function (data) {
                        $scope.getVenderCompanyList = data.ResponseData;
                    })
            }
        }

        function getVendorCompanyDetailById(val) {
            //debugger;
            //alert(val);
            if (val != undefined && val != null && val != '') {
                vendorFactory.getSelectedVendorDetailById(val)
                    .success(function (data) {
                        debugger;
                        if (data.ResponseData != null) {
                            $scope.vendorInvitationModal = {

                                firstName: "",
                                lastName: "",
                                companyName: "",
                                emaiAddress: "",
                                messageTextBox: "",
                                URL:"",
                                vendorPhoneNo: "",
                                CompanyIsVendorFirmRegistered: false,
                                VendorCompanyIsVendorFirmRegistered: false,
                                CId: 0,
                                VendorPOCList:[]
                            }
                            $scope.vendorInvitationModal.CId = val;
                            $scope.vendorInvitationModal.companyName = data.ResponseData.CompanyName;
                            $scope.vendorInvitationModal.firstName = data.ResponseData.ContactFirstName;
                            $scope.vendorInvitationModal.lastName = data.ResponseData.ContactLastName;
                            $scope.vendorInvitationModal.emaiAddress = data.ResponseData.CompanyEmail;
                            $scope.vendorInvitationModal.URL = data.ResponseData.CompanyWebsite;
                            $scope.vendorInvitationModal.vendorPhoneNo = data.ResponseData.CompanyPhoneNo;
                            $scope.vendorInvitationModal.VendorPOCList = data.ResponseData.VendorPOCList;
                            $scope.vendorInvitationModal.CompanyIsVendorFirmRegistered = data.ResponseData.CompanyIsVendorFirmRegistered;
                            $scope.vendorInvitationModal.VendorCompanyIsVendorFirmRegistered = data.ResponseData.VendorCompanyIsVendorFirmRegistered;
                           // alert($scope.vendorInvitationModal.VendorCompanyIsVendorFirmRegistered);
                           // $scope.IsManagePOC = !data.ResponseData.VendorCompanyIsVendorFirmRegistered;
                            $scope.getVenderCompanyPOCList = $scope.vendorInvitationModal.VendorPOCList;
                            $('#modalAddNewvendors').modal('show');
                        }
                       // $scope.getVenderCompanyList = data.ResponseData;
                    })
            }
        }

        function getProjectAndVendorDetail(ProjectId) {

            vendorFactory.getProjectAndVendorDetail(ProjectId, CompanyId)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.ProjectAndVendorDetailData = data.ResponseData;
                    }
                    $('#globalLoader').hide();

                }), Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })

        }
        ///////////////////////////////////////GetProjectToQuote for vendor Profile p ////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////Filtering////////////////////////////////////////////////////////////////////////////////////////////
        $scope.vendorDropModal = { CompanyId: 0, VendorId: 0, Id: 0, VendorCompanyId: 0, Name: "--Select All--" }

        function getCompanyVendorDrop(CompanyId) {
            debugger;
            $('#modalConfirmationCompleteProject').modal('show')
            vendorFactory.getCompanyVendorDropList(CompanyId)
                .success(function (data) {
                    if (data.success) {
                        debugger;
                        $scope.myVendorDropList = data.ResponseData;
                        $scope.myVendorDropList.push($scope.vendorDropModal);
                        getMyVendor();
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                , Error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                    $('#modalConfirmationCompleteProject').modal('hide');
                })

        }


        $scope.vendorInvitationModal = {

            firstName: "",
            lastName: "",
            companyName: "",
            emaiAddress: "",
            messageTextBox: "",
            URL:"",
            vendorPhoneNo: "",
            CompanyIsVendorFirmRegistered: false,
            VendorCompanyIsVendorFirmRegistered:false,
            CId:0,
            VendorPOCList:[]
        }
        $scope.onTagAdded = function (tag, limit) {
            debugger;

            if ($scope.Tags.Companies.length = limit + 1) {
                $scope.Tags.Companies.pop();
            }
            else {

                //alert($scope.Tags.Companies[0].CompanyId);
            }
        }
        $scope.inviteVendor = function (isVendorFirstName, IsVendorCompanyNameInvalid,IsVenderCompanyTagInValid, IsVendorEmailId,IsVendorURL,IsVendorPhoneNo, NewVendorForm, vendorInvitationModal) {

            debugger;
            if (isVendorFirstName || IsVendorCompanyNameInvalid || IsVendorEmailId || IsVenderCompanyTagInValid || IsVendorURL || IsVendorPhoneNo) {

                NewVendorForm.VendorFirstName.$dirty = true
                    NewVendorForm.companyName.$dirty = true
               
                    NewVendorForm.VendorEmaiAddress.$dirty = true
                    NewVendorForm.VendorPhoneNo.$dirty = true
                    NewVendorForm.VendorURL.$dirty = true
                return;

            }

            //vendorInvitationModal.companyName = sharedService.getShared().CompanyName;
            //if ($scope.Tags.Companies.length>0) {
               // vendorInvitationModal.companyName = $scope.Tags.Companies[0].CompanyName

                var data = {}
                data.FirstName = vendorInvitationModal.firstName
                data.LastName = vendorInvitationModal.lastName
                data.CompanyName = vendorInvitationModal.companyName
                data.EmaiAddress = vendorInvitationModal.emaiAddress
                data.PhoneNo = vendorInvitationModal.vendorPhoneNo
                data.Website = vendorInvitationModal.URL
                data.MessageTextBox = vendorInvitationModal.messageTextBox
                data.UserId = sharedService.getUserId()
                data.CompanyId = sharedService.getCompanyId();
                data.CId = sharedService.getCompanyId();
                $('#globalLoader').show();
                vendorFactory.inviteVendor(data)
                    .success(function (data) {
                        debugger;
                        $('#globalLoader').hide();
                        debugger;
                        if (data.ResponseData != null) {
                            if (data.ResponseData.Id == -1) {

                                toaster.warning("Warning", message.InvitationAlreadySend);
                                NewVendorForm.VendorFirstName.$dirty = false;
                                NewVendorForm.companyName.$dirty = false;
                                NewVendorForm.VendorEmaiAddress.$dirty = false;
                                NewVendorForm.VendorEmaiAddress = false;
                                NewVendorForm.VendorPhoneNo.$dirty = false;
                                //NewVendorForm.VendorWebsite.$dirty = false; 
                                NewVendorForm.VendorLastName.$dirty = false;
                                NewVendorForm.VendorURL.$dirty = false;
                            }
                            else if (data.ResponseData.Id == -2) {

                                toaster.warning("Warning", message.DomainError);
                                NewVendorForm.VendorFirstName.$dirty = false;
                                NewVendorForm.companyName.$dirty = false;
                                NewVendorForm.VendorEmaiAddress.$dirty = false;
                                NewVendorForm.VendorEmaiAddress = false;
                                NewVendorForm.VendorPhoneNo.$dirty = false;
                                //NewVendorForm.VendorWebsite.$dirty = false;
                                NewVendorForm.VendorLastName.$dirty = false;
                                NewVendorForm.VendorURL.$dirty = false;
                            }
                            else {
                                toaster.success("Success", message.InvitationToVendor);
                                
                            }
                        }
                        else {
                            toaster.error("Error", "Some Error Occured !");
                        }
                        NewVendorForm.VendorFirstName.$dirty = false;
                        NewVendorForm.VendorLastName.$dirty = false;
                                NewVendorForm.companyName.$dirty = false;
                                NewVendorForm.VendorEmaiAddress.$dirty = false;
                                NewVendorForm.VendorEmaiAddress = false;
                                NewVendorForm.VendorPhoneNo.$dirty = false;
                                //NewVendorForm.VendorWebsite.$dirty = false;
                                NewVendorForm.VendorURL.$dirty = false;
                        $scope.vendorInvitationModal = {}
                        $('#modalAddNewvendors').modal('hide');
                        getMyVendor()//after Inviting show vendorlist
                    }),
                    Error(function (data, status, headers, config) {
                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();
                    })
            //} else {
            //    toaster.error("Error", "Some Error Occured !");
            //    $('#globalLoader').hide();
            //}


        }


        function getMyVendor() {
            debugger

            dataModal.CompanyId = CompanyId;
            $('#globalLoader').show();
            vendorFactory.getMyVendor(dataModal)
                .success(function (data) {
                    debugger;
                    if (data.ResponseData != null) {
                        $scope.myEnterpriseList = data.ResponseData;
                    }
                    $('#globalLoader').hide();

                }),
                Error(function (data, status, headers, config) {
                    debugger;
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })

        }


        $scope.FilterByVendorCompany = function (item) {
            dataModal.VendorCompanyId = item.VendorCompanyId;
            getMyVendor();

        }

        $scope.myVendorstatusList = [
            { IsApproved: null, status: '---Select All---', Id: 0 },
            { IsApproved: true, status: 'Accepted', Id: 1 },
            { IsApproved: false, status: 'Rejected', Id: 2 },
            { IsApproved: null, status: 'Waiting for approval', Id: 3 },

        ]

        $scope.myvendorstatusModal = $scope.myVendorstatusList[0]

        $scope.FilterVendorByStatus = function (item) {
            debugger;

            dataModal.Id = item.Id;
            dataModal.IsApproved = item.IsApproved;
            getMyVendor();

        }


        //////////////////////////////////////////--autoCompleteOptions---////////////////////////////////////////////////////
        var autoModal = {}
        $scope.autoCompleteOptions = {

            minimumChars: 3,
            dropdownWidth: '500px',
            //dropdownHeight: '200px',
            //data: function (term) {
            //    return $http.post(baseurl + 'VendorApi/GetVendorName/',
            //        autoModal = { CompanyId: CompanyId, Name: term })
            //        .then(function (data) {
            //            debugger;
            //            return data.data.ResponseData;
            //        });
            //},
            data: function (term) {
                return $http.post(baseurl + 'VendorApi/GetVendorCompanyName/',
                    autoModal = { CompanyId: CompanyId, Name: term })
                    .then(function (data) {
                        debugger;
                        return data.data.ResponseData;
                    });
            },
            renderItem: function (item) {
                return {
                    value: item.Name,
                    label: $sce.trustAsHtml(
                        "<p class='auto-complete'>"
                        + item.Name +
                        "</p>")

                };

            },
            itemSelected: function (item) {
                debugger;
                dataModal.VendorId = item.item.VendorId;
                $scope.searchVendorName = item.item.Name;
                getMyVendor();
            }
        }


        $scope.vendorworkList = [];
        $scope.openVendorViewDetail = function (VendorId) {
            debugger;
            if (VendorId == 0) {

                $scope.vendorworkList = [];


                $('#modalVendorViewDetail').modal('show');
            }
            else {

                getVendorDetail(VendorId);
            }
        }

        function getVendorDetail(VendorId) {
            $('#globalLoader').show();

            vendorFactory.getVendorDetail(CompanyId, VendorId)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.vendorworkList = data.ResponseData;
                        $('#modalVendorViewDetail').modal('show');
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();
                    }

                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })
        }

        var isCtrlPressed;
        var isAPressed;
        /////////////////////////////empty text box project to quote companyProfile//////////////////////////////////////////////////////
        $scope.searchVendorNameInvitaion = function (searchVendorName, event) {
            debugger;
            if (searchVendorName != undefined && searchVendorName.length <= 1 && event.keyCode == 8) {
                dataModal.VendorId = 0
                dataModal.Name = "";
                getMyVendor();
            }
            // if key is pressed
            if (event.keyCode == 17 || isCtrlPressed) {
                isCtrlPressed = true;
            }
            else {
                isCtrlPressed = false;
            }

            // other key after the control
            if (isCtrlPressed) {
                if (event.keyCode == 97 || event.keyCode == 65) {
                    isAPressed = true;

                }
            }
            else {
                isAPressed = false;
            }


            if (isCtrlPressed && isAPressed) {
                if (event.keyCode == 46 || event.keyCode == 8) {
                    dataModal.VendorId = 0
                    dataModal.Name = "";
                    getMyVendor();
                    isCtrlPressed = false;
                    isAPressed = false;
                }

            }
            var text = window.getSelection().toString()
            if (text != "" && (event.keyCode == 8 || event.keyCode == 46)) {
                dataModal.VendorId = 0
                dataModal.Name = "";
                getMyVendor();
            }
        }


        $scope.cutSearchInvitaionByVendorName = function () {
            dataModal.VendorId = 0
            dataModal.Name = "";
            getMyVendor();

        }

        ////////////////////////////////////------------paging code/////////////////////////////////
        $scope.pagesizeList = [
            { PageSize: 5 },
            { PageSize: 10 },
            { PageSize: 25 },
            { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];

        $scope.changePageSize = function (pageSizeSelected) {
            debugger;
            dataModal.PageSize = pageSizeSelected;
            getInvitationTovendor(dataModal)
        }

        $scope.pageChanged = function (pageIndex) {
            debugger;
            dataModal.PageIndex = pageIndex;
            getInvitationTovendor(dataModal)
        }


        ////////////////////////////////////------------paging code/////////////////////////////////


        /*New functionality to invite users*/

        $scope.userdata = {};
        $scope.jobProfileModal = {};
        $scope.Tags = {};
        $scope.Tags.Roles = [];

        /*Open pop up to add new user and send invitation*/
        $scope.modalAddNewUsers = function () {
            //alert("Hello");
            $scope.companyDomain = "";
            $scope.userInvitationModal = {}
            $scope.userdata = {};
            $scope.Tags = {};
            $scope.Tags.Roles = [];
            getusersRole();
            getAllDesignation();
            $scope.GetCompanyEmailForInviteNewUser(sharedService.getShared().CompanyName);
            $("input[name='UserFirstName']").removeClass("inputError");
            $("input[name='UserLastName']").removeClass("inputError");
            $("input[name='UserEmailAddress']").removeClass("inputError");
            $("textarea[name='UserMessageTextBox']").removeClass("inputError");
            //$("select[name='jobProfile']").removeClass("inputError");
            //$("input[name='txtRole']").removeClass('inputError');

            multiSelectPreviousData = [];
            multiSelectCurrentData = [];

            $('#modalAddNewUsers').modal('show');
        }

        /*Code to get domain of company i.e. string after @ in companyemail by company name*/
        $scope.companyDomain = "";
        $scope.GetCompanyEmailForInviteNewUser = function (CompanyName) {
            $('#globalLoader').show();
            vendorFactory.GetCompanyEmailForInviteNewUser(CompanyName)
                .success(function (data) {
                    if (data.success && data.ResponseData != null) {
                        $scope.companyDomain = '@' + data.ResponseData.split('@')[1];
                    }
                    else {
                        $scope.companyDomain = '';

                    }
                    $('#globalLoader').hide();

                })
                ,
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })

        };

         /*Get all roles of a signed in company i.e where type in (1,3) means its role or both */
        function getusersRole() {
            vendorFactory.GetNewUserRole(sharedService.getCompanyId())
                .success(function (data) {
                    $scope.getUserRoleList = data.ResponseData;
                })
        }

        /*Get all roles of a signed in company i.e where type in (1,3) means its role or both */
        function getusersRole() {
            vendorFactory.GetNewUserRole(sharedService.getCompanyId())
                .success(function (data) {
                    $scope.getUserRoleList = data.ResponseData;
                })
        }
       
        /*Code to add user,send invitation and save info*/
        $scope.inviteUser = function (isUserFirstName, IsUserCompanyNameInvalid, IsUserEmailId, NewUserForm, userInvitationModal) {

            debugger;
            if (isUserFirstName || IsUserCompanyNameInvalid || IsUserEmailId) {

                NewUserForm.UserFirstName.$dirty = true
                NewUserForm.companyName.$dirty = true
                NewUserForm.UserEmailAddress.$dirty = true
                return;

            }
            $scope.userdata.FirstName = userInvitationModal.firstName;
            $scope.userdata.LastName = userInvitationModal.lastName;
            $scope.userdata.CompanyName = sharedService.getShared().CompanyName;
            $scope.userdata.Email = userInvitationModal.emailAddress + $scope.companyDomain;
            $scope.userdata.MessageTextBox = userInvitationModal.messageTextBox;
            $scope.userdata.UserId = sharedService.getUserId();
            $scope.userdata.CompanyId = sharedService.getCompanyId();
            $scope.userdata.UserRoleList = $scope.Tags.Roles;
            $scope.userdata.JobProfileId = $scope.jobProfileModal.RoleId;
            $scope.userdata.JobProfileName = $scope.jobProfileModal.Name;
            //$("select[name='jobProfile']").removeClass("inputError");

            $('#globalLoader').show();
            vendorFactory.inviteUser($scope.userdata)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    debugger;
                    if (data.ResponseData != null) {
                        if (data.ResponseData.Id == -1) {

                            toaster.warning("Warning", message.InvitationError)
                            NewUserForm.UserFirstName.$dirty = false;
                            
                            NewUserForm.UserEmailAddress.$dirty = false;
                            NewUserForm.UserEmailAddress = false;
                            NewUserForm.UserMessageTextBox = false;
                            NewUserForm.UserLastName = false;
                            NewUserForm.companyName.$dirty = false;
                        }
                        else {
                            toaster.success("Success", message.InvitationToUser);
                        }
                    }
                    else {
                        toaster.error("Error", "Some Error Occured !");
                    }
                    $scope.userInvitationModal = {}
                    $scope.roleModal = {};
                    $scope.userdata = {};
                    $('#modalAddNewUsers').modal('hide');
                    getMyVendor()    //after Inviting show vendorlist using old method to bind vendorlist no changes
                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })



        }

        /*Code to not allow user to  enter @ in Email address as it will be by default set to companies domain*/
        $scope.emailkeyUp = function () {
            var email = $scope.vendorInvitationModal.emaiAddress;
            //if (email != undefined && email != null) {
            //    if (email.indexOf(' ') >= 0) {
            //        toaster.error("Space not allowed");
            //        $scope.userInvitationModal.emailAddress = '';
            //    }
            //    if (email.indexOf("@") > -1) {
            //        toaster.error("Enter username only");
            //        $scope.userInvitationModal.emailAddress = '';
            //    }
            //}
            if (email == '') {
                $('#UserEmailAddress').addClass('inputError');
            }
            else {
                $('#UserEmailAddress').removeClass('inputError');
            }
        };

        /*Get All Designation or Job Profiles  i.e where type in (2,3) means its job profile or both*/
        function getAllDesignation() {
            $('#globalLoader').show();
            teamFactory.getAllDesignation()
                .success(function (data) {
                    $scope.getAllDesignationList = data.ResponseData;
                    $('#globalLoader').hide();

                })
        }

        $scope.openSaveCompanyTags = true;
        var multiSelectPreviousData = [];
        var multiSelectCurrentData = [];




        //////////////////////////////////////////--autoCompleteCompanyOptions---////////////////////////////////////////////////////
        var autoCompanyModal = {}
        $scope.autoCompleteCompanyOptions = {

            minimumChars: 3,
           
            
            data: function (term) {
                //alert(term);
                debugger;
                if (term != '') {
                    return $http.get(baseurl + 'VendorApi/GetCompaniesForVenderCompanyName/' + term
                        //autoCompanyModal = {  Name: term }
                        )
                        .then(function (data) {
                            debugger;
                            return data.data.ResponseData;
                        });
                }
            },
            renderItem: function (item) {
                return {
                    value: item.CompanyName,
                    label: $sce.trustAsHtml(
                        "<p class='auto-complete'>"
                        + item.CompanyName +
                        "</p>")

                };

            },
            itemSelected: function (item) {
                debugger;
                dataCompanyModal.CompanyId = item.item.CompanyId;
                $scope.CName = item.item.CompanyName;
                getMyVendor();
                getSelectedVenderDetail(item.item.CompanyId);
            }

        }
        function getSelectedVenderDetail(VendorCompanyId) {
            vendorFactory.getSelectedVendorDetail(VendorCompanyId)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    debugger;
                    if (data.ResponseData != null && data.ResponseData.CompanyIsVendorFirmRegistered) {
                        
                        $scope.vendorInvitationModal.URL = data.ResponseData.CompanyWebsite;
                        $scope.vendorInvitationModal.emaiAddress = data.ResponseData.CompanyEmail;
                        $scope.vendorInvitationModal.vendorPhoneNo = data.ResponseData.CompanyPhoneNo;
                        
                    }
                    else {
                        $scope.vendorInvitationModal.URL = "";
                        $scope.vendorInvitationModal.emaiAddress = "";
                        $scope.vendorInvitationModal.vendorPhoneNo = "";
                        $scope.vendorInvitationModal.VendorPOCList = data.ResponseData.VendorPOCList;
                        for (var i = 0; i < $scope.vendorInvitationModal.VendorPOCList.length; i++) {
                            var temp = {};
                            temp.pocFirstName = $scope.vendorInvitationModal.VendorPOCList
                        }
                    }
                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })
        }
        $scope.deleteVendorDetail = function (Id) {
            debugger;
            //alert($scope.vendorInvitationModal.CId);
            Id = $scope.vendorInvitationModal.CId;
            $confirm({ text: 'Are you sure you want to delete this Vendor?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
           
              .then(function () {
                  vendorFactory.deleteVendorDetail(Id)
                      .success(function (data) {
                          debugger;
                          if (data.success) {
                              toaster.success("Success", message[data.message]);
                              $('#globalLoader').hide();
                              $('#modalAddNewvendors').modal('hide');
                              getMyVendor();
                          } else {
                              if (data.message == "ProjectPresentInVendorDetail") {
                                  toaster.warning("Warning", message[data.message]);
                                  $('#globalLoader').hide();
                              }
                              else {
                                  toaster.error("Error", data.message);
                                  $('#globalLoader').hide();
                              }
                          }

                      }),
                      Error(function (data, status, headers, config) {
                          toaster.error("Error", "Some Error Occured !");
                          $('#globalLoader').hide();
                      })
              })
            }


        $scope.updateVendor = function (isVendorFirstName, IsVendorCompanyNameInvalid, IsVenderCompanyTagInValid, IsVendorEmailId, IsVendorURL, IsVendorPhoneNo, NewVendorForm, vendorInvitationModal) {

            debugger;
            if (isVendorFirstName || IsVendorCompanyNameInvalid || IsVendorEmailId || IsVenderCompanyTagInValid || IsVendorURL || IsVendorPhoneNo) {

                NewVendorForm.VendorFirstName.$dirty = true
                NewVendorForm.companyName.$dirty = true

                NewVendorForm.VendorEmaiAddress.$dirty = true
                NewVendorForm.VendorPhoneNo.$dirty = true
                NewVendorForm.VendorURL.$dirty = true
                return;

            }
            for (var i = 0; i < $scope.getVenderCompanyPOCList.length; i++) {
                var pocEmail = $scope.getVenderCompanyPOCList[i].pocEmail;
                if ($scope.vendorInvitationModal.URL != pocEmail.substr(pocEmail.indexOf('@') + 1)) {
                    toaster.warning("Warning", message.NotValidDomainEmail);
                    $('#globalLoader').hide();
                    return;
                }
            }
            //vendorInvitationModal.companyName = sharedService.getShared().CompanyName;
            //if ($scope.Tags.Companies.length>0) {
            // vendorInvitationModal.companyName = $scope.Tags.Companies[0].CompanyName

            var data = {}
         
            data.FirstName = vendorInvitationModal.firstName
            data.LastName = vendorInvitationModal.lastName
            data.CompanyName = vendorInvitationModal.companyName
            data.EmaiAddress = vendorInvitationModal.emaiAddress
            data.PhoneNo = vendorInvitationModal.vendorPhoneNo
            data.Website = vendorInvitationModal.URL
            data.VendorPOCList = $scope.getVenderCompanyPOCList;
            data.DeletedVendorPOCList = $scope.DeletedVenderCompanyPOCList;
            //data.MessageTextBox = vendorInvitationModal.messageTextBox
            //data.UserId = sharedService.getUserId()
            data.CompanyId = sharedService.getCompanyId();
            data.CId = vendorInvitationModal.CId;
            $('#globalLoader').show();
            vendorFactory.updateVendor(data)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    debugger;
                    if (data.ResponseData != null) {
                        if (data.ResponseData.Id == -1) {

                            toaster.warning("Warning", message.UpdateError);
                            NewVendorForm.VendorFirstName.$dirty = false;
                            NewVendorForm.companyName.$dirty = false;
                            NewVendorForm.VendorEmaiAddress.$dirty = false;
                            NewVendorForm.VendorEmaiAddress = false;
                            NewVendorForm.VendorPhoneNo.$dirty = false;
                            NewVendorForm.VendorLastName.$dirty = false;
                            NewVendorForm.VendorURL.$dirty = false;
                        }
                        else {
                            toaster.success("Success", message.UpdateToVendor);
                            $scope.getVenderCompanyPOCList = [];
                        }
                    }
                    else {
                        toaster.error("Error", "Some Error Occured !");
                    }
                    NewVendorForm.VendorFirstName.$dirty = false;
                    NewVendorForm.VendorLastName.$dirty = false;
                    NewVendorForm.companyName.$dirty = false;
                    NewVendorForm.VendorEmaiAddress.$dirty = false;
                    NewVendorForm.VendorEmaiAddress = false;
                    NewVendorForm.VendorPhoneNo.$dirty = false;
                    NewVendorForm.VendorURL.$dirty = false;
                    $scope.vendorInvitationModal = {}
                    $('#modalAddNewvendors').modal('hide');
                    getMyVendor()//after Inviting show vendorlist
                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })
            //} else {
            //    toaster.error("Error", "Some Error Occured !");
            //    $('#globalLoader').hide();
            //}


        }

        /*       Manage POC        */

       

        $scope.AddPOCToList = function (IsPOCFirstName, IsPOCLastName, IsPOCEmail, NewVendorForm) {
            debugger;
            //alert(val);
            $('#globalLoader').show();
            if (IsPOCFirstName || IsPOCLastName || IsPOCEmail) {

                NewVendorForm.POCFirstName.$dirty = true;
                NewVendorForm.POCLastName.$dirty = true;

                NewVendorForm.POCEmail.$dirty = true;
                $('#globalLoader').hide();
                return;

            }
            var pocEmail = $scope.vendorInvitationModal.pocEmail;
            if ($scope.vendorInvitationModal.URL != pocEmail.substr(pocEmail.indexOf('@') + 1)) {
                toaster.warning("Warning", message.NotValidDomainEmail);
                $('#globalLoader').hide();
                return;
            }
            for (var i = 0; i < $scope.getVenderCompanyPOCList.length; i++) {
                
                if ($scope.getVenderCompanyPOCList[i].pocEmail == $scope.vendorInvitationModal.pocEmail) {
                    toaster.warning("Warning", message.AlreadyExistPOC);
                    $('#globalLoader').hide();
                    return;
                }
            }
            vendorFactory.ValidationEmailForPOC($scope.vendorInvitationModal.pocEmail, CompanyId)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    debugger;
                    if (data.ResponseData != null) {
                        if (data.ResponseData == false) {

                            toaster.warning("Warning", message.AlreadyExistPOC);
                            
                            NewVendorForm.POCFirstName.$dirty = false;
                            NewVendorForm.POCLastName.$dirty = false;

                            NewVendorForm.POCEmail.$dirty = false;
                        }
                        else {
                            var tempPOC = {};

                            tempPOC.pocFirstName = $scope.vendorInvitationModal.pocFirstName;
                            tempPOC.pocLastName = $scope.vendorInvitationModal.pocLastName;
                            tempPOC.pocEmail = $scope.vendorInvitationModal.pocEmail;
                            $scope.getVenderCompanyPOCList.push(tempPOC);
                            $scope.vendorInvitationModal.pocFirstName = "";
                            $scope.vendorInvitationModal.pocLastName = "";
                            $scope.vendorInvitationModal.pocEmail = "";
                            NewVendorForm.POCFirstName.$dirty = false;
                            NewVendorForm.POCLastName.$dirty = false;

                            NewVendorForm.POCEmail.$dirty = false;
                        }
                    }
                    else {
                        toaster.error("Error", "Some Error Occured !");
                    }
                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })

            
        }

        $scope.deleteVendorPOCDetail = function (index) {  //pocFirstName,pocLastName,pocEmail
            //debugger;
           // alert(index);
            $confirm({ text: 'Are you sure you want to delete this POC?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
           
             .then(function () {
                 $scope.DeletedVenderCompanyPOCList.push($scope.getVenderCompanyPOCList[index]);
                 $scope.getVenderCompanyPOCList.splice(index, 1);
                 //for (var i = 0; i < $scope.getVenderCompanyPOCList.length; i++) {
                 //    if($scope.getVenderCompanyPOCList[i].pocFirstName==pocFirstName&&$scope.getVenderCompanyPOCList[i].pocLastName==pocLastName
                 //        &&$scope.getVenderCompanyPOCList[i].pocEmail==pocEmail){

                 //    }
                 //    multiSelectPreviousData.push(multiSelectCurrentData[i]);
                 //    //checkTagrol = true;
             
                 //}
             });
        }

        $scope.openPOCViewDetail = function (Id) {
            debugger;
            //alert(Id.toString());
            //$scope.Tags = {};
            //$scope.Tags.Companies = [];
            $scope.vendorInvitationModal = {

                firstName: "",
                lastName: "",
                companyName: "",
                emaiAddress: "",
                messageTextBox: "",
                URL: "",
                vendorPhoneNo: "",
                CompanyIsVendorFirmRegistered: false,
                VendorCompanyIsVendorFirmRegistered: false,
                CId: 0,
                VendorPOCList: []
            }
            $scope.getVenderCompanyPOCList = [];
            $scope.IsSaveVendor = false;
            $scope.IsEditVendor = false;
            $scope.IsManagePOC = true;
            $scope.IsViewVendor = false;
            getVendorCompanyDetailById(Id);

            //getCompanyListForVenderCompanyName();
        }

        //var checkTagrol = false;
        /*  This function is used to edit Role Tags  */
        //$scope.editRoleTags = function () {
        //    debugger;
        //    multiSelectCurrentData = $scope.Tags.Roles;
        //    for (var i = 0; i < multiSelectCurrentData.length; i++) {
        //        multiSelectPreviousData.push(multiSelectCurrentData[i]);
        //        //checkTagrol = true;
             
        //    }

        //    $scope.openSaveRoleTags = true;
        //    $timeout(function () {
        //        $('#RoleTags  .host .tags .input').focus();
        //    }, 100);

           
        //}
    })


});

