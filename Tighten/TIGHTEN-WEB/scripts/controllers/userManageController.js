﻿define(['app', 'usersFactory', 'roleFactory'], function (app, usersFactory, roleFactory) {
    app.controller("userManageController", function ($scope, $rootScope, apiURL, $timeout,$location, $http, sharedService, toaster, usersFactory, roleFactory, $sce, $window, $filter, $confirm, FreelancerInvitationFactory
        ) {

        window.Selectize = require('selectize');
        $scope.imgURL = apiURL.imageAddress;
        $scope.FreelancerModel = {};
        //$scope.isEmailOrHandleActive = true;
        $scope.isFreelancerAllreadyAttachedToCompany = false;

        /* Showing User Management Section to see the information*/
        $scope.showUserManagementSection = function () {
            //closeCentreSections();
            $scope.showCenterLoader = true;
            //$scope.UsersObject = {};
            $scope.inputSearchUser = "";
            $scope.selectedProject = 0;
            $scope.selectedTodo = 0;
            $scope.selectedNavigation = "UserManagementSection";
            /* Getting Users Details */
            debugger;
            usersFactory.getUsers(sharedService.getUserId(), false, sharedService.getCompanyId())              // here false is used for chkEmailConfirmed
            
                .success(function (data) {
                    if (data.success) {
                        $scope.UsersObject = data.ResponseData;
                        $scope.filteredFreelancersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: true }, true);
                        $scope.filteredUsersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: false }, true);

                        $timeout(function () {
                            $scope.$apply();
                            roleFactory.getRoles(sharedService.getCompanyId(),sharedService.getRoleId())
                            .success(function (data) {
                                if (data.success) {
                                    $scope.RoleObject = data.ResponseData;
                                    //$scope.RoleObjectForFreelancer = $filter('filter')(data.ResponseData, [{ 'RoleId': 2 }, { 'RoleId': 3 }], true)
                                    var roleObjectForFreelancer = [];
                                    for (var i = 0; i < $scope.RoleObject.length; i++) {
                                        if ($scope.RoleObject[i].RoleId != 1 && $scope.RoleObject[i].RoleId != 5) {
                                            roleObjectForFreelancer.push($scope.RoleObject[i]);
                                        }

                                    }
                                    $scope.RoleObjectForFreelancer = roleObjectForFreelancer;

                                        var objForAllProjectsOfCompany = {CompanyId:sharedService.getCompanyId(),UserId: sharedService.getUserId(),EmailOrHandle: "", chkInvitationType: ""};
                                        usersFactory.GetAllProjectsOfCompany(objForAllProjectsOfCompany)
                                            .success(function (response) {
                                                $scope.ProjectsObject = response.ResponseData;
                                                bindProjectDdl();
                                            })
                                            .error(function (response) {
                                                toaster.error(message[response.message]);
                                            })
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some error occured");
                            });
                        });
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some error occured");
                });
        };

        /* SHowing Add New User Modal Pop Up*/
        $scope.showAddNewUserModal = function () {
            debugger;
            $('#globalLoader').show();
            $scope.userModel = {};
            $scope.userModel.email = "";
            $scope.isEmailDirty = false;
            $scope.isFirstNameDirty = false;
            $scope.isLastNameDirty = false;
            $scope.isUserRoleDirty = false;
            $scope.FreelancerModel = {};
            $scope.confirmFreelancerObject = {};
            $scope.confirmFreelancerObject.isEmailOrHandleActive = true;
            bindProjectDdl();

            FreelancerInvitationFactory.getAllFreelancer()
            .success(function (response) {
                $scope.allFreelancerObject = response.ResponseData;
                $('#globalLoader').hide();
            })
            .error(function (response) {
                toaster.error(message[response]);
                $('#globalLoader').hide();
            })

            $('#modalAddNewUser').modal('show');
        };


        function bindProjectDdl() {


            if ($scope.ProjectsObject != undefined) {
                var projects = $scope.ProjectsObject;
                var allProjects = [], i = 0;
                for (i = 0; i < projects.length; i++) {
                    var jsonItem = {};
                    jsonItem.Value = projects[i].ProjectId;
                    jsonItem.Text = projects[i].ProjectName;
                    jsonItem.ticked = false;
                    if (projects[i].isTicked > 0) {
                        jsonItem.false = true;
                    }
                    else {
                        jsonItem.false = false;
                    }

                    allProjects.push(jsonItem);
                }
                $scope.newModel = allProjects;
                $scope.output = [];
            }
        };

        /* Showing Edit User ROle Modal Pop Up*/
        $scope.showModalUserRoleEdit = function (userObject) {
            
            $scope.UserRoleEditObject = {};
            $scope.UserRoleEditObject.FirstName = userObject.FirstName;
            $scope.UserRoleEditObject.LastName = userObject.LastName;
            $scope.UserRoleEditObject.Email = userObject.Email;
            $scope.UserRoleEditObject.UserId = userObject.UserId;
            $scope.UserRoleEditObject.UserRole = userObject.UserRole;
            $scope.UserRoleEditObject.RoleId = userObject.RoleId;
            $scope.UserRoleEditObject.IsFreelancer = userObject.IsFreelancer;
            $('#modalUserRoleEdit').modal('show');
        }

        /* Delete User */
        $scope.deleteUser = function (userObject) {

            $confirm({ text: 'Are you sure you want to delete this User?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
             .then(function () {

                 $('#globalLoader').show();
                 usersFactory.deleteUser(userObject.UserId)
                 .success(function (data) {
                     
                     if (data.success == false) {
                         toaster.error("Error", message[data.ResponseData]);
                     }
                     else {
                         $scope.showUserManagementSection();
                         $scope.$emit('showMenu', []);
                         toaster.success("Success", message["userDeleteSuccess"]);
                     }
                     $('#globalLoader').hide();
                 })
                 .error(function (data) {
                     toaster.error("Error", message[data.ResponseData]);
                     $('#globalLoader').hide();
                 })

             })
        }


        /* Updating User Role */
        $scope.updateUserRole = function () {
            var z = $scope.UserRoleEditObject;
            //return;
            var model = { 'UserRoleModel': $scope.UserRoleEditObject, 'UserId': sharedService.getUserId() };
            usersFactory.updateUserRole(model)
                .success(function (data) {
                    if (data.success) {
                        toaster.success("Success", data.message);

                        $('#modalUserRoleEdit').modal('hide');
                        $scope.showUserManagementSection();
                    }
                    else {
                        toaster.error("Error", data.errors.toString());
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });
        };


        /* Saving New User*/
        $scope.addNewUser = function (isEmailInvalid, isFirstNameInvalid, isLastNameInvalid, isUserRoleInvalid) {
            
            debugger;
            if (isEmailInvalid || isFirstNameInvalid || isLastNameInvalid || isUserRoleInvalid) {
                $scope.isEmailDirty = true;
                $scope.isFirstNameDirty = true;
                $scope.isLastNameDirty = true;
                $scope.isUserRoleDirty = true;
            }
            else {
                $('#globalLoader').show();
                var model = { 'NewUserModel': $scope.userModel, 'UserId': sharedService.getUserId() };
                usersFactory.insertUser(model)
                    .success(function (data) {
                        if (data.success == false) {
                            //toaster.error("Error", data.errors.toString());
                            var responseData = data.errors;
                            var errormsg = message["DuplicateUser"];
                            var errormsg = errormsg.replace("##DuplicateUserId", responseData.DuplicateUserId);
                            errormsg = errormsg.replace("##firstName", responseData.firstName).replace("##lastName", responseData.lastName).replace("##email", responseData.email);
                            errormsg = errormsg.replace("##CompanyName", responseData.CompanyName).replace("##DuplicateUserId", responseData.DuplicateUserId);

                            toaster.pop('error', "Error", errormsg, 5000, 'trustedHtml', function (toaster) {
                                return true;
                            });

                            $('#globalLoader').hide();
                        }
                        else {
                            toaster.success('Success',"New User Added Successfully");
                            $('#modalAddNewUser').modal('hide');
                            /* Getting Users Details */

                            usersFactory.getUsers(sharedService.getUserId(), false, sharedService.getCompanyId())         // here false is used for chkEmailConfirmed
                                .success(function (data) {
                                    if (data.success) {
                                        $scope.UsersObject = data.ResponseData;
                                        $scope.filteredFreelancersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: true }, true);
                                        $scope.filteredUsersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: false }, true);

                                        $('#globalLoader').hide();
                                    }
                                    else {
                                        toaster.error("Error", data.message);
                                        $('#globalLoader').hide();
                                    }
                                })
                                .error(function () {
                                    $('#globalLoader').hide();
                                    toaster.error("Error", "Some error occured");
                                });
                        }
                    })
                    .error(function (data) {
                        $('#globalLoader').hide();
                        toaster.error("Some Problem Occured");
                    });
            }
        }

        // onload
        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu', []);
            }
            $scope.CurrentUserId = sharedService.getUserId();
            $scope.showUserManagementSection();

        });


        // Checking wheather the input chaaracter is number or not 
       //$scope.isNumber = function (evnt) {
       //     if (evnt.keyCode > 47 && evnt.keyCode < 58 || evnt.charCode > 47 && evnt.charCode < 58) {
       //         return true;
       //     }
       //     else {
       //         evnt.preventDefault();
       //         return false;
       //     }
       // };

        $scope.showOtherUserProfileFromUserManage = function (freelancerObj) {
            if (freelancerObj.IsEmailConfirmed == false) {
                toaster.warning("Warning", "You cant view the profile because this freelancer is not yet member of tighten yet.");
            }
            else {
                $location.path('/user/profile/' + freelancerObj.UserId);
            }
        }

        $scope.sendInvitationToFreelancer = function (event, addNewUserForm) {
            
            $('#globalLoader').show();
            //var outputModel = $scope.newModel;
            //var i = 0;
            var AllProjectId = [];
            //for (i; i < outputModel.length; i++) {
            //    if (outputModel[i].false == true) {
            //        AllProjectId.push(outputModel[i].Value);
            //    }
            //}
            
            if ($scope.isFreelancerAllreadyAttachedToCompany == true && AllProjectId.length == 0) {
                toaster.error("Error", "Project is not selected");
                $('#globalLoader').hide();
                return;
            }

            var model = {};
            model = $scope.FreelancerModel;
            model.ProjectIdList = AllProjectId;
            model.SentBy = sharedService.getUserId();
            model.CompanyId = sharedService.getCompanyId();
            model.invitationType = $scope.chkInvitationType;
            // IsHireMe = false 
            // if its value is false then we will not save Job Title And Job Description
            model.IsHireMe = false;
            
            //var model = { 'FreelancerModel': $scope.FreelancerModel, 'SentBy': sharedService.getUserId(),'CompanyId': sharedService.geCompanyId() };
            usersFactory.insertFreelancer(model)
                .success(function (data) {
                    if (data.success == true) {
                        toaster.success("Success", message[data.message]);
                        $('#modalAddNewUser').modal('hide');

                        /* Getting Users Details */
                        usersFactory.getUsers(sharedService.getUserId(), false, sharedService.getCompanyId())         // here false is used for chkEmailConfirmed
                            .success(function (data) {
                                if (data.success) {
                                    $scope.UsersObject = data.ResponseData;
                                    $scope.filteredFreelancersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: true }, true);
                                    $scope.filteredUsersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: false }, true);

                                    $('#globalLoader').hide();
                                }
                                else {
                                    toaster.error("Error", data.message);
                                    $('#globalLoader').hide();
                                }
                            })
                            .error(function () {
                                $('#globalLoader').hide();
                                toaster.error("Error", "Some error occured");
                            });
                    }
                    else {
                        if ( data.message == "freelancerAllreadyAttachedToCompanyWithoutAnyProject" ) {
                            usersFactory.getCompanyAndFreelancerName(model.invitationType, $scope.FreelancerModel.EmailOrHandle, sharedService.getCompanyId())         // here false is used for chkEmailConfirmed
                                .success(function (dataComp) {
                                    toaster.warning("Warning", dataComp.ResponseData.FreelancerName + " is already a part of " + dataComp.ResponseData.CompanyName + " with no assigned projects. ");
                                })
                                .error(function () {
                                    toaster.error("Error", "Some error occured");
                                });
                        }
                        else {
                            toaster.warning("Warning", message[data.message]);
                        }
                        $('#globalLoader').hide();
                    }
                })
                .error(function (data) {
                    toaster.error("Error", message[data.message]);
                    $('#globalLoader').hide();
                })


        };


        //$scope.$watch('newModel', function (newValue, oldValue) {
        //    
        //    alert("watch function");
        //    // Check if value has changes
        //    if (newValue === oldValue) {
        //        return;
        //    }

        //    // Do anything you like here
        //});



        $scope.confirmEmailOrHandle = function (EmailOrHandle) {
            
            $('#globalLoader').show();
            if (EmailOrHandle == undefined || EmailOrHandle == '') {
                //toaster.error("Error" , "Please provide email or handle");
                //var objForAllProjectsOfCompany = { CompanyId: sharedService.getCompanyId(), UserId: sharedService.getUserId(), EmailOrHandle: "", chkInvitationType: "" };
                //usersFactory.GetAllProjectsOfCompany(objForAllProjectsOfCompany)
                //                    .success(function (data) {
                //                        $scope.ProjectsObject = data.ResponseData;
                //                        bindProjectDdl();
                //                    })
                //                    .error(function (data) {
                //                        toaster.error(message[data.message]);
                //                    })

                $('#globalLoader').hide();
                return;
            }
            else {
                    
                var specialCharacters = "!#$%^&*()+=-[]\\\';,/{}|\":<>?";

                for (var i = 0; i < EmailOrHandle.length; i++) {
                    if (specialCharacters.indexOf(EmailOrHandle.charAt(i)) != -1) {
                        $scope.FreelancerModel.EmailOrHandle = EmailOrHandle.replace(EmailOrHandle.charAt(i), '');
                    }
                }

            }

            if (EmailOrHandle.indexOf('@') > 0 && EmailOrHandle.indexOf('.') >= 0) {
                $scope.chkInvitationType = 'email';
            }
            else if (EmailOrHandle.indexOf('@') == 0) {
                $scope.chkInvitationType = 'handle';
                EmailOrHandle = EmailOrHandle.substring(1);
            }
            else {
                $('#globalLoader').hide();
                return;
            }

            
            var data = { 'EmailOrHandle': EmailOrHandle, 'CompanyId': sharedService.getCompanyId(), 'chkInvitationType': $scope.chkInvitationType };
            FreelancerInvitationFactory.confirmFreelancerEmailOrHandle(data)
              .success(function (response) {

                  //if (response.ResponseData != null) {
                  //    if (response.ResponseData.InvitationStatus == "Accepted") {
                  //        $scope.isFreelancerAllreadyAttachedToCompany = true;
                  //        $scope.isEmailOrHandleActive = false;
                  //    }
                  //    else {
                  //        $scope.isFreelancerAllreadyAttachedToCompany = false;
                  //        $scope.isEmailOrHandleActive = false;
                  //    }
                  //}
                  //else {
                  //    $scope.isFreelancerAllreadyAttachedToCompany = false;
                  //    $scope.isEmailOrHandleActive = true;
                  //}

                  $scope.confirmFreelancerObject = response.ResponseData;

                  //var objForAllProjectsOfCompany = { CompanyId: sharedService.getCompanyId(), UserId: sharedService.getUserId(), EmailOrHandle: EmailOrHandle, chkInvitationType: $scope.chkInvitationType };
                  //usersFactory.GetAllProjectsOfCompany(objForAllProjectsOfCompany)
                  //                 .success(function (data) {
                  //                     $scope.ProjectsObject = data.ResponseData;
                  //                     bindProjectDdl();
                  //                 })
                  //                 .error(function (data) {
                  //                     toaster.error(message[data.message]);
                  //                 })

                  $('#globalLoader').hide();
              })
              .error(function (response) {
                  toaster.error(message[response]);
                  $('#globalLoader').hide();
              })

        };





    });
});