﻿
define(['app'], function (app) {
    //registrationFactory is the factory for the registration
    app.controller('confirmEmailController', function ($scope, $rootScope, $timeout, $http, $location, confirmEmailFactory, $stateParams, toaster) {
        $rootScope.navbar = true;
        // method called on page load
        angular.element(document).ready(function () {
            
            // getting the parameters from the url
            var checkEmailConfirmation = function (userId, emailCode) {
                
                // email confirmtion service to save the user detail
                confirmEmailFactory.ConfirmEmail(userId, emailCode, function (response) {
                    //
                    //if (response.success) {
                    //$('#confirmation').dialog('open');
                    
                    if (response.success) {
                        if (redirect == 1) {
                            debugger;
                            if (response.message == "EmailConfirmed") {
                                var newLocation = '/setpassword/' + response.ResponseData.userId + "/" + response.ResponseData.EmailConfirmationCode + "/set";
                                $location.path(newLocation);
                            }
                            else {
                                $scope.message = message[response.message];
                                //toaster.warning('Sucess', 'Your account is activated now.');
                                $('.panel-body').show();
                            }
                        }
                        else {
                            $scope.message = message[response.message];
                            toaster.success('Sucess','Your account is activated now.');
                            $('.panel-body').show();
                            $location.path('/login');
                        }
                    }
                    else {
                        $scope.message = message[response.message];
                        $('.panel-body').show();
                    }
                });
            };

            

            var userId = $stateParams.userId;
            var emailCode = $stateParams.emailCode;
            var redirect = $stateParams.redirect;
            checkEmailConfirmation(userId, emailCode);
        });

    });
});