﻿define(['app', 'companiesFactory', 'teamFactory'], function (app, companiesFactory, teamFactory) {
    app.controller("companyController", function ($scope, $rootScope, $timeout, $http, $location, $stateParams, sharedService, toaster, companiesFactory, teamFactory, $upload, apiURL
        )

    {
        var baseurl = apiURL.baseAddress;
        $scope.imgURL = apiURL.imageAddress;
        //$scope.Permission = {};

        /* Showing Company profile to see the information*/
        $scope.showCompanyProfile = function (id, iseditable) {
            //closeCentreSections();
            $scope.showCenterLoader = true;
            $scope.selectedProject = 0;
            $scope.selectedTodo = 0;
            $scope.selectedNavigation = "CompanyProfile";
            $scope.otherUser = !iseditable;
            $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
            $scope.pageSizeSelected = 1; // Maximum number of items per page.
            /* Getting Company Details */
            companiesFactory.getCompanyDetails(id)
                .success(function (data) {
                    //
                    debugger;
                    if (data.success) {
                        $scope.CompanyObject = data.ResponseData[0];
                        /* Getting Company Teams with Users */
                        teamFactory.getCompanyTeams(id, sharedService.getUserId(), $scope.pageIndex, $scope.pageSizeSelected)
                            .success(function (data) {
                                debugger;
                                if (data.success) {
                                    $scope.CompanyTeamsObject = data.ResponseData;
                                    $scope.showCenterLoader = false;
                                    $('#divCompanyProfile').show();
                                }
                                else {
                                    toaster.error("Error", data.message);
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured");
                            });
                    }
                    else {
                        toaster.error("Error", "Some Error Occured");
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured");
                });
        };


        /* Opening Company Edit Modal pop up */
        $scope.editCompanyProfile = function () {
            $scope.CompanyEditObject = {};
            $scope.CompanyEditObject.CompanyId = $scope.CompanyObject.CompanyId;
            $scope.CompanyEditObject.CompanyName = $scope.CompanyObject.CompanyName;
            $scope.CompanyEditObject.CompanyEmail = $scope.CompanyObject.CompanyEmail;
            $scope.CompanyEditObject.CompanyWebsite = $scope.CompanyObject.CompanyWebsite;
            $scope.CompanyEditObject.CompanyPhoneNo = $scope.CompanyObject.CompanyPhoneNo;
            $scope.CompanyEditObject.CompanyFax = $scope.CompanyObject.CompanyFax;
            $scope.CompanyEditObject.CompanyAddress = $scope.CompanyObject.CompanyAddress;
            $scope.CompanyEditObject.CompanyDescription = $scope.CompanyObject.CompanyDescription;
            $('#modalEditCompanyProfile').modal('show');
        };

        /* Update Company Details */
        $scope.updateCompanyProfile = function (isEmailInvalid, isCompanyWebsitenvalid, editCompanyProfileForm) {
            
            if (isEmailInvalid || isCompanyWebsitenvalid) {
                editCompanyProfileForm.Email.$dirty = true;
                editCompanyProfileForm.CompanyWebsite.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                var NewCompanyEditObject = { 'Company': $scope.CompanyEditObject, 'UserId': sharedService.getUserId() }
                companiesFactory.updateCompanyDetails(NewCompanyEditObject)
                    .success(function (data) {
                        if (data.success) {
                            $scope.CompanyObject.CompanyName = $scope.CompanyEditObject.CompanyName;
                            $scope.CompanyObject.CompanyEmail = $scope.CompanyEditObject.CompanyEmail;
                            $scope.CompanyObject.CompanyWebsite = $scope.CompanyEditObject.CompanyWebsite;
                            $scope.CompanyObject.CompanyPhoneNo = $scope.CompanyEditObject.CompanyPhoneNo;
                            $scope.CompanyObject.CompanyFax = $scope.CompanyEditObject.CompanyFax;
                            $scope.CompanyObject.CompanyAddress = $scope.CompanyEditObject.CompanyAddress;
                            $scope.CompanyObject.CompanyDescription = $scope.CompanyEditObject.CompanyDescription;
                            // Updating the company prfile in the left menu
                            $scope.$emit('bindCompanyDetail', []);
                            toaster.success("Success", data.message);
                            $('#modalEditCompanyProfile').modal('hide');
                            $('#globalLoader').hide();
                        }
                        else {
                            if (data.message == undefined) {
                                toaster.error("Error", data.errors.toString());
                                $('#globalLoader').hide();
                            }
                            else {
                                toaster.error("Error", data.message);
                                $('#globalLoader').hide();
                            }
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured");
                        $('#globalLoader').hide();
                    });
            }
        };

        /* Updating Company Logo*/
        $scope.updateCompanyLogo = function ($files) {
            $('#internalLoaderForImage').show();
            $scope.showCompanyLogoLoader = true;
            //$files: an array of files selected, each file has name, size, and type.
            for (var i = 0; i < $files.length; i++) {
                var $file = $files[i];
                (function (index) {

                    //$scope.upload[index] = 
                         $upload.upload({
                        url: baseurl + "CompaniesAPI/UploadLogo", // webapi url
                        method: "POST",
                        data: { UploadedProfile: $file, Companyid: sharedService.getCompanyId(), UserId: sharedService.getUserId()},
                        file: $file
                    })
                        .progress(function (evt) {
                            // get upload percentage
                            $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                        })
                        .success(function (data) {
                            // file is uploaded successfully
                            if (data.success) {
                                angular.forEach(
                                 angular.element("input[type='file']"),
                                         function (inputElem) {
                                             angular.element(inputElem).val(null);
                                         });
                                $timeout(function () {
                                    $scope.$apply();
                                    companiesFactory.getCompanyDetails(sharedService.getCompanyId())
                                        .success(function (companydata) {
                                            if (companydata.success) {
                                                $scope.CompanyObject = companydata.ResponseData[0];
                                                $timeout(function () {
                                                    $scope.$apply();
                                                    $scope.showCompanyLogoLoader = false;
                                                    toaster.success("Success", data.message);
                                                    $('#internalLoaderForImage').hide();
                                                });
                                            }
                                            else {
                                                $scope.showCompanyLogoLoader = false;
                                                toaster.error("Error", "Some Error Occured");
                                                $('#internalLoaderForImage').hide();
                                            }
                                        }).error(function () {
                                            $scope.showCompanyLogoLoader = false;
                                            toaster.error("Error", "Some error occured");
                                            $('#internalLoaderForImage').hide();
                                        });
                                });
                            }
                            else {
                                angular.forEach(
                                 angular.element("input[type='file']"),
                                         function (inputElem) {
                                             angular.element(inputElem).val(null);
                                         });
                                $timeout(function () {
                                    $scope.$apply();
                                    $scope.showCompanyLogoLoader = false;
                                    toaster.error("Error", data.message);
                                });
                                $('#internalLoaderForImage').hide();
                            }
                        })
                        .error(function (data, status, headers, config) {
                            // file failed to upload
                            console.log(data);
                            $('#internalLoaderForImage').hide();
                        });
                })(i);
            }

        }


        /* check Path of company Profile Pick*/
        $scope.checkCompanyProfilePic = function (obj) {
            if ((obj != 'Uploads/CompanyLogos/') && (obj != null)) {
                return true;
            }
            else {
                return false;
            }
        }

        angular.element(document).ready(function () {
            debugger;
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu', []);
            }
            var uid = $stateParams.uid;
            var iseditable = false;
            $scope.Role = sharedService.getRoleId();
            if (uid == sharedService.getCompanyId()) {
                iseditable = true;
                $scope.showCompanyProfile(sharedService.getCompanyId(), iseditable);
            }
            else {
                $scope.showCompanyProfile(uid, iseditable);
            }

            //$scope.Permission = sharedService.getInternalPermissionInShared();

        });


        //$scope.isNumber = function (evnt) {
        //    if (evnt.keyCode > 47 && evnt.keyCode < 58 || evnt.charCode > 47 && evnt.charCode < 58) {
        //        return true;
        //    }
        //    else {
        //        evnt.preventDefault();
        //        return false;
        //    }
        //};


        $scope.companyNameClick = function (Permission) {
            if (Permission.CanEditCompanyProfile == false) {
                toaster.warning("Warning", "It seems that you don't have permission to edit company profile.");
                return;
            }
            $scope.toggleCompanyName = !$scope.toggleCompanyName;
            $timeout(function () {
                $('input[name="CompanyName"]').focus();
            }, 100);
        }

        $scope.updateCompanyName = function () {
            
            //$('#globalLoader').show();
            $scope.toggleCompanyName = !$scope.toggleCompanyName;
            var CompanyEditObject = { 'Company': $scope.CompanyObject, 'UserId': sharedService.getUserId() }
            companiesFactory.updateCompanyName(CompanyEditObject)
                .success(function (data) {
                    $scope.$emit('bindCompanyDetail', []);
                    //$('#globalLoader').hide();
                })
                .error(function (data) {
                    //$('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.phoneNoClick = function (Permission) {
            if (Permission.CanEditCompanyProfile == false) {
                toaster.warning("Warning", "It seems that you don't have permission to edit company profile.");
                return;
            }
            $scope.toggleCompanyPhoneNo = !$scope.toggleCompanyPhoneNo;
            $timeout(function () {
                $('input[name="CompanyPhoneNo"]').focus();
            }, 100);
        }

        $scope.updateCompanyPhoneNo = function () {

            //$('#globalLoader').show();
            $scope.toggleCompanyPhoneNo = !$scope.toggleCompanyPhoneNo;
            var CompanyEditObject = { 'Company': $scope.CompanyObject, 'UserId': sharedService.getUserId() }
            companiesFactory.updateCompanyPhoneNo(CompanyEditObject)
                .success(function (data) {
                    //$('#globalLoader').hide();
                })
                .error(function (data) {
                    //$('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.faxClick = function (Permission) {
            if (Permission.CanEditCompanyProfile == false) {
                toaster.warning("Warning", "It seems that you don't have permission to edit company profile.");
                return;
            }
            $scope.toggleCompanyFax = !$scope.toggleCompanyFax;
            $timeout(function () {
                $('input[name="CompanyFax"]').focus();
            }, 100);
        }

        $scope.updateCompanyFax = function () {

            //$('#globalLoader').show();
            $scope.toggleCompanyFax = !$scope.toggleCompanyFax;
            var CompanyEditObject = { 'Company': $scope.CompanyObject, 'UserId': sharedService.getUserId() }
            companiesFactory.updateCompanyFax(CompanyEditObject)
                .success(function (data) {
                    //$('#globalLoader').hide();
                })
                .error(function (data) {
                    //$('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.emailClick = function (Permission) {
            if (Permission.CanEditCompanyProfile == false) {
                toaster.warning("Warning", "It seems that you don't have permission to edit company profile.");
                return;
            }
            $scope.toggleCompanyEmail = !$scope.toggleCompanyEmail;
            $timeout(function () {
                $('input[name="CompanyEmail"]').focus();
            }, 100);
        }

        $scope.updateCompanyEmail = function () {

            //$('#globalLoader').show();
            $scope.toggleCompanyEmail = !$scope.toggleCompanyEmail;
            var CompanyEditObject = { 'Company': $scope.CompanyObject, 'UserId': sharedService.getUserId() }
            companiesFactory.updateCompanyEmail(CompanyEditObject)
                .success(function (data) {
                    //$('#globalLoader').hide();
                })
                .error(function (data) {
                    //$('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.addressClick = function (Permission) {
            if (Permission.CanEditCompanyProfile == false) {
                toaster.warning("Warning", "It seems that you don't have permission to edit company profile.");
                return;
            }
            $scope.toggleCompanyAddress = !$scope.toggleCompanyAddress;
            $timeout(function () {
                $('input[name="CompanyAddress"]').focus();
            }, 100);
        }

        $scope.updateCompanyAddress = function () {

            //$('#globalLoader').show();
            $scope.toggleCompanyAddress = !$scope.toggleCompanyAddress;
            var CompanyEditObject = { 'Company': $scope.CompanyObject, 'UserId': sharedService.getUserId() }
            companiesFactory.updateCompanyAddress(CompanyEditObject)
                .success(function (data) {
                    //$('#globalLoader').hide();
                })
                .error(function (data) {
                    //$('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.webAddressClick = function (Permission) {
            if (Permission.CanEditCompanyProfile == false) {
                toaster.warning("Warning", "It seems that you don't have permission to edit company profile.");
                return;
            }
            $scope.toggleCompanyWebAddress = !$scope.toggleCompanyWebAddress;
            $timeout(function () {
                $('input[name="CompanyWebAddress"]').focus();
            }, 100);
        }

        $scope.updateCompanyWebAddress = function () {

            //$('#globalLoader').show();
            $scope.toggleCompanyWebAddress = !$scope.toggleCompanyWebAddress;
            var CompanyEditObject = { 'Company': $scope.CompanyObject, 'UserId': sharedService.getUserId() }
            companiesFactory.updateCompanyWebAddress(CompanyEditObject)
                .success(function (data) {
                    //$('#globalLoader').hide();
                })
                .error(function (data) {
                    //$('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.companyDescriptionClick = function (Permission) {
            if (Permission.CanEditCompanyProfile == false) {
                toaster.warning("Warning", "It seems that you don't have permission to edit company profile.");
                return;
            }
            $scope.toggleCompanyDescription = !$scope.toggleCompanyDescription;
            $timeout(function () {
                $('#CompanyDescription').focus();
            }, 100);
        }

        $scope.updateCompanyDescription = function () {

            //$('#globalLoader').show();
            $scope.toggleCompanyDescription = !$scope.toggleCompanyDescription;
            var CompanyEditObject = { 'Company': $scope.CompanyObject, 'UserId': sharedService.getUserId() }
            companiesFactory.updateCompanyDescription(CompanyEditObject)
                .success(function (data) {
                    //$('#globalLoader').hide();
                })
                .error(function (data) {
                    //$('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured");
                })
        }



    });
});