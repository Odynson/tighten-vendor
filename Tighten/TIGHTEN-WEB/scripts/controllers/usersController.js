﻿(function () {
    'use strict';


    app.controller('usersController', function ($scope, usersFactory) {
        alert('It is working');

        getUsers();


        function getUsers() {

            usersFactory.getUsers()
          .success(function (data) {
              $scope.users = data;
          })
          .error(function (error) {
              $scope.status = 'Unable to load Users data: ' + error.message;
          });
        }


        $scope.saveUser = function saveUser(user) {


            var frmValid = $("#frmUser").valid();

            if (frmValid) {

                if ($scope.user.userId == 0)
                    insertUser();
                else
                    updateUser();

            }
        }

        $scope.editUser = function editUser(userId) {

            usersFactory.getUser(userId).success(function (data) {
                $scope.user = data;
                $('#myModal').modal('show');
            }).error(function (error) { });

        }

        $scope.deleteUser = function deleteUser(userId) {
            usersFactory.deleteUser(userId).success(function (data) {

                if (data.success)
                    getUsers();

            }).error(function (error) { });

        }


        function insertUser() {
            usersFactory.insertUser($scope.user)
                         .success(function (data) {

                             if (!data.success) {
                                 if (data.errors.length > 1) {
                                     $scope.status = data.errors[1];
                                 }
                                 else {
                                     $scope.status = data.errors[0];
                                 }
                             }
                             else {
                                 $scope.status = 'Inserted User! Refreshing user list.';


                                 getUsers();

                                 setTimeout(function () {
                                     clearFormValues();
                                     $('#myModal').modal('hide');
                                 }, 1000);

                             }

                         }).
                         error(function (error) {
                             $scope.status = 'Unable to insert user: ' + error.message;
                         });
        }


        function updateUser() {
            usersFactory.updateUser($scope.user)
                      .success(function (data) {

                          if (!data.success) {
                              if (data.errors.length > 1) {
                                  $scope.status = data.errors[1];
                              }
                              else {
                                  $scope.status = data.errors[0];
                              }
                          }
                          else {
                              $scope.status = 'Updated User! Refreshing user list.';

                              getUsers();

                              setTimeout(function () {
                                  clearFormValues();
                                  $('#myModal').modal('hide');
                              }, 1000);

                          }
                          console.log($scope.status);
                      }).
                      error(function (error) {
                          $scope.status = 'Unable to update user: ' + error.message;
                      });
        }




        $scope.addUser = function addUser() {
            clearFormValues();
        }

        function clearFormValues() {
            $scope.user = { userId: 0 };
            $scope.status = "";
        }


    }
    )
})();
