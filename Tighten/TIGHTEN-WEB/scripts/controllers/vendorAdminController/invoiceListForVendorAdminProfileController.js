﻿define(['app'], function (app) {


    app.controller('invoiceListForVendorAdminProfileController', function ($scope, $rootScope, apiURL, $location, vendorAdminProfileFactory, sharedService, toaster, $timeout) {
        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCountOutstanding = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.totalCountPaid = 0;
        $scope.totalCountRejected = 0;

        $scope.pageIndexOutstanding = 1;   // Current page number. First page is 1.-->  
        $scope.pageIndexPaid = 1;
        $scope.pageIndexRejected = 1;

        $scope.pageSizeSelectedOutstanding = 5; // Maximum number of items per page. 
        $scope.pageSizeSelectedPaid = 5;
        $scope.pageSizeSelectedRejected = 5;


        $scope.pagesizeListOutstanding = [
            { PageSizeOutstanding: 5 },
            { PageSizeOutstanding: 10 },
            { PageSizeOutstanding: 25 },
            { PageSizeOutstanding: 50 },

        ];
        $scope.PageSizeOutstanding = $scope.pagesizeListOutstanding[0];


        $scope.pagesizeListPaid = [
            { PageSizePaid: 5 },
            { PageSizePaid: 10 },
            { PageSizePaid: 25 },
            { PageSizePaid: 50 },

        ];
        $scope.PageSizePaid = $scope.pagesizeListPaid[0];


        $scope.pagesizeListRejected = [
            { PageSizeRejected: 5 },
            { PageSizeRejected: 10 },
            { PageSizeRejected: 25 },
            { PageSizeRejected: 50 },

        ];
        $scope.PageSizeRejected = $scope.pagesizeListRejected[0];



        $scope.totalPageCountOutstanding = 0;
        $scope.totalPageCountPaid = 0;
        $scope.totalPageCountRejected = 0;

        $scope.pagesizeList = [
            { PageSize: 5 },
            { PageSize: 10 },
            { PageSize: 25 },
            { PageSize: 50 },

        ];


        ///working
        $scope.imgURL = apiURL.imageAddress;
        var VendorCompanyId;
        var dataModel = {}
        $scope.invoiceListOutstanding = [];
        $scope.invoiceListPaid = [];
        $scope.invoiceListRejected = [];
        $scope.TotalOutstandingInvoiceAmount = 0;
        $scope.TotalPaidInvoiceAmount = 0;
        $scope.TotalRejectedInvoiceAmount = 0
        $scope.filterInvocieModal = { fromDate: "", toDate: "" }
        $scope.POCsModel = { VendorId: 0, Name: "--Select All--" };
        $scope.CompanyDropModel = { CompanyId: 0, Name: "--Select All--" };



        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }
            VendorCompanyId = sharedService.getCompanyId();
            CompanyId = sharedService.getCompanyId();
            dataModel = { VendorCompanyId: "", VendorCompanyId, FromDate: "", ToDate: "" }
            $scope.getYearsList = function (range) {
                var currentYear = new Date().getFullYear();
                var years = [];
                for (var i = 0; i < range; i++) {
                    years.push(currentYear + i);
                }
                return years;
            };
            getPocsForInvoiceListAdminProfileDrop(VendorCompanyId)
            dataModel.VendorCompanyId = VendorCompanyId;


        })

        $scope.filterInvocieModal.toDate = moment(new Date()).format("MM/DD/YYYY");

        $scope.format = 'MM/dd/yyyy';

        $scope.popup = {
            opened1: false,
            opened2: false
        };

        $scope.open1 = function () {
            $scope.popup.opened1 = true;

        };

        $scope.open2 = function () {
            $scope.popup.opened2 = true;
        };


        $scope.invoiceCompanyList = [];


        ///////////////////////////////////////////GETPOCS DROP DOWN LIST Project Aware ////////////////////////////////////////////////////////////////////////////////
        function getPocsForInvoiceListAdminProfileDrop(VendorCompanyId) {
            $('#globalLoader').show();

            vendorAdminProfileFactory.getPocsforDrops(VendorCompanyId)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    if (data.ResponseData != null) {
                        $scope.VendorInvoiceAdminProfileCompanyPOCsDropList = data.ResponseData;
                        $scope.VendorInvoiceAdminProfileCompanyPOCsDropList.push($scope.POCsModel);
                        getcompanyForDropFilterInvoice(VendorCompanyId);
                    }
                    else {
                        toaster.error("Error", message.error);
                    }
                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })

        }
        /////////////////////////////////////////////////////////////////getcompanyForDropFilterInvoice
        function getcompanyForDropFilterInvoice(VendorCompanyId) {
            debugger;
            $('#globalLoader').show();
            vendorAdminProfileFactory.getCompanyInvoiceVendorAdminProfileDrop(VendorCompanyId)
                .success(function (data) {
                    if (data.success) {
                        $('#globalLoader').hide();
                        $scope.CompanyDropList = data.ResponseData;
                        $scope.CompanyDropList.push($scope.CompanyDropModel)
                        //getAllInvoiceListVendorAdminProfile(dataModel)
                        getAllInvoiceListVendorAdminProfileOutstanding(dataModel);
                        getAllInvoiceListVendorAdminProfilePaid(dataModel);
                        getAllInvoiceListVendorAdminProfileRejected(dataModel);
                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    }

                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        ////////////////////////////////////////////////////////////////////////////////////// Filter by Company
        $scope.filterInvoiceByCompanyInVendorAdminProfile = function (CompanyDropModel) {
            debugger;
            dataModel.CompanyId = CompanyDropModel.CompanyId;
            //	getAllInvoiceListVendorAdminProfile(dataModel);
            getAllInvoiceListVendorAdminProfileOutstanding(dataModel);
            getAllInvoiceListVendorAdminProfilePaid(dataModel);
            getAllInvoiceListVendorAdminProfileRejected(dataModel);
        }

        ////////////////////////////////////////////////////////////////////////////////////// Filter by Date
        $scope.SearchInvoiceVendorProfile = function () {
            debugger;

            dataModel.FromDate = $scope.filterInvocieModal.fromDate;
            dataModel.ToDate = $scope.filterInvocieModal.toDate;
            //getAllInvoiceListVendorAdminProfile(dataModel);
            getAllInvoiceListVendorAdminProfileOutstanding(dataModel);
            getAllInvoiceListVendorAdminProfilePaid(dataModel);
            getAllInvoiceListVendorAdminProfileRejected(dataModel);
        }

        ////////////////////////////////////////////////////////////////////////////////////// Filter by pocs
        $scope.filterInvoiceByCompanyPOCsInVendorAdminProfile = function (POCsModel) {
            debugger;
            dataModel.VendorId = POCsModel.VendorId;
            //getAllInvoiceListVendorAdminProfile(dataModel);
            getAllInvoiceListVendorAdminProfileOutstanding(dataModel);
            getAllInvoiceListVendorAdminProfilePaid(dataModel);
            getAllInvoiceListVendorAdminProfileRejected(dataModel);
        }

        ////////////////////////////////////getAllInvoiceDetailList//////////////////////////////////////////////////////////////
        function getTotalAmountForNotPaidInvoice() {
            debugger;
            var total_Amount = 0;
            if ($scope.invoiceListOutstanding.length > 0) {
                for (var i = 0; i < $scope.invoiceListOutstanding.length; i++) {
                    var Inv = $scope.invoiceListOutstanding[i];
                    if (Inv.IsPaid == false && Inv.IsRejected == false) {
                        //	$scope.TotalOutstandingInvoiceAmount += Inv.TotalAmount;
                        $scope.TotalOutstandingInvoiceAmount = (parseFloat($scope.TotalOutstandingInvoiceAmount) + parseFloat(Inv.TotalAmount)).toFixed(2);

                    }
                }
            }

        }

        ////////////////////////////////////getTotalAmountForPaidInvoice//////////////////////////////////////////////////////////////
        function getTotalAmountForPaidInvoice() {
            debugger;
            var total_Amount = 0;
            if ($scope.invoiceListPaid.length > 0) {
                for (var i = 0; i < $scope.invoiceListPaid.length; i++) {
                    var Inv = $scope.invoiceListPaid[i];
                    if (Inv.IsPaid == true && Inv.IsRejected == false) {
                        //$scope.TotalPaidInvoiceAmount += (Inv.TotalAmount);
                        $scope.TotalPaidInvoiceAmount = (parseFloat($scope.TotalPaidInvoiceAmount) + parseFloat(Inv.TotalAmount)).toFixed(2);

                    }
                }
            }

        }

        ////////////////////////////////////getTotalAmountForPaidInvoice//////////////////////////////////////////////////////////////
        function getTotalAmountRejectedForPaidInvoice() {
            debugger;
            var total_Amount = 0;
            if ($scope.invoiceListRejected.length > 0) {
                for (var i = 0; i < $scope.invoiceListRejected.length; i++) {
                    var Inv = $scope.invoiceListRejected[i];
                    if (Inv.IsRejected == true) {
                        //$scope.TotalRejectedInvoiceAmount += (Inv.TotalAmount);
                        $scope.TotalRejectedInvoiceAmount = (parseFloat($scope.TotalRejectedInvoiceAmount) + parseFloat(Inv.TotalAmount)).toFixed(2);

                    }
                }
            }

        }


        ////////////////////////////////////getAllInvoiceDetailList//////////////////////////////////////////////////////////////
        function getAllInvoiceListVendorAdminProfile(dataModel) {
            debugger;
            $('#globalLoader').show();
            $scope.TotalOutstandingInvoiceAmount = 0;
            $scope.TotalPaidInvoiceAmount = 0;
            $scope.TotalRejectedInvoiceAmount = 0
            dataModel.PageSizeSelected = $scope.pageSizeSelected;
            dataModel.PageIndex = $scope.pageIndex;

            vendorAdminProfileFactory.getAllInvoiceListVendorAdminProfile(dataModel)
                .success(function (data) {
                    if (data.ResponseData != null) {

                        $scope.invoiceList = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCount = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        //getcompanyForDropFilterInvoice(CompanyId);
                        getTotalAmountForNotPaidInvoice();
                        getTotalAmountForPaidInvoice();
                        getTotalAmountRejectedForPaidInvoice();
                        $('#globalLoader').hide();
                    }
                    else {

                        $('#globalLoader').hide();
                    }
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        $scope.previewInvoiceAftersend = function (item) {
            debugger;
            vendorInvoiceFactory.getPreviewInvoicePaidOutstandingReject(item.Id, VendorId)
                .success(function (data) {
                    if (data.success) {

                        $scope.vedorInvoicePaidOutstandRejectPreview = data.ResponseData;
                        $('#PreviewAfterRasisingInvoiceVendorProfile').modal('show')
                    }
                    else {
                        toaster.error("Error", message.error);
                    }
                })

            $('#InvoicePreviewAfterRasisingInvoice').modal('show')
            $scope.paymentDetailAddUpdateModel = {};
        }

        $scope.previewInvoiceVendorAdminProfile = function (item) {
            debugger;
            vendorAdminProfileFactory.previewInvoiceVendorAdminProfile(item.InvoiceId)
                .success(function (data) {
                    if (data.success) {

                        $scope.vedorInvoicePaidOutstandRejectPreview = data.ResponseData;
                        $('#InvoicePreviewVendorAdminProfileModal').modal('show')
                    }
                    else {
                        toaster.error("Error", message.error);
                    }
                })

            $('#InvoicePreviewAfterRasisingInvoice').modal('show')
            $scope.paymentDetailAddUpdateModel = {};
        },
            Error(function () {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();
            })

        function getAllInvoiceListVendorAdminProfileOutstanding(dataModel) {
            debugger;
            $('#globalLoader').show();
            $scope.TotalOutstandingInvoiceAmount = 0;
            dataModel.PageSizeSelected = $scope.pageSizeSelectedOutstanding;
            dataModel.PageIndex = $scope.pageIndexOutstanding;

            vendorAdminProfileFactory.getAllInvoiceListVendorAdminProfileOutstanding(dataModel)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.invoiceListOutstanding = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCountOutstanding = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountForNotPaidInvoice();
                        $('#globalLoader').hide();
                    }
                    else {

                        $('#globalLoader').hide();
                    }
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        function getAllInvoiceListVendorAdminProfilePaid(dataModel) {
            debugger;
            $('#globalLoader').show();
            $scope.TotalPaidInvoiceAmount = 0;
            dataModel.PageSizeSelected = $scope.pageSizeSelectedPaid;
            dataModel.PageIndex = $scope.pageIndexPaid;
            vendorAdminProfileFactory.getAllInvoiceListVendorAdminProfilePaid(dataModel)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.invoiceListPaid = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCountPaid = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountForPaidInvoice();
                        $('#globalLoader').hide();
                    }
                    else {

                        $('#globalLoader').hide();
                    }
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        function getAllInvoiceListVendorAdminProfileRejected(dataModel) {
            debugger;
            $('#globalLoader').show();
            $scope.TotalRejectedInvoiceAmount = 0
            dataModel.PageSizeSelected = $scope.pageSizeSelectedRejected;
            dataModel.PageIndex = $scope.pageIndexRejected;

            vendorAdminProfileFactory.getAllInvoiceListVendorAdminProfileRejected(dataModel)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.invoiceListRejected = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCountRejected = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountRejectedForPaidInvoice();
                        $('#globalLoader').hide();
                    }
                    else {

                        $('#globalLoader').hide();
                    }
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }







        ////////////////////////////////////------------paging code/////////////////////////////////
        $scope.changePageSize = function (pageSizeSelected, param) {
            debugger;
            if (param == 'Outstanding') {
                $scope.pageSizeSelectedOutstanding = pageSizeSelected.PageSizeOutstanding;
                getAllInvoiceListVendorAdminProfileOutstanding(dataModel)

            }
            else if (param == 'Paid') {
                $scope.pageSizeSelectedPaid = pageSizeSelected.PageSizePaid;
                getAllInvoiceListVendorAdminProfilePaid(dataModel)
            }
            else if (param == 'Rejected') {
                $scope.pageSizeSelectedRejected = pageSizeSelected.PageSizeRejected;
                getAllInvoiceListVendorAdminProfileRejected(dataModel)

            }
        }

        $scope.pageChanged = function (pageIndex, param) {
            debugger;
            if (param == 'Outstanding') {
                $scope.pageIndexOutstanding = pageIndex;
                getAllInvoiceListVendorAdminProfileOutstanding(dataModel)

            }
            else if (param == 'Paid') {
                $scope.pageIndexPaid = pageIndex;
                getAllInvoiceListVendorAdminProfilePaid(dataModel)
            }
            else if (param == 'Rejected') {
                $scope.pageIndexRejected = pageIndex;
                getAllInvoiceListVendorAdminProfileRejected(dataModel)

            }
        }

        ////////////////////////////////////------------paging code/////////////////////////////////


    })

})