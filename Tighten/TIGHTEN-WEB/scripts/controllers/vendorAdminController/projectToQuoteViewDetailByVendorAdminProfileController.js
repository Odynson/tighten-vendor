﻿define(['app'], function (app) {

    app.controller('projectToQuoteViewDetailByVendorAdminProfileController', function ($scope, $rootScope, $stateParams, sharedService, toaster, $sce, $http, apiURL, vendorAdminProfileFactory) {


        var VendorCompanyId;
        var ProjectId;
        var VendorId;
        var dataModal;

        ProjectId = $stateParams.pid;
        VendorId = $stateParams.Vid;

        // custom control
        $scope.CustomDirectiveReady = false;
        $scope.ProjectIsEdit = true;
        $scope.customcontrolmodel = {};

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }
            VendorCompanyId = sharedService.getCompanyId();

            dataModal = { ProjectId: ProjectId, VendorId: VendorId }

            getProjectToQuoteViewDetailAdminProfile(dataModal);
        })



        //////////////////////////////////////////////////////modalMilestoneDetail///////////////////////////////////////////////////////////////////////
        $scope.PhaseIndex
        $scope.modalMilestoneProjectToQuoteAdminProfile = function (index) {

            debugger;
            $scope.PhaseIndex = index;
            $('#modalshowMilestoneDetail').modal('show')

        }


        function getProjectToQuoteViewDetailAdminProfile(dataModal) {
            debugger;
            $('#globalLoader').show();
            vendorAdminProfileFactory.getProjectToQuoteViewDetailAdminProfile(dataModal)
            .success(function (data) {
                debugger;
                $('#globalLoader').hide();
                if (data.ResponseData != null) {
                    $scope.getProjectDetailForVendorQuoteList = data.ResponseData;


                    //------getting custom control area ---------//
                    if (data.ResponseData.CustomControlValueModel != null) {
                        $scope.customcontrolmodel = JSON.parse(data.ResponseData.CustomControlValueModel.CustomControlData);


                    }
                    //-----------------Custom cuntrol is not use by Company---------------------------------------///

                    $scope.CustomcontrolList = data.ResponseData.CustomControlUIList;
                    $scope.CustomDirectiveReady = true;


                }
                else {
                    toaster.error("Error", message.error);
                }
            }), Error(function myfunction() {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();

            })

        }




        // file download code here
        $scope.downloadQuoteProjectForVendorAdminProfile = function (FilePath, FileName, ActualFileName) {
            debugger;
            $('#globalLoader').show();

            var url = apiURL.baseAddress + "VendorApi/";

            $http({
                method: 'Post',
                url: url + "GetFiledownload",
                data: { FileName: FileName, FilePath: FilePath },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                headers = headers();
                debugger;
                //var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", ActualFileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    $('#globalLoader').hide();

                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    console.log(ex);
                    $('#globalLoader').hide();
                }
            }).error(function (data, status) {
                console.log(data);
                $('#globalLoader').hide();
            });
        };

    })
})