﻿define(['app'], function () {

    app.controller('vendorAdminProfileInvitationController', function ($scope, sharedService, $rootScope, toaster, vendorAdminProfileFactory) {

        var VendorCompanyId = 0;

        var dataModal = { CompanyId: 0, VendorId: 0, IsApproved: null, VendorCompanyId: VendorCompanyId, PageIndex: 1, PageSize: 5, TotalPageCount: 0 }



        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  




        $scope.CompanyDropModal = { CompanyId: 0, Name: "--Select All--" };
        $scope.POCsModal = { VendorId: 0, Name: "--Select All--" };


        angular.element(document).ready(function () {
            debugger;
            VendorCompanyId = sharedService.getCompanyId();
            dataModal = { CompanyId: 0, VendorId: 0, IsApproved: null, VendorCompanyId: VendorCompanyId, PageIndex: 1, PageSize: 5, TotalPageCount: 0 }

            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }
            getPocsforDrops(VendorCompanyId);

        })


        ///////////////////////////////////////////GETPOCS DROP DOWN LIST ////////////////////////////////////////////////////////////////////////////////
        function getPocsforDrops(VendorCompanyId) {
            $('#globalLoader').show();

            vendorAdminProfileFactory.getPocsforDrops(VendorCompanyId)
            .success(function (data) {
                debugger;
                $('#globalLoader').hide();
                if (data.ResponseData != null) {
                    $scope.CompanyPOCsList = data.ResponseData;
                    $scope.CompanyPOCsList.push($scope.POCsModal);

                    getCompanyForInvitaionDrop(VendorCompanyId);
                }
                else {
                    toaster.error("Error", message.error);
                }
            }),
            Error(function myfunction() {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();

            })

        }


        //////////////////////////////GetCompanyForInvitaionDrop DROP DOWN LIST //////////////////////////////////////////////////////////


        function getCompanyForInvitaionDrop(VendorCompanyId) {
            debugger;
            $('#globalLoader').show();
            vendorAdminProfileFactory.getCompanyForInvitaionDrop(VendorCompanyId)
            .success(function (data) {
                debugger;
                $('#globalLoader').hide();
                if (data.ResponseData != null) {
                    $scope.CompanyDropList = data.ResponseData
                    $scope.CompanyDropList.push($scope.CompanyDropModal)
                    getInvitationTovendor(dataModal);

                }
                else {
                    toaster.error("Error", message.error);
                }


            }),
            Error(function myfunction() {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();

            })

        }

        //////////////////////////////getInvitationTovendor DROP DOWN LIST //////////////////////////////////////////////////////////


        function getInvitationTovendor(dataModal) {
            debugger;
            $('#globalLoader').show();

            vendorAdminProfileFactory.getInvitationToVendor(dataModal)
            .success(function (data) {
                debugger;
                $('#globalLoader').hide();
                if (data.ResponseData != null) {
                    $scope.invitaionToVendorRespList = data.ResponseData;
                    $scope.totalCount = data.ResponseData[0].TotalPageCount;


                }
                else {
                    toaster.error("Error", message.error);
                }

            }),
            Error(function myfunction() {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();

            })

        }

        $scope.myVendorstatusList = [
          { IsApproved: null, status: '---Select All---', Id: 0 },
          { IsApproved: true, status: 'Accepted', Id: 1 },
          { IsApproved: false, status: 'Rejected', Id: 2 },
          { IsApproved: null, status: 'Waiting for approval', Id: 3 },

        ]

        $scope.selectStatusModal = $scope.myVendorstatusList[0];



        $scope.FilterInvitationsByCompanyAdminProfile = function (CompanyDropModal) {
            debugger;
            dataModal.CompanyId = CompanyDropModal.CompanyId
            getInvitationTovendor(dataModal)
        }

        $scope.FilterInvitationPOCsAdminProfile = function (CompanyDropModal) {
            debugger;
            dataModal.VendorId = CompanyDropModal.VendorId
            getInvitationTovendor(dataModal)
        }


        $scope.FilterInvitationByStatusAdminProfile = function (CompanyDropModal) {
            debugger;
            dataModal.IsApproved = CompanyDropModal.IsApproved
            dataModal.IsApprovedId = CompanyDropModal.Id
            getInvitationTovendor(dataModal)
        }

        ////////////////////////////////////------------paging code/////////////////////////////////
        $scope.pagesizeList = [
       { PageSize: 5 },
       { PageSize: 10 },
       { PageSize: 25 },
       { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];




        $scope.changePageSize = function (pageSizeSelected) {
            debugger;
            dataModal.PageSize = pageSizeSelected.PageSize;
            $scope.pageSizeSelected = pageSizeSelected.PageSize;
            getInvitationTovendor(dataModal)
        }

        $scope.pageChanged = function (pageIndex) {
            debugger;
            dataModal.PageIndex = pageIndex;
            getInvitationTovendor(dataModal)
        }


        ////////////////////////////////////------------paging code/////////////////////////////////



    })


})
