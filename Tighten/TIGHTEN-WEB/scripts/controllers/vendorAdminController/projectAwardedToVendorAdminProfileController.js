﻿

define(['app'], function (app) {

    app.controller('projectAwardedToVendorAdminProfileController', function ($scope, $rootScope, apiURL, $location, vendorAdminProfileFactory, sharedService, toaster, $timeout, $http, $sce) {

        //Working
  
        var baseurl = apiURL.baseAddress;
        var VendorCompanyId = 0
        var dataModel = {} // request for mail list for quoted project

        $scope.CompanyDropModal = { CompanyId: 0, Name: "--Select All--" };
        $scope.POCsModal = { VendorId: 0, Name: "--Select All--" }


        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 100;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  



        
        angular.element(document).ready(function () {

            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }


            VendorCompanyId = sharedService.getCompanyId();
            getPocsForProjectAwardedAdminProfileDrop(VendorCompanyId)

            dataModel = { ProjectId: 0, VendorId: 0, CompanyId: 0, StatusId: 0, VendorCompanyId: VendorCompanyId,UnpaidInvoice: 0, PageIndex: 1, PageSize: 5, TotalPageCount: 0 }
        })



            

            ///////////////////////////////////////////GETPOCS DROP DOWN LIST Project Aware ////////////////////////////////////////////////////////////////////////////////
            function getPocsForProjectAwardedAdminProfileDrop(VendorCompanyId) {
                $('#globalLoader').show();

                vendorAdminProfileFactory.getPocsforDrops(VendorCompanyId)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    if (data.ResponseData != null) {
                        $scope.ProjectAwardedScreenAdminProfileCompanyPOCsDropList = data.ResponseData;
                        $scope.ProjectAwardedScreenAdminProfileCompanyPOCsDropList.push($scope.POCsModal);
                        getCompanyForProjectAwardedAdminProfileDrop(VendorCompanyId)
                    }
                    else {
                        toaster.error("Error", message.error);
                    }
                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })

            }



            ///////////////////////////////////////////GETPOCS DROP DOWN LIST ////////////////////////////////////////////////////////////////////////////////
            function getCompanyForProjectAwardedAdminProfileDrop(VendorCompanyId) {
                $('#globalLoader').show();
                vendorAdminProfileFactory.getCompanyForProjectAwardedAdminProfileDrop(VendorCompanyId)
                .success(function (data) {
                    $('#globalLoader').hide();
                    if (data.ResponseData != null) {
                        $scope.CompanyDropList = data.ResponseData;
                        $scope.CompanyDropList.push($scope.CompanyDropModal);
                        getProjectAwardedScreenListVendorAdminProfile(dataModel);


                    }
                    else {
                        toaster.error("Error", message.error);
                    }

                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
            }

        ///////////////////////////////////////////-getProjectToQuoteListVendorAdminProfile-//////////////////////////////////////////////////////////////////////


            function getProjectAwardedScreenListVendorAdminProfile(dataModel) {
                $('#globalLoader').show();

                vendorAdminProfileFactory.getAwardedProjectListAdminProfile(dataModel)
                .success(function (data) {
                    debugger;
                    $('#globalLoader').hide();
                    if (data.ResponseData != null) {
                        $scope.vendorAdminProfileProjectAwardedList = data.ResponseData;
                        $scope.totalCount = data.ResponseData[0].TotalPageCount;
                    }
                    else {
                        toaster.error("Error", message.error);
                    }
                }), Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
            }





            $scope.performanceStatusProjectAwardedScreenDropDownList = [
                 { StatusId: 0, Status: "--Select All--" },
                 { StatusId: 1, Status: "--well in range--" },
                 { StatusId: 2, Status: "--neck to neck--" },
                 { StatusId: 3, Status: "--Over Spent--" },

            ];

            $scope.filterPerformanceModal = $scope.performanceStatusProjectAwardedScreenDropDownList[0];



            $scope.autoCompleteOptionsVendorAdminProjectAwardedScreen = {

                minimumChars: 3,
                dropdownWidth: '500px',
                dropdownHeight: '200px',
                data: function (ProjectName) {
                    return $http.get(baseurl + 'VendorAdminProfileApi/GetProjectByNameVendorAdminProfile/' + VendorCompanyId + '/' + ProjectName)
                        .then(function (data) {
                            debugger;
                            return data.data.ResponseData;

                        });
                },
                renderItem: function (item) {
                    return {
                        value: item.ProjectName,
                        label: $sce.trustAsHtml(
                        "<p class='auto-complete'>"
                        + item.ProjectName +
                        "</p>")
                    };
                },
                itemSelected: function (item) {
                    debugger;
                    $scope.qouteSentToProjectNameModal = item.item.ProjectName;
                    dataModel.ProjectId = item.item.ProjectId
                    getProjectAwardedScreenListVendorAdminProfile(dataModel);
                }
            }



            var isCtrlPressed;
            var isAPressed;

        ///////////////////////////// on key combination type search//////////////////////////////////////////////////////
            $scope.searchProjectNameProjectAwardedScreenVendorAdminProfile = function (qouteSentToProjectNameModal, event) {
                debugger;
                if (qouteSentToProjectNameModal != undefined && qouteSentToProjectNameModal.length <= 1 && event.keyCode == 8) {
                    dataModel.Name = "";
                    dataModel.ProjectId = 0
                    getProjectAwardedScreenListVendorAdminProfile(dataModel);
                }
                // if key is pressed
                if (event.keyCode == 17 || isCtrlPressed) {
                    isCtrlPressed = true;
                }
                else {
                    isCtrlPressed = false;
                }

                // other key after the control
                if (isCtrlPressed) {
                    if (event.keyCode == 97 || event.keyCode == 65) {
                        isAPressed = true;
                    }
                }
                else {
                    isAPressed = false;
                }


                if (isCtrlPressed && isAPressed) {
                    if (event.keyCode == 46 || event.keyCode == 8) {
                        dataModel.Name = "";
                        dataModel.ProjectId = 0
                        getProjectAwardedScreenListVendorAdminProfile(dataModel);
                        isCtrlPressed = false;
                        isAPressed = false;
                    }

                }
                var text = window.getSelection().toString()
                if (text != "" && (event.keyCode == 8 || event.keyCode == 46)) {
                    dataModel.Name = "";
                    dataModel.ProjectId = 0
                    getProjectAwardedScreenListVendorAdminProfile(dataModel);
                }
            }

        ///////////////////////////////////Mouse Right click cut///////////////////////////////////////////////////////////////////////////////////
            $scope.cutSearchProjectAwardedScreenVendorAdminProfile = function () {
                dataModel.Name = "";
                dataModel.ProjectId = 0
                getProjectAwardedScreenListVendorAdminProfile(dataModel);

            }

            
        ///////////////////////////////////////////////// Filter by performance status

            $scope.filterByPerformanceStatusAwardedScreenVendorAdminProfile = function (filterPerformanceModal) {
                debugger
                dataModel.StatusId = filterPerformanceModal.StatusId;
                getProjectAwardedScreenListVendorAdminProfile(dataModel);
            }


        /////////////////////////////////////////////////////////////////// Filter by Unpaid Invoice

            $scope.showUpdaidInvoice = function (UnpaidInvoice) {
                debugger;
                dataModel.UnpaidInvoice = UnpaidInvoice;
                getProjectAwardedScreenListVendorAdminProfile(dataModel);
            }

        /////////////////////////////////////////// Filter by company

            $scope.filterAwardedProjectByCompanyInVendorAdminProfile = function (CompanyDropModal) {
                debugger;
                dataModel.CompanyId = CompanyDropModal.CompanyId
                getProjectAwardedScreenListVendorAdminProfile(dataModel);

            }
    

        ///////////////////////////////////////////Filter by Pocs
            $scope.filterProjectAwardedPOCsInVendorAdminProfile = function (POCsModal) {
                debugger;
                dataModel.VendorId = POCsModal.VendorId
                getProjectAwardedScreenListVendorAdminProfile(dataModel);
            }

        //-------------------pagesizeList---------------------------------------------------/////
            $scope.pagesizeList = [
                { PageSize: 5 },
                { PageSize: 10 },
                { PageSize: 25 },
                { PageSize: 50 },

            ];

            $scope.PageSize = $scope.pagesizeList[0];



        //-------------------changePageSize--------------------------------------------------------/////
            $scope.changePageSize = function (pageSizeSelected) {
                debugger;
                dataModel.PageSize = pageSizeSelected.PageSize;
                $scope.pageSizeSelected = pageSizeSelected.PageSize;
                getProjectAwardedScreenListVendorAdminProfile(dataModel);
            }

        //-------------------pageChanged-------------------------------------------------------/////
            $scope.pageChanged = function (pageIndex) {
                debugger;
                dataModel.PageIndex = pageIndex;
                getProjectAwardedScreenListVendorAdminProfile(dataModel)
            }

            
    })

})