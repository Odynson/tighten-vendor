﻿
define(['app'], function (app) {
    app.controller('projectToQuoteListVendorAdminProfileController', function ($scope, $rootScope, $http, apiURL, $stateParams, sharedService, toaster, $sce, vendorAdminProfileFactory) {

        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  



        var VendorCompanyId = 0
        var baseurl = apiURL.baseAddress;
        var dataModel = {} // request for mail list for quoted project

        $scope.CompanyDropModal = { CompanyId: 0, Name: "--Select All--" };
        $scope.POCsModal = { VendorId: 0, Name: "--Select All--" };
        angular.element(document).ready(function () {

            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }
            VendorCompanyId = sharedService.getCompanyId();
            getPocsforDrops(VendorCompanyId);

            dataModel = { ProjectId: 0, VendorId: 0, CompanyId: 0, StatusId: 0, VendorCompanyId: VendorCompanyId, PageIndex: 1, PageSize: 5, TotalPageCount: 0 }
        })

        // custom control
        $scope.CustomDirectiveReady = false;
        $scope.ProjectIsEdit = true;
        $scope.customcontrolmodel = {};

        ///////////////////////////////////////////GETPOCS DROP DOWN LIST ////////////////////////////////////////////////////////////////////////////////
        function getPocsforDrops(VendorCompanyId) {
            $('#globalLoader').show();

            vendorAdminProfileFactory.getPocsforDrops(VendorCompanyId)
            .success(function (data) {

                $('#globalLoader').hide();
                if (data.ResponseData != null) {
                    $scope.CompanyPOCsProjectToQouteList = data.ResponseData;
                    $scope.CompanyPOCsProjectToQouteList.push($scope.POCsModal);
                    getCompanyForProjectToQuoteDrop(VendorCompanyId);
                }
                else {
                    toaster.error("Error", message.error);
                }
            }),
            Error(function myfunction() {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();

            })

        }

        ///////////////////////////////////////////GETPOCS DROP DOWN LIST ////////////////////////////////////////////////////////////////////////////////
        function getCompanyForProjectToQuoteDrop(VendorCompanyId) {
            $('#globalLoader').show();
            vendorAdminProfileFactory.getCompanyForProjectToQuoteDrop(VendorCompanyId)
            .success(function (data) {

                $('#globalLoader').hide();
                if (data.ResponseData != null) {
                    $scope.CompanyDropList = data.ResponseData;
                    $scope.CompanyDropList.push($scope.CompanyDropModal);
                    getProjectToQuoteListVendorAdminProfile(dataModel)

                }
                else {
                    toaster.error("Error", message.error);
                }

            }),
            Error(function myfunction() {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();
            })
        }




        function getProjectToQuoteListVendorAdminProfile(dataModel) {
            $('#globalLoader').show();
            debugger;

            vendorAdminProfileFactory.getProjectToQuoteListVendorAdminProfile(dataModel)
            .success(function (data) {
                debugger;
                $('#globalLoader').hide();
                if (data.ResponseData != null) {
                    $scope.getAdminProfileProjectTOQuoteList = data.ResponseData;
                    if (data.ResponseData != null) {
                        $scope.totalCount = data.ResponseData[0].TotalPageCount;
                    }
                   
                }
                else {
                    toaster.error("Error", message.error);
                }
            }), Error(function myfunction() {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();
            })
        }





        ///////////////////////////////////////////VendorStatusDropdown ////////////////////////////////////////////////////////////////////////////////

        $scope.QouteSendByVendorDropList = [
                    { StatusId: 0, Status: '--Select All --' },
                    { StatusId: 3, Status: 'given' },
                    { StatusId: 2, Status: 'pending' },
                    { StatusId: 1, Status: 'rejected' }

        ]




        $scope.QouteSendStatusDropModal = $scope.QouteSendByVendorDropList[0]

        ///////////////////////////////////////////FilterProjectToQouteByCompanyInVendorAdminProfile ////////////////////////////////////////////////////////////

        $scope.FilterProjectToQouteByCompanyInVendorAdminProfile = function (CompanyDropModal) {
            dataModel.CompanyId = CompanyDropModal.CompanyId
            getProjectToQuoteListVendorAdminProfile(dataModel)

        }
        ///////////////////////////////////////////FilterProjectToQouteByQuotedStatusInVendorAdminProfile /////////////////////////////////////////////////////////

        $scope.FilterProjectToQouteByQuotedStatusInVendorAdminProfile = function (QouteSendStatusDropModal) {

            dataModel.StatusId = QouteSendStatusDropModal.StatusId
            getProjectToQuoteListVendorAdminProfile(dataModel)
        }

        ///////////////////////////////////////////FilterProjectToQouteByPOCsInVendorAdminProfile /////////////////////////////////////////////////////////
        $scope.FilterProjectToQouteByPOCsInVendorAdminProfile = function (POCsModal) {
            dataModel.VendorId = POCsModal.VendorId
            getProjectToQuoteListVendorAdminProfile(dataModel)
        }

        $scope.autoCompleteOptionsVendorAdmin = {

            minimumChars: 3,
            dropdownWidth: '500px',
            dropdownHeight: '200px',
            data: function (ProjectName) {
                return $http.get(baseurl + 'VendorAdminProfileApi/GetProjectByNameVendorAdminProfile/' + VendorCompanyId + '/' + ProjectName)
                    .then(function (data) {
                        return data.data.ResponseData;

                    });
            },
            renderItem: function (item) {
                return {
                    value: item.ProjectName,
                    label: $sce.trustAsHtml(
                    "<p class='auto-complete'>"
                    + item.ProjectName +
                    "</p>")
                };
            },
            itemSelected: function (item) {
               
                $scope.qouteSentToProjectNameModal = item.item.ProjectName;
                dataModel.ProjectId = item.item.ProjectId
                getProjectToQuoteListVendorAdminProfile(dataModel);
            }
        }
        /////////////////////////////empty text box project to quote companyProfile//////////////////////////////////////////////////////
        $scope.searchProjectNameProjectToQuote = function (qouteSentToProjectNameModal, event) {

           
            if (event.keyCode == 46) {
                qouteSentToProjectNameModal = "p";

            }
            if (qouteSentToProjectNameModal != undefined && qouteSentToProjectNameModal.length <= 1 && (event.keyCode == 8 || event.keyCode == 46)) {

                dataModel.CompanyId = 0;
                dataModel.Name = "";
                dataModel.ProjectId = 0
                getProjectToQuoteListVendorAdminProfile(dataModel)();
            }



        }

        var isCtrlPressed;
        var isAPressed;

        /////////////////////////////empty text box project to quote companyProfile//////////////////////////////////////////////////////
        $scope.searchProjectNameProjectToQuote = function (qouteSentToProjectNameModal, event) {
           
            if (qouteSentToProjectNameModal != undefined && qouteSentToProjectNameModal.length <= 1 && event.keyCode == 8) {
                dataModel.Name = "";
                dataModel.ProjectId = 0
                getProjectToQuoteListVendorAdminProfile(dataModel)();
            }
            // if key is pressed
            if (event.keyCode == 17 || isCtrlPressed) {
                isCtrlPressed = true;
            }
            else {
                isCtrlPressed = false;
            }

            // other key after the control
            if (isCtrlPressed) {
                if (event.keyCode == 97 || event.keyCode == 65) {
                    isAPressed = true;
                }
            }
            else {
                isAPressed = false;
            }


            if (isCtrlPressed && isAPressed) {
                if (event.keyCode == 46 || event.keyCode == 8) {
                    dataModel.Name = "";
                    dataModel.ProjectId = 0
                    getProjectToQuoteListVendorAdminProfile(dataModel);
                    isCtrlPressed = false;
                    isAPressed = false;
                }

            }
            var text = window.getSelection().toString()
            if (text != "" && (event.keyCode == 8 || event.keyCode == 46)) {
                dataModel.Name = "";
                dataModel.ProjectId = 0
                getProjectToQuoteListVendorAdminProfile(dataModel);
            }
        }


        $scope.cutSearchProjectToQuoteProjectName = function () {
            dataModel.Name = "";
            dataModel.ProjectId = 0
            getProjectToQuoteListVendorAdminProfile(dataModel);

        }

        ////////////////////////////////////------------paging code/////////////////////////////////
        $scope.pagesizeList = [
       { PageSize: 5 },
       { PageSize: 10 },
       { PageSize: 25 },
       { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];

        $scope.changePageSize = function (pageSizeSelected) {
            debugger;
            dataModel.PageSize = pageSizeSelected.PageSize;
            $scope.pageSizeSelected = pageSizeSelected.PageSize;
            getProjectToQuoteListVendorAdminProfile(dataModel);
        }

        $scope.pageChanged = function (pageIndex) {
            debugger;
            dataModel.PageIndex = pageIndex;
            getProjectToQuoteListVendorAdminProfile(dataModel);
        }


        ////////////////////////////////////------------paging code/////////////////////////////////


    })
})