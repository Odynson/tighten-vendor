﻿
define(['app'], function (app) {

    app.controller('projectCompletionFeedBackVendorAdminProfileController',
        function ($scope, $rootScope, apiURL, $location, $stateParams, vendorAdminProfileFactory, sharedService, vendorAdminProfileFactory, toaster, $timeout) {


            var ProjectId = $stateParams.pid;
            var VendorId = $stateParams.vid;

            $scope.feedbackModal = { vendorId: '', projectCompletiontDate: '', vendorFeedback: '', rating: '', ResourceList: [] }

            angular.element(document).ready(function () {


                if ($rootScope.loadSideMenu) {
                    $scope.$emit('showMenu');
                }
                CompanyId = sharedService.getCompanyId();
                getProjectFeedBackVendorAdminProfile(ProjectId, VendorId)

            })

            $scope.popup = {}
            $scope.format = 'MM/dd/yyyy';
            $scope.open = function () {
                $scope.popup.opened = true;
                $scope.mindate = new Date();
            };

            $scope.GetCurrentdate = function () {
                debugger;
                $scope.feedbackModal.projectCompletiontDate = moment(new Date()).format("MM/DD/YYYY");
            }


            function getProjectFeedBackVendorAdminProfile(ProjectId, VendorId) {
                $('#globalLoader').show();
                debugger;
                vendorAdminProfileFactory.getProjectCompletionFeedBackVendorAdminProfile(ProjectId, VendorId)
                .success(function (data) {

                    if (data.success) {
                        $('#globalLoader').hide();
                        if (data.ResponseData.ProjectId != 0) {

                            $scope.ProjectCompletionModal = data.ResponseData;
                            toaster.success("Success", message.successdataLoad);
                            $scope.feedbackModal.vendorFeedback = data.ResponseData.vendorFeedback;
                            $scope.feedbackModal.rating = data.ResponseData.Rating;
                            $scope.feedbackModal.projectCompletiontDate = data.ResponseData.ProjectEndDate;
                            $scope.isRated = $scope.ProjectCompletionModal.IsComplete == 3 ? 'true' : false
                            $('#globalLoader').hide();
                        }
                        else {
                            toaster.error("Error", data.error);
                            $('#globalLoader').hide();
                        }
                    }

                }), Error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                })

            }
        })
})