﻿define(['app'], function (app) {

    app.controller('roleManageVendorAdminProfileController', function ($scope, $rootScope, apiURL, $location, sharedService, toaster, $timeout, $http, $sce, roleVendorAdminProfileFactory) {


        debugger;

        var VendorCompanyId;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }

        angular.element(document).ready(function () {

            VendorCompanyId = sharedService.getCompanyId();
            getCompanyRoleVendorAdminProfile(VendorCompanyId);
        })



        function getCompanyRoleVendorAdminProfile(VendorCompanyId) {

            $('#globalLoader').show();
            roleVendorAdminProfileFactory.getAllRolesForVendorAdminProfile(VendorCompanyId)
            .success(function (data) {
                $scope.VendorCompanyRoleList = data.ResponseData;
                $('#globalLoader').hide();

            })
             .error(function () {
                 toaster.error("Error", message["error"]);
                 $('#globalLoader').hide();
             })

        }


        /*SHowing Add Update Role Modal Pop Up*/
        $scope.showRoleAddUpdateVendorAdminProfileModal = function () {

            $scope.roleButton = false;
            $scope.roleAddUpdateModel = {};

            $("input[name='RoleName']").removeClass("inputError");
            $("input[name='RoleDescription']").removeClass("inputError");
            $("input[name='RolePriority']").removeClass("inputError");

            $('#modalRoleAddUpdateVendorCompanyProfile').modal('show');
        };


        $scope.insertRoleVendorAdminProfile = function (isRoleNameInvalid, isRoleDescriptionInvalid, isRolePriorityInvalid, roleAddUpdateForm) {
            debugger;

            if (isRoleNameInvalid || isRoleDescriptionInvalid) {
                roleAddUpdateForm.RoleName.$dirty = true;
                roleAddUpdateForm.RoleDescription.$dirty = true;
                roleAddUpdateForm.RolePriority.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                var NewRoleObj = { 'RoleModel': $scope.roleAddUpdateModel, 'UserId': sharedService.getUserId(), 'VendorCompanyId': sharedService.getCompanyId() }
                roleVendorAdminProfileFactory.insertRoleVendorAdminProfile(NewRoleObj)
                   .success(function (insertData) {
                       if (insertData.success) {
                           $('#globalLoader').hide();
                           $('#modalRoleAddUpdateVendorCompanyProfile').modal('hide');
                           debugger;
                           if (insertData.ResponseData == 'RoleAddedSuccess') {

                               /* Getting Company Teams with Users */
                               getCompanyRoleVendorAdminProfile(VendorCompanyId);

                           }
                           else {
                               toaster.warning("Warning", message[insertData.ResponseData]);
                               $('#globalLoader').hide();
                           }
                       }
                       else {
                           toaster.error("Error", message["error"]);
                           $('#globalLoader').hide();
                       }

                   })
                    .error(function (error) {
                        toaster.error("Error", message["error"]);
                        $('#globalLoader').hide();
                    });
            }
        };




        /*Edit Role*/
        $scope.editRole = function (roleObject) {
            debugger;
            $scope.roleButton = true;
            $scope.roleAddUpdateModel = {};
            $scope.roleAddUpdateModel.RoleId = roleObject.RoleId;
            $scope.roleAddUpdateModel.RoleName = roleObject.RoleName;
            $scope.roleAddUpdateModel.RoleDescription = roleObject.RoleDescription;
            $scope.roleAddUpdateModel.RolePriority = roleObject.RolePriority;

            $('#modalRoleAddUpdateVendorCompanyProfile').modal('show');
        };


        /*Update Role*/
        $scope.updateRoleVendorAdminProfile = function (isRoleNameInvalid, isRoleDescriptionInvalid, isRolePriorityInvalid, roleAddUpdateForm) {
            if (isRoleNameInvalid || isRoleDescriptionInvalid) {
                roleAddUpdateForm.RoleName.$dirty = true;
                roleAddUpdateForm.RoleDescription.$dirty = true;
                roleAddUpdateForm.RolePriority.$dirty = true;
            }
            else {

                $('#globalLoader').show();
                var NewRoleObj = { 'RoleModel': $scope.roleAddUpdateModel, 'UserId': sharedService.getUserId(), 'VendorCompanyId': sharedService.getCompanyId() }
                roleVendorAdminProfileFactory.updateRoleVendorAdminProfile(NewRoleObj)
                   .success(function (updateData) {
                       if (updateData.success) {
                           $('#globalLoader').hide();
                           $('#modalRoleAddUpdateVendorCompanyProfile').modal('hide');
                           if (updateData.ResponseData == 'RoleUpdatedSuccess') {

                               getCompanyRoleVendorAdminProfile(VendorCompanyId);


                           }
                           else {
                               toaster.warning("Warning", message[updateData.ResponseData]);
                               $('#globalLoader').hide();
                           }
                       }
                       else {
                           toaster.error("Error", message["error"]);
                           $('#globalLoader').hide();
                       }

                   })
                    .error(function (error) {
                        toaster.error("Error", message["error"]);
                        $('#globalLoader').hide();
                    });

            }
        };

        $scope.RoleId = 0
        $scope.deleteRoleVendorAdminProfile = function (RoleId) {
            debugger;
            $scope.RoleId = RoleId;
            $('#deleteVendorProfileRoleconfirmModal').modal('show');

        }


        /*Update Role*/
        $scope.deleteRoleVendorAdminProfileYes = function (RoleId) {
            debugger;
            $('#globalLoader').show();
            roleVendorAdminProfileFactory.deleteRoleVendorAdminProfile(RoleId)
                   .success(function (ResponseData) {
                       debugger
                       if (ResponseData.success) {
                           $('#globalLoader').hide();
                           $('#deleteVendorProfileRoleconfirmModal').modal('hide')
                           if (ResponseData.ResponseData == 'RoleDeletedSuccessfully') {
                                getCompanyRoleVendorAdminProfile(VendorCompanyId);
                               toaster.success("Success", message.saveSccessFullyComman);
                     

                           }
                           else {
                                toaster.error("Error", message["error"]);
                               $('#globalLoader').hide();
                           }
                       }

                   })
                    .error(function(error) {
                        toaster.error("Error", message["error"]);
                        $('#globalLoader').hide();
                    });


        };






    })



})