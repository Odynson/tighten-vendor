﻿define(['app'], function (app) {


    app.controller('userManagementVendorAdminProfileController', function ($scope, $rootScope, apiURL, $location, userManagementFactory, sharedService, toaster, $timeout, vendorFactory, teamFactory) {
        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCountUsers = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.totalCountResource = 0;
        $scope.totalCountInternalUsers = 0;


        $scope.pageIndexUsers = 1;   // Current page number. First page is 1.-->  
        $scope.pageIndexResource = 1;
        $scope.pageIndexInternalUsers = 1;

        $scope.pageSizeSelectedUsers = 5; // Maximum number of items per page. 
        $scope.pageSizeSelectedResource = 5;
        $scope.pageSizeSelectedInternalUsers = 5;

        $scope.pagesizeListUsers = [
            { PageSizeUsers: 5 },
            { PageSizeUsers: 10 },
            { PageSizeUsers: 25 },
            { PageSizeUsers: 50 },

        ];
        $scope.PageSizeUsers = $scope.pagesizeListUsers[0];


        $scope.pagesizeListResource = [
            { PageSizeResource: 5 },
            { PageSizeResource: 10 },
            { PageSizeResource: 25 },
            { PageSizeResource: 50 },

        ];
        $scope.PageSizeResource = $scope.pagesizeListResource[0];

        $scope.pagesizeListInternalUsers = [
            { PageSizeInternalUsers: 5 },
            { PageSizeInternalUsers: 10 },
            { PageSizeInternalUsers: 25 },
            { PageSizeInternalUsers: 50 },

        ];
        $scope.PageSizeInternalUsers = $scope.pagesizeListInternalUsers[0];

        $scope.totalPageCountUsers = 0;
        $scope.totalPageCountResource = 0;
        $scope.totalPageCountInternalUsers = 0;

        $scope.RoleModel = {};
        var VendorCompanyId = 0;
        var UserId;
        $scope.vendorAdminProfileExistingUserAndResourceList = [];
        $scope.vendorAdminProfileExistingUserList = [];
        $scope.vendorAdminProfileExistingResourceList = [];
        $scope.vendorAdminProfileExistingInternalUsersLists = [];
        $scope.imgURL = apiURL.imageAddress;
        $scope.isRated = true;
        UserId = sharedService.getUserId();

        $scope.jobProfileModal = {};
        $scope.Tags = {};
        $scope.Tags.Roles = [];

        $scope.openSaveRoleTags = false;
        var multiSelectPreviousData = [];
        var multiSelectCurrentData = [];

        angular.element(document).ready(function () {
            VendorCompanyId = sharedService.getCompanyId();
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }
            // getUserAndResourceVendorAdminProfile(VendorCompanyId)
            getUsersForVendorAdminProfile(VendorCompanyId);
            getResourceForVendorAdminProfile(VendorCompanyId);
            getAllExistingInternalUsers(VendorCompanyId);
        })
        /////////////////////////////////////////////////////////// function Get user and Resource
        function getUserAndResourceVendorAdminProfile(VendorCompanyId) {
            $('#globalLoader').show();

            userManagementFactory.getUserAndResourceVendorAdminProfile(VendorCompanyId)
                .success(function (data) {
                    ;
                    $('#globalLoader').hide();
                    if (data.ResponseData != null) {
                        $scope.vendorAdminProfileExistingUserAndResourceList = data.ResponseData;
                    }
                    else {
                        toaster.error("Error", message.error);
                    }

                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })
        }


        /////////////////////////////////////////////////////////////// Get Role for ExistingUserModel

        $scope.ExistingUserModel = { ExistingUserIndex: 0, VendorName: "", Rating: 0, Email: "", IsRated: true };//, Indefinitely: true, ValidUntil: false, ValidUntilDate: ""
        $scope.ExistingInternalUsersModel = {};

        /* Open modal pop up for edit */
        $scope.openExistingUserDetailVendorAdminProfileModal = function (index, item, flag) {
            debugger
            /* flag=0 means edit ExistingUsers(POCs) and flag=1 means edit existing internal users */
            if (flag == 0) {
                $scope.ExistingUserModel.ExistingUserIndex - index;
                $scope.ExistingUserModel.VendorName = item.VendorName;
                $scope.ExistingUserModel.Rating = item.Rating;
                $scope.ExistingUserModel.Email = item.Email;
                $scope.ExistingUserModel.ProfilePhoto = item.ProfilePhoto;
                $scope.ExistingUserModel.VendorId = item.VendorId;
                $scope.ExistingUserModel.Password = item.Password;
                $scope.ExistingUserModel.FirstName = item.FirstName;
                $scope.ExistingUserModel.LastName = item.LastName;
                $scope.ExistingUserModel.IsActive = item.IsActive;
                $scope.RoleModel.RoleId = item.RoleId;
                $scope.RoleModel.RoleName = item.RoleName;
                $scope.ExistingUserModel.ValidUntilDate = item.ValidUntilDate;
                $scope.ExistingUserModel.Indefinitely = item.Indefinitely;
                $scope.ExistingUserModel.ValidUntil = item.ValidUntil;
                // getExistingUserRoleVendorAdminProfile(item.VendorId);
                getExistingUserRoleVendorAdminProfile(VendorCompanyId);

            }
            else {
                console.log(item);
                $scope.ExistingInternalUsersModel.InternalUserIndex - index;
                $scope.ExistingInternalUsersModel.Name = item.name;
                $scope.ExistingInternalUsersModel.Rating = item.Rating;
                $scope.ExistingInternalUsersModel.Email = item.Email;
                $scope.ExistingInternalUsersModel.ProfilePhoto = item.ProfilePhoto;
                $scope.ExistingInternalUsersModel.Id = item.Id;
                $scope.ExistingInternalUsersModel.Password = item.Password;
                $scope.ExistingInternalUsersModel.FirstName = item.FirstName;
                $scope.ExistingInternalUsersModel.LastName = item.LastName;
                $scope.ExistingInternalUsersModel.IsActive = item.IsActive;
                $scope.jobProfileModal.Name = item.JobProfileName;
                $scope.jobProfileModal.RoleId = item.JobProfileId;
                $scope.Tags.Roles = item.UserRoleList;
                $scope.editRoleTags();
                $scope.ExistingInternalUsersModel.JobProfileId = item.JobProfileId;
                $scope.ExistingInternalUsersModel.ValidUntilDate = item.ValidUntilDate;
                $scope.ExistingInternalUsersModel.Indefinitely = item.Indefinitely;
                $scope.ExistingInternalUsersModel.ValidUntil = item.ValidUntil;
                $scope.ExistingInternalUsersModel.UserId = item.UserId;
                $scope.ExistingInternalUsersModel.CompanyId = item.CompanyId;

                getInternalUserRoleVendorAdminProfile(VendorCompanyId);
                getAllDesignation();
            }

        }


        /* Get Role for drop down for existing users (POCs) */
        function getExistingUserRoleVendorAdminProfile(VendorId) {
            $('#globalLoader').show();
            userManagementFactory.getExistingUserRoleVendorAdminProfile(VendorId)
                .success(function (data) {
                    ;
                    $('#globalLoader').hide();
                    if (data.ResponseData != null) {
                        $scope.vendorAdminProfileExistingUserRoleList = data.ResponseData;
                        $("#ExistingUserVendorAdminProfileModal_Id").modal('show')
                    }
                    else {
                        toaster.error("Error", message.error);
                    }

                }), Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })

        }

        $scope.openResourcerDetailVedorAdminProfileModal = function (item) {
            getResourceVendorAdminProfile(item)
        }

        function getResourceVendorAdminProfile(item) {
            $('#globalLoader').show();
            userManagementFactory.getResourceVendorAdminProfile(item)
                .success(function (data) {
                    ;
                    $('#globalLoader').hide();
                    if (data.ResponseData != null) {
                        $scope.resourceVendorAdminProfileList = data.ResponseData;
                        $("#ResourceVendorAdminProfileModal_Id").modal('show')
                    }
                    else {
                        toaster.error("Error", message.error);
                    }

                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })

        }

        var saveExistingUserRole = { RoleId: 0, VendorId: 0, Password: "" }

        $scope.changeExistingUserRole = function (RoleModel) {
            $scope.RoleModel = RoleModel;
            saveExistingUserRole.RoleId = RoleModel.RoleId;
        }

        $scope.radioIndefinatly = function (radioType, ExistingUserModel) {
            if (radioType == 'Indefinitely') {
                ExistingUserModel.Indefinitely = true;
                ExistingUserModel.ValidUntil = false;
            }
            else {
                ExistingUserModel.Indefinitely = false;
                ExistingUserModel.ValidUntil = true;
            }
        }

        $scope.SaveExistingUser = function (saveExistingUserRole) {
            $('#globalLoader').show();
            if (saveExistingUserRole.FirstName == "" || saveExistingUserRole.LastName == "" || $scope.RoleModel.RoleId == undefined || $scope.RoleModel.RoleId == 0 || saveExistingUserRole.Password == "" || (saveExistingUserRole.ValidUntil == true && saveExistingUserRole.ValidUntilDate == "")) {//||saveExistingUserRole.Indefinitely == true || saveExistingUserRole.ValidUntil == false || saveExistingUserRole.ValidUntilDate == ""
                $('#globalLoader').hide();
                if (saveExistingUserRole.ValidUntil == true && saveExistingUserRole.ValidUntilDate == "") {
                    toaster.error("Error", " fill valid until date");
                }
                else {
                    toaster.error("Error", "* fields are compulsory");
                }

                return false;
            }
            saveExistingUserRole.RoleId = $scope.RoleModel.RoleId;
            //saveExistingUserRole.Indefinitely = saveExistingUserRole.Indefinitely
            //saveExistingUserRole.ValidUntil = saveExistingUserRole.ValidUntil;
            //saveExistingUserRole.ValidUntilDate = saveExistingUserRole.ValidUntilDate;
            userManagementFactory.saveExistingUser(saveExistingUserRole)
                .success(function (data) {
                    ;
                    $('#globalLoader').hide();
                    if (data.ResponseData == 1) {

                        $("#ExistingUserVendorAdminProfileModal_Id").hide('hide')
                        toaster.success("Success", message.saveSccessFullyComman);
                    }

                    else {
                        toaster.error("Error", message.error);
                    }

                }),
                Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                });

        }

        // Role is assigned static for  Resource 3
        var vendorInvitationModel = { RoleId: 3, Email: "", VendorCompanyId: "" }

        $scope.inviteResourceAsVendor = function (ResourceForm, RoleSelect, Email) {
            ;
            if (RoleSelect) {
                ResourceForm.RoleSelect.$dirty = true;
                return;
            }

            $('#globalLoader').show();
            vendorInvitationModel.VendorCompanyId = VendorCompanyId;
            vendorInvitationModel.Email = Email;
            vendorInvitationModel.UserId = UserId;

            userManagementFactory.inviteResourceAsVendor(vendorInvitationModel)
                .success(function (data) {
                    ;
                    $('#globalLoader').hide();
                    if (data.ResponseData == 1) {

                        toaster.success("Success", message.saveSccessFullyComman);
                        $("#ResourceVendorAdminProfileModal_Id").modal('hide')
                    }

                    else {
                        toaster.error("Error", message.error);
                    }

                }),
                Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })


        }

        /*active inactive existing users(POCs) when flag 1 and when 0 then active inactive internal users  */
        $scope.activeInactiveUserVendorAdminProfile = function (item, flag) {
            debugger
            $('#globalLoader').show();
            if (flag == 0) {
                userManagementFactory.activeInactiveUserVendorAdminProfile(item)
                    .success(function (data) {
                        getUsersForVendorAdminProfile(VendorCompanyId);
                        $('#globalLoader').hide();
                    }),
                    Error(function () {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    })
            }
            else {
                userManagementFactory.activeInactiveInternalUsersVendorAdminProfile(item)
                    .success(function (data) {
                        getAllExistingInternalUsers(VendorCompanyId);
                        $('#globalLoader').hide();
                    }),
                    Error(function () {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    })
            }

        }

        /* Get existing users (POCs)*/
        function getUsersForVendorAdminProfile(VendorCompanyId) {
            debugger;
            $('#globalLoader').show();
            userManagementFactory.getUserForVendorAdminProfile(VendorCompanyId, $scope.pageIndexUsers, $scope.pageSizeSelectedUsers)
                .success(function (data) {
                    ;
                    $('#globalLoader').hide();
                    if (data.success && data.ResponseData != undefined && data.ResponseData.length > 0) {
                        $scope.vendorAdminProfileExistingUserList = data.ResponseData;
                        $scope.totalCountUsers = data.ResponseData[0].TotalPageCount;

                    }

                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        /*Get resources where RoleId=3 in userdetails table */
        function getResourceForVendorAdminProfile(VendorCompanyId) {
            debugger;
            $('#globalLoader').show();
            userManagementFactory.getResourceForVendorAdminProfile(VendorCompanyId, $scope.pageIndexResource, $scope.pageSizeSelectedResource)
                .success(function (data) {
                    $('#globalLoader').hide();
                    if (data.success && data.ResponseData.length > 0) {
                        console.log('if')
                        $scope.vendorAdminProfileExistingResourceList = data.ResponseData;
                        $scope.totalCountResource = data.ResponseData[0].TotalPageCount;
                    }

                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        /*paging code starts*/


        $scope.changePageSize = function (pageSizeSelected, param) {
            ;
            if (param == 'Users') {
                $scope.pageSizeSelectedUsers = pageSizeSelected.PageSizeUsers;
                getUsersForVendorAdminProfile(VendorCompanyId);

            }
            else if (param == 'Resource') {
                $scope.pageSizeSelectedResource = pageSizeSelected.PageSizeResource;
                getResourceForVendorAdminProfile(VendorCompanyId);
            }
            else if (param == 'InternalUsers') {
                $scope.pageSizeSelectedInternalUsers = pageSizeSelected.PageSizeInternalUsers;
                getAllExistingInternalUsers(VendorCompanyId);
            }

        }

        $scope.pageChanged = function (pageIndex, param) {
            if (param == 'Users') {
                $scope.pageIndexUsers = pageIndex;
                getUsersForVendorAdminProfile(VendorCompanyId);

            }
            else if (param == 'Resource') {
                $scope.pageIndexResource = pageIndex;
                getResourceForVendorAdminProfile(VendorCompanyId);
            }
            else if (param == 'InternalUsers') {
                debugger;
                $scope.pageIndexInternalUsers = pageIndex;
                getAllExistingInternalUsers(VendorCompanyId);
            }
        }

        /* paging code ends */

        $scope.inputType = 'password';
        //change type
        $scope.changeType = function () {
            ;
            if ($scope.inputType == 'password') {
                $scope.inputType = 'text';
            }
            else {
                $scope.inputType = 'password';
            }
        }



        ////////////Date Functions/////////////
        /************************/
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        var da = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
        };

        $scope.toggleMin();

        $scope.maxDate = new Date(da.getFullYear(), 5, 22);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,

        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup2 = {
            opened: false
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        /*Get all internal users invited from invite new users option*/
        function getAllExistingInternalUsers(VendorCompanyId) {
            $('#globalLoader').show();
            userManagementFactory.getAllExistingInternalUsers(VendorCompanyId, $scope.pageIndexInternalUsers, $scope.pageSizeSelectedInternalUsers)
                .success(function (data) {
                    $('#globalLoader').hide();
                    if (data.success && data.ResponseData != null) {
                        $scope.vendorAdminProfileExistingInternalUsersLists = data.ResponseData;
                        $scope.totalCountInternalUsers = data.ResponseData[0].TotalPageCount;
                        console.log(data.ResponseData);
                    }
                    else {
                        toaster.error("Error", message.error);
                    }
                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        /*Get All Designation or Job Profiles  i.e where type in (2,3) means its job profile or both*/
        function getAllDesignation() {
            $('#globalLoader').show();
            teamFactory.getAllDesignation()
                .success(function (data) {
                    $scope.getAllDesignationList = data.ResponseData;
                    $('#globalLoader').hide();

                })
        }

        /*Get all roles of a signed in company i.e where type in (1,3) means its role or both */
        function getInternalUserRoleVendorAdminProfile(VendorCompanyId) {
            debugger;
            vendorFactory.GetNewUserRole(VendorCompanyId)
                .success(function (data) {
                    $scope.getUserRoleList = data.ResponseData;
                    $("#ExistingInternalUserVendorAdminProfileModal").modal('show')

                })
        }



        /*  This function is used to edit Role Tags  */
        $scope.editRoleTags = function () {
            debugger;
            multiSelectCurrentData = $scope.Tags.Roles;
            for (var i = 0; i < multiSelectCurrentData.length; i++) {
                multiSelectPreviousData.push(multiSelectCurrentData[i]);
            }

            $scope.openSaveRoleTags = true;
            $timeout(function () {
                $('#RoleTags  .host .tags .input').focus();
            }, 100);
        }

        /*Set values for job profile on selection from drop down*/
        $scope.setJobProfileModal = function (jobProfileModal) {
            $scope.jobProfileModal.RoleId = jobProfileModal.RoleId;
            $scope.jobProfileModal.Name = jobProfileModal.Name;
        }

        /*Code to update internal user info*/
        $scope.UpdateExistingInternalUsers = function (isFirstName, IsLastName, IsEmail, InternalUserForm, ExistingInternalUsersModel) {

            debugger;
            if (isFirstName || IsLastName || IsEmail) {

                NewUserForm.FirstName.$dirty = true
                NewUserForm.LastName.$dirty = true
                NewUserForm.EmailAddress.$dirty = true
                return;
            }

            $scope.ExistingInternalUsersModel.JobProfileId = $scope.jobProfileModal.RoleId;
            $scope.ExistingInternalUsersModel.JobProfileName = $scope.jobProfileModal.Name;
            $scope.ExistingInternalUsersModel.UserRoleList = $scope.Tags.Roles;

            console.log($scope.ExistingInternalUsersModel);
            $('#globalLoader').show();
            vendorFactory.updateExistingInternalUsers($scope.ExistingInternalUsersModel)
                .success(function (data) {
                    $('#globalLoader').hide();
                    if (data.ResponseData != null) {
                        if (data.ResponseData.Id == -1) {

                            toaster.warning("Warning", message.InvitationError)
                            InternalUserForm.FirstName.$dirty = false;
                            InternalUserForm.LastName = false;
                        }
                        else {
                            toaster.success("Success", message.InvitationToUser);
                        }
                    }
                    else {
                        toaster.error("Error", "Some Error Occured !");
                    }
                    $scope.ExistingInternalUsersModel = {}
                    $scope.jobProfileModal = {};
                    $scope.userdata = {};
                    $('#ExistingInternalUserVendorAdminProfileModal').modal('hide');
                    getAllExistingInternalUsers(VendorCompanyId);
                }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })



        }
    })

})