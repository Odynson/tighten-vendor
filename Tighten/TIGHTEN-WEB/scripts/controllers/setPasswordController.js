﻿
define(['app'], function (app) {
    //registrationFactory is the factory for the registration
    app.controller('setPasswordController', function ($scope, $rootScope, toaster, $timeout, $http, $location,sharedService,$cookieStore, setPasswordFactory, $stateParams) {
        //$rootScope.navbar = true;
        $scope.user = {};
        $scope.user.pass = '';
        $scope.user.confirmPass = '';
        var setPasswordObj = {};

        $scope.Confirm = false;

        // getting the parameters from the url
        var checkEmailConfirmation = function (setPasswordObj) {
            // email confirmtion service to save the user detail
            setPasswordFactory.setPassword(setPasswordObj, function (response) {
                //
                //if (response.success) {
                //$('#confirmation').dialog('open');
                $scope.message = response.message;
                $scope.Confirm = true;
                $('.panel-body').show();
                if (setPasswordObj.UserId != sharedService.getUserId()) {
                    $cookieStore.put('globals', {});
                    $cookieStore.put('sharedService', {});
                    sharedService.setShared("");
                    //$location.path('/login');
                }
                //}
            });
        };


        $scope.setPassword = function (user) {
            
            if ($scope.user.pass == '' || $scope.user.pass == undefined) {
                toaster.warning("Warning", message["emptyPasswordField"]);
            }
            else if ($scope.user.pass != $scope.user.confirmPass) {
                toaster.warning("Warning", message["passwordAndConfirmPasswordNotMatched"]);
            }
            else if ($scope.user.pass != '' && $scope.user.pass != undefined && ($scope.user.pass == $scope.user.confirmPass)) {
                setPasswordObj.NewPassword = $scope.user.pass;
                checkEmailConfirmation(setPasswordObj);
            }
            

        }


        /*Password Key Up Event */
        $scope.passwordkeyUp = function () {
            if ($scope.user.pass == '') {
                $('#Password').addClass('inputError');
            }
            else {
                $('#Password').removeClass('inputError');
            }
        };

        /*Confirm Password Key Up Event */
        $scope.confirmPasswordkeyUp = function () {
            if ($scope.user.confirmPass == '') {
                $('#confirmPassword').addClass('inputError');
            }
            else {
                $('#confirmPassword').removeClass('inputError');
            }
        };


        // method called on page load
        angular.element(document).ready(function () {

            setPasswordObj.UserId = $stateParams.userId;
            setPasswordObj.Code = $stateParams.emailCode;
            $scope.formType = $stateParams.formType;
            setPasswordObj.formType = $scope.formType;

            setPasswordFactory.chkLinkExpiry(setPasswordObj, function (response) {

                $scope.message = response.message;
                $scope.Confirm = true;
                $('.panel-body').show();

            });



        });





    });
});