﻿
define(['app'], function (app) {

    app.controller("settingsController", function ($scope, $rootScope, $http, $timeout, sharedService, toaster, apiURL, $location, sharedService, settingsFactory) {

        $scope.projectSettingObject = [];
        $scope.customizeProject = false;

        $scope.todoSettingObject = [];
        $scope.customizeTodo = false;
        $scope.VendorAreaListModal = []
        $scope.VendorMainSettingsModel = [];
        //$scope.VendorMainSettingsModel = [
        //    {
        //        VendorAreaList: [{ Id: 0, ServiceName: "", }],
        //        CompanyId: $scope.selectedCompanymodel
        //    }
        //]

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.UserId = sharedService.getUserId();
        $scope.isFreelancer = sharedService.getIsFreelancer();

        $scope.selectedCompanymodel = sharedService.getCompanyId();
        $scope.selectedCompanyId = $scope.selectedCompanymodel;
        $scope.selectedProjectId = 0;

        //bindProjectsForFreelancer($scope.selectedCompanyId);


        // Company dropdown change event on left side menu for freelancer 
        $scope.$on('leftCompanyDropdownChange', function (event, data) {

            $scope.selectedCompanyId = data.CompanyId;
            $scope.selectedCompanymodel = data.CompanyId;
            //bindProjectsForFreelancer($scope.selectedCompanyId);
            $scope.customizeProject = false;
            $scope.customizeTodo = false;
            showAllSettingsOptions();

        });

        $scope.selectedCompanyChanged = function (selectedCompany) {

            $scope.selectedCompanyId = selectedCompany;
            //bindProjectsForFreelancer($scope.selectedCompanyId);
            $scope.customizeProject = false;
            $scope.customizeTodo = false;
            showAllSettingsOptions();

        };

        $scope.selectedProjectChanged = function (selectedProject) {
            $scope.selectedProjectId = selectedProject;
            showAllSettingsOptions();
        };




        function bindProjectsForFreelancer(compId) {

            settingsFactory.getProjectsForCurrentCompany(sharedService.getUserId(), compId)
                .success(function (data) {
                    if (data.success) {
                        $scope.ProjectsObjectForFreelancer = data.ResponseData;
                        if (data.ResponseData != null && data.ResponseData != undefined) {
                            if (data.ResponseData.length > 0) {
                                $scope.selectedProjectId = data.ResponseData[0].ProjectId;
                                $scope.selectedProject = data.ResponseData[0].ProjectId;
                                showAllSettingsOptions();
                            }
                            else {
                                $scope.selectedProjectId = 0;
                                $scope.selectedProject = 0;
                                showAllSettingsOptions();
                            }
                        }
                        else {
                            $scope.selectedProjectId = 0;
                            showAllSettingsOptions();
                        }
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        };


        $scope.IsVendor = false;
        $scope.openSaveVendorAreas = false;

        $scope.changeAllVendorSetting = function (IsVendor) {

            debugger
            settingsFactory.changeVendorSetting($scope.selectedCompanymodel, IsVendor)
            .success(function (data) {
                $scope.IsVendor = data.ResponseData.IsVendor
                $scope.allWrokingAreasObject = data.ResponseData.VendorAreaList;
            })



        }





        function getVendorSelectedAreas(CompanyId) {
            debugger;
            settingsFactory.getVendorArea(CompanyId)
            .success(function (data) {
                $scope.VendorAreaList = data.ResponseData.VendorAreaList;
                //$scope.allWrokingAreasObject = data.ResponseData.VendorAreaList;
                $scope.VendorAreaListModal = data.ResponseData.VendorAreaList
                $scope.openSaveVendorAreas = false;

            })
        }

        $scope.saveVendorWorkingArea = function () {
            debugger;

     
                var data = { VendorAreaList: $scope.VendorAreaListModal, CompanyId: $scope.selectedCompanymodel }

                settingsFactory.saveVendorWorkingArea(data)
                 .success(function (data) {
                     debugger;
                     $scope.VendorAreaList = data.ResponseData.VendorAreaList;

                     $scope.openSaveVendorAreas = false;
                 })
            

        }





        $scope.changeEmailSetting = function (currentObject) {
            debugger;
            currentObject.UserId = $scope.UserId;
            settingsFactory.changeEmailSettingsOption(currentObject)
            .success(function (dataEmailSettingsOptionChange) {
                if (dataEmailSettingsOptionChange.success) {
                    $scope.customizeProject = false;
                    $scope.customizeTodo = false;
                    toaster.success("Success", dataEmailSettingsOptionChange.ResponseData);
                }
                else {
                    toaster.error("Error", dataEmailSettingsOptionChange.message);
                }
            })
            .error(function (dataEmailSettingsOptionChange) {
                toaster.error("Error", dataEmailSettingsOptionChange.message);
            });

        }


        $scope.changeAllProjectSetting = function (IsUserNotificationActive) {

            var currentObject = {
                UserId: $scope.UserId, CompanyId: $scope.selectedCompanyId, IsUserNotificationActive: IsUserNotificationActive
            };
            settingsFactory.changeAllProjectSetting(currentObject)
            .success(function (data) {
                if (data.success) {
                    $scope.customizeProject = false;
                    $scope.projectSettingObject.ActiveProjects = data.ResponseData.ActiveProjects;

                    var projectMembers = [];
                    if ($scope.projectSettingObject.ActiveProjects != null) {
                        if ($scope.projectSettingObject.ActiveProjects.length > 0) {
                            for (var i = 0; i < $scope.projectSettingObject.ActiveProjects.length; i++) {
                                projectMembers.push($scope.projectSettingObject.ActiveProjects[i].ProjectId)
                            }
                        }
                    }
                    $scope.projectSettingObject.ActiveProjects = projectMembers;

                    toaster.success("Success", "Settings for project update is changed Successfully !!");
                }
                else {
                    toaster.error("Error", data.message);
                }
            })
            .error(function (data) {
                toaster.error("Error", data.message);
            });

        }



        $scope.changeAllTodoSetting = function (IsUserNotificationActive) {

            var currentObject = {
                UserId: $scope.UserId, CompanyId: $scope.selectedCompanyId, IsUserNotificationActive: IsUserNotificationActive
            };
            settingsFactory.changeAllTodoSetting(currentObject)
            .success(function (data) {
                if (data.success) {
                    $scope.customizeTodo = false;
                    $scope.todoSettingObject.ActiveTodos = data.ResponseData.ActiveTodos;

                    var todoMembers = [];
                    if ($scope.todoSettingObject.ActiveTodos != null) {
                        if ($scope.todoSettingObject.ActiveTodos.length > 0) {
                            for (var i = 0; i < $scope.todoSettingObject.ActiveTodos.length; i++) {
                                todoMembers.push($scope.todoSettingObject.ActiveTodos[i].TodoId)
                            }
                        }
                    }
                    $scope.todoSettingObject.ActiveTodos = todoMembers;

                    toaster.success("Success", "Settings for todo updation is updated Successfully !!");
                }
                else {
                    toaster.error("Error", data.message);
                }
            })
            .error(function (data) {
                toaster.error("Error", data.message);
            });

        }



        function showAllSettingsOptions() {

            settingsFactory.getAllEmailSettingsOption($scope.UserId, $scope.selectedCompanyId, $scope.selectedProjectId, sharedService.getRoleId())
            .success(function (data) {
                if (data.success) {
                    $scope.emailObject = data.ResponseData;
         
                }
                else {
                    toaster.error("Error", data.message);
                }
            })
                .error(function (data) {
                    toaster.error("Error", data.message);
                });


            settingsFactory.getEmailSettingsForProjectLevel($scope.UserId, $scope.selectedCompanyId)
            .success(function (data) {
                if (data.success) {
                    $scope.projectSettingObject = data.ResponseData;

                    var projectMembers = [];
                    if ($scope.projectSettingObject.ActiveProjects != null) {
                        if ($scope.projectSettingObject.ActiveProjects.length > 0) {
                            for (var i = 0; i < $scope.projectSettingObject.ActiveProjects.length; i++) {
                                projectMembers.push($scope.projectSettingObject.ActiveProjects[i].ProjectId)
                            }
                        }
                    }
                    $scope.projectSettingObject.ActiveProjects = projectMembers;

                }
                else {
                    toaster.error("Error", data.message);
                }
            })
                .error(function (data) {
                    toaster.error("Error", data.message);
                });


            settingsFactory.getEmailSettingsForTodoLevel($scope.UserId, $scope.selectedCompanyId)
            .success(function (data) {
                if (data.success) {
                    $scope.todoSettingObject = data.ResponseData;

                    var todoMembers = [];
                    if ($scope.todoSettingObject.ActiveTodos.length > 0) {
                        for (var i = 0; i < $scope.todoSettingObject.ActiveTodos.length; i++) {
                            todoMembers.push($scope.todoSettingObject.ActiveTodos[i].TodoId)
                        }
                    }
                    $scope.todoSettingObject.ActiveTodos = todoMembers;

                }
                else {
                    toaster.error("Error", data.message);
                }
            })
            .error(function (data) {
                toaster.error("Error", data.message);
            });



        };







        $scope.ProjectLevelSetting = function () {
            $scope.customizeProject = !$scope.customizeProject;
            $scope.customizeTodo = false;
        }

        $scope.TodoLevelSetting = function () {
            $scope.customizeTodo = !$scope.customizeTodo;
            $scope.customizeProject = false;
        }





        $scope.projectConfig = {
            create: false,
            // maxItems: 1,
            required: true,
            plugins: ['remove_button'],
            valueField: 'ProjectId',
            labelField: 'ProjectName',
            // delimiter: '|',
            placeholder: 'Pick Projects...',
            onChange: function ($dropdown) {

                var currentObject = { UserId: $scope.UserId, CompanyId: $scope.selectedCompanyId, UserSelectedProjects: $scope.projectSettingObject.ActiveProjects };
                settingsFactory.changeProjectSetting(currentObject)
                .success(function (data) {
                    if (data.success) {
                        toaster.success("Success", data.ResponseData);
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function (data) {
                    toaster.error("Error", data.message);
                });

            },
            onDropdownClose: function () {
                $timeout(function () {
                    showAllSettingsOptions()
                }, 1000)

            }


        }


        $scope.todoConfig = {
            create: false,
            // maxItems: 1,
            required: true,
            plugins: ['remove_button'],
            valueField: 'TodoId',
            labelField: 'TodoName',
            // delimiter: '|',
            placeholder: 'Pick Todos...',
            onChange: function ($dropdown) {

                var currentObject = { UserId: $scope.UserId, CompanyId: $scope.selectedCompanyId, UserSelectedTodos: $scope.todoSettingObject.ActiveTodos };
                settingsFactory.changeTodoSetting(currentObject)
                .success(function (data) {
                    if (data.success) {
                        toaster.success("Success", data.ResponseData);
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
            .error(function (data) {
                toaster.error("Error", data.message);
            });

            },
            onDropdownClose: $timeout(function () {
                showAllSettingsOptions()
            }, 1000)


        }


        angular.element(document).ready(function () {
            //showAllSettingsOptions();

        

        });




    });

});