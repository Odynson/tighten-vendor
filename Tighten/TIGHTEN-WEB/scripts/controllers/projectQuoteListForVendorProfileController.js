﻿
define(['app'], function (app) {


    app.controller("projectQuoteListForVendorProfileController", function ($scope, $rootScope, apiURL, $location, vendorFactory, sharedService, toaster, $timeout, $http, $sce) {

        var baseurl = apiURL.baseAddress;
        var CompanyId = 0;
        var dataModal = {}
        var userId = 0
        var VendorId = 0
        $scope.searchText = ''
        $scope.qouteSendToCompanyDropList = [];
        $scope.getProjectTOQuoteList = []; 0

        //pagination
        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 4;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.
       
        //pagination

        $scope.qouteSentToCompanyModal = { CompanyId: 0, Name: "--Select All--" }

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');

            }
            CompanyId = sharedService.getCompanyId();
            userId = sharedService.getUserId();
            VendorId = sharedService.getVendorId();
            dataModal = { Id: 0, CompanyId: 0, Name: '', VendorId: VendorId, ProjectId: 0, RequestType: 0, userId: userId, PageSize: 5, PageIndex: 1 };

            getCompanyForFilterDrop(CompanyId)


        })

        function getCompanyForFilterDrop(CompanyId) {
   
            vendorFactory.getCompanyForDropFilter(CompanyId)
            .success(function (data) {
                if (data.success) {
                    $scope.qouteSendToCompanyDropList = data.ResponseData;
                    $scope.qouteSendToCompanyDropList.push($scope.qouteSentToCompanyModal);
                    getProjectTOQuote();//binding Project to quote  list of recode
                }
            }), Error(function () {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();
            })
        }



        function getProjectTOQuote() {
            debugger;

            $('#globalLoader').show();
            vendorFactory.getProjectTOQuote(dataModal)
                .success(function (data) {
                    debugger;
                    $scope.getProjectTOQuoteList = data.ResponseData;
                    if (data.ResponseData != null && data.ResponseData.length > 0) {
                        $scope.totalCount = data.ResponseData[0].TotalPageCount;
                    }
                    $('#globalLoader').hide();
                }),
            Error(function () {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();
            })

        }


        $scope.qouteSendDropList = [
            { Id: 0, Status: '--Select All --' },
            { Id: 3, Status: 'given' },
            { Id: 2, Status: 'pending' },
            { Id: 1, Status: 'rejected' }

        ]

        $scope.qouteSendStatusDropModal = $scope.qouteSendDropList[0]

        $scope.qouteSendDropModal = { Id: 0, Status: '--Select All --' }



        $scope.FilterByQuotedSendToCompany = function (qouteSentToCompanyModal) {

            dataModal.CompanyId = qouteSentToCompanyModal.CompanyId;
            getProjectTOQuote();

        }

        $scope.FilterByQuotedStatusSend = function (qouteSendStatusDropModal) {
          
            dataModal.Id = qouteSendStatusDropModal.Id;
            getProjectTOQuote();
        }


        $scope.searchProjectByName = function (searchText) {
   

            if (searchText != undefined && searchText.length > 3) {
                dataModal.Name = searchText;
                getProjectTOQuote();

            }
        }

    

        $scope.autoCompleteOptions = {

            minimumChars: 3,
            dropdownWidth: '500px',
            dropdownHeight: '200px',
            data: function (term) {
                return $http.post(baseurl + 'ProjectsApi/GetProjectName',
                    dataModal = { Id: 0, CompanyId: 0, Name: term, VendorId: CompanyId, ProjectId: 0, RequestType: 0 })
                    .then(function (data) {
                        
                        return data.data.ResponseData;

                    });
            },
            renderItem: function (item) {
                return {
                    value: item.Name,
                    label: $sce.trustAsHtml(
                    "<p class='auto-complete'>"
                    + item.Name +
                    "</p>")
                };
            },
            itemSelected: function (item) {
                
                $scope.qouteSentToProjectNameModal = item.item.Name;
                dataModal.ProjectId = item.item.ProjectId
                getProjectTOQuote();
            }
        }


        /////////////////////////////empty text box project to quote companyProfile//////////////////////////////////////////////////////
        $scope.searchProjectNameProjectToQuote = function (qouteSentToProjectNameModal, event) {

            
            if (event.keyCode == 46) {
                qouteSentToProjectNameModal = "p";

            }
            if (qouteSentToProjectNameModal != undefined && qouteSentToProjectNameModal.length <= 1 && (event.keyCode == 8 || event.keyCode == 46)) {

                dataModal.CompanyId = 0;
                dataModal.Name = "";
                dataModal.ProjectId = 0
                getProjectTOQuote();
            }



        }

        var isCtrlPressed;
        var isAPressed;

        /////////////////////////////empty text box project to quote companyProfile//////////////////////////////////////////////////////
        $scope.searchProjectNameProjectToQuote = function (qouteSentToProjectNameModal, event) {
           
            if (qouteSentToProjectNameModal != undefined && qouteSentToProjectNameModal.length <= 1 && event.keyCode == 8) {
                dataModal.Name = "";
                dataModal.ProjectId = 0
                getProjectTOQuote();
            }
            // if key is pressed
            if (event.keyCode == 17 || isCtrlPressed) {
                isCtrlPressed = true;
            }
            else {
                isCtrlPressed = false;
            }

            // other key after the control
            if (isCtrlPressed) {
                if (event.keyCode == 97 || event.keyCode == 65) {
                    isAPressed = true;
                }
            }
            else {
                isAPressed = false;
            }


            if (isCtrlPressed && isAPressed) {
                if (event.keyCode == 46 || event.keyCode == 8) {
                    dataModal.Name = "";
                    dataModal.ProjectId = 0
                    getProjectTOQuote();
                    isCtrlPressed = false;
                    isAPressed = false;
                }

            }
            var text = window.getSelection().toString()
            if (text != "" && (event.keyCode == 8 || event.keyCode == 46)) {
                dataModal.Name = "";
                dataModal.ProjectId = 0
                getProjectTOQuote();
            }
        }


        $scope.cutSearchProjectToQuoteProjectName = function () {
            dataModal.Name = "";
            dataModal.ProjectId = 0
            getProjectTOQuote();

        }
        ////////////////////////////////////------------paging code/////////////////////////////////
        $scope.pagesizeList = [
       
       { PageSize: 5 },
       { PageSize: 10 },
       { PageSize: 25 },
       { PageSize: 50 },

        ];


        $scope.PageSize = $scope.pagesizeList[0];

        $scope.changePageSize = function (pageSizeSelected) {
            debugger;
            dataModal.PageSize = pageSizeSelected.PageSize;
            $scope.pageSizeSelected = pageSizeSelected.PageSize;
            getProjectTOQuote();
        }

        $scope.pageChanged = function (pageIndex) {
          
            dataModal.PageIndex = pageIndex;
            getProjectTOQuote();
        }


        ////////////////////////////////////------------paging code/////////////////////////////////



    })


});

