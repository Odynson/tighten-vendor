﻿define(['app'], function (app) {
    app.controller("invoiceRaiseController", function ($scope, $rootScope, apiURL, $stateParams, $location, vendorFactory, invoiceFactory, vendorInvoiceFactory, sharedService, toaster,projectsFactory,worklogFactory) {

        $scope.imgURL = apiURL.imageAddress;
        var UserId = sharedService.getUserId();
        var ProjectId = 0;
        var CompanyId;
        var UserId;
        var VendorId;
        $scope.worklogList = [];
        $scope.projectId = 0;
        $scope.finalTotalHrs = 0;
        angular.element(document).ready(function () {
            $scope.format = 'MM/dd/yyyy';

            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }
            //ProjectId = $stateParams.pid;
            CompanyId = sharedService.getCompanyId()
            UserId = sharedService.getUserId();
            //alert(UserId);
            VendorId = sharedService.getVendorId()
            debugger;
            if (ProjectId == 0 || ProjectId == "") {
                getApprovedBillableProjectByUserId();
                //getProjectForInvoiceRaise(0, VendorId)
            }
            else {
                getSpecificProjectForInvoiceRaise(ProjectId, VendorId)
            }
        })


        $scope.SelectProject = {};
        $scope.selectedCompany = {};
        $scope.disbleDropDown = false;


        $scope.getProjectInvoiceList = [];

        $scope.InvoiceToExtraResourceList = [{Description: "",Amount: ""}]
        $scope.getMilestoneProjectList = [];
        $scope.SearchInvoiceVendorProfile = function () {
            debugger
            if ($scope.invoiceSendModal.fromDate == undefined || $scope.invoiceSendModal.fromDate == null || $scope.invoiceSendModal.fromDate == ""
                || $scope.invoiceSendModal.toDate == undefined || $scope.invoiceSendModal.toDate == null || $scope.invoiceSendModal.toDate == ""
                || $scope.SelectProject.Id == undefined || $scope.SelectProject.Id == null || $scope.SelectProject.Id == ""
                ) {
                $scope.invoiceSendModal.fromDate.$dirty = true;
                $scope.invoiceSendModal.toDate.$dirty = true;
                $scope.SelectProject.$dirty = true;
                toaster.error("Warning", "Please select Project, From and To date for search worklogs");
                return;
            }
                $('#globalLoader').show();
            worklogFactory.GetWorkLogListByProjectFromTo(UserId, $scope.SelectProject.Id, moment($scope.invoiceSendModal.fromDate).format("DD-MM-YYYY"), moment($scope.invoiceSendModal.toDate).format("DD-MM-YYYY"))
                .success(function (data) {
                    debugger;
                    if (data.success) {
                        $scope.worklogList = data.ResponseData;
                    }
                    else {
                        toaster.error("Error", message.error)
                    }
                    $('#globalLoader').hide();
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }
        $scope.ChangeTotalAmount = function (Amount) {
            if(Amount!=undefined && Amount!=null && Amount!=""){
                $scope.finalTotalHrs = 0;
                for(var i=0;i<$scope.worklogList.length;i++){
                    $scope.finalTotalHrs += $scope.worklogList[i].TotalHrs;
                }
                $scope.finalTotalAmount = $scope.finalTotalHrs * Amount;
            }
        }
        
        $scope.invoiceSendModal = {
            milestoneList: [], invoiceNumber: "", sendTo: "", companyId: "", fromDate: "", toDate: "", totalHours: "", totalAmount: "", message: "",
            MilestoneId: "",
            phaseId: "",

        }
        var dataModal = {
            InvoiceNumber: "", UserId: "", SendTo: "", CompanyId: "", FromDate: "", ToDate: "", TotalHours: "", TotalAmount: "", Message: "",
            ProjectId: ProjectId,
            VendorId: VendorId,
            PhaseId: "",
            MilestoneId: "",
            InvoiceForResourceList: [],
            InvoiceToMilestoneList: []
        }

        $scope.previewInvoiceModal = {
            companyName: "",
            invoiceNo: "",
            vendorCompanyName: "",
            vendorUserName: "",
            CompanyProfilePic: "",
            currentdate: moment(new Date()).format("MM/DD/YYYY"),

        };

        var InvoiceApprovedStatus, IsAccountSetByVendorPoc;

        $scope.popup = {
            opened1: false, opened2: false
        };

        $scope.open1 = function () {
            $scope.popup.opened1 = true;

        };

        $scope.open2 = function () {
            $scope.popup.opened2 = true;
            $scope.mindate = new Date($scope.invoiceSendModal.fromDate);
        };


        function getApprovedBillableProjectByUserId() {
            debugger;
            $('#globalLoader').show();
            
            projectsFactory.getApprovedBillableProjectByUserId(UserId)
                .success(function (data) {
                    if (data.success) {
                        debugger;
                        $scope.getProjectInvoiceList.InvoiceRaiseToProjectList = data.ResponseData;
                        
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                    $('#globalLoader').hide();
                })
                , Error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                })

        }


        //////////////////////////////////////////GetSpecific Project/////////////////////////////////////////////////////////////////////////
        function getSpecificProjectForInvoiceRaise(ProjectId, VendorId) {
            debugger;
            $('#globalLoader').show();
            vendorFactory.getProjectForInvoiceRaise(ProjectId, VendorId)
            .success(function (data) {
                if (data.success) {
                    debugger;
                    $scope.getProjectInvoiceList = data.ResponseData;
                    $scope.SelectProject = data.ResponseData.InvoiceRaiseToProjectList[0];
                    $scope.selectedCompany = data.ResponseData.InvoiceRaiseToCompanyList[0];
                    $scope.invoiceSendModal.invoiceNumber = data.ResponseData.InvoiceRaiseToProjectList[0].InvoiceNumber;
                    $scope.invoiceSendModal.companyId = data.ResponseData.InvoiceRaiseToCompanyList[0].Id;
                    $scope.invoiceSendModal.sendTo = data.ResponseData.InvoiceRaiseToCompanyList[0].UserId;
                    $scope.disbleDropDown = true;
                    /////////////////////////////preview/////////////////////////////
                    $scope.previewInvoiceModal.companyName = data.ResponseData.InvoiceRaiseToCompanyList[0].Name;
                    $scope.previewInvoiceModal.invoiceNo = data.ResponseData.InvoiceRaiseToProjectList[0].InvoiceNumber;
                    $scope.previewInvoiceModal.vendorCompanyName = data.ResponseData.InvoiceRaiseToProjectList[0].VendorCompanyName;
                    $scope.previewInvoiceModal.vendorUserName = data.ResponseData.InvoiceRaiseToProjectList[0].VendorUserName;
                    $scope.previewInvoiceModal.companyname = data.ResponseData.InvoiceRaiseToCompanyList[0].Name;
                    $scope.previewInvoiceModal.CompanyProfilePic = data.ResponseData.InvoiceRaiseToCompanyList[0].CompanyProfilePic
                    $scope.previewInvoiceModal.CompanyLogo = data.ResponseData.InvoiceRaiseToCompanyList[0].CompanyLogo
                    
                    InvoiceApprovedStatus = data.ResponseData.InvoiceRaiseToCompanyList[0].InvoiceApprovedStatus;
                    IsAccountSetByVendorPoc = data.ResponseData.InvoiceRaiseToCompanyList[0].IsAccountSetByVendorPoc;
                    if (data.ResponseData.InvoiceRaiseToCompanyList[0].IsAccountSetByVendorPoc == false) {
                        toaster.warning("Warning", "Reset your Account before Raise the invoice");
                    }
                    //if (data.ResponseData.InvoiceRaiseToCompanyList[0].InvoiceApprovedStatus == false) {
                    //    toaster.warning("Warning", "Reset your Account before Raise the invoice");
                    //}

                    toaster.success("Success", message.successdataLoad);
                }
                else {
                    toaster.error("Error", message.error)
                }
                $('#globalLoader').hide();

            }), Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();
            })
        }




        //////////////////////////////////////////Get All Project/////////////////////////////////////////////////////////////////////////
        function getProjectForInvoiceRaise(ProjectId, VendorId) {
            $('#globalLoader').show();
            vendorFactory.getProjectForInvoiceRaise(ProjectId, VendorId)
            .success(function (data) {
                if (data.success) {
                    debugger;
                    $scope.invoiceSendModal.invoiceNumber = data.ResponseData.InvoiceRaiseToCompanyList[0].InvoiceNumber;

                    $scope.getProjectInvoiceList = data.ResponseData;


                    ////////////////////////////////////////preview invoice////////////////////////////////////////////////////////////////////

                    $scope.previewInvoiceModal.invoiceNo = $scope.getProjectInvoiceList.InvoiceRaiseToCompanyList[0].InvoiceNumber;
                    $scope.previewInvoiceModal.vendorCompanyName = $scope.getProjectInvoiceList.InvoiceRaiseToCompanyList[0].VendorCompanyName;
                    $scope.previewInvoiceModal.vendorUserName = $scope.getProjectInvoiceList.InvoiceRaiseToCompanyList[0].VendorUserName;
                    $scope.getProjectInvoiceList.InvoiceRaiseToCompanyList = $scope.getProjectInvoiceList.InvoiceRaiseToCompanyList;
                    $scope.previewInvoiceModal.CompanyProfilePic = data.ResponseData.InvoiceRaiseToCompanyList[0].CompanyProfilePic;
                    $scope.previewInvoiceModal.CompanyLogo = data.ResponseData.InvoiceRaiseToCompanyList[0].CompanyLogo

                    //if (data.ResponseData.InvoiceRaiseToCompanyList[0].InvoiceApprovedStatus == false) {
                    //    toaster.warning("Warning", "Reset your Account before Raise the invoice");
                    //}
                    if (data.ResponseData.InvoiceRaiseToCompanyList[0].IsAccountSetByVendorPoc == false) {
                        toaster.warning("Warning", "Reset your Account before Raise the invoice");
                    }
                    IsAccountSetByVendorPoc = data.ResponseData.InvoiceRaiseToCompanyList[0].IsAccountSetByVendorPoc;
                    InvoiceApprovedStatus = data.ResponseData.InvoiceRaiseToCompanyList[0].InvoiceApprovedStatus;
                    $scope.disbleDropDown = false;
                    toaster.success("Success", message.successdataLoad);
                }
                else {
                    toaster.error("Error", message.error)
                }
                $('#globalLoader').hide();
            }), Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();
            })
        }

        ////////////////////////////////////////select Phase data from serverside/////////////////////////////////////////////////////////////////////
        $scope.changeInvoicePhase = function (selectedPhase) {
            debugger
            if (selectedPhase != null) {
                $('#globalLoader').show();
                $scope.invoiceSendModal.phaseId = selectedPhase.Id
                vendorFactory.getMilestoneforProject(selectedPhase.Id)
                .success(function (data) {
                    if (data.success) {
                        $scope.MilestoneList = data.ResponseData;
                        $scope.milestoneDetailList = [] // push data in milestone for grid new for new phase
                        toaster.success("Success", message.successdataLoad);
                    }
                    else {
                        toaster.error("Error", message.error)
                    }
                    $('#globalLoader').hide();
                }), Error(function () {
                    toaster.error("Error", message.error)
                    $('#globalLoader').hide();
                })
            }
        }
        ///////////////////////////////////////////////////////changeMilestone////////////////////////////////////////////////////////////////////////////////

        $scope.milestoneDetailList = [];

        $scope.changeMilestone = function (selectedMilestone) {
            debugger;
            if (selectedMilestone)

                if ($scope.milestoneDetailList.length == 0) {
                    $scope.milestoneDetailList.push(selectedMilestone);
                }
                else {
                    var IsExist = true;
                    MilestonechecckforInvoice(selectedMilestone);
                    function MilestonechecckforInvoice(selectedMilestone) {
                        $scope.milestoneDetailList.forEach(function (item) {
                            debugger;
                            if (item.Id == selectedMilestone.Id) {
                                IsExist = false;
                            }
                        });
                    }
                    if (IsExist)
                        $scope.milestoneDetailList.push(selectedMilestone);

                }

        }


        ///////////////////////////////////////////////////ChangeProject///////////////////////////////////////////////////////////////////////////////////////
        $scope.ChangeProjectDrop = function (SelectProject) {
            debugger;
            if (SelectProject == undefined) {
                return
            }
            //alert(SelectProject.Id);
            $('#globalLoader').show();
            $scope.invoiceSendModal.ProjectId = SelectProject.Id;
            getSpecificProjectForInvoiceRaise(SelectProject.Id, VendorId);
            //vendorInvoiceFactory.getPhaseAndResourceforProject(SelectProject.Id, VendorId)
            //.success(function (data) {
            //    if (data.success) {
            //        debugger;
            //        $scope.getProjectInvoiceList.InvoiceToPhaseList = data.ResponseData.InvoiceToPhaseList;
            //        $scope.invoiceSendModal.sendTo = data.ResponseData.InvoiceRaiseToCompanyList[0].UserId;
            //        $scope.selectedCompany = data.ResponseData.InvoiceRaiseToCompanyList[0];
            //        $scope.invoiceSendModal.companyId = data.ResponseData.InvoiceRaiseToCompanyList[0].Id;
            //        $scope.getProjectInvoiceList.InvoiceForResourceList = data.ResponseData.InvoiceForResourceList;
            //        ///////////////////////////////////for preview////////////////////////////////////////////////////
            //        $scope.previewInvoiceModal.companyname = data.ResponseData.InvoiceRaiseToCompanyList[0].Name;
            //        $scope.previewInvoiceModal.CompanyProfilePic = data.ResponseData.InvoiceRaiseToCompanyList[0].CompanyProfilePic;

            //        toaster.success("Success", message.successdataLoad);
            //    }
            //    else {

            //        toaster.error("Error", message.error);
            //    }
            //    $('#globalLoader').hide();
            //}), Error(function () {
            //    toaster.error("Error", message.error)
            //    $('#globalLoader').hide();
            //})

        }

        ////////////////////////////////////////////////hourSpentOnSourceByVendor///////////////////////////////////////////////////////////////
        var sumResource = 0
        var invoiceTotal = 0;
      
        $scope.hourSpentOnSourceByVendor = function () {
            debugger;
            sumResource = 0
            invoiceTotal = 0;
            $scope.invoiceSendModal.totalAmount = 0
            invoiceTotal += $scope.invoiceSendModal.totalHours == "" ? 0 : parseFloat($scope.invoiceSendModal.totalHours);
            angular.forEach($scope.getProjectInvoiceList.InvoiceForResourceList, function (item) {
                debugger;
                sumResource += item.Hours == undefined ? 0 : item.Hours == "" ? 0 : parseFloat(item.Hours);
                item.Amount = item.Hours * item.Rate
                $scope.invoiceSendModal.totalHours = sumResource;
                $scope.invoiceSendModal.totalAmount += item.Hours * item.Rate; // update total sum of Invoice


            });
             //////some of extra Resource/////////
            angular.forEach($scope.InvoiceToExtraResourceList, function myfunction(item) {
                debugger;
                if (item.Amount != "" && item.Amount) {
                    $scope.invoiceSendModal.totalAmount += parseFloat(item.Amount);    
                }
            })

        }
        ///////////////////////////////////removeExtraResourceInVendor/////////////////////////////////////////////////////////////
        $scope.removeExtraResourceInVendor = function (index) {
            $scope.InvoiceToExtraResourceList.splice(index, 1)
            $scope.hourSpentOnSourceByVendor();
        }



        var sumMilestonehours = 0
        ///////////////////////////////////////////////////////deleteMilestonefromInvoice////////////////////////////////////////////////////////////
        $scope.hourSpentOnMilestoneByVendor = function () {
            debugger;
           // sumMilestonehours = 0
            invoiceTotal = 0;
            sumMilestonehours = 0;
            invoiceTotal += $scope.invoiceSendModal.totalHours == "" ? 0 : parseFloat($scope.invoiceSendModal.totalHours);
            angular.forEach($scope.milestoneDetailList, function (item) {
                debugger;
                sumMilestonehours += item.Hours == undefined ? 0 : parseFloat(item.Hours);
            });
        }
        

  




        $scope.AddExtraResourceInVendor = function () {
            $scope.InvoiceToExtraResourceList.push({ Description: "", Amount: "" })
        }

   

        ///////////////////////////////////////////////////////removeMilestonefromInvoice////////////////////////////////////////////////////////////////////////////////
        var milestoneIndex
        $scope.removeMilestonefromInvoice = function (index) {
            debugger;
            $('#modaldeleteMilestoneInvoice').modal('show')
            milestoneIndex = index;
        }

        ///////////////////////////////////////////////////////deleteMilestonefromInvoice////////////////////////////////////////////////////////////////////////

        $scope.deleteMilestonefromInvoice = function () {
            debugger;
            $scope.milestoneDetailList.splice(milestoneIndex, 1)
            $('#modaldeleteMilestoneInvoice').modal('hide')
        }




        ///////////////////////////////////////////////////ChangeCompany//////////////////////////////////////////////////////////////////////////////
        $scope.ChangeCompany = function (selectedCompany) {
            debugger;
            $scope.invoiceSendModal.companyId = selectedCompany.Id;
            getApprovedBillableProjectByUserId();
            //vendorInvoiceFactory.getProjectForDrop(selectedCompany.Id,VendorId)
            //.success(function (data) {
            //    if (data.success) {
            //        $scope.getProjectInvoiceList.InvoiceRaiseToProjectList = data.ResponseData;
            //        toaster.success("Success", message.successdataLoad);
            //    }
            //    else {

            //        toaster.error("Error", message.error);
            //    }

            //}), Error(function () {
            //    toaster.error("Error", message.error)
            //    $('#globalLoader').hide();
            //})



        }

        ///////////////////////////////////////////////////previewVendorRaiseInvoice/////////////////////////////////////////////////////
        $scope.previewVendorRaiseInvoice = function (invoiceSendModal, SelectProjectName, SelectCompanyName, todateName, fromdateName,invoiceAmountName,  invoiceRaiseForm) {
            debugger;

            //if (InvoiceApprovedStatus == false) {
            //    toaster.warning("Warning", "Reset your Account before Raise the invoice");

            //}
            if (IsAccountSetByVendorPoc == false) {
                toaster.warning("Warning", "Reset your Account before Raise the invoice");
            }


            if (SelectProjectName || SelectCompanyName || todateName || fromdateName || invoiceAmountName) {

                invoiceRaiseForm.SelectCompanyName.$dirty = true;
                invoiceRaiseForm.SelectProjectName.$dirty = true;
                invoiceRaiseForm.fromdateName.$dirty = true;
                invoiceRaiseForm.todateName.$dirty = true;
                invoiceRaiseForm.invoiceAmountName.$dirty = true
                return;

            }
            $scope.finalTotalHrs = 0;
            for (var i = 0; i < $scope.worklogList.length; i++) {
                $scope.finalTotalHrs+= $scope.worklogList[i].TotalHrs;
            }
            //if (invoiceTotal != sumResource) {
            //    toaster.error("error", "Sum of hours spent by resources should be exactly equal to the total invoice Hours ")
            //    return;
            //}
            //if (invoiceTotal != sumMilestonehours) {
            //    toaster.error("error", "Sum of Hours spent on milstones should be exactly equal to the total invoice Hours")

            //    return;
            //}

            $('#InvoicePreviewForVendorModal').modal('show');

        }



        $scope.saveVendorInvoice = function (invoiceSendModal, SelectProjectName, SelectCompanyName, todateName, fromdateName,  invoiceAmountName, invoiceRaiseForm) {
            debugger;

            if (SelectCompanyName || todateName || fromdateName ||  invoiceAmountName) {
                invoiceRaiseForm.SelectCompanyName.$dirty = true;
                invoiceRaiseForm.SelectProjectName.$dirty = true;
                invoiceRaiseForm.fromdateName.$dirty = true;
                invoiceRaiseForm.todateName.$dirty = true;
                invoiceRaiseForm.invoiceAmountName.$dirty = true
                return;

            }

            //if (invoiceTotal != sumResource) {
            //    toaster.error("error", "Sum of hours spent by resources should be exactly equal to the total invoice Hours ")
            //    return;
            //}

            //if (invoiceTotal != sumMilestonehours) {
            //    toaster.error("error", "Sum of Hours spent on milstones should be exactly equal to the total invoice Hours")

            //    return;
            //}
            if ($scope.worklogList == undefined || $scope.worklogList==null||$scope.worklogList.length==0){
                toaster.error("error", "There is no work log in selected date ranges ");
                return;
            }


            if (invoiceSendModal.milestoneList != undefined) {
                for (var i = 0; i < invoiceSendModal.milestoneList.length; i++) {
                    dataModal.MilestoneId += invoiceSendModal.milestoneList[i].Id + ',';
                }
            }


            dataModal.InvoiceNumber = invoiceSendModal.invoiceNumber;
            dataModal.ProjectId = invoiceSendModal.ProjectId;
            dataModal.UserId = UserId;
            dataModal.SendTo = $scope.invoiceSendModal.sendTo;
            dataModal.CompanyId = invoiceSendModal.companyId;
            dataModal.FromDate = invoiceSendModal.fromDate;
            dataModal.ToDate = invoiceSendModal.toDate;
            dataModal.TotalHours = $scope.finalTotalHrs;//invoiceSendModal.totalHours;
            dataModal.TotalAmount = $scope.finalTotalAmount; //invoiceSendModal.totalAmount;
            dataModal.Message = invoiceSendModal.message;
            dataModal.ProjectId = invoiceSendModal.ProjectId == undefined ? ProjectId : invoiceSendModal.ProjectId;
            dataModal.VendorId = VendorId;
            dataModal.PhaseId = invoiceSendModal.phaseId;
            dataModal.InvoiceForResourceList = $scope.getProjectInvoiceList.InvoiceForResourceList;
            dataModal.InvoiceToMilestoneList = $scope.milestoneDetailList;
            dataModal.WorkLogList = $scope.worklogList;
            debugger;
            //alert("Hello");
            $('#globalLoader').show();
            vendorInvoiceFactory.saveVendorInvoice(dataModal)
            .success(function (data) {
                debugger;
                if (data.success) {
                    toaster.success("Success", message.VendorInvoiceSentSuccessfully);
                    $scope.invoiceSendModal = {};
                    dataModal = {};
                    $location.path('project/awarded');
                }
                else {
                    toaster.error("Error", message.error)
                }
                $('#globalLoader').hide();
            }), Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();
            })
        }


        $scope.saveVendorInvoiceFromPreview = function () {
            debugger;


            if ($scope.invoiceSendModal.milestoneList != undefined) {
                for (var i = 0; i < $scope.invoiceSendModal.milestoneList.length; i++) {
                    dataModal.MilestoneId += $scope.invoiceSendModal.milestoneList[i].Id + ',';
                }
            }
           



            dataModal.InvoiceNumber = $scope.invoiceSendModal.invoiceNumber;
            dataModal.ProjectId = $scope.invoiceSendModal.ProjectId;
            dataModal.UserId = UserId;
            dataModal.SendTo = $scope.invoiceSendModal.sendTo;
            dataModal.CompanyId = $scope.invoiceSendModal.companyId;
            dataModal.FromDate = $scope.invoiceSendModal.fromDate;
            dataModal.ToDate = $scope.invoiceSendModal.toDate;
            dataModal.TotalHours = $scope.finalTotalHrs;
            dataModal.TotalAmount = $scope.finalTotalAmount;
            dataModal.Message = $scope.invoiceSendModal.message;
            dataModal.ProjectId = $scope.invoiceSendModal.ProjectId == undefined ? ProjectId : $scope.invoiceSendModal.ProjectId;
            dataModal.VendorId = VendorId;
            dataModal.PhaseId = $scope.invoiceSendModal.phaseId;
            dataModal.InvoiceForResourceList = $scope.getProjectInvoiceList.InvoiceForResourceList;
            dataModal.InvoiceToMilestoneList = $scope.milestoneDetailList;
           // dataModal.InvoiceToExtraResourceList = $scope.InvoiceToExtraResourceList;
            dataModal.WorkLogList = $scope.worklogList;
            $('#globalLoader').show();
            vendorInvoiceFactory.saveVendorInvoice(dataModal)
            .success(function (data) {
                debugger;
                if (data.success) {
                    toaster.success("Success", message.VendorInvoiceSentSuccessfully);
                    $scope.invoiceSendModal = {};
                    dataModal = {};
                    $location.path('project/awarded');
                }
                else {
                    toaster.error("Error", message.error)
                }
                $('#globalLoader').hide();
            }), Error(function () {
                toaster.error("Error", message.error)
                $('#globalLoader').hide();
            })
        }

        $scope.previewInvoiceAftersend = function () {
            debugger;
            $('#InvoicePreviewAfterRasisingInvoice').modal('show');

        }


    })
})