﻿
define(['app', 'roleManageFactory'], function (app) {

    app.controller("roleManageController", function ($scope, $rootScope, $confirm, $http, sharedService, toaster, apiURL, $location, roleManageFactory) {

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.imgURL = apiURL.imageAddress;
        $scope.roleAddUpdateModel = {};
        $scope.items = [
            {
                value: 1,
                label: 'Role',
                checked:true
            },
            {
                value: 2,
                //label: 'Job Profile',
                label: 'Job Title',
                checked: false
            },
            {
                value: 3,
                label: 'Both',
                checked: false
            }
            ];


        function getCompanyRoles() {

            /* Get All Role Of Company*/
            $('#globalLoader').show();
            roleManageFactory.getCompanyRoles(sharedService.getCompanyId())
                .success(function (data) {
                    $scope.CompanyRoleObject = data.ResponseData;
                    $('#globalLoader').hide();
                })
                .error(function () {
                    toaster.error("Error", message["error"]);
                    $('#globalLoader').hide();
                })

        }

        /*SHowing Add Update Role Modal Pop Up*/
        $scope.showRoleAddUpdateModal = function () {

            $scope.roleButton = false;
            $scope.roleAddUpdateModel = {};
            $scope.roleAddUpdateModel.Type = 1; // by default value =1 means its a role

            $("input[name='RoleName']").removeClass("inputError");
            $("input[name='RoleDescription']").removeClass("inputError");
            $("input[name='RolePriority']").removeClass("inputError");

            $('#modalRoleAddUpdate').modal('show');
        };


        /*Add new Role */
        $scope.addNewRole = function (isRoleNameInvalid, isRoleDescriptionInvalid, isRolePriorityInvalid, roleAddUpdateForm) {
            if (isRoleNameInvalid || isRoleDescriptionInvalid) {
                roleAddUpdateForm.RoleName.$dirty = true;
                roleAddUpdateForm.RoleDescription.$dirty = true;
                roleAddUpdateForm.RolePriority.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                var NewRoleObj = { 'RoleModel': $scope.roleAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': sharedService.getCompanyId() }
                roleManageFactory.insertRole(NewRoleObj)
                    .success(function (insertData) {
                        if (insertData.success) {
                            if (insertData.ResponseData == 'RoleAddedSuccess' || insertData.ResponseData == 'DesignationAddedSuccess' || insertData.ResponseData == 'BothAddedSuccess') {

                                $('#modalRoleAddUpdate').modal('hide');
                                $('#globalLoader').hide();
                                toaster.success("Success", message[insertData.ResponseData]);

                                roleAddUpdateForm.RoleName.$dirty = false;
                                roleAddUpdateForm.RoleDescription.$dirty = false;
                                roleAddUpdateForm.RolePriority.$dirty = false;

                                getCompanyRoles();

                            }
                            else {
                                toaster.warning("Warning", message[insertData.ResponseData]);
                                $('#globalLoader').hide();
                            }
                        }
                        else {
                            toaster.error("Error", message["error"]);
                            $('#globalLoader').hide();
                        }

                    })
                    .error(function (error) {
                        toaster.error("Error", message["error"]);
                        $('#globalLoader').hide();
                    });
            }
        };

        /*Edit Role*/
        $scope.editRole = function (roleObject) {

            $scope.roleButton = true;
            $scope.roleAddUpdateModel = {};
            $scope.roleAddUpdateModel.RoleId = roleObject.RoleId;
            $scope.roleAddUpdateModel.RoleName = roleObject.RoleName;
            $scope.roleAddUpdateModel.RoleDescription = roleObject.RoleDescription;
            $scope.roleAddUpdateModel.RolePriority = roleObject.RolePriority;
            $scope.roleAddUpdateModel.Type = roleObject.Type;

            $('#modalRoleAddUpdate').modal('show');
        };

        /*Update Role*/
        $scope.updateRole = function (isRoleNameInvalid, isRoleDescriptionInvalid, isRolePriorityInvalid, roleAddUpdateForm) {
            if (isRoleNameInvalid || isRoleDescriptionInvalid) {
                roleAddUpdateForm.RoleName.$dirty = true;
                roleAddUpdateForm.RoleDescription.$dirty = true;
                roleAddUpdateForm.RolePriority.$dirty = true;
            }
            else {

                $('#globalLoader').show();
                var NewRoleObj = { 'RoleModel': $scope.roleAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': sharedService.getCompanyId() }
                roleManageFactory.updateRole(NewRoleObj)
                    .success(function (updateData) {
                        if (updateData.success) {
                            if (updateData.ResponseData == 'RoleUpdatedSuccess' || updateData.ResponseData == 'DesignationUpdatedSuccess' || updateData.ResponseData == 'RecordUpdatedSuccess') {

                                getCompanyRoles();

                                $('#modalRoleAddUpdate').modal('hide');
                                $('#globalLoader').hide();
                                toaster.success("Success", message[updateData.ResponseData]);

                                roleAddUpdateForm.RoleName.$dirty = false;
                                roleAddUpdateForm.RoleDescription.$dirty = false;
                                roleAddUpdateForm.RolePriority.$dirty = false;

                            }
                            else {
                                toaster.warning("Warning", message[updateData.ResponseData]);
                                $('#globalLoader').hide();
                            }
                        }
                        else {
                            toaster.error("Error", message["error"]);
                            $('#globalLoader').hide();
                        }

                    })
                    .error(function (error) {
                        toaster.error("Error", message["error"]);
                        $('#globalLoader').hide();
                    });

            }
        };

        /*Delete Role*/
        $scope.deleteRole = function (RoleId, index) {
            $confirm({ text: 'Are you sure you want to delete this Role?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
                .then(function () {
                    $('#globalLoader').show();
                    roleManageFactory.deleteRole(RoleId, sharedService.getCompanyId())
                        .success(function (deleteData) {
                            if (deleteData.success) {
                                toaster.success("Record deleted successfully");
                                $scope.CompanyRoleObject.splice(index, 1);
                                $('#globalLoader').hide();
                            }
                            else {
                                toaster.error("Error");
                                $('#globalLoader').hide();
                            }
                        })
                        .error(function () {
                            toaster.error("Error");
                            $('#globalLoader').hide();
                        });
                })
        };

        /*ActiveInactive Role*/
        $scope.activeInactiveRole = function (RoleId) {
            $('#globalLoader').show();
            roleManageFactory.activeInactiveRole(RoleId, sharedService.getCompanyId())
                .success(function (data) {
                    if (data.success) {
                        toaster.success("Success");
                        getCompanyRoles();
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error");
                        $('#globalLoader').hide();
                    }
                })
                .error(function () {
                    toaster.error("Error");
                    $('#globalLoader').hide();
                });

        };

        /*Set type whether its a role,designation or both*/
        $scope.handleRadioClick = function (item) {
            $scope.roleAddUpdateModel.Type = item.value;
        };

        angular.element(document).ready(function () {
            getCompanyRoles();
        });







    });

});

