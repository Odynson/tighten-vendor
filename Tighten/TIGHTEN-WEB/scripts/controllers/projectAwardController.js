﻿define(['app'], function () {

    app.controller("projectAwardController",
        function ($scope, sharedService, $rootScope, toaster, $filter, $http, $location, vendorProjectFactory, $sce, apiURL) {
            $scope.imgURL = apiURL.imageAddress;
            var CompanyId;
            var baseurl = apiURL.baseAddress;


            $scope.qouteSentToCompanyModal = ''
            var dataModal = {}

            $scope.maxSize = 5;     // Limit number for pagination display number.  
            $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
            $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
            $scope.pageSizeSelected = 5; // Maximum number of items per page.  


            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
                $rootScope.activeTeam = [];
            }

            $scope.qouteSentToCompanyModal = { CompanyId: 0, Name: "--Select All--" };
            angular.element(document).ready(function () {
                CompanyId = sharedService.getCompanyId();
                dataModal = { Id: 0, CompanyId: 0, VendorId: sharedService.getVendorId(), Name: "", UnpaidInvoice: false, ProjectId: 0, RequestType: 1, PageIndex: 1, PageSize: 5 }
                companyListForDrop(CompanyId)


            })

            function companyListForDrop(CompanyId) {

                $('#globalLoader').show();
                var VendorId = sharedService.getVendorId()
                vendorProjectFactory.companyListForDrop(VendorId)
                .success(function (data) {
                    if (data.success) {

                        $scope.awardedProjectByCompanyList = data.ResponseData;
                        $scope.awardedProjectByCompanyList.push($scope.qouteSentToCompanyModal)
                        getProjectAwarded();// gettin all project list
                        toaster.success("Success", message.successdataLoad);
                        $('#globalLoader').hide();
                    }
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })


            }


            $scope.awardedProjectByCompanyList = [];

            function getProjectAwarded() {
                debugger;
                $('#globalLoader').show();
                vendorProjectFactory.getProjectAwarded(dataModal)
                .success(function (data) {
                    if (data.success) {
                        debugger;
                        if (data.ResponseData != null) {

                            $scope.projectAwardedList = data.ResponseData;
                            toaster.success("Success", message.successdataLoad);
                      
                            if (data.ResponseData.length > 0) {
                                $scope.totalCount = data.ResponseData[0].TotalPageCount;
                            }
                        }
                        $('#globalLoader').hide();
                    }
                }),
                    Error(function (data, status, headers, config) {
                        $('#globalLoader').hide();
                        toaster.error("Error", data.error);
                    })

            }

            $scope.performanceStatusList = [
            { Id: 0, Status: "--Select All--" },
            { Id: 1, Status: "--well in range--" },
            { Id: 2, Status: "--neck to neck--" },
            { Id: 3, Status: "--Over Spent--" },

            ];

            $scope.filterPerformanceModal = $scope.performanceStatusList[0];

            $scope.searchProjectByName = function (searchText) {

                dataModal.Name = searchText;
                getProjectAwarded();
            }

            $scope.FilterByAwardedProjectCompany = function (qouteSentToCompanyModal) {

                dataModal.CompanyId = qouteSentToCompanyModal.CompanyId;
                getProjectAwarded();
            }

            $scope.FilterByPerformanceStatus = function (filterPerformanceModal) {

                dataModal.Id = filterPerformanceModal.Id;
                getProjectAwarded();
            }


            $scope.showUpdaidInvoice = function (unpaidInvoice) {

                dataModal.unpaidInvoice = unpaidInvoice;
                getProjectAwarded();
            }

            $scope.autoCompleteOptions = {

                minimumChars: 3,
                dropdownWidth: '500px',
                dropdownHeight: '200px',
                data: function (term) {
                    return $http.post(baseurl + 'ProjectsApi/GetProjectName',
                        dataModal = { Id: 0, CompanyId: 0, Name: term, VendorId: CompanyId, ProjectId: 0, RequestType: 1 })
                        .then(function (data) {
                            debugger;
                            $scope.getProjectTOQuoteList = data.data.ResponseData;
                            return data.data.ResponseData;
                        });
                },
                renderItem: function (item) {
                    return {
                        value: item.Name,
                        label: $sce.trustAsHtml(
                        "<p class='auto-complete'>"
                        + item.Name +
                        "</p>")

                    };

                },
                itemSelected: function (item) {
                    debugger;
                    dataModal.ProjectId = item.item.ProjectId
                    $scope.searchAwardedByProjectNameModal = item.item.Name;
                    getProjectAwarded()
                }
            }
            var isCtrlPressed;
            var isAPressed;
            /////////////////////////////empty text box project to quote companyProfile//////////////////////////////////////////////////////
            $scope.searchAwardedProjectByProjectName = function (searchAwardedByProjectNameModal, event) {

                if (searchAwardedByProjectNameModal != undefined && searchAwardedByProjectNameModal.length <= 1 && event.keyCode == 8) {
                    dataModal.CompanyId = 0;
                    dataModal.Name = "";
                    getProjectAwarded();
                }
                // if key is pressed
                if (event.keyCode == 17 || isCtrlPressed) {
                    isCtrlPressed = true;
                }
                else {
                    isCtrlPressed = false;
                }

                // other key after the control
                if (isCtrlPressed) {
                    if (event.keyCode == 97 || event.keyCode == 65) {
                        isAPressed = true;

                    }
                }
                else {
                    isAPressed = false;
                }


                if (isCtrlPressed && isAPressed) {
                    if (event.keyCode == 46 || event.keyCode == 8) {
                        dataModal.ProjectId = 0;
                        dataModal.Name = "";
                        getProjectAwarded();
                        isCtrlPressed = false;
                        isAPressed = false;
                    }

                }
                var text = window.getSelection().toString()
                if (text != "" && (event.keyCode == 8 || event.keyCode == 46)) {
                    dataModal.ProjectId = 0;
                    dataModal.Name = "";
                    getProjectAwarded();
                }
            }


            $scope.cutSearchAwardedProjectByProjectName = function () {
                dataModal.ProjectId = 0;
                dataModal.Name = "";
                getProjectAwarded();

            }


            //-------------------pagesizeList---------------------------------------------------/////
            $scope.pagesizeList = [
                { PageSize: 5 },
                { PageSize: 10 },
                { PageSize: 25 },
                { PageSize: 50 },

            ];

            $scope.PageSize = $scope.pagesizeList[0];



            //-------------------changePageSize--------------------------------------------------------/////
            $scope.changePageSize = function (pageSizeSelected) {

                dataModal.PageSize = pageSizeSelected.PageSize;
                $scope.pageSizeSelected = pageSizeSelected.PageSize;
                getProjectAwarded(dataModal);
            }

            //-------------------pageChanged-------------------------------------------------------/////
            $scope.pageChanged = function (pageIndex) {

                dataModal.PageIndex = pageIndex;
                getProjectAwarded(dataModal);
            }



        })

})