﻿
define(['app', 'projectsFactory', 'usersFactory', 'teamFactory', 'accountFactory'], function (app, projectsFactory, usersFactory, teamFactory, accountFactory, userProfileFactory, roleFactory, FreelancerInvitationFactory) {
    app.controller("userProfileController", function ($scope, $filter, $rootScope, $stateParams, $timeout, $confirm, $http, $location, sharedService, toaster, $upload, apiURL, projectsFactory, usersFactory, teamFactory, accountFactory, userProfileFactory, utilityService, roleFactory, FreelancerInvitationFactory, setPasswordFactory
        ) {
        //$scope.phonePattern = '/^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/';
        $scope.phoneNumber = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;
        $scope.genders = [
                        { name: 'Female', value: "2" },
                        { name: 'Male', value: "1" }
        ];
        var baseurl = apiURL.baseAddress;
        $scope.imgURL = apiURL.imageAddress;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.openSaveHourlyRate = false;
        $scope.openSaveAvailabilty = false;
        $scope.openSaveFavouriteProjects = false;
        $scope.openSaveTestimonials = false;
        $scope.openSaveSkillSetTags = false;
        $scope.openSaveBusinessDomainTags = false;
        $scope.FavProjects = {};
        $scope.FavProjects.userFavouriteProjects = [];
        $scope.Tags = {};
        $scope.Tags.SkillSetTags = [];
        $scope.EducationDetail = {};
        $scope.EducationDetail.EducationalStream = [];
        $scope.currentUserId = 0;
        $scope.currentCompanyId = 0;
        $scope.FreelancerModel = {};
        $scope.LoggedInFirstTimeByLinkedIn = {};

        $scope.IsVendor = true;

        $scope.checkIfCompanyIsVendor = function () {
            debugger
            usersFactory.checkIfCompanyIsVendor($scope.currentCompanyId)
                .success(function (data) {
                    debugger;
                    $scope.IsVendor = data.ResponseData;
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });

        }

        if (sharedService.getUserId() != undefined) {
            $scope.currentUserId = sharedService.getUserId();
        }
        else {

        }
        if (sharedService.getCompanyId() != undefined) {
            $scope.currentCompanyId = sharedService.getCompanyId();
        }

        // comment on  4-4-17
        //$scope.user = [];
        //var multiSelectPreviousData = [];
        //var multiSelectCurrentData = [];

        //var range = [];
        //for (var i = 1; i < 41; i++) {
        //    range.push(i);
        //}
        //$scope.availableHours = range;

        $scope.user = [];
        var multiSelectPreviousData = [];
        var multiSelectCurrentData = [];

        var range = [{ "name": "0-10", "id": 10 },
            { "name": "10-20", "id": 20 },
            { "name": "20-30", "id": 30 },
            { "name": "30-40", "id": 40 },
            { "name": "More then 40", "id": 41 }];
        $scope.availableHours = range;



        $scope.DegreeRange = ["High School", "Associate's Degree", "Bachelor's Degree", "Master's Degree", "Master of Business Administration (M.B.A.)",
            "Juris Doctor (J.D.)", "Doctor of Medicine (M.D)", "Doctor of  Philosophy (Ph.D.)", "Engineer's Degree", "Other"];


        var currentDateForDatesAttended = new Date();
        var currentYearForDatesAttended = currentDateForDatesAttended.getFullYear();

        var rangeForDatesAttendedFrom = [];
        for (var i = currentYearForDatesAttended; i >= currentYearForDatesAttended - 100; i--) {
            rangeForDatesAttendedFrom.push(i);
        }
        $scope.objForDatesAttendedFrom = rangeForDatesAttendedFrom;

        var rangeForDatesAttendedTo = [];
        for (var i = currentYearForDatesAttended + 10; i >= currentYearForDatesAttended - 100; i--) {
            rangeForDatesAttendedTo.push(i);
        }
        $scope.objForDatesAttendedTo = rangeForDatesAttendedTo;





        $scope.MyProfileObject = {};
        /*This is not usable by rakesh*/
        /* Showing User profile with Details and Edit Options*/
        $scope.showUserProfile = function () {
            $scope.UserProfileObject = $scope.MyProfileObject;
            $scope.UserTeamsObject = $scope.TeamsObject;
            //closeCentreSections();
            $scope.otherUser = false;
            $scope.showCenterLoader = true;
            $scope.selectedProject = 0;
            $scope.selectedTodo = 0;
            $scope.selectedNavigation = "MyProfile";


            /* Getting projects in which user is project member irrespective of teamid */
            projectsFactory.getUserProjects($scope.MyProfileObject.userId)
                .success(function (data) {
                    if (data.success) {
                        $scope.MyProjectsObject = data.responseData;
                        $scope.showCenterLoader = false;
                        $('#divProfile').show();
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });
        };

        $scope.sendUserMessage = function (UserId) {

            $location.path('/inbox/' + UserId);
        }


        $scope.changeUserProfile = function () {
            debugger
            /*  change User Profile To Public or Private */
            var data = { 'UserId': sharedService.getUserId(), 'UserProfileStatus': $scope.UserProfile };
            userProfileFactory.changeUserProfile(data)
            .success(function (dataUserProfile) {
                $scope.UserProfile = dataUserProfile.ResponseData;
                toaster.success("Success", "Profile status is changed");
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured !");
            });

        }


        /*This is not usable by rakesh*/
        /* Showing Third User Profile with Details Only  */
        $scope.showOtherUserProfile = function (userId) {
            if (userId == null || userId == undefined) {
                return;
            }
            if (userId == $scope.MyProfileObject.userId) {
                $scope.UserProfileObject = $scope.MyProfileObject;
                $scope.UserTeamsObject = $scope.TeamsObject;
                //    closeCentreSections();
                $scope.showCenterLoader = true;
                $scope.selectedProject = 0;
                $scope.selectedTodo = 0;
                $scope.selectedNavigation = "MyProfile";
                $scope.otherUser = false;

                /* Getting projects in which user is project member irrespective of teamid */
                projectsFactory.getUserProjects(userId)
                    .success(function (data) {
                        if (data.success) {
                            $scope.MyProjectsObject = data.responseData;
                            $scope.showCenterLoader = false;
                            $('#divProfile').show();
                        }
                        else {
                            toaster.error("Error", data.message);
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                    });
            }
            else {
                //closeCentreSections();
                $scope.showCenterLoader = true;
                $scope.selectedProject = 0;
                $scope.selectedTodo = 0;
                $scope.selectedNavigation = "MyProfile";
                $scope.otherUser = true;

                /* Getting User Details  */
                usersFactory.getUser(userId)
                    .success(function (data) {
                        if (data.success) {
                            $scope.UserProfileObject = data.responseData;

                            /* Getting projects in which user is project member irrespective of teamid */
                            projectsFactory.getUserProjects(userId)
                                .success(function (data) {
                                    if (data.success) {
                                        $scope.MyProjectsObject = data.responseData;
                                        /*  Getting User Teams */
                                        teamFactory.getUserTeams(userId)
                                            .success(function (data) {
                                                if (data.success) {
                                                    $scope.UserTeamsObject = data.responseData;
                                                    $scope.showCenterLoader = false;
                                                    $('#divProfile').show();
                                                }
                                                else {
                                                    toaster.error("Error", data.message);
                                                }
                                            })
                                            .error(function () {
                                                toaster.error("Error", "Some Error Occured !");
                                            });
                                    }
                                    else {
                                        toaster.error("Error", data.message);
                                    }
                                })
                                .error(function () {
                                    toaster.error("Error", "Some Error Occured !");
                                });
                        }
                        else {
                            toaster.error("Error", data.message);
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                    });
            }
        };




        ////////////Date Functions/////////////
        /************************/

        var d = new Date();

        d.setDate(d.getDate() - 7);
        $scope.fromDate = d;// d.setDate(d.getDate() - 7);

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };
        var da = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
        };

        $scope.toggleMin();

        $scope.maxDate = new Date(da.getFullYear(), 5, 22);
        $scope.maxDOBValue = new Date((da.getFullYear() - 10), 1, 1);

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.open3 = function () {
            $scope.popup3.opened = true;
        };

        $scope.setDate = function (year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
        //$scope.init = new Date(da.getFullYear()-10, 5, 22);
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.popup3 = {
            opened: false
        };

        $scope.getDayClass = function (date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        };

        /***********************/

        /* Showing All User Profile with Details */
        $scope.showProfile = function (userId, iseditable) {
            $('#idGeneralPersonalInformation').show();
            //closeCentreSections();
            $scope.showCenterLoader = true;
            $scope.selectedProject = 0;
            $scope.selectedTodo = 0;
            $scope.selectedNavigation = "MyProfile";
            $scope.otherUser = !iseditable;
           // alert(userId);
            /* Getting User Details  */
            usersFactory.getUser(userId, $scope.currentCompanyId)
                .success(function (dataUser) {
                    debugger;
                    if (dataUser.success) {
                        if (dataUser.ResponseData != undefined && dataUser.ResponseData != null) {
                            $scope.UserTeamAndProjectCount = dataUser.ResponseData;
                            $scope.UserProfileObject = dataUser.ResponseData;
                            $scope.HourlyRate = dataUser.ResponseData.Rate;
                            $scope.UserProfile = dataUser.ResponseData.isProfilePublic;
                            $scope.userPassword = dataUser.ResponseData.Password;
                            $scope.user.SelectedAvailability = dataUser.ResponseData.Availabilty == "" ? 0 : dataUser.ResponseData.Availabilty;

                            if ($scope.UserProfileObject.gender != 0) {
                                if ($scope.UserProfileObject.gender == 1) {
                                    $("#radioMale").prop("checked", true);
                                }
                                else if ($scope.UserProfileObject.gender == 2) {
                                    $("#radioFemale").prop("checked", true);
                                }
                            }
                        }

                    }
                    else {
                        toaster.error("Error", dataUser.message);
                    }
                    $('#idGeneralPersonalInformation').hide();
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                    $('#idGeneralPersonalInformation').hide();
                });


            ///* Getting projects in which user is project member irrespective of teamid */
            //projectsFactory.getUserProjects(userId, sharedService.getCompanyId())
            //    .success(function (dataUserProjects) {
            //        if (dataUserProjects.success) {
            //            $scope.MyProjectsObject = dataUserProjects.ResponseData;
            //        }
            //        else {
            //            toaster.error("Error", dataUserProjects.message);
            //        }
            //    })
            //    .error(function () {
            //        toaster.error("Error", "Some Error Occured !");
            //    });


            ///*  Getting User Teams */
            //teamFactory.getUserTeams(userId, sharedService.getCompanyId(), $scope.otherUser)
            //    .success(function (dataUserTeams) {
            //        if (dataUserTeams.success) {
            //            $scope.UserTeamsObject = dataUserTeams.ResponseData;
            //        }
            //        else {
            //            toaster.error("Error", dataUserTeams.message);
            //        }
            //    })
            //    .error(function () {
            //        toaster.error("Error", "Some Error Occured !");
            //    });



            /*  Getting User Team And Project Count */
            //userProfileFactory.getUserTeamAndProjectCount(userId)
            //.success(function (dataTeamAndProjectCount) {
            //    if (dataTeamAndProjectCount.success) {
            //        $scope.UserTeamAndProjectCount = dataTeamAndProjectCount.ResponseData;
            //    }
            //    else {
            //        toaster.error("Error", dataTeamAndProjectCount.message);
            //    }
            //})
            //.error(function () {
            //    toaster.error("Error", "Some Error Occured !");
            //});

            $('#idEducationalDetails').show();
            /*  Getting User Education Detail */
            userProfileFactory.getUserEducationDetail(userId)
            .success(function (dataEducationDetail) {
                if (dataEducationDetail.success) {
                    $scope.UserEducationDetailObject = dataEducationDetail.ResponseData;
                }
                else {
                    toaster.error("Error", dataEducationDetail.message);
                }
                $('#idEducationalDetails').hide();
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured !");
                $('#idEducationalDetails').hide();
            });

            $('#idProfessionalDetails').show();
            /*  Getting User Professional Detail */
            userProfileFactory.getUserProfessionalDetail(userId)
             .success(function (dataProfessionalDetail) {
                 if (dataProfessionalDetail.success) {
                     $scope.UserProfessionalDetailObject = dataProfessionalDetail.ResponseData;
                 }
                 else {
                     toaster.error("Error", dataProfessionalDetail.message);
                 }
                 $('#idProfessionalDetails').hide();
             })
             .error(function () {
                 toaster.error("Error", "Some Error Occured !");
                 $('#idProfessionalDetails').hide();
             });


            // Getting User's Skill Set Tags
            userProfileFactory.getUserSkillSetTags(userId)
             .success(function (UserSkillSetTags) {
                 debugger;
                 $scope.allSkillSetTagsObject = UserSkillSetTags.ResponseData.allSkillSetTagsObject;
                 $scope.allBusinessDomainObject = UserSkillSetTags.ResponseData.allBusinessDomainObject;
                 $scope.Tags = {};
                 $scope.Tags.SkillSetTags = UserSkillSetTags.ResponseData.SkillSetTags;
                 $scope.Tags.BusinessDomainExpertiseTags = UserSkillSetTags.ResponseData.BusinessDomainExpertiseTags;

             })
            .error(function () {
                toaster.error("Error", "Some Error Occured !");
            });


            // Getting User's Efficiency
            userProfileFactory.getUserEfficiency(userId, $scope.currentUserId)
             .success(function (dataUserEfficiency) {
                 $scope.userEfficiency = dataUserEfficiency.ResponseData;
             })
             .error(function () {
                 toaster.error("Error", "Some Error Occured !");
             });


            // Getting User's Efficiency For strong and week technologies
            userProfileFactory.getUserEfficiencyForStrongAndWeekTechnology(userId)
             .success(function (dataStrongAndWeekTechnology) {
                 $scope.StrongAndWeekTechnology = dataStrongAndWeekTechnology.ResponseData;
             })
             .error(function () {
                 toaster.error("Error", "Some Error Occured !");
             });


            $('#idmyStatistics').show();
            // Getting User's Statistics

            userProfileFactory.getUserStatistics(userId, $scope.currentUserId)
            .success(function (dataUserStatistics) {

                $scope.userStatisticsProjects = dataUserStatistics.ResponseData;
                $('#idmyStatistics').hide();
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured !");
                $('#idmyStatistics').hide();
            });


            // Getting User's AvgRate
            userProfileFactory.getUserAvgRate(userId)
            .success(function (dataUserAvgRate) {

                $scope.userdataUserAvgRate = dataUserAvgRate.ResponseData;
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured !");
            });


            // Getting User's Favourite Projects

            userProfileFactory.getUserFavouriteProjects(userId)

             .success(function (dataFavouriteProjects) {
                 $scope.FavProjects = {};
                 if (dataFavouriteProjects.ResponseData != null) {

                     $scope.FavProjects.userFavouriteProjects = dataFavouriteProjects.ResponseData.userFavProjects;
                     $scope.userProjectsObject = dataFavouriteProjects.ResponseData.userProjectsForAllOrgs;
                 }
                 else {
                     $scope.FavProjects.userFavouriteProjects = null;
                     $scope.userProjectsObject = null;
                 }

                 $timeout(function () {
                     $scope.$apply();
                 });
             })
             .error(function () {
                 toaster.error("Error", "Some Error Occured !");
             });

            /***** This will be used when we create the screen to add testimonial
            // Getting User's Testimonial
            userProfileFactory.getUserTestimonials(userId)
            .success(function (dataUserTestimonials) {

                $scope.UserTestimonials = dataUserTestimonials.ResponseData;
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured !");
            });
            ***************************/

            $('#idFeedbacks').show();
            // Getting User's Feedback
            userProfileFactory.getUserFeedbacks(userId)
            .success(function (dataUserFeedbacks) {

                $scope.UserFeedbacks = dataUserFeedbacks.ResponseData;
                $('#idFeedbacks').hide();
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured !");
                $('#idFeedbacks').hide();
            });

            $('#idEducationalDetails').show();
            // Getting User's EducationalStreams
            userProfileFactory.getEducationalStreams(userId)
            .success(function (dataUserEducationalStreams) {

                $scope.allEducationalStreamObject = dataUserEducationalStreams.ResponseData.allEducationalStreamObject;
                $scope.EducationDetail = {}
                $scope.EducationDetail.EducationalStream = dataUserEducationalStreams.ResponseData.EducationalStream;
                $('#idEducationalDetails').hide();
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured !");
                $('#idEducationalDetails').hide();
            });


            // Getting OtherUser's Profile Status
            userProfileFactory.getOtherUserProfileStatus(userId)
            .success(function (dataOtherUserProfile) {

                $scope.OtherUsersProfile = dataOtherUserProfile.ResponseData;
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured !");
            });


            roleFactory.getRoles(sharedService.getCompanyId(), sharedService.getRoleId())
            .success(function (data) {
                if (data.success) {
                    $scope.RoleObject = data.ResponseData;
                    var roleObjectForFreelancer = [];
                    for (var i = 0; i < $scope.RoleObject.length; i++) {
                        if ($scope.RoleObject[i].RoleId != 1 && $scope.RoleObject[i].RoleId != 5) {
                            roleObjectForFreelancer.push($scope.RoleObject[i]);
                        }
                    }
                    $scope.RoleObjectForFreelancer = roleObjectForFreelancer;

                    var objForAllProjectsOfCompany = { CompanyId: sharedService.getCompanyId(), UserId: sharedService.getUserId(), EmailOrHandle: "", chkInvitationType: "" };
                    usersFactory.GetAllProjectsOfCompany(objForAllProjectsOfCompany)
                        .success(function (response) {
                            $scope.ProjectsObject = response.ResponseData;
                            bindProjectDdl();
                        })
                        .error(function (response) {
                            toaster.error(message[response.message]);
                        })
                }
            })
            .error(function () {
                toaster.error("Error", "Some error occured");
            });


            //$scope.showCenterLoader = false;
            //$('#divProfile').show();

        };

        /* Open Modal popup for editing User Profile */
        $scope.editUserProfile = function () {

            $scope.MyProfileEditObject = {};
            $scope.MyProfileEditObject.gender = 1;
            $scope.MyProfileEditObject.firstName = $scope.UserProfileObject.firstName;
            $scope.MyProfileEditObject.lastName = $scope.UserProfileObject.lastName;
            if ($scope.UserProfileObject.gender != 0) {
                $scope.MyProfileEditObject.gender = $scope.UserProfileObject.gender;
                if ($scope.MyProfileEditObject.gender == 1) {
                    $("#radioMale").prop("checked", true);
                }
                else if ($scope.MyProfileEditObject.gender == 2) {
                    $("#radioFemale").prop("checked", true);
                }
            }
            $scope.MyProfileEditObject.Address = $scope.UserProfileObject.Address;
            $scope.MyProfileEditObject.DateOfBirth = $scope.UserProfileObject.DateOfBirth;
            $scope.MyProfileEditObject.PhoneNo = $scope.UserProfileObject.PhoneNo;
            $scope.MyProfileEditObject.Designation = $scope.UserProfileObject.Designation;
            $scope.MyProfileEditObject.Department = $scope.UserProfileObject.Department;
            $scope.MyProfileEditObject.Experience = $scope.UserProfileObject.Experience;
            $scope.MyProfileEditObject.Expertise = $scope.UserProfileObject.Expertise;
            $scope.MyProfileEditObject.AboutMe = $scope.UserProfileObject.AboutMe;
            $scope.MyProfileEditObject.email = $scope.UserProfileObject.email;
            $('#modalMyProfileEdit').modal('show');
        };

        /*  Update User Profile */
        $scope.updateUserProfile = function (frmUser, isFirstNameInvalid) {

            if (isFirstNameInvalid) {
                frmUser.firstName.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                $scope.MyProfileEditObject.updateflag = 4;
                $scope.MyProfileEditObject.userId = sharedService.getUserId();
                usersFactory.updateUserProfile($scope.MyProfileEditObject)
                    .success(function (data) {
                        if (data.success) {

                            $scope.UserProfileObject.firstName = $scope.MyProfileEditObject.firstName;
                            $scope.UserProfileObject.lastName = $scope.MyProfileEditObject.lastName;
                            $scope.UserProfileObject.gender = $scope.MyProfileEditObject.gender;
                            $scope.UserProfileObject.Address = $scope.MyProfileEditObject.Address;
                            $scope.UserProfileObject.DateOfBirth = $scope.MyProfileEditObject.DateOfBirth;
                            $scope.UserProfileObject.PhoneNo = $scope.MyProfileEditObject.PhoneNo;
                            $scope.UserProfileObject.Designation = $scope.MyProfileEditObject.Designation;
                            $scope.UserProfileObject.Department = $scope.MyProfileEditObject.Department;
                            $scope.UserProfileObject.Experience = $scope.MyProfileEditObject.Experience;
                            $scope.UserProfileObject.Expertise = $scope.MyProfileEditObject.Expertise;
                            $scope.UserProfileObject.AboutMe = $scope.MyProfileEditObject.AboutMe;
                            $scope.$emit('bindUserDetail', []);
                            $('#modalMyProfileEdit').modal('hide');
                            toaster.success("Success", data.message);
                            $('#globalLoader').hide();
                        }
                        else {
                            if (data.message == undefined) {
                                toaster.error("Error", data.errors.toString());
                                $('#globalLoader').hide();
                            }
                            else {
                                toaster.error("Error", data.message);
                                $('#globalLoader').hide();
                            }
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();
                    });
            }
        };


        

        /* Updating User Profile Photo*/
        $scope.updateUserPhoto = function ($files) {
            //$files: an array of files selected, each file has name, size, and type.

            $('#globalLoader').show();
            for (var i = 0; i < $files.length; i++) {
                var $file = $files[i];
                (function (index) {

                    //if ($scope.upload != undefined) {
                    //$scope.upload[index] =
                    $upload.upload({
                        url: baseurl + "UsersAPI/UploadProfile", // webapi url
                        method: "POST",
                        data: { UploadedProfile: $file, UserId: sharedService.getUserId() },
                        file: $file
                    })
                    .progress(function (evt) {
                        // get upload percentage
                        $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                    })
                    .success(function (data, status, headers, config) {
                        // file is uploaded successfully
                        if (data.success) {
                            toaster.success("Success", data.message);
                            angular.forEach(
                            angular.element("input[type='file']"),
                                    function (inputElem) {
                                        angular.element(inputElem).val(null);
                                    });
                            $scope.showProfile(sharedService.getUserId(), true)
                            $scope.$emit('showMenu', []);
                            $('#globalLoader').hide();
                        }
                        else {
                            angular.forEach(
                             angular.element("input[type='file']"),
                                     function (inputElem) {
                                         angular.element(inputElem).val(null);
                                     });
                            toaster.error("Error", data.message);
                            $('#globalLoader').hide();
                        }
                    })
                    .error(function (data, status, headers, config) {
                        // file failed to upload
                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();
                    });

                    //}
                })(i);
            }


        };

        /* Show Modal For Changing User Login Password*/
        $scope.ShowChangePasswordModal = function () {
            $scope.ChangePasswordMessage = "";
            //$scope.changePasswordForm.inputCurrentPassword.$dirty = false;
            //$scope.changePasswordForm.inputNewPassword.$dirty = false;
            //$scope.changePasswordForm.inputConfirmNewPassword.$dirty = false;
            $scope.changePasswordObject = {};
            $('#modalChangePassword').modal('show');
        };


        $scope.ShowSetPasswordModal = function () {
            $scope.isLinkedInUsed = false;
            $scope.LoggedInFirstTimeByLinkedIn.EmailId = sharedService.getEmailId();
            $('#modalLinkedInMessage').modal('show');
        };

        var setPasswordObj = {};
        $scope.saveMypassword = function () {
            $('#globalLoader').show();

            if ($scope.LoggedInFirstTimeByLinkedIn.Password != '' && ($scope.LoggedInFirstTimeByLinkedIn.Password == $scope.LoggedInFirstTimeByLinkedIn.ConfirmPassword)) {
                setPasswordObj.NewPassword = $scope.LoggedInFirstTimeByLinkedIn.Password;
                setPasswordObj.UserId = sharedService.getUserId();
                setPasswordObj.EmailId = sharedService.getEmailId();
                checkEmailConfirmation(setPasswordObj);
            }
            else if ($scope.LoggedInFirstTimeByLinkedIn.Password == '') {
                toaster.warning("Warning", message["emptyPasswordField"]);
                $('#globalLoader').hide();
            }
            else if ($scope.LoggedInFirstTimeByLinkedIn.Password != $scope.LoggedInFirstTimeByLinkedIn.ConfirmPassword) {
                toaster.warning("Warning", message["passwordAndConfirmPasswordNotMatched"]);
                $('#globalLoader').hide();
            }
        }

        // getting the parameters from the url
        var checkEmailConfirmation = function (setPasswordObj) {
            // email confirmtion service to save the user detail

            setPasswordFactory.setPasswordFromUserProfile(setPasswordObj, function (response) {
                /* Getting User Details  */
                usersFactory.getUser($stateParams.uid, $scope.currentCompanyId)
                    .success(function (dataUser) {
                        if (dataUser.success) {

                            $scope.UserProfileObject = dataUser.ResponseData;
                            $scope.HourlyRate = dataUser.ResponseData.Rate;
                            $scope.user.SelectedAvailability = dataUser.ResponseData.Availabilty == "" ? 0 : dataUser.ResponseData.Availabilty;
                            $scope.UserProfile = dataUser.ResponseData.isProfilePublic;
                            $scope.userPassword = dataUser.ResponseData.Password;

                            toaster.success("Success", response.message);
                            $("#modalLinkedInMessage").modal('hide');
                            $('#globalLoader').hide();
                        }
                        else {
                            toaster.error("Error", dataUser.message);
                            $("#modalLinkedInMessage").modal('hide');
                            $('#globalLoader').hide();
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                        $("#modalLinkedInMessage").modal('hide');
                        $('#globalLoader').hide();
                    });


            });
        };



        /* Change Password Function */
        $scope.ChangePassword = function (changePasswordForm, isCurrentPasswordInvalid, isNewPasswordInvalid, isConfirmNewPasswordInvalid) {

            if (isCurrentPasswordInvalid || isNewPasswordInvalid || isConfirmNewPasswordInvalid) {
                changePasswordForm.inputCurrentPassword.$dirty = true;
                changePasswordForm.inputNewPassword.$dirty = true;
                changePasswordForm.inputConfirmNewPassword.$dirty = true;
            }
            else if ($scope.changePasswordObject.NewPassword != $scope.changePasswordObject.ConfirmNewPassword) {
                $scope.ChangePasswordMessage = "New passwords must match";
            }
            else {
                $('#globalLoader').show();
                $scope.ChangePasswordMessage = "";
                var NewChangePasswordModel = { "Model": $scope.changePasswordObject, "UserId": sharedService.getUserId() };
                accountFactory.ChangePasswordFunction(NewChangePasswordModel)
                    .success(function (data) {
                        if (data.success) {
                            toaster.success("Success", data.message);
                            changePasswordForm.inputCurrentPassword.$dirty = false;
                            changePasswordForm.inputNewPassword.$dirty = false;
                            changePasswordForm.inputConfirmNewPassword.$dirty = false;
                            $('#modalChangePassword').modal('hide');
                        }
                        else {
                            toaster.warning("Warning", data.message);
                        }
                        $('#globalLoader').hide();
                    })

                    .error(function () {
                        toaster.error("Error", "Some Error Occured");
                        $('#globalLoader').hide();
                    });
            }
        }

        ////////////Date Functions/////////////
        /************************/
        /*$scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };
        var da = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
        };

        $scope.toggleMin();
        
        $scope.maxDate = new Date(da.getFullYear(), 5, 22);

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.setDate = function (year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
        //$scope.init = new Date(da.getFullYear()-10, 5, 22);
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,

        };
        
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[2];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
        
        $scope.getDayClass = function (date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        };
        */
        /***********************/

        // onload
        angular.element(document).ready(function () {
            var uid = $stateParams.uid;
            var iseditable = false;

            if (uid == sharedService.getUserId()) {
                iseditable = true;
            }

            $scope.profileUserId = uid;
            
            $scope.showProfile(uid, iseditable);
            $scope.checkIfCompanyIsVendor();
        });



        ///* Configuration of selectize for adding teammembers from Team Add Update Modal */
        //$scope.favouriteProjectsConfig = {
        //    create: false,
        //    maxItems: 3,
        //    required: true,
        //    plugins: ['remove_button'],
        //    valueField: 'ProjectId',
        //    labelField: 'ProjectName',
        //    // delimiter: '|',
        //    placeholder: 'Pick Favourite Projects...',

        //}


        ///* Configuration of selectize for adding teammembers from Team Add Update Modal */
        //$scope.SkillSetTagsConfig = {
        //    create: false,
        //    maxItems: 6,
        //    required: true,
        //    plugins: ['remove_button'],
        //    valueField: 'TagId',
        //    labelField: 'TagName',
        //    // delimiter: '|',
        //    placeholder: 'Pick Tags...',

        //}

        ///* Configuration of selectize for adding teammembers from Team Add Update Modal */
        //$scope.BusinessDomainExpertiseTagsConfig = {
        //    create: false,
        //    maxItems: 6,
        //    required: true,
        //    plugins: ['remove_button'],
        //    valueField: 'TagId',
        //    labelField: 'TagName',
        //    // delimiter: '|',
        //    placeholder: 'Pick Tags...',

        //}


        $scope.showProjectsForCompany = function (item) {

            $scope.userStatisticsProjects.UserProjectsForSelectedCompany = $filter('filter')($scope.userStatisticsProjects.UserProjects, { 'CompanyId': item.CompanyId });

            //$scope.showProjects = true;

            //$('#modalUserStatistics').modal('show');
        };


        $scope.showOrganizationsForFreelancer = function () {


            //if (!$scope.otherUser) {
            $scope.showOrganizationsWithEfficiency = false;
            $scope.showOrganizations = !$scope.showOrganizations;
            //}
            //$scope.showProjects = false;
        };


        $scope.showOrganizationsForFreelancerWithEfficiency = function () {

            $scope.showOrganizations = false;
            $scope.showOrganizationsWithEfficiency = !$scope.showOrganizationsWithEfficiency;
        };

        /*  This function is used to add new entry in User's Education Detail which will be saved in UserEducationDetail table  */
        $scope.saveUserEducationDetail = function (AddUpdateEducationDetailForm, isSchoolInvalid, isDegreeStreamInvalid, isGradeInvalid, isActivitiesAndSocietiesInvalid, isDescriptionInvalid, isDatesAttendedFromInvalid, isDatesAttendedToInvalid) {

            debugger;
            if (isSchoolInvalid || isDegreeStreamInvalid || isGradeInvalid || isActivitiesAndSocietiesInvalid || isDescriptionInvalid || isDatesAttendedFromInvalid || isDatesAttendedToInvalid) {
                AddUpdateEducationDetailForm.School.$dirty = true;
                AddUpdateEducationDetailForm.Degree.$dirty = true;
                AddUpdateEducationDetailForm.Grade.$dirty = true;
                AddUpdateEducationDetailForm.ActivitiesAndSocieties.$dirty = true;
                AddUpdateEducationDetailForm.Description.$dirty = true;
                AddUpdateEducationDetailForm.DatesAttendedFrom.$dirty = true;
                AddUpdateEducationDetailForm.DatesAttendedTo.$dirty = true;

            }
            else if ($scope.EducationDetailAddUpdateModel.DateAttendedFrom > $scope.EducationDetailAddUpdateModel.DateAttendedTo) {
                toaster.warning("Warming", "Dates Attended From can't be greater then Dates Attended to !");
                return;
            }
            else {

                if ($scope.EducationDetail.EducationalStream.length > 0) {
                    var EducationalStreamIds = 0;
                    for (var i = 0; i < $scope.EducationDetail.EducationalStream.length; i++) {
                        if (EducationalStreamIds == 0) {
                            EducationalStreamIds = $scope.EducationDetail.EducationalStream[i].Id;
                        }
                        else {
                            EducationalStreamIds += "," + $scope.EducationDetail.EducationalStream[i].Id;
                        }
                    }
                    $scope.EducationDetailAddUpdateModel.CommaSeperatedEducationalStream = EducationalStreamIds;
                }
                else {
                    toaster.warning("Warning", "Educational Stream should not be empty");
                    return;
                }

                $('#idEducationalDetails').show();
                $scope.EducationDetailAddUpdateModel.UserId = sharedService.getUserId();
                userProfileFactory.saveUserEducationDetail($scope.EducationDetailAddUpdateModel)
                .success(function (response) {

                    /*  Getting User Education Detail */
                    userProfileFactory.getUserEducationDetail(sharedService.getUserId())
                    .success(function (dataEducationDetail) {
                        $scope.UserEducationDetailObject = dataEducationDetail.ResponseData;

                        //clearign Education Fields
                        $scope.EducationDetail.EducationalStream = "";
                        AddUpdateEducationDetailForm.School.$dirty = false;
                        AddUpdateEducationDetailForm.Degree.$dirty = false;
                        AddUpdateEducationDetailForm.Grade.$dirty = false;
                        AddUpdateEducationDetailForm.ActivitiesAndSocieties.$dirty = false;
                        AddUpdateEducationDetailForm.Description.$dirty = false;
                        AddUpdateEducationDetailForm.DatesAttendedFrom.$dirty = false;
                        AddUpdateEducationDetailForm.DatesAttendedTo.$dirty = false;

                        $('#modalAddUpdateEducationDetail').modal('hide');
                        toaster.success("Success", response.ResponseData);
                        $('#idEducationalDetails').hide();
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                        $('#idEducationalDetails').hide();
                    });

                })
                .error(function (response) {
                    toaster.error("Error", response.message);
                    $('#idEducationalDetails').hide();
                })
            }
        };


        //function clearEducationFields(AddUpdateEducationDetailForm) {

        //    $scope.AddUpdateEducationDetailForm.Name.$dirty = false;
        //    $scope.AddUpdateEducationDetailForm.YearOfPassing.$dirty = false;
        //    $scope.AddUpdateEducationDetailForm.Subjects.$dirty = false;
        //    $scope.AddUpdateEducationDetailForm.Marks.$dirty = false;
        //    $scope.AddUpdateEducationDetailForm.Percentage.$dirty = false;
        //    $scope.AddUpdateEducationDetailForm.InstituteOrCollage.$dirty = false;
        //};


        /*  This function is used to edit existing entry in User's Education Detail which will is in UserEducationDetail table  */
        $scope.updateUserEducationDetail = function (AddUpdateEducationDetailForm, isSchoolInvalid, isDegreeStreamInvalid, isGradeInvalid, isActivitiesAndSocietiesInvalid, isDescriptionInvalid, isDatesAttendedFromInvalid, isDatesAttendedToInvalid) {

            if (isSchoolInvalid || isDegreeStreamInvalid || isGradeInvalid || isActivitiesAndSocietiesInvalid || isDescriptionInvalid || isDatesAttendedFromInvalid || isDatesAttendedToInvalid) {
                AddUpdateEducationDetailForm.School.$dirty = true;
                AddUpdateEducationDetailForm.Degree.$dirty = true;
                AddUpdateEducationDetailForm.Grade.$dirty = true;
                AddUpdateEducationDetailForm.ActivitiesAndSocieties.$dirty = true;
                AddUpdateEducationDetailForm.Description.$dirty = true;
                AddUpdateEducationDetailForm.DatesAttendedFrom.$dirty = true;
                AddUpdateEducationDetailForm.DatesAttendedTo.$dirty = true;

            }
            else if ($scope.EducationDetailAddUpdateModel.DateAttendedFrom > $scope.EducationDetailAddUpdateModel.DateAttendedTo) {
                toaster.warning("Warming", "Dates Attended From can't be greater then Dates Attended to !");
                return;
            }
            else {

                if ($scope.EducationDetail.EducationalStream.length > 0) {
                    var EducationalStreamIds = 0;
                    for (var i = 0; i < $scope.EducationDetail.EducationalStream.length; i++) {
                        if (EducationalStreamIds == 0) {
                            EducationalStreamIds = $scope.EducationDetail.EducationalStream[i].Id;
                        }
                        else {
                            EducationalStreamIds += "," + $scope.EducationDetail.EducationalStream[i].Id;
                        }
                    }
                    $scope.EducationDetailAddUpdateModel.CommaSeperatedEducationalStream = EducationalStreamIds;
                }
                else {
                    toaster.warning("Warming", "Educational Stream should not be empty");
                    return;
                }

                $('#idEducationalDetails').show();

                $scope.EducationDetailAddUpdateModel.UserId = sharedService.getUserId();
                userProfileFactory.updateUserEducationDetail($scope.EducationDetailAddUpdateModel)
                .success(function (response) {

                    userProfileFactory.getUserEducationDetail(sharedService.getUserId())
                              .success(function (data) {
                                  if (data.success) {
                                      $scope.UserEducationDetailObject = data.ResponseData;

                                      //clearign Education Fields
                                      $scope.EducationDetail.EducationalStream = "";
                                      AddUpdateEducationDetailForm.School.$dirty = false;
                                      AddUpdateEducationDetailForm.Degree.$dirty = false;
                                      AddUpdateEducationDetailForm.Grade.$dirty = false;
                                      AddUpdateEducationDetailForm.ActivitiesAndSocieties.$dirty = false;
                                      AddUpdateEducationDetailForm.Description.$dirty = false;
                                      AddUpdateEducationDetailForm.DatesAttendedFrom.$dirty = false;
                                      AddUpdateEducationDetailForm.DatesAttendedTo.$dirty = false;

                                      $('#modalAddUpdateEducationDetail').modal('hide');
                                      toaster.success("Success", message[response.message]);

                                  }
                                  else {
                                      toaster.error("Error", data.message);
                                  }
                                  $('#idEducationalDetails').hide();
                              })
                              .error(function () {
                                  toaster.error("Error", "Some Error Occured !");
                                  $('#idEducationalDetails').hide();
                              });

                })
                .error(function (response) {
                    toaster.error("Error", response.message);
                    $('#idEducationalDetails').hide();
                })
            }
        };



        /*  This function is used to add new entry in User's Education Detail which will be saved in UserEducationDetail table  */
        $scope.addUserEducationDetail = function () {

            $scope.EducationDetailButton = false;
            $scope.EducationDetailAddUpdateModel = {};
            $scope.EducationDetail.EducationalStream = "";
            //$scope.AddUpdateEducationDetailForm.Name.$dirty = false;
            //$scope.AddUpdateEducationDetailForm.YearOfPassing.$dirty = false;
            //$scope.AddUpdateEducationDetailForm.Subjects.$dirty = false;
            //$scope.AddUpdateEducationDetailForm.Marks.$dirty = false;
            //$scope.AddUpdateEducationDetailForm.Percentage.$dirty = false;
            //$scope.AddUpdateEducationDetailForm.InstituteOrCollage.$dirty = false;
            $('#modalAddUpdateEducationDetail').modal('show');

        };



        /*  This function is used to edit existing entry in User's Education Detail which will is in UserEducationDetail table  */
        $scope.editUserEducationDetail = function (EducationDetailObject) {

            $scope.EducationDetail.EducationalStream = EducationDetailObject.EducationalStream;

            $scope.EducationDetailButton = true;
            $scope.EducationDetailAddUpdateModel = {};
            $scope.EducationDetailAddUpdateModel.Id = EducationDetailObject.Id;
            $scope.EducationDetailAddUpdateModel.School = EducationDetailObject.School;
            $scope.EducationDetailAddUpdateModel.DateAttendedFrom = EducationDetailObject.DateAttendedFrom;
            $scope.EducationDetailAddUpdateModel.DateAttendedTo = EducationDetailObject.DateAttendedTo;
            $scope.EducationDetailAddUpdateModel.Degree = EducationDetailObject.Degree;
            $scope.EducationDetailAddUpdateModel.EducationalStream = $scope.EducationDetail.EducationalStream;
            $scope.EducationDetailAddUpdateModel.Grade = EducationDetailObject.Grade;
            $scope.EducationDetailAddUpdateModel.ActivitiesAndSocieties = EducationDetailObject.ActivitiesAndSocieties;
            $scope.EducationDetailAddUpdateModel.Description = EducationDetailObject.Description;

            $('#modalAddUpdateEducationDetail').modal('show');
        };




        /*  This function is used to delete User's Education Detail from UserEducationDetail table  */
        $scope.deleteUserEducationDetail = function (EducationDetailObjectId, index) {

            $confirm({ text: 'Are you sure you want to delete this Education Detail ?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
                .then(function () {

                    userProfileFactory.deleteUserEducationDetail(EducationDetailObjectId)
                       .success(function (response) {
                           $scope.UserEducationDetailObject.splice(index, 1);
                           toaster.success("Success", message[response.message]);
                           $('#globalLoader').hide();
                       })
                       .error(function (response) {
                           toaster.error("Error", response.message);
                           $('#globalLoader').hide();
                       })
                })

        };


        $scope.changeCurrentWorkingStatus = function (currentWorking) {

            if (currentWorking)
                $scope.ProfessionalDetailAddUpdateModel.To = new Date();
            else
                $scope.ProfessionalDetailAddUpdateModel.To = '';
        }


        /*  This function is used to add new entry in User's Professional Detail which will be saved in UserProfessionalDetail table  */
        $scope.saveUserProfessionalDetail = function (AddUpdateProfessionalDetailForm, isCompanyNameInvalid, isTitleInvalid, isLocationInvalid, isDescriptionInvalid, isFromInvalid, isToInvalid) {

            if (isCompanyNameInvalid || isTitleInvalid || isLocationInvalid || isDescriptionInvalid || isFromInvalid || isToInvalid) {
                AddUpdateProfessionalDetailForm.CompanyName.$dirty = true;
                AddUpdateProfessionalDetailForm.Title.$dirty = true;
                AddUpdateProfessionalDetailForm.Location.$dirty = true;
                AddUpdateProfessionalDetailForm.Description.$dirty = true;
                AddUpdateProfessionalDetailForm.From.$dirty = true;
                AddUpdateProfessionalDetailForm.To.$dirty = true;

            }
            else if ($scope.ProfessionalDetailAddUpdateModel.From > $scope.ProfessionalDetailAddUpdateModel.To) {
                toaster.warning("Warming", "Time period from can't be greater then time period to !");
                return;
            }
            else {
                $('#idProfessionalDetails').show();
                $scope.ProfessionalDetailAddUpdateModel.UserId = sharedService.getUserId();
                $scope.ProfessionalDetailAddUpdateModel.FromDate = $filter('date')($scope.ProfessionalDetailAddUpdateModel.From, "dd/MM/yyyy HH:mm:ss");
                $scope.ProfessionalDetailAddUpdateModel.ToDate = $filter('date')($scope.ProfessionalDetailAddUpdateModel.To, "dd/MM/yyyy HH:mm:ss");
                userProfileFactory.saveUserProfessionalDetail($scope.ProfessionalDetailAddUpdateModel)
                .success(function (response) {

                    /*  Getting User Professional Detail */
                    userProfileFactory.getUserProfessionalDetail(sharedService.getUserId())
                     .success(function (dataProfessionalDetail) {

                         $scope.UserProfessionalDetailObject = dataProfessionalDetail.ResponseData;
                         toaster.success("Success", message[response.message]);
                         $('#modalAddUpdateProfessionalDetail').modal('hide');
                         //clearing Professional fields
                         AddUpdateProfessionalDetailForm.CompanyName.$dirty = false;
                         AddUpdateProfessionalDetailForm.Title.$dirty = false;
                         AddUpdateProfessionalDetailForm.Location.$dirty = false;
                         AddUpdateProfessionalDetailForm.Description.$dirty = false;
                         AddUpdateProfessionalDetailForm.From.$dirty = false;
                         AddUpdateProfessionalDetailForm.To.$dirty = false;

                         $('#idProfessionalDetails').hide();
                     })
                     .error(function () {
                         toaster.error("Error", "Some Error Occured !");
                         $('#idProfessionalDetails').hide();
                     });
                })
                .error(function (response) {
                    toaster.error("Error", response.message);
                    $('#idProfessionalDetails').hide();
                })
            }

        };


        function clearfields(AddUpdateProfessionalDetailForm) {
            $scope.AddUpdateProfessionalDetailForm.Company.$dirty = false;
            $scope.AddUpdateProfessionalDetailForm.YearsWorked.$dirty = false;
            $scope.AddUpdateProfessionalDetailForm.Designation.$dirty = false;
            $scope.AddUpdateProfessionalDetailForm.HourlyRate.$dirty = false;
            $scope.AddUpdateProfessionalDetailForm.salary.$dirty = false;
            $scope.AddUpdateProfessionalDetailForm.HoursWorked.$dirty = false;
        };


        /*  This function is used to update existing entry in User's Professional Detail which will is in UserProfessionalDetail table  */
        $scope.updateUserProfessionalDetail = function (AddUpdateProfessionalDetailForm, isCompanyNameInvalid, isTitleInvalid, isLocationInvalid, isDescriptionInvalid, isFromInvalid, isToInvalid) {

            if (isCompanyNameInvalid || isTitleInvalid || isLocationInvalid || isDescriptionInvalid || isFromInvalid || isToInvalid) {
                AddUpdateProfessionalDetailForm.CompanyName.$dirty = true;
                AddUpdateProfessionalDetailForm.Title.$dirty = true;
                AddUpdateProfessionalDetailForm.Location.$dirty = true;
                AddUpdateProfessionalDetailForm.Description.$dirty = true;
                AddUpdateProfessionalDetailForm.From.$dirty = true;
                AddUpdateProfessionalDetailForm.To.$dirty = true;

            }
            else if ($scope.ProfessionalDetailAddUpdateModel.From > $scope.ProfessionalDetailAddUpdateModel.To) {
                toaster.warning("Warming", "Time period from can't be greater then time period to !");
                return;
            }
            else {

                $('#idProfessionalDetails').show();
                $scope.ProfessionalDetailAddUpdateModel.UserId = sharedService.getUserId();
                userProfileFactory.updateUserProfessionalDetail($scope.ProfessionalDetailAddUpdateModel)
                .success(function (response) {

                    /*  Getting User Professional Detail */
                    userProfileFactory.getUserProfessionalDetail(sharedService.getUserId())
                     .success(function (dataProfessionalDetail) {

                         $scope.UserProfessionalDetailObject = dataProfessionalDetail.ResponseData;
                         toaster.success("Success", response.message);
                         $('#modalAddUpdateProfessionalDetail').modal('hide');
                         //clearfields(AddUpdateProfessionalDetailForm);
                         $('#idProfessionalDetails').hide();
                     })
                     .error(function () {
                         toaster.error("Error", "Some Error Occured !");
                         $('#idProfessionalDetails').hide();
                     });

                })
                .error(function (response) {
                    toaster.error("Error", response.message);
                    $('#idProfessionalDetails').hide();
                })

            }

        };

        /*  This function is used to open AddUpdateProfessionalDetail Modal to add new entry in User's Professional Detail which will be saved in UserProfessionalDetail table  */
        $scope.addUserProfessionalDetail = function () {
            //$('#globalLoader').show();
            $scope.ProfessionalDetailButton = false;
            $scope.ProfessionalDetailAddUpdateModel = {};

            //userProfileFactory.getTagsForProfessionalDetails($scope.ProfessionalDetailAddUpdateModel)
            //   .success(function (response) {
            //       $scope.BusinessDomainExpertise = response.ResponseData.BusinessDomainExpertise;
            //       $scope.TechnicalSkillSet = response.ResponseData.TechnicalSkillSet;
            //       $('#globalLoader').hide();
            //   })
            //   .error(function (response) {
            //       toaster.error("Error", response.message);
            //       $('#globalLoader').hide();
            //   })

            $scope.currentWorking = false;
            $('#modalAddUpdateProfessionalDetail').modal('show');
        };



        /*  This function is used to edit existing entry in User's Professional Detail which will is in UserProfessionalDetail table  */
        $scope.editUserProfessionalDetail = function (ProfessionalDetailObject) {

            $scope.ProfessionalDetailButton = true;
            $scope.ProfessionalDetailAddUpdateModel = {};
            $scope.ProfessionalDetailAddUpdateModel.Id = ProfessionalDetailObject.Id;
            $scope.ProfessionalDetailAddUpdateModel.CompanyName = ProfessionalDetailObject.CompanyName;
            //$scope.ProfessionalDetailAddUpdateModel.YearsWorked = ProfessionalDetailObject.YearsWorked;
            $scope.ProfessionalDetailAddUpdateModel.From = $filter('date')(ProfessionalDetailObject.From, "dd.MM.yyyy");
            $scope.ProfessionalDetailAddUpdateModel.To = $filter('date')(ProfessionalDetailObject.To, "dd.`MM.yyyy");
            $scope.ProfessionalDetailAddUpdateModel.Title = ProfessionalDetailObject.Title;
            $scope.ProfessionalDetailAddUpdateModel.Location = ProfessionalDetailObject.Location;
            $scope.ProfessionalDetailAddUpdateModel.Description = ProfessionalDetailObject.Description;


            $('#modalAddUpdateProfessionalDetail').modal('show');
        };


        /*  This function is used to delete User's Education Detail from UserEducationDetail table  */
        $scope.deleteUserProfessionalDetail = function (ProfessionalDetailObjectId, index) {

            $confirm({ text: 'Are you sure you want to delete this Professional Detail ?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
                .then(function () {
                    $('#idProfessionalDetails').show();
                    userProfileFactory.deleteUserProfessionalDetail(ProfessionalDetailObjectId)
                       .success(function (response) {
                           $scope.UserProfessionalDetailObject.splice(index, 1);
                           toaster.success("Success", response.message);
                           $('#idProfessionalDetails').hide();
                       })
                       .error(function (response) {
                           toaster.error("Error", response.message);
                           $('#idProfessionalDetails').hide();
                       })

                })

        };



        /*  This function is used to edit Hourly Rate  */

        $scope.editHourlyRate = function () {
            $scope.editableHourlyRate = $scope.HourlyRate;
            $scope.openSaveHourlyRate = true;
        }



        /*  This function is used to save Hourly Rate  */

        $scope.updateHourlyRate = function () {

            if ($scope.HourlyRate == undefined) {
                toaster.warning("Warning", "Please provide hourly rate");
                return;
            }
            //$('#globalLoader').show();
            userProfileFactory.updateUserHourlyRate(sharedService.getUserId(), $scope.HourlyRate)
                .success(function (response) {

                    $scope.openSaveHourlyRate = false;
                    //$scope.HourlyRate = $scope.newHourlyRate
                    //toaster.success("Success", response.ResponseData);
                    //$('#globalLoader').hide();
                })
                .error(function (response) {
                    toaster.error("Error", response.message);
                    //$('#globalLoader').hide();
                })

        }


        /*  This function is used to cancel update of Hourly Rate  */

        $scope.cancelHourlyRate = function (s) {
            $scope.HourlyRate = $scope.editableHourlyRate;
            $scope.openSaveHourlyRate = false;
        }





        /*  This function is used to save User Availabilty  */
        $scope.editAvailabilty = function (item) {
            
            $scope.editableAvailability = $scope.user.SelectedAvailability;
            $scope.openSaveAvailabilty = true;
            $scope.user.SelectedAvailability = true;

            if (item != "") {
                if (item == 10) {
                    $scope.user.SelectedAvailability = 10;
                }
                else if (item == 20) {
                    $scope.user.SelectedAvailability = 20;
                    
                }
                else if (item == 30) {
                    $scope.user.SelectedAvailability = 30
                }
                else if (item == 40) {
                    $scope.user.SelectedAvailability = 40

                }

                else if (item == 41) {
                    $scope.user.SelectedAvailability = 41

                }
            }




        }


        /*  This function is used to save User Availabilty  */
        $scope.cancelAvailabilty = function () {
            $scope.user.SelectedAvailability = $scope.editableAvailability;
            $scope.openSaveAvailabilty = false;
        }


        /*  This function is used to save User Availabilty  */
        $scope.updateAvailabilty = function () {
            
            if ($scope.user.SelectedAvailability != null) {
                userProfileFactory.updateUserAvailabilty($scope.profileUserId, $scope.user.SelectedAvailability)
                   .success(function (UserAvailabilty) {
                       $scope.openSaveAvailabilty = false;

                       userProfileFactory.getUserAvailabilty($scope.profileUserId)
                          .success(function (dataUserAvailabilty) {
                              //$scope.user.SelectedAvailability = dataUserAvailabilty.ResponseData.Availabilty;

                              //toaster.success("Success", UserAvailabilty.ResponseData);
                              //$('#globalLoader').hide();
                          })
                        .error(function () {
                            toaster.error("Error", "Some Error Occured !");
                        });

                   })
                   .error(function (UserTags) {
                       toaster.error("Error", UserTags.message);
                       //$('#globalLoader').hide();
                   })

            }
            else {
                toaster.warning("Warning", "Please select the dropdown value");
            }

            var asd = $scope.user.SelectedAvailability;

        }



        /*  #region Skill Set Tags    */

        /*  This function is used to edit User Skill Set Tags  */
        $scope.editSkillSetTags = function () {
            

            multiSelectCurrentData = $scope.Tags.SkillSetTags;
            for (var i = 0; i < multiSelectCurrentData.length; i++) {
                multiSelectPreviousData.push(multiSelectCurrentData[i]);
            }
            $scope.openSaveSkillSetTags = true;

            $timeout(function () {
                $('#SkillSetTags  .host .tags .input').focus();
            }, 100);
        }



        /*  This function is used to edit User Business Domain Tags  */
        $scope.editBusinessDomainTags = function () {
            debugger;
            multiSelectCurrentData = $scope.Tags.BusinessDomainExpertiseTags;
            for (var i = 0; i < multiSelectCurrentData.length; i++) {
                multiSelectPreviousData.push(multiSelectCurrentData[i]);
            }
            $scope.openSaveBusinessDomainTags = true;
            $timeout(function () {
                $('#BusinessDomainTags  .host .tags .input').focus();
            }, 100);
        }



        //$scope.editTags = function () {
        //    //userProfileFactory.getUserTags($scope.profileUserId)
        //    //               .success(function (dataTags) {
        //    //                   $scope.Tags = {};
        //    //                   $scope.Tags.userTags = dataTags.ResponseData;
        //    //                   toaster.success("Success", dataTags.message);
        //    //                   $('#globalLoader').hide();
        //    //               })
        //    //                .error(function (dataTags) {
        //    //                    toaster.error("Error", dataTags.message);
        //    //                    $('#globalLoader').hide();
        //    //                })
        //    $scope.openSaveTags = true;
        //}



        /*  This function is used to cancel editing User Skill Set Tags  */
        $scope.cancelSkillSetTags = function () {


            $scope.Tags.SkillSetTags = multiSelectPreviousData;
            multiSelectPreviousData = [];
            $scope.openSaveSkillSetTags = false;
        }


        /*  This function is used to cancel editing User Business Domain Tags  */
        $scope.cancelBusinessDomainTags = function () {

            $scope.Tags.BusinessDomainExpertiseTags = multiSelectPreviousData;
            multiSelectPreviousData = [];
            $scope.openSaveBusinessDomainTags = false;
        }


        /*  This function is used to save User Skill Set Tags  */
        $scope.saveSkillSetTags = function () {
            debugger;
            if ($scope.Tags.SkillSetTags.length > 0) {
                var data = { 'UserId': $scope.profileUserId, 'Tags': $scope.Tags.SkillSetTags, 'TagType': 'SkillSetTags' };
                userProfileFactory.saveUserTags(data)
                       .success(function (UserTags) {
                           $scope.openSaveSkillSetTags = false;

                           userProfileFactory.getUserSkillSetTags($scope.profileUserId)
                              .success(function (dataUserTags) {
                                  $scope.allSkillSetTagsObject = dataUserTags.ResponseData.allSkillSetTagsObject;
                                  $scope.allBusinessDomainObject = dataUserTags.ResponseData.allBusinessDomainObject;
                                  $scope.Tags = {};
                                  $scope.Tags.SkillSetTags = dataUserTags.ResponseData.SkillSetTags;
                                  $scope.Tags.BusinessDomainExpertiseTags = dataUserTags.ResponseData.BusinessDomainExpertiseTags;

                                  toaster.success("Success", UserTags.ResponseData);
                                  $('#globalLoader').hide();
                              })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured !");
                            });
                       })
                       .error(function (UserTags) {
                           toaster.error("Error", UserTags.message);
                           $('#globalLoader').hide();
                       })
            }
            else {
                toaster.warning("Warning", message["SelectTagWarning"]);
            }

        }



        /*  This function is used to save User Skill Set Tags  */
        $scope.saveBusinessDomainTags = function () {

            if ($scope.Tags.BusinessDomainExpertiseTags.length > 0) {
                var data = { 'UserId': $scope.profileUserId, 'Tags': $scope.Tags.BusinessDomainExpertiseTags, 'TagType': 'BusinessDomainTags' };
                userProfileFactory.saveUserTags(data)
                       .success(function (UserTags) {
                           $scope.openSaveBusinessDomainTags = false;

                           userProfileFactory.getUserSkillSetTags($scope.profileUserId)
                              .success(function (dataUserTags) {
                                  $scope.allSkillSetTagsObject = dataUserTags.ResponseData.allSkillSetTagsObject;
                                  $scope.allBusinessDomainObject = dataUserTags.ResponseData.allBusinessDomainObject;
                                  $scope.Tags = {};
                                  $scope.Tags.SkillSetTags = dataUserTags.ResponseData.SkillSetTags;
                                  $scope.Tags.BusinessDomainExpertiseTags = dataUserTags.ResponseData.BusinessDomainExpertiseTags;

                                  toaster.success("Success", UserTags.ResponseData);
                                  $('#globalLoader').hide();
                              })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured !");
                            });
                       })
                       .error(function (UserTags) {
                           toaster.error("Error", UserTags.message);
                           $('#globalLoader').hide();
                       })
            }
            else {
                toaster.warning("Warning", message["SelectTagWarning"]);
            }

        }


        /*  #endregion Tags  */




        /*  #region Favourite Projects  */

        /*  This function is used to edit User Favourite Projects  */
        $scope.editFavouriteProjects = function () {
            debugger
            multiSelectCurrentData = $scope.FavProjects.userFavouriteProjects;
            for (var i = 0; i < multiSelectCurrentData.length; i++) {
                multiSelectPreviousData.push(multiSelectCurrentData[i]);
            }

            $scope.openSaveFavouriteProjects = true;
            $timeout(function () {
                $('#FavouriteProjects  .host .tags .input').focus();
            }, 100);
        }



        /*  This function is used to cancel editing User Favourite Projects  */
        $scope.cancelFavouriteProjects = function () {
            $scope.FavProjects.userFavouriteProjects = multiSelectPreviousData;
            multiSelectPreviousData = [];
            $scope.openSaveFavouriteProjects = false;
        }


        /*  This function is used to save User Favourite Projects  */
        $scope.saveFavouriteProjects = function () {

            if ($scope.FavProjects.userFavouriteProjects.length > 0) {

                if ($scope.FavProjects.userFavouriteProjects.length > 3) {
                    toaster.warning("Warning", message["MaxFavProjectWarning"]);
                    $scope.openSaveFavouriteProjects = false;
                    return;
                }

                var data = { 'UserId': sharedService.getUserId(), 'FavouriteProjectsId': $scope.FavProjects.userFavouriteProjects };
                userProfileFactory.saveUserFavouriteProjects(data)
                       .success(function (responseUserFavouriteProjects) {
                           $scope.openSaveFavouriteProjects = false;

                           userProfileFactory.getUserFavouriteProjects($scope.profileUserId)
                               .success(function (dataFavouriteProjects) {
                                   $scope.FavProjects = {};
                                   if (dataFavouriteProjects.ResponseData != null) {
                                       $scope.FavProjects.userFavouriteProjects = dataFavouriteProjects.ResponseData.userFavProjects;
                                   }
                                   toaster.success("Success", responseUserFavouriteProjects.ResponseData);
                                   $('#globalLoader').hide();
                               })
                                .error(function (dataFavouriteProjects) {
                                    toaster.error("Error", dataFavouriteProjects.message);
                                    $('#globalLoader').hide();
                                })

                       })
                       .error(function (ResponseData) {
                           toaster.error("Error", ResponseData.message);
                           $('#globalLoader').hide();
                       })
            }
            else {
                toaster.warning("Warning", message["SelectFavProjectWarning"]);
            }

        }

        /*  #endregion Favourite Projects  */




        /*  #region Testimonial  */

        /*  This function is used to edit User Testimonials  */
        $scope.editTestimonial = function () {
            multiSelectPreviousData = [];
            multiSelectCurrentData = $scope.UserTestimonials.UserTestimonialList;
            for (var i = 0; i < multiSelectCurrentData.length; i++) {
                multiSelectPreviousData.push(multiSelectCurrentData[i]);
            }

            $scope.openSaveTestimonials = true;
        }



        /*  This function is used to cancel editing User Testimonials  */
        $scope.cancelTestimonial = function () {
            $scope.UserTestimonials.UserTestimonialList = multiSelectPreviousData;
            multiSelectPreviousData = [];
            $scope.openSaveTestimonials = false;
        }


        /*  This function is used to save User Favourite Projects  */
        $scope.saveTestimonial = function () {

            if ($scope.UserTestimonials.UserTestimonialList.length > 0) {

                if ($scope.FavProjects.userFavouriteProjects.length > 3) {
                    toaster.warning("Warning", message["MaxSelectTagWarning"]);
                    return;
                }

                var data = {};
                data.Testimonial = $scope.UserTestimonials.UserTestimonialList;
                data.UserId = sharedService.getUserId();
                //var data = { 'UserId': sharedService.getUserId(), 'Testimonials': $scope.UserTestimonials.UserTestimonialList };
                userProfileFactory.saveUserTestimonials(data)
                       .success(function (responseUserTestimonials) {
                           $scope.openSaveTestimonials = false;

                           userProfileFactory.getUserTestimonials(sharedService.getUserId())
                           .success(function (dataUserTestimonials) {

                               $scope.UserTestimonials = dataUserTestimonials.ResponseData;
                               toaster.success("Success", responseUserTestimonials.ResponseData);
                               $('#globalLoader').hide();
                           })
                           .error(function () {
                               toaster.error("Error", "Some Error Occured !");
                               $('#globalLoader').hide();
                           });


                       })
                       .error(function (ResponseData) {
                           toaster.error("Error", ResponseData.message);
                           $('#globalLoader').hide();
                       })
            }
            else {
                toaster.warning("Warning", message["SelectTestimonialWarning"]);
            }

        }

        /*  #endregion Favourite Projects  */





        /*  #region Hire Me  */

        $scope.showHireMeModal = function () {

            $('#globalLoader').show();
            //$scope.userModel = {};
            //$scope.userModel.email = "";
            //$scope.isEmailDirty = false;
            //$scope.isFirstNameDirty = false;
            //$scope.isLastNameDirty = false;
            //$scope.isUserRoleDirty = false;
            debugger;
            userProfileFactory.getUserEmailFromUserId($scope.profileUserId)
             .success(function (dataEmailIdForHire) {
                 if (dataEmailIdForHire.ResponseData != null) {
                     $scope.EmailIdForHire = dataEmailIdForHire.ResponseData;

                     var data = { 'EmailOrHandle': $scope.EmailIdForHire, 'CompanyId': sharedService.getCompanyId(), 'chkInvitationType': 'email' };
                     FreelancerInvitationFactory.confirmFreelancerEmailOrHandle(data)
              .success(function (response) {

                  if (response.ResponseData.PreviousProjectsList != null) {
                      if (response.ResponseData.PreviousProjectsList.length == 1) {
                          $scope.FreelancerModel.RoleId = response.ResponseData.LastRoleInCompany;
                      }
                  }

                  $scope.confirmFreelancerObject = response.ResponseData;
                  var objForAllProjectsOfCompany = { CompanyId: sharedService.getCompanyId(), UserId: $scope.profileUserId, EmailOrHandle: $scope.EmailIdForHire, chkInvitationType: 'email' };
                  usersFactory.GetAllProjectsOfCompany(objForAllProjectsOfCompany)
                                   .success(function (dataAllProjectsOfCompany) {
                                       $scope.ProjectsObject = dataAllProjectsOfCompany.ResponseData;
                                       bindProjectDdl();

                                       $scope.FreelancerModel = {};
                                       $('#modalHireMe').modal('show');
                                       $('#globalLoader').hide();

                                   })
                                   .error(function (dataAllProjectsOfCompany) {
                                       toaster.error(message[dataAllProjectsOfCompany.message]);
                                   })
                  $('#globalLoader').hide();
              })
              .error(function (response) {
                  toaster.error(message[response]);
                  $('#globalLoader').hide();
              })

                 }
                 else {
                     toaster.error("Error", dataEmailIdForHire.message);
                 }
             })
             .error(function () {
                 toaster.error("Error", "Some Error Occured !");
             });


        };



        function bindProjectDdl() {


            if ($scope.ProjectsObject != undefined) {
                var projects = $scope.ProjectsObject;
                var allProjects = [], i = 0;
                for (i = 0; i < projects.length; i++) {
                    var jsonItem = {};
                    jsonItem.Value = projects[i].ProjectId;
                    jsonItem.Text = projects[i].ProjectName;
                    jsonItem.ticked = false;
                    if (projects[i].isTicked > 0) {
                        jsonItem.false = true;
                    }
                    else {
                        jsonItem.false = false;
                    }

                    allProjects.push(jsonItem);
                }
                $scope.newModel = allProjects;
                $scope.output = [];
            }
        };



        //// Checking wheather the input character is number or not 
        //$scope.isNumber = function (event) {
        //    
        //    if (event.keyCode > 47 && event.keyCode < 58) {

        //        return true;
        //    }
        //    else {
        //        event.preventDefault();
        //        return false;
        //    }
        //};






        $scope.sendInvitationToFreelancer = function (event, addNewUserForm) {

            $('#globalLoader').show();
            var outputModel = $scope.newModel;
            var i = 0;
            var AllProjectId = [];
            for (i; i < outputModel.length; i++) {
                if (outputModel[i].false == true) {
                    AllProjectId.push(outputModel[i].Value);
                }
            }

            if ($scope.isFreelancerAllreadyAttachedToCompany == true && AllProjectId.length == 0) {
                toaster.error("Error", "Project is not selected");
                $('#globalLoader').hide();
                return;
            }

            var modeldata = {};
            $scope.FreelancerModel.EmailOrHandle = $scope.EmailIdForHire;
            modeldata = $scope.FreelancerModel;
            modeldata.ProjectIdList = AllProjectId;
            modeldata.SentBy = sharedService.getUserId();
            modeldata.CompanyId = sharedService.getCompanyId();
            modeldata.invitationType = 'email';

            // IsHireMe = true 
            // if its value is true then we will save Job Title And Job Description
            modeldata.IsHireMe = true;

            //var modeldata = { 'FreelancerModel': $scope.FreelancerModel, 'SentBy': sharedService.getUserId(),'CompanyId': sharedService.geCompanyId() };
            usersFactory.insertFreelancer(modeldata)
                .success(function (data) {
                    if (data.success == true) {
                        toaster.success("Success", message[data.message]);
                        $('#modalHireMe').modal('hide');
                        $('#globalLoader').hide();
                        /* Getting Users Details */
                    }
                    else {
                        toaster.warning("Warning", message[data.message]);
                        $('#globalLoader').hide();
                    }
                })
                .error(function (data) {
                    toaster.error("Error", message[data.message]);
                    $('#globalLoader').hide();
                })


        };




        /*  #endregion Hire Me  */



        //$scope.toggleId = [];

        // opening the concern child for other users feedback of project
        $scope.openSubChild = function (ProjectId) {
            if ($scope.toggleId == ProjectId) {
                $scope.toggleId = 0;
            }
            $scope.toggleId = ProjectId;
        };




        //   Checking wheather the input chaaracter is number or not 
        $scope.isNotNumber = function (event) {
            if (event.keyCode > 47 && event.keyCode < 58) {
                event.preventDefault();
                return false;
            }
            else {
                //event.preventDefault();
                return true;
            }
        };



        $scope.userFirstNameClick = function () {
            $scope.toggleUserFirstName = !$scope.toggleUserFirstName;
            $timeout(function () {
                $('input[name="firstName"]').focus();
            }, 100);
        }

        $scope.updateUserFirstName = function () {

            $scope.toggleUserFirstName = !$scope.toggleUserFirstName;
            $scope.editUserProfileObject = {};
            $scope.editUserProfileObject.userId = sharedService.getUserId();
            $scope.editUserProfileObject.firstName = $scope.UserProfileObject.firstName;
            usersFactory.updateUserFirstName($scope.editUserProfileObject)
                .success(function (data) {
                    $scope.$emit('bindUserDetail', []);
                })
                .error(function (data) {
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.userLastNameClick = function () {
            $scope.toggleUserLastName = !$scope.toggleUserLastName;
            $timeout(function () {
                $('input[name="lastName"]').focus();
            }, 100);
        }

        $scope.updateUserLastName = function () {

            $scope.toggleUserLastName = !$scope.toggleUserLastName;
            $scope.editUserProfileObject = {};
            $scope.editUserProfileObject.userId = sharedService.getUserId();
            $scope.editUserProfileObject.lastName = $scope.UserProfileObject.lastName;
            usersFactory.updateUserLastName($scope.editUserProfileObject)
                .success(function (data) {
                    $scope.$emit('bindUserDetail', []);
                })
                .error(function (data) {
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.userAddressClick = function () {
            debugger;
            $scope.toggleUserAddress = !$scope.toggleUserAddress;
            $timeout(function () {
                $('#Address').focus();
            }, 100);
        }

        $scope.updateUserAddress = function () {

            $scope.toggleUserAddress = !$scope.toggleUserAddress;
            $scope.editUserProfileObject = {};
            $scope.editUserProfileObject.userId = sharedService.getUserId();
            $scope.editUserProfileObject.Address = $scope.UserProfileObject.Address;
            usersFactory.updateUserAddress($scope.editUserProfileObject)
                .success(function (data) {
                })
                .error(function (data) {
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.userPhoneNoClick = function () {
            $scope.toggleUserPhoneNo = !$scope.toggleUserPhoneNo;
            $timeout(function () {
                $('input[name="PhoneNo"]').focus();
            }, 100);
        }

        $scope.updateUserPhoneNo = function () {

            $scope.toggleUserPhoneNo = !$scope.toggleUserPhoneNo;
            $scope.editUserProfileObject = {};
            $scope.editUserProfileObject.userId = sharedService.getUserId();
            $scope.editUserProfileObject.PhoneNo = $scope.UserProfileObject.PhoneNo;
            usersFactory.updateUserPhoneNo($scope.editUserProfileObject)
                .success(function (data) {
                })
                .error(function (data) {
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.userDesignationClick = function () {
            $scope.toggleUserDesignation = !$scope.toggleUserDesignation;
            $timeout(function () {
                $('input[name="Designation"]').focus();
            }, 100);
        }

        $scope.updateUserDesignation = function () {

            $scope.toggleUserDesignation = !$scope.toggleUserDesignation;
            $scope.editUserProfileObject = {};
            $scope.editUserProfileObject.userId = sharedService.getUserId();
            $scope.editUserProfileObject.Designation = $scope.UserProfileObject.Designation;
            usersFactory.updateUserDesignation($scope.editUserProfileObject)
                .success(function (data) {
                })
                .error(function (data) {
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.userAboutMeClick = function () {
            $scope.toggleUserAboutMe = !$scope.toggleUserAboutMe;
            $timeout(function () {
                $('#AboutMe').focus();
            }, 100);
        }

        $scope.updateUserAboutMe = function () {

            $scope.toggleUserAboutMe = !$scope.toggleUserAboutMe;
            $scope.editUserProfileObject = {};
            $scope.editUserProfileObject.userId = sharedService.getUserId();
            $scope.editUserProfileObject.AboutMe = $scope.UserProfileObject.AboutMe;
            usersFactory.updateUserAboutMe($scope.editUserProfileObject)
                .success(function (data) {
                })
                .error(function (data) {
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.clickOnGender = function (value) {

            if (value == 1) {
                $("#radioMale").prop("checked", true);
                $("#radioFemale").prop("checked", false);
            }
            else if (value == 2) {
                $("#radioMale").prop("checked", false);
                $("#radioFemale").prop("checked", true);
            }

            $scope.editUserProfileObject = {};
            $scope.editUserProfileObject.userId = sharedService.getUserId();
            $scope.editUserProfileObject.gender = value;
            usersFactory.updateUserGender($scope.editUserProfileObject)
                .success(function (data) {
                })
                .error(function (data) {
                    toaster.error("Error", "Some Error Occured");
                })
        }

        $scope.updateUserDateOfBirth = function () {

            $scope.editUserProfileObject = {};
            $scope.editUserProfileObject.userId = sharedService.getUserId();
            $scope.editUserProfileObject.DateOfBirth = $scope.UserProfileObject.DateOfBirth;
            usersFactory.updateUserDateOfBirth($scope.editUserProfileObject)
                .success(function (data) {
                })
                .error(function (data) {
                    toaster.error("Error", "Some Error Occured");
                })
        }

        /*Code for toggle button to enable general company  to vendor company */


        $scope.updateGeneralCompanyToVendorCompany = function () {
            debugger
            usersFactory.updateGeneralCompanyToVendorCompany($scope.currentUserId, $scope.currentCompanyId)
                .success(function (data) {
                    debugger;
                    $scope.IsVendor = data.ResponseData;
                    toaster.success("Success", "Company enabled as vendor company");
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });

        }



    });
});