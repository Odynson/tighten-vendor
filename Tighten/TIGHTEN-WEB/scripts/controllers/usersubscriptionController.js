﻿
define(['app', 'userDashboardFactory', 'usersFactory', 'accountFactory', 'subscriptionFactory', 'transactionsFactory'], function (app) {
    app.controller("userSubscriptionController", function ($scope, $rootScope, $cookieStore, $filter, apiURL,stripSetting, $timeout, $http, $location, sharedService, toaster, userDashboardFactory, usersFactory, accountFactory, subscriptionFactory, transactionsFactory,invoiceFactory) {

        /*  region Stripe  Starts Here   */
        debugger;
        //This is Publish Key For Stripe


        Stripe.setPublishableKey('pk_test_HLVaHbwHkZYpehMozE1z3fnz');
        $scope.isSubsctriptionPaymentDone = false;
        
        $scope.PayNow = function (event, PaymentInfo) {
            // Request a token from Stripe:

            var isValid = $scope.checkPaymentValidation(event, PaymentInfo);
            if (isValid) {
                $('#globalLoader').show();
                var $form = $('#payment-form');
                $scope.paymentType = 1;
                Stripe.card.createToken($form, stripeResponseHandler);
            }
        }
                
        $scope.modalPayNow = function () {

            $('#globalLoader').show();
            var $form = $('#payment-form1');
            $scope.paymentType = 2;
            //$form.find('.submit').prop('disabled', true);
            Stripe.card.createToken($form, stripeResponseHandler);
        }
        
        function stripeResponseHandler(status, response) {
            // Grab the form:
            var $form = $('#payment-form1');

            if (response.error) { // Problem!
                // Show the errors on the form:
                //$form.find('.payment-errors').text(response.error.message);
                toaster.error("Error", response.error.message);
                $form.find('.submit').prop('disabled', false); // Re-enable submission
                $('#globalLoader').hide();
            }
            else { // Token was created!

                // Get the token ID:
                var token = response.id;

                var data = { Token: response.id, Amount: $scope.Total, CompanyId: sharedService.getCompanyId() };
                // calling method on server to complete the payment process
                subscriptionFactory.SubsctriptionPayment(data)
               .success(function (SubsctriptionPaymentData) {
                   if (SubsctriptionPaymentData.success) {
                       $scope.isSubsctriptionPaymentDone = true;
                       $scope.SubsctriptionPaymentStart(SubsctriptionPaymentData.ResponseData);

                   }
               })
               .error(function () {
                   toaster.error("Error", "Some Error Occured !");
                   $('#globalLoader').hide();
               });


            }
        };
        
        /*  region Stripe  End Here   */
        
        $('#headerMenu').hide();
        $('#sideMenu').hide();
        $('#sideMenu').parent().parent().hide();
        $scope.PackageId = 0;
        $scope.CalculatePackageCost = [];
        $scope.cardType = "";
        $scope.isPackageUpgrade = false;
        $scope.filter = function (homes /*, thisp*/) {
            var res = [];
            $.map(homes, function (val, key) {
                debugger;
                if (val.Id > 1)
                    res.push(val);
            });
            return res;
        };
        $scope.PaymentData = {};
        $scope.allCardsForPaymentDetailObject = {};
        $scope.paymentType = 1;
        $scope.sectionName = "UserSubscription";

        $scope.PackagesBinding = function () {
            debugger;
            $('#globalLoader').show();
            userDashboardFactory.getAllPackages(sharedService.getCompanyId())
                .success(function (data) {
                    debugger;
                    //data.ResponseData = $filter('filter')(data.ResponseData, { Id : 1 }, true);
                    data.ResponseData = $scope.filter(data.ResponseData)
                    $scope.AllPackageObject = data.ResponseData;
                    $('#modalUpgradePackage').modal('show');
                    $('#globalLoader').hide();
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                });
        };

        $scope.PackagesBinding();

        $scope.upgradePackage = function () {


            var SelectedPackage, Price, PackageName
            angular.forEach($scope.AllPackageObject, function (obj) {
                if (obj.IsActive == true) {
                    SelectedPackage = obj.Id;
                    Price = obj.Price;
                    PackageName = obj.Name;
                }
            });
            $scope.PackageId = SelectedPackage;
            $scope.PackageCostPerUser = []

            if ($scope.PackageId == undefined) {
                toaster.warning("Warning", "You have to select a package first !!");
                return false;
            }
            else {
                $('#modalUpgradePackage').modal('hide');
                $('#globalLoader').show();
            }
            usersFactory.getActiveUsersCount(sharedService.getCompanyId()).success(function (data) {
                var UsersCount = data.ResponseData;
                var Total = 0

                var Total = parseInt(UsersCount) * parseInt(Price)
                var PackageCost = { PackageId: SelectedPackage, Item: PackageName, UsersCount: UsersCount, PricePerUser: Price, SubTotal: Total }
                $scope.CalculatePackageCost.push(PackageCost);
                $scope.Total = Total;
                $('#modalCalculatePackageCost').show();
                $('#globalLoader').hide();
            });
            //usersFactory.getUsers(sharedService.getUserId(), false, sharedService.getCompanyId())// here false is used for chkEmailConfirmed
            //    .success(function (data) {

            //        if (data.success) {
            //            $scope.UsersObject = data.ResponseData;
            //            $scope.filteredFreelancersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: true }, true);
            //            $scope.filteredUsersObject = $filter('filter')($scope.UsersObject, { IsFreelancer: false }, true);

            //            var Total = 0
            //            angular.forEach($scope.filteredUsersObject, function (obj) {

            //                if (obj.IsEmailConfirmed == true) {
            //                    var PackageCost = { ProfilePhoto: 'http://localhost:55580/' + obj.ProfilePhoto, UserName: obj.FirstName + ' ' + obj.LastName, PricePerUser: Price }
            //                    $scope.PackageCostPerUser.push(PackageCost);
            //                }
            //            });

            //            var UsersCount = $scope.filteredUsersObject.length;
            //            var Total = 0
            //            var Total = parseInt(UsersCount) * parseInt(Price)
            //            var PackageCost = { PackageId: SelectedPackage, Item: PackageName, UsersCount: UsersCount, PricePerUser: Price, SubTotal: Total }
            //            $scope.CalculatePackageCost.push(PackageCost);
            //            var Total = parseInt(UsersCount) * parseInt(Price)
            //            $scope.Total = Total;
            //            $('#modalCalculatePackageCost').show();
            //            $('#globalLoader').hide();
            //        }
            //    });
        }

        $scope.proceedToPayment = function () {
            $('#modalCalculatePackageCost').hide();
            $('#globalLoader').show();
            var SelectedPackage, Price, PackageName
            angular.forEach($scope.AllPackageObject, function (obj) {
                if (obj.IsActive == true) {
                    SelectedPackage = obj.Id;
                    Price = obj.Price;
                    PackageName = obj.Name;
                }
            });
            $scope.PackageId = SelectedPackage;
            
            $scope.PackageId = SelectedPackage;
            usersFactory.isUserCardInfoExist(sharedService.getUserId(), sharedService.getCompanyId()).success(function (dataSub) {
                if (dataSub.ResponseData == false) {
                    usersFactory.getUserloginUserCardDetail(sharedService.getUserId(), sharedService.getCompanyId()).success(function (data) {
                        data.ResponseData.CvvNumber = "";
                        $scope.PaymentData = data.ResponseData;

                        $('#invoicePackage').show();
                        $('#globalLoader').hide();
                    });
                }
                else {
                    //invoiceFactory.getAllCardsForPaymentDetail(sharedService.getCompanyId())
                    //.success(function (resultAllCards) {
                    //    $scope.allCardsForPaymentDetailObject.AllCards = resultAllCards.ResponseData;
                    //    $scope.allCardsForPaymentDetailObject.InvoiceId = "";
                    //    $scope.allCardsForPaymentDetailObject.index = "";

                    //    $('#modalPaymentDetail').modal('show');
                    //    $('#globalLoader').hide();
                    //})
                    //.error(function () {
                    //    $('#globalLoader').hide();
                    //    toaster.error("Error", message["error"]);
                    //})


                    invoiceFactory.getCardDetailForPayment(0, sharedService.getCompanyId(), 0)
                    .success(function (result) {
                        $scope.userCardDetail = result.ResponseData;

                        $('#modalPaymentDetail').modal('hide');
                        $('#modalInvoicePackage').modal('show');
                        $('#globalLoader').hide();
                    })
                    .error(function () {
                        $('#globalLoader').hide();
                        toaster.error("Error", message["error"]);
                    })


                }
            });

        }
        
        $scope.selectPaymentDetail = function (InvoiceId, index) {
            
            $('#globalLoader').show();

            var SelectedPaymentDetailId = 0;
            angular.forEach($scope.allCardsForPaymentDetailObject.AllCards, function (obj) {
                if (obj.IsSelected == true) {
                    SelectedPaymentDetailId = obj.Id;
                }
            });

            if (SelectedPaymentDetailId == 0) {
                toaster.warning("Warning", "Please select any of the payment detail option");
                $('#globalLoader').hide();
                return;
            }
            invoiceFactory.getCardDetailForPayment(SelectedPaymentDetailId, sharedService.getCompanyId(), 0)
            .success(function (result) {
                $scope.userCardDetail = result.ResponseData;


                $('#modalPaymentDetail').modal('hide');
                $('#modalInvoicePackage').modal('show');
                $('#globalLoader').hide();
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message["error"]);
            })
        }
        
        $scope.clearAllCheckBoxes = function (accessid) {
            angular.forEach($scope.allCardsForPaymentDetailObject.AllCards, function (obj) {
                obj.IsSelected = false;
                if (obj.Id == accessid) {
                    obj.IsSelected = true;
                }
            });
        }
                
        $scope.SubsctriptionPaymentStart = function (chargeId) {

            //var isValid = $scope.checkPaymentValidation(event, PaymentInfo);
            //if (isValid) {
            if ($scope.isSubsctriptionPaymentDone) {
                if ($scope.paymentType == 1) {
                    $('#invoicePackage').hide();
                }
                else {
                    $('#modalInvoicePackage').modal('hide');
                }
                
                //$('#globalLoader').show();
                $timeout(function () {
                    $scope.PaymentData.PackageId = $scope.PackageId;
                    $scope.PaymentData.CompanyId = sharedService.getCompanyId();
                    $scope.PaymentData.UserId = sharedService.getUserId();
                    $scope.PaymentData.PaymentType = $scope.paymentType;
                    var subscription = $scope.PaymentData
                    subscriptionFactory.insertSubscription(subscription)
                        .success(function (dataSub) {
                            var transactions = { 'EventID': dataSub.ResponseData, 'AmountPaid': $scope.Total, 'StirpeTransactionID': chargeId, 'TransactionBy': sharedService.getUserId(), 'TransactionType': 'Subscription', 'TransactionFor': sharedService.getUserId() }
                            transactionsFactory.insertTransaction(transactions)
                                .success(function (dataTran) {

                                    var obj = $cookieStore.get('sharedService')
                                    obj.IsSubscriptionsEnds = false
                                    $cookieStore.remove('sharedService');
                                    $cookieStore.put('sharedService', obj)
                                    $scope.transctionId = dataTran.ResponseData;
                                    $('#divThankYou').show();
                                    $('#globalLoader').hide();
                                    $('.modal-backdrop').removeClass("in");
                                });
                        });
                }, 5000);
            }
            else {
                $('#globalLoader').hide();
            }
            //}
        }

        $scope.changeCardType = function () {
            if ($scope.PaymentData.CreditCard !== "") {
                if ($scope.PaymentData.CreditCard.startsWith("3")) {
                    $scope.cardType = "fa fa-cc-amex";
                }
                else if ($scope.PaymentData.CreditCard.startsWith("4")) {
                    $scope.cardType = "fa fa-cc-visa";
                }
                else if ($scope.PaymentData.CreditCard.startsWith("5")) {
                    $scope.cardType = "fa fa-cc-mastercard";
                }
                else if ($scope.PaymentData.CreditCard.startsWith("6")) {
                    $scope.cardType = "fa fa-cc-discover";
                }
                else {
                    $scope.cardType = "";
                }
            }
            else {
                $scope.cardType = "";
            }
        };

        $scope.getYearsList = function (range) {
            var currentYear = new Date().getFullYear();
            var years = [];
            for (var i = 0; i < range; i++) {
                years.push(currentYear + i);
            }
            return years;
        };

        $scope.checkPaymentValidation = function (event, PaymentInfo) {

            var isvalid = true;
            if (PaymentInfo.CreditCard != undefined) {
                if (PaymentInfo.CreditCard.$invalid) {
                    PaymentInfo.CreditCard.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide Credit Card Number';
                    //return;
                }
            }
            if (PaymentInfo.ExpiryMonth != undefined) {
                if (PaymentInfo.ExpiryMonth.$invalid) {
                    PaymentInfo.ExpiryMonth.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide Expiry Month';
                    //return;
                }
            }
            if (PaymentInfo.ExpiryYear != undefined) {
                if (PaymentInfo.ExpiryYear.$invalid) {
                    PaymentInfo.ExpiryYear.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide Expiry Year';
                    //return;
                }
            }
            if (PaymentInfo.CvvNumber != undefined) {
                if (PaymentInfo.CvvNumber.$invalid) {
                    PaymentInfo.CvvNumber.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide Cvv Number';
                    //return;
                }
            }
            if (PaymentInfo.CardName != undefined) {
                if (PaymentInfo.CardName.$invalid) {
                    PaymentInfo.CardName.$dirty = true;
                    isvalid = false;
                    //$scope.message = 'You must provide CardHolder Name';
                    //return;
                }
            }

            return isvalid;
        };

        $scope.getRandomSpan = function () {
            //return Math.floor((Math.random() * 6) + 1);
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
            }
            return s4() + '-' + s4()
            //return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        };

        $scope.clearAllCheckBoxesUpgradePackage = function (accessid) {
            angular.forEach($scope.AllPackageObject, function (obj) {
                obj.IsActive = false;
                if (obj.Id == accessid) {
                    obj.IsActive = true;
                }
            });
        }

        $scope.goToDashBoard = function () {
            $location.path('/dashboard/u');
        }

        $scope.cancelUpgradePackage = function () {
            $scope.logout();
        }
    });
});