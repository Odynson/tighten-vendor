﻿
define(['app'], function (app) {
    app.controller("projectController", function ($scope, $rootScope, apiURL, $filter, $timeout, $http, $location, $upload, sharedService, $confirm, toaster, projectsFactory, teamFactory, FreelancerAccountFactory, $sce) {



        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  

        window.Selectize = require('selectize');
        $scope.testing = "test";
        //alert("alert");
        var baseurl = apiURL.baseAddress;
        $scope.imgURL = apiURL.imageAddress;


        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }


        $scope.projectAddUpdateModel = {};
        $scope.projectAddUpdateModel.TeamMembers = [];
        $scope.projectAddUpdateModel.ProjectAttachments = [];
        $scope.myProjectMember = {};
        $scope.myProjectMember.teamMembers = [];
        $scope.addClientFeedbackModel = {};
        $scope.AllProjectOfCompanyObject = {};
        $scope.FreelancerModel = {};
        $scope.FreelancerObject = {};
        $scope.teamNameForTitle = "";
        $scope.freelancerDetails = [];
        $scope.showAttachmentDescription = false;
        $scope.projectMode = "add";
        $scope.showMilestone = false;

        //$scope.Permission = {};
        bindFreelancerDdl();

        // #region freelancer
        // this section is for freelancer
        //if ($rootScope.activeTeam.TeamId == undefined) {
        //    $rootScope.activeTeam.TeamId = sharedService.getDefaultTeamId();
        //}

        //$scope.selectedTeamId = $rootScope.activeTeam.TeamId;
        $scope.isFreelancer = sharedService.getIsFreelancer();

        var range = [];
        for (var i = 1; i < 6; i++) {
            range.push(i);
        }
        $scope.availableRange = range;


        $scope.allAccessLevel = [
                     { id: 1, name: "Private", selected: false },
                     { id: 2, name: "Semi-Public", selected: false },
                     { id: 3, name: "Public", selected: true }
        ];

        $scope.clearAllCheckBoxes = function (accessid) {
            debugger;
            angular.forEach($scope.allAccessLevel, function (id) {
                id.selected = false;
                if (id.id == accessid) {
                    id.selected = true;
                }
            });
        };

        //if ($cookieStore.get('globals') != undefined) {
        //    var globals = $cookieStore.get('globals');
        //    $scope.selectedCompanyId = globals.currentUser.CompanyId;
        //    //sharedService.setCompanyId($scope.selectedCompanyId);
        //    sharedService.setShared(globals.currentUser);
        //}
        //else {
        //    $scope.selectedCompanyId = 0;
        //}


        $scope.selectedCompanyId = sharedService.getCompanyId();
        bindTeamsForFreelancer($scope.selectedCompanyId);
        $scope.projectAddUpdateForm = {};
        $scope.Role = sharedService.getRoleId();


        // Company dropdown change event on left side menu for freelancer 
        $scope.$on('leftCompanyDropdownChange', function (event, data) {

            $scope.selectedCompanyId = data.CompanyId;
            $scope.selectedCompanymodel = data.CompanyId;
            bindTeamsForFreelancer(data.CompanyId);
            $scope.selectedTeamId = $scope.selectedTeam;
            //$scope.showProjectManagementSection();
        });

        $scope.$on('leftTeamDropdownChange', function (event, data) {

            //$scope.selectedTeamId = $rootScope.activeTeam.TeamId;
            //$scope.showProjectManagementSection();
        });


        $scope.selectedCompanyChanged = function (selectedCompany) {

            $scope.selectedCompanyId = selectedCompany;
            bindTeamsForFreelancer(selectedCompany);
            $scope.selectedTeamId = $scope.selectedTeam;
            //$scope.showProjectManagementSection();
        };


        $scope.selectedTeamChanged = function (selectedTeam) {

            $scope.selectedTeamId = selectedTeam;
            $scope.selectedTeam = selectedTeam;
            //$scope.showProjectManagementSection();
        };


        function bindTeamsForFreelancer(compId) {

            teamFactory.getLoggedInUserTeams(sharedService.getUserId(), compId).success(function (data) {
                if (data.success) {
                    $scope.TeamsObjectForFreelancer = data.ResponseData;
                    $scope.selectedTeam = 0;
                }
                else {
                    toaster.error("Error", data.message);
                }
            }).error(function () {
                toaster.error("Error", "Some Error Occured!");
            });
        };


        /* Showing Project Management Section to see the information*/
        $scope.showProjectManagementSection = function () {
            //closeCentreSections();

            if ($rootScope.activeTeam.TeamName != undefined) {
                $scope.teamName = $rootScope.activeTeam.TeamName;
                $scope.teamNameForTitle = $rootScope.activeTeam.TeamName + "'s";
            }
            else {
                teamFactory.getTeam($scope.selectedTeamId, sharedService.getUserId())
                   .success(function (Teamdata) {
                       $scope.TeamsObject = Teamdata.ResponseData;
                       $scope.teamName = $scope.TeamsObject.TeamName;
                       $scope.teamNameForTitle = $scope.teamName + "'s";
                   })
                   .error(function () {
                       toaster.error("Error", "Some Error Occured!");
                   })
            }

            $('#globalLoader').show();
            $scope.showCenterLoader = true;
            $scope.selectedProject = 0;
            $scope.inputSearchProject = "";
            $scope.selectedTodo = 0;
            $scope.selectedNavigation = "ProjectManagementSection";
            var test = $scope.selectedTeamId;

            /* Getting User Owned Projects */
            projectsFactory.getUserOwnedProjects($scope.selectedTeamId, sharedService.getUserId(), sharedService.getRoleId(), $scope.selectedCompanyId)
                .success(function (data) {
                    if (data.success) {
                        $scope.UserOwnedProjectsObject = data.ResponseData;
                        $scope.showCenterLoader = false;
                        $('#divProjectManagementSection').show();
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", data.message);
                        $('#globalLoader').hide();
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                    $('#globalLoader').hide();
                });
        };



        /* go to addUpdateProject */
        $scope.showProjectAddUpdateModal = function () {
            $location.path('/addupdateproject/0');
        };


        /* ------------- Manage Projects With Freelancers Starts Here -----------------*/
        $scope.showManageProjectsWithFreelancerModal = function () {

            $('#globalLoader').show();
            $scope.FreelancerModel = {};
            $scope.newModel = {};
            $("input[name='UserRate']").removeClass("inputError");
            $("input[name='WeeklyLimit']").removeClass("inputError");

            projectsFactory.GetAllProjectsOfCompany(sharedService.getCompanyId(), $scope.selectedTeamId)
                 .success(function (data) {
                     $scope.AllProjectOfCompanyObject = data.ResponseData;
                     $('#modalManageProjectsWithFreelancer').modal('show');
                     $('#globalLoader').hide();
                 })
                 .error(function (data) {
                     toaster.error("Error", "Some Error Occured!");
                     $('#globalLoader').hide();
                 })
        }

        $scope.getFreelancerForSelectedProject = function (ProjectId) {

            $('#globalLoader').show();
            projectsFactory.getAllActiveFreelancerOfProject(ProjectId)
                 .success(function (ProjectsData) {
                     $scope.FreelancerObject = ProjectsData.ResponseData;
                     bindFreelancerDdl();
                     $('#globalLoader').hide();
                 })
                 .error(function (data) {
                     toaster.error("Error", "Some Error Occured!");
                     $('#globalLoader').hide();
                 })
        }


        $scope.saveProjectOptionsForFreelancer = function (isUserRateInvalid, isWeeklyLimitInvalid, manageProjectsWithFreelancerForm) {

            if (isUserRateInvalid || isWeeklyLimitInvalid) {
                manageProjectsWithFreelancerForm.UserRate.$dirty = true;
                manageProjectsWithFreelancerForm.WeeklyLimit.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                var outputModel = $scope.newModel;
                var i = 0;
                var AllFreelancerId = [];
                for (i; i < outputModel.length; i++) {
                    if (outputModel[i].false == true) {
                        AllFreelancerId.push(outputModel[i].Value);
                    }
                }
                if (AllFreelancerId.length == 0) {
                    toaster.error("Error", "Freelancer is not selected");
                    $('#globalLoader').hide();
                    return;
                }

                var model = {};
                model = $scope.FreelancerModel;
                model.FreelancerIdList = AllFreelancerId;

                projectsFactory.saveProjectOptionsForFreelancer(model)
                     .success(function (data) {
                         if (data.success == true) {
                             toaster.success("Success", data.ResponseData);
                             $('#modalManageProjectsWithFreelancer').modal('hide');
                             $('#globalLoader').hide();
                             manageProjectsWithFreelancerForm.UserRate.$dirty = false;
                             manageProjectsWithFreelancerForm.WeeklyLimit.$dirty = false;
                         }
                         else {
                             toaster.warning("Warning", "Some Error Occured!");
                             $('#globalLoader').hide();
                         }
                     })
                     .error(function (data) {
                         toaster.error("Error", "Some Error Occured!");
                         $('#globalLoader').hide();
                     })
            }
        }


        function bindFreelancerDdl() {

            if ($scope.FreelancerObject != undefined) {
                var Freelancers = $scope.FreelancerObject;
                var allFreelancers = [], i = 0;
                for (i = 0; i < Freelancers.length; i++) {
                    var jsonItem = {};
                    jsonItem.Value = Freelancers[i].UserId;
                    jsonItem.Text = Freelancers[i].FreelancerName;
                    jsonItem.ticked = false;
                    if (Freelancers[i].isTicked > 0) {
                        jsonItem.false = true;
                    }
                    else {
                        jsonItem.false = false;
                    }
                    allFreelancers.push(jsonItem);
                }
                $scope.newModel = allFreelancers;
                $scope.output = [];
            }
        };


        /* ------------- Manage Projects With Freelancers Ends Here ------------- */

        ///* Add new Project */

        /* Edit Project*/
        $scope.editProject = function (projectObject) {

            $location.path('/addupdateproject/' + projectObject.ProjectId);
        };


        /* Delete Project */
        $scope.deleteProject = function (projectId, index) {
            $confirm({ text: 'Are you sure you want to delete this Project?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
          .then(function () {
              $('#globalLoader').show();
              projectsFactory.deleteProject(projectId, sharedService.getCompanyId(), $scope.isFreelancer)
                  .success(function (data) {
                      if (data.success) {
                          toaster.success("Success", data.message);
                          $scope.UserOwnedProjectsObject.splice(index, 1);
                          $scope.$emit('showMenu', []);
                          /* Getting Projects of User for a particular team in which user is project member */
                          getProjects();
                          $timeout(function () {
                              $scope.$apply();
                              $('#globalLoader').hide();
                          });
                      }
                      else {
                          if (data.message == "InvoicePresentInProject") {
                              toaster.warning("Warning", message[data.message]);
                          }
                          else {
                              toaster.error("Error", data.message);
                          }
                          $('#globalLoader').hide();
                      }
                  })
                  .error(function () {
                      toaster.error("Error", "Some Error Occured !");
                      $('#globalLoader').hide();
                  });
          });
        };


        /* Open screen for Project feedback */
        $scope.projectFeedback = function (projectId) {
            $location.path('/project/feedback/' + projectId);
        }




        function getProjects() {
            projectsFactory.getProjects(sharedService.getUserId(), $scope.selectedTeamId, $scope.selectedCompanyId, sharedService.getIsFreelancer())
                .success(function (data) {
                    if (data.success) {
                        $scope.projects = data.ResponseData;
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });
        }
        CompanyId = sharedService.getCompanyId()
        angular.element(document).ready(function () {
            //$scope.showProjectManagementSection();


        });

        $scope.AddNewProjectModel = {
            ProjectName: "",
            ProjectBudget: 0,
            ProjectHours: 0,
            ProjectDescription: "",
            FileName: "",
            ActualFileName: "",
            FilePath: "",
            UserId: sharedService.getUserId(),
            CompanyId: sharedService.getCompanyId(),
            VendorList: [],
            PhaseList: []


        }

        $scope.Phase = { Name: "", Budget: "", Description: "", PhaseDocumentAttached: [], PhaseDocumentsName: [], MileStone: [] }


        $scope.phaseModel = { Name: "", Budget: 0, Description: "", }
        $scope.MileStoneModel = { Name: "", Description: "", EstimatedHours: 0 }
        $scope.PhaseDocumentAttachedView = [];



        $scope.openPhasePopUp = function () {

            $('#modalAddNewPhase').modal('show');

        };


        $scope.addNewPhase = function (Phase) {

            debugger;
            $('#globalLoader').show();
            var data = { Name: "", Budget: "", Description: "", PhaseDocumentAttached: [], MileStone: [], PhaseDocumentsName: [] };
            data.Name = Phase.Name;
            data.Budget = Phase.Budget;
            data.Description = Phase.Description;
            data.PhaseDocumentAttached = $scope.PhaseDocumentAttachedView;

            var leng = 1;


            angular.forEach(data.PhaseDocumentAttached, function (attchmnt) {
                $('#globalLoader').show();
                $upload.upload({
                    url: baseurl + "ProjectsApi/UploadFileForAttachments", // webapi url
                    method: "POST",
                    //data: { UploadedFile: $file, UserId: sharedService.getUserId() },
                    data: { UploadedFile: attchmnt },
                    file: attchmnt
                })
                .progress(function (evt) {
                    // get upload percentage
                    $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                })
                .success(function (dataUpload, status, headers, config) {
                    // file is uploaded successfully
                    if (dataUpload.success) {
                        debugger;

                        var atchData = {
                            FileName: dataUpload.ResponseData.FileName,
                            ActualFileName: dataUpload.ResponseData.ActualFileName,
                            FilePath: dataUpload.ResponseData.FilePath
                        };

                        data.PhaseDocumentsName.push(atchData);
                        if (data.PhaseDocumentAttached.length == leng) {

                            $scope.AddNewProjectModel.PhaseList.push(data);
                            $('#modalAddNewPhase').modal('hide');
                            $scope.Phase = {};
                            $scope.PhaseDocumentAttachedView = [];
                        }

                        leng++
                    }
                    else {
                        toaster.error("Error", message[dataUpload.message]);


                    }

                    $('#globalLoader').hide();

                })
                .error(function (data, status, headers, config) {
                    // file failed to upload
                    //customIndex = customIndex + 1;
                    toaster.error("Error", "File failed to upload ");
                    $('#globalLoader').hide();
                });
            });


            $('#globalLoader').hide();

            if (data.PhaseDocumentAttached.length == 0) {
                $scope.AddNewProjectModel.PhaseList.push(data);
                $('#modalAddNewPhase').modal('hide');
                $scope.Phase = {};
                $scope.PhaseDocumentAttachedView = [];
            }



        }
        /////////////////////////////selectFileForDocumentPhase1//////////////////////////////////////////////////
        var PhaseIndex;
        $scope.selectFileForDocumentPhase1 = function ($files, index) {
            debugger;
            for (var i = 0; i < $files.length; i++) {
                $scope.PhaseDocumentAttachedView.push($files[i])
            }
        }

        ///////////////////////////////////////openMileStonePopUp/////////////////////////////////////////////////////////////////////////////////////
        $scope.openMileStonePopUp = function (index) {
            debugger;
            $('#modalAddNewMileStone').modal('show');
            PhaseIndex = index; // for update mile stone 
        }

        //////////////////////////////////addNewMilestone //////////////////////////////////////////////////////////////////
        $scope.addNewMilestone = function (MileStoneModel) {
            debugger;



            $scope.AddNewProjectModel.PhaseList[PhaseIndex].MileStone.push(MileStoneModel);
            $('#modalAddNewMileStone').modal('hide');
            $scope.PhaseDocumentAttachedView = [];
            $scope.MileStoneModel = {}
        }



        /////////////////////////////////////////RemoveMileStone///////////////////////////////////////////////////

        $scope.RemoveMileStone = function (ParentIndex, index) {

            $scope.AddNewProjectModel.PhaseList[ParentIndex].MileStone.splice(index, 1);

        }


        ///////////////////////////////removePhaseAttachments///////////////////////////////////////
        $scope.removePhaseAttachments = function (index, parentIndex) {
            debugger
            for (var i = 0; i < $scope.PhaseDocumentAttachedView.length; i++) {
                $scope.PhaseDocumentAttachedView.splice(index, 1)

            }


        }
        ////////////////////////////selectFileForProjectAttachent/////////////////////////////////////////
        var ProjectDocumentAttached = [];
        $scope.selectNewFileForProjectAttachent = function ($files) {

            ProjectDocumentAttached = $files[0];

        }

        ////////////////////////////createNewProject/////////////////////////////////////////////////////////

        $scope.createNewProject = function (projectAddUpdateModel, selected) {
            debugger;

            $scope.AddNewProjectModel.ProjectName = projectAddUpdateModel.projectName,
            $scope.AddNewProjectModel.ProjectDescription = projectAddUpdateModel.projectDescription,
            $scope.AddNewProjectModel.ProjectHours = projectAddUpdateModel.projectHours,
            $scope.AddNewProjectModel.ProjectBudget = projectAddUpdateModel.projectBudget,
            $scope.AddNewProjectModel.AccessLevel = 3;




            debugger;

            if (ProjectDocumentAttached.size > 0) {

                $upload.upload({
                    url: baseurl + "ProjectsApi/UploadFileForAttachments",
                    method: "POST",
                    data: { UploadedFile: ProjectDocumentAttached },
                    file: ProjectDocumentAttached
                })
                 .progress(function (evt) {
                     $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                 })
                  .success(function (dataUpload, status, headers, config) {
                      if (dataUpload.success) {
                          debugger;

                          $scope.AddNewProjectModel.FileName = dataUpload.ResponseData.FileName,
                             $scope.AddNewProjectModel.ActualFileName = dataUpload.ResponseData.ActualFileName,
                             $scope.AddNewProjectModel.FilePath = dataUpload.ResponseData.FilePath

                          SaveAllProjectData($scope.AddNewProjectModel); // save all project data
                      }

                  })
                     .error(function (data, status, headers, config) {

                         attachmentIndex = attachmentIndex + 1;
                         if (attachmentIndex == flncr.Attachments.length) {
                             customIndex = customIndex + 1;
                         }

                         toaster.error("Error", "File failed to upload ");
                         $('#globalLoader').hide();
                     });

            }
            else {
                SaveAllProjectData($scope.AddNewProjectModel)

            }


        }



        function SaveAllProjectData(AllProjectData) {
            $('#globalLoader').show();
            projectsFactory.createNewProject(AllProjectData)
               .success(function (data) {
                   debugger;
                   if (data.ResponseData != null) {
                       toaster.success("Success", message.NewProjectCreateSuccesfully);
                   }

                   $scope.AddNewProjectModel = {
                       ProjectName: "", ProjectBudget: 0, ProjectHours: 0, ProjectDescription: "", FileName: "",
                       ActualFileName: "",
                       FilePath: "",
                       UserId: sharedService.getUserId(),
                       CompanyId: sharedService.getCompanyId(),
                       VendorList: [],
                       PhaseList: []


                   }

                   $scope.projectAddUpdateModel = {};
                   $scope.VendorModelItem = "";  // empty vendor 

               }).error(function (data, status, headers, config) {
                   toaster.error("Error", "Some Error Occured !");
                   $('#globalLoader').hide();
               });

            $('#globalLoader').hide();
        }

        ////////////////////////////showMileforPhase /////////////////////////////////////////////////////////////


        $scope.showMileforPhase = function (showMilestone) {
            $scope.showMilestone = !showMilestone
        }


        //////////////////////////////////////////editPhase//////////////////////////////////////////////////////////
        $scope.editPhase = function (index) {

            $scope.AddNewProjectModel[index].PhaseList;

        }

        //////////////////////////////////////////deletePhase//////////////////////////////////////////////////////////
        $scope.deletePhase = function (index) {

            //$scope.AddNewProjectModel[index].PhaseList

        }
        var CompanyId = sharedService.getCompanyId()
        var statusDropId = 0
        function getProjectVendor(CompanyId, ProjectId, statusDropId, Name) {
            debugger
            dataModal = {}
            dataModal.CompanyId = CompanyId;
            dataModal.ProjectId = ProjectId;
            dataModal.Id = statusDropId;
            dataModal.Name = Name;

            projectsFactory.getProjectVendor(dataModal)
        .success(function (data) {
            $scope.myProjectVendor = data.ResponseData;

        }).error(function (data, status, headers, config) {
            toaster.error("Error", "Some Error Occured !");
            $('#globalLoader').hide();
        });

        }

        ////////////////////////////////////////////////ViewProjectQuote///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $scope.ViewProjectQuote = function (ProjectId) {
            $('#modalViewProjectQuotes').modal('show');
            $('#globalLoader').show();
            debugger;
            projectsFactory.getQuotedSubmitbyVendor(ProjectId)
            .success(function (data) {
                if (data.success) {
                    $scope.getQuotedSubmitbyVendor = data.ResponseData;
                }
                $('#globalLoader').hide();

            })
        }




        /////////////////////////getting project  FOR company profile/////////////////
        var dataModal = { Id: 0, CompanyId: CompanyId, Name: "", VendorId: 0, ProjectId: 0, RequestType: 2, PageIndex: 1, PageSize: 5 } // Id is status dropdown
        var ProjectName = ''
        var statusDropId = 0
        getQuotedProjectList(dataModal)
        function getQuotedProjectList(dataModal) {
            debugger;
            $('#globalLoader').show();
            dataModal.CompanyId = CompanyId;


            projectsFactory.getQuotedProjectList(dataModal)
            .success(function (data) {
                if (data.success) {
                    if (data.ResponseData != null) {
                        $scope.quotedProjectListRes = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCount = data.ResponseData[0].TotalPageCount;
                        }
                    }
                    else {
                        toaster.error("Error", data.message);

                    }
                    $('#globalLoader').hide();
                }
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            });


        }

        /////////////////////////////////////openModalProjectandVendorDetail///////////////////////////////////////////////////////////
        $scope.openModalProjectandVendorDetail = function (ProjectId) {


            $('#modalProjectandVendorDetail').modal('show');

            //getProjectAndVendorDetail(ProjectId, CompanyId)
            //$('#globalLoader').show();
        }

        $scope.qouteReceivedDropList = [
            { Id: 0, Status: '--Select All --' },
            { Id: 1, Status: 'Quotes Received' },
            { Id: 2, Status: 'no Quotes Received' }


        ]
        $scope.qouteReceivedDropModal = $scope.qouteReceivedDropList[0];

        $scope.FilterByQuotedReceived = function (qouteReceivedDropModal) {
            debugger;
            statusDropId = qouteReceivedDropModal.Id;
            dataModal.ProjectId = 0;
            dataModal.Id = statusDropId
            getQuotedProjectList(dataModal);

        }


        //////////////////////////////////////////--autoCompleteOptions---////////////////////////////////////////////////////
        $scope.autoCompleteOptions = {

            minimumChars: 3,
            dropdownWidth: '500px',
            dropdownHeight: '200px',
            data: function (term) {
                return $http.post(baseurl + 'ProjectsApi/GetProjectName',
                    dataModal = { Id: 0, CompanyId: CompanyId, Name: term, VendorId: 0, ProjectId: 0, RequestType: 2, PageIndex: 1, PageSize: 5 })
                    .then(function (data) {
                        debugger;
                        return data.data.ResponseData;
                    });
            },
            renderItem: function (item) {
                return {
                    value: item.Name,
                    label: $sce.trustAsHtml(
                    "<p class='auto-complete'>"
                    + item.Name +
                    "</p>")

                };

            },
            itemSelected: function (item) {
                debugger;
                dataModal.ProjectId = item.item.ProjectId
                $scope.searchQuotedProjectName = item.item.Name;
                getQuotedProjectList()
            }
        }

        //////////////////////////////////////////projectVendorConfig For Multi Drop down//////////////////////////////////////////////////////////

        $scope.projectVendorConfig = {

            create: false,
            // maxItems: 1,
            required: true,
            plugins: ['remove_button'],
            valueField: 'Id',
            labelField: 'Name',
            // delimiter: '|',
            placeholder: 'Pick Project Vendor...',

            render: {
                option: function (VendorModelItem, escape) {
                    debugger;
                    var label = VendorModelItem.Id;
                    var caption = VendorModelItem.Name;
                    var iconByType = "fa fa-compress";

                    return '<div>' +
                    '<span >  <i class="' + iconByType + '" aria-hidden="true"></i>   </span> ' +
                    (caption ? '<span>' + escape(caption) + '</span>' : '') +
                    '</div>';

                }
            },
            onItemAdd: function onItemAdd(value, $item) {
                debugger;

                $scope.AddNewProjectModel.VendorList.push({ Id: parseInt(value) })
            },
            onItemRemove: function onItemAdd(value, $item) {

                angular.forEach($scope.AddNewProjectModel.VendorList, function (item1) {

                    var index = $scope.AddNewProjectModel.VendorList.indexOf(item1);
                    $scope.AddNewProjectModel.VendorList.splice(index, 1);
                    $scope.$apply();

                });

            }

        }


        var isCtrlPressed;
        var isAPressed;
        /////////////////////////////empty text box project to quote companyProfile//////////////////////////////////////////////////////
        $scope.searchQuotedProjectNameEmpty = function (searchQuotedProjectName, event) {
            debugger;
            if (searchQuotedProjectName != undefined && searchQuotedProjectName.length <= 1 && event.keyCode == 8) {

                dataModal.Name = "";
                dataModal.ProjectId = 0
                getQuotedProjectList(dataModal)
            }
            // if key is pressed
            if (event.keyCode == 17 || isCtrlPressed) {
                isCtrlPressed = true;
            }
            else {
                isCtrlPressed = false;
            }

            // other key after the control
            if (isCtrlPressed) {
                if (event.keyCode == 97 || event.keyCode == 65) {
                    isAPressed = true;

                }
            }
            else {
                isAPressed = false;
            }


            if (isCtrlPressed && isAPressed) {
                if (event.keyCode == 46 || event.keyCode == 8) {
                    dataModal.Name = "";
                    dataModal.ProjectId = 0
                    getQuotedProjectList(dataModal)

                    isCtrlPressed = false;
                    isAPressed = false;
                }

            }
            var text = window.getSelection().toString()
            if (text != "" && (event.keyCode == 8 || event.keyCode == 46)) {
                dataModal.Name = "";
                dataModal.ProjectId = 0
                getQuotedProjectList(dataModal)
            }
        }


        $scope.cutSearchQuotedProjectByProjectName = function () {
            debugger
            dataModal.Name = "";
            dataModal.ProjectId = 0
            getQuotedProjectList(dataModal)

        }


        ////////////////////////////////////------------paging code/////////////////////////////////
        $scope.pagesizeList = [
       { PageSize: 5 },
       { PageSize: 10 },
       { PageSize: 25 },
       { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];




        $scope.changePageSize = function (pageSizeSelected) {
            debugger;
            dataModal.PageSize = pageSizeSelected.PageSize;;
            $scope.pageSizeSelected = pageSizeSelected.PageSize;
            getQuotedProjectList(dataModal)
        }

        $scope.pageChanged = function (pageIndex) {
            debugger;
            dataModal.PageIndex = pageIndex;
            getQuotedProjectList(dataModal)
        }


        ////////////////////////////////////------------paging code/////////////////////////////////



    });

});

