﻿
define(['app'], function () {

    app.controller('projectAwardedViewDetailForCompanyProfileController',
        function ($scope, sharedService, $rootScope, toaster, apiURL, $stateParams, $location, $timeout, projectsFactory, vendorInvoiceFactory,$http, invoiceFactory) {

            var CompanyId = 0
            var UserId;
            var ProjectId = $stateParams.pid;
            var VendorId = $stateParams.vid;
            $scope.ProjectIsEdit = true;
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
                $rootScope.activeTeam = [];
            }
            angular.element(document).ready(function () {
                CompanyId = sharedService.getCompanyId();
                UserId = sharedService.getUserId();
                getProjectAwardedViewDetailCompanyProfile(ProjectId, VendorId, CompanyId);
            })
            // custom control
            $scope.CustomDirectiveReady = false;
            $scope.customcontrolmodel = {};

            function getProjectAwardedViewDetailCompanyProfile(ProjectId, VendorId, CompanyId) {
                debugger;
                $('#globalLoader').show();
                projectsFactory.getProjectAwardedViewDetailCompanyProfile(ProjectId, VendorId, CompanyId)
                 .success(function (data) {
                     if (data.success) {
                         $scope.projectAwardedViewDetailList = data.ResponseData
                         toaster.success("Success", message.successdataLoad);
                      
                             
                         //------getting custom control area ---------//
                         $scope.customcontrolmodel = JSON.parse(data.ResponseData.CustomControlValueModel.CustomControlData);

                         //-----------------Custom cuntrol is not use by Company---------------------------------------///
              
                         $scope.CustomcontrolList = data.ResponseData.CustomControlUIList;
                         $scope.CustomDirectiveReady = true;

                        countUpaidInvoiceCompanyProfile();// calculation unpaid invoice
                     }
                     else {
                         toaster.error("Error", data.error);
                     }
                     $('#globalLoader').hide();
                 }),
                Error(function (data, status, headers, config) {
                    toaster.error("Error", data.error);
                    $('#globalLoader').hide();
                })

            }

            ///////////////////////////////////////////////countUpaidInvoiceCompanyProfile////////////////////////////////////////////////////////////////////////////

            $scope.countUnpaidInvoice = 0
            function countUpaidInvoiceCompanyProfile() {
                debugger;
                $scope.projectAwardedViewDetailList.VendorProjectProgressList;

                for (var i = 0; i < $scope.projectAwardedViewDetailList.VendorProjectProgressList.length; i++) {
                    if ($scope.projectAwardedViewDetailList.VendorProjectProgressList[i].IsPaid == false) {
                        $scope.countUnpaidInvoice += 1

                    }
                }
            }


            ////////////////////////////////////showVendorInvoiceCompanyProfile from Awarded Screen and//////////////////////////////////////////////////////////////
            //share vendor Invoice factory here for show 
            $scope.showInvoicePreviewviewDetailInCompany = function (Id) {
                debugger;
                $('#globalLoader').show();
                vendorInvoiceFactory.getPreviewInvoicePaidOutstandingReject(Id, CompanyId)
                .success(function (data) {
                    if (data.success) {
                        if (data.ResponseData != null)
                            $scope.vedorInvoicePaidOutstandRejectPreview = data.ResponseData;
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    }
                    $('#VendorInvoicePreviewForCompanyProfileModal').modal('show');
                }),
                Error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                })

            }



            //////////////////////////////////////////////////////modalMilestoneDetail///////////////////////////////////////////////////////////////////////
            $scope.PhaseIndex
            $scope.modalMilestoneDetailCompanyProfile = function (index) {
                debugger;
                $scope.PhaseIndex = index;
                $('#milestoneProjectAwardedForCompanyProfileModal').modal('show')

            }
            ////////////////////////////////////showVendorInvoicePreviewInCompany//////////////////////////////////////////////////////////////
            $scope.showVendorInvoicePreviewInCompany = function (Id) {
                debugger;
                $('#globalLoader').show();
                vendorInvoiceFactory.getPreviewInvoicePaidOutstandingReject(Id, CompanyId)
                .success(function (data) {
                    if (data.success) {
                        if (data.ResponseData != null)
                            $scope.vedorInvoicePaidOutstandRejectPreview = data.ResponseData;
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    }
                    $('#VendorInvoicePreviewForCompanyProfileModal').modal('show');
                }),
                Error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                })

            }
            ////////////////////////////////////approveVendorInvoice//////////////////////////////////////////////////////////////
            //vendorInvoiceFactory this is common use with
            $scope.approveVendorInvoice = function (InvoiceId, index) {
                $('#globalLoader').show();
                debugger;
                var data = { 'InvoiceId': InvoiceId, 'UserId': UserId };
                vendorInvoiceFactory.approvedVendorInvoice(data)
                    .success(function (response) {
                        $('#VendorInvoicePreviewForCompanyProfileModal').modal('hide');
                        $('#globalLoader').hide();
                        toaster.success("Success", message.VendorInvoiceApproved);
                        $location.path('/vendor/invoice');
                    })
                    .error(function (response) {
                        $('#globalLoader').hide();
                        toaster.error("Error", message.error);
                    })
            }


            ////////////////////////////////////rejectVendorInvoice//////////////////////////////////////////////////////////////
            $scope.rejectVendorInvoice = function (InvoiceId, index) {

                $scope.Reject = { 'InvoiceId': InvoiceId, 'UserId': sharedService.getUserId() };
                $("input[name='Reason']").removeClass("inputError");
                $('#VendorInvoicePreviewForCompanyProfileModal').modal('hide');
                $('#modalInvoiceReject').modal('show');
            }

            ////////////////////////////////////rejectInvoice//////////////////////////////////////////////////////////////

            $scope.rejectInvoice = function (IsReasonInvalid, addUpdateInvoiceForm) {

                if (IsReasonInvalid) {
                    addUpdateInvoiceForm.Reason.$dirty = true;
                    return;
                }

                $('#globalLoader').show();
                vendorInvoiceFactory.rejectInvoice($scope.Reject)
                    .success(function (response) {
                        $('#modalInvoiceReject').modal('hide');

                        // Load All Invoices again
                        var d = new Date();
                        d.setDate(d.getDate() - 7);
                        $scope.fromDate = d;
                        $scope.toDate = new Date();
                        $('#globalLoader').hide();
                        getAllInvoiceDetailList(CompanyId);

                        $location.path('/vendor/invoice');

                    })
                    .error(function (response) {
                        $('#globalLoader').hide();
                        toaster.error("Error", message.error);
                    })
            }
            $scope.DataApproveInvoice = { InvoiceId: 0, UserId: "", Token: "" };
            $scope.selectVendorPaymentDetail = function (InvoiceId, index) {
                //    $('#globalLoader').show();
                debugger;


                vendorInvoiceFactory.getCardDetailForVendorPayment(0, CompanyId, InvoiceId)
                .success(function (result) {
                    $scope.userCardDetail = result.ResponseData;

                    $scope.DataApproveInvoice = { InvoiceId: InvoiceId, UserId: UserId, Token: "" };

                    $('#VendorInvoicePreviewForCompanyProfileModal').modal('hide');
                    $('#modalInvoicePreview').modal('hide');
                    $('#VendorStripePaymentModal').modal('show');
                    $('#globalLoader').hide();
                })
                .error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", message["error"]);
                })
            }





            ///////////////////////*  region Stripe  Starts Here   */////////////////////////////


            //Stripe.setPublishableKey('pk_test_vlfopRnE9Tff7fQBrEPagmeR');
            Stripe.setPublishableKey('pk_test_HLVaHbwHkZYpehMozE1z3fnz');
            $scope.isSubsctriptionPaymentDone = false;

            $scope.approveAndPaymentVenodrInvoice = function () {
                debugger;
                $('#globalLoader').show();
                var $form = $('#payment-form');
                if ($scope.userCardDetail == null) {
                    $('#globalLoader').hide();
                    toaster.warning("Warning", "Please contact admin to set-up default Account");
                    return;
                }
                //$form.find('.submit').prop('disabled', true);
                Stripe.card.createToken($form, stripeResponseHandler);
            }


            /////////////*  region Stripe   Here   */////////////////
            function stripeResponseHandler(status, response) {
                // Grab the form:
                var $form = $('#payment-form');
                if (response.error) {
                    // Show the errors on the form:
                    //$form.find('.payment-errors').text(response.error.message);
                    toaster.error("Error", response.error.message);
                    $('#globalLoader').hide();
                }
                else {
                    // Token was created!

                    // Get the token ID:
                    $scope.DataApproveInvoice.Token = response.id;
                    var data = $scope.DataApproveInvoice;
                    var index = $scope.DataApproveInvoice.Index;
                    // this factory called for invoice payment
                    invoiceFactory.approveInvoice(data)
                    .success(function (response) {
                        debugger;
                        $timeout(function () {
                            if (response.ResponseData == 'Payment successfull') {
                                toaster.success("Success", message.ApproveInvoiceSuccess);
                                $('#VendorStripePaymentModal').modal('hide');
                                $('#globalLoader').hide();
                                $location.path('/vendor/invoice');
                            }
                            else {

                                toaster.error("Error", message["error"]);
                                $('#VendorStripePaymentModal').modal('show');
                                $('#globalLoader').hide();
                            }


                        }, 2000);
                    })
                    .error(function () {
                        $('#globalLoader').hide();
                        toaster.error("Error", message["error"]);
                        $('#VendorStripePaymentModal').modal('show');
                    })


                }
            };


            /////////////*  region Stripe  End Here   */////////////////






            $scope.downloadProjectandAwardedForCompanyProfile = function (FilePath, FileName, ActualFileName) {
                debugger;
                $('#globalLoader').show();

                var url = apiURL.baseAddress + "VendorApi/";

                $http({
                    method: 'Post',
                    url: url + "GetFiledownload",
                    data: { FileName: FileName, FilePath: FilePath },
                    responseType: 'arraybuffer'
                }).success(function (data, status, headers) {
                    headers = headers();
                    debugger;
                    //var filename = headers['x-filename'];
                    var contentType = headers['content-type'];

                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], { type: contentType });
                        var url = window.URL.createObjectURL(blob);
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", ActualFileName);

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        $('#globalLoader').hide();

                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        console.log(ex);
                        $('#globalLoader').hide();
                    }
                }).error(function (data, status) {
                    console.log(data);
                    $('#globalLoader').hide();
                });
            };




        })

})