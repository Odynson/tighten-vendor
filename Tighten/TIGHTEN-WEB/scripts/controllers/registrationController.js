﻿
define(['app'], function (app) {
    //registrationFactory is the factory for the registration
    app.controller('registrationController', function ($scope, $rootScope, $cookieStore, $timeout, $http, $location, $window, toaster, registrationFactory, sharedService, FreelancerAccountFactory, AuthenticationFactory, rolePermissionFactory) {
        $rootScope.navbar = true;
        $scope.Type = "Company";
        $scope.registrationOption = "";
        $scope.registrationModel = {};


        // method called on register button click
        $scope.checkRegistrationValidation = function (event, registrationForm) {
            debugger
            $('#globalLoader').show();
            event.preventDefault();
            $scope.dataLoading = true;
            // check if the user infomation is valid or not
            var isValid = $scope.checkloginValidation(event, registrationForm);
            if (isValid) {

                var registrationData = {};
                registrationData.Email = $scope.registrationModel.Email;
                registrationData.Password = $scope.registrationModel.Password;
                registrationData.FirstName = $scope.registrationModel.FirstName;
                registrationData.LastName = $scope.registrationModel.LastName;
                registrationData.CompanyName = $scope.registrationModel.CompanyName;
                registrationData.Token = $scope.registrationModel.Token;
                registrationData.UserName = $scope.registrationModel.UserName;
                registrationData.isFreelancer = $scope.isFreelancer;          // used to check registration is for company or freelancer 
                //registrationData.CreditCard = $scope.CreditCard;
                //registrationData.CvvNumber = $scope.CvvNumber;

                if ($scope.isFreelancer && !$scope.invitedUser) {
                   
                    var ModelToInsert = {};
                    ModelToInsert.RegisterBindingModel = registrationData;
                    ModelToInsert.FreelancerInvitationModel = sharedService.getFreelancerInfoInShared();
                    // registration service to save the user detail For Freelancer
                    FreelancerAccountFactory.RegisterFreelancer(ModelToInsert)
                    .success(function (data) {
                        
                        if (data.ResponseData == "freelancerRegistrationSuccess") {
                            toaster.success("Success", message[data.ResponseData]);
                            sharedService.setFreelancerInfoInShared();
                            $('#globalLoader').hide();
                            $location.path('/login');
                        }
                        else {
                            toaster.error("Error", message["error"]);
                            $('#globalLoader').hide();
                        }
                        
                    })
                    .error(function (data) {
                        toaster.error("Error", message[data.ResponseData]);
                        $('#globalLoader').hide();
                    })

                    $scope.dataLoading = false;


                }
                else {

                    // registration service to save the user detail for Company
                    registrationFactory.Registration(registrationData, function (response) {
                        //

                        if (response.success) {
                            if (response.errors == true) {
                                $scope.message = response.message;
                                toaster.warning(response.message);
                            }
                            else {
                                toaster.success(response.message);
                                $scope.registrationModel.Email = '';
                                $scope.registrationModel.FirstName = '';
                                $scope.registrationModel.LastName = '';
                                $scope.registrationModel.CompanyName = '';
                                $scope.registrationModel.Password = '';
                                $scope.registrationModel.Token = '';
                                $scope.registrationModel.TermsAndConditions = false;
                                $location.path('/login');
                            }

                            //toaster.success(response.message);
                        } else {
                            $scope.message = response.message;
                            toaster.error(response.message);
                        }
                        $scope.dataLoading = false;
                        $('#globalLoader').hide();
                    });

                }
            }
            else {
                $scope.dataLoading = false;
                $('#globalLoader').hide();
            }
        };
        // Check the user detail is valid or not
        $scope.checkloginValidation = function (event, registrationForm) {

            var isvalid = true;
            if (registrationForm.Email.$invalid) {
                registrationForm.Email.$dirty = true;
                isvalid = false;
                $scope.Email_message = 'You must provide Email Address';
                return;
            }
            if (registrationForm.FirstName.$invalid) {
                registrationForm.FirstName.$dirty = true;
                isvalid = false;
                $scope.FirstName_message = 'You must provide Full Name';
                return;
            }
            if ($scope.Type == "Company") {
                if (registrationForm.CompanyName.$invalid) {
                    registrationForm.CompanyName.$dirty = true;
                    isvalid = false;
                    $scope.CompanyName_message = 'You must provide Company Name';
                    return;
                }
            }
            if ($scope.Type == "FreeLancer") {
                if (registrationForm.UserName.$invalid) {
                    registrationForm.UserName.$dirty = true;
                    isvalid = false;
                    $scope.UserName_message = 'You must provide UserName';
                    return;
                }
                if ($scope.isUserNameExist == true) {
                    isvalid = false;
                    $scope.UserName_message = 'You must provide different UserName';
                    return;
                }
            }

            if (registrationForm.Password.$invalid) {
                registrationForm.Password.$dirty = true;
                isvalid = false;
                $scope.Password_message = 'You must provide Password';
                return;
            }

            if ($scope.invitedUser != false) {
                if (registrationForm.Token.$invalid) {
                    registrationForm.Token.$dirty = true;
                    isvalid = false;
                    $scope.Token_message = 'You must provide Token';
                    return;
                }
            }

            if ($scope.registrationModel.TermsAndConditions != true) {
                isvalid = false;
                $scope.TermsAndConditions_message = 'You must agree to Terms and Conditions';
                return;
            }
            else {
                $scope.Email_message = '';
                $scope.FirstName_message = '';
                $scope.CompanyName_message = '';
                $scope.UserName_message = '';
                $scope.Password_message = '';
                $scope.Token_message = '';
                $scope.TermsAndConditions_message = '';

            }
            if (!isvalid) {
                // shake the registration form if not valid
                $('#signin').addClass('animate0 wobble');
                $timeout(removeClass, 1000);
            }
            return isvalid;

            function removeClass() {
                $('#signin').removeClass('animate0 wobble');
            };
        };


        // 
        $scope.getUserName = function (email) {

            if ($scope.Type == "FreeLancer") {
                if (email != undefined) {

                    var specialCharacters = "!#$%^&*()+=-[]\\\';,/{}|\":<>?";
                    for (var i = 0; i < email.length; i++) {
                        if (specialCharacters.indexOf(email.charAt(i)) != -1) {
                            email = email.replace(email.charAt(i), '');
                            $scope.Email = email;
                        }
                    }

                    if (email != undefined) {
                        var newemail = email.split("@");
                        if (newemail.length > 1) {
                            $scope.registrationModel.UserName = newemail[0];
                            $scope.chkUserNameOnBlur($scope.registrationModel.UserName);
                        }
                    }
                }
            }
            //else if ($scope.Type = "Company") {
            //    $scope.Email = ' ';
            //    $scope.UserName = ' ';
            //}

        };

        $scope.chkUserNameOnBlur = function (UserName) {
            
            $('#globalLoader').show();
            var data = { "UserName": UserName };
            registrationFactory.chkUserNameExist(data)
            .success(function (response) {
                if (response.success) {
                    $("input[name='UserName']").addClass("inputError");
                    $scope.isUserNameExist = true;
                    $('#globalLoader').hide();
                }
                else {
                    $("input[name='UserName']").removeClass("inputError");
                    $scope.isUserNameExist = false;
                    $('#globalLoader').hide();
                }
            })
            .error(function (data) {
                toaster.error("Error", message[data.ResponseData]);
                $('#globalLoader').hide();
            })
        };



        // enable fields for Company Tab 
        $scope.companyTabClick = function (registrationForm) {

            $("#companyTab").addClass("active");
            $("#freelancerTab").removeClass("active");
            $scope.Type = "Company";
            $scope.isFreelancer = false;
            $scope.invitedUser = true;
            if (registrationForm != 'pageload') {
                clearFields(registrationForm);
            }
            $scope.registrationOption = "Tighten";

        };

        // enable fields for Freelancer Tab 
        $scope.freelancerTabClick = function (registrationForm) {

            $("#freelancerTab").addClass("active");
            $("#companyTab").removeClass("active");
            $scope.Type = "FreeLancer";
            $scope.isFreelancer = true;
            if (registrationForm != 'pageload') {
                clearFields(registrationForm);
            }

            if ($.isEmptyObject(sharedService.getFreelancerInfoInShared())) {
                $scope.invitedUser = true;
            }
            else {
                $scope.invitedUser = false;
            }
            //$scope.CompanyName
        };

        function clearFields(registrationForm) {

            $scope.registrationForm.Email.$dirty = false;
            $scope.registrationForm.UserName.$dirty = false;
            $scope.registrationModel.Email = '';
            $scope.registrationModel.UserName = '';
        };

        // enable Company or Freelancer Tab fields on Page Load 
        angular.element(document).ready(function () {
            $('#globalLoader').show();
            $("input[name='Password']").val('');

            var freelancerInfo = sharedService.getFreelancerInfoInShared();
            if ($.isEmptyObject(freelancerInfo)) {
                $scope.companyTabClick('pageload');
                $('#globalLoader').hide();
            }
            else {
                $scope.registrationModel.Email = freelancerInfo.EmailForFreelancer;
                $scope.freelancerTabClick('pageload');
                $("input[name='Email']").focus();
                $('#globalLoader').hide();
            }

            $scope.registrationModel.CompanyName = '';
            $scope.registrationModel.Password = '';

        });




        $scope.tightenRegistration = function () {

            $scope.registrationOption = "Tighten";
            $scope.getUserName($scope.registrationModel.Email);
        }


        $scope.backToRegistrationOption = function () {
            $scope.registrationOption = "";
            $scope.registrationModel.LastName = "";
            $scope.registrationModel.FirstName = "";
            $scope.registrationModel.Password = "";
        }



        /*  #Region LinkedIn          */

        $scope.linkedInRegistration = function () {
            IN.User.authorize(callbackFunction);
        }

        function callbackFunction() {

            IN.API.Profile("me").fields("first-name", "last-name", "email-address", "picture-url").result(onSuccess).error(onError);
        }

        function onSuccess(data) {
            $('#globalLoader').show();
            var linkedInData = data;
            if (linkedInData.values.length > 0) {
                if (linkedInData.values[0].emailAddress == $scope.registrationModel.Email) {
                    //$scope.registrationLinkedIn(linkedInData.values[0]);
                    $scope.linkedInData = linkedInData.values[0];
                    $scope.registrationOption = "linkedIn";
                    $scope.$apply();
                    IN.User.logout();
                    $('#globalLoader').hide();
                }
                else {
                    toaster.error("Error", "Please login again with same Email-Id");
                    IN.User.logout();
                    $('#globalLoader').hide();
                }
            }
        }

        // Handle an error response from the API call
        function onError(error) {
            toaster.error("Error", message["error"]);
        }


        //$scope.$on('onLinkedInSuccess',function (event, data) {
        //    var linkedInData = data.data;
        //    if (linkedInData.values.length > 0) {
        //        if (linkedInData.values[0].emailAddress == $scope.Email)
        //        {
        //            $scope.registrationLinkedIn(linkedInData.values[0]);
        //        }
        //        else {
        //            toaster.error("Error", "Please login again with same Email-Id");
        //            IN.User.logout();
        //        }
        //    }
        //}) ;


        $scope.cancelLinkedInRegistration = function () {
            $scope.linkedInData = {};
            $scope.registrationOption = "";
        }



        $scope.registrationLinkedIn = function (data) {

            //$scope.dataLoading = true;
            $('#globalLoader').show();
            var registrationData = {};
            registrationData.Email = $scope.linkedInData.emailAddress;
            //registrationData.Password = $scope.Password;
            registrationData.FirstName = $scope.linkedInData.firstName;
            //registrationData.CompanyName = $scope.CompanyName;
            //registrationData.Token = $scope.Token;
            var newemail = registrationData.Email.split("@");
            if (newemail.length > 1) {
                $scope.registrationModel.UserName = newemail[0];
                $scope.chkUserNameOnBlur($scope.registrationModel.UserName);
            }

            registrationData.UserName = $scope.registrationModel.UserName;
            registrationData.isFreelancer = $scope.isFreelancer;          // used to check registration is for company or freelancer 
            //registrationData.CreditCard = $scope.CreditCard;
            //registrationData.CvvNumber = $scope.CvvNumber;

            if ($scope.isFreelancer && !$scope.invitedUser) {
                var ModelToInsert = {};
                ModelToInsert.RegisterBindingModel = registrationData;
                ModelToInsert.FreelancerInvitationModel = sharedService.getFreelancerInfoInShared();
                ModelToInsert.pictureUrl = $scope.linkedInData.pictureUrl;
                // registration service to save the user detail For Freelancer
                FreelancerAccountFactory.RegisterFreelancer(ModelToInsert)
                .success(function (data) {
                    $('#globalLoader').show();
                    toaster.success("Success", message[data.ResponseData]);
                    sharedService.setFreelancerInfoInShared();
                    //$('#globalLoader').hide();
                    $scope.login();
                    //$location.path('/login');
                })
                .error(function (data) {
                    toaster.error("Error", message[data.ResponseData]);
                    $('#globalLoader').hide();
                    //$scope.dataLoading = false;
                })


            }
            else {
                $scope.dataLoading = false;
            }

        }


        //using
        $scope.login = function (event, loginForm) {

            var loginObject = {};
            loginObject.Email = $scope.linkedInData.emailAddress;
            loginObject.Password = '';
            loginObject.IsLoginWithLinedIn = true;

            AuthenticationFactory.ClearCredentials();
            AuthenticationFactory.Login(loginObject, function (response) {
                if (response.success && response.message == "Success") {
                    if (response.ResponseData.isFreelancer == true) {
                        FreelancerAccountFactory.getCompaniesForFreelancer(response.ResponseData.userId)
                        .success(function (freelancerData) {
                            if (freelancerData.ResponseData.length == 0) {
                                response.ResponseData.CompanyId = 0;

                                AuthenticationFactory.SetCredentials(response.ResponseData);
                                $cookieStore.put('sharedService', response.ResponseData);
                                sharedService.setShared(response.ResponseData);
                                sharedService.setIsLoggedInFirstTimeByLinkedIn(true);

                                //if (response.ResponseData.RoleId==5)
                                //    $location.path('/dashboard/c');
                                //else
                                // Call getMessagesCount on global controller 
                                $scope.$emit('getMessagesCount', []);
                                $location.path('/dashboard/u');
                                //$scope.dataLoading = false;
                                $('#globalLoader').hide();
                            }
                            else {
                                response.ResponseData.CompanyId = freelancerData.ResponseData[0].CompanyId;
                                FreelancerAccountFactory.GetFreelancerRoleForCompany(response.ResponseData.userId, response.ResponseData.CompanyId)
                                            .success(function (FreelancerAccountData) {
                                                response.ResponseData.RoleId = FreelancerAccountData.ResponseData.RoleId;
                                                AuthenticationFactory.SetCredentials(response.ResponseData);
                                                $cookieStore.put('sharedService', response.ResponseData);
                                                sharedService.setShared(response.ResponseData);

                                                sharedService.setIsLoggedInFirstTimeByLinkedIn(true);
                                                //if (response.ResponseData.RoleId==5)
                                                //    $location.path('/dashboard/c');
                                                //else
                                                // Call getMessagesCount on global controller 
                                                $scope.$emit('getMessagesCount', []);
                                                $location.path('/dashboard/u');
                                                $('#globalLoader').hide();
                                            })
                                            .error(function () {
                                                toaster.error("Error", "Some Error Occured !");
                                                $('#globalLoader').hide();
                                            })
                            }
                        })
                        .error(function (data) {
                            toaster.error("Error", "Some Error Occured !");
                            $('#globalLoader').hide();
                        })
                    }


                    //Get Internal Permissions For Current User 
                    rolePermissionFactory.getInternalPermissionsForCurrentUser(response.ResponseData.RoleId, response.ResponseData.CompanyId)
                       .success(function (permissionResponse) {
                           sharedService.setInternalPermissionInShared(permissionResponse.ResponseData);
                       })
                       .error(function () {
                           toaster.error("Error", "Some Error Occured!");
                       })



                    $rootScope.headerMenu = false;
                    $rootScope.showSideMenu = false;
                    $('#headerMenu').show();
                    $('#sideMenu').show();
                }
                else {
                    toaster.warning(message[response.message]);
                    $scope.error = response.message;
                    $('#globalLoader').hide();
                }
            });



        };



        /*  #endRegion LinkedIn          */



    });
});