﻿define(['app'], function (app) {
    app.controller("inboxController", function ($scope, $compile,$sce, $rootScope, $stateParams, $timeout, $http, sharedService, userMessagesFactory, toaster, $location, $interval, $filter, apiURL) {

        $scope.uid = $stateParams.userId;
        $scope.SenderModel = [];
        $scope.displayMessageModel = [];
        $scope.displayMessageModelList = [];


        var mytimeoutnew;
        var IsPostback = 0;
        $scope.apiURL = apiURL.imageAddress;

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
            $scope.$emit('startTimer', []);
        }



        /* Send Message Section (Tagging users & sending messages)*/

        /*Showing users in the list when anyone enter @ in message textbox */
        $scope.ShowUsersForTagging = function (e) {
            
            function getCaretCharacterOffsetWithin(element) {
                var caretOffset = 0;
                if (typeof window.getSelection != "undefined") {
                    var range = window.getSelection().getRangeAt(0);
                    var preCaretRange = range.cloneRange();
                    preCaretRange.selectNodeContents(element);
                    preCaretRange.setEnd(range.endContainer, range.endOffset);
                    caretOffset = preCaretRange.toString().length;
                }
                else if (typeof document.selection != "undefined" && document.selection.type != "Control") {
                    var textRange = document.selection.createRange();
                    var preCaretTextRange = document.body.createTextRange();
                    preCaretTextRange.moveToElementText(element);
                    preCaretTextRange.setEndPoint("EndToEnd", textRange);
                    caretOffset = preCaretTextRange.text.length;
                }
                return caretOffset;
            }
            var el = document.getElementById("txtMessage2");
            var CaretPositionIndex = getCaretCharacterOffsetWithin(el);
            $scope.CaretPositionIndex = CaretPositionIndex;
            var divText = $('#txtMessage2').text();
            var characterMain = divText.substring(0, CaretPositionIndex)
            var value = characterMain;
            var segmentsDividedBySpace = [];
            segmentsDividedBySpace = value.split(" ");
            if (segmentsDividedBySpace.length == 1) {
                $scope.IsItStartingOfMessage = true;
            }
            else {
                $scope.IsItStartingOfMessage = false;
            }
            var lastWord = segmentsDividedBySpace[segmentsDividedBySpace.length - 1];
            var firstCharacterOfLastWord = lastWord.substring(0, 1);
            var lastCharactersOfLastWord = lastWord.substring(1, lastWord.length);
            $scope.lastCharactersOfLastWord = lastCharactersOfLastWord;
            if (firstCharacterOfLastWord == "@") {
                $scope.searchassigneefortagging = lastCharactersOfLastWord;
                $scope.searchattachmentfortagging = null;
                $scope.showassigneesearch = true;
            }
            else {
                $scope.showassigneesearch = false;
            }
            if (e.shiftKey == true && e.keyCode == 50) {
                var characterUser = divText.substring(CaretPositionIndex - 2, CaretPositionIndex - 1)
                if (characterUser == " " || characterUser == "") {
                    $scope.showassigneesearch = true;
                }
            }
            else if (e.keyCode == 32) {
                $scope.showassigneesearch = false;
            }
        };

        /* Allowing tab for selecting the searched user */
        $scope.TabKeyPressEvent = function (e) {
            
            var TABKEY = 9;
            var ENTERKEY = 13;
            if (e.keyCode == TABKEY) {
                if ($scope.showassigneesearch == true) {

                    var x = $filter('filter')($scope.SenderModel, $scope.searchassigneefortagging);
                    if (x != null) {
                        if (x.length > 0) {

                            $scope.people = x;
                            var Id = guid();
                            var data = "<div class='glyphicon glyphicon-user divUserInComment' name='" + $scope.people[0].MemberName + "' email='" + $scope.people[0].MemberEmail + "' userId='" + $scope.people[0].MemberUserId + "'  id='" + Id + "'   contenteditable='false'  >" + $scope.people[0].MemberName + "<a id='" + Id + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></div>";
                            var compiledElement = $compile(data)($scope);
                            var htmlnode = $('#txtMessage2').html();
                            if ($scope.IsItStartingOfComment == true) {
                                var stringTobeReplaced = "@" + $scope.lastCharactersOfLastWord;
                            }
                            else {
                                var stringTobeReplaced = " @" + $scope.lastCharactersOfLastWord;
                            }
                            var res = htmlnode.replace(stringTobeReplaced, " " + data);
                            $('#txtMessage2').html("");
                            $('#txtMessage2').append(res);
                            var newhtml = "<c>" + $('#txtMessage2').html() + "</c>";
                            var newcompilehtml = $compile(newhtml)($scope);
                            $('#txtMessage2').html("");
                            $('#txtMessage2').append(newcompilehtml);
                            $('#txtMessage2').append("&nbsp;");
                            $scope.showassigneesearch = false;
                            $scope.searchassigneefortagging = null;
                            placeCursorAtEnd(document.getElementById("txtMessage2"));
                            $timeout(function () {
                                $scope.$apply();
                            });
                        }
                    }
                }
                if (e.preventDefault) {
                    e.preventDefault();
                }
                return false;
            }
            else if (e.shiftKey == false && e.keyCode == ENTERKEY) {
                $scope.SendMessage();
                e.preventDefault();
            }
            else if (e.shiftKey == true && e.keyCode == ENTERKEY) {
                $('#txtMessage2').append("\r");
            }
        };

        /* Adding User In Comment in my todo*/
        $scope.AddUserInMessage = function (userId, FirstName, LastName, Emailid) {

            var Id = guid();
            var data = "<div class='glyphicon glyphicon-user divUserInComment' name='" + FirstName + "' email='" + Emailid + "' userId='" + userId + "'  id='" +
                        Id + "'   contenteditable='false'  >" + FirstName + "<a id='" + Id +
                        "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></div>";

            var compiledElement = $compile(data)($scope);
            var htmlnode = $('#txtMessage2').html();

            if ($scope.IsItStartingOfMessage == true) {
                var stringTobeReplaced = "@" + $scope.lastCharactersOfLastWord;
            }
            else {
                var stringTobeReplaced = " @" + $scope.lastCharactersOfLastWord;
            }

            var res = htmlnode.replace(stringTobeReplaced, " " + data);


            $('#txtMessage2').html("");
            $('#txtMessage2').append(res);



            var newhtml = "<c>" + $('#txtMessage2').html() + "</c>";
            var newcompilehtml = $compile(newhtml)($scope);

            $('#txtMessage2').html("");
            $('#txtMessage2').append(newcompilehtml);
            $('#txtMessage2').append("&nbsp;");

            $scope.showassigneesearch = false;
            $scope.searchassigneefortagging = "";

            placeCursorAtEnd(document.getElementById("txtMessage2"));

            $timeout(function () {
                $scope.$apply();

            });
        };

        /* Code For Placing Cursor at the end of div*/
        function placeCursorAtEnd(el) {
            el.focus();
            if (typeof window.getSelection != "undefined"
                    && typeof document.createRange != "undefined") {
                var range = document.createRange();
                range.selectNodeContents(el);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (typeof document.body.createTextRange != "undefined") {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(el);
                textRange.collapse(false);
                textRange.select();
            }
        }

        /* Generate GUID in Javascript*/

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
              s4() + '-' + s4() + s4() + s4();
        }

        /* Removing tagged items from comment*/
        $scope.DeleteTaggedItem = function (event) {

            $('#' + $(event.target).attr('id')).remove();

        };





        $scope.sendUserMessage = function (UserId) {
            
            if (angular.isDefined(mytimeoutnew)) {
                $interval.cancel(mytimeoutnew);
                mytimeoutnew = undefined;
            }
            $location.path('/inbox/' + UserId);
        }
        $scope.IscurrentUser = function (Id) {

            var senderId = sharedService.getUserId();
            if (senderId == Id) {
                return true;
            }
            else {
                return false;
            }
        }
        $scope.GetUserMessage = function () {
            
            if ($scope.uid == 0 && $scope.SenderModel.length > 0) {
                $scope.uid = $scope.SenderModel[0].SenderId;
                //$scope.CurrentChatUser = $filter('filter')($scope.SenderModel, { 'SenderId': $scope.uid })[0].SenderName;
                $scope.CurrentChatUser = $filter('filter')($scope.SenderModel, { 'SenderId': $scope.uid })[0].SenderName;
            }

            var senderId = sharedService.getUserId();
            userMessagesFactory.getUsersMessages($scope.uid, senderId)
                .success(function (data) {
                    
                    $scope.displayMessageModel = data.ResponseData;
                   
                    // For arranging messages in desc list according to Date
                    $scope.displayMessageModelList = [];
                    angular.copy($scope.displayMessageModel, $scope.displayMessageModelList);
                    $scope.displayMessageModelList = $scope.convertTo($scope.displayMessageModelList, 'CreatedDateTime', true);


                    $scope.txtMessage2 = "";
                    $('#globalLoader').hide();
                    $timeout(function () {
                        var scroller = document.getElementById("divMessages");
                        scroller.scrollTop = scroller.scrollHeight + 200;
                        mytimeoutnew = $interval(function () { $scope.GetNewUserMessages(); }, 5000);
                        $scope.$apply();
                    }, 0, false);
                }).error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        };

        $scope.GetSenderUsers = function () {

            //var $scope.uid = sharedService.getUserId();
            //userMessagesFactory.getSenderUsers($scope.uid)
            //    .success(function (data) {

            //        $scope.SenderModel = data.ResponseData;
            //        //$scope.Message = "";
            //        //$('#globalLoader').hide();
            //        //$timeout(function () {
            //        //    var scroller = document.getElementById("divMessages");
            //        //    scroller.scrollTop = scroller.scrollHeight + 200;
            //        //}, 0, false);
            //    }).error(function () {
            //        toaster.error("Error", "Some Error Occured!");
            //    });

            
            userMessagesFactory.getAllSenders(sharedService.getUserId(), sharedService.getCompanyId(), $scope.uid)
             .success(function (data) {
                 
                 $scope.SenderModel = data.ResponseData;
                 if (IsPostback == 0) {
                     $scope.GetUserMessage();
                     IsPostback = 1;
                 }
             })
             .error(function (data) {
                 toaster.error("Error", "Some Error Occured!");
             })


        };

        $scope.SendMessage = function () {
            //$scope.Message
            
            
            $('#txtMessage2 div').each(function (index) {
                var name = $(this).attr('name');
                var userId = $(this).attr('userId');
                $(this).replaceWith(" <a href='#/user/profile/"+ userId +"' target='_blank' >"+ name +"</a> ");
            });
            var data = $('#txtMessage2').html();

            //$scope.txtMessage2 = $sce.trustAsHtml(data);
            
            $scope.txtMessage2 = data;

            var now = new Date();
            var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
            if (sharedService.getUserId() != null && sharedService.getUserId() != undefined) {
                var senderId = sharedService.getUserId();
                if ($scope.txtMessage2 != "") {
                    
                    var profilePhoto = sharedService.getDefaultProfilePic();
                    $scope.MessageModel = {};
                    $scope.MessageModel.SenderId = senderId;
                    $scope.MessageModel.ReceiverId = $scope.uid;
                    $scope.MessageModel.SenderName = sharedService.getNameId();
                    $scope.MessageModel.SenderProfileImage = profilePhoto == 'profile.png' ? "Uploads/Default/" + profilePhoto : "Uploads/Profile/Icon/" + profilePhoto;
                    $scope.MessageModel.Message = $scope.txtMessage2;
                    $scope.MessageModel.CreatedDateTime = now_utc;
                    $scope.displayMessageModel.push($scope.MessageModel);
                    $scope.txtMessage2 = "";
                    $('#txtMessage2').html("");

                    // For arranging messages in desc list according to Date
                    $scope.displayMessageModelList = [];
                    angular.copy($scope.displayMessageModel, $scope.displayMessageModelList);
                    $scope.displayMessageModelList = $scope.convertTo($scope.displayMessageModelList, 'CreatedDateTime', true);

                    userMessagesFactory.insertUserMessages($scope.MessageModel)
                        .success(function (data) {
                            //$scope.displayMessageModel.push($scope.MessageModel);
                            //$scope.Message = "";
                            $timeout(function () {
                                $scope.GetSenderUsers();
                                var scroller = document.getElementById("divMessages");
                                scroller.scrollTop = scroller.scrollHeight + 200;
                                $('#txtMessage2').html("");
                            }, 0, false);
                        }).error(function () {
                            toaster.error("Error", "Some Error Occured!");
                        });
                }
                else {
                    $scope.txtMessage2 = "";
                    $('#txtMessage2').html("");
                }
            }
        };

        $scope.SendMessageOnEnter = function (event) {
            if (event.which === 13) {
                $scope.SendMessage();
            }
        }

        $scope.GetNewUserMessages = function () {
            //$scope.InboxCount++;
            
            if (sharedService.getUserId() != null && sharedService.getUserId() != undefined) {
                var senderId = sharedService.getUserId();
                $scope.MessageModel = {};
                $scope.MessageModel.SenderId = senderId;
                $scope.MessageModel.ReceiverId = $scope.uid;
                userMessagesFactory.getUpdateMessageIsRead($scope.MessageModel)
                    .success(function (data) {
                        if (data.ResponseData.length > 0) {
                            for (var i = 0; i < data.ResponseData.length; i++) {
                                $scope.displayMessageModel.push(data.ResponseData[i]);

                                // For arranging messages in desc list according to Date
                                $scope.displayMessageModelList = [];
                                angular.copy($scope.displayMessageModel, $scope.displayMessageModelList);
                                $scope.displayMessageModelList = $scope.convertTo($scope.displayMessageModelList, 'CreatedDateTime', true);


                                $timeout(function () {
                                    $scope.GetSenderUsers();
                                    var scroller = document.getElementById("divMessages");
                                    scroller.scrollTop = scroller.scrollHeight + 200;
                                }, 0, false);
                            }
                        }
                    });
            }
        }


        $scope.weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        $scope.convertTo = function (arr, key, dayWise) {
            var groups = {};
            for (var i = 0; l = arr.length, i < l; i++) {
                var newkey = '';
                if (dayWise) {
                    var weekday_value = $scope.weekdays[new Date(arr[i][key]).getDay()];
                    var date = new Date(arr[i][key]).toUTCString();

                    var cur_Date = new Date().toUTCString();
                    var current_Date = cur_Date.substring(5, 16)
                    var currentDate = new Date(current_Date);
                     

                    var actual_Date = date.substring(5, 16)
                    var actualDate = new Date(actual_Date);
                     
                    if (currentDate.getTime() == actualDate.getTime()) {
                        newkey = "Today";
                    }
                    else if (currentDate.setDate(currentDate.getDate() - 1) == actualDate.getTime()) {
                        newkey = "Yesterday";
                    }
                    else {
                        var newdate = date.substring(0, 16)
                        //arr[i][key] = [weekday_value] + ', ' + date;
                        newkey = newdate;
                    }
                    
                }
                else {
                    //arr[i][key] = arr[i][key].toTimeString();
                    newkey = arr[i][key].toTimeString();
                }
                //groups[arr[i][key]] = groups[arr[i][key]] || [];
                //groups[arr[i][key]].push(arr[i]);
                groups[newkey] = groups[newkey] || [];
                groups[newkey].push(arr[i]);
            }
            return groups;
        };


        $scope.getAllSenders = function () {

        }

        
        var mytimeout = $interval(function () { $scope.GetSenderUsers(); }, 5000);


        angular.element(document).ready(function () {

            $('#globalLoader').show();
            //$scope.displayMessageModel = [];
            $scope.txtMessage2 = "";
            //$scope.getAllSenders();
            $scope.GetSenderUsers();
            
        })


    });
});