﻿define(['app', 'todoManualTrackingFactory'], function (app) {

    app.controller("todoManualTrackingController", function ($scope, $rootScope, $timeout, $location, $stateParams, $http, sharedService, toaster, apiURL, todoManualTrackingFactory) {
        $scope.$emit('showMenu', []);
        $scope.todoManualTime = {};
        $scope.todoManualTimeObj = {};
        $scope.todoManualTime.time = "My Time";

        // Change event of Task type dropdown
        $scope.ddlTaskTypeChange = function (obj) {

            //if PMS Task then
            // get task list from server according to user
            // show the fields accordingly
            //
            if (obj == 5) {
                todoManualTrackingFactory.getMyTodos(sharedService.getUserId(), sharedService.getRoleId())
                    .success(function (result) {
                        $scope.TaskList = result.ResponseData;
                    })
                    .error(function (error) {

                    });
            }
                // hide the todo list dropdown
                // show the textboxes
            else {
                var taskType = $scope.todoManualTime.TaskType;
                $scope.todoManualTime = {};
                $scope.todoManualTime.TaskType = taskType;
            }
        }


        $scope.ddlTaskChange = function (obj) {
            //
            // converting the sting object to the Json object
            obj = JSON.parse(obj);
            // Assigning the ojects value to the models
            $scope.todoManualTime.Name = obj.Name;
            $scope.todoManualTime.Description = obj.Description;
            $scope.todoManualTime.EstimatedTime = obj.EstimatedTime;
            $scope.todoManualTime.DeadlineDate = obj.DeadlineDate;
            $scope.todoManualTime.StartDate = obj.StartDate;
            $scope.todoManualTime.StopDate = obj.StopDate;
            $scope.todoManualTime.IsDone = obj.IsDone;
            $scope.todoManualTime.InProgress = obj.InProgress;
            $scope.todoManualTime.TodoId = obj.TodoId;
        }

        /*This is the save method for manual time tracking*/
        $scope.SaveManualTodo = function () {
            
            $('#globalLoader').show();
            var z = $scope.todoManualTime;
            // Check if the Todo Type =5 or not
            // check if we have to save the data to the PMS or to add new todo
            // if tasktype not =5 then it is new entry
            if ($scope.todoManualTime.TaskType != 5) {
                // Settign the current user Id
                $scope.todoManualTime.UserId = sharedService.getUserId();
                /// Request to save the manual todo
                todoManualTrackingFactory.insertTodo($scope.todoManualTime)
                .success(function (result) {
                    //
                    if (result.message == null) {
                        var message = result.errors[0] + ' ' + result.errors[1];
                        toaster.error("Error", message);
                        $('#globalLoader').hide();
                    }
                    else {
                        $scope.todoManualTime = {};
                        toaster.success("Success", "Todo has been saved");
                        $('#globalLoader').hide();
                    }
                    //alert(result.message);
                })
                .error(function (error) {
                    alert(error);
                    $('#globalLoader').hide();
                });
            }
                // if task type is ==5
                // it is the PMS Task
                // Need to update the entry in the database
            else {
                $scope.todoManualTime.UserId = sharedService.getUserId();
                
                /// Request to save the manual todo
                todoManualTrackingFactory.updateTodo($scope.todoManualTime)
                .success(function (result) {
                    //
                    if (result.message == null) {
                        var message = result.errors[0];
                        if (result.errors[1] != undefined) {
                            message = message + ' ' + result.errors[1];
                        }
                        toaster.error("Error", message);
                        $('#globalLoader').hide();
                    }
                    else if (result.message == "Todo is still in Progress") {
                        toaster.warning("Warning", "Todo is still in Progress");
                        $('#globalLoader').hide();
                    }
                    else {
                        $scope.todoManualTime = {};
                        clearFields();
                        toaster.success("Success", "Todo has been Updated");
                        $('#globalLoader').hide();
                    }
                    //alert(result.message);
                })
                .error(function (error) {
                    alert(error);
                    $('#globalLoader').hide();
                });
            }
        }


        function clearFields() {
            $scope.frmManualTimeTracking.TaskType.$dirty = false;
            $scope.frmManualTimeTracking.TaskId.$dirty = false;
            $scope.frmManualTimeTracking.Summary.$dirty = false;
            $scope.frmManualTimeTracking.Description.$dirty = false;
            $scope.frmManualTimeTracking.EstimatedTime.$dirty = false;
            $scope.frmManualTimeTracking.StopDate.$dirty = false;
            $scope.frmManualTimeTracking.StartDate.$dirty = false;
            $scope.frmManualTimeTracking.LoggedTime.$dirty = false;
            $scope.frmManualTimeTracking.DeadlineDate.$dirty = false;

        };

        /**/




        ////////////Date Functions/////////////
        /************************/
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };
        var da = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
        };

        $scope.toggleMin();

        $scope.maxDate = new Date(da.getFullYear(), 5, 22);

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.open3 = function () {
            $scope.popup3.opened = true;
        };

        $scope.setDate = function (year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
        //$scope.init = new Date(da.getFullYear()-10, 5, 22);
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,

        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.popup3 = {
            opened: false
        };

        $scope.getDayClass = function (date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        };
        /***********************/


        $scope.estimatedTimeKeyPress = function (e) {

            var keyCode = e.keyCode;
            var charCode = e.charCode;
            if ((charCode > 47 && charCode < 58) || (keyCode > 47 && keyCode < 58) || (charCode == 100) || (charCode == 104) || (charCode == 109) || (charCode == 32) || (keyCode == 100) || (keyCode == 104) || (keyCode == 109) || (keyCode == 32)) { //return true;
                return true;
            }
            else {
                e.preventDefault();
                return false;
            }
        }


    });
});