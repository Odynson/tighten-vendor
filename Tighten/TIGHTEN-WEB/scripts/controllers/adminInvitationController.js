﻿
define(['app'], function (app) {
    app.controller("adminInvitationController", function ($scope, $rootScope, $timeout, $location, $stateParams, $http, sharedService, toaster, apiURL, adminInvitationFactory, invitationFactory, projectsFactory) {

        $scope.imgURL = apiURL.imageAddress;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }

        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.PageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  
        //-------------------pagesizeList---------------------------------------------------/////
        $scope.pagesizeList = [
            { PageSize: 5 },
            { PageSize: 10 },
            { PageSize: 25 },
            { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];
        $scope.totalPageCount = 0;



        /* Showing Team Management Section to see the information*/
        $scope.showAllSentRequest = function () {

            $('#globalLoader').show();
            adminInvitationFactory.getAllSentRequest(sharedService.getCompanyId(), $scope.PageIndex, $scope.pageSizeSelected)
            .success(function (response) {
                $scope.AllSentRequest = response.ResponseData;
                if (response.ResponseData.length > 0) {
                    $scope.totalCount = response.ResponseData[0].TotalPageCount;
                }
                $('#globalLoader').hide();
            })
            .error(function (response) {
                $('#globalLoader').hide();
                toaster.error("Error", "Some Error Occured");
            });
        };


        angular.element(document).ready(function () {
            $scope.showAllSentRequest();
            $scope.UserId = sharedService.getUserId();
        });


        $scope.weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        $scope.convertTo = function (arr, key, dayWise) {

            var groups = [];
            for (var i = 0; l = arr.length, i < l; i++) {
                var newkey = '';

                var weekday_value = $scope.weekdays[new Date(arr[i][key]).getDay()];
                var date = new Date(arr[i][key]).toUTCString();
                var newdate = date.substring(0, 16)
                //arr[i][key] = [weekday_value] + ', ' + date;

                arr[i][key] = newdate;
                //groups.push(arr);
            }
            return arr;
        };


        $scope.getMessage = function (item) {

            $('#globalLoader').show();
            projectsFactory.getProject(item.ProjectId)
            .success(function (projectResponse) {
                $scope.invitationProjectObject = projectResponse.ResponseData;
            })
            .error(function (projectResponse) {
                toaster.error("Error", message["error"]);
                $('#globalLoader').hide();
            })

            invitationFactory.getFreelancerInfo(item.FreelancerInvitationId)
            .success(function (freelancerInfotResponse) {
                $scope.invitationFreelancerObject = freelancerInfotResponse.ResponseData;

                invitationFactory.getUserMessage(item.FreelancerInvitationId, sharedService.getUserId())
               .success(function (response) {
                   $scope.invitationMessageObject = [];
                   //$scope.invitationMessageObject.Messages = response.ResponseData;
                   var Messages = response.ResponseData;
                   $scope.invitationMessageObject.Messages = $scope.convertTo(Messages, 'CreatedDate', true);

                   $scope.invitationMessageObject.offerSentTo_UserId = item.FreelancerUserId;
                   $scope.invitationMessageObject.InvitationId = item.FreelancerInvitationId;
                   $scope.invitationMessageObject.isProjectExist = true;
                   $('#modalInvitationMessage').modal('show');
                   $('#globalLoader').hide();
               })
               .error(function (response) {
                   toaster.error("Error", message["error"]);
                   $('#globalLoader').hide();
               })
            })
            .error(function (freelancerInfotResponse) {
                toaster.error("Error", message["error"]);
                $('#globalLoader').hide();
            })
        }


        //Download Freelancer Attachments File with original name 
        $scope.downloadFreelancerAttachments = function (freelancer) {
            
            var data = { FileName: freelancer.FileName, ActualFileName: freelancer.ActualFileName, FilePath: freelancer.FilePath }
            projectsFactory.downloadFreelancerAttachments(data)
                .success(function (data, status, headers, config) {

                    var file = new Blob([data], {
                        type: 'application/csv'
                    });
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = freelancer.ActualFileName;
                    document.body.appendChild(a);
                    a.click();
                })
                .error(function (data, status, headers, config) {
                    toaster.error("Error", message["error"]);
                })
        }


        $scope.sendMessgae = function (NewMsg) {

            $('#globalLoader').show();
            if (NewMsg != "") {
                var asd = $scope.invitationMessageObject.NewMsg;
                var data = { UserId: $scope.invitationMessageObject.offerSentTo_UserId, Message: NewMsg, SentBy: sharedService.getUserId(), InvitationId: $scope.invitationMessageObject.InvitationId };
                invitationFactory.sendMessgae(data)
               .success(function (response) {
                   invitationFactory.getUserMessage($scope.invitationMessageObject.InvitationId, sharedService.getUserId())
                   .success(function (response) {
                       $timeout(function () {
                           $scope.$apply();
                           //$scope.invitationMessageObject.Messages = response.ResponseData;
                           var Messages = response.ResponseData;
                           $scope.invitationMessageObject.Messages = $scope.convertTo(Messages, 'CreatedDate', true);
                           $scope.invitationMessageObject.NewMsg = "";
                           $('#globalLoader').hide();
                       }, 1000);
                   })
                   .error(function (response) {
                       toaster.error("Error", message["error"]);
                       $('#globalLoader').hide();
                   })
               })
               .error(function (response) {
                   toaster.error("Error", message["error"]);
                   $('#globalLoader').hide();
               })
            }
        }


        $scope.TabKeyPressEvent = function (e) {

            var ENTERKEY = 13;
            if (e.shiftKey == false && e.keyCode == ENTERKEY) {
                $scope.sendMessgae($scope.invitationMessageObject.NewMsg);
            }
            else if (e.shiftKey == true && e.keyCode == ENTERKEY) {
                //$('#txtMessage2').append("\r");
                //$scope.invitationMessageObject.NewMsg = $scope.invitationMessageObject.NewMsg + "\r";
            }
        };

        

        //-------------------changePageSize--------------------------------------------------------/////
        $scope.changePageSize = function (pageSizeSelected) {
            debugger;
            $scope.pageSizeSelected = pageSizeSelected.PageSize;
            $scope.showAllSentRequest();
        }

        //-------------------pageChanged-------------------------------------------------------/////
        $scope.pageChanged = function (pageIndex) {
            debugger;
            $scope.PageIndex = pageIndex;
            $scope.showAllSentRequest();
        }
    });

});
