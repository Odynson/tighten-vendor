﻿
define(['app'], function (app) {

    app.controller("accountSettingsController", function ($scope, $rootScope, $http, sharedService, toaster, apiURL, $location, sharedService, accountSettingsFactory) {

        $scope.stripeAccountSetting = {};

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }



        $scope.UpdateStripeAccount = function (AccountSettingForm, isAccountSettingFormInvalid) {
            
            if (isAccountSettingFormInvalid) {
                AccountSettingForm.RoutingNumber.$dirty = true;
                AccountSettingForm.AccountNumber.$dirty = true;
                AccountSettingForm.SocialSecurityNumber.$dirty = true;
                AccountSettingForm.Address.$dirty = true;
                AccountSettingForm.AddressCity.$dirty = true;
                AccountSettingForm.AddressState.$dirty = true;
                AccountSettingForm.AddressPostalCode.$dirty = true;
                AccountSettingForm.Country.$dirty = true;
                AccountSettingForm.DOB.$dirty = true;

            }
            else {
                $('#globalLoader').show();
                $scope.stripeAccountSetting.UserId = sharedService.getUserId();
                accountSettingsFactory.updateAccountSettings($scope.stripeAccountSetting)
                .success(function (response) {
                    if (response.ResponseData == "Verified") {
                        toaster.success("Success", message["stripeAccountVerified"]);
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", message["error"]);
                        $('#globalLoader').hide();
                    }
                })
                .error(function (response) {
                    toaster.error("Error", message["error"]);
                    $('#globalLoader').hide();
                })


            }

        }


        function getAccountSettings() {
            $('#globalLoader').show();
            accountSettingsFactory.getAccountSettings(sharedService.getUserId())
            .success(function (response) {
                $scope.stripeAccountSetting = response.ResponseData;
                $('#globalLoader').hide();
            })
            .error(function (response) {
                toaster.error("Error", message["error"]);
                $('#globalLoader').hide();
            })

        }


        angular.element(document).ready(function () {
            getAccountSettings();
        });




        ////////////Date Functions/////////////
        /************************/
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        var da = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
        };

        $scope.toggleMin();

        $scope.maxDate = new Date(da.getFullYear(), 5, 22);

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,

        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup2 = {
            opened: false
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };






    });

});