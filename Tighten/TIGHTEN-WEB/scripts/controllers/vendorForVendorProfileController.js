﻿
define(["app"], function (app) {

    app.controller("vendorForVendorProfileController", function ($scope, $rootScope, apiURL, $location, vendorFactory, sharedService, toaster) {
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();
        $scope.myVendorList = [];  // my vendor for my vendor page
        $scope.companyVendorRespList = [];

        $scope.searchText = "";


        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 100;  // Total number of items in all pages. initialize as a zero  
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  



        var dataModel = { VendorId: "", CompanyId: 0, UserId: "", Id: 0, PageIndex: 1, PageSize: 5, TotalPageCount: 0 }

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');

            }

            var RoleId = sharedService.getRoleId();
            dataModel.VendorId = CompanyId;
            getCompanyForDropFilter(CompanyId);


        })


        ///------------------------popUpfor Rejection--------------------------------------------------------

        $scope.openAcceptRejectModel = function (item, IsApproved) {
            debugger;
            IdForRejectUPdate = item.Id;
            $('#modalAcceptReject').modal('show');

        }
        ///---------------------------------acceptRejectInvitaionByModal----------------------------------------------------------------------
        var tempdata = {};
        $scope.acceptRejectInvitaionByModal = function (ReasonReject, IsApproved) {
            debugger;
            var data = {}

            data.IsApproved = IsApproved;
            data.ReasonReject = ReasonReject;
            data.Id = IdForRejectUPdate;
            data.UserId = UserId;
            data.VendorId = CompanyId; // this company is now vendor id for company vendor
            $('#globalLoader').show();
            vendorFactory.acceptRejectInvitaionAsVendor(data)
                .success(function (data) {

                    if (data.ResponseData != null) {
                        //toaster.success("Success", message.InvitationAccepted);
                        getInvitaionFromCompany(dataModel);
                    }
                    else {
                        toaster.warning("Warning", message.error)
                    }
                    $('#modalAcceptReject').modal('hide');



                }), Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");

                })

            $('#globalLoader').hide();
        }


        ///------------------------openPopforAcceptReject--------------------------------------------------------

        $scope.acceptRejectInvitaionAsVendor = function (item, IsApproved) {
            debugger;
            var data = {}

            data.CompanyId = item.CompanyId;
            data.Name = item.Name;
            data.VendorId = item.VendorId; // this company is now vendor id for company vendor
            data.IsApproved = IsApproved;
            data.Id = item.Id;
            data.UserId = UserId;

            $('#globalLoader').show();
            vendorFactory.acceptRejectInvitaionAsVendor(data)
                .success(function (data) {

                    if (data.ResponseData != null) {
                        toaster.success("Success", message.InvitationAccepted + data.ResponseData.Name);
                        getInvitaionFromCompany(dataModel);
               
                    }
                    else {
                        toaster.warning("Warning", message.error)
                    }


                    $('#globalLoader').hide();
                }), Error(function (data, status, headers, config) {
                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();
                })


        }


        $scope.modalAddNewvendors = function () {
            debugger;
            $('#modalAddNewvendors').modal('show');


        }


        ///------------------------getInvitaionToVendorsList--------------------------------------------------------

        function getInvitaionFromCompany(dataModel) {
            debugger;
            $('#globalLoader').show();
            dataModel.VendorId = CompanyId;
            dataModel.UserId = UserId;

            vendorFactory.getInvitaionFromCompany(dataModel).success(function (data) {
                debugger;
                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.CompanyInvitaionRespList = data.ResponseData;
                        if (data.ResponseData[0]!=null)
                        $scope.totalCount = data.ResponseData[0].TotalPageCount;
                    }
                    $('#globalLoader').hide();
                }


            })
                .error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured!");
                });
        }






        ///////////////////////////////////////////////////Filtering////////////////////////////////////////////////////////////////////////////////////////////
        function getCompanyForDropFilter(CompanyId) {
            debugger;
            $('#modalConfirmationCompleteProject').modal('show')
            vendorFactory.getCompanyForDropFilter(CompanyId)
            .success(function (data) {
                if (data.success) {
                    $scope.myCompanyDropList = data.ResponseData;
                    getInvitaionFromCompany(dataModel);
                }
                else {
                    toaster.error("Error", data.message);
                }
            })
            , Error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message.error);
                $('#modalConfirmationCompleteProject').modal('hide')
            })

        }

        ///////////////////////////////////////-Filter criteria  Vendor FilterInvitationsByCompany///////////////////////////////////////////////////////////////////////////////////////////////

        $scope.FilterInvitationsByCompany = function (item) {
            debugger;
            if (item != null) {
                dataModel.CompanyId = item.CompanyId;
            }
            else {
                dataModel.CompanyId = 0;
            }
            getInvitaionFromCompany(dataModel);

        }

        ///////////////////////////////////////-Filter criteria  Vendor myVendorstatusList///////////////////////////////////////////////////////////////////////////////////////////////

        $scope.myVendorstatusList = [
            { IsApproved: null, status: '--Select All--', Id: 0 },
            { IsApproved: true, status: 'Accepted', Id: 1 },
            { IsApproved: false, status: 'Rejected', Id: 2 },
            { IsApproved: null, status: 'Pending', Id: 3 }

        ];
        $scope.selectStatusModal = $scope.myVendorstatusList[0];


        $scope.FilterInvitationVenByStatus = function (item) {
            debugger;

            dataModel.IsApproved = item.IsApproved;
            dataModel.Id = item.Id;
            getInvitaionFromCompany(dataModel);

        }


        ///////////////////////////////////////-Filter criteria  Vendor myVendorstatusList////////////////////////////////////////////////////////////////

        $scope.filterInvitationVendorProfile = function () {

            getInvitaionFromCompany(dataModel)
        }




        $scope.pagesizeList = [
                    { PageSize: 5 },
                    { PageSize: 10 },
                    { PageSize: 25 },
                    { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];




        $scope.changePageSize = function (pageSizeSelected) {
            debugger;
            dataModel.PageSize = pageSizeSelected.PageSize;
            $scope.pageSizeSelected = pageSizeSelected.PageSize;
            getInvitaionFromCompany(dataModel);
        }

        $scope.pageChanged = function (pageIndex) {
            debugger;
            dataModel.PageIndex = pageIndex;
            getInvitaionFromCompany(dataModel);
        }


    })


});

