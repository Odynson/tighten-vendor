﻿
define(['app'], function (app) {
    app.controller("editInitiativeController", function ($scope, $rootScope, $stateParams, apiURL, $timeout, $http, $location, sharedService, $confirm, toaster, worklogFactory, projectsFactory, $window, initiativeFactory, companyAccountFactory) {
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();
        $scope.imgURL = apiURL.imageAddress;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }
        $scope.billableDropList = [
            { text: "Billable", value: "B" },
            { text: "Not-Billable", value: "NB" }
        ]
        $scope.worklogList = [];
        $scope.approvedProjectList = [];
        $scope.phaseMilestoneList = [];
        
        $scope.selectProjectObj = {};
        $scope.selectStatusObj = {};
        $scope.vendorProjectStatusList = [{ StatusName: "In Progress" }, { StatusName: "Hold" }, { StatusName: "Completed" }]
        $scope.selectMilestoneObj = {};
        $scope.AddNewWorkLogModel = {};
        $scope.AddNewProjectModel = {};
        $scope.AddNewInitiativeModel = {};
        $scope.AddNewInitiativeModel.vendorProjectDocumentList = [];
        $scope.myVendorList = [];
        $scope.myProjectDocumentList = [];
        $scope.vendorProjectList = [];
        $scope.AddNewInitiativeModel.initiativeVendors = [];
        $scope.DeletedVenderProjectList = [];
        $scope.vendorProjectDocumentList = [];
        $scope.popup = {
            opened1: false
        };
        function getVendorList(CompanyId) {
            debugger;
            companyAccountFactory.getVendorList(CompanyId).success(function (data) {

                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.myVendorList = data.ResponseData;
                    }
                }
                else {
                    toaster.error("Error", data.message);
                }

            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }
        function getInitiativeById() {
            debugger;
            initiativeFactory.GetInitiativeById($scope.AddNewInitiativeModel.Id).success(function (data) {
                debugger;
                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.AddNewInitiativeModel = data.ResponseData;
                        //var tempObj = {};
                        //tempObj.StatusName = $scope.AddNewInitiativeModel.status;
                        if ($scope.AddNewInitiativeModel.status != undefined && $scope.AddNewInitiativeModel.status != null && $scope.AddNewInitiativeModel.status!="")
                            for (var i = 0; i < $scope.vendorProjectStatusList.length; i++) {
                                
                                if ($scope.vendorProjectStatusList[i].StatusName == $scope.AddNewInitiativeModel.status) {

                                    $scope.selectStatusObj = $scope.vendorProjectStatusList[i];
                                    break;
                            }
                        }
                        
                        for (var i = 0; i < $scope.AddNewInitiativeModel.vendorProjectDocumentList.length; i++) {
                            var tempObj = {};
                            tempObj.name = $scope.AddNewInitiativeModel.vendorProjectDocumentList[i].ActualFileName;
                            tempObj.IsAdded = true;
                            tempObj.IsDeleted = $scope.AddNewInitiativeModel.vendorProjectDocumentList[i].IsDeleted;
                            tempObj.Id = $scope.AddNewInitiativeModel.vendorProjectDocumentList[i].Id;
                            $scope.myProjectDocumentList.push(tempObj);
                        }
                    }
                }
                else {
                    toaster.error("Error", data.message);
                }

            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }
        $scope.selectFileForProject = function ($files, index) {
            debugger;

            for (var i = 0; i < $files.length; i++) {

                if ($files[0].size < 10485760) {
                    $files[i].Id = 0;
                    $files[i].IsAdded = true;
                    $files[i].IsDeleted = false;
                    $scope.myProjectDocumentList.push($files[i]);
                    var tempObj = {};
                    tempObj.Id = $files[i].Id;
                    tempObj.IsAdded = $files[i].IsAdded;
                    tempObj.IsDeleted = $files[i].IsDeleted;
                    tempObj.ActualFileName = $files[i].name;
                    $scope.AddNewInitiativeModel.vendorProjectDocumentList.push(tempObj);
                }
                else {
                    toaster.error("Error", message.FileSizeExceeds);
                }
            }
        }
        //////////////////////////////////////////////removeProjectAttachments/////////////////////////////////////////////////////


        $scope.removeAttachments = function (index) {
            debugger;
            if ($scope.myProjectDocumentList[index].Id == 0) {
                $scope.myProjectDocumentList.splice(index, 1);
                $scope.AddNewInitiativeModel.vendorProjectDocumentList.splice(index, 1);
            }
            else
            {
                $scope.myProjectDocumentList[index].IsDeleted = true;
                $scope.AddNewInitiativeModel.vendorProjectDocumentList[index].IsDeleted = true;
               // $scope.myProjectDocumentList[index].IsAdded = false;
            }
        }
        function getProjectList(CompanyId) {
            debugger;
            projectsFactory.GetProjectsOfCompany(CompanyId).success(function (data) {

                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        $scope.vendorProjectList = data.ResponseData;
                    }
                }
                else {
                    toaster.error("Error", data.message);
                }

            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }
        

        $scope.selectedVendorChanged = function (VendorObj) {
            debugger;
            $('#globalLoader').show();
            //alert(ProjectObj.ProjectId);
            $scope.AddNewProjectModel.vendorId = VendorObj.VendorId;
            $scope.AddNewProjectModel.vendorName = VendorObj.CompanyName;
            $('#globalLoader').hide();
            
        }
        $scope.selectedProjectChanged = function (ProjectObj) {
            debugger;
            $('#globalLoader').show();
            //alert(ProjectObj.ProjectId);
            $scope.AddNewProjectModel.projectid = ProjectObj.ProjectId;
            $scope.AddNewProjectModel.projectName = ProjectObj.ProjectName;
            $('#globalLoader').hide();

        }
        $scope.selectedStatusChanged = function (StatusObj) {
            debugger;
            $('#globalLoader').show();
            $scope.AddNewInitiativeModel.status = StatusObj.StatusName;
            $('#globalLoader').hide();

        }
        $scope.getWorkLogList = function (UserId) {
            $('#globalLoader').show();
            worklogFactory.getWorkLogList(UserId)
            .success(function (data) {

                debugger;
                if (data.ResponseData != null) {
                    if (data.ResponseData.length > 0) {
                        $scope.worklogList = data.ResponseData[0];
                    }
                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }
        $scope.AddProjectToList = function (IsVendorContactPerson, IsvendorContactPersonPhNo, NewVendorForm) {
            debugger;
            //alert(val);
            $('#globalLoader').show();
            if (IsVendorContactPerson || IsvendorContactPersonPhNo || $scope.AddNewProjectModel.projectid == null || $scope.AddNewProjectModel.vendorId == null) {

                NewVendorForm.vendorName.$dirty = true;
                NewVendorForm.projectName.$dirty = true;

                NewVendorForm.vendorContactPerson.$dirty = true;
                NewVendorForm.vendorContactPersonPhNo.$dirty = true;
                $('#globalLoader').hide();
                return;

            }
            for (var i = 0; i < $scope.AddNewInitiativeModel.initiativeVendors.length; i++) {
                
                if ($scope.AddNewInitiativeModel.initiativeVendors[i].isDeleted==false&& $scope.AddNewInitiativeModel.initiativeVendors[i].vendorId == $scope.AddNewProjectModel.vendorId && $scope.AddNewInitiativeModel.initiativeVendors[i].projectid == $scope.AddNewProjectModel.projectid) {
                    toaster.warning("Warning","Selected Vendor and Project is already added");
                    $('#globalLoader').hide();
                    return;
                }
            }
            $scope.AddNewProjectModel.Id = 0;
            $scope.AddNewProjectModel.IsAdded = true;
            $scope.AddNewProjectModel.isDeleted = false;
            $scope.AddNewInitiativeModel.initiativeVendors.push($scope.AddNewProjectModel);
            $scope.AddNewProjectModel = {};
            $scope.selectVendorObj = {};
            $scope.selectvendorProjectObj = {};
            $('#globalLoader').hide();
        }

        $scope.deleteVendorProjectDetail = function (index) {  //pocFirstName,pocLastName,pocEmail
            debugger;
            // alert(index);
            $confirm({ text: 'Are you sure you want to delete this Project?', title: 'Delete it', ok: 'Yes', cancel: 'No' })

             .then(function () {
                 debugger;
                 if ($scope.AddNewInitiativeModel.initiativeVendors[index].Id == 0) {
                     $scope.DeletedVenderProjectList.push($scope.AddNewInitiativeModel.initiativeVendors[index]);
                     $scope.AddNewInitiativeModel.initiativeVendors.splice(index, 1);
                 }
                 else
                 {
                     $scope.AddNewInitiativeModel.initiativeVendors[index].isDeleted = true;
                     //$scope.AddNewInitiativeModel.initiativeVendors[index].IsAdded = false;
                 }
             });
        }
        $scope.deleteInitiative = function () { 
            debugger;
            
            $confirm({ text: 'Are you sure you want to delete this Initiative?', title: 'Delete it', ok: 'Yes', cancel: 'No' })

             .then(function () {
             
                 initiativeFactory.DeleteInitiative($scope.AddNewInitiativeModel.Id).success(function (data) {

                     if (data.success) {
                         if (data.ResponseData != null && data.ResponseData != undefined) {
                             toaster.success("Success", message.DeleteInitiativeSuccesfully);
                             $window.location.href = ('/#/my/initiative');
                         }
                     }
                     else {
                         toaster.error("Error", data.message);
                     }

                 })
               .error(function () {
                   toaster.error("Error", "Some Error Occured!");
               });
                 

             });
        }
        $scope.getApprovedProjectByUserId = function (UserId) {
            $('#globalLoader').show();
            projectsFactory.getApprovedProjectByUserId(UserId)
            .success(function (data) {

                debugger;
                if (data.ResponseData != null) {
                        $scope.approvedProjectList = data.ResponseData;
                    
                }
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }

        $scope.editInitiative = function (IsInitiativeTitle, IsContactPerson, IsPhNo, NewInitiativeForm, AddNewWorkLogModel) {
            if (IsInitiativeTitle || IsContactPerson || IsPhNo || $scope.AddNewInitiativeModel.initiativeVendors == undefined || $scope.AddNewInitiativeModel.initiativeVendors == null || $scope.AddNewInitiativeModel.initiativeVendors == "") {
                NewInitiativeForm.initiativeTitle.$dirty = true;
                NewInitiativeForm.contactPerson.$dirty = true;
                NewInitiativeForm.phNo.$dirty = true;
                return;
            }
            else {
                $('#globalLoader').show();
                debugger;
                ////alert(moment(NewInitiativeForm.createdOn.$viewValue).format("DD/MM/YYYY"));
                //var model = {};
                //model.WorkLogTitle = $scope.AddNewWorkLogModel.WorkLogTitle;
                //model.WorkLogDescription = $scope.AddNewWorkLogModel.WorkLogDescription;
                //model.CreatedOn = moment(NewInitiativeForm.createdOn.$viewValue).format("DD/MM/YYYY");
                ////data.CreateOn = moment(NewInitiativeForm.createdOn.$viewValue).format("MM/DD/YYYY");
                //model.CreatedUserId = UserId;
                //model.BillableStatus = $scope.AddNewWorkLogModel.BillableStatus;
                //model.ProjectId = $scope.AddNewWorkLogModel.ProjectId;
                //model.PhaseId = $scope.AddNewWorkLogModel.PhaseId;
                //model.PhaseMilestoneId = $scope.AddNewWorkLogModel.PhaseMilestoneId;
                //model.IsDeleted = false;
                //worklogFactory.saveWorkLog(model)
                //     .success(function (data) {
                //         debugger;
                //         if (data.success == true) {
                //             toaster.success("Success", data.ResponseData);
                //             $('#globalLoader').hide();

                //             NewInitiativeForm.workLogTitle.$dirty = false;
                //             NewInitiativeForm.createdOn.$dirty = false;
                //             NewInitiativeForm.worklogBStatus.$dirty = false;
                //             $window.location.href = ('/#/Worklogs');
                //         }
                //         else {
                //             toaster.warning("Warning", "Some Error Occured!");
                //             $('#globalLoader').hide();
                //         }
                //     })
                //     .error(function (data) {
                //         toaster.error("Error", "Some Error Occured!");
                //         $('#globalLoader').hide();
                //     })
                var initiativeurl = apiURL.baseAddress + "InitiativeApi/";
                $scope.AddNewInitiativeModel.enterpriseCompanyID = CompanyId;
                $scope.AddNewInitiativeModel.createdby = UserId;
                //for (l = 0; l < $scope.myProjectDocumentList.length; l++) {
                //    tempObj = {};
                //    tempObj.ActualFileName = $scope.myProjectDocumentList[l].name;
                //    $scope
                //    //formData.append("InitiDocuments", $scope.myProjectDocumentList[l]);

                //}
                $scope.jsonData = $scope.AddNewInitiativeModel;
                $http({
                    method: 'POST',
                    url: initiativeurl + "EditInitiative",
                    headers: { 'Content-Type': undefined },

                    transformRequest: function (data) {
                        var formData = new FormData();
                        formData.append("model", angular.toJson(data.model));

                        for (l = 0; l < $scope.myProjectDocumentList.length; l++) {
                            formData.append("InitiDocuments", $scope.myProjectDocumentList[l]);

                        }

                        for (i = 0; i < $scope.AddNewInitiativeModel.initiativeVendors.length; i++) {
                            formData.append("Project", $scope.AddNewInitiativeModel.initiativeVendors[i]);
                            
                        }

                        return formData;
                    },

                    data: { model: $scope.jsonData, files: $scope.files }
                }).
                    success(function (data, status, headers, config) {
                        debugger;
                        $('#globalLoader').hide();

                        if (data.success) {
                            if (data.ResponseData == 1) {
                                toaster.success("Success", message.InitiativeEditSuccesfully);
                                $window.location.href = ('/#/my/initiative');
                                //$location.path('/quoted/project');
                            }

                            else if (data.ResponseData == -1) {
                                toaster.warning("Warning", message.DuplicateProject);
                            }

                        }
                        else {
                            toaster.success("error", message.error);
                        }

                    }).
                    error(function (data, status, headers, config) {
                        $('#globalLoader').hide();

                    });
            }
        }
        $scope.GoBack = function () {
            $window.location.href = ('/#/my/initiative');
        }
        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
                $rootScope.activeTeam = [];
                $scope.format = 'MM/dd/yyyy';
            }
            //$scope.getWorkLogList(UserId);
            //$scope.getApprovedProjectByUserId(UserId);
            $scope.AddNewInitiativeModel.Id = $stateParams.pid;
            getInitiativeById();
            getVendorList(CompanyId);
            getProjectList(CompanyId);
        });


        $scope.open1 = function () {
            debugger;
            $scope.popup.opened1 = true;
            $scope.format = 'MM/dd/yyyy';
            $scope.mindate = new Date();
            
        };




    });
});