﻿

define(['app'], function (app) {
    app.controller("userFeedbackController", function ($scope, $rootScope, apiURL, $timeout, $http, $location, sharedService, $confirm, toaster, feedbackFactory) {
        
        $scope.imgURL = apiURL.imageAddress;
        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }
        $scope.isFreelancer = sharedService.getIsFreelancer();
        $scope.selectedCompanyId = sharedService.getCompanyId();


        // Company dropdown change event on left side menu for freelancer 
        $scope.$on('leftCompanyDropdownChange', function (event, data) {

            $scope.selectedCompanyId = data.CompanyId;
            $scope.selectedCompanymodel = data.CompanyId;
            $scope.getProjects();
        });


        $scope.selectedCompanyChanged = function (selectedCompany) {
            $scope.selectedCompanyId = selectedCompany;
            $scope.getProjects();
        };


        $scope.getProjects = function () {
            $('#globalLoader').show();
            feedbackFactory.getProjects($scope.selectedCompanyId, sharedService.getUserId())
            .success(function (projectsData) {
                $scope.projectsObject = projectsData.ResponseData;
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }


        
        $scope.showAllFeedbacksOfProject = function (object) {
            $('#globalLoader').show();
            feedbackFactory.showAllFeedbacksOfProject(object.ProjectId, sharedService.getUserId())
            .success(function (allFeedbacksData) {
                $scope.allFeedbacksOfProjectObject = allFeedbacksData.ResponseData;
                //$('#modalFeedbacksOfProject').modal('show');
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }


        
        $scope.showOnProfile = function (feedback) {
            $('#globalLoader').show();
            var data = { FeedbackOfProject: feedback }
            feedbackFactory.showOnProfile(data)
            .success(function (allFeedbacksData) {
                toaster.success("Success", allFeedbacksData.message);
                $('#modalFeedbacksOfProject').modal('hide');
                $('#globalLoader').hide();
            })
            .error(function (data) {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }




        angular.element(document).ready(function () {
            $scope.getProjects();
        });




    });
});