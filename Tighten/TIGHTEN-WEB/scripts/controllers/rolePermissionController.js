﻿

define(['app', 'rolePermissionFactory'], function (app) {

    app.controller("rolePermissionController", function ($scope, $rootScope, $confirm, $http, sharedService, toaster, apiURL, $location, rolePermissionFactory) {

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.imgURL = apiURL.imageAddress;
        $scope.RoleObject = [];
        $scope.internalPermissionsObject = {};
        $scope.selectedRoleModel = 0;


        function getCustomizableOptions() {
            /* Get All Role Of Company*/
            $('#globalLoader').show();
            rolePermissionFactory.getAllRolesForCompany(sharedService.getCompanyId())
            .success(function (response) {
                $scope.RoleObject = response.ResponseData;
                if (response.ResponseData != null) {
                    if (response.ResponseData.length > 0) {
                        $scope.selectedRoleModel = response.ResponseData[0].RoleId;
                    }
                    else {
                        $scope.selectedRoleModel = 0;
                    }
                }
                else {
                    $scope.selectedRoleModel = 0;
                }

                getPermissionForSelectedRole();
                //toaster.success("Success", data.ResponseData);
            })
            .error(function () {
                toaster.error("Error", "Some Error Occured!");
                $('#globalLoader').hide();
            })
        }



        function getPermissionForSelectedRole() {
           
            rolePermissionFactory.getPermissionForSelectedRole($scope.selectedRoleModel, sharedService.getCompanyId())
                       .success(function (permissionResponse) {
                           
                           $scope.customizePermissionObject = permissionResponse.ResponseData;
                           $('#globalLoader').hide();
                       })
                       .error(function () {
                           toaster.error("Error", "Some Error Occured!");
                           $('#globalLoader').hide();
                       })
        }



        $scope.selectedRoleChanged = function (selectedRole) {

            $scope.selectedRoleModel = selectedRole;
            $('#globalLoader').show();
            getPermissionForSelectedRole();
        }


        $scope.savePermissions = function () {

            $('#globalLoader').show();
            var data = { PermissionsList: $scope.customizePermissionObject };
            rolePermissionFactory.savePermissionsForSelectedRole(data)
                       .success(function (saveResponse) {
                           toaster.success("Success", saveResponse.ResponseData);
                           //$scope.$emit('showMenu', []);
                           $('#globalLoader').hide();
                       })
                       .error(function () {
                           toaster.error("Error", "Some Error Occured!");
                           $('#globalLoader').hide();
                       })
        }



        $scope.clickInternalPermission = function (InternalPerm) {

            if (InternalPerm.IsAdminLevelPermission) {
                //e.preventDefault();
                $confirm({ text: 'Are you sure you want to change permissions for ' + InternalPerm.PermissionName, title: 'Change Permission', ok: 'Yes', cancel: 'No' })
                   .then(function () {
                       InternalPerm.IsPermissionActive = !InternalPerm.IsPermissionActive;
                   });
            }
            else {
                InternalPerm.IsPermissionActive = !InternalPerm.IsPermissionActive;
            }
        }


        $scope.openInternalPermissionModal = function (obj) {

            $('#globalLoader').show();

            rolePermissionFactory.getAllInternalPermissionsForPermission(obj.MenuId, obj.RoleId, obj.CompanyId)
                       .success(function (InternalPermissionsResponse) {
                           $scope.internalPermissionsObject = InternalPermissionsResponse.ResponseData;
                           $scope.internalPermissionsObject.MenuName = obj.MenuName;
                           $('#modalInternalPermission').modal('show');
                           $('#globalLoader').hide();
                       })
                       .error(function () {
                           toaster.error("Error", "Some Error Occured!");
                           $('#globalLoader').hide();
                       })

        }




        $scope.saveInternalPermissions = function () {

            $('#globalLoader').show();
            var data = { PermissionsList: $scope.internalPermissionsObject };
            rolePermissionFactory.saveInternalPermissions(data)
                       .success(function (saveResponse) {
                           toaster.success("Success", saveResponse.ResponseData);
                           $scope.$emit('showMenu', []);
                           $('#modalInternalPermission').modal('hide');
                           $('#globalLoader').hide();
                       })
                       .error(function () {
                           toaster.error("Error", "Some Error Occured!");
                           $('#globalLoader').hide();
                       })

        }




        angular.element(document).ready(function () {
            getCustomizableOptions();
        });



        /* ng-change="menuChange(PermissionItem)" & ng-change="subMenuChange(PermissionItemSubMenu,PermissionItem)"  ....... */


        $scope.menuChange = function (obj) {

            angular.forEach($scope.customizePermissionObject, function (value, index) {
                if (obj.MenuId == value.ParentId) {
                    value.IsActive = obj.IsActive;
                }
            })
        }


        $scope.subMenuChange = function (obj) {

            var IsActive;

            for (i = 0; i < $scope.customizePermissionObject.length; i++) {
                if (obj.ParentId == $scope.customizePermissionObject[i].ParentId) {
                    if ($scope.customizePermissionObject[i].IsActive == true) {
                        IsActive = true;
                        break;
                    }
                    else {
                        IsActive = false;
                    }
                }
            }

            if (IsActive) {
                for (i = 0; i < $scope.customizePermissionObject.length; i++) {
                    if (obj.ParentId == $scope.customizePermissionObject[i].MenuId) {
                        $scope.customizePermissionObject[i].IsActive = true;
                        return false;
                    }
                }
            }
            else {
                for (i = 0; i < $scope.customizePermissionObject.length; i++) {
                    if (obj.ParentId == $scope.customizePermissionObject[i].MenuId) {
                        $scope.customizePermissionObject[i].IsActive = false;
                        return false;
                    }
                }
            }

        }





    });

});

