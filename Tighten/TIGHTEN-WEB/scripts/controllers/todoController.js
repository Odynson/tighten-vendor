﻿define(['app'], function (app) {

    app.controller("todoController", ['$scope', '$rootScope', '$timeout', '$location', '$stateParams', '$http', 'sharedService', 'toaster',
                    '$sce', '$compile', '$upload', 'apiURL', '$window', '$filter', '$confirm', 'todosFactory', 'attachmentsFactory',
                    'followersFactory', 'todoNotificationsFactory', 'projectsFactory', 'sectionsFactory', 'projectUsersFactory', 'emailFactory',
                    'assigneeFactory', 'DropBoxSettings', 'lkGoogleSettings', 'appInfoForAttachments',
                   function ($scope, $rootScope, $timeout, $location, $stateParams, $http, sharedService, toaster, $sce, $compile, $upload,
                       apiURL, $window, $filter, $confirm, todosFactory, attachmentsFactory, followersFactory, todoNotificationsFactory,
                       projectsFactory, sectionsFactory, projectUsersFactory, emailFactory, assigneeFactory, DropBoxSettings, lkGoogleSettings,
                       appInfoForAttachments) {
                       window.Selectize = require('selectize');
                       var baseurl = apiURL.baseAddress;
                       $scope.imgURL = apiURL.imageAddress;
                       $scope.isFreelancer = sharedService.getIsFreelancer();
                       $scope.selectedCompanyId = sharedService.getCompanyId();
                       DropBoxSettings.box_clientId = appInfoForAttachments.boxClientId;
                       //DropBoxSettings.clientId = clientId.box;
                       lkGoogleSettings.apiKey = appInfoForAttachments.googleDriveApiKey;
                       lkGoogleSettings.clientId = appInfoForAttachments.googleDriveClientId;
                       $scope.UserId = sharedService.getUserId();
                       $scope.openedAccordion = "Comments";

                       //$scope.$on("myEvent", function (event, args) {
                       //    alert('brodcast todo');
                       //});

                       $scope.Tags = {};
                       $scope.Tags.TodoTaskType = [];
                       $scope.Tags.AllTaskTypes = [];
                       $scope.toggleEstimatedTime = true;
                       $scope.toggleTaskType = true;
                       $scope.currentSectionIdForRow = 0;
                       $scope.currentTodoIdForRow = 0;

                       $scope.sendUserMessage = function (UserId) {

                           $location.path('/inbox/' + UserId);
                       }


                       // Company dropdown change event on left side menu for freelancer 
                       $scope.$on('leftCompanyDropdownChange', function (event, data) {

                           $('#globalLoader').show();
                           $scope.selectedCompanyId = data.CompanyId;
                           projectsFactory.getProjects(sharedService.getUserId(), 0, $scope.selectedCompanyId, sharedService.getIsFreelancer())
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.projects = data.ResponseData;
                                       $timeout(function () {
                                           $scope.showTodoManagementSection($scope.projects[0]);
                                           $('#globalLoader').hide();
                                       });
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                       $('#globalLoader').hide();
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                                   $('#globalLoader').hide();
                               });
                       });


                       $scope.$on('leftTeamDropdownChange', function (event, data) {

                           $('#globalLoader').show();
                           projectsFactory.getProjects(sharedService.getUserId(), data.TeamId, sharedService.getCompanyId(), sharedService.getIsFreelancer())
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.projects = data.ResponseData;
                                       $timeout(function () {
                                           $scope.showTodoManagementSection($scope.projects[0]);
                                           $('#globalLoader').hide();
                                       });
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                       $('#globalLoader').hide();
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                                   $('#globalLoader').hide();
                               });
                       });


                       // Company dropdown change event
                       $scope.selectedCompanyChanged = function (selectedCompany) {
                           $('#globalLoader').show();
                           $scope.selectedCompanyId = selectedCompany;

                           projectsFactory.getProjects(sharedService.getUserId(), 0, $scope.selectedCompanyId, sharedService.getIsFreelancer())
                               .success(function (data) {

                                   if (data.success) {
                                       $scope.projects = data.ResponseData;
                                       $timeout(function () {
                                           $scope.showTodoManagementSection($scope.projects[0]);
                                           $('#globalLoader').hide();
                                       });
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                       $('#globalLoader').hide();
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                                   $('#globalLoader').hide();
                               });

                       }


                       if ($rootScope.loadSideMenu) {
                           $scope.$emit('showMenu', []);
                           $scope.$emit('startTimer', []);
                       }

                       $scope.addUpdateTodoForm = [];
                       $scope.selectedList = [];
                       $scope.upload = [];
                       $scope.IsUserAllowed = true;

                       /* Generate GUID in Javascript*/

                       function guid() {
                           function s4() {
                               return Math.floor((1 + Math.random()) * 0x10000)
                                 .toString(16)
                                 .substring(1);
                           }
                           return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                             s4() + '-' + s4() + s4() + s4();
                       }
                       //------------------------------------------------------------------------
                       //Create the formated string message for the time tracking
                       function createFormatedTimeString(loggedTime) {
                           var hourString = ' hours';
                           var minuteString = ' minutes';
                           var totalString = '';
                           var splitedHour = ''
                           var splitedMinutes = ''
                           var splitedHourArr = loggedTime.split('h');
                           splitedHour = splitedHourArr[0];
                           var splitedMinuteArr = splitedHourArr[1].split('m');
                           splitedMinutes = splitedMinuteArr[0];
                           if ((splitedHour == '0') || (splitedHour == '1')) {
                               hourString = ' hour';
                           }
                           if ((splitedMinutes == ' 0') || (splitedMinutes == ' 1')) {
                               minuteString = ' minute';
                           }
                           totalString = splitedHour + hourString + ' ' + splitedMinutes + minuteString
                           return totalString;
                       }
                       // create minutes from formated string value

                       function createMinutesFromTimeString(estimatedTimeString, loggedTimeString) {
                           var hourString = ' hours';
                           var minuteString = ' minutes';
                           var totalString = '';
                           var splitedHourEstimatedTimeString = ''
                           var splitedHourLoggedTimeString = ''
                           var splitedMinutesEstimatedTimeString = ''
                           var splitedMinutesLoggedTimeString = ''
                           //hour array
                           var splitedHourEstimatedTimeStringArr = estimatedTimeString.split('h');
                           var splitedHourLoggedTimeStringArr = loggedTimeString.split('h');

                           //hours 
                           splitedHourEstimatedTimeString = splitedHourEstimatedTimeStringArr[0];
                           splitedHourLoggedTimeString = splitedHourLoggedTimeStringArr[0];
                           // minute array
                           var splitedMinuteEstimatedTimeStringArr = splitedHourEstimatedTimeString[1].split('m');
                           var splitedMinuteLoggedTimeStringArr = splitedHourLoggedTimeString[1].split('m');
                           //minutes
                           splitedMinutesEstimatedTimeString = splitedMinuteEstimatedTimeStringArr[0];
                           splitedMinutesLoggedTimeString = splitedMinuteLoggedTimeStringArr[0];

                           estimatedMinutes = (splitedHourEstimatedTimeString * 60) + (splitedMinutesEstimatedTimeString);
                           loggedMinutes = (splitedHourLoggedTimeString * 60) + (splitedMinutesLoggedTimeString);

                           if (loggedMinutes <= estimatedMinutes) {
                               return { color: 'green' };
                           }
                           else {
                               return { color: 'red' };
                           }

                       }

                       ///-------------------------------------------------------
                       $scope.stopTimer = function () {

                           $scope.$emit('stopTimer', { 'taskId': 1, 'dateTime': new Date() });

                       }

                       /// getting the task detail which is in progress
                       $scope.getInProgressTodo = function () {
                           
                           var json = $scope.SectionsForAllProjects;
                           var object;
                           sectionIndex = 0;
                           todoIndex = 0;
                           var mysectionId = 0;
                           var canBreak = false;
                           for (j = 0; j < json.length; j++) {
                               sectionIndex = j;
                               for (var i = 0; i < json[j].Todos.length; i++) {
                                   if (json[j].Todos[i].InProgress == true && json[j].Todos[i].AssigneeId == sharedService.getUserId()) {
                                       object = json[j].Todos[i];
                                       todoIndex = i;
                                       mysectionId = json[j].Id;
                                       break;
                                   }
                               }
                               if (canBreak) { break; }
                           }
                           //return { obj: object, sectionIndex: sectionIndex, todoIndex: todoIndex };
                           return { obj: object, sectionId: mysectionId, todoIndex: todoIndex };
                       }

                       // method calls on clicking the start button
                       $scope.StartProgressInternal = function (todoObject, sectionIndex, todoIndex) {


                           $scope.todoAddUpdateModel = {};
                           $scope.todoAddUpdateModel.TodoId = todoObject.TodoId;
                           $scope.todoAddUpdateModel.Name = todoObject.Name;
                           $scope.todoAddUpdateModel.DeadlineDate = todoObject.DeadlineDate;
                           $scope.todoAddUpdateModel.EstimatedTime = todoObject.EstimatedTime;
                           $scope.todoAddUpdateModel.Description = todoObject.Description;
                           $scope.todoAddUpdateModel.SectionId = todoObject.SectionId;
                           $scope.todoAddUpdateModel.AssigneeId = todoObject.AssigneeId;
                           $scope.todoAddUpdateModel.ReporterId = todoObject.ReporterId;
                           $scope.todoAddUpdateModel.InProgress = true;
                           $scope.todoAddUpdateModel.IsDone = todoObject.IsDone;
                           $scope.todoAddUpdateModel.IsAvailableForInvoice = false;
                           $scope.todoAddUpdateModel.isEdit = false;
                           $scope.todoAddUpdateModel.TaskTypeId = $scope.Tags.TodoTaskType;
                           var NewTodoAddObj = {
                               'TodoUpdateModel': $scope.todoAddUpdateModel, 'userId': sharedService.getUserId(),
                               'ProjectId': $scope.selectedProject.ProjectId, 'CompanyId': sharedService.getCompanyId()
                           }
                           todosFactory.updateTodo(NewTodoAddObj).success(function (data) {

                               // start the timer of the task on the gloabal controller
                               $scope.$emit('startTimer', []);

                               if (data.success) {
                                   $scope.todoDescriptionObject.InProgress = true;
                                   $scope.todoDescriptionObject.ModifiedDate = new Date();
                                   /*call the notification service */
                                   $scope.getNotifications($scope.todoDescriptionObject.TodoId);

                                   /*call the function to update Progress status of Todo*/
                                   getTodosWithSectionsForAllProjects();

                                   toaster.success("Success", "Todo is in Progress ");
                                   $('#globalLoader').hide();
                               }
                               else {
                                   $('#globalLoader').hide();
                                   toaster.error("Error", data.message);
                               }
                           }).error(function () {
                               $('#globalLoader').hide();
                               toaster.error("Error", "Some Error Occured !");
                           });
                       }

                       /* Stop Progress Of Todo */
                       $scope.stopProgressInternal = function (todoObject, sectionIndex, todoIndex, newTodoObject, newSectionIndex, newTodoIndex) {

                           $scope.todoAddUpdateModel = {};
                           $scope.todoAddUpdateModel.TodoId = todoObject.TodoId;
                           $scope.todoAddUpdateModel.Name = todoObject.Name;
                           $scope.todoAddUpdateModel.DeadlineDate = todoObject.DeadlineDate;
                           $scope.todoAddUpdateModel.EstimatedTime = todoObject.EstimatedTime;
                           $scope.todoAddUpdateModel.Description = todoObject.Description;
                           $scope.todoAddUpdateModel.SectionId = todoObject.SectionId;
                           $scope.todoAddUpdateModel.AssigneeId = todoObject.AssigneeId;
                           $scope.todoAddUpdateModel.ReporterId = todoObject.ReporterId;
                           $scope.todoAddUpdateModel.InProgress = false;
                           $scope.todoAddUpdateModel.IsDone = todoObject.IsDone;
                           $scope.todoAddUpdateModel.IsAvailableForInvoice = false;
                           $scope.todoAddUpdateModel.TaskTypeId = $scope.Tags.TodoTaskType;
                           var updatedTodoAddObj = {
                               'TodoUpdateModel': $scope.todoAddUpdateModel, 'userId': sharedService.getUserId(),
                               'ProjectId': $scope.selectedProject.ProjectId, 'CompanyId': sharedService.getCompanyId()
                           }
                           $timeout(function () {
                               $scope.$apply();
                           }, 1000);
                           $scope.todoAddUpdateModel.isEdit = false;
                           todosFactory.updateTodo(updatedTodoAddObj)

                               .success(function (data) {
                                   if (data.success) {
                                       //$scope.todoDescriptionObject.InProgress = false;
                                       //$scope.todoDescriptionObject.ModifiedDate = new Date();
                                       //Stop the timer for the task on the global page
                                       $scope.$emit('stopTimer', []);

                                       /*Get estimated time from server*/
                                       todosFactory.getLoggedTime(todoObject.TodoId, sharedService.getUserId()).success(function (data) {


                                           var logmessage = " are logged for ";
                                           var isSectionInSameProject = false;
                                           for (j = 0; j < $scope.Sections.length; j++) {
                                               var currentSectionId = $scope.Sections[j].Id;
                                               if (currentSectionId == sectionIndex) {
                                                   isSectionInSameProject = true;
                                                   break;
                                               }
                                           }


                                           if (data.success) {
                                               var previousSectionIndex = '';
                                               if (isSectionInSameProject) {
                                                   previousSectionIndex = $.map($scope.Sections, function (obj, index) {
                                                       if (obj.Id == sectionIndex) {
                                                           return index;
                                                       }
                                                   })

                                                   $scope.Sections[previousSectionIndex].Todos[todoIndex].LoggedTime = data.ResponseData;
                                                   $scope.Sections[previousSectionIndex].Todos[todoIndex].InProgress = false;
                                                   //$scope.todoDescriptionObject.LoggedTime = data.ResponseData;
                                                   var taskName = $scope.Sections[previousSectionIndex].Todos[todoIndex].Name;
                                                   var formatedMessage = createFormatedTimeString(data.ResponseData);

                                                   logmessage = formatedMessage + " are logged for " + taskName;
                                               }

                                               else {
                                                   previousSectionIndex = $.map($scope.SectionsForAllProjects, function (obj, index) {
                                                       if (obj.Id == sectionIndex) {
                                                           return index;
                                                       }
                                                   })

                                                   $scope.SectionsForAllProjects[previousSectionIndex].Todos[todoIndex].LoggedTime = data.ResponseData;
                                                   $scope.SectionsForAllProjects[previousSectionIndex].Todos[todoIndex].InProgress = false;

                                                   var taskName = $scope.SectionsForAllProjects[previousSectionIndex].Todos[todoIndex].Name;
                                                   var formatedMessage = createFormatedTimeString(data.ResponseData);

                                                   logmessage = formatedMessage + " are logged for " + taskName;
                                               }


                                               /*call the function to update Progress status of Todo*/
                                               todosFactory.getTodosWithSectionsForAllProjects(sharedService.getCompanyId())
                                                   .success(function (dataSectionsForAllProjects) {
                                                       if (dataSectionsForAllProjects.success) {
                                                           $scope.SectionsForAllProjects = dataSectionsForAllProjects.ResponseData;

                                                           // Starting the new Task
                                                           $scope.StartProgressInternal(newTodoObject, newSectionIndex, newTodoIndex);
                                                           $timeout(function () {
                                                               $scope.$apply();
                                                           }, 1000);
                                                           toaster.success("Success", logmessage);

                                                       }
                                                       else {
                                                           toaster.error("Error", dataSectionsForAllProjects.message);
                                                       }
                                                   })
                                                   .error(function () {
                                                       toaster.error("Error", "Some Error Occured !");
                                                   });

                                           }
                                           else {
                                               var previousSectionIndex = '';
                                               if (isSectionInSameProject) {
                                                   previousSectionIndex = $.map($scope.Sections, function (obj, index) {
                                                       if (obj.Id == sectionIndex) {
                                                           return index;
                                                       }
                                                   })

                                                   $scope.Sections[previousSectionIndex].Todos[todoIndex].LoggedTime = '0h 0m';
                                               }
                                               else {
                                                   previousSectionIndex = $.map($scope.SectionsForAllProjects, function (obj, index) {
                                                       if (obj.Id == sectionIndex) {
                                                           return index;
                                                       }
                                                   })

                                                   $scope.SectionsForAllProjects[previousSectionIndex].Todos[todoIndex].LoggedTime = '0h 0m';
                                               }


                                               //$scope.todoDescriptionObject.LoggedTime = '0h 0m';
                                               toaster.error("Error", data.message);
                                               $('#globalLoader').hide();
                                           }
                                       }).error(function () {
                                           toaster.error("Error", "Some error occured !");
                                           $('#globalLoader').hide();
                                       });

                                       /*call the notification service */
                                       $scope.getNotifications($scope.todoDescriptionObject.TodoId);
                                       toaster.success("Success", "Progress is stopped ");

                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                       $('#globalLoader').hide();
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                                   $('#globalLoader').hide();
                               });
                       };

                       $scope.getNotifications = function (TodoId) {

                           /* Getting Todo Notification or We can Say History  */
                           todoNotificationsFactory.getTodoNotifications(TodoId)
                               .success(function (data) {

                                   if (data.success) {
                                       $scope.todoNotificationObject = data.ResponseData;
                                       $('#globalLoader').hide();
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                                   $('#globalLoader').hide();
                               });
                       }
                       /* Showing Todo Management Section to see the information*/
                       $scope.showTodoManagementSection = function (projectObject) {
                           debugger;
                           //closeCentreSections();
                           $scope.Sections = [];
                           $scope.projectMembersObject = [];

                           if (projectObject == null) {
                               return;
                           }

                           $scope.inProgressFilter = '';
                           $scope.isDoneFilter = '';
                           $scope.statusFilter = '';
                           $scope.inputProjectFilter = '';
                           $scope.projectMembersObject = projectObject.ProjectMembers;
                           $scope.projectDescriptionObject = projectObject;
                           $scope.editProjectUsers = projectObject.Display;
                           $scope.selectedWindow = "defaultTodosWindow";
                           $scope.hideRightWindow = true;
                           $scope.stretchCentreWindow = true;
                           $scope.selectedTodoView = "list";
                           $scope.selectedNavigation = '';
                           $scope.selectedProject = projectObject;
                           $scope.selectedTodo = '';
                           $('#divTodoManagementSection').show();
                           $scope.projectId = projectObject.ProjectId;
                           $scope.projectName = projectObject.ProjectName;
                           $scope.projectDescription = projectObject.ProjectDescription;


                           $scope.todotypeflag = 0;

                           /* It fetches the Todos with sections as an array in it */
                           todosFactory.getTodosWithSections($scope.projectId, sharedService.getUserId())
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.Sections = data.ResponseData;

                                       $timeout(function () {
                                           $scope.$apply();
                                       });
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                               });

                           getTodosWithSectionsForAllProjects();

                       };


                       function getTodosWithSectionsForAllProjects() {

                           /* It fetches the Todos in All Projects with sections as an array in it */
                           todosFactory.getTodosWithSectionsForAllProjects(sharedService.getCompanyId())
                           .success(function (dataSectionsForAllProjects) {
                               if (dataSectionsForAllProjects.success) {
                                   $scope.SectionsForAllProjects = dataSectionsForAllProjects.ResponseData;
                               }
                               else {
                                   toaster.error("Error", dataSectionsForAllProjects.message);
                               }
                           })
                           .error(function () {
                               toaster.error("Error", "Some Error Occured !");
                           });

                       };


                       /* Showing Todos In List View */
                       $scope.showTodosInListView = function () {

                           $scope.selectedTodo = "";
                           $scope.stretchCentreWindow = true;
                           $scope.hideRightWindow = true;
                           $scope.selectedTodoView = "list";

                       },


                       /* Showing Todos In List View */
                       $scope.showTodosInKanBanView = function () {

                           $scope.selectedTodo = "";
                           $scope.stretchCentreWindow = true;
                           $scope.hideRightWindow = true;
                           $scope.selectedTodoView = "kanban";

                       },


                       /* SHowing Add Update Todo Modal Pop Up*/
                       $scope.showTodoAddUpdateModal = function (sectionId) {

                           $('#globalLoader').show();
                           $scope.todoDescriptionObject = {};
                           $scope.todoUpdateButton = false;
                           //$scope.addUpdateTodoForm.name.$dirty = false;
                           //$scope.addUpdateTodoForm.section.$dirty = false;

                           $scope.todoAddUpdateModel = {};
                           if (sectionId != null) {
                               $scope.todoAddUpdateModel.SectionId = sectionId;
                           }
                           else if ($scope.Sections.length > 0) {
                               $scope.todoAddUpdateModel.SectionId = $scope.Sections[0].Id;
                           }

                           todosFactory.getTaskTypeForCreatingTodo(0)
                               .success(function (data) {

                                   $scope.Tags = {};
                                   $scope.Tags.TodoTaskType = data.ResponseData.TodoTaskType;
                                   $scope.Tags.AllTaskTypes = data.ResponseData.AllTaskTypes;
                                   $("input[name='name']").removeClass("inputError");
                                   $("input[name='Reporter']").removeClass("inputError");
                                   $('#modalTodoAddUpdate').modal('show');
                                   $('#globalLoader').hide();

                                   $timeout(function () {
                                       $scope.$apply();
                                   });
                               })
                               .error(function (data) {
                                   toaster.error("Error", data.message);
                                   $('#globalLoader').hide();
                               })
                       },


                        $scope.estimatedTimeKeyPress = function (e) {

                            //var estimatedTime = $scope.todoAddUpdateModel.EstimatedTime;
                            var keyCode = e.keyCode;
                            var charCode = e.charCode;

                            if ((charCode > 47 && charCode < 58) || (keyCode > 47 && keyCode < 58) || (charCode == 100) || (charCode == 104) || (charCode == 109) || (charCode == 32) || (keyCode == 100) || (keyCode == 104) || (keyCode == 109) || (keyCode == 32)) { //return true;

                                //if (estimatedTime != "" && estimatedTime != undefined) {
                                //    var estimatedTimeSplited = estimatedTime.split('');
                                //}

                                return true;
                            }
                            else {
                                e.preventDefault();
                                return false;
                            }
                        }



                       /* Add new Todo */
                       $scope.addNewTodo = function (SectionId, TodoListOrder, addType, sectionIndex, todoIndex) {

                           var currentUserId = sharedService.getUserId();
                           $scope.todoAddUpdateObject = {};
                           $scope.todoAddUpdateObject.SectionId = SectionId;
                           $scope.todoAddUpdateObject.TodoListOrder = TodoListOrder;
                           $scope.todoAddUpdateObject.addType = addType;
                           $scope.todoAddUpdateObject.ProjectId = $scope.projectId;

                           var NewTodoAddObj = {
                               'TodoAddModel': $scope.todoAddUpdateObject, 'userId': currentUserId,
                               'ProjectId': $scope.selectedProject.ProjectId, 'CompanyId': sharedService.getCompanyId()
                           }
                           todosFactory.insertTodo(NewTodoAddObj).success(function (data) {

                               if (data.success) {
                                   $scope.selectedtodo = "list";

                                   var AssigneeProfilePhoto;
                                   if (($scope.todoAddUpdateObject.AssigneeId == undefined) || ($scope.todoAddUpdateObject.AssigneeId == '')) {
                                       AssigneeProfilePhoto = "Uploads/Default/profile.png";
                                       AssigneeName = '';
                                       AssigneeEmail = '';
                                       AssigneeId = '';
                                   }
                                   else {
                                       var FilterProfilePhotoOfAssignee = $filter('filter')($scope.projectMembersObject, { 'MemberUserId': $scope.todoAddUpdateObject.AssigneeId }, true);

                                       AssigneeProfilePhoto = FilterProfilePhotoOfAssignee[0].MemberProfilePhoto;
                                       AssigneeName = FilterProfilePhotoOfAssignee[0].MemberName;
                                       AssigneeEmail = FilterProfilePhotoOfAssignee[0].MemberEmail;
                                       AssigneeId = $scope.todoAddUpdateObject.AssigneeId;
                                   }

                                   //Fetching Reporter's info to use on description panel
                                   var FilterReporter = $filter('filter')($scope.projectMembersObject, { 'MemberUserId': currentUserId }, true);

                                   /*Adding to todoDescriptionObject object to show on the right panel*/
                                   var FilteredSection = $filter('filter')($scope.Sections, { 'Id': $scope.todoAddUpdateObject.SectionId });
                                   $scope.todoDescriptionObject = {};
                                   $scope.todoDescriptionObject.SectionName = FilteredSection[0].SectionName;
                                   $scope.todoDescriptionObject.AssigneeProfilePhoto = AssigneeProfilePhoto;
                                   $scope.todoDescriptionObject.AssigneeName = AssigneeName;
                                   $scope.todoDescriptionObject.AssigneeEmail = AssigneeEmail;
                                   $scope.todoDescriptionObject.AssigneeId = AssigneeId;
                                   $scope.todoDescriptionObject.TeamName = $rootScope.activeTeam.TeamName;
                                   if (FilterReporter != null) {
                                       $scope.todoDescriptionObject.ReporterName = FilterReporter[0].MemberName;
                                       $scope.todoDescriptionObject.ReporterProfilePhoto = FilterReporter[0].MemberProfilePhoto;
                                       $scope.todoDescriptionObject.ReporterEmail = FilterReporter[0].MemberEmail;
                                   }
                                   else {
                                       $scope.todoDescriptionObject.ReporterName = '';
                                       $scope.todoDescriptionObject.ReporterProfilePhoto = "Uploads/Default/profile.png";
                                       $scope.todoDescriptionObject.ReporterEmail = '';
                                   }
                                   $scope.todoDescriptionObject.ReporterId = currentUserId;
                                   if ($rootScope.activeProject != null && $rootScope.activeProject != undefined) {
                                       $scope.todoDescriptionObject.ProjectName = $rootScope.activeProject.ProjectName;
                                   }
                                   else {
                                       $scope.todoDescriptionObject.ProjectName = $scope.projectName;
                                   }

                                   $scope.todoDescriptionObject.SectionId = $scope.todoAddUpdateObject.SectionId;
                                   $scope.todoDescriptionObject.CreatedDate = new Date();
                                   $scope.todoDescriptionObject.ModifiedDate = new Date();
                                   $scope.todoDescriptionObject.EstimatedTime = $scope.todoAddUpdateObject.EstimatedTime;
                                   $scope.todoDescriptionObject.LoggedTime = 0;
                                   //todoDescriptionObject.CreatedDate = new Date();
                                   $scope.todoDescriptionObject.ModifiedDate = new Date();

                                   /*Adding the todoAddUpdateModel object*/
                                   $scope.todoAddUpdateObject.TodoId = data.message;
                                   $scope.todoAddUpdateObject.IsDone = false;
                                   $scope.todoAddUpdateObject.InProgress = false;
                                   $scope.todoAddUpdateObject.AssigneeProfilePhoto = AssigneeProfilePhoto;

                                   $scope.todoAddUpdateObject.SectionName = $scope.todoDescriptionObject.SectionName;
                                   $scope.todoAddUpdateObject.AssigneeName = $scope.todoDescriptionObject.AssigneeName;
                                   $scope.todoAddUpdateObject.ReporterProfilePhoto = $scope.todoDescriptionObject.ReporterProfilePhoto;
                                   $scope.todoAddUpdateObject.ReporterName = $scope.todoDescriptionObject.ReporterName;
                                   $scope.todoAddUpdateObject.ReporterEmail = $scope.todoDescriptionObject.ReporterEmail;
                                   $scope.todoAddUpdateObject.ReporterId = currentUserId;
                                   $scope.todoAddUpdateObject.AssigneeEmail = $scope.todoDescriptionObject.AssigneeEmail;
                                   //$scope.todoAddUpdateModel.AssigneeId = $scope.todoDescriptionObject.AssigneeId;
                                   $scope.todoAddUpdateObject.TeamName = $scope.todoDescriptionObject.TeamName;
                                   $scope.todoAddUpdateObject.ProjectName = $scope.todoDescriptionObject.ProjectName;
                                   $scope.todoAddUpdateObject.CreatedDate = $scope.todoDescriptionObject.CreatedDate;
                                   $scope.todoAddUpdateObject.ModifiedDate = $scope.todoDescriptionObject.ModifiedDate;
                                   $scope.todoAddUpdateObject.LoggedTime = 0;

                                   if (addType == "middle") {
                                       todoIndex = todoIndex + 1;
                                       //adding todo on particular index to the section
                                       $scope.Sections.forEach(function (entry) {
                                           if (entry.Id == $scope.todoAddUpdateObject.SectionId) {
                                               entry.Todos.splice($scope.todoAddUpdateObject.TodoListOrder, 0, $scope.todoAddUpdateObject);
                                           }
                                       });
                                   }
                                   else {
                                       //adding todo to the section
                                       $scope.Sections.forEach(function (entry) {
                                           if (entry.Id == $scope.todoAddUpdateObject.SectionId) {
                                               entry.Todos.push($scope.todoAddUpdateObject);
                                           }
                                       });
                                   }

                                   $timeout(function () {
                                       $scope.$apply();
                                       $scope.currentTodoIdForRow = data.message;
                                       $scope.sectionIndex = sectionIndex;
                                       $scope.todoIndex = todoIndex;
                                       if ($scope.selectedTodoView == 'list') {
                                           $scope.showTodoDescriptionSection($scope.todoAddUpdateObject, $scope.sectionIndex, $scope.todoIndex);
                                       }
                                   });

                                   $timeout(function () {
                                       $("#" + data.message).focus();
                                   }, 100);
                               }
                               else {
                                   if (data.message == null) {
                                       toaster.error("Error", data.errors.toString());
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }
                                   //$('#globalLoader').hide();
                               }
                           })
                            .error(function () {
                                //$('#globalLoader').hide();
                                toaster.error("Error", "Some Error Occured !")
                            });
                       },



                       $scope.editTodo = function (TodoObj, sectionIndex, todoIndex) {
                           debugger;
                           //$scope.currentTodoIdForRow = TodoObj.TodoId;
                           //$timeout(function () {
                           //    $("#" + TodoObj.TodoId).focus();
                           //    $scope.sectionIndex = sectionIndex;
                           //    $scope.todoIndex = todoIndex;
                           //    $scope.showTodoDescriptionSection(TodoObj, $scope.sectionIndex, $scope.todoIndex);
                           //});

                           $scope.sectionIndex = sectionIndex;
                           $scope.todoIndex = todoIndex;
                           $scope.showTodoDescriptionSection(TodoObj, $scope.sectionIndex, $scope.todoIndex);
                       }


                       /* Update Todo*/
                       $scope.updateTodo = function (SectionId) {

                           $scope.currentTodoIdForRow = 0;
                           $scope.todoAddUpdateModel.isEdit = true;
                           $scope.todoAddUpdateModel.TaskTypeId = $scope.Tags.TodoTaskType;
                           var model = {
                               'TodoUpdateModel': $scope.todoAddUpdateModel, 'userId': sharedService.getUserId(),
                               'ProjectId': $scope.selectedProject.ProjectId, 'CompanyId': sharedService.getCompanyId()
                           }
                           todosFactory.updateTodo(model).success(function (data) {

                               if (data.success) {

                               }
                               else {
                                   if (data.message == null) {
                                       toaster.error("Error", data.errors.toString());
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }
                                   //$('#globalLoader').hide();
                               }
                           }).error(function () {
                               //$('#globalLoader').hide();
                               toaster.error("Error", "Some Error Occured !");
                           });
                       },


                       /* Delete Todo */
                       $scope.deleteTodo = function ($event, todoId, sectionIndex, todoIndex,isTodoInProgress) {

                           if (isTodoInProgress) {
                               toaster.warning("Warning", "Todo is still in Progress");
                               return;
                           }
                           else {
                               $event.stopPropagation();
                               $confirm({ text: 'Are you sure you want to delete this todo ?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
                                   .then(function () {
                                       $('#globalLoader').show(); 
                                       todosFactory.deleteTodo(todoId).success(function (data) {
                                           if (data.success) {

                                               $scope.Sections[sectionIndex].Todos.splice(todoIndex, 1);
                                               $scope.hideRightWindowFunction();
                                               //toaster.success("Success", data.message);
                                               $('#globalLoader').hide();
                                           }
                                           else {
                                               toaster.error("Error", data.message);
                                               $('#globalLoader').hide();
                                           }
                                       }).error(function () {
                                           toaster.error("Error", "Some Error Occured !");
                                           $('#globalLoader').hide();
                                       });
                                   });
                           }
                       },


                       /* Showing Todo Description Window */
                       $scope.showTodoDescriptionSection = function (todoObject, sectionIndex, todoIndex) {
                           $scope.todoIndex = todoIndex;
                           $scope.sectionIndex = sectionIndex;

                           $scope.toDoId = todoObject.TodoId;
                           $scope.myTodoUpdate = false;
                           $scope.selectedTodo = todoObject.TodoId;
                           $scope.stretchCentreWindow = false;
                           $scope.todoDescriptionObject = todoObject;
                           $scope.todoDescriptionObject.ReporterName = todoObject.ReporterName;
                           $scope.closeRightSections();
                           $scope.UserRole = sharedService.getRoleId();

                           if (todoObject.AssigneeId != sharedService.getUserId()) {
                               $scope.IsUserAllowed = false;
                           }
                           else {
                               $scope.IsUserAllowed = true;
                           }

                           if ($scope.selectedTodoView == 'list') {
                               $scope.hideRightWindow = false;
                               $('#divToDoDescriptionSection').show();
                               $('#hideRightWindow').show();
                               // This below line is for clearing the textbox for comments in Todo Description pannel
                               $('#txtComments').html('');
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               $scope.showassigneesearch = false;                // To hide assignee search section
                               $scope.showattachmentsearch = false;             // To hide attachment search section

                               $scope.stretchCentreWindow = true;
                               $scope.hideRightWindow = true;
                               $('#modalToDoDescriptionForKanban').modal('show');
                               // This below line is for clearing the textbox for comments in Todo Description pannel
                               $('#txtCommentsForKanban').html('');
                           }




                           //$scope.LoggedTimeStyle = createMinutesFromTimeString(todoObject.EstimatedTime, todoObject.LoggedTime);

                           /* Getting Tags For Todos*/
                           todosFactory.getTodoTags($scope.todoDescriptionObject.TaskType, todoObject.TodoId)
                               .success(function (data) {
                                   $scope.TodoTagsObject = data.ResponseData;
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                               });


                           /* Getting Attachments Corresponding To ToDo/Task*/
                           attachmentsFactory.getToDoAttachments(todoObject.TodoId, sharedService.getUserId())
                               .success(function (data) {
                               if (data.success) {
                                   $scope.attachmentsObject = data.ResponseData;
                                   $scope.AllAttachmentsForComment = data.ResponseData;
                                   $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/computer.png' });
                                   $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/logo-drive.png' });
                                   $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/drop-box.png' });
                                   $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/box-icon.png' });
                               }
                               else {
                                   toaster.error("Error", data.message);
                               }
                           })
                           .error(function () {
                               toaster.error("Error", "Some Error Occured !");
                           });

                           /* Get Followers for the Todo*/
                           followersFactory.getFollowers(todoObject.TodoId)
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.followersObject = data.ResponseData;
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }
                               })
                           .error(function () {
                               toaster.error("Error", "Some Error Occured !");
                           });

                           /*Getting Todo Comments*/
                           getTodoCommentsInArrangedMode(todoObject.TodoId);

                           /* Getting Todo Notification or We can Say History  */
                           todoNotificationsFactory.getTodoNotifications(todoObject.TodoId)
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.todoNotificationObject = data.ResponseData;
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                               });
                       };


                       /* Filtering Todos with Status for eg: Completed Todos, InProgress Todos, IsDone Todos */
                       $scope.filterTodosWithStatus = function (filterValue) {

                           if (filterValue == "Completed") {
                               $scope.inProgressFilter = false;
                               $scope.isDoneFilter = true;
                           }
                           else if (filterValue == "Pending") {
                               $scope.inProgressFilter = false;
                               $scope.isDoneFilter = false;
                           }
                           else if (filterValue == "InProgress") {
                               $scope.inProgressFilter = true;
                               $scope.isDoneFilter = false;
                           }
                           else {
                               $scope.inProgressFilter = '';
                               $scope.isDoneFilter = '';
                           }
                       },

                       /* Filtering Todos with Role for eg: As Assignee, as Reporter etc */
                       $scope.filterTodosWithRole = function (filterValue) {

                           if (filterValue == "Assignee") {
                               $scope.assigneeFilter = $scope.MyProfileObject.userId;
                               $scope.reporterFilter = "";
                           }
                           else if (filterValue == "Reporter") {
                               $scope.assigneeFilter = "";
                               $scope.reporterFilter = $scope.MyProfileObject.userId;
                           }

                           else if (filterValue == "AssigneeReporter") {
                               $scope.assigneeFilter = $scope.MyProfileObject.userId;
                               $scope.reporterFilter = $scope.MyProfileObject.userId;
                           }
                           else {
                               $scope.assigneeFilter = "";
                               $scope.reporterFilter = "";
                           }

                       },


                       /* Show Project Members Modal Popup */
                       $scope.showProjectMembersModal = function () {
                           $('#modalProjectMembers').modal('show');
                       },


                       /* Start Progress Of Todo */
                       $scope.startProgress = function (todoObject, sectionIndex, todoIndex) {
                           // if $scope.getInProgressTodo() retuns not null object means it find the running todo
                           //Stop the timer for the task on the global page
                           //$('#globalLoader').show();

                           //$scope.$emit('stopTimer', []);
                           
                           var newTodObject = $scope.getInProgressTodo();
                           var oldTodoObject = todoObject;
                           var oldSectionIndex = sectionIndex;
                           var oldTodoIndex = todoIndex;
                           if (newTodObject.obj != undefined) {
                               var alertText = '"' + newTodObject.obj.Name + '" todo is already in progress. Are you sure you want to stop that and start "' + todoObject.Name + '" todo?';
                               $confirm({ text: alertText, title: 'Other todo is in progress', ok: 'Yes', cancel: 'No' })
                               .then(function (todoObject, sectionIndex, todoIndex) {
                                   $('#globalLoader').show();
                                   $scope.$emit('stopTimer', []);
                                   //$scope.stopProgress(newTodObject.obj, newTodObject.sectionIndex, newTodObject.todoIndex);
                                   //$scope.StartProgressInternal(oldTodoObject, oldSectionIndex, oldTodoIndex);
                                   //$scope.stopProgressInternal(newTodObject.obj, newTodObject.sectionIndex, newTodObject.todoIndex, oldTodoObject, oldSectionIndex, oldTodoIndex);
                                   $scope.stopProgressInternal(newTodObject.obj, newTodObject.sectionId, newTodObject.todoIndex, oldTodoObject, oldSectionIndex, oldTodoIndex);
                               });
                           }
                               // this means no running todo found
                           else {
                               $('#globalLoader').show();
                               $scope.$emit('stopTimer', []);
                               $scope.StartProgressInternal(todoObject, sectionIndex, todoIndex);

                           }


                       };


                       /* Stop Progress Of Todo */
                       $scope.stopProgress = function (todoObject, sectionIndex, todoIndex) {

                           $('#globalLoader').show();
                           $scope.todoAddUpdateModel = {};
                           $scope.todoAddUpdateModel.TodoId = todoObject.TodoId;
                           $scope.todoAddUpdateModel.Name = todoObject.Name;
                           $scope.todoAddUpdateModel.DeadlineDate = todoObject.DeadlineDate;
                           $scope.todoAddUpdateModel.EstimatedTime = todoObject.EstimatedTime;
                           $scope.todoAddUpdateModel.Description = todoObject.Description;
                           $scope.todoAddUpdateModel.SectionId = todoObject.SectionId;
                           $scope.todoAddUpdateModel.AssigneeId = todoObject.AssigneeId;
                           $scope.todoAddUpdateModel.ReporterId = todoObject.ReporterId;
                           $scope.todoAddUpdateModel.InProgress = false;
                           $scope.todoAddUpdateModel.IsDone = todoObject.IsDone;
                           $scope.todoAddUpdateModel.IsAvailableForInvoice = true;
                           $scope.todoAddUpdateModel.TaskTypeId = $scope.Tags.TodoTaskType;
                           var NewTodoAddObj = {
                               'TodoUpdateModel': $scope.todoAddUpdateModel, 'userId': sharedService.getUserId(),
                               'ProjectId': $scope.selectedProject.ProjectId, 'CompanyId': sharedService.getCompanyId()
                           }
                           $timeout(function () {
                               $scope.$apply();
                           }, 1000);
                           $scope.todoAddUpdateModel.isEdit = false;
                           todosFactory.updateTodo(NewTodoAddObj)

                               .success(function (data) {
                                   if (data.success) {
                                       var currentSectionId = $scope.Sections[sectionIndex].Id;
                                       if (currentSectionId != undefined) {
                                           for (j = 0; j < $scope.SectionsForAllProjects.length; j++) {
                                               if (currentSectionId == $scope.SectionsForAllProjects[j].Id) {
                                                   $scope.SectionsForAllProjects[j].Todos[todoIndex].InProgress = false;
                                               }
                                           }
                                       }

                                       $scope.todoDescriptionObject.InProgress = false;
                                       $scope.todoDescriptionObject.ModifiedDate = new Date();
                                       //Stop the timer for the task on the global page
                                       $scope.$emit('stopTimer', []);

                                       /*Get estimated time from server*/
                                       todosFactory.getLoggedTime(todoObject.TodoId, sharedService.getUserId()).success(function (data) {

                                           if (data.success) {
                                               $scope.Sections[sectionIndex].Todos[todoIndex].LoggedTime = data.ResponseData;
                                               $scope.todoDescriptionObject.LoggedTime = data.ResponseData;
                                               var taskName = $scope.Sections[sectionIndex].Todos[todoIndex].Name;
                                               var formatedMessage = createFormatedTimeString(data.ResponseData);

                                               var estimatedTime = $scope.Sections[sectionIndex].Todos[todoIndex].EstimatedTime;
                                               //var styleProperty = createMinutesFromTimeString(estimatedTime, $scope.todoDescriptionObject.LoggedTime)
                                               //$scope.LoggedTimeStyle = styleProperty;

                                               var logmessage = formatedMessage + " are logged for " + taskName;
                                               $timeout(function () {
                                                   $scope.$apply();
                                               }, 1000);
                                               $('#globalLoader').hide();
                                               toaster.success("Success", logmessage);
                                           }
                                           else {
                                               $scope.Sections[sectionIndex].Todos[todoIndex].LoggedTime = '0h 0m';
                                               $scope.todoDescriptionObject.LoggedTime = '0h 0m';
                                               $('#globalLoader').hide();
                                               toaster.error("Error", data.message);
                                           }
                                       }).error(function () {
                                           $('#globalLoader').hide();
                                           toaster.error("Error", "Some error occured !");
                                       });

                                       /*call the notification service */
                                       $scope.getNotifications($scope.todoDescriptionObject.TodoId);
                                       $('#globalLoader').hide();
                                       toaster.success("Success", "Progress is stopped ");

                                   }
                                   else {
                                       $('#globalLoader').hide();
                                       toaster.error("Error", data.message);
                                   }
                               })
                               .error(function () {
                                   $('#globalLoader').hide();
                                   toaster.error("Error", "Some Error Occured !");
                               });

                       };


                       /* Mark Todo As Done */
                       $scope.isDone = function (todoObject, sectionIndex, todoIndex) {
                           $('#globalLoader').show();
                           debugger;
                           $scope.todoAddUpdateModel = {};
                           $scope.todoAddUpdateModel.TodoId = todoObject.TodoId;
                           $scope.todoAddUpdateModel.Name = todoObject.Name;
                           $scope.todoAddUpdateModel.DeadlineDate = todoObject.DeadlineDate;
                           $scope.todoAddUpdateModel.EstimatedTime = todoObject.EstimatedTime;
                           $scope.todoAddUpdateModel.Description = todoObject.Description;
                           $scope.todoAddUpdateModel.SectionId = todoObject.SectionId;
                           $scope.todoAddUpdateModel.AssigneeId = todoObject.AssigneeId;
                           $scope.todoAddUpdateModel.ReporterId = todoObject.ReporterId;
                           $scope.todoAddUpdateModel.InProgress = false;
                           $scope.todoAddUpdateModel.PreviouslyInProgress = todoObject.InProgress;
                           $scope.todoAddUpdateModel.IsDone = true;
                           $scope.todoAddUpdateModel.IsAvailableForInvoice = true;
                           $scope.todoAddUpdateModel.TaskTypeId = $scope.Tags.TodoTaskType;
                           var NewTodoAddObj = {
                               'TodoUpdateModel': $scope.todoAddUpdateModel, 'userId': sharedService.getUserId(),
                               'ProjectId': $scope.selectedProject.ProjectId, 'CompanyId': sharedService.getCompanyId()
                           }
                           $scope.todoAddUpdateModel.isEdit = false;
                           todosFactory.updateTodo(NewTodoAddObj)
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.todoDescriptionObject.IsDone = true;
                                       $scope.todoDescriptionObject.InProgress = true;
                                       $scope.todoDescriptionObject.ModifiedDate = new Date();
                                       //Stop the timer 
                                       $scope.$emit('stopTimer', []);

                                       var currentSectionId = $scope.Sections[sectionIndex].Id;
                                       if (currentSectionId != undefined) {
                                           for (j = 0; j < $scope.SectionsForAllProjects.length; j++) {
                                               if (currentSectionId == $scope.SectionsForAllProjects[j].Id) {
                                                   $scope.SectionsForAllProjects[j].Todos[todoIndex].InProgress = false;
                                               }
                                           }
                                       }

                                       /*Get estimated time from server*/
                                       todosFactory.getLoggedTime($scope.todoAddUpdateModel.TodoId, sharedService.getUserId()).success(function (data) {

                                           if (data.success) {
                                               $scope.Sections[sectionIndex].Todos[todoIndex].LoggedTime = data.ResponseData;
                                               $scope.todoDescriptionObject.LoggedTime = data.ResponseData;
                                               //------------------------
                                               var taskName = $scope.Sections[sectionIndex].Todos[todoIndex].Name;
                                               var formatedMessage = createFormatedTimeString(data.ResponseData);
                                               var logmessage = formatedMessage + " are logged for " + taskName;
                                               $('#globalLoader').hide();
                                               toaster.success("Success", logmessage);
                                           }
                                           else {
                                               $scope.Sections[sectionIndex].Todos[todoIndex].LoggedTime = '0h 0m';
                                               $scope.todoDescriptionObject.LoggedTime = '0h 0m';
                                               $('#globalLoader').hide();
                                               toaster.error("Error", data.message);
                                           }
                                       }).error(function () {
                                           $('#globalLoader').hide();
                                           toaster.error("Error", "Some error occured !");
                                       });

                                       /*call the notification service */
                                       $scope.getNotifications($scope.todoDescriptionObject.TodoId);
                                       $('#globalLoader').hide();
                                       toaster.success("Success", "Todo is Complete !");
                                   }
                                   else {
                                       $('#globalLoader').hide();
                                       toaster.error("Error", data.message);
                                   }
                               })
                               .error(function () {
                                   $('#globalLoader').hide();
                                   toaster.error("Error", "Some Error Occured !");
                               });

                       };


                       /* Reopen Todo And Start Progress */
                       $scope.reOpenTodoAndStartProgress = function (todoObject) {
                           $('#globalLoader').show();
                           $scope.todoAddUpdateModel = {};
                           $scope.todoAddUpdateModel.TodoId = todoObject.TodoId;
                           $scope.todoAddUpdateModel.Name = todoObject.Name;
                           $scope.todoAddUpdateModel.DeadlineDate = todoObject.DeadlineDate;
                           $scope.todoAddUpdateModel.EstimatedTime = todoObject.EstimatedTime;
                           $scope.todoAddUpdateModel.Description = todoObject.Description;
                           $scope.todoAddUpdateModel.SectionId = todoObject.SectionId;
                           $scope.todoAddUpdateModel.AssigneeId = todoObject.AssigneeId;
                           $scope.todoAddUpdateModel.ReporterId = todoObject.ReporterId;
                           $scope.todoAddUpdateModel.InProgress = true;
                           $scope.todoAddUpdateModel.IsDone = false;
                           $scope.todoAddUpdateModel.TaskTypeId = $scope.Tags.TodoTaskType;
                           var NewTodoAddObj = {
                               'TodoUpdateModel': $scope.todoAddUpdateModel, 'userId': sharedService.getUserId(),
                               'ProjectId': $scope.selectedProject.ProjectId, 'CompanyId': sharedService.getCompanyId()
                           }
                           $scope.todoAddUpdateModel.isEdit = false;
                           todosFactory.updateTodo(NewTodoAddObj)
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.todoDescriptionObject.IsDone = false;
                                       $scope.todoDescriptionObject.InProgress = true;
                                       $scope.todoDescriptionObject.ModifiedDate = new Date();

                                       /*call the notification service */
                                       $scope.getNotifications($scope.todoDescriptionObject.TodoId);

                                       $('#globalLoader').hide();
                                       toaster.success("Success", "Todo is in Progress");
                                   }
                                   else {
                                       $('#globalLoader').hide();
                                       toaster.error("Error", data.message);
                                   }
                               })
                               .error(function () {
                                   $('#globalLoader').hide();
                                   toaster.error("Error", "Some Error Occured !");
                               });
                       };


                       /* Showing Date calendar for Deadline Date While Saving New TOdo*/
                       $scope.showCalendar = function ($event) {

                           $event.preventDefault();
                           $event.stopPropagation();
                           $scope.deadlineCalendarOpened = true;
                       },


                       /* Showing MyTodosManagementSection*/
                       $scope.showMyTodosManagementSection = function () {
                           //closeCentreSections();
                           $scope.assigneeFilter = "";
                           $scope.reporterFilter = "";
                           $scope.statusFilter = "";
                           $scope.roleFilter = "";
                           $scope.inProgressFilter = "";
                           $scope.isDoneFilter = "";
                           $scope.Sections = [];
                           $scope.selectedProjectInMyTodosSection = [];
                           $scope.selectedTeamInMyTodosSection = [];
                           $scope.selectedNavigation = "MyTodos";

                           $('#divMyTodosManagementSection').show();

                           /* It gets MyTodosWithSections */
                           todosFactory.getMyTodosWithSections(sharedService.getUserId())
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.MyTodosObject = data.ResponseData;

                                       /* It Gets My Projects With Teams*/
                                       projectsFactory.getMyProjectsWithTeams(sharedService.getUserId())
                                           .success(function (data) {
                                               if (data.success) {
                                                   $scope.MyTeamsAndProjectsObject = data.ResponseData;
                                               }
                                               else {
                                                   toaster.error("Error", data.message);
                                               }
                                           })
                                           .error(function () {
                                               toaster.error("Error", "Some Error Occured !");
                                           });
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                               });
                       };


                       /* Edit Todo From MyTodosManagement Section*/
                       $scope.editMyTodo = function (todoObject) {

                           $scope.todoDescriptionObject = todoObject;
                           //$scope.todoEditIndex = todoObject.todoIndex;
                           $scope.filterProjectId = todoObject.ProjectId;
                           $scope.Sections = [];
                           $scope.todoUpdateButton = true;
                           $scope.myTodoUpdate = true;
                           $scope.todoAddUpdateModel = [];
                           $scope.todoAddUpdateModel.TodoId = todoObject.TodoId;
                           $scope.todoAddUpdateModel.Name = todoObject.Name;
                           $scope.todoAddUpdateModel.DeadlineDate = todoObject.DeadlineDate;
                           $scope.todoAddUpdateModel.Description = todoObject.Description;
                           $scope.todoAddUpdateModel.SectionId = todoObject.SectionId;
                           $scope.todoAddUpdateModel.AssigneeId = todoObject.AssigneeId;
                           $scope.todoAddUpdateModel.InProgress = todoObject.InProgress;
                           $scope.todoAddUpdateModel.IsDone = todoObject.IsDone;
                           $scope.previousAssigneeId = todoObject.AssigneeId;

                           /* It fetches Sections for a particular Project */
                           sectionsFactory.getSections(todoObject.ProjectId)
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.Sections = data.ResponseData;
                                       $timeout(function () {
                                           $scope.$apply();
                                       });
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                               });

                           /* It fetches Project Members */
                           projectUsersFactory.getProjectMembers(todoObject.ProjectId)
                               .success(function (data) {
                                   if (data.success) {
                                       $scope.projectMembersObject = data.ResponseData;
                                       $timeout(function () {
                                           $scope.$apply();
                                           $('#modalTodoAddUpdate').modal('show');
                                       });
                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                               });
                       };


                       /* Update Todo From MyTodosManagement*/
                       $scope.updateMyTodo = function (isTodoNameInvalid, isSectionInvalid) {
                           if (isTodoNameInvalid || isSectionInvalid) {
                               $scope.addUpdateTodoForm.name.$dirty = true;
                               $scope.addUpdateTodoForm.section.$dirty = true;
                               return;
                           }

                           $('#globalLoader').show();
                           $scope.todoAddUpdateModel.isEdit = true;
                           todosFactory.updateTodo($scope.todoAddUpdateModel)
                               .success(function (data) {
                                   if (data.success) {
                                       /* Updating Current Todo Row through angular */
                                       $scope.todoDescriptionObject.Name = $scope.todoAddUpdateModel.Name;
                                       $scope.todoDescriptionObject.SectionId = $scope.todoAddUpdateModel.SectionId;
                                       $scope.todoDescriptionObject.Description = $scope.todoAddUpdateModel.Description;
                                       $scope.todoDescriptionObject.EstimatedTime = $scope.todoAddUpdateModel.EstimatedTime;
                                       $scope.todoDescriptionObject.DeadlineDate = $scope.todoAddUpdateModel.DeadlineDate;
                                       $scope.todoDescriptionObject.AssigneeId = $scope.todoAddUpdateModel.AssigneeId;
                                       /* Updating Calendar Title */
                                       $scope.calendarTodoDescription.Title = $scope.todoAddUpdateModel.Name;
                                       //$scope.MyTodosObject[$scope.todoEditIndex].name = $scope.todoAddUpdateModel.name;
                                       //$scope.MyTodosObject[$scope.todoEditIndex].sectionId = $scope.todoAddUpdateModel.sectionId;
                                       //$scope.MyTodosObject[$scope.todoEditIndex].description = $scope.todoAddUpdateModel.description;
                                       //$scope.MyTodosObject[$scope.todoEditIndex].deadlineDate = $scope.todoAddUpdateModel.deadlineDate;
                                       //$scope.MyTodosObject[$scope.todoEditIndex].assigneeId = $scope.todoAddUpdateModel.assigneeId;
                                       var currentAssigneeId = $scope.todoAddUpdateModel.AssigneeId;
                                       var previousAssigneeId = $scope.PreviousAssigneeId;
                                       if (currentAssigneeId != previousAssigneeId) {
                                           var AssigneeProfilePhoto;
                                           if ($scope.todoAddUpdateModel.AssigneeId == undefined) {
                                               AssigneeProfilePhoto = "Uploads/Default/profile.png";
                                           }
                                           else {
                                               var FilterProfilePhotoOfAssignee = $filter('filter')($scope.projectMembersObject, { 'memberUserId': $scope.todoAddUpdateModel.AssigneeId });
                                               AssigneeProfilePhoto = FilterProfilePhotoOfAssignee[0].MemberProfilePhoto;
                                           }
                                           $scope.todoDescriptionObject.AssigneeProfilePhoto = AssigneeProfilePhoto;
                                       }
                                       $timeout(function () {
                                           $scope.$apply();
                                           toaster.success("Success", data.message);
                                           $('#modalTodoAddUpdate').modal('hide');
                                           $('#globalLoader').hide();
                                       });
                                   }
                                   else {
                                       if (data.message == null) {
                                           toaster.error("Error", data.errors.toString());
                                       }
                                       else {
                                           toaster.error("Error", data.message);
                                       }
                                       $('#globalLoader').hide();
                                   }
                               })
                               .error(function () {
                                   $('#globalLoader').hide();
                                   toaster.error("Error", "Some Error Occured !");
                               });
                       };


                       /* Showing Todo Description Window */
                       $scope.showMyTodoDescriptionSection = function (todoObject) {

                           $scope.myTodoUpdate = true;
                           $scope.selectedTodo = todoObject.TodoId;
                           $scope.hideRightWindow = false;
                           $scope.stretchCentreWindow = false;
                           $scope.todoDescriptionObject = todoObject;

                           $scope.closeRightSections();
                           $('#divToDoDescriptionSection').show();


                           /* Getting Attachments Corresponding To ToDo/Task*/
                           attachmentsFactory.getToDoAttachments(todoObject.TodoId, sharedService.getUserId())
                               .success(function (data) {

                                   if (data.success) {

                                       $scope.attachmentsObject = data.ResponseData;

                                       $scope.AllAttachmentsForComment = data.ResponseData;

                                       $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/computer.png' });
                                       $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/logo-drive.png' });
                                       $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/drop-box.png' });
                                       $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/box-icon.png' });


                                   }
                                   else {
                                       toaster.error("Error", data.message);
                                   }

                               })
                               .error(function () {

                                   toaster.error("Error", "Some Error Occured !");

                               });

                           /* Get Followers for the Todo*/
                           followersFactory.getFollowers(todoObject.TodoId).success(function (data) {

                               if (data.success) {
                                   $scope.followersObject = data.ResponseData;
                               }
                               else {
                                   toaster.error("Error", data.message);
                               }
                           }).error(function () {
                               toaster.error("Error", "Some Error Occured !");
                           });


                           /*Getting Todo Comments*/
                           getTodoCommentsInArrangedMode(todoObject.todoId);


                           /* Getting Project Members */
                           projectUsersFactory.getProjectMembers(todoObject.projectId).success(function (data) {

                               debugger;
                               if (data.success) {

                                   $scope.projectMembersObject = data.ResponseData;

                                   $timeout(function () {
                                       $scope.$apply();
                                   });

                               }
                               else {

                                   toaster.error("Error", data.message);
                               }

                           }).error(function () {

                               toaster.error("Error", "Some Error Occured !");

                           });

                       };


                       /* Change Team Filter */
                       $scope.changeTeamFilter = function () {

                           $scope.selectedProjectInMyTodosSection = [];

                       },


                       /* Configuration for adding assignee for the Todo */
                       $scope.todoAssigneeConfig = {
                           create: false,
                           maxItems: 1,
                           required: true,
                           plugins: ['remove_button'],
                           valueField: 'Value',
                           labelField: 'Text',
                           delimiter: '|',
                           // placeholder: 'Pick Project Members...',
                       }

                       $scope.assigneeConfig = {
                           create: false,
                           maxItems: 1,
                           required: true,
                           valueField: 'Value',
                           labelField: 'Text',
                           //delimiter: '|'
                       }

                       $scope.followerConfig = {
                           create: false,
                           //required: true,
                           //delimiter: '|',
                           valueField: 'Value',
                           labelField: 'Text',
                           //plugins: ['remove_button'],
                           placeholder: 'Pick followers...',
                       }

                       /* Get Only Todo Comments in arranged mode */

                       function getTodoCommentsInArrangedMode(todoid) {

                           todosFactory.getToDoComments(todoid, sharedService.getUserId()).success(function (data) {

                               $scope.todocomments = [];
                               var totalLoop = data.length;
                               var FinalDescription = [];
                               if (totalLoop > 0) {

                                   for (var i = 0; i < totalLoop; i++) {

                                       var description = data[i].Description;
                                       var createdBy = data[i].CreatedBy;
                                       var isOwner = data[i].IsOwner;
                                       var createdDate = data[i].CreatedDate;
                                       var creatorProfilePhoto = data[i].CreatorProfilePhoto;
                                       var id = data[i].Id;
                                       var splittedDesc = description.split('@@');
                                       var temsplittedDescFiles = description.split('##');
                                       var newUserCount = splittedDesc.length - 1;
                                       var newFileCount = temsplittedDescFiles.length - 1;
                                       var splittedDescForFiles = '';
                                       var newCommentWithUserReplacements = '';
                                       var newCommentWithFileReplacements = '';



                                       if (newUserCount > 0) {

                                           for (var j = 0; j < newUserCount; j++) {

                                               if (data[i].UsersTagged[j] != undefined) {
                                                   newCommentWithUserReplacements += splittedDesc[j] + "<a class='taggedUsers' href='/#/user/profile/" + data[i].UsersTagged[j].UserId + "'> <i class='fa fa-user'></i> " + data[i].UsersTagged[j].FullName + "</a>";
                                               }
                                               else {
                                                   newCommentWithUserReplacements += splittedDesc[j] + "@@";
                                               }
                                           }
                                           newCommentWithUserReplacements += splittedDesc[newUserCount];
                                           splittedDescForFiles = newCommentWithUserReplacements.split('##');
                                       }
                                       else {

                                           newCommentWithUserReplacements = description;
                                           splittedDescForFiles = description.split('##');
                                       }

                                       if (newFileCount > 0) {
                                           for (var k = 0; k < newFileCount; k++) {

                                               if (data[i].FilesTagged[k] == undefined) {
                                                   //if (data[i].FilesTagged[k].isGif == false) {
                                                   //newCommentWithFileReplacements += splittedDescForFiles[k] + "<a class='taggedUsers' href='"+ $scope.imgURL + data[i].FilesTagged[k].FilePath + "' target='_blank' > <i class='fa fa-file'></i> " + data[i].FilesTagged[k].OriginalName + "</a>";
                                                   newCommentWithFileReplacements += splittedDescForFiles[k] + "##";
                                               }
                                               else {
                                                   //newCommentWithFileReplacements += splittedDescForFiles[k] + "<a class='taggedUsers' href='" + $scope.imgURL + data[i].FilesTagged[k].FilePath + "' target='_blank' > <i class='fa fa-file-movie-o'></i> gif:" + data[i].FilesTagged[k].OriginalName + "</a>" + "<img class='taggedGifInComment' width='371' height='200' src='" + $scope.imgURL + data[i].FilesTagged[k].FilePath + "'  />";

                                                   newCommentWithFileReplacements += splittedDescForFiles[k] + "<a title=" + data[i].FilesTagged[k].OriginalName + " style='margin-left:5px;margin-right:5px;'  target='_blank' " + " href='" + $scope.imgURL + data[i].FilesTagged[k].FilePath + "' class='ng-binding'  > " + data[i].FilesTagged[k].OriginalName + "</a> ";
                                               }
                                           }
                                           newCommentWithFileReplacements += splittedDescForFiles[newFileCount];

                                       }
                                       else {

                                           newCommentWithFileReplacements = newCommentWithUserReplacements;
                                       }

                                       $scope.pushComment = {};
                                       $scope.pushComment.Description = newCommentWithFileReplacements;
                                       $scope.pushComment.CreatedBy = createdBy;
                                       $scope.pushComment.Id = id;
                                       $scope.pushComment.IsOwner = isOwner;
                                       $scope.pushComment.CreatedDate = createdDate;
                                       $scope.pushComment.CreatorProfilePhoto = creatorProfilePhoto;

                                       $scope.todocomments.push($scope.pushComment);
                                       //var lastDesc = { description: newCommentWithFileReplacements, createdBy: createdBy, id: id, creatorProfilePhoto: creatorProfilePhoto };
                                       //FinalDescription.push(lastDesc);


                                   }


                               }
                           })

                       };


                       // onload
                       angular.element(document).ready(function () {
                          
                           //window.Selectize = require('selectize');
                           $('#globalLoader').show();
                           if ($location.path() == '/mytodo') {
                               $scope.showMyTodosManagementSection();
                           }
                           else {

                               var pid = $stateParams.pid;
                               var tid = $stateParams.tid;
                               $scope.activeTeam = {};
                               $scope.activeTeam.TeamId = tid;
                               projectsFactory.getProjects(sharedService.getUserId(), tid, $scope.selectedCompanyId, sharedService.getIsFreelancer())
                                   .success(function (data) {
                                       if (data.success) {
                                           $scope.projects = data.ResponseData;
                                           var projectObj = $scope.projects.filter(function (item) { return item.ProjectId == pid; })
                                           $scope.showTodoManagementSection(projectObj[0]);
                                           $timeout(function () {

                                               $scope.$apply();
                                               $('#globalLoader').hide();
                                               $('#divToDoDescriptionSection').slimScroll({
                                                   //position: 'left',
                                                   height: '95%',
                                                   railVisible: false,
                                                   alwaysVisible: false
                                               });
                                               $('#divToDoDescriptionSection').css('width', '97%');
                                           });
                                       }
                                       else {
                                           toaster.error("Error", data.message);
                                       }
                                   }).error(function () {
                                       toaster.error("Error", "Some Error Occured !");
                                   });

                           }
                       });

                       ////////////Date Functions/////////////
                       /************************/
                       $scope.today = function () {
                           $scope.dt = new Date();
                       };
                       $scope.today();

                       $scope.clear = function () {
                           $scope.dt = null;
                       };

                       // Disable weekend selection
                       $scope.disabled = function (date, mode) {
                           return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
                       };
                       var da = new Date();
                       $scope.toggleMin = function () {
                           $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
                       };

                       $scope.toggleMin();

                       $scope.maxDate = new Date(da.getFullYear(), 5, 22);

                       $scope.open1 = function () {
                           $scope.popup1.opened = true;
                       };

                       $scope.open2 = function () {
                           $scope.popup2.opened = true;
                       };

                       $scope.setDate = function (year, month, day) {
                           $scope.dt = new Date(year, month, day);
                       };
                       //$scope.init = new Date(da.getFullYear()-10, 5, 22);
                       $scope.dateOptions = {
                           formatYear: 'yy',
                           startingDay: 1,

                       };

                       $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
                       $scope.format = $scope.formats[3];
                       $scope.altInputFormats = ['M!/d!/yyyy'];

                       $scope.popup1 = {
                           opened: false
                       };

                       $scope.popup2 = {
                           opened: false
                       };

                       $scope.getDayClass = function (date, mode) {
                           if (mode === 'day') {
                               var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                               for (var i = 0; i < $scope.events.length; i++) {
                                   var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                                   if (dayToCheck === currentDay) {
                                       return $scope.events[i].status;
                                   }
                               }
                           }

                           return '';
                       };
                       /***********************/

                       ////////////////////////////////////
                       /*Start of canban setting*/
                       ///////////////////////////////////
                       $scope.nestedOptions = {
                           dragStart: function (event) {
                           },
                           accept: function (sourceNodeScope, destNodesScope, destIndex) {

                               var destType = destNodesScope.$element.attr('data-type');
                               var sourceType = sourceNodeScope.$element.attr('data-type');
                               if (sourceType == "Section") {
                                   if (sourceType == destType) {
                                       return true;
                                   }
                                   return false;
                               }
                               else {
                                   if (destType == "Section") {
                                       return false;
                                   }
                                   return true;
                               }
                               //  if (destNodesScope.nodrop || destNodesScope.outOfDepth(sourceNodeScope)) {
                               //    return false;
                               //   }
                               //return true;
                               //  var data = sourceNodeScope.$modelValue.datatype,
                               // destType = destNodesScope.$nodeScope.$modelValue.datatype;//destNodesScope.$element.attr('data-type');//destNodes.$element.attr('data-type');
                               //  alert(data + " and " + destType);

                               return true;//(data != destType); // only accept the same type
                           },
                           dropped: function (event) {

                               //alert('working');
                               /* to check if it moves in same parent node */
                               var MovedObject = event.source.nodeScope.$element.attr('data-type');
                               var isParent = event.dest.nodesScope.isParent(event.source.nodeScope);
                               /*  To Check whether its position was changed or it was not moved */
                               if (event.source.nodesScope.$modelValue == event.dest.nodesScope.$modelValue) {
                                   if (event.source.index == event.dest.index) {
                                       return;
                                   }
                               }
                               /* It means it is just sorting */
                               if (isParent == true) {
                                   if (MovedObject == "Section") {

                                       sectionsFactory.updateSectionOrder(event.dest.nodesScope.$modelValue).success(function (data) {
                                           //toaster.success("Success", "Moved Successfully");
                                       });
                                       //  alert("You Moved Section");
                                   }
                                   else {

                                       var destModelObject = event.dest.nodesScope.$modelValue;

                                       $.each(destModelObject, function (key, value) {
                                           this.activeUserId = sharedService.getUserId();
                                           this.activeTodoId = event.source.nodesScope.$modelValue.TodoId;
                                       });

                                       sectionsFactory.updateTodoOrder(destModelObject).success(function (data) {
                                           //toaster.success("Success", "Moved Successfully");
                                       });
                                       // alert("You Moved Todo");
                                   }
                               }
                                   /* It means it is sorting and dragging element from other object */
                               else {
                                   event.source.nodeScope.$modelValue.sectionId = event.dest.nodesScope.$parent.node.Id;
                                   event.source.nodeScope.$modelValue.sectionName = event.dest.nodesScope.$parent.node.SectionName;

                                   var destModelObject = event.dest.nodesScope.$modelValue;

                                   $.each(destModelObject, function (key, value) {
                                       this.activeUserId = sharedService.getUserId();
                                       this.activeTodoId = event.source.nodesScope.$modelValue.TodoId;
                                   });

                                   sectionsFactory.updateTodoOrder(destModelObject).success(function (data) {
                                       //toaster.success("Success", "Moved Successfully");
                                   });
                                   //  alert("You Moved Todo from Other Element");
                               }
                           },
                           beforeDrop: function (event) {
                               //if (!window.confirm('Are you sure you want to drop it here?')) {
                               //    event.source.nodeScope.$$apply = false;
                               //}
                           }
                       };


                       $scope.removeTodoFromList = function (scope) {
                           var TodoId = scope.$nodeScope.$modelValue.TodoId;
                           alert(TodoId);

                           scope.remove();
                       };

                       $scope.newSubItem = function (scope) {

                           var nodeData = scope.$modelValue;
                           nodeData.nodes.push({
                               id: nodeData.id * 10 + nodeData.nodes.length,
                               title: nodeData.title + '.' + (nodeData.nodes.length + 1),
                               nodes: []
                           });
                           //var nodeData = scope.$modelValue;
                           //nodeData.todos.push({
                           //    todoId: 1,
                           //    name: "New Todo",
                           //    profilePhoto: "Uploads/Default/profile.png"

                           //});
                       };

                       $scope.visible = function (item) {

                           return !($scope.query && $scope.query.length > 0
                           && item.title.indexOf($scope.query) == -1);

                       };

                       $scope.findNodes = function () {

                       };

                       $scope.data = [{
                           'id': 1,
                           'title': 'node1',
                           'nodes': [
                             {
                                 'id': 11,
                                 'title': 'node1.1',
                                 'nodes': [
                                   {
                                       'id': 111,
                                       'title': 'node1.1.1',
                                       'nodes': []
                                   }
                                 ]
                             },
                             {
                                 'id': 12,
                                 'title': 'node1.2',
                                 'nodes': []
                             }
                           ]
                       }, {
                           'id': 2,
                           'title': 'node2',
                           'nodes': [
                             {
                                 'id': 21,
                                 'title': 'node2.1',
                                 'nodes': []
                             },
                             {
                                 'id': 22,
                                 'title': 'node2.2',
                                 'nodes': []
                             }
                           ]
                       }, {
                           'id': 3,
                           'title': 'node3',
                           'nodes': [
                             {
                                 'id': 31,
                                 'title': 'node3.1',
                                 'nodes': []
                             }
                           ]
                       }, {
                           'id': 4,
                           'title': 'node4',
                           'nodes': [
                             {
                                 'id': 41,
                                 'title': 'node4.1',
                                 'nodes': []
                             }
                           ]
                       }];




                       /* Angular Timer */
                       $scope.clock = "loading clock..."; // initialise the time variable
                       $scope.tickInterval = 1000 //ms

                       var tick = function () {
                           $scope.clock = Date.now() // get the current time
                           $timeout(tick, $scope.tickInterval); // reset the timer
                       }

                       // Start the timer
                       $timeout(tick, $scope.tickInterval);


                       $scope.barConfig = {
                           group: 'shared',

                           animation: 150,
                           sort: true,
                           draggable: false,
                           onSort: function (/** ngSortEvent */evt) {

                               // @see https://github.com/RubaXa/Sortable/blob/master/ng-sortable.js#L18-L24
                           }
                       };
                       $scope.sortTodos = {

                           animation: 150,
                           sort: true,
                           dataIdAttr: 'todo.id',
                           onSort: function (/** ngSortEvent */evt) {
                               alert('Sorting Todos');

                               // @see https://github.com/RubaXa/Sortable/blob/master/ng-sortable.js#L18-L24
                           }
                       };

                       //$scope.todos = [
                       //	{text: 'learn angular', done: true},
                       //	{text: 'build an angular app', done: false}
                       //];
                       $scope.todos = [{ "id": 2, "sectionName": "No Section", "projectId": 2, "todos": [{ "todoId": 24, "name": "Deep", "sectionId": 2, "profilePhoto": "Uploads/Default/profile.png" }] },
                           { "id": 4, "sectionName": "Low Priority", "projectId": 2, "todos": [] },
                           { "id": 5, "sectionName": "High Priority", "projectId": 2, "todos": [] },
                           { "id": 6, "sectionName": "Intermediate Priority", "projectId": 2, "todos": [{ "todoId": 19, "name": "Ubhawal", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 11, "name": "Second Task", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 14, "name": "Fifth Task", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 16, "name": "Hardeepinder Singh", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 6, "name": "koghioioo", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 12, "name": "Third Task", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 15, "name": "Deep Dhindsa", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 17, "name": "Dhindsa", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 18, "name": "Singh", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }] },
                           { "id": 7, "sectionName": "Low Priority", "projectId": 2, "todos": [{ "todoId": 23, "name": "Deep", "sectionId": 7, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 13, "name": "FourthTask", "sectionId": 7, "profilePhoto": "Uploads/Default/profile.png" }] },
                       { "id": 8, "sectionName": "High Cost", "projectId": 2, "todos": [] },
                       { "id": 9, "sectionName": "Low Cost", "projectId": 2, "todos": [{ "todoId": 25, "name": "Deep", "sectionId": 9, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 10, "name": "First Task", "sectionId": 9, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 8, "name": "A Totally New Task", "sectionId": 9, "profilePhoto": "Uploads/Default/profile.png" }] }]
                       //$scope.addTodo = function () {
                       //    alert('It is working');
                       //    $scope.todos.push({ text: $scope.todoText, done: false });
                       //    $scope.todoText = '';
                       //};
                       $scope.addTodo = function () {
                           alert('It is working');
                           $scope.todos.push({ sectionName: $scope.todoText });
                           $scope.todoText = '';
                       };
                       $scope.remaining = function () {
                           var count = 0;
                           angular.forEach($scope.todos, function (todo) {
                               count += todo.done ? 0 : 1;
                           });
                           return count;
                       };

                       $scope.archive = function () {
                           var oldTodos = $scope.todos;
                           $scope.todos = [];
                           angular.forEach(oldTodos, function (todo) {
                               if (!todo.done) $scope.todos.push(todo);
                           });
                       };


                       $scope.remaining = function () {
                           var count = 0;
                           angular.forEach($scope.todos, function (todo) {
                               count += todo.done ? 0 : 1;
                           });
                           return count;
                       };

                       $scope.sortableConfig = { group: 'todo', animation: 150 };
                       'Start End Add Update Remove Sort'.split(' ').forEach(function (name) {
                           $scope.sortableConfig['on' + name] = console.log.bind(console, name);
                       });



                       /* Update section name on blur in Kanban View */
                       $scope.updateSectionNameOnBlur = function (id, sectionName, item) {

                           var newSectionText = $('#' + id).text();
                           if (newSectionText != sectionName) {
                               item.SectionName = newSectionText;
                               $scope.updateSectionModel = {};
                               $scope.updateSectionModel.Id = id;
                               $scope.updateSectionModel.SectionName = newSectionText;

                               sectionsFactory.updateSection($scope.updateSectionModel)
                                   .success(function (data) {
                                       if (data.success) {
                                           toaster.success("Section Updated Successfully", "Success");
                                       }
                                       else {
                                           toaster.error("Some Error Occured", "Error");
                                           item.SectionName = SectionName;
                                       }

                                   });
                           }
                       };


                       /* Show Task Add Section on AddNewTask Click In KanBan View */
                       $scope.ShowTaskAddSection = function (column) {
                           column.ShowTaskAddSectionStatus = true;
                           var id = "input" + column.id;
                           $('#' + id).focus();
                       };


                       /* Hide input on blur in Kanban View */
                       $scope.HideAddNewTodoInputKanban = function (column) {

                           column.ShowTaskAddSectionStatus = false;
                           var id = "input" + column.id;
                           $('#' + id).val("");
                       };

                       /*  It includes KanBan Operations like Dragging and Sorting */
                       $scope.kanbanSortOptionsForSection = {

                           //restrict move across columns. move only within column.
                           /*accept: function (sourceItemHandleScope, destSortableScope) {
                            return sourceItemHandleScope.itemScope.sortableScope.$id !== destSortableScope.$id;
                            },*/

                           dragMove: function (itemPosition, containment, eventObj) {

                               if (eventObj) {
                                   var targetY,
                                   contatiner = document.getElementById("board").offsetLeft,
                                   targetY = eventObj.pageX - (window.pageXOffset);
                                   document.getElementById("board").offsetLeft = eventObj.offsetX;

                                   if (targetY < contatiner.top) {
                                       contatiner.scrollLeft = contatiner.scrollLeft - 20;
                                   } else if (targetY > contatiner.bottom) {
                                       contatiner.scrollLeft = contatiner.scrollLeft + 20;
                                   }
                               }

                           },

                           itemMoved: function (event) {

                               event.source.itemScope.modelValue.SectionId = event.dest.sortableScope.$parent.column.id;
                               sectionsFactory.updateListOrderOrSectionIdInSection(event.dest.sortableScope.modelValue);
                           },

                           orderChanged: function (event) {
                               alert('Working');

                               sectionsFactory.updateListOrderOrSectionIdInSection(event.dest.sortableScope.modelValue);
                           },

                           containment: '#kanbanPanel',

                           removeTodo: function (column, todo) {

                               if (confirm("Are you sure")) {
                                   column.todos.splice(column.todos.indexOf(todo), 1);
                                   todosFactory.deleteTodo(todo.TodoId);
                               }
                           }
                       };

                       /*  It includes KanBan Operations like Dragging and Sorting */
                       $scope.kanbanSortOptions = {

                           //restrict move across columns. move only within column.
                           /*accept: function (sourceItemHandleScope, destSortableScope) {
                            return sourceItemHandleScope.itemScope.sortableScope.$id !== destSortableScope.$id;
                            },*/

                           dragMove: function (itemPosition, containment, eventObj) {

                               if (eventObj) {

                                   var targetY,
                                   contatiner = document.getElementById("board"),
                                   targetY = eventObj.pageY - ($window.pageYOffset);
                                   if (targetY < contatiner.top) {
                                       contatiner.scrollTop = contatiner.scrollTop - Configuration.ScrollSpeed;
                                   } else if (targetY > contatiner.bottom) {
                                       contatiner.scrollTop = contatiner.scrollTop + Configuration.ScrollSpeed;
                                   }
                               }

                           },

                           itemMoved: function (event) {

                               event.source.itemScope.modelValue.SectionId = event.dest.sortableScope.$parent.column.id;
                               sectionsFactory.updateListOrderOrSectionIdInSection(event.dest.sortableScope.modelValue);
                           },

                           orderChanged: function (event) {

                               sectionsFactory.updateListOrderOrSectionIdInSection(event.dest.sortableScope.modelValue);
                           },

                           containment: '#kanbanPanel',

                           removeTodo: function (column, todo) {

                               if (confirm("Are you sure")) {
                                   column.todos.splice(column.todos.indexOf(todo), 1);
                                   todosFactory.deleteTodo(todo.TodoId);
                               }
                           }
                       };

                       /* Add New Task From Kanban View */
                       $scope.AddNewTaskFromKanbanView = function (column) {
                           var id = "input" + column.id;
                           var TodoName = $('#' + id).val();
                           $scope.newTask = {};
                           $scope.newTask.Name = TodoName;
                           $scope.newTask.SectionId = column.id;
                           $scope.newTask.ListOrder = column.todos.length;
                           todosFactory.insertTodo($scope.newTask).success(function (data) {
                               if (data.success) {
                                   $scope.newTask.profilePhoto = "Uploads/Default/profile.png";
                                   $scope.newTask.TodoId = data.message;
                                   column.ShowTaskAddSectionStatus = false;

                                   column.todos.push($scope.newTask);
                                   $scope.newTask.focusInput = true;
                                   $('#' + id).val("");
                               }
                           });

                       }

                       $scope.addNewCard = function (column) {

                           $scope.newTask = {};
                           $scope.newTask.TodoId = 4;
                           $scope.newTask.Name = "Hardeepinder";
                           $scope.newTask.profilePhoto = "Uploads/Profile/Thumbnail/41a7c97b.jpg";
                           $scope.SectionId = 8;
                           column.push($scope.newTask);
                       }

                       /*///////////////End Of KanBan settings////////////////*/



                       //////////////////////////Start of Sction code////////////////
                       /* SectionManagement is a CLASS which has all the FUNCTIONS related to Section i.e Section Creation, Deletion etc*/

                       /* Showing Section Management Modal Pop Up */
                       $scope.showSectionManagementModal = function () {
                           $('#modalSectionManagement').modal('show');
                       };

                       /* SHow Section Add Update Modal Pop Up */
                       $scope.showAddUpdateSectionModal = function () {
                           $scope.showUpdateSectionButton = false;
                           $scope.sectionAddUpdateModel = {};
                           $('#modalSectionManagement').modal('hide');
                           $('#modalAddUpdateSection').modal('show');
                       };

                       /* Adding New Section */
                       $scope.addNewSection = function () {
                           $('#globalLoader').show();
                           $scope.sectionAddUpdateModel = {};
                           $scope.sectionAddUpdateModel.ProjectId = $scope.projectId;
                           var model = { "SectionAddModel": $scope.sectionAddUpdateModel, "UserId": sharedService.getUserId() };
                           sectionsFactory.insertSection(model)
                               .success(function (data) {
                                   if (data.message != 0) {

                                       if (data.success) {
                                           /* data.message is the unique id of newly created section */
                                           $scope.sectionAddUpdateModel.Id = data.message;
                                           $scope.sectionAddUpdateModel.Todos = [];
                                           $scope.sectionAddUpdateModel.SectionCreator = $scope.MyProfileObject.profilePhoto;
                                           $scope.sectionAddUpdateModel.Text = "New Section";
                                           $scope.sectionAddUpdateModel.SectionName = "New Section";
                                           $scope.sectionAddUpdateModel.Value = data.message;
                                           $scope.Sections.push($scope.sectionAddUpdateModel);
                                           $timeout(function () {
                                               $scope.$apply();
                                           });
                                           $scope.editSection(data.message);
                                           //toaster.success("Success", message["SectionAddedSuccess"]);
                                           $('#globalLoader').hide();
                                       }
                                       else {
                                           if (data.message == null) {
                                               toaster.error("Error", data.errors.toString());
                                               $('#globalLoader').hide();
                                           }
                                           else {
                                               toaster.error("Error", data.message)
                                               $('#globalLoader').hide();
                                           }
                                       }
                                   }
                                   else {
                                       toaster.warning("Warning", message["DuplicateSection"])
                                       $('#globalLoader').hide();
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !")
                                   $('#globalLoader').hide();
                               });
                       };

                       /* Edit Section */
                       $scope.editSection = function (sectionId) {

                           $scope.currentSectionIdForRow = sectionId;
                           $timeout(function () {
                               $("#" + sectionId + "_section").focus();
                           }, 100);
                       }

                       /* Keypress Section*/
                       $scope.keypressSection = function (event, sectionId, SectionName, node) {

                           if (event.keyCode == 13 || event.CharCode == 13) {
                               event.preventDefault();
                               $scope.updateSection(sectionId, SectionName, node);
                           }
                       }

                       /* Update Section*/
                       $scope.updateSection = function (sectionId, SectionName, node) {

                           //$('#globalLoader').show();
                           //SectionName = $('#' + sectionId + '_section').html();
                           //node.SectionName = SectionName;
                           $scope.currentSectionIdForRow = 0;
                           $scope.sectionAddUpdateModel = {};
                           $scope.sectionAddUpdateModel.ProjectId = $scope.projectId;
                           $scope.sectionAddUpdateModel.SectionId = sectionId;
                           $scope.sectionAddUpdateModel.SectionName = SectionName;
                           var model = { "SectionUpdateModel": $scope.sectionAddUpdateModel, "UserId": sharedService.getUserId() };
                           sectionsFactory.updateSection(model)
                               .success(function (data) {
                                   if (data.ResponseData == 'SectionUpdatedSuccess') {
                                       if (data.success) {
                                           if ($scope.todoDescriptionObject != undefined) {
                                               $scope.todoDescriptionObject.SectionName = SectionName;
                                           }
                                           angular.forEach(
                                               angular.element(node.Todos), function (todosElem) {
                                                   todosElem.SectionName = SectionName;
                                               });
                                           //$scope.Sections[$scope.sectionEditIndex].SectionName = $scope.sectionAddUpdateModel.SectionName;
                                           //$scope.Sections[$scope.sectionEditIndex].text = $scope.sectionAddUpdateModel.SectionName;
                                           //toaster.success("Success", message[data.ResponseData]);
                                           //$('#globalLoader').hide();
                                           //$('#modalAddUpdateSection').modal('hide');
                                       }
                                       else {
                                           if (data.message == null) {
                                               toaster.error("Error", data.errors.toString());
                                               //$('#globalLoader').hide();
                                           }
                                           else {
                                               toaster.error("Error", data.message);
                                               //$('#globalLoader').hide();
                                           }
                                       }
                                   }
                                   else {
                                       toaster.warning("Warning", message[data.ResponseData]);
                                       //$('#globalLoader').hide();
                                   }

                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                                   //$('#globalLoader').hide();
                               });
                       };

                       /* Delete Section */
                       $scope.deleteSection = function (sectionId, index) {

                           //$('#modalSectionManagement').modal('hide');
                           $confirm({ text: 'Are you sure you want to delete this Section?', title: 'Delete Section', ok: 'Yes', cancel: 'No' })
                            .then(function () {
                                $('#globalLoader').show();
                                sectionsFactory.deleteSection(sectionId)
                                    .success(function (data) {
                                        if (data.success) {
                                            $scope.Sections.splice(index, 1);
                                            //toaster.success("Success", data.message);
                                            $('#globalLoader').hide();
                                        }
                                        else {
                                            toaster.error("Error", data.message);
                                            $('#globalLoader').hide();
                                        }
                                    })
                                .error(function () {
                                    toaster.error("Error", "Some Error Occured !");
                                    $('#globalLoader').hide();
                                });
                            });
                       }
                       ///End Of Section Code////////////////

                       /* show hide the right container window*/
                       $scope.closeRightSections = function () {
                           $('#divToDoDescriptionSection').hide();
                           $('#divEventDescriptionSection').hide();
                           $('#divProjectOverview').hide();
                           $('#divUserFeedbackDescriptionSection').hide();
                           $('#divTeamDescriptionSection').hide();
                           $('#divProjectDescriptionSection').hide();
                       }

                       $scope.hideRightWindowFunction = function () {
                           $scope.hideRightWindow = true;
                           $scope.stretchCentreWindow = true;
                           $scope.selectedTodo = "";
                           $scope.selectedNotification = "";
                           $scope.selectedInbox = "";
                           $scope.selectedUserFeedback = "";
                       }
                       /*End Of show hide right container window*/

                       /* AssigneeManagement is a CLASS which has all the FUNCTIONS related to Assignee i.e Update Assignee, Show assignee update section etc*/
                       $scope.AssigneeManagement = {
                           /* Showing Assignee Modal For editing */
                           showAssigneeEditModal: function () {

                               $('#modalAssigneeEdit').modal('show');
                               $scope.assigneeAddUpdateModel = {};
                               $scope.assigneeAddUpdateModel.AssigneeId = $scope.todoDescriptionObject.AssigneeId;
                               $scope.assigneeAddUpdateModel.TodoId = $scope.todoDescriptionObject.TodoId;
                               $scope.assigneeAddUpdateModel.InProgress = $scope.todoDescriptionObject.InProgress;
                               $scope.previousAssigneeId = $scope.todoDescriptionObject.AssigneeId;
                           },
                           /* Update Assignee For a particular Todo */
                           updateAssignee: function (isAssigneeInvalid, assigneeEditForm) {

                               if (isAssigneeInvalid) {
                                   assigneeEditForm.assignee.$dirty = true;
                                   return;
                               }
                               if ($scope.assigneeAddUpdateModel.AssigneeId == $scope.previousAssigneeId) {
                                   toaster.warning("Warning", "Already assigned to this User !");
                               }
                               else if ($scope.assigneeAddUpdateModel.InProgress == true) {
                                   toaster.warning("Warning", "Todo is in progress !");
                               }
                               else {
                                   $('#globalLoader').show();
                                   var model = { 'AssigneeUpdateModel': $scope.assigneeAddUpdateModel, 'UserId': sharedService.getUserId() };
                                   assigneeFactory.updateTodoAssignee(model)
                                       .success(function (data) {
                                           if (data.success) {

                                               toaster.success("Success", data.message);
                                               $('#modalAssigneeEdit').modal('hide');
                                               var currentAssigneeId = $scope.assigneeAddUpdateModel.AssigneeId;
                                               var previousAssigneeId = $scope.previousAssigneeId;
                                               if (currentAssigneeId != previousAssigneeId) {
                                                   var AssigneeProfilePhoto;
                                                   var AssigneeName;
                                                   var AssigneeEmail;
                                                   var AssigneeId;
                                                   if ($scope.assigneeAddUpdateModel.AssigneeId == undefined) {
                                                       AssigneeProfilePhoto = "Uploads/Default/profile.png";
                                                       AssigneeName = "Not Assigned";
                                                       AssigneeId = null;
                                                   }
                                                   else {
                                                       var FilterProfilePhotoOfAssignee = $filter('filter')($scope.projectMembersObject, { 'MemberUserId': $scope.assigneeAddUpdateModel.AssigneeId });
                                                       AssigneeProfilePhoto = FilterProfilePhotoOfAssignee[0].MemberProfilePhoto;
                                                       AssigneeName = FilterProfilePhotoOfAssignee[0].MemberName;
                                                       AssigneeEmail = FilterProfilePhotoOfAssignee[0].MemberEmail;
                                                       AssigneeId = $scope.assigneeAddUpdateModel.AssigneeId;
                                                   }
                                                   $scope.todoDescriptionObject.AssigneeProfilePhoto = AssigneeProfilePhoto;
                                                   $scope.todoDescriptionObject.AssigneeName = AssigneeName;
                                                   $scope.todoDescriptionObject.AssigneeEmail = AssigneeEmail;
                                                   $scope.todoDescriptionObject.AssigneeId = AssigneeId;

                                                   $scope.showTodoDescriptionSection($scope.todoDescriptionObject, $scope.sectionIndex, $scope.todoIndex);
                                                   /*call the notification service */
                                                   $scope.getNotifications($scope.todoDescriptionObject.TodoId);
                                                   $('#globalLoader').hide();
                                               }
                                           }
                                           else {
                                               toaster.error("Error", data.message);
                                               $('#globalLoader').hide();
                                           }
                                       })
                                       .error(function () {
                                           toaster.error("Error", "Some Error Occured !");
                                           $('#globalLoader').hide();
                                       });
                               }
                           },

                           /* Configuration for adding assignee for the Todo */
                           assigneeConfig: {
                               create: false,
                               maxItems: 1,
                               required: true,
                               valueField: 'Value',
                               labelField: 'Text',
                               delimiter: '|'
                           }
                       };

                       /* FollowerManagement is a CLASS which has all the FUNCTIONS related to Followers i.e Follower Creation, Deletion etc*/
                       $scope.FollowerManagement = {
                           /* Showing Follower Modal For editing */
                           showFollowerEditModal: function (followersObject) {

                               $('#modalFollowerEdit').modal('show');

                               $scope.followerAddUpdateModel = {};
                               //$scope.followerAddUpdateModel.Followers = followersObject;//currentFollowers;
                               $scope.followerAddUpdateModel.TodoId = $scope.todoDescriptionObject.TodoId;
                               //$scope.newModel = projectMembersObject.
                               var oo = $scope.projectMembersObject;
                               var item = [], i = 0, folo = [];
                               for (i = 0; i < oo.length; i++) {
                                   var jsonItem = {};
                                   jsonItem.Value = oo[i].Value;
                                   jsonItem.Text = oo[i].Text;
                                   jsonItem.icon = '<img src="' + $scope.imgURL + oo[i].MemberProfilePhoto + '" />';
                                   jsonItem.ticked = false;
                                   item.push(jsonItem);
                               }
                               for (i = 0; i < followersObject.length; i++) {
                                   var arr = followersObject[i].Value;
                                   //jsonItem.Text = followersObject[i].Text;
                                   //folo.push(jsonItem);
                                   item.filter(function (obj) { if (obj.Value == arr) { obj.false = true; } });
                               }

                               $scope.newModel = item;
                               $scope.output = [];
                               // $scope.FollowersModel = folo;//currentFollowers;

                           },

                           /* Update Followers For a particular Todo */
                           updateFollower: function () {

                               $('#globalLoader').show();
                               var obj = $scope.output;
                               var currentFollowers = [];
                               var followerIdList = [];
                               currentFollowers = $filter('filter')($scope.newModel, { 'false': true });
                               for (var i = 0; i < currentFollowers.length; i++) {
                                   followerIdList.push(currentFollowers[i].Value);
                               }

                               $scope.followerAddUpdateModel.Followers = followerIdList;
                               //return false;
                               var model = { 'FollowerUpdateModel': $scope.followerAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': sharedService.getCompanyId() };
                               followersFactory.updateFollowers(model)
                                   .success(function (dataUpdate) {
                                       if (dataUpdate.success) {
                                           /* Get Updated Followers for the Todo*/
                                           followersFactory.getFollowers($scope.todoDescriptionObject.TodoId)
                                               .success(function (data) {
                                                   if (data.success) {
                                                       $scope.followersObject = data.ResponseData;
                                                       $timeout(function () {
                                                           $scope.$apply();

                                                           /*call the notification service */
                                                           $scope.getNotifications($scope.todoDescriptionObject.TodoId);

                                                           toaster.success("Success", dataUpdate.message);
                                                           $('#modalFollowerEdit').modal('hide');
                                                           $('#globalLoader').hide();
                                                       });
                                                   }
                                                   else {
                                                       toaster.error("Error", data.message);
                                                       $('#globalLoader').hide();
                                                   }
                                               })
                                               .error(function () {
                                                   toaster.error("Error", "Some Error Occured !");
                                                   $('#globalLoader').hide();
                                               });
                                       }
                                       else {
                                           toaster.error("Error", dataUpdate.message);
                                           $('#globalLoader').hide();
                                       }
                                   })
                                   .error(function () {
                                       toaster.error("Error", "Some Error Occured !");
                                       $('#globalLoader').hide();
                                   });
                           },

                           /* Configuration for adding follower for the Todo */
                           followerConfig: {
                               create: false,
                               required: true,
                               delimiter: '|',
                               valueField: 'Value',
                               labelField: 'Text',
                               plugins: ['remove_button']
                           }
                       };

                       /* AttachmentManagement is a CLASS which has all the FUNCTIONS related to Attachment i.e Add or Delete or Fetch Attachments etc*/
                       $scope.AttachmentManagement = {


                           /* Showing Attach Modal Pop Up */
                           showAttachFileModal: function () {
                               if ($scope.selectedTodoView == 'kanban') {
                                   $('#modalToDoDescriptionForKanban').modal('hide');
                               }
                               $scope.todoInfo = { ToDoId: $scope.toDoId, UserId: sharedService.getUserId() };
                               $('#modalAttachFile').modal('show');
                           },


                           /* Show all attachments for the Project */
                           showProjectAttachments: function (projectObject) {

                               $scope.selectedTodoView = "attachment";
                               $scope.stretchCentreWindow = true;
                               $scope.hideRightWindow = true;

                               /* Fetching Attachments of a particular Project */
                               attachmentsFactory.getProjectAttachments(projectObject.ProjectId)
                                   .success(function (data) {
                                       if (data.success) {
                                           $scope.projectAttachmentsObject = data.ResponseData;
                                       }
                                       else {
                                           toaster.error("Error", data.message);
                                       }
                                   })
                                   .error(function () {
                                       toaster.error("Error", "Some Error Occured !")
                                   });
                           },

                           /* Attach file from Local for example Computer */
                           attachFileFromLocal: function ($files) {

                               $('#globalLoader').show();
                               $scope.progressperc = true;
                               var j = ($files.length) - 1;
                               //$files: an array of files selected, each file has name, size, and type.
                               for (var i = 0; i < $files.length; i++) {
                                   var $file = $files[i];
                                   if (i == j) {
                                       (function (index) {

                                           $scope.upload[index] = $upload.upload({
                                               url: baseurl + "files/upload", // webapi url
                                               method: "POST",
                                               data: { UploadedAttachment: $file, ToDoId: $scope.toDoId, UserId: sharedService.getUserId() },
                                               file: $file
                                           })
                                               .progress(function (evt) {
                                                   // get upload percentage
                                                   console.log(parseInt(100.0 * evt.loaded / evt.total));
                                                   $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                                               })
                                               .success(function (data, status, headers, config) {
                                                   // file is uploaded successfully
                                                   $timeout(function () {
                                                       $scope.$apply();
                                                       /* Empty Input After Success*/
                                                       angular.forEach(
                                                        angular.element("input[type='file']"),
                                                                function (inputElem) {
                                                                    angular.element(inputElem).val(null);
                                                                });

                                                       /* Getting Attachments Corresponding To ToDo/Task*/
                                                       attachmentsFactory.getToDoAttachments($scope.todoDescriptionObject.TodoId, sharedService.getUserId())
                                                           .success(function (data) {
                                                               if (data.success) {

                                                                   $scope.attachmentsObject = data.ResponseData;
                                                                   $scope.AllAttachmentsForComment = data.ResponseData;
                                                                   $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/computer.png' });
                                                                   $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/logo-drive.png' });
                                                                   $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/drop-box.png' });
                                                                   $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': '../../assets/img/source/box-icon.png' });

                                                                   ///* Getting Todo Notification or We can Say History  */

                                                                   $scope.getNotifications($scope.todoDescriptionObject.TodoId);
                                                                   //todoNotificationsFactory.getTodoNotifications($scope.todoDescriptionObject.TodoId)
                                                                   //    .success(function (data) {
                                                                   //        if (data.success) {
                                                                   //            $scope.todoNotificationObject = data.ResponseData;
                                                                   //            $('#globalLoader').hide();
                                                                   //        }
                                                                   //        else {
                                                                   //            toaster.error("Error", data.message);
                                                                   //        }
                                                                   //    })
                                                                   //    .error(function () {
                                                                   //        toaster.error("Error", "Some Error Occured !");
                                                                   //        $('#globalLoader').hide();
                                                                   //    });
                                                               }
                                                               else {
                                                                   toaster.error("Error", data.message);
                                                                   $('#globalLoader').hide();
                                                               }
                                                           })
                                                           .error(function () {
                                                               toaster.error("Error", "Some Error Occured !");
                                                               $('#globalLoader').hide();
                                                           });
                                                   });
                                                   $timeout(function () {
                                                       $scope.$apply();
                                                       toaster.success("Computer", "Attachments uploaded Successfully");
                                                   });
                                               })
                                               .error(function (data, status, headers, config) {
                                                   // file failed to upload
                                                   console.log(data);
                                                   toaster.error("Error", "Some Error Occured !");
                                                   $('#globalLoader').hide();
                                               });
                                       })(i);
                                   }
                                   else {
                                       (function (index) {
                                           $scope.upload[index] = $upload.upload({
                                               url: baseurl + "files/upload", // webapi url
                                               method: "POST",
                                               data: { UploadedAttachment: $file, ToDoId: $scope.toDoId, UserId: sharedService.getUserId() },
                                               file: $file
                                           })
                                               .progress(function (evt) {
                                                   // get upload percentage
                                                   console.log(parseInt(100.0 * evt.loaded / evt.total));
                                                   $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);

                                               })
                                               .success(function (data, status, headers, config) {
                                                   // file is uploaded successfully
                                               })
                                           .error(function (data, status, headers, config) {
                                               // file failed to upload 
                                               console.log(data);
                                           });
                                       })(i);
                                   }
                               }
                           },

                           /*  Deleting Attachment from Todo */
                           deleteAttachment: function (attachmentId, index, todoObject) {
                               $confirm({ text: 'Are you sure you want to delete this File?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
                              .then(function () {
                                  $('#globalLoader').show();
                                  /* Deleting attachment */
                                  attachmentsFactory.deleteTodoAttachments(attachmentId)
                                      .success(function (data) {
                                          if (data.success) {
                                              $scope.attachmentsObject.splice(index, 1);
                                              $scope.getNotifications(todoObject.TodoId);
                                              $('#globalLoader').hide();
                                              toaster.success("Success", message[data.message]);
                                          }
                                          else {
                                              $('#globalLoader').hide();
                                              toaster.error("Error", data.message);
                                          }
                                      })
                                      .error(function () {
                                          $('#globalLoader').hide();
                                          toaster.error(message[data.message]);
                                      });
                              });
                           }
                       };


                       /* check which type of accordion is opened */
                       $scope.accordionClick = function (accordionType) {

                           if ($scope.openedAccordion == accordionType) {
                               $scope.openedAccordion = "default";
                           }
                           else {
                               $scope.openedAccordion = accordionType;
                           }
                       };


                       /* Save Todo Comment Function */
                       function SaveToDoCommentFunction() {

                           $scope.showassigneesearch = false;
                           $scope.showattachmentsearch = false;
                           $('#globalLoader').show();

                           var arrayNames = [];
                           var arrayEmails = [];
                           var arrayUsers = [];
                           var arrayAttachments = [];
                           var arrayOriginalFileNames = [];
                           var arrayFilePaths = [];
                           var arrayIsGif = [];
                           var CommentText;
                           var CommentHtml;
                           var newCommentText;

                           if ($scope.selectedTodoView == 'list') {
                               $('#txtComments div').each(function (index) {
                                   arrayNames[index] = $(this).attr('name');
                               });

                               $('#txtComments div').each(function (index) {
                                   arrayEmails[index] = $(this).attr('email');
                               });

                               $('#txtComments div').each(function (index) {
                                   arrayUsers[index] = $(this).attr('userId');
                               });

                               $('#txtComments span').each(function (index) {
                                   arrayAttachments[index] = $(this).attr('fileName');
                               });

                               $('#txtComments span').each(function (index) {
                                   arrayOriginalFileNames[index] = $(this).attr('OriginalName');
                               });

                               $('#txtComments span').each(function (index) {
                                   arrayFilePaths[index] = $(this).attr('Path');
                               });

                               $('#txtComments span').each(function (index) {
                                   arrayIsGif[index] = $(this).attr('IsGif');
                               });

                               CommentText = $('#txtComments').text();
                               CommentHtml = $('#txtComments').html();
                               newCommentText = $('#txtComments').text();
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               $('#txtCommentsForKanban div').each(function (index) {
                                   arrayNames[index] = $(this).attr('name');
                               });

                               $('#txtCommentsForKanban div').each(function (index) {
                                   arrayEmails[index] = $(this).attr('email');
                               });

                               $('#txtCommentsForKanban div').each(function (index) {
                                   arrayUsers[index] = $(this).attr('userId');
                               });

                               $('#txtCommentsForKanban span').each(function (index) {
                                   arrayAttachments[index] = $(this).attr('fileName');
                               });

                               $('#txtCommentsForKanban span').each(function (index) {
                                   arrayOriginalFileNames[index] = $(this).attr('OriginalName');
                               });

                               $('#txtCommentsForKanban span').each(function (index) {
                                   arrayFilePaths[index] = $(this).attr('Path');
                               });

                               $('#txtCommentsForKanban span').each(function (index) {
                                   arrayIsGif[index] = $(this).attr('IsGif');
                               });

                               CommentText = $('#txtCommentsForKanban').text();
                               CommentHtml = $('#txtCommentsForKanban').html();
                               newCommentText = $('#txtCommentsForKanban').text();
                           }



                           var CommentHtmlWithoutAnchors = CommentHtml.replace(/<\/?a[^>]*>[^>]*>/g, '');
                           // $('#txtComments').html(CommentHtmlWithoutAnchors);

                           var contentSansAnchors = CommentHtmlWithoutAnchors.replace(/<\/?div[^>]*>[^>]*>/g, '@@');
                           var finalstring = contentSansAnchors.replace(/<\/?span[^>]*>[^>]*>/g, '##');
                           //var newstring = contentSansAnchors.split('@@');
                           //  alert(userIds.length);
                           // var finalString = newstring[0] + "Aryan Deol" + newstring[1] + "Naaz Bhullar";
                           //  alert(finalString);
                           $scope.todocomment = {};
                           $scope.todocomment.Description = finalstring;
                           $scope.todocomment.Todoid = $scope.todoDescriptionObject.TodoId;
                           $scope.todocomment.UserTag = arrayUsers;
                           $scope.todocomment.FileTag = arrayAttachments;
                           $scope.todocomment.OriginalFileName = arrayOriginalFileNames;
                           $scope.todocomment.FilePath = arrayFilePaths;
                           $scope.todocomment.IsGif = arrayIsGif;
                           $('#txtComments').text('');
                           $('#txtCommentsForKanban').text('');
                           var model = { 'ToDoComment': $scope.todocomment, 'UserId': sharedService.getUserId() };
                           todosFactory.insertTodoComment(model)
                                                    .success(function (data) {
                                                        $('#txtComments').html('');
                                                        $('#txtCommentsForKanban').html('');
                                                        $scope.status = data;
                                                        /* Getting Comments Corresponding To ToDo/Task*/
                                                        getTodoCommentsInArrangedMode($scope.todoDescriptionObject.TodoId);
                                                        /*call the notification service */
                                                        $scope.getNotifications($scope.todoDescriptionObject.TodoId);
                                                        $scope.todocomment.Dscription = "";

                                                        $('#txtOthersToDoComments').html('');
                                                        $('#globalLoader').hide();
                                                        toaster.success("You Commented Successfully");
                                                    })
                                                     .error(function (error) {
                                                         $scope.status = 'unable to save todo comment : ' + error.message;
                                                     });

                           for (var i = 0; i < arrayUsers.length; i++) {
                               $scope.email = {};
                               $scope.email.EmailId = arrayEmails[i];
                               $scope.email.Username = arrayNames[i];
                               $scope.email.Todo = $scope.ToDoTaskName;
                               $scope.email.ProjectName = $scope.ProjectName;
                               $scope.email.Comment = newCommentText;
                               $scope.email.Flag = 5;
                               emailFactory.sendEmail($scope.email).success(function () { });
                           }
                       };

                       /* Save new ToDo Comment */
                       $scope.saveToDoComment = function saveToDoComment() {
                           SaveToDoCommentFunction();
                       };

                       /*Showing users in the list when anyone enter @ in comment textbox in my todo Description*/
                       $scope.ShowUsersorAttachmentsForTagging = function (e) {

                           function getCaretCharacterOffsetWithin(element) {
                               var caretOffset = 0;
                               if (typeof window.getSelection != "undefined") {
                                   var range = window.getSelection().getRangeAt(0);
                                   var preCaretRange = range.cloneRange();
                                   preCaretRange.selectNodeContents(element);
                                   preCaretRange.setEnd(range.endContainer, range.endOffset);
                                   caretOffset = preCaretRange.toString().length;
                               }
                               else if (typeof document.selection != "undefined" && document.selection.type != "Control") {
                                   var textRange = document.selection.createRange();
                                   var preCaretTextRange = document.body.createTextRange();
                                   preCaretTextRange.moveToElementText(element);
                                   preCaretTextRange.setEndPoint("EndToEnd", textRange);
                                   caretOffset = preCaretTextRange.text.length;
                               }
                               return caretOffset;
                           }
                           var el;
                           var divText;
                           if ($scope.selectedTodoView == 'list') {
                               el = document.getElementById("txtComments");
                               divText = $('#txtComments').text();
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               el = document.getElementById("txtCommentsForKanban");
                               divText = $('#txtCommentsForKanban').text();
                           }

                           var CaretPositionIndex = getCaretCharacterOffsetWithin(el);
                           $scope.CaretPositionIndex = CaretPositionIndex;

                           var characterMain = divText.substring(0, CaretPositionIndex)
                           var value = characterMain;
                           var segmentsDividedBySpace = [];
                           segmentsDividedBySpace = value.split(" ");
                           if (segmentsDividedBySpace.length == 1) {
                               $scope.IsItStartingOfComment = true;
                           }
                           else {
                               $scope.IsItStartingOfComment = false;
                           }
                           var lastWord = segmentsDividedBySpace[segmentsDividedBySpace.length - 1];
                           var firstCharacterOfLastWord = lastWord.substring(0, 1);
                           var lastCharactersOfLastWord = lastWord.substring(1, lastWord.length);
                           $scope.lastCharactersOfLastWord = lastCharactersOfLastWord;
                           if (firstCharacterOfLastWord == "@") {
                               $scope.searchassigneefortagging = lastCharactersOfLastWord;
                               $scope.searchattachmentfortagging = null;
                               $scope.showassigneesearch = true;
                               $scope.showattachmentsearch = false;
                           }
                           else if (firstCharacterOfLastWord == "#") {
                               $scope.searchassigneefortagging = null;
                               $scope.showassigneesearch = false;
                               $scope.showattachmentsearch = true;
                               var middleFourCharactersOfWord = lastCharactersOfLastWord.substring(0, 4);
                               switch (middleFourCharactersOfWord) {
                                   case "gif:":
                                       $scope.selectedCommentTab = "GifFiles";
                                       $scope.showGifImages = true;
                                       var searchForGif = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                                       $scope.searchGiffortagging = searchForGif;
                                       $scope.searchattachmentfortagging = "";
                                       $scope.searchGoogleDrivefortagging = "";
                                       $scope.searchDropBoxfortagging = "";
                                       $scope.searchBoxfortagging = "";
                                       $scope.searchLocalfortagging = "";
                                       break;
                                   case "gdf:":
                                       $scope.selectedCommentTab = "GoogleDriveFiles";
                                       $scope.searchGoogleDrivefortagging = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                                       $scope.searchattachmentfortagging = "";
                                       $scope.searchattachmentfortagging = "";
                                       $scope.searchGiffortagging = "";
                                       $scope.searchDropBoxfortagging = "";
                                       $scope.searchBoxfortagging = "";
                                       $scope.searchLocalfortagging = "";
                                       break;
                                   case "dbf:":
                                       $scope.selectedCommentTab = "DropBoxFiles";
                                       var search = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                                       $scope.searchDropBoxfortagging = search;
                                       $scope.searchattachmentfortagging = "";
                                       $scope.searchGiffortagging = "";
                                       $scope.searchGoogleDrivefortagging = "";
                                       $scope.searchBoxfortagging = "";
                                       $scope.searchLocalfortagging = "";
                                       break;
                                   case "box:":
                                       $scope.selectedCommentTab = "BoxFiles";
                                       $scope.searchBoxfortagging = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                                       $scope.searchattachmentfortagging = "";
                                       $scope.searchGiffortagging = "";
                                       $scope.searchGoogleDrivefortagging = "";
                                       $scope.searchDropBoxfortagging = "";
                                       $scope.searchLocalfortagging = "";
                                       break;
                                   case "loc:":
                                       $scope.selectedCommentTab = "LocalFiles";
                                       $scope.searchLocalfortagging = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                                       $scope.searchattachmentfortagging = "";
                                       $scope.searchGiffortagging = "";
                                       $scope.searchGoogleDrivefortagging = "";
                                       $scope.searchDropBoxfortagging = "";
                                       $scope.searchBoxfortagging = "";
                                       break;
                                   default:
                                       $scope.selectedCommentTab = "AllFiles";
                                       $scope.searchGiffortagging = "";
                                       $scope.searchGoogleDrivefortagging = "";
                                       $scope.searchDropBoxfortagging = "";
                                       $scope.searchBoxfortagging = "";
                                       $scope.searchLocalfortagging = "";
                                       $scope.showGifImages = false;
                                       $scope.searchattachmentfortagging = lastCharactersOfLastWord;
                               }
                           }
                           else {
                               $scope.showassigneesearch = false;
                               $scope.showattachmentsearch = false;
                           }
                           if (e.shiftKey == true && e.keyCode == 50) {
                               var characterUser = divText.substring(CaretPositionIndex - 2, CaretPositionIndex - 1)
                               if (characterUser == " " || characterUser == "") {
                                   $scope.showassigneesearch = true;
                                   $scope.showattachmentsearch = false;
                               }
                           }
                           else if (e.shiftKey == true && e.keyCode == 51) {
                               $scope.showassigneesearch = false;
                               $scope.showattachmentsearch = true;
                               $scope.AllAttachmentsForComment = $scope.uploads;
                               $scope.LocalAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'computer.png' });
                               $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'logo-drive.png' });
                               $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'drop-box.png' });
                               $scope.BoxAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'box-icon.png' });
                           }
                           else if (e.keyCode == 32) {
                               $scope.showassigneesearch = false;
                               $scope.showattachmentsearch = false;
                           }
                       };

                       /* Adding User In Comment in my todo*/
                       $scope.AddUserInComment = function (userId, FirstName, LastName, Emailid) {

                           var Id = guid();
                           var data = "<div class='glyphicon glyphicon-user divUserInComment' name='" + FirstName + "' email='" + Emailid + "' userId='" + userId + "'  id='" +
                                       Id + "'   contenteditable='false'  >" + FirstName + "<a id='" + Id +
                                       "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></div>";

                           var compiledElement = $compile(data)($scope);
                           if ($scope.selectedTodoView == 'list') {
                               var htmlnode = $('#txtComments').html();
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               var htmlnode = $('#txtCommentsForKanban').html();
                           }

                           if ($scope.IsItStartingOfComment == true) {

                               var stringTobeReplaced = "@" + $scope.lastCharactersOfLastWord;

                           }
                           else {
                               var stringTobeReplaced = " @" + $scope.lastCharactersOfLastWord;
                           }

                           var res = htmlnode.replace(stringTobeReplaced, " " + data);


                           if ($scope.selectedTodoView == 'list') {
                               $('#txtComments').html("");
                               $('#txtComments').append(res);

                               var newhtml = "<c>" + $('#txtComments').html() + "</c>";
                               var newcompilehtml = $compile(newhtml)($scope);

                               $('#txtComments').html("");
                               $('#txtComments').append(newcompilehtml);
                               $('#txtComments').append("&nbsp;");
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               $('#txtCommentsForKanban').html("");
                               $('#txtCommentsForKanban').append(res);

                               var newhtml = "<c>" + $('#txtCommentsForKanban').html() + "</c>";
                               var newcompilehtml = $compile(newhtml)($scope);

                               $('#txtCommentsForKanban').html("");
                               $('#txtCommentsForKanban').append(newcompilehtml);
                               $('#txtCommentsForKanban').append("&nbsp;");
                           }

                           $scope.showassigneesearch = false;
                           $scope.showattachmentsearch = false;
                           $scope.searchassigneefortagging = "";
                           $scope.searchattachmentfortagging = "";

                           if ($scope.selectedTodoView == 'list') {
                               placeCursorAtEnd(document.getElementById("txtComments"));
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               placeCursorAtEnd(document.getElementById("txtCommentsForKanban"));
                           }


                           $timeout(function () {
                               $scope.$apply();

                           });
                       };


                       /* Adding Attachment In Comment in my todo*/
                       $scope.AddFileInComment = function (Name, OriginalName, Path) {

                           var Id = guid();
                           var data = "<span  class='glyphicon glyphicon-file spanFileInComment' IsGif='false' OriginalName='" + OriginalName + "' Path='" + Path + "' fileName='" + Name + "'  id='" + Id + "'   contenteditable='false'  >" + OriginalName + "<a id='" + Id + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></span>";
                           var compiledElement = $compile(data)($scope);

                           if ($scope.selectedTodoView == 'list') {
                               var htmlnode = $('#txtComments').html();
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               var htmlnode = $('#txtCommentsForKanban').html();
                           }


                           if ($scope.IsItStartingOfComment == true) {

                               var stringTobeReplaced = "#" + $scope.lastCharactersOfLastWord;

                           }
                           else {
                               var stringTobeReplaced = " #" + $scope.lastCharactersOfLastWord;
                           }

                           var res = htmlnode.replace(stringTobeReplaced, " " + data);

                           if ($scope.selectedTodoView == 'list') {
                               $('#txtComments').html("");
                               $('#txtComments').append(res);
                               var newhtml = "<c>" + $('#txtComments').html() + "</c>";
                               var newcompilehtml = $compile(newhtml)($scope);

                               $('#txtComments').html("");
                               $('#txtComments').append(newcompilehtml);
                               $('#txtComments').append("&nbsp;");
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               $('#txtCommentsForKanban').html("");
                               $('#txtCommentsForKanban').append(res);
                               var newhtml = "<c>" + $('#txtCommentsForKanban').html() + "</c>";
                               var newcompilehtml = $compile(newhtml)($scope);

                               $('#txtCommentsForKanban').html("");
                               $('#txtCommentsForKanban').append(newcompilehtml);
                               $('#txtCommentsForKanban').append("&nbsp;");
                           }


                           $scope.showassigneesearch = false;
                           $scope.showattachmentsearch = false;
                           $scope.searchassigneefortagging = "";
                           $scope.searchattachmentfortagging = "";

                           if ($scope.selectedTodoView == 'list') {
                               placeCursorAtEnd(document.getElementById("txtComments"));
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               placeCursorAtEnd(document.getElementById("txtCommentsForKanban"));
                           }

                           $timeout(function () {
                               $scope.$apply();
                           });

                       };

                       /* Adding Attachment In Comment in my todo*/
                       $scope.AddGifFileInComment = function (Name, Path) {

                           var Id = guid();
                           var data = "<span  class='glyphicon glyphicon-file spanFileInComment' IsGif='true' OriginalName='" + Name + "' Path='" + Path
                               + "' fileName='" + Name + "'  id='" + Id + "'   contenteditable='false'  >" + "gif:" + Name + "<a id='" + Id
                               + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></span>";

                           if ($scope.selectedTodoView == 'list') {
                               var htmlnode = $('#txtComments').html();
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               var htmlnode = $('#txtCommentsForKanban').html();
                           }

                           if ($scope.IsItStartingOfComment == true) {

                               var stringTobeReplaced = "#" + $scope.lastCharactersOfLastWord;

                           }
                           else {
                               var stringTobeReplaced = " #" + $scope.lastCharactersOfLastWord;
                           }

                           var res = htmlnode.replace(stringTobeReplaced, " " + data);

                           if ($scope.selectedTodoView == 'list') {
                               $('#txtComments').html("");
                               $('#txtComments').append(res);
                               var newhtml = "<c>" + $('#txtComments').html() + "</c>";
                               var newcompilehtml = $compile(newhtml)($scope);

                               $('#txtComments').html("");
                               $('#txtComments').append(newcompilehtml);
                               $('#txtComments').append("&nbsp;");
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               $('#txtCommentsForKanban').html("");
                               $('#txtCommentsForKanban').append(res);
                               var newhtml = "<c>" + $('#txtCommentsForKanban').html() + "</c>";
                               var newcompilehtml = $compile(newhtml)($scope);

                               $('#txtCommentsForKanban').html("");
                               $('#txtCommentsForKanban').append(newcompilehtml);
                               $('#txtCommentsForKanban').append("&nbsp;");
                           }

                           $scope.showassigneesearch = false;
                           $scope.showattachmentsearch = false;
                           $scope.searchassigneefortagging = "";
                           $scope.searchattachmentfortagging = "";

                           if ($scope.selectedTodoView == 'list') {
                               placeCursorAtEnd(document.getElementById("txtComments"));
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               placeCursorAtEnd(document.getElementById("txtCommentsForKanban"));
                           }

                           $timeout(function () {
                               $scope.$apply();
                           });
                       };


                       /* Code For Placing Cursor at the end of div*/
                       function placeCursorAtEnd(el) {
                           el.focus();
                           if (typeof window.getSelection != "undefined"
                                   && typeof document.createRange != "undefined") {
                               var range = document.createRange();
                               range.selectNodeContents(el);
                               range.collapse(false);
                               var sel = window.getSelection();
                               sel.removeAllRanges();
                               sel.addRange(range);
                           } else if (typeof document.body.createTextRange != "undefined") {
                               var textRange = document.body.createTextRange();
                               textRange.moveToElementText(el);
                               textRange.collapse(false);
                               textRange.select();
                           }
                       }


                       /* Removing tagged items from comment*/
                       $scope.DeleteTaggedItem = function (event) {
                           $('#' + $(event.target).attr('id')).remove();
                       };


                       /* Allowing tab for selecting the searched user */
                       $scope.TabKeyPressEvent = function (e) {

                           var TABKEY = 9;
                           var ENTERKEY = 13;
                           if (e.keyCode == TABKEY) {
                               if ($scope.showassigneesearch == true) {

                                   var x = $filter('filter')($scope.projectMembersObject, $scope.searchassigneefortagging);
                                   if (x != null) {
                                       if (x.length > 0) {

                                           $scope.people = x;
                                           var Id = guid();
                                           var data = "<div class='glyphicon glyphicon-user divUserInComment' name='" + $scope.people[0].MemberName + "' email='" + $scope.people[0].MemberEmail + "' userId='" + $scope.people[0].MemberUserId + "'  id='" + Id + "'   contenteditable='false'  >" + $scope.people[0].MemberName + "<a id='" + Id + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></div>";
                                           var compiledElement = $compile(data)($scope);
                                           if ($scope.selectedTodoView == 'list') {
                                               var htmlnode = $('#txtComments').html();
                                           }
                                           else if ($scope.selectedTodoView == 'kanban') {
                                               var htmlnode = $('#txtCommentsForKanban').html();
                                           }

                                           if ($scope.IsItStartingOfComment == true) {
                                               var stringTobeReplaced = "@" + $scope.lastCharactersOfLastWord;
                                           }
                                           else {
                                               var stringTobeReplaced = " @" + $scope.lastCharactersOfLastWord;
                                           }
                                           var res = htmlnode.replace(stringTobeReplaced, " " + data);

                                           if ($scope.selectedTodoView == 'list') {
                                               $('#txtComments').html("");
                                               $('#txtComments').append(res);
                                               var newhtml = "<c>" + $('#txtComments').html() + "</c>";
                                               var newcompilehtml = $compile(newhtml)($scope);
                                               $('#txtComments').html("");
                                               $('#txtComments').append(newcompilehtml);
                                               $('#txtComments').append("&nbsp;");
                                           }
                                           else if ($scope.selectedTodoView == 'kanban') {
                                               $('#txtCommentsForKanban').html("");
                                               $('#txtCommentsForKanban').append(res);
                                               var newhtml = "<c>" + $('#txtCommentsForKanban').html() + "</c>";
                                               var newcompilehtml = $compile(newhtml)($scope);
                                               $('#txtCommentsForKanban').html("");
                                               $('#txtCommentsForKanban').append(newcompilehtml);
                                               $('#txtCommentsForKanban').append("&nbsp;");
                                           }

                                           $scope.showassigneesearch = false;
                                           $scope.showattachmentsearch = false;
                                           $scope.searchassigneefortagging = null;
                                           $scope.searchattachmentfortagging = null;

                                           if ($scope.selectedTodoView == 'list') {
                                               placeCursorAtEnd(document.getElementById("txtComments"));
                                           }
                                           else if ($scope.selectedTodoView == 'kanban') {
                                               placeCursorAtEnd(document.getElementById("txtCommentsForKanban"));
                                           }

                                           $timeout(function () {
                                               $scope.$apply();
                                           });
                                       }
                                   }
                               }
                               else if ($scope.showattachmentsearch == true) {
                                   var ActiveTab = $scope.selectedCommentTab;
                                   switch (ActiveTab) {
                                       case "GifFiles":
                                           var GifImageData = $filter('filter')($scope.GiphyImages, { 'name': $scope.searchGiffortagging });
                                           AddGifFileInCommentOnTabClick(GifImageData);
                                           break;
                                       case "GoogleDriveFiles":
                                           var GoogleDriveFilesData = $filter('filter')($scope.GoogleDriveAttachmentsForComment, { 'originalName': $scope.searchGoogleDrivefortagging });
                                           AddFileInCommentOnTabClick(GoogleDriveFilesData);
                                           break;
                                       case "DropBoxFiles":
                                           var DropBoxFilesData = $filter('filter')($scope.DropBoxAttachmentsForComment, { 'originalName': $scope.searchDropBoxfortagging });
                                           AddFileInCommentOnTabClick(DropBoxFilesData);
                                           break;
                                       case "BoxFiles":
                                           var BoxFilesData = $filter('filter')($scope.BoxAttachmentsForComment, { 'originalName': $scope.searchBoxfortagging });
                                           AddFileInCommentOnTabClick(BoxFilesData);
                                           break;
                                       case "LocalFiles":
                                           var LocalFilesData = $filter('filter')($scope.LocalAttachmentsForComment, { 'originalName': $scope.searchLocalfortagging });
                                           AddFileInCommentOnTabClick(LocalFilesData);
                                           break;
                                       case "AllFiles":
                                           var AllFilesData = $filter('filter')($scope.AllAttachmentsForComment, { 'originalName': $scope.searchattachmentfortagging });
                                           AddFileInCommentOnTabClick(AllFilesData);
                                           break;
                                   }
                               }
                               if (e.preventDefault) {
                                   e.preventDefault();
                               }
                               return false;
                           }
                           else if (e.shiftKey == false && e.keyCode == ENTERKEY) {
                               SaveToDoCommentFunction();
                           }
                           else if (e.shiftKey == true && e.keyCode == ENTERKEY) {
                               if ($scope.selectedTodoView == 'list') {
                                   $('#txtComments').append("\r");
                               }
                               else if ($scope.selectedTodoView == 'kanban') {
                                   $('#txtCommentsForKanban').append("\r");
                               }

                           }
                       };

                       $scope.hideCommentBox = function myfunction() {

                           $scope.showassigneesearch = false;                // To hide assignee search section
                           $scope.showattachmentsearch = false;             // To hide attachment search section

                           if ($scope.selectedTodoView == 'list') {
                               var commentText = $('#txtComments').html();
                               var newCommentText = commentText.substring(0, commentText.length - 1);
                               $('#txtComments').html(newCommentText);
                               placeCursorAtEnd(document.getElementById("txtComments"));
                           }
                           else if ($scope.selectedTodoView == 'kanban') {
                               var commentText = $('#txtCommentsForKanban').html();
                               var newCommentText = commentText.substring(0, commentText.length - 1);
                               $('#txtCommentsForKanban').html(newCommentText);
                               placeCursorAtEnd(document.getElementById("txtCommentsForKanban"));
                           }


                       }

                       $('#nput-tags').selectize({
                           delimiter: ',',
                           persist: false
                       });

                       $scope.TodoUpdate = function (event, todoObj, SectionId) {

                           $scope.todoAddUpdateModel = todoObj;
                           if (event.relatedTarget != null) {
                               $timeout(function () {
                                   $scope.editDescriptionObjectTodoName();
                               }, 100);
                           }
                           $scope.updateTodo(SectionId);
                       }


                       $scope.TodoKeypress = function (event, todoObj, SectionId, nodeIndex, $index) {

                           $scope.todoDescriptionObject.Name = todoObj.Name;

                           if (event.keyCode == 13 || event.CharCode == 13) {
                               event.preventDefault();

                               $scope.addNewTodo(SectionId, todoObj.TodoListOrder + 1, 'middle', nodeIndex, $index);
                               // To Update todo
                               $scope.todoAddUpdateModel = todoObj;
                               $scope.updateTodo(SectionId);
                           }

                       }



                       // Update todo description 
                       $scope.saveTodoDescription = function (TodoId, Description) {

                           $scope.toggleDescriptionObjectTodoDescription = !$scope.toggleDescriptionObjectTodoDescription;
                           var model = { 'TodoId': TodoId, 'Description': Description };
                           todosFactory.saveTodoDescription(model)
                               .success(function (data) {
                                   // code after success goes here
                                   $scope.Sections[$scope.sectionIndex].Todos[$scope.todoIndex].ModifiedDate = new Date();
                               })
                               .error(function (data) {
                                   toaster.error("Error", "Some Error Occured !")
                               })
                       }

                       /* Edit Description Object Todo-Name */
                       $scope.editDescriptionObjectTodoDescription = function () {

                           $scope.toggleDescriptionObjectTodoDescription = !$scope.toggleDescriptionObjectTodoDescription;
                           $timeout(function () {
                               if ($scope.selectedTodoView == 'list') {
                                   $("#TodoDescription").focus();
                               }
                               else if ($scope.selectedTodoView == 'kanban') {
                                   $("#TodoDescriptionForKanban").focus();
                               }

                           }, 100);
                       }

                       /* Edit Description Object Todo-Name */
                       $scope.editDescriptionObjectTodoName = function () {

                           $scope.toggleDescriptionObjectTodoName = !$scope.toggleDescriptionObjectTodoName;
                           $timeout(function () {
                               if ($scope.selectedTodoView == 'list') {
                                   $("#TodoName").focus();
                               }
                               else if ($scope.selectedTodoView == 'kanban') {
                                   $("#TodoNameForKanban").focus();
                               }
                           }, 100);
                       }


                       // name Change In Todo Description Panel
                       $scope.nameChangeInTodoDescriptionObject = function () {
                           $scope.Sections[$scope.sectionIndex].Todos[$scope.todoIndex].Name = $scope.todoDescriptionObject.Name;
                       }

                       // Update todo Name from Todo-Description Panel 
                       $scope.updateTodoName = function ($event, TodoId, Name) {

                           $scope.toggleDescriptionObjectTodoName = !$scope.toggleDescriptionObjectTodoName;
                           if ($event.relatedTarget != null) {
                               $scope.editDescriptionObjectTodoDescription();
                           }
                           var model = {
                               'TodoId': TodoId, 'Name': Name
                           }
                           todosFactory.updateTodoName(model)
                               .success(function (data) {
                                   // code after success goes here
                                   $scope.Sections[$scope.sectionIndex].Todos[$scope.todoIndex].ModifiedDate = new Date();
                               })
                               .error(function (data) {
                                   toaster.error("Error", "Some Error Occured !")
                               })
                       }

                       ///Show Assignee Modal to update
                       $scope.showAssigneeEditModal = function () {

                           $scope.assigneeAddUpdateModel = {};
                           $scope.assigneeAddUpdateModel.AssigneeId = $scope.todoDescriptionObject.AssigneeId;
                           $scope.assigneeAddUpdateModel.TodoId = $scope.todoDescriptionObject.TodoId;
                           $scope.assigneeAddUpdateModel.InProgress = $scope.todoDescriptionObject.InProgress;
                           $scope.previousAssigneeId = $scope.todoDescriptionObject.AssigneeId;
                       }

                       //Update Assignee
                       $scope.updateAssignee = function () {

                           if ($scope.assigneeAddUpdateModel.AssigneeId == $scope.previousAssigneeId) {
                               toaster.warning("Warning", "Already assigned to this User !");
                           }
                           else if ($scope.assigneeAddUpdateModel.InProgress == true) {
                               toaster.warning("Warning", "Todo is in progress !");
                           }
                           else {
                               //$('#globalLoader').show();
                               var model = { 'AssigneeUpdateModel': $scope.assigneeAddUpdateModel, 'UserId': sharedService.getUserId() };
                               assigneeFactory.updateTodoAssignee(model)
                                   .success(function (data) {
                                       if (data.success) {

                                           //toaster.success("Success", data.message);
                                           //$('#modalAssigneeEdit').modal('hide');
                                           var currentAssigneeId = $scope.assigneeAddUpdateModel.AssigneeId;
                                           var previousAssigneeId = $scope.previousAssigneeId;
                                           if (currentAssigneeId != previousAssigneeId) {
                                               var AssigneeProfilePhoto;
                                               var AssigneeName;
                                               var AssigneeEmail;
                                               var AssigneeId;
                                               if ($scope.assigneeAddUpdateModel.AssigneeId == undefined || $scope.assigneeAddUpdateModel.AssigneeId == "") {
                                                   AssigneeProfilePhoto = "Uploads/Default/profile.png";
                                                   AssigneeName = "Not Assigned";
                                                   AssigneeId = null;
                                               }
                                               else {
                                                   var FilterProfilePhotoOfAssignee = $filter('filter')($scope.projectMembersObject, { 'MemberUserId': $scope.assigneeAddUpdateModel.AssigneeId });
                                                   AssigneeProfilePhoto = FilterProfilePhotoOfAssignee[0].MemberProfilePhoto;
                                                   AssigneeName = FilterProfilePhotoOfAssignee[0].MemberName;
                                                   AssigneeEmail = FilterProfilePhotoOfAssignee[0].MemberEmail;
                                                   AssigneeId = $scope.assigneeAddUpdateModel.AssigneeId;
                                               }
                                               $scope.todoDescriptionObject.AssigneeProfilePhoto = AssigneeProfilePhoto;
                                               $scope.todoDescriptionObject.AssigneeName = AssigneeName;
                                               $scope.todoDescriptionObject.AssigneeEmail = AssigneeEmail;
                                               $scope.todoDescriptionObject.AssigneeId = AssigneeId;

                                               $scope.showTodoDescriptionSection($scope.todoDescriptionObject, $scope.sectionIndex, $scope.todoIndex);
                                               /*call the notification service */
                                               $scope.getNotifications($scope.todoDescriptionObject.TodoId);
                                               $scope.Sections[$scope.sectionIndex].Todos[$scope.todoIndex].ModifiedDate = new Date();
                                               //$('#globalLoader').hide();
                                           }
                                       }
                                       else {
                                           toaster.error("Error", data.message);
                                           //$('#globalLoader').hide();
                                       }
                                   })
                                   .error(function () {
                                       toaster.error("Error", "Some Error Occured !");
                                       //$('#globalLoader').hide();
                                   });
                           }
                       }


                       ///Show Reporter Modal to update
                       $scope.showReporterEditModal = function () {

                           $scope.reporterAddUpdateModel = {};
                           $scope.reporterAddUpdateModel.ReporterId = $scope.todoDescriptionObject.ReporterId;
                           $scope.reporterAddUpdateModel.TodoId = $scope.todoDescriptionObject.TodoId;
                           $scope.reporterAddUpdateModel.InProgress = $scope.todoDescriptionObject.InProgress;
                           $scope.previousReporterId = $scope.todoDescriptionObject.ReporterId;
                       }


                       //Update Reporter
                       $scope.updateReporter = function () {

                           if ($scope.reporterAddUpdateModel.ReporterId == $scope.previousReporterId) {
                               toaster.warning("Warning", "Already assigned to this User !");
                           }
                           else if ($scope.reporterAddUpdateModel.InProgress == true) {
                               toaster.warning("Warning", "Todo is in progress !");
                           }
                           else {
                               //$('#globalLoader').show();
                               var model = { 'ReporterUpdateModel': $scope.reporterAddUpdateModel, 'UserId': sharedService.getUserId() };
                               todosFactory.updateTodoReporter(model)
                                   .success(function (data) {
                                       if (data.success) {

                                           //toaster.success("Success", data.message);
                                           //$('#modalAssigneeEdit').modal('hide');
                                           var currentReporterId = $scope.reporterAddUpdateModel.ReporterId;
                                           var previousReporterId = $scope.previousReporterId;
                                           if (currentReporterId != previousReporterId) {
                                               var ReporterProfilePhoto;
                                               var ReporterName;
                                               var ReporterEmail;
                                               var ReporterId;
                                               if ($scope.reporterAddUpdateModel.ReporterId == undefined || $scope.reporterAddUpdateModel.ReporterId == "") {
                                                   ReporterProfilePhoto = "Uploads/Default/profile.png";
                                                   ReporterName = "Not Assigned";
                                                   ReporterId = null;
                                               }
                                               else {
                                                   var FilterProfilePhotoOfReporter = $filter('filter')($scope.projectMembersObject, { 'MemberUserId': $scope.reporterAddUpdateModel.ReporterId });
                                                   ReporterProfilePhoto = FilterProfilePhotoOfReporter[0].MemberProfilePhoto;
                                                   ReporterName = FilterProfilePhotoOfReporter[0].MemberName;
                                                   ReporterEmail = FilterProfilePhotoOfReporter[0].MemberEmail;
                                                   ReporterId = $scope.reporterAddUpdateModel.ReporterId;
                                               }
                                               $scope.todoDescriptionObject.ReporterProfilePhoto = ReporterProfilePhoto;
                                               $scope.todoDescriptionObject.ReporterName = ReporterName;
                                               $scope.todoDescriptionObject.ReporterEmail = ReporterEmail;
                                               $scope.todoDescriptionObject.ReporterId = ReporterId;

                                               $scope.showTodoDescriptionSection($scope.todoDescriptionObject, $scope.sectionIndex, $scope.todoIndex);
                                               /*call the notification service */
                                               $scope.getNotifications($scope.todoDescriptionObject.TodoId);
                                               $scope.Sections[$scope.sectionIndex].Todos[$scope.todoIndex].ModifiedDate = new Date();
                                               //$('#globalLoader').hide();
                                           }
                                       }
                                       else {
                                           toaster.error("Error", data.message);
                                           //$('#globalLoader').hide();
                                       }
                                   })
                                   .error(function () {
                                       toaster.error("Error", "Some Error Occured !");
                                       //$('#globalLoader').hide();
                                   });
                           }
                       }


                       // Update todo Due Date from Todo-Description Panel 
                       $scope.updateTodoDueDate = function (TodoId, Name) {
                           var model = {
                               'TodoId': TodoId, 'Name': Name
                           }
                           todosFactory.updateTodoName(model)
                               .success(function (data) {
                                   // code after success goes here
                               })
                               .error(function (data) {
                                   toaster.error("Error", "Some Error Occured !")
                               })
                       }


                       $scope.deadlineDateChanged = function (TodoId, DeadlineDate) {
                           var model = {
                               'TodoId': TodoId, 'DeadlineDate': DeadlineDate
                           }
                           todosFactory.updateTodoDeadlineDate(model)
                               .success(function (data) {
                                   // code after success goes here
                                   $scope.Sections[$scope.sectionIndex].Todos[$scope.todoIndex].ModifiedDate = new Date();
                               })
                               .error(function (data) {
                                   toaster.error("Error", "Some Error Occured !")
                               })
                       }

                       $scope.editEstimatedTime = function () {
                           $timeout(function () {
                               $('input[name="EstimatedTime"]').focus();
                           }, 100);
                       }

                       $scope.saveEstimatedTime = function (TodoId, EstimatedTime) {
                           //$scope.toggleEstimatedTime = !$scope.toggleEstimatedTime;

                           var model = {
                               'TodoId': TodoId, 'EstimatedTime': EstimatedTime
                           }
                           todosFactory.updateTodoEstimatedTime(model)
                               .success(function (data) {
                                   // code after success goes here
                                   $scope.Sections[$scope.sectionIndex].Todos[$scope.todoIndex].ModifiedDate = new Date();
                               })
                               .error(function (data) {
                                   toaster.error("Error", "Some Error Occured !")
                               })
                       }

                       $scope.editTaskType = function () {
                           //$('#globalLoader').show();
                           todosFactory.getTaskTypeForCreatingTodo(0)
                               .success(function (data) {

                                   $scope.Tags = {};
                                   $scope.Tags.TodoTaskType = data.ResponseData.TodoTaskType;
                                   $scope.Tags.AllTaskTypes = data.ResponseData.AllTaskTypes;
                                   //$('#globalLoader').hide();
                                   $timeout(function () {
                                       $('input[name="TaskType"]').focus();
                                   }, 100);
                               })
                               .error(function (data) {
                                   toaster.error("Error", data.message);
                                   //$('#globalLoader').hide();
                               })
                       }

                       $scope.saveTaskType = function (TodoId, TaskTypeId) {
                           var model = {
                               'TodoId': TodoId, 'TaskTypeId': TaskTypeId
                           }
                           todosFactory.updateTodoTaskType(model)
                               .success(function (data) {
                                   // code after success goes here
                                   $scope.Sections[$scope.sectionIndex].Todos[$scope.todoIndex].ModifiedDate = new Date();
                               })
                               .error(function (data) {
                                   toaster.error("Error", "Some Error Occured !")
                               })
                       }


                       $scope.showFollowerEditModal = function (followersObject) {


                           $scope.followerAddUpdateModel = {};
                           //$scope.followerAddUpdateModel.Followers = followersObject;//currentFollowers;
                           $scope.followerAddUpdateModel.TodoId = $scope.todoDescriptionObject.TodoId;
                           //$scope.newModel = projectMembersObject.
                           var oo = $scope.projectMembersObject;
                           var item = [], i = 0, folo = [];
                           for (i = 0; i < oo.length; i++) {
                               var jsonItem = {};
                               jsonItem.Value = oo[i].Value;
                               jsonItem.Text = oo[i].Text;
                               jsonItem.icon = '<img src="' + $scope.imgURL + oo[i].MemberProfilePhoto + '" />';
                               jsonItem.ticked = false;
                               item.push(jsonItem);
                           }
                           for (i = 0; i < followersObject.length; i++) {
                               var arr = followersObject[i].Value;
                               //jsonItem.Text = followersObject[i].Text;
                               //folo.push(jsonItem);
                               item.filter(function (obj) { if (obj.Value == arr) { obj.false = true; } });
                           }

                           $scope.newModel = item;
                           $scope.output = [];
                           // $scope.FollowersModel = folo;//currentFollowers;

                       }


                       $scope.updateFollower = function () {

                           //$('#globalLoader').show();
                           var obj = $scope.output;
                           var currentFollowers = [];
                           var followerIdList = [];
                           currentFollowers = $filter('filter')($scope.newModel, { 'false': true });
                           for (var i = 0; i < currentFollowers.length; i++) {
                               followerIdList.push(currentFollowers[i].Value);
                           }

                           $scope.followerAddUpdateModel.Followers = followerIdList;
                           //return false;
                           var model = { 'FollowerUpdateModel': $scope.followerAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': sharedService.getCompanyId() };
                           followersFactory.updateFollowers(model)
                               .success(function (dataUpdate) {
                                   if (dataUpdate.success) {
                                       /* Get Updated Followers for the Todo*/
                                       followersFactory.getFollowers($scope.todoDescriptionObject.TodoId)
                                           .success(function (data) {
                                               if (data.success) {
                                                   $scope.followersObject = data.ResponseData;
                                                   $timeout(function () {
                                                       $scope.$apply();

                                                       /*call the notification service */
                                                       $scope.getNotifications($scope.todoDescriptionObject.TodoId);
                                                       $scope.Sections[$scope.sectionIndex].Todos[$scope.todoIndex].ModifiedDate = new Date();
                                                       //toaster.success("Success", dataUpdate.message);
                                                       //$('#modalFollowerEdit').modal('hide');
                                                       //$('#globalLoader').hide();
                                                   });
                                               }
                                               else {
                                                   toaster.error("Error", data.message);
                                                   //$('#globalLoader').hide();
                                               }
                                           })
                                           .error(function () {
                                               toaster.error("Error", "Some Error Occured !");
                                               //$('#globalLoader').hide();
                                           });
                                   }
                                   else {
                                       toaster.error("Error", dataUpdate.message);
                                       //$('#globalLoader').hide();
                                   }
                               })
                               .error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                                   //$('#globalLoader').hide();
                               });
                       }


                       $scope.showOtherUserProfileForTodo = function ($event, id) {
                           $event.stopPropagation();
                           if (id == undefined || id == null) {
                               toaster.warning("Warning", "This todo is not assigned to anyone !!");
                               return;
                           }
                           $location.path('/user/profile/' + id);
                       }



                   }]);
});