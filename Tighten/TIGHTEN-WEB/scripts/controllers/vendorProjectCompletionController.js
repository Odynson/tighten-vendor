﻿define(["app"], function () {

    app.controller('vendorProjectCompletionController', function ($scope, sharedService, $rootScope, toaster, apiURL, $stateParams, $location, $timeout, projectsFactory) {


        var ProjectId = $stateParams.pid;
        $scope.imgURL = apiURL.imageAddress;
        
        $scope.ProjectCompletionModal = {};
        
        $scope.feedbackModal = { vendorId: '', projectCompletiontDate: '' ,vendorFeedback: '',rating: '', ResourceList: []}

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }

        angular.element(document).ready(function () {
            CompanyId = sharedService.getCompanyId();
            UserId = sharedService.getUserId();
            VendorId = sharedService.getVendorId();

            getProjectforCompletion(ProjectId, CompanyId, VendorId)

        })

        $scope.popup = {}
        $scope.format = 'MM/dd/yyyy';
        $scope.open = function () {
            $scope.popup.opened = true;
            $scope.mindate = new Date();
        };


        $scope.GetCurrentdate = function () {
            debugger;
            $scope.feedbackModal.projectCompletiontDate = moment(new Date()).format("MM/DD/YYYY");
        }



        function getProjectforCompletion(ProjectId, CompanyId, VendorId) {
            $('#globalLoader').show();
            debugger;
            projectsFactory.getProjectforCompletion(ProjectId, CompanyId, VendorId)
            .success(function (data) {
     
                if (data.success) {

                    if (data.ResponseData.ProjectId != 0) {

                        $scope.ProjectCompletionModal = data.ResponseData;
                        toaster.success("Success", message.successdataLoad);
                        $scope.feedbackModal.vendorFeedback = data.ResponseData.vendorFeedback;
                        $scope.feedbackModal.rating = data.ResponseData.Rating;
                        $scope.feedbackModal.projectCompletiontDate = data.ResponseData.ProjectEndDate;
                        $scope.isRated = $scope.ProjectCompletionModal.IsComplete == 3 ? 'true' : false
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", data.error);
                        $('#globalLoader').hide();
                    }
                }

            }), Error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message.error);
            })

        }

        $scope.confirmationBeforSaveProject = function () {
            debugger;
            if ($scope.ProjectCompletionModal.Invoice > 0)
            { 
                toaster.error("Error", "Project can't be completed untill all invoice are cleared.There seems to have some invoice(s) pending");
                return;
            }
            $('#modalConfirmationCompleteProject').modal('show')
        }




        // Checking wheather the input character is number or not 
        $scope.isNumber = function (evnt) {
            if (evnt.keyCode > 47 && evnt.keyCode < 58 || evnt.charCode > 47 && evnt.charCode < 58 || evnt.charCode > 0 && event.charCode < 15) {
                return true;
            }
            else {
                evnt.preventDefault();
                return false;
            }
        };

        $scope.saveCompletionVendorProject = function () {
            debugger;
            var data = {}

            data.VendorId = $scope.ProjectCompletionModal.VendorId;
            data.ProjectId = $scope.ProjectCompletionModal.ProjectId;
            data.ProjectEndDate = $scope.feedbackModal.projectCompletiontDate;
            data.VendorFeedback = $scope.feedbackModal.vendorFeedback;
            data.Rating = $scope.feedbackModal.rating;
            data.VendorResourceList = $scope.ProjectCompletionModal.VendorResourceList;
            projectsFactory.completionProject(data)
            .success(function (data) {
                if (data.success) {
                    toaster.success("Success", message.saveSccessFullyComman);
                    $location.path('/project/summary');
        

                }
                else {
                    $('#globalLoader').hide();
                    toaster.error("Error", message.error);
                    $('#modalConfirmationCompleteProject').modal('hide')
                }

            }), Error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message.error);
                $('#modalConfirmationCompleteProject').modal('hide')
            })
       
        }

    })


})