﻿define(['app', 'ngResource'], function (app) {


    app.controller('myProjectVendorGeneralUserProfile', function ($scope, $rootScope, $stateParams, apiURL, sharedService, toaster, $sce, vendorGeneralProfileFactory, $http, $resource, DTOptionsBuilder, DTColumnBuilder) {
        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCount = 100;  // Total number of items in all pages. initialize as a zero  
        $scope.PageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 5; // Maximum number of items per page.  
        //-------------------pagesizeList---------------------------------------------------/////
        $scope.pagesizeList = [
            { PageSize: 5 },
            { PageSize: 10 },
            { PageSize: 25 },
            { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];
        $scope.totalPageCount = 0;

        var baseurl = apiURL.baseAddress;
        var dataModelProject = {};
        var VendorGeneralUserId = ""
        $scope.format = 'MM/dd/yyyy';
        $scope.popup = {}
        $scope.workdiaryModel = { note: "", date: moment(new Date()).format("MM/DD/YYYY"), hour: "" }
        $scope.vendorGeneraluserProfileMyProjectList = [];

        //$(document).ready(function () {
        //$('#table_id').DataTable();
        //});
        angular.element(document).ready(function () {

            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }

            VendorGeneralUserId = sharedService.getUserId();
            dataModelProject = {
                VendorGeneralUserId: VendorGeneralUserId,
                projectid: 0,
                projectstatus: 0
            }
            getMyProjectVendorGeneralProfile(dataModelProject)

        })


        //////////////////////////////////////--Getting Project which Resource Actually worked/////////////////////////////////////////////////////
        function getMyProjectVendorGeneralProfile(dataModelProject) {
            debugger;

            dataModelProject.PageIndex = $scope.PageIndex;
            dataModelProject.PageSizeSelected = $scope.pageSizeSelected;

            $('#globalLoader').show();
            vendorGeneralProfileFactory.getMyProjectVendorGeneralProfile(dataModelProject)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.vendorGeneraluserProfileMyProjectList = data.ResponseData
                        if (data.ResponseData.length > 0) {
                            $scope.totalCount = data.ResponseData[0].TotalPageCount;
                        }
                        debugger;
                        //$('#table_id').DataTable({
                        //    data: data.ResponseData,
                        //    deferRender: true,
                        //    scrollY: 200,
                        //    scrollCollapse: true,
                        //    scroller: true,
                        //    //"ajax": {
                        //    //    "url": "data/objects_root_array.txt",
                        //    //    "dataSrc": ""
                        //    //},
                        //    "columns": [
                        //        { "data": "ProjectName" },
                        //        { "data": "ProjectStartDate" },
                        //        { "data": "Rating" },
                        //        { "data": "RoleAllotted" },
                        //        { "data": "VendorPoc" },
                        //        { "data": "HoursAllotted" },
                        //        { "data": "HoursSpent" },
                        //        //{ "data": "ProjectId" }
                        //    ]
                        //});
                        //$('#table_id').DataTable();
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    }
                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })
        }



        //////////////////////////////////////---Check box Filter//////////////////////////////////////////////////////////
        $scope.isOnGoingChecked = false;
        $scope.isCompleteChecked = false;
        $scope.isAllProjectChecked = true;


        $scope.onGoingProjectChange = function (isOnGoingChecked) {
            $scope.isOnGoingChecked = true;
            $scope.isCompleteChecked = false;
            $scope.isAllProjectChecked = false;

            if (isOnGoingChecked) {
                dataModelProject.projectstatus = 1
                getMyProjectVendorGeneralProfile(dataModelProject)
            }

        }


        $scope.completedProjectChange = function (isCompleteChecked) {
            debugger
            $scope.isOnGoingChecked = false;
            $scope.isCompleteChecked = true;
            $scope.isAllProjectChecked = false;
            if (isCompleteChecked) {

                dataModelProject.projectstatus = 3
                getMyProjectVendorGeneralProfile(dataModelProject)
            }

        }

        $scope.allProjectClick = function (isAllProjectChecked) {
            debugger
            $scope.isOnGoingChecked = false;
            $scope.isCompleteChecked = false;
            $scope.isAllProjectChecked = true;
            if (isAllProjectChecked) {
                dataModelProject.projectstatus = 0
                getMyProjectVendorGeneralProfile(dataModelProject)
            }

        }
        //////////////////////////////////////---//////////////////////////////////////////////////////////


        //////////////////////////////////////--Work Diary---/////////////////////////////////////////////////////////
        $scope.workDiaryList = []
        var ProjectId = 0

        var dataModelSaveNote = {
            Note: "",
            NoteDate: "",
            hour: 0,
            ProjectId: ProjectId,
            VendorGeneralUserId: VendorGeneralUserId,
        }

        $scope.workDiaryVendorGeneralUserModal = function (ProjectId) {
            dataModelSaveNote.ProjectId = ProjectId;
            getVendorGeneraUserWorkNote(ProjectId, VendorGeneralUserId);
            $("#WorkDiaryVendorGeneralUserModal").modal('show');

        }

        /////////////////////////////////////---Get-working Note List//////////////////////////////////////////////////////////////////
        function getVendorGeneraUserWorkNote(ProjectId, VendorGeneralUserId) {
            debugger;
            $('#globalLoader').show();
            vendorGeneralProfileFactory.getVendorGeneraUserWorkNote(ProjectId, VendorGeneralUserId)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.workDiaryList = data.ResponseData;
                        $("#WorkDiaryVendorGeneralUserModal").modal('show');
                        $('#globalLoader').hide();
                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    }
                }),
                Error(function myfunction() {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();

                })

        }

        ///////////////////////////////////--add Note for Project///////////////////////////////////////////////

        $scope.addWorkDiaryVendorGeneralUserClick = function (workdiaryModel, ResourceForm) {
            debugger;
            if (!ResourceForm.$valid) {
                if (ResourceForm.noteName) {
                    ResourceForm.noteName.$touched = true;
                    ResourceForm.noteName.$dirty = true;
                }
                if (ResourceForm.projectStartDate) {
                    ResourceForm.projectStartDate.$touched = true;
                    ResourceForm.projectStartDate.$dirty = true;
                }
                if (ResourceForm.Enterhour) {
                    ResourceForm.Enterhour.$touched = true;
                    ResourceForm.Enterhour.$dirty = true;
                }
                return;
            }

            $('#globalLoader').hide();

            dataModelSaveNote.Note = workdiaryModel.note,
                dataModelSaveNote.NoteDate = workdiaryModel.date,
                dataModelSaveNote.hour = workdiaryModel.hour,
                dataModelSaveNote.VendorGeneralUserId = VendorGeneralUserId,


                vendorGeneralProfileFactory.addWorkDiaryVendorGeneralUser(dataModelSaveNote)
                    .success(function (data) {

                        if (data.ResponseData > 0) {
                            toaster.success("success", message.saveSccessFullyComman);
                            getVendorGeneraUserWorkNote(dataModelSaveNote.ProjectId, dataModelSaveNote.VendorGeneralUserId); // bind data again

                            $scope.workdiaryModel = { note: "", date: moment(new Date()).format("MM/DD/YYYY"), hour: "" }; // empty Object
                            $('#globalLoader').hide();
                        }
                        else {
                            toaster.error("Error", message.error);
                            $('#globalLoader').hide();
                        }

                        ResourceForm.noteName.$dirty = false;
                        ResourceForm.projectStartDate.$dirty = false;
                        ResourceForm.Enterhour.$dirty = false;

                    }), Error(function () {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    })

        }





        ///////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////date related for Work Diary/////////////////////////////////////////////////



        $scope.open1 = function () {
            debugger;
            $scope.popup.opened1 = true;
            $scope.mindate1 = new Date();
        };

        ///////////////////////////////////date related for Work Diary/////////////////////////////////////////////////


        $scope.autoCompleteOptionVendorGeneralUser = {

            minimumChars: 3,
            dropdownWidth: '500px',
            dropdownHeight: '200px',
            data: function (term) {
                debugger;
                return $http.post(baseurl + 'VendorGeneralProfileApi/GetProjectNameForFilter',
                    dataModal = { VendorGeneralUserId: VendorGeneralUserId, ProjectName: term })
                    .then(function (data) {
                        debugger;
                        return data.data.ResponseData;

                    });
            },
            renderItem: function (item) {
                return {
                    value: item.ProjectName,
                    label: $sce.trustAsHtml(
                        "<p class='auto-complete'>"
                        + item.ProjectName +
                        "</p>")
                };
            },
            itemSelected: function (item) {
                debugger;

                dataModelProject.ProjectId = item.item.ProjectId;
                getMyProjectVendorGeneralProfile(dataModelProject)
            }
        }

        var isCtrlPressed;
        var isAPressed;

        /////////////////////////////empty search text box project  in Vendor general user Profile//////////////////////////////////////////////////////
        $scope.searchProjectByNameVendorGeneralUser = function (ProjectNameFilter, event) {

            if (ProjectNameFilter != undefined && ProjectNameFilter.length <= 1 && event.keyCode == 8) {
                dataModelProject.ProjectId = 0
                getMyProjectVendorGeneralProfile(dataModelProject)
            }
            // if key is pressed
            if (event.keyCode == 17 || isCtrlPressed) {
                isCtrlPressed = true;
            }
            else {
                isCtrlPressed = false;
            }
            // other key after the control
            if (isCtrlPressed) {
                if (event.keyCode == 97 || event.keyCode == 65) {
                    isAPressed = true;
                }
            } else {
                isAPressed = false;
            }

            if (isCtrlPressed && isAPressed) {
                if (event.keyCode == 46 || event.keyCode == 8) {
                    dataModelProject.ProjectId = 0
                    getMyProjectVendorGeneralProfile(dataModelProject)
                    isCtrlPressed = false;
                    isAPressed = false;
                }

            }
            var text = window.getSelection().toString()
            if (text != "" && (event.keyCode == 8 || event.keyCode == 46)) {
                dataModelProject.ProjectId = 0
                getMyProjectVendorGeneralProfile(dataModelProject);
            }
        }

        $scope.cutSearchProjectVendorGeneraluser = function () {

            dataModelProject.ProjectId = 0
            getMyProjectVendorGeneralProfile(dataModelProject);

        }

        ////////////////////////////////////------------paging code/////////////////////////////////

        //-------------------changePageSize--------------------------------------------------------/////
        $scope.changePageSize = function (pageSizeSelected) {
            debugger;
            $scope.pageSizeSelected = pageSizeSelected.PageSize;
            getMyProjectVendorGeneralProfile(dataModelProject);
        }

        //-------------------pageChanged-------------------------------------------------------/////
        $scope.pageChanged = function (pageIndex) {
            debugger;
            $scope.PageIndex = pageIndex;
            getMyProjectVendorGeneralProfile(dataModelProject);
        }


        ////////////////////////////////////------------paging code/////////////////////////////////
    })


})
