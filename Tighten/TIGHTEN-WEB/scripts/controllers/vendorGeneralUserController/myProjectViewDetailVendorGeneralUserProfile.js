﻿define(['app'], function (app) {

    app.controller('myProjectViewDetailVendorGeneralUserProfile', function ($scope, $rootScope, $stateParams, toaster, $http, apiURL, sharedService, vendorGeneralProfileFactory) {


        var ProjectId = $stateParams.pid;
        var UserId = sharedService.getUserId();

        // custom control
        $scope.CustomDirectiveReady = false;
        $scope.ProjectIsEdit = true;
        $scope.customcontrolmodel = {};

        angular.element(document).ready(function () {

            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }

            getMyProjectViewDetailVendorGeneralProfile(ProjectId, UserId)

        })

        function getMyProjectViewDetailVendorGeneralProfile(ProjectId, UserId) {
            debugger;
            $('#globalLoader').show();
            vendorGeneralProfileFactory.getMyProjectViewDetailVendorGeneralProfile(ProjectId, UserId)
            .success(function (data) {

                debugger;
                $('#globalLoader').hide();
                if (data.ResponseData != null) {
                    $scope.projectAwardedViewDetailList = data.ResponseData;
                    //------getting custom control area ---------//
                    if (data.ResponseData.CustomControlValueModel != null) {
                        $scope.customcontrolmodel = JSON.parse(data.ResponseData.CustomControlValueModel.CustomControlData);


                    }
                    //-----------------Custom cuntrol is not use by Company---------------------------------------///       

                    $scope.CustomcontrolList = data.ResponseData.CustomControlUIList;
                    $scope.CustomDirectiveReady = true;
                }
                else {
                    toaster.error("Error", message.error);
                }


            }), Error(function myfunction() {
                toaster.error("Error", message.error);
                $('#globalLoader').hide();

            })


        }


        //////////////////////////////////////////////////////modalMilestoneDetail///////////////////////////////////////////////////////////////////////
        $scope.PhaseIndex
        $scope.modalMilestoneProjectAwardedScreenVendorProfile = function (index) {
            debugger;
            $scope.PhaseIndex = index;
            $('#modalshowMilestoneProjectAwardedVendorGeneralProfile').modal('show')

        }


        // file download code here
        $scope.downloadProjectandAwardedForVendorGeneralProfile = function (FilePath, FileName, ActualFileName) {
            debugger;
            $('#globalLoader').show();

            var url = apiURL.baseAddress + "VendorApi/";

            $http({
                method: 'Post',
                url: url + "GetFiledownload",
                data: { FileName: FileName, FilePath: FilePath },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                headers = headers();
                debugger;
                //var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", ActualFileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    $('#globalLoader').hide();

                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    console.log(ex);
                    $('#globalLoader').hide();
                }
            }).error(function (data, status) {
                console.log(data);
                $('#globalLoader').hide();
            });
        };




    })


})