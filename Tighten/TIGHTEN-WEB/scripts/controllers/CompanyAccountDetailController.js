﻿
define(["app"], function (app) {

    app.controller("CompanyAccountDetailController", function ($scope, $rootScope, apiURL, $location, companyAccountFactory, sharedService, toaster, $stateParams, $window, $confirm, $timeout, vendorFactory) {
        var baseurl = apiURL.baseAddress;
        var CompanyId = sharedService.getCompanyId();
        var UserId = sharedService.getUserId();
        $scope.myList = [];  // my vendor for my vendor page
        $scope.companyAccountDetail = {};
        $scope.AccId;
        $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
        $scope.pageSizeSelected = 1;
        $scope.isEditMode = true;
        $scope.IsRegisteredVendor = true;
        $scope.getPOCList = [];
        $scope.POCTags = [];
        $scope.FromPage = "";
        $scope.IsPageFromRegCompany =false;
        var multiSelectPreviousData = [];
        var multiSelectCurrentData = [];
        angular.element(document).ready(function () {
            debugger;
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');

            }
            $scope.AccId = $stateParams.accid;
            $scope.FromPage = $stateParams.frompage;
            if ($scope.FromPage == "RegisteredComapny") {
                $scope.IsPageFromRegCompany = true;
            }
            else if ($scope.FromPage == "Edit") {
                $scope.IsPageFromRegCompany = false;
            }
            BindVendorPOCList();
            if ($scope.AccId != undefined && $scope.AccId != null && $scope.AccId != "") {
                getCompanyAccountDetail()
            }
            $scope.isEditMode = false;
            //$scope.getVendorPOCList();
            

        })

        ////////////////////////////////////------------paging code/////////////////////////////////
        $scope.pagesizeList = [
            { PageSize: 5 },
            { PageSize: 10 },
            { PageSize: 25 },
            { PageSize: 50 },

        ];

        $scope.PageSize = $scope.pagesizeList[0];

        $scope.changePageSize = function (pageSizeSelected) {
            debugger;
            dataModal.PageSize = pageSizeSelected;
            getInvitationTovendor(dataModal)
        }

        $scope.pageChanged = function (pageIndex) {
            debugger;
            dataModal.PageIndex = pageIndex;
            getInvitationTovendor(dataModal)
        }

        ////---------------------------getCompanyAccountDetail---------------------------------------------///////////

        function getCompanyAccountDetail() {
            $('#globalLoader').show();
            companyAccountFactory.GetCompanyAccount($scope.AccId).success(function (data) {
                debugger;
                $('#globalLoader').hide();
                if (data.success) {
                    if (data.ResponseData != null && data.ResponseData != undefined) {
                        debugger;
                        $scope.companyAccountDetail = data.ResponseData;
                        $scope.companyAccountDetail.AccountProjectList = [];
                        $scope.IsRegisteredVendor = $scope.companyAccountDetail.IsRegisteredVendor;
                        if ($scope.companyAccountDetail != undefined && $scope.companyAccountDetail != null &&
                            $scope.companyAccountDetail.AccountPOCList != undefined && $scope.companyAccountDetail.AccountPOCList != null) {
                            
                            for (var i = 0; i < $scope.companyAccountDetail.AccountPOCList.length; i++) {
                                var temp = {};
                                temp.UserId = $scope.companyAccountDetail.AccountPOCList[i].UserId;
                                temp.FirstName = $scope.companyAccountDetail.AccountPOCList[i].FirstName;
                                temp.LastName = $scope.companyAccountDetail.AccountPOCList[i].LastName;
                                //temp.pocFullName = $scope.companyAccountDetail.AccountPOCList[i].FirstName + " " + $scope.companyAccountDetail.AccountPOCList[i].LastName + " - " + $scope.companyAccountDetail.AccountPOCList[i].Id;
                                temp.pocFullName = $scope.companyAccountDetail.AccountPOCList[i].pocFullName;// + " " + $scope.companyAccountDetail.AccountPOCList[i].LastName + " - " + $scope.companyAccountDetail.AccountPOCList[i].Id;
                                temp.CompanyId = $scope.companyAccountDetail.AccountPOCList[i].VendorId;
                                temp.pocEmail = $scope.companyAccountDetail.AccountPOCList[i].pocEmail;
                                $scope.POCTags.push(temp);
                            }
                        }
                    }
                }
                else {
                    toaster.error("Error", data.message);
                }

            })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
        }
        $scope.GoBack = function () {
            RedirectBack();
            //if ($scope.IsPageFromRegCompany == false) {
            //    $window.location.href = ('/#/my/account');
            //}
            //else
            //{
            //    $window.location.href = ('/#/my/enterpriseaccounts');
            //}
        }
        function RedirectBack() {
            if ($scope.IsPageFromRegCompany == false) {
                $window.location.href = ('/#/my/account');
            }
            else {
                $window.location.href = ('/#/my/enterpriseaccounts');
            }
        }
        $scope.deleteCompanyAccountDetail = function () {
            debugger;
            var Id = $scope.AccId;
            $confirm({ text: 'Are you sure you want to delete this Company Account Detail?', title: 'Delete it', ok: 'Yes', cancel: 'No' })

              .then(function () {
                  companyAccountFactory.deleteCompanyAccountDetail(Id)
                      .success(function (data) {
                          debugger;
                          if (data.success) {
                              toaster.success("Success", message[data.message]);
                              $('#globalLoader').hide();
                              
                          }

                      }),
                      Error(function (data, status, headers, config) {
                          toaster.error("Error", "Some Error Occured !");
                          $('#globalLoader').hide();
                      })
              })
        }
        $scope.EditCompanyAccountDetail = function () {
            $scope.isEditMode = false;
            $scope.getPOCList = [];
            $scope.getVendorPOCList();
            if ($scope.POCTags == undefined || $scope.POCTags == null) {
                $scope.POCTags = [];
            }
        }
        $scope.CancelCompanyAccountDetail = function () {
            $scope.isEditMode = true;
            $scope.getPOCList = [];
        }
        $scope.editRoleTags = function () {
            debugger;
            multiSelectCurrentData = $scope.POCTags;
            for (var i = 0; i < multiSelectCurrentData.length; i++) {
                multiSelectPreviousData.push(multiSelectCurrentData[i]);
            }

            $scope.openSaveRoleTags = true;
            $timeout(function () {
                $('#RoleTags  .host .tags .input').focus();
            }, 100);
        }
        function BindVendorPOCList() {
            $('#globalLoader').show();
            if ($scope.IsPageFromRegCompany == false) {
                /* Getting Company Teams with Users */
                companyAccountFactory.GetVendorPOCList(CompanyId, $scope.companyAccountDetail.VendorId)
                    .success(function (data) {
                        debugger
                        $('#globalLoader').hide();
                        if (data.success) {

                            if (data.ResponseData.length > 0) {
                                $scope.getPOCList = data.ResponseData;
                            }
                        }
                        else {
                            toaster.error("Error", data.message);
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured");
                    });
            }
            else {
               // alert('Hello ' + sharedService.getUserId());
                vendorFactory.GetAllNewUsers(CompanyId, sharedService.getUserId())
               .success(function (data) {
                   debugger;
                   if (data.success && data.ResponseData != undefined) {

                       $scope.getPOCList = [];
                       var UsersObject = data.ResponseData;
                       for (var i = 0; i < UsersObject.length; i++) {
                           var temp = {};
                           temp.pocFullName = UsersObject[i].Name + ' - ' + UsersObject[i].Id;
                           temp.UserId = UsersObject[i].UserId;
                           $scope.getPOCList.push(temp);
                       }
                       $('#globalLoader').hide();
                   }
                   else {
                       $('#globalLoader').hide();
                   }
               })
                .error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured");
                });
              //  alert('bye');
            }
        }
        $scope.getVendorPOCList = function () {
            debugger
            $('#globalLoader').show();
            if ($scope.IsPageFromRegCompany == false) {
                /* Getting Company Teams with Users */
                companyAccountFactory.GetVendorPOCList(CompanyId, $scope.companyAccountDetail.VendorId)
                    .success(function (data) {
                        debugger
                        $('#globalLoader').hide();
                        if (data.success) {

                            if (data.ResponseData.length > 0) {
                                $scope.getPOCList = data.ResponseData;
                            }
                        }
                        else {
                            toaster.error("Error", data.message);
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured");
                    });
            }
            else {
                vendorFactory.GetAllNewUsers($scope.selectedCompanyId, sharedService.getUserId())
               .success(function (data) {
                   debugger;
                   if (data.success && data.ResponseData != undefined) {
                       
                       $scope.getPOCList = [];
                       var UsersObject = data.ResponseData;
                       for (var i = 0; i < UsersObject.length; i++) {
                           var temp = {};
                           temp.pocFullName = UsersObject[i].Name + ' ' + UsersObject[i].Id;
                           temp.UserId = UsersObject[i].UserId;
                           $scope.getPOCList.push(temp);
                       }
                       $('#globalLoader').hide();
                   }
                   else {
                       $('#globalLoader').hide();
                   }
               })
                .error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", "Some Error Occured");
                });
               
            }
        }
        $scope.tagRemoved = function (tag) {
            debugger;
            for (var i = 0; i < $scope.companyAccountDetail.AccountPOCList.length; i++) {
                if ($scope.companyAccountDetail.AccountPOCList[i].UserId == tag.UserId) {
                    $scope.companyAccountDetail.AccountPOCList.splice(i, 1);
                    break;
                }
            }
            //var index = $scope.columns.indexOf(tag);
            //$scope.removeColumn(index);
        }
        $scope.tagAdded = function (tag) {
            debugger;
            //for (var i = 0; i < $scope.companyAccountDetail.AccountPOCList.length; i++) {
            if ($scope.companyAccountDetail != undefined && $scope.companyAccountDetail != null
                && $scope.companyAccountDetail.AccountPOCList != undefined && $scope.companyAccountDetail.AccountPOCList != null) {
            }
            else
            {
                if ($scope.companyAccountDetail == undefined || $scope.companyAccountDetail == null)
                $scope.companyAccountDetail = {};
                $scope.companyAccountDetail.AccountPOCList = [];
            }
                var temp = {};
                temp.UserId = tag.UserId;
                temp.FirstName = tag.pocFirstName;
                temp.LastName = tag.pocLastName;
                temp.CompanyId = CompanyId;
                temp.VendorId = tag.CompanyId;
                temp.Email = tag.pocEmail;
                temp.Id = tag.Id;
                $scope.companyAccountDetail.AccountPOCList.push(temp);
            //}
         //   $scope.addNewColumn(tag);
        }


        /*Update Team*/
        $scope.updateCompanyAccount = function () {
            debugger;
            //alert($scope.isEditMode);
            if (($scope.IsRegisteredVendor ==false|| $scope.IsPageFromRegCompany==true)&&$scope.POCTags.length == 0) {
                $("#RoleTags").focus();
                return;
            }
            else {
                //var NewTeamObj = { 'TeamModel': $scope.teamAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId }
                $('#globalLoader').show();
                CompanyId
                $scope.companyAccountDetail.UserId = UserId;
                companyAccountFactory.updateCompanyAccountDetail($scope.companyAccountDetail)
                    .success(function (data) {
                        debugger;
                        if (data.success) {
                            if (data.ResponseData == 'CompanyAccDetailUpdatedSuccessfully') {
                                toaster.success("Success", message[data.ResponseData]);
                                RedirectBack();
                                //if ($scope.IsPageFromRegCompany == false) {
                                //    $window.location.href = ('/#/my/account');
                                //}
                                //else {
                                //    $window.location.href = ('/#/my/enterpriseaccounts');
                                //}
                            }
                            else if (data.ResponseData == 'CompanyAccountNotExists') {
                                toaster.warning("Warning", message[data.ResponseData]);

                            }
                            else {
                                toaster.error("Error", data.message);

                            }
                                $('#globalLoader').hide();
                                return;
                            }
                            else {
                                toaster.error("Error", data.message);
                                $('#globalLoader').hide();
                            }
                        
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();
                    });
            }
        }

         $scope.create_zip = function () {
             debugger;
             var accId = $scope.AccId;
            $window.open(baseurl+"CompanyAccountApi/DownloadDocs/" + accId, "_self");
            //$window.open = "http://localhost:55580/api/CompanyAccountApi/DownloadDocs/"+accId;
            //companyAccountFactory.DownloadDocAttachments(accId)
            //    .success(function (data, status, headers, config) {
            //        debugger;
            //        var file = new Blob([data], {
            //            type: 'application/zip'
            //        });
            //        //trick to download store a file having its URL
            //        var fileURL = URL.createObjectURL(file);
            //        var a = document.createElement('a');
            //        a.href = fileURL;
            //        a.target = '_self';
            //        a.download = "doc.zip";// projectAddUpdateModel.ActualFileName;
            //        document.body.appendChild(a);
            //        a.click();
            //    })
            //    .error(function (data, status, headers, config) {
            //        toaster.error("Error", message["error"]);
            //    })
         }
         $scope.DownloadFile = function (DownloadItem) {
             debugger;
             //var itemobj = {};
             //itemobj.Id = DownloadItem.Id;
             //itemobj.ActualFileName = DownloadItem.ActualFileName;
             //itemobj.Name = DownloadItem.Name;
             //itemobj.AccId = DownloadItem.AccId;
             //itemobj.Path = DownloadItem.Path;
             companyAccountFactory.GetSingleAttachmentownload(DownloadItem.Id)
                 .success(function (data, status, headers, config) {

                     var file = new Blob([data], {
                         type: 'application/csv'
                     });
                     //trick to download store a file having its URL
                     var fileURL = URL.createObjectURL(file);
                     var a = document.createElement('a');
                     a.href = fileURL;
                     a.target = '_blank';
                     a.download = DownloadItem.ActualFileName;
                     document.body.appendChild(a);
                     a.click();
                     setTimeout(function(){
                         // For Firefox it is necessary to delay revoking the ObjectURL
                         window.URL.revokeObjectURL(data)
                         , 100});
                 })
                 .error(function (data, status, headers, config) {
                     toaster.error("Error", message["error"]);
                 })
         }

        //Download Project Document File with original name 
         $scope.PreviewFile = function (PreviewItem) {
             $window.open('/#/account/attachement/preview/' + PreviewItem.Id, "_blank");
             //$window.location.href = ('/#/account/attachement/preview/'+PreviewItem.Id);
         }
    })


});

