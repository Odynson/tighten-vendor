﻿define(['app', 'notificationFactory', 'teamFactory', 'CalendarEventsFactory', ], function (app, usersFactory, roleFactory) {
    app.controller("notificationController", function ($scope, $rootScope, apiURL,$timeout, $http, sharedService, toaster, $sce, $window,projectsFactory,
                                                        $filter, notificationFactory, teamFactory, CalendarEventsFactory, todosFactory, $location) {
        $scope.imgURL = apiURL.imageAddress;
        /* Showing Notification Management Section to see the information*/
        $scope.showNotificationManagementSection = function () {
            //closeCentreSections();
            $('#globalLoader').show();
            $scope.showCenterLoader = true;
            $scope.selectedNotification = "";
            $scope.inputSearchNotification = "";
            $scope.selectedProject = 0;
            $scope.selectedNotification = "";
            $scope.selectedTodo = 0;
            $scope.selectedNavigation = "NotificationManagementSection";
            notificationFactory.getNotifications(sharedService.getUserId()).success(function (data) {
                if (data.success) {
                    $scope.NotificationsObject = data.ResponseData;
                    $scope.showCenterLoader = false;
                    $('#divNotificationManagementSection').show();

                    //$timeout(function () {
                    //    $scope.$apply();
                    //    $('#ProjectDescriptionSection').slimScroll({
                    //        //position: 'left',
                    //        height: '45%',
                    //        railVisible: false,
                    //        alwaysVisible: false
                    //    });
                    //    $('#ProjectDescriptionSection').css('width', '97%');
                    //});
                    $('#globalLoader').hide();
                }
                else {
                    toaster.error("Error", data.message);
                     $('#globalLoader').hide();
                }
            }).error(function () {
                toaster.error("Error", "Some Error Occured !");
                $('#globalLoader').hide();
            });
        };

        /* Showing Notification Description Section to see full information  */
        
        $scope.showNotificationDescriptionSection = function (notificationObject) {
            
            
            $scope.selectedNotification = notificationObject.Id;
            
            if (notificationObject.Type == 1) {
                $scope.closeRightSections();
                $scope.stretchCentreWindow = false;
                teamFactory.getTeam(notificationObject.ConcernedSectionId, sharedService.getUserId()).success(function (data) {
                    if (data.success) {
                        $scope.teamDescriptionObject = data.ResponseData;
                        $('#divTeamDescriptionSection').show();
                        $('#hideRightWindow').show();
                        $scope.hideRightWindow = false;
                        $scope.hideRightWindow_Project = true;
                        $scope.hideRightWindow_Event = true;
                        $scope.hideRightWindow_Todo = true;
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                }).error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });
            }
            else if (notificationObject.Type == 2) {
                
                $scope.closeRightSections();
                $scope.stretchCentreWindow = false;
                projectsFactory.getProject(notificationObject.ConcernedSectionId)
                .success(function (data) {
                    if (data.success) {
                        $scope.projectDescriptionObject = data.ResponseData;
                        $('#divProjectDescriptionSection').show();
                        $('#hideRightWindow_Project').show();
                        $scope.hideRightWindow_Project = false;
                        $scope.hideRightWindow = true;
                        $scope.hideRightWindow_Event = true;
                        $scope.hideRightWindow_Todo = true;
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                })

                
            }
            else if (notificationObject.Type == 3) {
                $scope.closeRightSections();
                $scope.stretchCentreWindow = false;
                todosFactory.getTodoDetailForNotification(notificationObject.ConcernedSectionId, sharedService.getUserId())
                .success(function (data) {

                    if (data.success) {
                        $scope.todoDescriptionObject = data.ResponseData;
                        $('#divTodoDescriptionSectionForNotification').show();
                        $('#hideRightWindow_Todo').show();
                        $scope.hideRightWindow_Todo = false;
                        $scope.hideRightWindow = true;
                        $scope.hideRightWindow_Project = true;
                        $scope.hideRightWindow_Event = true;
                        
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                })

            }


            else if (notificationObject.Type == 6) {
                $scope.closeRightSections();
                $scope.stretchCentreWindow = false;
                CalendarEventsFactory.getSingleCalendarEvent(notificationObject.ConcernedSectionId).success(function (data) {
                    if (data.success) {
                        $scope.eventDescriptionObject = {};
                        $scope.eventDescriptionObject = data.ResponseData;
                        $('#divEventDescriptionSection').show();
                        $('#hideRightWindow_Event').show();
                        $scope.hideRightWindow_Event = false;
                        $scope.hideRightWindow = true;
                        $scope.hideRightWindow_Project = true;
                        $scope.hideRightWindow_Todo = true;
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                }).error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });
            }

            else if (notificationObject.Type == 10) {
                $scope.closeRightSections();
                $scope.stretchCentreWindow = false;
                $scope.hideRightWindow = false;
                $scope.hideRightWindow_Project = false;
                $scope.hideRightWindow_Event = false;
                $scope.hideRightWindow_Todo = false;
                $location.path('/invitation/' + notificationObject.ConcernedSectionId );

            }


        }

        $scope.closeRightSections = function ()
        {

            $('#divToDoDescriptionSection').hide();
            $('#divEventDescriptionSection').hide();
            $('#divProjectOverview').hide();
            $('#divUserFeedbackDescriptionSection').hide();
            $('#divTeamDescriptionSection').hide();
            $('#divProjectDescriptionSection').hide();
            $('#divTodoDescriptionSectionForNotification').hide();
            
        }

        $scope.hideRightWindowFunction = function () {
            $scope.hideRightWindow = true;
            $scope.hideRightWindow_Project = true;
            $scope.hideRightWindow_Event = true;
            $scope.hideRightWindow_Todo = true;
            $scope.stretchCentreWindow = true;
            $scope.selectedTodo = "";
            $scope.selectedNotification = "";
            $scope.selectedInbox = "";
            $scope.selectedUserFeedback = "";
        }

        $scope.goToCreatorProfile = function (item) {
            $location.path('/user/profile/' + item.CreatorUserId);
        }

        $scope.goToCreatorProfileFromSidepanel = function (item) {
            
            $location.path('/user/profile/' + item);
        }

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu', []);
            }
            $scope.showNotificationManagementSection();
        });
    });
});