﻿define(['app', 'userTodoApprovalFactory', 'utilityService'], function (app) {


    //function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
    //$(document).on("keydown", disableF5);

    app.controller("userTodoApprovalController", function ($scope, $rootScope, $window, $timeout, $location, $stateParams, $http, $confirm, sharedService, toaster, apiURL, userTodoApprovalFactory, utilityService, reportersFactory) {

        //var windowElement = angular.element($window);
        //windowElement.on('beforeunload', function (event) {
        //    // do whatever you want in here before the page unloads.        

        //    // the following line of code will prevent reload or navigating away.
        //    event.preventDefault();
        //});

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.imgURL = apiURL.imageAddress;
        $scope.TodosList = [];
        $scope.groupedTodoList = [];
        $scope.Projects = [];
        //$scope.groupedTodoList = [];
        $scope.sendReportsModel = {};

        $scope.isFreelancer = sharedService.getIsFreelancer();
        $scope.selectedCompanyId = sharedService.getCompanyId();
        $scope.selectedProject = 0;

        // Company dropdown change event on left side menu for freelancer 
        $scope.$on('leftCompanyDropdownChange', function (event, data) {
            $scope.selectedCompanyId = data.CompanyId;
            showTodosSection();
        });

        $scope.selectedCompanyChanged = function (selectedCompany) {

            $scope.selectedCompanyId = selectedCompany;
            showTodosSection();
        };


        $scope.getColor = function (approvedBy, isApproved) {
            if (approvedBy == null) {
                return ""
            }
            else if (isApproved) {
                return "green-border"
            }
            else {
                return "red-border";
            }
        }

        $scope.weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        $scope.convertTo = function (arr, key, dayWise) {
            var groups = {};
            for (var i = 0; l = arr.length, i < l; i++) {
                var newkey = '';
                if (dayWise) {
                    var weekday_value = $scope.weekdays[new Date(arr[i][key]).getDay()];
                    var date = new Date(arr[i][key]).toUTCString();
                    var newdate = date.substring(0, 16)
                    //arr[i][key] = [weekday_value] + ', ' + date;
                    newkey = newdate;
                }
                else {
                    //arr[i][key] = arr[i][key].toTimeString();
                    newkey = arr[i][key].toTimeString();
                }
                //groups[arr[i][key]] = groups[arr[i][key]] || [];
                //groups[arr[i][key]].push(arr[i]);
                groups[newkey] = groups[newkey] || [];
                groups[newkey].push(arr[i]);
            }
            return groups;
        };

        // convert the minutes to time string
        $scope.ConvertMinutesToTimeString = function (minutes) {
            return utilityService.ConvertMinutesToFormatedTimeLogString(minutes);
        }

        $scope.searchResult = function () {
            //
            $('#globalLoader').show();
            var projectId = "";
            //if ($scope.selectedProject)
            //    projectIds = $scope.selectedProject.Value;
            projectId = $scope.selectedProject;

            search = { 'CompanyId': $scope.selectedCompanyId, 'AssigneeId': sharedService.getUserId(), 'From': $scope.fromDate, 'To': $scope.toDate, 'IsApproved': '', 'ProjectId': $scope.selectedProject, 'TaskStatus': $scope.selectedTaskStatus, 'TaskType': $scope.selectedTaskType };

            // Get data first time with default filter
            userTodoApprovalFactory.getAllTodos(search)
                .success(function (data) {
                    $scope.TodosList = data.ResponseData;
                    $scope.groupedTodoList = [];
                    angular.copy($scope.TodosList, $scope.groupedTodoList);
                    $scope.groupedTodoList = $scope.convertTo($scope.groupedTodoList, 'ModifiedDate', true);
                    $('#globalLoader').hide();

                    //getAssigneeList($scope.TodosList);
                    setTimeout(function () {
                        $scope.$apply();
                    }, 1000);
                })
                .error(function () {
                    $('#globalLoader').hide();
                    toaster.error("Error", "Some error occured");
                });
            /////////
        }

        $scope.removeTodoLoggedTime = function (key, parentIndex, childIndex, todoDetail) {
            $confirm({ text: 'Are you sure you want to delete this logged time ?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
                               .then(function () {
                                   $('#globalLoader').show();
                                   userTodoApprovalFactory.DeleteUserLoggedTimeSlot(todoDetail.Id)
                                   .success(function (data) {
                                       if (data != null) {
                                           $scope.groupedTodoList[key][parentIndex].TodoDetails.splice(childIndex, 1);
                                           $scope.groupedTodoList[key][parentIndex].LoggedTime = data.ResponseData;
                                       }
                                       $('#globalLoader').hide();
                                       toaster.success("Success", "record deleted sucessfully");
                                   })
                                   .error(function (data) {
                                       $('#globalLoader').hide();
                                       toaster.error("Error", "Some error occured");
                                   })
                               });
        }

        $scope.updateTimeSlot = function (key, parentIndex, childIndex, todoDetail) {
            //
            todoDetail.Duration = utilityService.createMinutesFromTimeString(todoDetail.DurationString);
            todoDetail.edit = false;

            $('#globalLoader').show();
            userTodoApprovalFactory.UpdateUserLoggedTimeSlot(sharedService.getUserId(), todoDetail.Id, todoDetail.Duration)
            .success(function (data) {
                if (data != null) {
                    $scope.groupedTodoList[key][parentIndex].LoggedTime = data.ResponseData;
                }
                $('#globalLoader').hide();
                toaster.success("Success", "record updated sucessfully");
            })
            .error(function (data) {
                $('#globalLoader').hide();
                toaster.error("Error", "Some error occured");
            })
        }

        $scope.estimatedTimeKeyPress = function (e) {

            var keyCode = e.keyCode;
            var charCode = e.charCode;
            if ((charCode > 47 && charCode < 58) || (keyCode > 47 && keyCode < 58) || (charCode == 100) || (charCode == 104) || (charCode == 109) || (charCode == 32) || (keyCode == 100) || (keyCode == 104) || (keyCode == 109) || (keyCode == 32)) { //return true;
                return true;
            }
            else {
                e.preventDefault();
                return false;
            }
        }

        ////////////Date Functions/////////////
        /************************/
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };
        var da = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date((da.getFullYear() - 100), 5, 22);
        };

        $scope.toggleMin();

        $scope.maxDate = new Date(da.getFullYear(), 5, 22);

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.open3 = function () {
            $scope.popup3.opened = true;
        };

        $scope.setDate = function (year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
        //$scope.init = new Date(da.getFullYear()-10, 5, 22);
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,

        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[3];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        $scope.popup3 = {
            opened: false
        };

        $scope.getDayClass = function (date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        };
        /***********************/

        // Page load
        angular.element(document).ready(function () {

            showTodosSection();
        });

        function showTodosSection() {

            $('#globalLoader').show();
            var d = new Date();
            d.setDate(d.getDate() - 7);
            $scope.fromDate = d;// d.setDate(d.getDate() - 7);
            $scope.toDate = new Date();

            search = { 'CompanyId': $scope.selectedCompanyId, 'AssigneeId': sharedService.getUserId(), 'From': '', 'To': '', 'IsApproved': '', 'ProjectId': $scope.selectedProject, 'TaskStatus': $scope.selectedTaskStatus, 'TaskType': $scope.selectedTaskType };
            $scope.groupedTodoList = [];
            //'ProjectId': $scope.selectedProject, 
            userTodoApprovalFactory.getUserProjects($scope.selectedCompanyId, sharedService.getUserId())
            .success(function (result) {
                // getting the list of the company users
                $scope.Projects = result.ResponseData;

                // Get data first time with default filter
                userTodoApprovalFactory.getAllTodos(search)
                    .success(function (data) {
                        $scope.TodosList = data.ResponseData;
                        angular.copy($scope.TodosList, $scope.groupedTodoList);
                        $scope.groupedTodoList = $scope.convertTo($scope.groupedTodoList, 'ModifiedDate', true);
                        $('#globalLoader').hide();
                        //getAssigneeList($scope.TodosList);
                        setTimeout(function () {
                            $scope.$apply();
                        }, 1000);
                    })
                    .error(function () {
                        $('#globalLoader').hide();
                        toaster.error("Error", "Some error occured");
                    });
                /////////
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", "Some error occured");
            })
        };

        // #region start
        $scope.TaskStatus = {
            alltask: [
                { id: 1, task: 'Not Processed' },
                { id: 2, task: 'Approved' },
                { id: 3, task: 'Rejected' }

            ]
        };

        $scope.TaskType = {
            alltasktype: [
                { id: '1', type: 'Phone Call' },
                { id: '2', type: 'Research & Development' },
                { id: '3', type: 'Meeting' },
                { id: '4', type: 'Admin Duties' },
                { id: '5', type: 'PMS Task' }
            ]
        };

        $scope.selectedTaskStatus = '0';
        $scope.selectedTaskType = '0';
        //$scope.selectedProject = {};
        $scope.selectedProject = 0;

        $scope.showTodoFilteredByProjects = function (selectedProject) {
            $('#globalLoader').show();
            $scope.groupedTodoList = [];

            search = { 'CompanyId': $scope.selectedCompanyId, 'AssigneeId': sharedService.getUserId(), 'From': $scope.fromDate, 'To': $scope.toDate, 'IsApproved': '', 'ProjectId': $scope.selectedProject, 'TaskStatus': $scope.selectedTaskStatus, 'TaskType': $scope.selectedTaskType };

            userTodoApprovalFactory.getAllTodos(search)
                    .success(function (data) {
                        $scope.TodosList = data.ResponseData;
                        angular.copy($scope.TodosList, $scope.groupedTodoList);
                        $scope.groupedTodoList = $scope.convertTo($scope.groupedTodoList, 'ModifiedDate', true);
                        $('#globalLoader').hide();
                        //getAssigneeList($scope.TodosList);
                        setTimeout(function () {
                            $scope.$apply();
                        }, 1000);
                    })
                    .error(function () {
                        $('#globalLoader').hide();
                        toaster.error("Error", "Some error occured");
                    });

        }

        $scope.showTodoFilteredByStatus = function (selectedTaskStatus) {
            $('#globalLoader').show();
            $scope.groupedTodoList = [];

            search = { 'CompanyId': $scope.selectedCompanyId, 'AssigneeId': sharedService.getUserId(), 'From': $scope.fromDate, 'To': $scope.toDate, 'IsApproved': '', 'ProjectId': $scope.selectedProject, 'TaskStatus': $scope.selectedTaskStatus, 'TaskType': $scope.selectedTaskType };

            userTodoApprovalFactory.getAllTodos(search)
                    .success(function (data) {
                        $scope.TodosList = data.ResponseData;
                        angular.copy($scope.TodosList, $scope.groupedTodoList);
                        $scope.groupedTodoList = $scope.convertTo($scope.groupedTodoList, 'ModifiedDate', true);
                        $('#globalLoader').hide();
                        //getAssigneeList($scope.TodosList);
                        setTimeout(function () {
                            $scope.$apply();
                        }, 1000);
                    })
                    .error(function () {
                        $('#globalLoader').hide();
                        toaster.error("Error", "Some error occured");
                    });
        }

        $scope.showTodoFilteredByTaskType = function (selectedTaskType) {
            $('#globalLoader').show();
            $scope.groupedTodoList = [];

            search = { 'CompanyId': $scope.selectedCompanyId, 'AssigneeId': sharedService.getUserId(), 'From': $scope.fromDate, 'To': $scope.toDate, 'IsApproved': '', 'ProjectId': $scope.selectedProject, 'TaskStatus': $scope.selectedTaskStatus, 'TaskType': $scope.selectedTaskType };

            userTodoApprovalFactory.getAllTodos(search)
                    .success(function (data) {
                        $scope.TodosList = data.ResponseData;
                        angular.copy($scope.TodosList, $scope.groupedTodoList);
                        $scope.groupedTodoList = $scope.convertTo($scope.groupedTodoList, 'ModifiedDate', true);
                        $('#globalLoader').hide();
                        //getAssigneeList($scope.TodosList);
                        setTimeout(function () {
                            $scope.$apply();
                        }, 1000);
                    })
                    .error(function () {
                        $('#globalLoader').hide();
                        toaster.error("Error", "Some error occured");
                    });
        }

        $scope.openPopupSendReport_From = function () {
            $scope.popupSendReport_From.opened = true;
        };

        $scope.openPopupSendReport_To = function () {
            $scope.popupSendReport_To.opened = true;
        };

        $scope.popupSendReport_From = {
            opened: false
        };

        $scope.popupSendReport_To = {
            opened: false
        };


        $scope.AllTodosForPreviewObject = {};
        $scope.Reporter = {};
        $scope.companiesForInvoice = {};

        $scope.ShowSendReportsModal = function () {

            $scope.sendReportsModel.ToDate = $scope.toDate;
            $scope.sendReportsModel.FromDate = $scope.fromDate;

            userTodoApprovalFactory.getUserCompanies(sharedService.getUserId())
           .success(function (data) {
               if (data.success) {
                   $scope.allCompaniesForUser = data.ResponseData;
                   $scope.sendReportsModel.Message = "";
                   $scope.sendReportsModel.companyId = "";
                   $scope.Reporter.projectId = 0;
                   $scope.Reporter.UserId = 0;
                   $('#modalSendReports').modal('show');
               }
               else {
                   toaster.error("Error", message[data.message]);
               }
           })
           .error(function (data) {
               toaster.error("Error", message[data.message]);
           })

        };

        $scope.userCompaniesChange = function (companyId) {
            
            if (companyId == "") 
                companyId = 0;

            if ($scope.sendReportsModel.companyId != undefined || $scope.sendReportsModel.companyId != 0) {
                //$scope.validateMessage = "";
                $("#divCompany").removeClass('ddlerror');
            }

            $scope.selectedCompanyId = companyId;

            userTodoApprovalFactory.getUserProjectsForAllCompanies(sharedService.getUserId(), companyId)
             .success(function (data) {
                 if (data.success) {
                     $scope.allProjectsForUser = data.ResponseData;
                 }
                 else {
                     toaster.error("Error", message[data.message]);
                 }
             })
             .error(function (data) {
                 toaster.error("Error", message[data.message]);
             })

        }


        $scope.userProjectChange = function (companyId, projectId) {
            
            if (projectId == "" || projectId == null ) {
                projectId = 0;
            }
            if (companyId == "" || companyId == null) {
                companyId = 0;
            }
            reportersFactory.getAllProjectUsersForTodoes(sharedService.getUserId(), companyId,projectId, sharedService.getRoleId())
                         .success(function (data) {
                             
                             if (data.success) {
                                 $scope.projectUsersForTodoesObject = data.ResponseData;
                                 $timeout(function () {
                                     $scope.$apply();
                                     bindDropdown_TodoReporters();
                                     $scope.Reporter.UserId = 0;
                                 });

                             }
                             else {
                                 toaster.error("Error", data.message);
                             }
                         })
                         .error(function () {
                             toaster.error("Error", "Some Error Occured !");
                         });

            function bindDropdown_TodoReporters() {
                var oo = $scope.projectUsersForTodoesObject;
                var item = [], i = 0, folo = [];
                for (i = 0; i < oo.length; i++) {
                    var jsonItem = {};
                    jsonItem.Value = oo[i].UserId;
                    jsonItem.Text = oo[i].FirstName + " - " + oo[i].Email ;
                    //jsonItem.ticked = false;
                    item.push(jsonItem);
                }

                $scope.newModel = item;
                $scope.output = [];
            }

        }


        $scope.goToCompanyProfile = function (companyId) {
            $location.path('/company/profile/' + companyId);
        };


        $scope.reporterForInvoiceChange = function (reporterId) {
            if (reporterId != undefined || reporterId != 0) {
                //$scope.validateMessage = "";
                $("#divReporter").removeClass('ddlerror');
            }
        };

        $scope.SendReport = function () {

            if ($scope.sendReportsModel.companyId == undefined || $scope.sendReportsModel.companyId == 0) {
                toaster.warning("Warning", "Please select company");
                //$scope.validateMessage = "Please select company";
                $("#divCompany").addClass('ddlerror');
                return;
            }
            else if ($scope.Reporter.UserId == undefined || $scope.Reporter.UserId == 0) {
                toaster.warning("Warning", "Please select reporter");
                //$scope.validateMessage = "Please select reporter";
                $("#divReporter").addClass('ddlerror');
                return;
            }
            else {
                $('#globalLoader').show();

                var data = { 'fromDate': $scope.sendReportsModel.FromDate, 'toDate': $scope.sendReportsModel.ToDate, 'userId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId, 'Message': $scope.sendReportsModel.Message, 'ReportersID': $scope.Reporter.UserId, 'ProjectId': $scope.Reporter.projectId }
                reportersFactory.getAllTodosForPreview(data)
                            .success(function (data) {
                                if (data.success) {

                                    $scope.AllTodosForPreviewObject = data.ResponseData;

                                    $scope.sendReportsModel.Reporters = $scope.Reporter.UserId;
                                    $scope.sendReportsModel.UserId = sharedService.getUserId();
                                    $scope.sendReportsModel.CompanyId = $scope.selectedCompanyId;
                                    $scope.sendReportsModel.ProjectId = $scope.Reporter.projectId;
                                    $scope.sendReportsModel.AllTodosForPreview = $scope.AllTodosForPreviewObject;

                                    var data = $scope.sendReportsModel;

                                    reportersFactory.SaveInvoice(data)
                                          .success(function (response) {
                                              if (response.ResponseData == "Account Not Verified") {
                                                  toaster.warning({ title: "", body: "Please configure your account first" });
                                                  $('#globalLoader').hide();
                                              }
                                              else if (response.ResponseData == "") {
                                                  toaster.warning({ title: "", body: "There are no Tasks available for invoice" });
                                                  $('#globalLoader').hide();
                                              }
                                              else {
                                                  toaster.success("Success", "Invoice sent");
                                                  //reportersFactory.sendMailToProjectUsersForTodoes(data);
                                                  $('#modalSendReports').modal('hide');
                                                  $('#globalLoader').hide();
                                              }
                                          })
                                          .error(function (response) {
                                              toaster.error("Error", "Some error occured");
                                              $('#globalLoader').hide();
                                          })
                                }
                                else {
                                    toaster.error("Error", data.message);
                                    $('#globalLoader').hide();
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured !");
                                $('#globalLoader').hide();
                            });
            }
        };

        $scope.ShowInvoicePreview = function () {

            if ($scope.sendReportsModel.companyId == undefined || $scope.sendReportsModel.companyId == 0) {
                toaster.warning("Warning", "Please select company");
                //$scope.validateMessage = "Please select company";
                $("#divCompany").addClass('ddlerror');
                return;
            }
            else if ($scope.Reporter.UserId == undefined || $scope.Reporter.UserId == 0) {
                toaster.warning("Warning", "Please select reporter");
                //$scope.validateMessage = "Please select reporter";
                $("#divReporter").addClass('ddlerror');
                return;
            }
            else {
                var data = { 'fromDate': $scope.sendReportsModel.FromDate, 'toDate': $scope.sendReportsModel.ToDate, 'userId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId, 'Message': $scope.sendReportsModel.Message, 'ReportersID': $scope.Reporter.UserId, 'ProjectId': $scope.Reporter.projectId }
                reportersFactory.getAllTodosForPreview(data)
                            .success(function (data) {
                                if (data.success) {
                                    
                                    $scope.AllTodosForPreviewObject = data.ResponseData;
                                }
                                else {
                                    toaster.error("Error", data.message);
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured !");
                            });

                $('#modalSendReports').modal('hide');
                $('#modalInvoicePreview').modal('show');
            }
        }

        $scope.CloseInvoicePreview = function () {

            $('#modalSendReports').modal('show');
            $('#modalInvoicePreview').modal('hide');
        }
        // endregion 

    });
});