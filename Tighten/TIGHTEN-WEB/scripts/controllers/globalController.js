﻿define(['app', 'utilityService'], function (app) {

    app.controller('globalController', function ($scope, $rootScope, apiURL, $filter, $sce, $window, toaster, $timeout, $interval, $http,
        $location, $cookies, $cookieStore, FreelancerAccountFactory, projectsFactory, usersFactory, userMessagesFactory,
        sharedService, teamFactory, globalFactory, utilityService, roleFactory, notificationFactory, rolePermissionFactory, $controller) {
        window.Selectize = require('selectize');
        $scope.imgURL = apiURL.imageAddress;
        $scope.TodoName = "";
        $scope.Description = "";
        $scope.EstimatedTime = "";
        $scope.LoggedTime = "";
        $scope.CurrentSessionTime = "";
        $scope.CompaniesForFreelancer = {};
        $scope.isFreelancer = false;
        $scope.selectedTeamObject = {};
        $rootScope.timeout = null;

        $scope.Permission = {};


        $scope.allAccessLevelFromGlobal = [
            { id: 1, name: "Private", selected: false },
            { id: 2, name: "Semi-Public", selected: false },
            { id: 3, name: "Public", selected: false }
        ];


        $scope.clearAllCheckBoxesFromGlobal = function (accessid) {

            angular.forEach($scope.allAccessLevelFromGlobal, function (id) {
                id.selected = false;
                if (id.id == accessid) {
                    id.selected = true;
                }
            });

        };



        //
        //////////////////
        //var testCtrl1ViewModel = $controller('projectController');
        //testCtrl1ViewModel.showProjectAddUpdateModal();

        /* Create New Project Old Method
        // #region Create New Project  

        $scope.myProjectMember = {};

        $scope.myProjectMember.teamMembers = [];

        $scope.openProjectModal = function () {
            
            angular.forEach($scope.allAccessLevelFromGlobal, function (id) {
                id.selected = false;
                if (id.id == 3) {
                    id.selected = true;
                }
            });

            $scope.projectButton = false;

            $scope.projectAddUpdateModel = {};

            $scope.projectAddUpdateModel.projectMembers = [];

            $scope.myProjectMember.projectMembers = $rootScope.activeTeam.TeamMembers;

            $scope.RoleForGlobal = sharedService.getRoleId();

            $('#modalProjectAddUpdateForGlobal').modal('show');

        }

        // Configuration for adding teammembers from Team Add Update Modal 
        $scope.projectMembersConfig = {
            create: false,
            // maxItems: 1,
            required: true,
            plugins: ['remove_button'],
            valueField: 'Value',
            labelField: 'Text',
            // delimiter: '|',
            placeholder: 'Pick Project Members...',

            render: {
                option: function (item, escape) {
                    var label = item.IsFreelancer;
                    var caption = item.Text;
                    var iconByType = "fa fa-compress";
                    if (label == true) {
                        iconByType = "fa fa-expand";
                    }

                    return '<div>' +
                    '<span >  <i class="' + iconByType + '" aria-hidden="true"></i>   </span> ' +
                    (caption ? '<span>' + escape(caption) + '</span>' : '') +
                    '</div>';

                }
            }
        }

       

        // Add new Project 
        $scope.addNewProjectFromGlobal = function (isProjectNameInvalid, isProjectDescriptionInvalid, event, projectAddUpdateForm) {

            if (isProjectNameInvalid || isProjectDescriptionInvalid) {
                projectAddUpdateForm.projectName.$dirty = true;
                projectAddUpdateForm.projectDescription.$dirty = true;
                projectAddUpdateForm.projectHours.$dirty = true;
                projectAddUpdateForm.projectBudget.$dirty = true;
            }
            else {

                $('#globalLoader').show();
                var accessLevel
                angular.forEach($scope.allAccessLevelFromGlobal, function (id) {
                    if (id.selected == true) {
                        accessLevel = id.id;
                    }
                });
                if (accessLevel == undefined) {
                    toaster.warning("Warning", "Please select access level for project");
                    $('#globalLoader').hide();
                    return;
                }
                $scope.projectAddUpdateModel.teamId = $rootScope.activeTeam.TeamId;
                $scope.projectAddUpdateModel.accessLevel = accessLevel;
                var NewTeamObj = { 'ProjectModel': $scope.projectAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': sharedService.getCompanyId() }
                projectsFactory.insertProject(NewTeamObj)
                          .success(function (data) {
                              if (data.ResponseData == 'ProjectAddedSuccess') {
                                  if (data.success) {
                                      $scope.$emit('showMenu', []);

                                      var location = window.location.href;
                                      var currentLocation = location.split("#");
                                      if (currentLocation[1] == "/project") {

                                            //Getting User Owned Projects 
                                          // 'leftTeamDropdownChange' , This method is for update project listing on Project Screen when 
                                          //    Team Dropdown is changed  .....
                                          //    Here it is called to update Project Listing When new project is created from the Create New Project Link in Left side menu  

                                          $scope.$broadcast('leftTeamDropdownChange');

                                      }


                                      $timeout(function () {
                                          $scope.$apply();
                                          $('#modalProjectAddUpdateForGlobal').modal('hide');
                                          $('#globalLoader').hide();
                                          toaster.success("Success", message[data.ResponseData]);

                                          projectAddUpdateForm.projectName.$dirty = false;
                                          projectAddUpdateForm.projectDescription.$dirty = false;
                                          projectAddUpdateForm.projectHours.$dirty = false;
                                          projectAddUpdateForm.projectBudget.$dirty = false;

                                      });
                                  }
                              }
                              else {
                                  toaster.warning("Warning", message[data.ResponseData]);
                                  $('#globalLoader').hide();
                              }

                          }).
                          error(function (error) {
                              $scope.status = 'Unable to insert user: ' + error.message;
                          });

            }
        };

        // #endregion Create New Project 
        */

        /* Go to addUpdateProject On Click on Create New Project in side menu */
        $scope.projectAddUpdateClick = function () {

            $location.path('/addupdateproject/0');
        };


        $scope.showTimerDiv = false;
        $scope.TimerValue = "";
        $scope.Interval;
        //alert('start');
        $scope.$on('showMenu', function (event, args) {
            $timeout(function () {

                // to check if has valid subscription
                var obj = $cookieStore.get('sharedService')
                if (obj.IsSubscriptionsEnds == true) {
                    $location.path('/subscription');
                }
                $('#menuTest').show();
                $('#sideMenu').parent().parent().show();
                $('#headerMenu').show();
                $('#sideMenu').show();
                $scope.showSideMenu = false;
                $scope.headerMenu = false;
                if (sharedService.getUserId() != undefined) {

                    if (sharedService.getIsFreelancer() == true) {
                        FreelancerAccountFactory.getCompaniesForFreelancer(sharedService.getUserId())
                            .success(function (data) {
                                debugger;
                                if (data.ResponseData.length == 0) {
                                    sharedService.setCompanyId(0);
                                    $scope.selectedCompany = 0;
                                    $scope.isFreelancer = sharedService.getIsFreelancer();
                                    getUserDetail();
                                    showSideMenuRolewise();
                                    getRolewiseInternalPermissions();
                                }
                                else {
                                    $scope.CompaniesForFreelancer = data.ResponseData;
                                    $scope.selectedCompany = $scope.CompaniesForFreelancer[0].CompanyId;
                                    sharedService.setCompanyId($scope.CompaniesForFreelancer[0].CompanyId);
                                    FreelancerAccountFactory.GetFreelancerRoleForCompany(sharedService.getUserId(), sharedService.getCompanyId())
                                        .success(function (response) {
                                            debugger;
                                            sharedService.setRoleId(response.ResponseData.RoleId);
                                            //if (sharedService.getRoleId() == "32") {
                                            //    $scope.IsVisibleBothMenu = false;
                                            //}
                                            //else {
                                            //    $scope.IsVisibleBothMenu = true;
                                            //}
                                            $scope.isFreelancer = sharedService.getIsFreelancer();
                                            getUserDetail();
                                            showSideMenuRolewise();
                                            getRolewiseInternalPermissions();
                                            $('#globalLoader').hide();
                                        })
                                        .error(function () {
                                            toaster.error("Error", "1Some Error Occured !");
                                        })
                                }
                            })
                            .error(function (data) {
                                toaster.error("Error", "2Some Error Occured !");
                            })
                    }
                    else {
                        $scope.isFreelancer = false;
                        getUserDetail();
                        showSideMenuRolewise();
                        getRolewiseInternalPermissions();
                    }
                }


                //$('.static-sidemenu').slimScroll({
                //    height: '90%',
                //    railVisible: true,
                //    alwaysVisible: true
                //});

                //$('.static-sidemenu').css('width', '17%');
                //$('.static-sidemenu').css('height', '95%');

                $('#sideMenu').parent().slimScroll({
                    height: '33%',
                    railVisible: true,
                    alwaysVisible: false
                });

                $('#sideMenu').parent().css('width', '17%');
                $('#sideMenu').parent().css('height', '92%');
                $('#menuTest .slimScrollDiv').css('position', '');
                $('#menuTest .slimScrollBar').css('margin-top', '60px');
                $('#menuTest .slimScrollBar').css('border', '1px solid #3facff');


            }, 1000);
            //alert('alert global');
        });
        $rootScope.$on('showHeaderMenu', function (event, args) {

            $rootScope.headerMenu = true;
            $('#headerMenu').show();
        })

        $scope.IsVisibleBothMenu = true;
        $scope.IsVisibleGeneral = true;
        $scope.IsVisibleVendor = false;
        $scope.ShowHide = function (typeofuser) {
            debugger;
            if (typeofuser == 'General') {
                //$scope.IsVisibleGeneral = ($scope.IsVisibleGeneral == true) ? false : true;
                //$scope.IsVisibleVendor = ($scope.IsVisibleGeneral == true) ? false : true;
                $scope.IsVisibleGeneral = true;
                $scope.IsVisibleVendor = true;
                $('#globalLoader').show();

                roleFactory.showSideMenuRolewise(1, 'de151472-1568-47d8-9049-1166a9e64174', 1) //'de151472-1568-47d8-9049-1166a9e64174'

                    .success(function (data) {
                        debugger;

                        $scope.AllSideMenuOptionRolewise = data.ResponseData;
                        //alert($scope.AllSideMenuOptionRolewise.IsAccountManager);
                        $('#globalLoader').hide();
                    })
                    .error(function (data) {
                        $('#globalLoader').hide();
                    })
            }
            else if (typeofuser == 'Vendor') {
                //$scope.IsVisibleVendor = ($scope.IsVisibleVendor == true) ? false : true;
                //$scope.IsVisibleGeneral = ($scope.IsVisibleVendor == true) ? false : true;
                $scope.isvisiblegeneral = false;
                $scope.isvisiblevendor = true;
                roleFactory.showSideMenuRolewise(sharedService.getRoleId(), sharedService.getUserId(), sharedService.getCompanyId())
                    .success(function (data) {
                        debugger;
                        console.log(data.ResponseData);
                        $scope.AllSideMenuOptionRolewise = data.ResponseData;
                        $('#globalLoader').hide();
                    })
                    .error(function (data) {
                        $('#globalLoader').hide();
                    })
            }

        }



        // show side menu according to role
        function showSideMenuRolewise() {

            $('#globalLoader').show();
           // alert(sharedService.getRoleId()+" -- "+ sharedService.getUserId()+" -- "+ sharedService.getCompanyId())
            roleFactory.showSideMenuRolewise(sharedService.getRoleId(), sharedService.getUserId(), sharedService.getCompanyId())
                .success(function (data) {
                    debugger;

                    $scope.AllSideMenuOptionRolewise = data.ResponseData;
                    //alert($scope.AllSideMenuOptionRolewise.IsAccountManager);
                    $('#globalLoader').hide();
                })
                .error(function (data) {
                    $('#globalLoader').hide();
                })

        }

        //Get Internal Permissions For Current User role
        function getRolewiseInternalPermissions() {
           // alert(sharedService.getRoleId() + " - " + sharedService.getCompanyId());
            rolePermissionFactory.getInternalPermissionsForCurrentUser(sharedService.getRoleId(), sharedService.getCompanyId())
                .success(function (permissionResponse) {
                    sharedService.setInternalPermissionInShared(permissionResponse.ResponseData);
                    $scope.Permission = permissionResponse.ResponseData;
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                })

        }



        $scope.selectedCompanyChanged = function (selectedCompany) {

            sharedService.setCompanyId(selectedCompany);
            $scope.selectedCompany = selectedCompany;
            FreelancerAccountFactory.GetFreelancerRoleForCompany(sharedService.getUserId(), sharedService.getCompanyId())
                .success(function (data) {

                    sharedService.setRoleId(data.ResponseData.RoleId);
                    ////sharedService.setRoleId(data.ResponseData.RoleId);
                    //if (sharedService.getRoleId() == "32") {
                    //    $scope.IsVisibleBothMenu = false;
                    //}
                    //else {
                    //    $scope.IsVisibleBothMenu = true;
                    //}
                    getUserDetail();
                    $scope.$broadcast('leftCompanyDropdownChange', { 'CompanyId': selectedCompany });
                    $('#globalLoader').hide();
                })
                .error(function () {
                    toaster.error("Error", "4Some Error Occured !");
                })
            $timeout(function () {
                $scope.$apply();
            }, 1000);
        };



        // bind teams on update from other controller
        $scope.$on('bindTeamDropdown', function (event, args) {
            //alert('team updated');
            $timeout(function () {
                if (sharedService.getUserId() != undefined) {
                    // getUserTeams();
                }
            }, 1000);
        });

        $scope.$on('bindCompanyDetail', function (event, args) {
            //alert('team updated');
            $timeout(function () {
                if (sharedService.getUserId() != undefined) {
                    getCompanyDetail();
                }
            }, 1000);
        });

        $scope.$on('bindUserDetail', function (event, args) {
            //alert('team updated');
            $timeout(function () {
                if (sharedService.getUserId() != undefined) {
                    getUserDetail();
                }
            }, 1000);
        });



        $scope.CompanyObject = {};
        $rootScope.CompanyObject = {};
        $rootScope.TeamsObject = {};
        // call when the controller loaded its data
        //getUserDetail();

        /*  CHange Selected Team */
        $scope.changeSelectedTeam = function (activeTeamObject) {


            if (activeTeamObject != null) {
                $('#globalLoader').show();

                $rootScope.activeTeam = activeTeamObject;
                $scope.selectedTeamObject.activeTeam = activeTeamObject;
                $scope.teamName = activeTeamObject.TeamName;
                var Users = {};
                Users.TeamId = activeTeamObject.TeamId;
                Users.updateflag = 2;
                Users.userId = sharedService.getUserId();

                usersFactory.updateUser(Users).success(function () {

                    getProjects();

                    $timeout(function () {
                        //$scope.$apply();
                        $('#globalLoader').hide();
                        $scope.$broadcast('leftTeamDropdownChange', { 'TeamId': activeTeamObject.TeamId });
                    });
                });
            }
        };

        /*  Getting projects related to Selected Team */
        function getProjects() {

            if ($scope.selectedTeamObject.activeTeam == null) {
                $scope.selectedTeamObject.activeTeam = [];
            }
            if ($scope.selectedTeamObject.activeTeam.TeamId == undefined) {
                $scope.selectedTeamObject.activeTeam.TeamId = sharedService.getDefaultTeamId();
            }
            projectsFactory.getProjects(sharedService.getUserId(), $scope.selectedTeamObject.activeTeam.TeamId, sharedService.getCompanyId(), sharedService.getIsFreelancer())
                .success(function (data) {
                    if (data.success) {

                        $scope.projects = data.ResponseData;
                        $timeout(function () {
                            //$scope.$apply();
                            $('#globalLoader').hide();
                        });
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                }).error(function () {
                    toaster.error("Error", "5Some Error Occured !");
                });
        }


        ///* SHowing Add Update Project Modal Pop Up*/
        //$scope.showProjectAddUpdateModal = function () {
        //        $scope.projectButton = false;
        //        $scope.projectAddUpdateModel = {};
        //        $scope.projectAddUpdateModel.projectMembers = [];
        //        //$scope.projectAddUpdateForm.projectName.$dirty = false;
        //        //$scope.projectAddUpdateForm.projectDescription.$dirty = false;
        //        //$scope.projectAddUpdateModel.projectMembers = $rootScope.activeTeam.TeamMembers;
        //        $scope.myProjectMember.projectMembers = $rootScope.activeTeam.TeamMembers;
        //        $('#modalProjectAddUpdate').modal('show');
        //};


        /* SHowing Add Update Project Modal Pop Up*/
        $scope.showProjectAddUpdateModal = function () {


            $scope.$broadcast('showingAddUpdateProjectModalPopUpUsingEmit', []);

            //var newLocation = '/project' ;
            //$location.path(newLocation);

            //setTimeout(function () {
            //    $scope.$broadcast('showingAddUpdateProjectModalPopUpUsingEmit', []);
            //}, 1000);



        }

        // get the user necessary detail
        function getUserDetail() {

            function hideloader() {
                //closeCentreSections();
                //$('#divMasterLoader').hide();
                $scope.selectedNavigation = "MyDashboard";
                //$('#divMyDashboard').show();
                $scope.setOverFlowY = "auto !important";
            };
            /*  Fetching User Details */
            debugger;
            usersFactory.getUser(sharedService.getUserId(), sharedService.getCompanyId())

                .success(function (data) {

                    if (data.success) {
                        debugger;
                        if (data.ResponseData) {

                            //$scope.NotifyCount = data.ResponseData.NotificationCount == 0 ? null : data.ResponseData.NotificationCount;
                            //$scope.InboxCount = data.ResponseData.inboxCount == 0 ? null : data.ResponseData.inboxCount;
                            $scope.hideNotifyCount = (data.ResponseData.NotificationCount == undefined || data.ResponseData.NotificationCount == null) ? 0 : data.ResponseData.NotificationCount == 0 ? false : true;
                            $scope.hideInboxCount = (data.ResponseData.inboxCount == undefined || data.ResponseData.inboxCount == null) ? 0 : data.ResponseData.inboxCount == 0 ? false : true;

                            $rootScope.NotificationCount = data.ResponseData.NotificationCount == undefined ? 0 : data.ResponseData.NotificationCount == null ? 0 : data.ResponseData.NotificationCount;
                            $rootScope.InboxCount = data.ResponseData.InboxCount == undefined ? 0 : data.ResponseData.InboxCount == null ? 0 : data.ResponseData.InboxCount;

                            // adding to the shared service
                            sharedService.setNotificationCount($rootScope.NotificationCount);
                            sharedService.setInboxCount($rootScope.InboxCount);
                            sharedService.setRoleId(data.ResponseData.RoleId);
                            debugger;
                            ////sharedService.setRoleId(response.ResponseData.RoleId);
                            //if (sharedService.getRoleId() == "32") {
                            //    $scope.IsVisibleBothMenu = false;
                            //}
                            //else {
                            //    $scope.IsVisibleBothMenu = true;
                            //}
                            $scope.CompanyObject.companyId = data.ResponseData.CompanyId;
                            $scope.CompanyObject.companyName = data.ResponseData.CompanyName;

                            $rootScope.CompanyObject.companyId = data.ResponseData.CompanyId;
                            $rootScope.CompanyObject.companyName = data.ResponseData.CompanyName;


                            $scope.IsAdminLoggedIn = data.ResponseData.IsAdminLoggedIn;
                            $scope.SendNotification = data.ResponseData.SendNotificationEmail;

                            $scope.MyProfileObject = data.ResponseData;
                            $rootScope.MyProfileObject = data.ResponseData;

                            $scope.teamName = data.ResponseData.teamName;
                            $scope.TeamId = data.ResponseData.TeamId;
                            $timeout(function () {
                                //$scope.$apply();
                                /* Fetching User Teams  */

                                //teamFactory.getLoggedInUserTeams(sharedService.getUserId(), sharedService.getCompanyId()).success(function (data) {
                                //    if (data.success) {

                                //        $scope.TeamsObject = data.ResponseData;
                                //        $scope.selectedTeamObject.activeTeam = $scope.TeamsObject[0];
                                //        if ($scope.selectedTeamObject.activeTeam != undefined) {
                                //            sharedService.setDefaultTeamId($scope.selectedTeamObject.activeTeam.TeamId);
                                //            $rootScope.activeTeam = $scope.selectedTeamObject.activeTeam;
                                //            $rootScope.TeamsObject = $scope.TeamsObject;
                                //        }

                                //        $timeout(function () {
                                //            //$scope.$apply();
                                //            /* Getting User Projects for Selected Team */
                                //            getProjects();
                                //            //projectsFactory.getProjects(sharedService.getUserId(), $scope.selectedTeamObject.activeTeam.TeamId)
                                //            //    .success(function (data) {
                                //            //        
                                //            //        if (data.success) {
                                //            //            $scope.projects = data.ResponseData;
                                //            //        }
                                //            //        else {
                                //            //            toaster.error("Error", data.message);
                                //            //        }
                                //            //    })
                                //            //    .error(function () {
                                //            //        toaster.error("Error", "Some Error Occured !");
                                //            //    });
                                //        });
                                //    }
                                //    else {
                                //        toaster.error("Error", data.message);
                                //    }
                                //}).error(function () {
                                //    toaster.error("Error", "Some Error Occured!");
                                //});
                            });

                        }

                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "6Some Error Occured !");
                });
        }


        // //on load
        angular.element(document).ready(function () {
           // alert(sharedService.getRoleId());
            //if (sharedService.getUserId() != undefined) {
            //    getUserDetail();
            //}
        });
        //$scope.$on('loadSideMenu', function (event, args) {
        //    alert('process');
        //    getUserDetail();
        //});


        // Checking wheather the input character is number or not 
        $scope.isNumber = function (evnt) {
            if (evnt.keyCode > 47 && evnt.keyCode < 58 || evnt.charCode > 47 && evnt.charCode < 58 || evnt.charCode > 0 && event.charCode < 15) {
                return true;
            }
            else {
                evnt.preventDefault();
                return false;
            }
        };


        $scope.showTodoManagementSection = function (projectItem) {
            $scope.activeProject = projectItem;
            $rootScope.activeProject = projectItem;
            $scope.myTodo = true;
            var newLocation = '/todo/' + projectItem.ProjectId + "/" + $scope.selectedTeamObject.activeTeam.TeamId;
            $location.path(newLocation);
        }

        $scope.resetNotifications = function () {

            notificationFactory.resetNotification(sharedService.getUserId())
                .success(function (result) {
                    if (result.ResponseData != null) {
                        $rootScope.NotificationCount = 0;
                    }
                })
                .error(function () { })

        }

        /*Show Company profile*/

        $scope.showCompanyProfile = function () {
            //alert('company');
            $location.path('/company/profile/' + sharedService.getCompanyId());
        }
        /*Show user profile*/
        $scope.showUserProfile = function () {
            $location.path('/user/profile/' + sharedService.getUserId());
        }
        $scope.showOtherUserProfile = function (id) {
            $location.path('/user/profile/' + id);
        }
        /*Show user management*/
        $scope.showUserManagementSection = function () {
            $location.path('/user/manage');
        }

        //Common finctionality for the discription in notifications/////
        //$rootScope.closeRightSections = function () {

        //    $('#divToDoDescriptionSection').hide();
        //    $('#divEventDescriptionSection').hide();
        //    $('#divProjectOverview').hide();
        //    $('#divUserFeedbackDescriptionSection').hide();
        //    $('#divTeamDescriptionSection').hide();
        //    $('#divProjectDescriptionSection').hide();

        //}

        /////////////////////////////////////////////////////////////////
        /*************Header button functionality***********************/
        /////////////////////////////////////////////////////////////////

        // logout button on in the header
        $scope.logout = function () {
            $cookieStore.put('globals', {});
            $cookieStore.put('sharedService', {});
            $('#headerMenu').hide();
            $('#sideMenu').hide();
            $rootScope.showSideMenu = true;
            $rootScope.headerMenu = true;


            if (angular.isDefined($rootScope.timeout)) {
                $interval.cancel($rootScope.timeout);
            }

            $timeout(function () {
                $('#sideMenu').parent().parent().hide();
                $scope.$apply();
                $location.path('/login');
            }, 1000);
        }



        function getUserTeams() {
            /* Fetching User Teams  */

            teamFactory.getLoggedInUserTeams(sharedService.getUserId(), sharedService.getCompanyId()).success(function (data) {
                if (data.success) {
                    $scope.TeamsObject = data.ResponseData;
                    $scope.selectedTeamObject.activeTeam = $scope.TeamsObject[0];
                    sharedService.setDefaultTeamId($scope.selectedTeamObject.activeTeam.TeamId);
                    $rootScope.activeTeam = $scope.selectedTeamObject.activeTeam;
                    $rootScope.TeamsObject = $scope.TeamsObject;
                    $timeout(function () { $rootScope.$apply() }, 1000);
                }
                else {
                    toaster.error("Error", data.message);
                }
            }).error(function () {
                toaster.error("Error", "7Some Error Occured!");
            });
        }



        //// getMessagesCount 
        //$scope.$on('getMessagesCount', function (event, args) {

        //$scope.timeout = $interval(function () {
        //    userMessagesFactory.getMessagesCount(sharedService.getUserId())
        //         .success(function (data) {

        //             $rootScope.InboxCount = data.ResponseData == undefined ? 0 : data.ResponseData == null ? 0 : data.ResponseData;
        //             sharedService.setNewMessagesCount($rootScope.InboxCount);
        //             $scope.InboxCount = $rootScope.InboxCount;
        //         });
        //}, 5000);

        //});



        // Update the side panel info when user changes the company profile 
        function getCompanyDetail() {
            function hideloader() {
                //closeCentreSections();
                //$('#divMasterLoader').hide();
                $scope.selectedNavigation = "MyDashboard";
                //$('#divMyDashboard').show();
                $scope.setOverFlowY = "auto !important";
            };
            /*  Fetching User Details */
            usersFactory.getUser(sharedService.getUserId(), sharedService.getCompanyId())
                .success(function (data) {
                    if (data.success) {
                        if (data.ResponseData != null) {
                            $scope.hideNotifyCount = data.ResponseData.NotificationCount == undefined ? 0 : data.ResponseData.NotificationCount == 0 ? false : true;
                            $scope.hideInboxCount = data.ResponseData.inboxCount == undefined ? 0 : data.ResponseData.inboxCount == 0 ? false : true;

                            $rootScope.NotificationCount = data.ResponseData.NotificationCount == undefined ? 0 : data.ResponseData.NotificationCount == null ? 0 : data.ResponseData.NotificationCount;
                            $rootScope.InboxCount = data.ResponseData.InboxCount == undefined ? 0 : data.ResponseData.InboxCount == null ? 0 : data.ResponseData.InboxCount;
                            // adding to the shared service
                            sharedService.setNotificationCount($rootScope.NotificationCount);
                            sharedService.setInboxCount($rootScope.InboxCount);
                            $scope.CompanyObject.companyId = data.ResponseData.CompanyId;
                            $scope.CompanyObject.companyName = data.ResponseData.CompanyName;
                            $rootScope.CompanyObject.companyId = data.ResponseData.CompanyId;
                            $rootScope.CompanyObject.companyName = data.ResponseData.CompanyName;
                            $scope.IsAdminLoggedIn = data.ResponseData.IsAdminLoggedIn;
                            $scope.SendNotification = data.ResponseData.SendNotificationEmail;
                            $timeout(function () {
                                $scope.$apply();
                            });
                        }
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "8Some Error Occured !");
                });
        }

        // this is to get the detail of the running task
        function getRunningTaskDetail() {

        }
        var count = 0;
        // update the timet values
        function startTime(minutes) {
            //
            count = minutes;
            $scope.TimerValue = utilityService.ConvertMinutesToFormatedTimeLogString(minutes)
            $scope.Interval = $interval(TimerCallbackFunction, 60000);
        }
        function TimerCallbackFunction() {
            count++;
            FormatedString = utilityService.ConvertMinutesToFormatedTimeLogString(count)
            $scope.TimerValue = FormatedString;
        }
        // getRunningTaskDetail();
        $scope.$on('stopTimer', function (event, args) {
            $interval.cancel($scope.Interval);
            $scope.showTodoTaskTimer = false;
        });
        // This is the timer which will start the timer 
        $scope.$on('startTimer', function (event, args) {
            //
            $timeout(function () {
                if (sharedService.getUserId() != undefined) {
                    globalFactory.getRunningTaskDetail(sharedService.getUserId())
                        .success(function (response) {
                            //
                            if (response.ResponseData) {
                                //
                                $scope.showTodoTaskTimer = true;
                                $scope.TodoName = response.ResponseData.TodoName;
                                $scope.Description = response.ResponseData.Description;
                                $scope.EstimatedTime = response.ResponseData.EstimatedTime;
                                $scope.LoggedTime = response.ResponseData.LoggedTime == null ? '0m' : response.ResponseData.LoggedTime;
                                $scope.CurrentSessionTime = response.ResponseData.CurrentSessionTime;
                                startTime(parseInt($scope.CurrentSessionTime));
                            }
                            else {
                                $scope.showTodoTaskTimer = false;
                            }
                            //
                        })
                        .error(function () {

                        });
                }
            }, 1000);
        });

        // not using
        // this method is moved to the utility service
        // convert the minutes to the Hour/ Minutes Format
        function ConvertMinutesToFormatedTimeLogString(TotalMinutes) {
            //
            var FormatedString = "";
            var Hours = TotalMinutes / 60;
            var Minutes = TotalMinutes % 60;
            Hours = parseInt(Hours);
            Minutes = parseInt(Minutes);
            if (Hours == 0)
                FormatedString = Minutes + "m";
            else if (Hours != 0 && Minutes == 0)
                FormatedString = Hours + "h ";
            else
                FormatedString = Hours + "h " + Minutes + "m";
            return FormatedString;
        }


    });
});

