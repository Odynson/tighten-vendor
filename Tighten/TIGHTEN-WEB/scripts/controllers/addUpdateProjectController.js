﻿
define(['app'], function (app) {
    app.controller("addUpdateProjectController", function ($scope, $rootScope, apiURL, $stateParams, $filter, $timeout, $http, $location, $upload, sharedService, $confirm, toaster, projectsFactory, teamFactory, FreelancerAccountFactory, invitationFactory, roleFactory) {
        window.Selectize = require('selectize');
        $scope.testing = "test";
        //alert("alert");
        var baseurl = apiURL.baseAddress;
        $scope.imgURL = apiURL.imageAddress;

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu');
            $rootScope.activeTeam = [];
        }


        $scope.projectAddUpdateModel = {};
        $scope.projectAddUpdateModel.TeamMembers = [];
        $scope.projectAddUpdateModel.ProjectAttachments = [];
        $scope.myProjectMember = {};
        $scope.myProjectMember.teamMembers = [];
        $scope.addClientFeedbackModel = {};
        $scope.AllProjectOfCompanyObject = {};
        $scope.FreelancerModel = {};
        $scope.FreelancerObject = {};
        $scope.teamNameForTitle = "";
        $scope.freelancerDetails = [];
        $scope.internalUserDetails = [];
        $scope.showAttachmentDescription = false;
        $scope.projectMode = "add";
        $scope.projectPhase = "Add Project";
        $scope.isBackButtonClick = false;
        $scope.addFreelancer = false;

        //$scope.Permission = {};
        bindFreelancerDdl();

        $scope.isFreelancer = sharedService.getIsFreelancer();

        var range = [];
        for (var i = 1; i < 6; i++) {
            range.push(i);
        }
        $scope.availableRange = range;

        $scope.allAccessLevel = [
                     { id: 1, name: "Private", selected: false },
                     { id: 2, name: "Semi-Public", selected: false },
                     { id: 3, name: "Public", selected: false }
        ];

        $scope.clearAllCheckBoxes = function (accessid) {
            debugger;
            angular.forEach($scope.allAccessLevel, function (id) {
                id.selected = false;
                if (id.id == accessid) {
                    id.selected = true;
                }
            });
        };

        $scope.selectedCompanyId = sharedService.getCompanyId();
        $scope.projectAddUpdateForm = {};
        $scope.Role = sharedService.getRoleId();
       

        /*  Getting All Possible Role Present For Particular Company   */
        roleFactory.getRoles(sharedService.getCompanyId(), sharedService.getRoleId())
              .success(function (data) {
                  if (data.success) {
                      $scope.RoleObject = data.ResponseData;
                      //$scope.RoleObjectForFreelancer = $filter('filter')(data.ResponseData, [{ 'RoleId': 2 }, { 'RoleId': 3 }], true)
                      var roleObjectForFreelancer = [];
                      for (var i = 0; i < $scope.RoleObject.length; i++) {
                          if ($scope.RoleObject[i].RoleId != 1 && $scope.RoleObject[i].RoleId != 5) {
                              roleObjectForFreelancer.push($scope.RoleObject[i]);
                          }
                      }
                      $scope.RoleObjectForFreelancer = roleObjectForFreelancer;
                  }
              })
              .error(function () {
                  toaster.error("Error", "Some error occured");
              });



        /* Showing Project Management Section to see the information*/
        $scope.showProjectManagementSection = function (projectId) {
            /* Getting User Owned Projects */
            $('#globalLoader').show();
            debugger;
            alert();
            projectsFactory.getProject(projectId)
                .success(function (data) {
                    if (data.success) {

                        $scope.UserOwnedProjectObject = data.ResponseData;
                        $scope.editProject($scope.UserOwnedProjectObject);
                    }
                    else {
                        toaster.error("Error", data.message);
                        $('#globalLoader').hide();
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                    $('#globalLoader').hide();
                });
        };



        /* SHowing Add Update Project Modal Pop Up*/
        $scope.showProjectAddUpdateModal = function () {

            $scope.projectMode = "add";
            angular.forEach($scope.allAccessLevel, function (id) {
                if (id.id == 3) {
                    id.selected = true;
                }
            });

            $scope.showProjectWithFreelancers = false;
            // To hide  Attachment Description field
            $scope.showAttachmentDescription = false;
            $scope.projectButton = false;
            $scope.projectAddUpdateModel = {};
            $scope.projectAddUpdateModel.projectMembers = [];
            $scope.projectAddUpdateModel.freelancersMembers = [];
            
            $scope.myProjectMember.projectMembers = $filter('filter')($scope.TeamsObject.activeTeam.TeamMembers, { IsFreelancer: false }, true);
            $scope.myProjectMember.freelancersOnly = $filter('filter')($scope.TeamsObject.activeTeam.TeamMembers, { IsFreelancer: true }, true);

            if ($scope.TeamsObject.activeTeam.TeamName == "My Personal") {
                $("input[name='projectBudget']").attr("disabled", "disabled");
            }
            else {
                $("input[name='projectBudget']").removeAttr("disabled");
            }
        };


        /* ------------- Manage Projects With Freelancers Starts Here -----------------*/

        $scope.showManageProjectsWithFreelancerModal = function () {

            $('#globalLoader').show();
            $scope.FreelancerModel = {};
            $scope.newModel = {};
            $("input[name='UserRate']").removeClass("inputError");
            $("input[name='WeeklyLimit']").removeClass("inputError");

            projectsFactory.GetAllProjectsOfCompany(sharedService.getCompanyId(), $scope.selectedTeamId)
                 .success(function (data) {
                     $scope.AllProjectOfCompanyObject = data.ResponseData;
                     $('#modalManageProjectsWithFreelancer').modal('show');
                     $('#globalLoader').hide();
                 })
                 .error(function (data) {
                     toaster.error("Error", "Some Error Occured!");
                     $('#globalLoader').hide();
                 })
        }

        $scope.getFreelancerForSelectedProject = function (ProjectId) {

            $('#globalLoader').show();
            projectsFactory.getAllActiveFreelancerOfProject(ProjectId)
                 .success(function (ProjectsData) {
                     $scope.FreelancerObject = ProjectsData.ResponseData;
                     bindFreelancerDdl();
                     $('#globalLoader').hide();
                 })
                 .error(function (data) {
                     toaster.error("Error", "Some Error Occured!");
                     $('#globalLoader').hide();
                 })
        }

        //$scope.isNumber = function (evnt) {
        //    if (evnt.keyCode > 47 && evnt.keyCode < 58 || evnt.charCode > 47 && evnt.charCode < 58) {
        //        return true;
        //    }
        //    else {
        //        evnt.preventDefault();
        //        return false;
        //    }
        //};


        $scope.saveProjectOptionsForFreelancer = function (isUserRateInvalid, isWeeklyLimitInvalid, manageProjectsWithFreelancerForm) {

            if (isUserRateInvalid || isWeeklyLimitInvalid) {
                manageProjectsWithFreelancerForm.UserRate.$dirty = true;
                manageProjectsWithFreelancerForm.WeeklyLimit.$dirty = true;
            }
            else {

                $('#globalLoader').show();
                var outputModel = $scope.newModel;
                var i = 0;
                var AllFreelancerId = [];
                for (i; i < outputModel.length; i++) {
                    if (outputModel[i].false == true) {
                        AllFreelancerId.push(outputModel[i].Value);
                    }
                }

                if (AllFreelancerId.length == 0) {
                    toaster.error("Error", "Freelancer is not selected");
                    $('#globalLoader').hide();
                    return;
                }

                var model = {};
                model = $scope.FreelancerModel;
                model.FreelancerIdList = AllFreelancerId;


                projectsFactory.saveProjectOptionsForFreelancer(model)
                     .success(function (data) {
                         if (data.success == true) {
                             toaster.success("Success", data.ResponseData);
                             $('#modalManageProjectsWithFreelancer').modal('hide');
                             $('#globalLoader').hide();

                             manageProjectsWithFreelancerForm.UserRate.$dirty = false;
                             manageProjectsWithFreelancerForm.WeeklyLimit.$dirty = false;
                         }
                         else {
                             toaster.warning("Warning", "Some Error Occured!");
                             $('#globalLoader').hide();
                         }
                     })
                     .error(function (data) {
                         toaster.error("Error", "Some Error Occured!");
                         $('#globalLoader').hide();
                     })
            }
        }


        function bindFreelancerDdl() {

            if ($scope.FreelancerObject != undefined) {
                var Freelancers = $scope.FreelancerObject;
                var allFreelancers = [], i = 0;
                for (i = 0; i < Freelancers.length; i++) {
                    var jsonItem = {};
                    jsonItem.Value = Freelancers[i].UserId;
                    jsonItem.Text = Freelancers[i].FreelancerName;
                    jsonItem.ticked = false;
                    if (Freelancers[i].isTicked > 0) {
                        jsonItem.false = true;
                    }
                    else {
                        jsonItem.false = false;
                    }
                    allFreelancers.push(jsonItem);
                }
                $scope.newModel = allFreelancers;
                $scope.output = [];
            }
        };

        /* ------------- Manage Projects With Freelancers Ends Here ------------- */


        $scope.clickNext = function (isProjectNameInvalid, isProjectDescriptionInvalid, isProjectHoursInvalid, isProjectBudgetInvalid, event, projectAddUpdateForm) {

            if (isProjectNameInvalid || isProjectDescriptionInvalid || isProjectHoursInvalid || isProjectBudgetInvalid) {
                projectAddUpdateForm.projectName.$dirty = true;
                projectAddUpdateForm.projectDescription.$dirty = true;
                projectAddUpdateForm.projectHours.$dirty = true;
                projectAddUpdateForm.projectBudget.$dirty = true;
            }
            else {
                $('#globalLoader').show();
                var isRoleSelected = false;
                // Loop through All selected Internal Users to check wheather role is selected for everyone or not
                angular.forEach($scope.internalUserDetails, function (usr) {
                    if (usr.RoleId < 1 || usr.RoleId == undefined) {
                        isRoleSelected = true;
                        return;
                    }
                });

                // Loop through All selected Freelancers to check wheather role is selected for everyone or not
                if (isRoleSelected == false) {
                    angular.forEach($scope.freelancerDetails, function (usr) {
                        if (usr.FreelancerRoleId < 1 || usr.FreelancerRoleId == undefined) {
                            isRoleSelected = true;
                            return;
                        }
                    });
                }

                if (isRoleSelected == true) {
                    toaster.warning("Warning", "Please provide role for each User");
                    $('#globalLoader').hide();
                }
                else {
                    $scope.projectPhase = "Send Offer";
                    $scope.isBackButtonClick = false;
                    $scope.accessProfileName = "";
                    angular.forEach($scope.allAccessLevel, function (id) {
                        if (id.selected == true) {
                            $scope.accessProfileName = id.name;
                        }
                    });
                    $('#globalLoader').hide();
                }
                //$scope.showProjectWithFreelancers = !$scope.showProjectWithFreelancers;
                //if ($scope.projectMode != "edit") {
                //    $scope.freelancerDetails = [];
                //    $scope.projectAddUpdateModel.freelancersMembers = [];
                //}
            }
        }



        $scope.clickBack = function () {

            if ($scope.freelancerDetails.length > 0) {
                $scope.addFreelancer = true;
            }
            $scope.projectPhase = "Add Project";
            $scope.isBackButtonClick = true;
            //$scope.freelancerDetails = [];
            //$scope.internalUserDetails = [];
        }


        $scope.manageFreelancerCheckboxChange = function (addFreelancer) {
            $scope.addFreelancer = !$scope.addFreelancer;
            $scope.addFreelancerChange(addFreelancer);
        }


        $scope.viewJobOffer = function (freelancer) {

            $('#globalLoader').show();
            $scope.invitationFreelancerObject = {};
            $scope.invitationFreelancerObject.RoleName = ($filter('filter')($scope.RoleObject, { 'RoleId': freelancer.FreelancerRoleId }, true))[0].Name;
            $scope.invitationFreelancerObject.FreelancerRoleId = freelancer.FreelancerRoleId;
            $scope.invitationFreelancerObject.RateOffered = freelancer.FreelancerOfferedPrice;
            $scope.invitationFreelancerObject.Notes = freelancer.FreelancerResponsibilities;
            $scope.invitationFreelancerObject.Attachments = [];
            $scope.invitationFreelancerObject.Attachments = freelancer.PreviousAttachments;
            $scope.invitationFreelancerObject.FreelancerProfilePic = freelancer.FreelancerProfilePhoto;
            $scope.invitationFreelancerObject.FreelancerName = freelancer.FreelancerName;
            $scope.invitationFreelancerObject.FreelancerProfileRate = freelancer.FreelancerProfileRate;
            $scope.invitationFreelancerObject.FreelancerUserId = freelancer.FreelancerUserId;

            $scope.invitationMessageObject = [];
            $scope.invitationMessageObject.Messages = [];

            $scope.invitationProjectObject = {};
            $scope.invitationProjectObject.ProjectName = $scope.projectAddUpdateModel.projectName;
            $scope.invitationProjectObject.ProjectDescription = $scope.projectAddUpdateModel.projectDescription;
            $scope.invitationProjectObject.projectHours = $scope.projectAddUpdateModel.projectHours;

            if ($scope.projectId == 0) {
                if ($scope.projectAddUpdateModel.ProjectAttachments != undefined) {
                    if ($scope.projectAddUpdateModel.ProjectAttachments[0].name != undefined) {
                        $scope.invitationProjectObject.ActualFileName = $scope.projectAddUpdateModel.ProjectAttachments[0].name;
                    }
                }
                $scope.invitationMessageObject.isProjectExist = false;
                $('#modalInvitationMessage').modal('show');
                $('#globalLoader').hide();
            }
            else {
                $scope.invitationProjectObject.ActualFileName = $scope.projectAddUpdateModel.ActualFileName;
                $scope.invitationProjectObject.FileName = $scope.projectAddUpdateModel.FileName;

                invitationFactory.getUserMessage(freelancer.FreelancerInvitationId, sharedService.getUserId())
               .success(function (response) {
                   var Messages = response.ResponseData;
                   $scope.invitationMessageObject.Messages = $scope.convertTo(Messages, 'CreatedDate', true);
                   $scope.invitationMessageObject.offerSentBy_UserId = freelancer.SentBy;
                   $scope.invitationMessageObject.InvitationId = freelancer.FreelancerInvitationId;
                   $scope.invitationMessageObject.isProjectExist = true;
                   $('#modalInvitationMessage').modal('show');
                   $('#globalLoader').hide();
               })
               .error(function (response) {
                   toaster.error("Error", message["error"]);
                   $('#globalLoader').hide();
               })
            }
        }



        $scope.sendMessgae = function (NewMsg) {

            $('#globalLoader').show();
            if (NewMsg != "") {
                var asd = $scope.invitationMessageObject.NewMsg;
                var data = { UserId: $scope.invitationMessageObject.offerSentBy_UserId, Message: NewMsg, SentBy: sharedService.getUserId(), InvitationId: $scope.invitationMessageObject.InvitationId };
                invitationFactory.sendMessgae(data)
               .success(function (response) {

                   invitationFactory.getUserMessage($scope.invitationMessageObject.InvitationId, sharedService.getUserId())
                   .success(function (response) {
                       //$scope.invitationMessageObject.Messages = response.ResponseData;
                       var Messages = response.ResponseData;
                       $scope.invitationMessageObject.Messages = $scope.convertTo(Messages, 'CreatedDate', true);
                       $scope.invitationMessageObject.NewMsg = "";
                       $('#globalLoader').hide();
                   })
                   .error(function (response) {
                       toaster.error("Error", message["error"]);
                       $('#globalLoader').hide();
                   })
               })
               .error(function (response) {
                   toaster.error("Error", message["error"]);
                   $('#globalLoader').hide();
               })
            }
        }


        $scope.TabKeyPressEvent = function (e) {

            var ENTERKEY = 13;
            if (e.shiftKey == false && e.keyCode == ENTERKEY) {
                $scope.sendMessgae($scope.invitationMessageObject.NewMsg);
            }
            else if (e.shiftKey == true && e.keyCode == ENTERKEY) {
                //$('#txtMessage2').append("\r");
                //$scope.invitationMessageObject.NewMsg = $scope.invitationMessageObject.NewMsg + "\r";
            }
        };


        $scope.weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        $scope.convertTo = function (arr, key, dayWise) {

            var groups = [];
            for (var i = 0; l = arr.length, i < l; i++) {
                var newkey = '';

                var weekday_value = $scope.weekdays[new Date(arr[i][key]).getDay()];
                var date = new Date(arr[i][key]).toUTCString();
                var newdate = date.substring(0, 16)
                //arr[i][key] = [weekday_value] + ', ' + date;

                arr[i][key] = newdate;
                //groups.push(arr);
            }
            return arr;
        };


        $scope.addFreelancerChange = function (addFreelancer) {
            if (addFreelancer == false) {
                $scope.freelancerDetails = [];
                $scope.projectAddUpdateModel.freelancersMembers = [];
            }
            else {
                if ($scope.projectMode != "edit") {
                    //$scope.freelancerDetails = [];
                    //$scope.projectAddUpdateModel.freelancersMembers = [];
                }
            }
        }


        /* Add new Project */
        $scope.addNewProject = function (isProjectNameInvalid, isProjectDescriptionInvalid, event, projectAddUpdateForm) {
            debugger;
            if (!$scope.showProjectWithFreelancers && (isProjectNameInvalid || isProjectDescriptionInvalid)) {
                projectAddUpdateForm.projectName.$dirty = true;
                projectAddUpdateForm.projectDescription.$dirty = true;
                projectAddUpdateForm.projectHours.$dirty = true;
                projectAddUpdateForm.projectBudget.$dirty = true;
            }
            else {

                $('#globalLoader').show();
                var accessLevel
                angular.forEach($scope.allAccessLevel, function (id) {
                    if (id.selected == true) {
                        accessLevel = id.id;
                    }
                });
                if (accessLevel == undefined) {
                    toaster.warning("Warning", "Please select access level for project");
                    $('#globalLoader').hide();
                    return;
                }
                $scope.projectAddUpdateModel.teamId = $scope.selectedTeamId;
                $scope.projectAddUpdateModel.accessLevel = accessLevel;
                $scope.projectAddUpdateModel.internalUserDetails = $scope.internalUserDetails;
                
                //Uploading all the attachments for freelancers
                var customIndex = 0;
                angular.forEach($scope.freelancerDetails, function (flncr) {

                    var attachmentIndex = 0;
                    flncr.AllAttachments = [];

                    if (flncr.Attachments != undefined) {
                        angular.forEach(flncr.Attachments, function (attchmnt) {

                            $upload.upload({
                                url: baseurl + "ProjectsApi/UploadFileForAttachments", // webapi url
                                method: "POST",
                                //data: { UploadedFile: $file, UserId: sharedService.getUserId() },
                                data: { UploadedFile: attchmnt },
                                file: attchmnt
                            })
                            .progress(function (evt) {
                                // get upload percentage
                                $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                            })
                            .success(function (dataUpload, status, headers, config) {
                                // file is uploaded successfully
                                if (dataUpload.success) {

                                    var atchData = {
                                        FileName: dataUpload.ResponseData.FileName,
                                        ActualFileName: dataUpload.ResponseData.ActualFileName,
                                        FilePath: dataUpload.ResponseData.FilePath
                                    };
                                    flncr.AllAttachments.push(atchData);

                                    attachmentIndex = attachmentIndex + 1;
                                    if (attachmentIndex == flncr.Attachments.length) {
                                        customIndex = customIndex + 1;
                                    }
                                    //customIndex = customIndex + 1;
                                }
                                else {
                                    toaster.error("Error", message[dataUpload.message]);
                                    $('#globalLoader').hide();
                                    //angular.forEach(angular.element("input[type='file']"), function (inputElem) {
                                    //        angular.element(inputElem).val(null);
                                    //});
                                    //customIndex = customIndex + 1;
                                    attachmentIndex = attachmentIndex + 1;
                                    if (attachmentIndex == flncr.Attachments.length) {
                                        customIndex = customIndex + 1;
                                    }
                                }
                                if (customIndex == $scope.freelancerDetails.length) {
                                    $scope.addNewProjectUploadFileForProject(projectAddUpdateForm);
                                }
                            })
                            .error(function (data, status, headers, config) {
                                // file failed to upload
                                attachmentIndex = attachmentIndex + 1;
                                if (attachmentIndex == flncr.Attachments.length) {
                                    customIndex = customIndex + 1;
                                }
                                //customIndex = customIndex + 1;
                                toaster.error("Error", "File failed to upload ");
                                $('#globalLoader').hide();
                            });
                        });
                    }
                    else {
                        customIndex = customIndex + 1;

                        if (customIndex == $scope.freelancerDetails.length) {
                            $scope.addNewProjectUploadFileForProject(projectAddUpdateForm);
                        }
                    }
                });
                if ($scope.freelancerDetails.length == 0) {
                    $scope.addNewProjectUploadFileForProject(projectAddUpdateForm);
                }
            }
        };


        $scope.addNewProjectUploadFileForProject = function (projectAddUpdateForm) {

            if ($scope.projectAddUpdateModel.ProjectAttachments != undefined) {

                $upload.upload({
                    url: baseurl + "ProjectsApi/UploadFileForAttachments", // webapi url
                    method: "POST",
                    //data: { UploadedFile: $file, UserId: sharedService.getUserId() },
                    data: { UploadedFile: $scope.projectAddUpdateModel.ProjectAttachments[0], index: 0 },
                    file: $scope.projectAddUpdateModel.ProjectAttachments[0]
                })
                .progress(function (evt) {
                    // get upload percentage
                    $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                })
                .success(function (dataUpload, status, headers, config) {
                    // file is uploaded successfully

                    if (dataUpload.success) {

                        var TempData = {
                            //'FreelancerRate': $scope.projectAddUpdateModel.FreelancerRate,
                            //'ProjectAttachments': $scope.projectAddUpdateModel.ProjectAttachments,
                            'AttachmentDescription': $scope.projectAddUpdateModel.AttachmentDescription,
                            //'FreelancerResponsibilities': $scope.projectAddUpdateModel.FreelancerResponsibilities,
                            'FileName': dataUpload.ResponseData.FileName,
                            'ActualFileName': dataUpload.ResponseData.ActualFileName,
                            'FilePath': dataUpload.ResponseData.FilePath
                        };

                        $scope.projectAddUpdateModel.TempData = TempData;
                        $scope.projectAddUpdateModel.freelancerDetails = $scope.freelancerDetails;

                        var NewProjectObj = { 'ProjectModel': $scope.projectAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId };

                        $scope.insertProject(NewProjectObj, projectAddUpdateForm);    // here 2nd parameter is add new project form 


                    }
                    else {
                        toaster.error("Error", message[dataUpload.message]);
                        $('#globalLoader').hide();

                        //angular.forEach(
                        // angular.element("input[type='file']"),
                        //         function (inputElem) {
                        //             angular.element(inputElem).val(null);
                        //         });
                    }
                })
                .error(function (data, status, headers, config) {
                    // file failed to upload
                    toaster.error("Error", "File failed to upload ");
                    $('#globalLoader').hide();
                });

            }
            else {

                var TempData = {
                    //'FreelancerRate': $scope.projectAddUpdateModel.FreelancerRate,
                    //'ProjectAttachments': $scope.projectAddUpdateModel.ProjectAttachments,
                    //'AttachmentDescription': $scope.projectAddUpdateModel.AttachmentDescription,
                    //'FreelancerResponsibilities': $scope.projectAddUpdateModel.FreelancerResponsibilities,
                };

                $scope.projectAddUpdateModel.TempData = TempData;
                $scope.projectAddUpdateModel.freelancerDetails = $scope.freelancerDetails;

                var NewProjectObj = { 'ProjectModel': $scope.projectAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId };

                $scope.insertProject(NewProjectObj, projectAddUpdateForm);    // here 2nd parameter is add new project form 

            }
        }


        /* Part of Add new Project , called from inside of $scope.addNewProject */
        $scope.insertProject = function (NewProjectObj) {

            projectsFactory.insertProject(NewProjectObj)
               .success(function (data) {
                   if (data.ResponseData == 'ProjectAddedSuccess') {
                       if (data.success) {
                           $scope.$emit('showMenu');
                           $('#globalLoader').hide();
                           toaster.success("Success", message[data.ResponseData]);
                           $location.path('/project');
                       }
                   }
                   else {
                       toaster.warning("Warning", message[data.ResponseData]);
                       $('#globalLoader').hide();
                   }
             
               })
               .error(function (data) {
                   $('#globalLoader').hide();
                   toaster.error("Error", "Some Error Occured !");
               });

        }


        /* Edit Project*/
        $scope.editProject = function (projectObject) {

            if (projectObject == null) {
                toaster.error("Error", "Please select appropriate project for edit");
                $('#globalLoader').hide();
                $location.path('/project');
            }
            $scope.projectMode = "edit";

            angular.forEach($scope.allAccessLevel, function (id) {

                if (id.id == projectObject.Accesslevel) {
                    id.selected = true;
                }
                else {
                    id.selected = false;
                }
            });

            $scope.addFreelancer = false;
            $scope.showProjectWithFreelancers = false;
            // To hide  Attachment Description field
            $scope.showAttachmentDescription = false;
            debugger;
            $scope.projectButton = true;
            $scope.projectAddUpdateModel = {};
            $scope.projectAddUpdateModel.projectName = projectObject.ProjectName;
            $scope.projectAddUpdateModel.projectDescription = projectObject.ProjectDescription;
            $scope.projectAddUpdateModel.projectHours = projectObject.projectHours;
            $scope.projectAddUpdateModel.projectBudget = projectObject.projectBudget;
            $scope.projectAddUpdateModel.projectId = projectObject.ProjectId;
            $scope.projectAddUpdateModel.ActualFileName = projectObject.ActualFileName;
            $scope.projectAddUpdateModel.FileName = projectObject.FileName;
            $scope.projectAddUpdateModel.FilePath = projectObject.FilePath;
            $scope.projectAddUpdateModel.AttachmentDescription = projectObject.AttachmentDescription;
            $scope.projectAddUpdateModel.accessLevel = projectObject.Accesslevel;

            if ($scope.projectAddUpdateModel.ActualFileName != undefined && $scope.projectAddUpdateModel.ActualFileName != "") {
                $scope.showAttachmentDescription = true;
            }

            var projectMembers = [];
            var freelancersMembers = [];
            if (projectObject.ProjectMembers.length > 0) {
                for (var i = 0; i < projectObject.ProjectMembers.length; i++) {
                    if (projectObject.ProjectMembers[i].IsFreelancer == true) {
                        freelancersMembers.push(projectObject.ProjectMembers[i].MemberUserId);
                    }
                    else {
                        projectMembers.push(projectObject.ProjectMembers[i].MemberUserId);
                    }
                }
            }

            $scope.projectAddUpdateModel.projectMembers = projectMembers;
            $scope.projectAddUpdateModel.freelancersMembers = freelancersMembers;

            //To add all invted freelancers who have not accepted the invitation yet
            projectsFactory.getInvitedFreelamcersOnlyNotAccepted(projectObject.ProjectId)
            .success(function (responseData) {

                $scope.myProjectMember.projectMembers = $filter('filter')($scope.TeamsObject.activeTeam.TeamMembers, { IsFreelancer: false }, true);
                $scope.myProjectMember.freelancersOnly = $filter('filter')($scope.TeamsObject.activeTeam.TeamMembers, { IsFreelancer: true }, true);

                for (var i = 0; i < responseData.ResponseData.length; i++) {
                    $scope.freelancerDetails.push(responseData.ResponseData[i]);

                    for (var k = 0; k < $scope.myProjectMember.freelancersOnly.length; k++) {
                        if ($scope.myProjectMember.freelancersOnly[k].MemberUserId == responseData.ResponseData[i].FreelancerUserId) {
                            $scope.myProjectMember.freelancersOnly.splice(k, 1);
                        }
                    }
                }

                if ($scope.freelancerDetails.length > 0) {
                    $scope.addFreelancer = true;
                }

                $('#globalLoader').hide();

            })
            .error(function (responseData) {
                toaster.error("Error", message["error"]);
                $('#globalLoader').hide();
            })

        };


        /* Update Project Step-1   */
        $scope.updateProject = function (isProjectNameInvalid, isProjectDescriptionInvalid, projectAddUpdateForm) {
            debugger;
            if (!$scope.showProjectWithFreelancers && (isProjectNameInvalid || isProjectDescriptionInvalid)) {
                projectAddUpdateForm.projectName.$dirty = true;
                projectAddUpdateForm.projectDescription.$dirty = true;
                projectAddUpdateForm.projectHours.$dirty = true;
                projectAddUpdateForm.projectBudget.$dirty = true;
            }
            else {
                $('#globalLoader').show();

                var accessLevel
                angular.forEach($scope.allAccessLevel, function (id) {
                    if (id.selected == true) {
                        accessLevel = id.id;
                    }
                });

                if (accessLevel == undefined) {
                    toaster.warning("Warning", "Please select access level for project");
                    $('#globalLoader').hide();
                    return;
                }
                var currentTeamId = $scope.selectedTeamId;
                $scope.projectAddUpdateModel.teamId = $scope.selectedTeamId;
                $scope.projectAddUpdateModel.accessLevel = accessLevel;
                $scope.projectAddUpdateModel.internalUserDetails = $scope.internalUserDetails;

                // Uploading all the attachments for freelancers
                var customIndex = 0;

                angular.forEach($scope.freelancerDetails, function (flncr) {

                    var attachmentIndex = 0;
                    flncr.AllAttachments = [];
                    if (flncr.Attachments != undefined) {

                        angular.forEach(flncr.Attachments, function (attchmnt) {
                            $upload.upload({
                                url: baseurl + "ProjectsApi/UploadFileForAttachments", // webapi url
                                method: "POST",
                                //data: { UploadedFile: $file, UserId: sharedService.getUserId() },
                                data: { UploadedFile: attchmnt },
                                file: attchmnt
                            })
                            .progress(function (evt) {
                                // get upload percentage
                                $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                            })
                            .success(function (dataUpload, status, headers, config) {
                                // file is uploaded successfully
                                if (dataUpload.success) {
                                    var atchData = {
                                        FileName: dataUpload.ResponseData.FileName,
                                        ActualFileName: dataUpload.ResponseData.ActualFileName,
                                        FilePath: dataUpload.ResponseData.FilePath
                                    };

                                    flncr.AllAttachments.push(atchData);
                                    attachmentIndex = attachmentIndex + 1;
                                    if (attachmentIndex == flncr.Attachments.length) {
                                        customIndex = customIndex + 1;
                                    }
                                }
                                else {
                                    toaster.error("Error", message[dataUpload.message]);
                                    $('#globalLoader').hide();
                                    attachmentIndex = attachmentIndex + 1;
                                    if (attachmentIndex == flncr.Attachments.length) {
                                        customIndex = customIndex + 1;
                                    }
                                }

                                if (customIndex == $scope.freelancerDetails.length) {
                                    $scope.updateProjectUploadFileForProject(projectAddUpdateForm);
                                }
                            })
                            .error(function (data, status, headers, config) {
                                // file failed to upload
                                attachmentIndex = attachmentIndex + 1;
                                if (attachmentIndex == flncr.Attachments.length) {
                                    customIndex = customIndex + 1;
                                }
                                toaster.error("Error", "File failed to upload ");
                                $('#globalLoader').hide();
                            });

                        });
                    }
                    else {
                        //attachmentIndex = attachmentIndex + 1;
                        //if (attachmentIndex == flncr.Attachments.length) {
                        //    customIndex = customIndex + 1;
                        //}

                        //if (customIndex == $scope.freelancerDetails.length) {
                        //    $scope.updateProjectUploadFileForProject(projectAddUpdateForm);
                        //}

                        customIndex = customIndex + 1;
                        if (customIndex == $scope.freelancerDetails.length) {
                            $scope.updateProjectUploadFileForProject(projectAddUpdateForm);
                        }

                    }
                });


                if ($scope.freelancerDetails.length == 0) {
                    $scope.updateProjectUploadFileForProject(projectAddUpdateForm);
                }

            }
        };

        /* Update Project   Step-2  */
        $scope.updateProjectUploadFileForProject = function () {
            if ($scope.projectAddUpdateModel.ProjectAttachments != undefined) {

                $upload.upload({
                    url: baseurl + "ProjectsApi/UploadFileForAttachments", // webapi url
                    method: "POST",
                    //data: { UploadedFile: $file, UserId: sharedService.getUserId() },
                    data: { UploadedFile: $scope.projectAddUpdateModel.ProjectAttachments[0], index: 0 },
                    file: $scope.projectAddUpdateModel.ProjectAttachments[0]
                })
                .progress(function (evt) {
                    // get upload percentage
                    $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
                })
                .success(function (dataUpload, status, headers, config) {
                    // file is uploaded successfully
                    if (dataUpload.success) {

                        var TempData = {
                            //'FreelancerRate': $scope.projectAddUpdateModel.FreelancerRate,
                            //'ProjectAttachments': $scope.projectAddUpdateModel.ProjectAttachments,
                            'AttachmentDescription': $scope.projectAddUpdateModel.AttachmentDescription,
                            //'FreelancerResponsibilities': $scope.projectAddUpdateModel.FreelancerResponsibilities,
                            'FileName': dataUpload.ResponseData.FileName,
                            'ActualFileName': dataUpload.ResponseData.ActualFileName,
                            'FilePath': dataUpload.ResponseData.FilePath
                        };

                        $scope.projectAddUpdateModel.TempData = TempData;
                        $scope.projectAddUpdateModel.freelancerDetails = $scope.freelancerDetails;

                        var NewProjectObj = { 'ProjectModel': $scope.projectAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId };

                        $scope.updateTheProject(NewProjectObj, projectAddUpdateForm);    // here 2nd parameter is add new project form 


                        // To empty  input-type= file
                        //angular.forEach(
                        //angular.element("input[type='file']"),
                        //        function (inputElem) {
                        //            angular.element(inputElem).val(null);
                        //        });
                    }
                    else {
                        toaster.error("Error", message[dataUpload.message]);
                        $('#globalLoader').hide();
                        //angular.forEach(
                        // angular.element("input[type='file']"),
                        //         function (inputElem) {
                        //             angular.element(inputElem).val(null);
                        //         });
                    }
                })
                .error(function (data, status, headers, config) {
                    // file failed to upload
                    toaster.error("Error", "File failed to upload ");
                    $('#globalLoader').hide();
                });

            }
            else {

                var TempData = {
                    'AttachmentDescription': $scope.projectAddUpdateModel.AttachmentDescription,
                    //'FreelancerResponsibilities': $scope.projectAddUpdateModel.FreelancerResponsibilities,
                    'FileName': $scope.projectAddUpdateModel.FileName,
                    'ActualFileName': $scope.projectAddUpdateModel.ActualFileName,
                    'FilePath': $scope.projectAddUpdateModel.FilePath
                };

                $scope.projectAddUpdateModel.TempData = TempData;
                $scope.projectAddUpdateModel.freelancerDetails = $scope.freelancerDetails;

                var NewProjectObj = { 'ProjectModel': $scope.projectAddUpdateModel, 'UserId': sharedService.getUserId(), 'CompanyId': $scope.selectedCompanyId };

                $scope.updateTheProject(NewProjectObj, projectAddUpdateForm);    // here 2nd parameter is add new project form 

            }
        }


        /* Update Project   Step-3  */
        /* Part of Update Project , called from inside of $scope.updateProject */
        $scope.updateTheProject = function (NewProjectObj) {
            debugger;
            projectsFactory.updateProject(NewProjectObj)
            .success(function (data) {
                if (data.ResponseData == 'ProjectUpdatedSuccess') {
                    $scope.$emit('showMenu');
                    $('#globalLoader').hide();
                    toaster.success("Success", message[data.ResponseData]);
                    $location.path('/project');
                }
                else {
                    toaster.warning("Warning", message[data.ResponseData]);
                    $('#globalLoader').hide();
                }
            })
            .error(function (data) {
                $scope.status = 'Unable to insert user';
            });

        }


        $scope.cancelAddUpdateProject = function () {
            $location.path('/project');
        }


        $scope.manageNotes = function (frlncr) {
            $scope.selectedFrlncr = frlncr;
            $scope.noteTypeIsSet = true;
            if (frlncr.FreelancerResponsibilities != null && frlncr.FreelancerResponsibilities != "") {
                $scope.noteTypeIsSet = false;
            }
            //$('#Notes').focus();
            $('#modalFreelancerNotes').modal('show');
            $timeout(function () {
                $('#Notes').focus();
            },500)
            
        }

        $scope.setNotes = function (selectedFrlncr) {

            angular.forEach($scope.freelancerDetails, function (item) {

                if (item.FreelancerUserId == selectedFrlncr.FreelancerUserId) {
                    item.FreelancerResponsibilities = selectedFrlncr.FreelancerNotes;
                }
            });

            $('#modalFreelancerNotes').modal('hide');
        }

        //Download Project Document File with original name 
        $scope.downloadProjectDocument = function (projectAddUpdateModel) {

            projectsFactory.downloadProjectDocument(projectAddUpdateModel.projectId)
                .success(function (data, status, headers, config) {

                    var file = new Blob([data], {
                        type: 'application/csv'
                    });
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = projectAddUpdateModel.ActualFileName;
                    document.body.appendChild(a);
                    a.click();
                })
                .error(function (data, status, headers, config) {
                    toaster.error("Error", message["error"]);
                })
        }


        //Download Freelancer Attachments File with original name 
        $scope.downloadFreelancerAttachments = function (freelancer) {

            var data = { FileName: freelancer.FileName, ActualFileName: freelancer.ActualFileName, FilePath: freelancer.FilePath }
            projectsFactory.downloadFreelancerAttachments(data)
                .success(function (data, status, headers, config) {

                    var file = new Blob([data], {
                        type: 'application/csv'
                    });
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = freelancer.ActualFileName;
                    document.body.appendChild(a);
                    a.click();
                })
                .error(function (data, status, headers, config) {
                    toaster.error("Error", message["error"]);
                })
        }


        /* Open screen for Project feedback & Completion */
        $scope.deleteAttachmentFileFromProject = function (projectModel) {

            var projectId = projectModel.projectId;
            $('#globalLoader').show();
            projectsFactory.deleteAttachmentFileFromProject(projectId)
                .success(function (freelancerData) {
                    if (freelancerData.ResponseData == "File Deleted") {

                        getProjects();
                        /* Getting User Owned Projects */
                        projectsFactory.getUserOwnedProjects($scope.selectedTeamId, sharedService.getUserId(), sharedService.getRoleId(), $scope.selectedCompanyId)
                            .success(function (data) {
                                if (data.success) {
                                    projectModel.ActualFileName = null;
                                    $scope.UserOwnedProjectsObject = data.ResponseData;
                                    toaster.success("Success", "File is deleted successfully !!");
                                    $('#globalLoader').hide();
                                }
                                else {
                                    toaster.error("Error", data.message);
                                    $('#globalLoader').hide();
                                }
                            })
                            .error(function () {
                                toaster.error("Error", "Some Error Occured!");
                                $('#globalLoader').hide();
                            });
                    }
                    else {
                        $('#globalLoader').hide();
                        toaster.error("Error", "File Not Found");
                    }
                })
                .error(function (freelancerData) {
                    $('#globalLoader').hide();
                    toaster.error("Error", message["error"]);
                })
        }

        $scope.downloadAttachmentFileFromProject = function (projectModel) {
            projectsFactory.downloadAttachmentFileFromProject(projectModel.projectId)
                .success(function (freelancerData) {
                    toaster.success("Success", "");
                })
                .error(function (freelancerData) {
                    $('#globalLoader').hide();
                    toaster.error("Error", message["error"]);
                })

        }


        $scope.selectFileForProjectAttachent = function ($files) {

            $scope.projectAddUpdateModel.ProjectAttachments = $files;
            if ($scope.projectAddUpdateModel.ProjectAttachments != undefined) {
                $scope.showAttachmentDescription = true;
            }
        }

        $scope.refreshProjectAttachments = function () {
            angular.forEach(angular.element("input[name='ProjectAttachments']"), function (inputElem) {
                angular.element(inputElem).val(null);
            });
            $scope.projectAddUpdateModel.ProjectAttachments = undefined;
            $scope.showAttachmentDescription = false;
            $scope.projectAddUpdateModel.AttachmentDescription = "";
        }

        $scope.selectFileForFreelancersAttachent = function ($files, index, FreelancerUserId) {
            debugger
            for (var i = 0; i < $files.length; i++) {
                if ($scope.freelancerDetails[index].Attachments == undefined) {
                    $scope.freelancerDetails[index].Attachments = [];
                }
                if ($scope.freelancerDetails[index].PreviousAttachments == null) {
                    $scope.freelancerDetails[index].PreviousAttachments = [];
                }

                $scope.freelancerDetails[index].Attachments.push($files[i]);
                var file = { ActualFileName: $files[i].name };
                $scope.freelancerDetails[index].PreviousAttachments.push(file);

                angular.forEach(angular.element("input[name='" + FreelancerUserId + "']"), function (inputElem) {
                    angular.element(inputElem).val(null);
                });
            }
        }


        $scope.removeFreelancerAttachments = function (index, parentIndex) {

            $scope.freelancerDetails[parentIndex].Attachments.splice(index, 1);

            $scope.freelancerDetails[parentIndex].PreviousAttachments.splice(index, 1);

        }


        /* Open screen for Project feedback */
        $scope.projectFeedback = function (projectId) {
            $location.path('/project/feedback/' + projectId);
        }


        /* Configuration for adding projectmembers from Project Add Update Modal */
        $scope.projectMembersConfig = {
            create: false,
            // maxItems: 1,
            required: true,
            plugins: ['remove_button'],
            valueField: 'Value',
            labelField: 'Text',
            // delimiter: '|',
            placeholder: 'Pick Project Members...',

            render: {

                option: function (item, escape) {
                    debugger;
                    var label = item.IsFreelancer;
                    var caption = item.Text;
                    var iconByType = "fa fa-compress";
                    if (label == true) {
                        iconByType = "fa fa-expand";
                    }

                    return '<div>' +
                    '<span >  <i class="' + iconByType + '" aria-hidden="true"></i>   </span> ' +
                    (caption ? '<span>' + escape(caption) + '</span>' : '') +
                    '</div>';

                }
            },
            onItemAdd: function onItemAdd(value, $item) {
                debugger;
                if (!$scope.isBackButtonClick) {
                    $('#globalLoader').show();
                    var projectId = 0;
                    if ($scope.projectMode == "edit") {
                        projectId = $scope.projectAddUpdateModel.projectId;
                    }
                    projectsFactory.getInternalUserDetails(value, projectId)
                        .success(function (response) {

                            var internalUserObj = response.ResponseData;
                            $scope.internalUserDetails.push(internalUserObj);
                            $('#globalLoader').hide();
                        })
                        .error(function (response) {
                            $('#globalLoader').hide();
                            toaster.error("Error", "Some Error Occured");
                        });
                }
                else if ($scope.freelancerDetails.length != undefined && $scope.freelancerDetails.length < 1) {
                    $scope.isBackButtonClick = false;
                }
            },

            onItemRemove: function onItemAdd(value, $item) {
                $('#globalLoader').show();

                angular.forEach($scope.internalUserDetails, function (item) {

                    if (item.UserId == value) {
                        var index = $scope.internalUserDetails.indexOf(item);
                        $scope.internalUserDetails.splice(index, 1);
                        $scope.$apply();
                    }
                });
                $('#globalLoader').hide();
            }

        }


        /* Configuration for adding projectmembers from Project Add Update Modal */
        $scope.freelancersConfig = {
            create: false,
            // maxItems: 1,
            required: true,
            plugins: ['remove_button'],
            valueField: 'Value',
            labelField: 'Text',
            // delimiter: '|',
            placeholder: 'Pick Freelancers As Project Members...',

            render: {
                option: function (item, escape) {
                    var label = item.IsFreelancer;
                    var caption = item.Text;
                    var iconByType = "fa fa-compress";
                    if (label == true) {
                        iconByType = "fa fa-expand";
                    }

                    return '<div>' +
                    '<span >  <i class="' + iconByType + '" aria-hidden="true"></i>   </span> ' +
                    (caption ? '<span>' + escape(caption) + '</span>' : '') +
                    '</div>';

                }
            },

            onItemAdd: function onItemAdd(value, $item) {

                if (!$scope.isBackButtonClick) {
                    $('#globalLoader').show();
                    var projectId = 0;
                    if ($scope.projectMode == "edit") {
                        projectId = $scope.projectAddUpdateModel.projectId;
                    }
                    projectsFactory.getFreelancerDetails(value, projectId)
                        .success(function (response) {
                            debugger;
                            var freelancer = response.ResponseData;
                            $scope.freelancerDetails.push(freelancer);
                            //$scope.$apply();
                            $('#globalLoader').hide();
                        })
                        .error(function (response) {
                            $('#globalLoader').hide();
                            toaster.error("Error", "Some Error Occured");
                        });
                }
                else {
                    $scope.isBackButtonClick = false;
                }
            },

            onItemRemove: function onItemAdd(value, $item) {
                $('#globalLoader').show();

                angular.forEach($scope.freelancerDetails, function (item) {

                    if (item.FreelancerUserId == value) {
                        var index = $scope.freelancerDetails.indexOf(item);
                        $scope.freelancerDetails.splice(index, 1);
                        $scope.$apply();
                    }
                });
                $('#globalLoader').hide();
            }


        }

        /*  Getting projects related to Selected Team */
        function getProjects() {
            projectsFactory.getProjects(sharedService.getUserId(), $scope.selectedTeamId, $scope.selectedCompanyId, sharedService.getIsFreelancer())
                .success(function (data) {
                    if (data.success) {
                        debugger;
                        $scope.projects = data.ResponseData;
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured !");
                });
        }



        angular.element(document).ready(function () {

            $scope.projectId = $stateParams.pid;
            
            //Get All The Team Members
            teamFactory.getLoggedInUserTeams(sharedService.getUserId(), sharedService.getCompanyId())
                .success(function (data) {
                    if (data.success) {
                        $scope.TeamsObject = {};
                        $scope.TeamsObject.activeTeam = data.ResponseData[0];
                        if ($scope.TeamsObject.activeTeam.TeamId == undefined) {
                            $scope.selectedTeamId = sharedService.getDefaultTeamId();
                        }
                        else {
                            $scope.selectedTeamId = $scope.TeamsObject.activeTeam.TeamId;
                        }

                        if ($scope.projectId == 0) {
                            $scope.showProjectAddUpdateModal();
                        }
                        else {
                            $scope.showProjectManagementSection($scope.projectId);
                        }
                    }
                    else {
                        toaster.error("Error", data.message);
                    }
                })
                .error(function () {
                    toaster.error("Error", "Some Error Occured!");
                });

        });







    });
});