﻿define(['app'], function () {

    app.controller("vendorProjectController",
        function ($scope, sharedService, $rootScope, toaster, $filter, $http, $location, projectsFactory, apiURL, vendorFactory, companyAccountFactory) {
        $scope.hideProjectInfo = true;
        $scope.hideShowRisk = false;
        $scope.hideShowPhaseMile = false;
        $scope.hideShowVendorInfo = false;
        $scope.editPhasebuttonShow = false;
        $scope.editMilestonebuttonShow = false;
        $scope.editRiskCommentbuttonShow = false;
        $scope.mySeverityList = [{ Text: "Normal", Value: "Normal" }, { Text: "High", Value: "High" }];
        $scope.myStatusList = [{ Text: "Open", Value: "Open" }, { Text: "Resolved", Value: "Resolved" }];
        $scope.myFirstCurrent = 'current';
        $scope.mySecondcCurrent = '';
        $scope.myThirdCurrent = '';
        $scope.myForthCurrent = '';
        $scope.dataFromat = 
        $scope.regEx = "/^\\d+$/";
        $scope.SelectedAcc = {};
        $scope.AddNewProjectModel = {
            ProjectName: "",
            ProjectStartDate:"",
            ProjectEndDate:"",
            ProjectBudget:"",
            ProjectDescription: "",
            UserId: sharedService.getUserId(),
            CompanyId: sharedService.getCompanyId(),
            VendorList: [],
            PhaseList: [],
            ProjectDocument: [],
            CompanyAccList: [],
            RiskList:[]

        }
            ///////////////

        $scope.CustomDirectiveReady = false;
        //$scope.customcontrolmodel = {
        //    "Project_Subtitle": "project subtitle", "Project_Time": { "Id": 2, "Name": "0-6 month" }, "Project_Type": { "Id": 1, "Name": "in-house" },
        //    "Is_outsourced": true, "stakeholder's_Addres": "Ne one stake holder", "Radio": "January", "Text_box_second": "second second",
        //    "financer's_status": "single vendor"
        //};

            
        $scope.customcontrolmodel = {};


     
        $scope.Phase = { Name: "", Budget: "", Description: "",  PhaseDocumentsName: [], MileStone: [] }
        $scope.phaseModel = {};
        $scope.milestoneModal = {};
        $scope.MileStoneModel = { Name: "", Description: "", EstimatedHours: "" }
        $scope.PhaseDocumentAttachedView = [];




        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }

            $scope.format = 'MM/dd/yyyy';
        })

        $scope.popup = {
            opened1: false,
            opened2: false
        };

        $scope.open1 = function () {
            $scope.popup.opened1 = true;
            $scope.mindate = new Date();
        };

        $scope.open2 = function () {
            $scope.popup.opened2 = true;
            $scope.mindate1 = new Date($scope.AddNewProjectModel.projectStartDate);
        };


        //////////////////////////////////////////Step first add project//////////////////////////////////////////////////////////
        $scope.nextFromProjectWiz = function (isVendorFirstNameInvalid, NewProjectToVendorForm, projectAddUpdateModel) {
            debugger;
          var x =    $scope.Customcontrolmodel;
            if (isVendorFirstNameInvalid) {
                NewProjectToVendorForm.projectName.$dirty = true

            }
            else {

                debugger;
                $scope.AddNewProjectModel.ProjectName = projectAddUpdateModel.projectName
                $scope.AddNewProjectModel.ProjectStartDate = $filter('date')(projectAddUpdateModel.projectStartDate, "MM/dd/yyyy");
                $scope.AddNewProjectModel.ProjectEndDate = $filter('date')(projectAddUpdateModel.projectEndDate, "MM/dd/yyyy");
                $scope.AddNewProjectModel.ProjectBudget = projectAddUpdateModel.projectBudget;
                $scope.hideProjectInfo = false;
                $scope.hideShowRisk = true;
                $scope.hideShowPhaseMile = false;
                $scope.hideShowVendorInfo = false;
            }
            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = 'current';
            $scope.myThirdCurrent = '';
            $scope.myForthCurrent = '';
            /////////////////////Severity Dropdown///////////////////////

        }

        /////////////////////////////selectFileForDocumentPhase1//////////////////////////////////////////////////
        var PhaseIndex;
        $scope.selectFileForProject = function ($files, index) {
            debugger;
            
            for (var i = 0; i < $files.length; i++) {

                if ($files[0].size < 10485760) {
                    $scope.AddNewProjectModel.ProjectDocument.push($files[i])
                }
                else {
                    toaster.error("Error",message.FileSizeExceeds);
                }
            }
        }

            /////////////////////////////////////editRisk///////////////////////////////////////////////////////////
        $scope.editRisk = function (index, item) {
            debugger;
            $scope.riskModel.RiskTitle = item.RiskTitle;
            $scope.riskModel.Description = item.Description;
            $scope.riskModel.Severity = {Text: item.Severity,Value: item.Severity};
            $scope.riskModel.Status = { Text: item.Status, Value: item.Status };
            $scope.RiskcurrentIndex = index;
            $('#modalAddNewRisk').modal('show');
            $scope.editRiskbuttonShow = true
        }
            ////////////////////////////////////////deleteRisk////////////////////////////////////////////////////////////////////////////
        $scope.RiskcurrentIndex = 0;
        $scope.deleteRiskConfirm = function (index, item) {
            debugger;
            $('#modalConfirmDeleteRisk').modal('show')
            $scope.riskTitleONRisk = item.RiskTitle;
            $scope.RiskcurrentIndex = index;
        }
        $scope.deleteRisk = function (index, item) {
            debugger;
            $scope.AddNewProjectModel.RiskList.splice($scope.RiskcurrentIndex, 1)

            $('#modalConfirmDeleteRisk').modal('hide')

        }


            //////////////////////////////////////////////////updateRisk///////////////////////////////////////////////////////////////////////////////////////////////////

        $scope.updateRisk = function (IsRiskTitleInvalid, IsSeverityInvalid, IsStatusInvalid, RiskForm, RiskModel) {

            debugger;
            if (IsRiskTitleInvalid || IsSeverityInvalid || IsStatusInvalid) {
                RiskForm.riskTitle.$dirty = true;
                RiskForm.severity.$dirty = true;
                RiskForm.status.$dirty = true;
            }
            else {

                

                $scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].RiskTitle = RiskModel.RiskTitle
                $scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].Description = RiskModel.Description;
                $scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].Severity = RiskModel.Severity.Value;
                $scope.AddNewProjectModel.RiskList[$scope.RiskcurrentIndex].Status = RiskModel.Status.Value;
                
                $scope.riskModel = {};
                RiskForm.riskTitle.$dirty = false;
                RiskForm.severity.$dirty = false;
                RiskForm.status.$dirty = false;
                $('#modalAddNewRisk').modal('hide');

            }


        }
        /////////////////////////////////////AddPhase///////////////////////////////////////////////////////////
        $scope.addNewPhase = function (IsNamevalid, Phaseform, phase) {
            debugger;

            if (IsNamevalid) {
                Phaseform.phaseName.$dirty = true;
            }
            else {
                var data = { Name: "", Budget: "", Description: "", PhaseDocumentAttached: [], MileStone: [], PhaseDocumentsName: [] };
                data.Name = phase.name;
                data.Budget = phase.budget;
                data.Description = phase.description;
                data.PhaseDocumentsName = $scope.PhaseDocumentAttachedView;
                data.CreateOn = moment(new Date()).format("MM/DD/YYYY");
                $scope.AddNewProjectModel.PhaseList.push(data);
                $scope.PhaseDocumentAttachedView = [];
                $scope.phaseModel = {};
                Phaseform.phaseName.$dirty = false;

              

                var input = $("#fileInput");

                clearInput(input);
                function clearInput(input) {
                    input = input.val('').clone(true);
                };

            }
        }

        ///////////////////////////////removePhaseAttachments///////////////////////////////////////
        $scope.removePhaseAttachments = function (index) {
            debugger
            var parentIndex = $scope.phaseModel.PhaseIndex;
            if (parentIndex == 'undefined') {
                $scope.AddNewProjectModel.PhaseList[parentIndex].PhaseDocumentsName.splice(index, 1);
            }
            $scope.PhaseDocumentAttachedView.splice(index, 1);

        }

        /////////////////////////////////////editPhase///////////////////////////////////////////////////////////
        $scope.editPhase = function (index, item) {
            debugger;
            $scope.phaseModel.name = item.Name;
            $scope.phaseModel.description = item.Description;
            $scope.phaseModel.budget = item.Budget;
            $scope.createOn = item.CreateOn;
            $scope.PhaseDocumentAttachedView = item.PhaseDocumentsName;
            $scope.phaseModel.PhaseIndex = index;

            $scope.editPhasebuttonShow = true
        }

        //////////////////////////////////////////////////updatePhase///////////////////////////////////////////////////////////////////////////////////////////////////

        $scope.updatePhase = function (IsNamevalid, Phaseform, phase) {
            
            if (IsNamevalid) {
                Phaseform.phaseName.$dirty = true;
            }
            else {
           
                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].Name = phase.name
                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].Budget = phase.budget;
                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].Description = phase.description;
                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].CreateOn = $scope.createOn;
                $scope.AddNewProjectModel.PhaseList[$scope.phaseModel.PhaseIndex].PhaseDocumentsName = $scope.PhaseDocumentAttachedView;
                $scope.PhaseDocumentAttachedView = [];
                $scope.phaseModel = {};
                Phaseform.phaseName.$dirty = false;
         
                $scope.editPhasebuttonShow = false;

            }


        }


        ////////////////////////////////////////deletePhase////////////////////////////////////////////////////////////////////////////
        var PhasecurrentIndex
        $scope.deletePhaseConfirm = function (index, item) {
            debugger;
            $('#modalConfirmDeletePhase').modal('show')
            $scope.phaseNameONMile = item.Name
            PhasecurrentIndex = index;
          }



        $scope.deletePhase = function (index, item) {
            debugger;
            $scope.AddNewProjectModel.PhaseList.splice(PhasecurrentIndex, 1)
            
            $('#modalConfirmDeletePhase').modal('hide')
            
        }




        /////////////////////////////selectFileForDocumentPhase1//////////////////////////////////////////////////
        var PhaseIndex;
        $scope.selectFileForDocumentPhase1 = function ($files, index) {
            debugger;


            for (var i = 0; i < $files.length; i++) {

                if ($files[0].size < 10485760) {
                    $scope.PhaseDocumentAttachedView.push($files[i])
                  
                }
                else {
                    toaster.error("Error", message.FileSizeExceeds);
                }
            }
        }


        /////////////////////////////////////previousFromProjectWiz///////////////////////////////////////////////////////////
        $scope.previousFromPhaseToRisk = function () {
            debugger;


            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = 'current';
            $scope.myThirdCurrent = '';
            $scope.myForthCurrent = '';

            $scope.hideProjectInfo = false;
            $scope.hideShowRisk = true;
            $scope.hideShowPhaseMile = false;    // 
            $scope.hideShowVendorInfo = false;
            $scope.editPhasebuttonShow = false; // update button hide
        }

        //////////////////////////////////////////////nextFromPhaseToVendor//////////////////////////////////////////////////////
        $scope.nextFromPhaseToVendor = function () {
            debugger;

            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = ''
            $scope.myForthCurrent = 'current'
            $scope.hideProjectInfo = false;
            $scope.hideShowRisk = false;
            $scope.hideShowPhaseMile = false;    // 
            $scope.hideShowVendorInfo = true;

            $scope.editPhasebuttonShow = false; // update button hide
        }

            ////////////////////////////////////////previousFromRisktoProject/////////////////////////////////////////////////////////////

        $scope.previousFromRisktoProject = function () {
            debugger;

            //  class for wizard
            $scope.myFirstCurrent = 'current';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = '';
            $scope.myForthCurrent = '';
            $scope.hideProjectInfo = true;
            $scope.hideShowRisk = false;
            $scope.hideShowPhaseMile = false;    // 
            $scope.hideShowVendorInfo = false;

            $scope.editPhasebuttonShow = false; // update button hide
        }


            //////////////////////////////////////////////nextFromRiskToPhase//////////////////////////////////////////////////////
        $scope.nextFromRiskToPhase = function () {
            debugger;

            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = 'current';
            $scope.myForthCurrent = '';
            $scope.hideProjectInfo = false;
            $scope.hideShowRisk = false;
            $scope.hideShowPhaseMile = true;    // 
            $scope.hideShowVendorInfo = false;

            $scope.editPhasebuttonShow = false; // update button hide
        }

            ////////////////////////////////////////previousFromVendortoProject/////////////////////////////////////////////////////////////

        $scope.previousFromVendortoProject = function () {
            debugger;

            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = 'current';
             $scope.myForthCurrent = '';
            $scope.hideProjectInfo = false;
            $scope.hideShowPhaseMile = false;    // 
            $scope.hideShowVendorInfo = true;

            $scope.editPhasebuttonShow = false; // update button hide
        }

            ////////////////////////////////////////previousFromVendortoProject/////////////////////////////////////////////////////////////

        $scope.previousFromVendortoProject = function () {
            debugger;

            //  class for wizard
            $scope.myFirstCurrent = '';
            $scope.mySecondcCurrent = '';
            $scope.myThirdCurrent = 'current';
            $scope.myForthCurrent = '';
            $scope.hideProjectInfo = false;
            $scope.hideShowRisk = false;
            $scope.hideShowPhaseMile = true;    // 
            $scope.hideShowVendorInfo =false;

            $scope.editPhasebuttonShow = false; // update button hide
        }
        /////////////////////////////////////addMilestone///////////////////////////////////////////////////////////
        $scope.PhaseIndexForMilestone;
        $scope.openMilestone = function (index, item) {
            debugger
            $scope.PhaseIndexForMilestone = index
            $scope.phaseNameONMile = item.Name
            $('#modalAddNewMileStone').modal('show');


        }



        ////////////////////////////////////////////////addNewMilestone///////////////////////////////////////////////////////////////////////////
        $scope.addNewMilestone = function (IsVendorNameValid, AddNewMilestone, milestoneModal) {
            debugger;

            if (IsVendorNameValid) {

                AddNewMilestone.milestoneName.$dirty = true
            }
            else {
                var data = {}
                data.Name = milestoneModal.name;
                data.CreateOn = moment(new Date()).format("MM/DD/YYYY");
                data.Description = milestoneModal.description;
                data.Budget = milestoneModal.budget;
                $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone.push(data);
                $scope.milestoneModal = {};
                
                AddNewMilestone.milestoneName.$dirty = false;
                //$scope.hideProjectInfo = true;
                //$scope.hideShowPhaseMile = false;    // 
                //$scope.editPhasebuttonShow = false; // update button hide

            }
        }
            ////////////////////////////////////////OpenRiskModal/////////////////////////////////////////////////////////////

        $scope.OpenRiskModal = function () {
            debugger;
            $scope.riskModel = {};
            $('#modalAddNewRisk').modal('show');
        }
            ////////////////////////////////////////AddNewRisk/////////////////////////////////////////////////////////////

        $scope.addNewRisk = function (IsRiskTitleInvalid,IsSeverityInvalid,IsStatusInvalid,RiskForm,RiskModel) {
            debugger;
            if (IsRiskTitleInvalid || IsSeverityInvalid || IsStatusInvalid) {
                RiskForm.riskTitle.$dirty = true;
                RiskForm.severity.$dirty = true;
                RiskForm.status.$dirty = true;
            }
            else {
                var data = { RiskTitle: "", Description: "", Severity: "", Status: "", IsDeleted: false, Createdon: "", NoOfComments: 0, RiskComments: [] };
                data.RiskTitle = RiskModel.RiskTitle;
                data.Description = RiskModel.Description;
                data.Severity = RiskModel.Severity.Value;
                data.Status = RiskModel.Status.Value;
                data.Createdon = moment(new Date()).format("MM/DD/YYYY");
                $scope.AddNewProjectModel.RiskList.push(data);
                
                $scope.riskModel = {};
                RiskForm.riskTitle.$dirty = false;
                RiskForm.severity.$dirty = false;
                RiskForm.status.$dirty = false;
                $('#modalAddNewRisk').modal('hide');

            }
            //$('#modalAddNewRisk').modal('show');
        }
        ////////////////////////////////////////////////editMilesonte////////////////////////////////////////////////////////////////////////////
        var mileStoneIndex;
        $scope.editMilesonte = function (index, item) {
            debugger;
            $scope.milestoneModal.name = item.Name;
            $scope.milestoneModal.description = item.Description;
            $scope.milestoneModal.budget = item.Budget
            mileStoneIndex = index;
            $scope.editMilestonebuttonShow = true;
        }

        //////////////////////////////////////////////////////updateMilestone/////////////////////////////////////////////////////////////////////////////////////////////

        $scope.updateMilestone = function (IsVendorNameValid, AddNewMilestone, milestoneModal) {
            debugger;
            if (IsVendorNameValid) {

                AddNewMilestone.milestoneName.$dirty = true;
            }
            else {
                $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone[mileStoneIndex].Name = milestoneModal.name;
                $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone[mileStoneIndex].Description = milestoneModal.description;
                $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone[mileStoneIndex].Budget = milestoneModal.budget
                $scope.milestoneModal = {};
                AddNewMilestone.milestoneName.$dirty = false;
                $scope.editMilestonebuttonShow = false;

            }
        }


        ////////////////////////////////////////////////confirm////////////////////////////////////////////////////////////////////////////
        
        var currentMileIndex
        $scope.deleteConfirmMilestone = function (index, item) {
            $('#modalConfirmDeleteMilestone').modal('show')
            currentMileIndex = index;
        }

        ///////////////////////////////delete///////////////////////////////////////
        $scope.deleteMilestone = function () {
            debugger;
            $scope.AddNewProjectModel.PhaseList[$scope.PhaseIndexForMilestone].MileStone.splice(currentMileIndex, 1);
            $('#modalConfirmDeleteMilestone').modal('hide')
            $scope.editMilestonebuttonShow = false;
        }

        ////////////////////////////////getProjectVendor////////////////////////////////////////////

        var CompanyId = sharedService.getCompanyId()
        getProjectVendor(CompanyId,0)
        function getProjectVendor(CompanyId,ProjectId) {
            debugger
            projectsFactory.getProjectVendor(CompanyId, ProjectId)
        .success(function (data) {
            $scope.AddNewProjectModel.VendorPOCList = data.ResponseData;
            $scope.AddNewProjectModel.VendorList = data.ResponseData;
            getCustomControl(CompanyId);

        }).error(function (data, status, headers, config) {
            toaster.error("Error", "Some Error Occured !");
            $('#globalLoader').hide();
        });

        }

        getAccounts(CompanyId);
        function getAccounts(CompanyId) {
            debugger
            companyAccountFactory.GetCompanyAccByCompanyId(CompanyId)
        .success(function (data) {
            debugger;
            $scope.AddNewProjectModel.CompanyAccList = data.ResponseData;
           // getCustomControl(CompanyId);

        }).error(function (data, status, headers, config) {
            toaster.error("Error", "Some Error Occured !");
            $('#globalLoader').hide();
        });

        }


        function getCustomControl(CompanyId) {

            debugger;
            projectsFactory.getCustomControl(CompanyId)
            .success(function (data) {
                debugger;
               
                $scope.CustomcontrolList = data.ResponseData;
                if ($scope.CustomcontrolList != null && $scope.CustomcontrolList.length > 0) {
                    $scope.CustomDirectiveReady = true;
                }
                else
                {
                    $scope.CustomDirectiveReady = false;
                }
            }).error(function (data, status, headers, config) {
                
                
                toaster.error("Error", "Some Error Occured !");
                $('#globalLoader').hide();
            });

        }



            /////////////////////////////////////changeVendor Add Vendor for Project////////////////////////////////////////////////////////////
        $scope.changeVendor = function (item, x) {

            debugger;
            if (item != null) {
                var currentAccSelected = $filter('filter')($scope.AddNewProjectModel.CompanyAccList, $scope.SelectedAcc)[0];
                if (currentAccSelected != null && currentAccSelected.Id != null) {
                    var currentSelected = $filter('filter')($scope.AddNewProjectModel.VendorPOCList, item)[0];
                    for (var i = 0; i < $scope.AddNewProjectModel.VendorList.length; i++) {
                        if ($scope.AddNewProjectModel.VendorList[i].VendorId == item.VendorId) {
                            $scope.AddNewProjectModel.VendorList[i].IsAdded = true;
                            break;
                        }
                    }
                    currentSelected.IsAdded = true;

                    currentAccSelected.IsAdded = true;
                    
                    $scope.SelectedAcc = {};
                }
            }

        }

        $scope.TempVendorList = [];
            /////////////////////////////////////changeVendor Add Vendor for Project////////////////////////////////////////////////////////////
        $scope.changeAccount = function (item, x) {

            debugger;
            if (item != null) {
                var currentSelected = $filter('filter')($scope.AddNewProjectModel.CompanyAccList, item)[0];
                for (var i = 0; i < $scope.AddNewProjectModel.CompanyAccList.length; i++) {
                    if ($scope.AddNewProjectModel.CompanyAccList[i].VendorId == currentSelected.VendorId) {
                        $scope.AddNewProjectModel.CompanyAccList[i].IsAdded = true;
                        //break;
                    }
                }
                getProjectVendorByCompIdId(CompanyId, currentSelected.Id);
                $scope.SelectedAcc = currentSelected;
               // currentSelected.IsAdded = true;
            }

        }

        function getProjectVendorByCompIdId(CompanyId, AccId) {
            debugger;
            projectsFactory.getProjectVendorByCompIdId(CompanyId, AccId)
.success(function (data) {
    $scope.TempVendorList = [];
    debugger;
    $scope.TempVendorList = $scope.AddNewProjectModel.VendorPOCList;
    $scope.AddNewProjectModel.VendorPOCList = data.ResponseData;
    //getCustomControl(CompanyId);

}).error(function (data, status, headers, config) {
    toaster.error("Error", "Some Error Occured !");
    $('#globalLoader').hide();
});

        }
        /////////////////////////////////////////////////openModalRemoveVendor///////////////////////////////////////////////////////////////////////////////////////////

        var currentDelVendorIndex;
        $scope.openModalRemoveVendor = function (index) {
            debugger;
            currentDelVendorIndex = index;
            $('#modalConfirmDeleteVendor').modal('show');
            
        }

        /////////////////////////////////////////////////removeVendor///////////////////////////////////////////////////////////////////////////////////////////

        $scope.removeVendor = function (item) {
            debugger;
            var currentSelected = $filter('filter')($scope.AddNewProjectModel.VendorList, $scope.AddNewProjectModel.VendorList[currentDelVendorIndex])[0];
            currentSelected.IsAdded = false;
            for (var i = 0; i < $scope.AddNewProjectModel.VendorPOCList.length; i++) {
                if ($scope.AddNewProjectModel.VendorPOCList[i].VendorId == currentSelected.VendorId) {
                    $scope.AddNewProjectModel.VendorPOCList[i].IsAdded = false;
                    break;
                }
            }
            for (var i = 0; i < $scope.AddNewProjectModel.CompanyAccList.length; i++) {
                if ($scope.AddNewProjectModel.CompanyAccList[i].VendorId == currentSelected.VendorCompanyId) {
                    $scope.AddNewProjectModel.CompanyAccList[i].IsAdded = false;
                    //break;
                }
            }
            $('#modalConfirmDeleteVendor').modal('hide');
        }




        //////////////////////////////////////////////removeProjectAttachments/////////////////////////////////////////////////////
        

        $scope.removeProjectAttachments = function (index) {
            debugger;
            
            $scope.AddNewProjectModel.ProjectDocument.splice(index, 1)
        }




        //    testing with form data
        $scope.SavenewProject = function (modal, IsQuoteSent) {
           
            debugger;
               

            if ($scope.AddNewProjectModel.VendorList.length == 0)
            {
                toaster.warning("Warning", message.notSelectedVenodr);
                return;
            }

            $('#globalLoader').show();
            $scope.AddNewProjectModel.ProjectId = 0;
            $scope.AddNewProjectModel.ProjectName  = modal.projectName;
            $scope.AddNewProjectModel.ProjectStartDate = modal.projectStartDate;
            $scope.AddNewProjectModel.ProjectEndDate = modal.projectEndDate
            $scope.AddNewProjectModel.ProjectBudget = modal.projectBudget == "" ? 0 : modal.projectBudget;
            $scope.AddNewProjectModel.ProjectDescription = modal.projectDescription;
            $scope.AddNewProjectModel.IsQuoteSent = IsQuoteSent;
            $scope.AddNewProjectModel.CustomControlData = JSON.stringify($scope.customcontrolmodel);

            var projecturl = apiURL.baseAddress + "ProjectsApi/";
            $scope.jsonData =  $scope.AddNewProjectModel;;
            $http({
                method: 'POST',
                url: projecturl + "CreateNewProject",
                headers: { 'Content-Type': undefined },

                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("model", angular.toJson(data.model));
                    
                    for (l = 0; l < $scope.AddNewProjectModel.ProjectDocument.length; l++) {
                        formData.append("Project", $scope.AddNewProjectModel.ProjectDocument[l]);

                    }

                    for (i = 0; i < $scope.AddNewProjectModel.PhaseList.length; i++)
                    {
                        for (l = 0; l < $scope.AddNewProjectModel.PhaseList[i].PhaseDocumentsName.length; l++) {
                            formData.append("Phase_"+i+"_"+l, $scope.AddNewProjectModel.PhaseList[i].PhaseDocumentsName[l]);
                        }
                    }

                    return formData;
                },

                data: { model: $scope.jsonData, files: $scope.files }
            }).
                success(function (data, status, headers, config) {
                    debugger;
                    $('#globalLoader').hide();

                    if (data.success) {
                        if (data.ResponseData == 1) {
                            toaster.success("Success", message.NewProjectCreateSuccesfully);
                            $location.path('/quoted/project');
                        }
                        else if (data.ResponseData == 2) {
                            toaster.success("Success", message.NewProjectCreatedAndEmailSent);
                            $location.path('/quoted/project');
                        }

                        else if (data.ResponseData == -1) {
                            toaster.warning("Warning", message.DuplicateProject);
                        }

                    }
                    else {
                        toaster.success("error", message.error);
                    }
                    
                }).
                error(function (data, status, headers, config) {
                    $('#globalLoader').hide();
            
                });


        }

            /////////////////////////////////////addMilestone///////////////////////////////////////////////////////////
        $scope.RiskIndexForComment;
        $scope.hideShowRiskComment = false;
        $scope.openRiskComment = function (index, item) {
            debugger
            $scope.RiskIndexForComment = index;
            $scope.riskTitleONComment = item.RiskTitle;
            $scope.descriptionOnComment = item.Description;
            $scope.severityOnComment = item.Severity;
            $scope.statusOnComment = item.Status;
            $scope.riskCommentModal = {};
            $scope.hideShowRiskComment = true;
            $('#modalAddNewComment').modal('show');


        }
            ////////////////////////////////////////////////addNewComment///////////////////////////////////////////////////////////////////////////
        $scope.addNewComment = function (IsCommentValid, AddNewRiskComment, riskCommentModal) {
            debugger;

            if (IsCommentValid) {

                AddNewRiskComment.comment.$dirty = true;
            }
            else {
                var data = {}
                data.Comment = riskCommentModal.Comment;
                data.Createdon = moment(new Date()).format("MM/DD/YYYY");
               
                data.UserName = sharedService.getNameId() + " " + sharedService.getLastNameId();
                
                data.UserId = sharedService.getUserId();
                
                $scope.AddNewProjectModel.RiskList[$scope.RiskIndexForComment].RiskComments.push(data);
                $scope.riskCommentModal = {};

                AddNewRiskComment.comment.$dirty = false;
                //$scope.hideProjectInfo = true;
                //$scope.hideShowPhaseMile = false;    // 
                //$scope.editPhasebuttonShow = false; // update button hide

            }
        }
        var riskCommentIndex;
        $scope.updateComment = function (IsCommentValid, AddNewRiskComment, riskCommentModal) {
            debugger;
            if (IsCommentValid) {

                AddNewRiskComment.comment.$dirty = true;
            }
            else {
                $scope.AddNewProjectModel.RiskList[$scope.RiskIndexForComment].RiskComments[riskCommentIndex].Comment = riskCommentModal.Comment;
                $scope.AddNewProjectModel.RiskList[$scope.RiskIndexForComment].RiskComments[riskCommentIndex].Createdon = moment(new Date()).format("MM/DD/YYYY");
              
                $scope.riskCommentModal = {};
                AddNewRiskComment.comment.$dirty = false;
                $scope.editRiskCommentbuttonShow = false;

            }
        }
            ////////////////////////////////////////////////editComment////////////////////////////////////////////////////////////////////////////
        
        $scope.editComment = function (index, item) {
            debugger;
            $scope.riskCommentModal.Comment = item.Comment;
            riskCommentIndex = index;
            $scope.editRiskCommentbuttonShow = true;
        }
            ////////////////////////////////////////////////confirm////////////////////////////////////////////////////////////////////////////

        var currentCommentIndex
        $scope.deleteConfirmComment = function (index, item) {
            $('#modalConfirmDeleteComment').modal('show')
            currentCommentIndex = index;
        }
            ///////////////////////////////delete comment///////////////////////////////////////
        $scope.deleteRiskComment = function () {
            debugger;
            $scope.AddNewProjectModel.RiskList[$scope.RiskIndexForComment].RiskComments.splice(currentCommentIndex, 1);
            $('#modalConfirmDeleteComment').modal('hide')
            $scope.editRiskCommentbuttonShow = false;
        }

    })
})


