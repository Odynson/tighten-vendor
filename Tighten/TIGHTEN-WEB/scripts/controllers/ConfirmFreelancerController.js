﻿define(['app', 'ngCookies'], function (app) {

    app.controller("ConfirmFreelancerController", function ($scope, $location, toaster, sharedService, apiURL, FreelancerAccountFactory,$stateParams) {

        angular.element(document).ready(function () {
            
            var key = $stateParams.key;
            FreelancerAccountFactory.getFreelancerInfoForRegistration(key)
            .success(function (response) {
                
                sharedService.setFreelancerInfoInShared(response.ResponseData);
                if (response.ResponseData.IsFreelancerUserIdExist == null) {
                    $location.path('/signup');
                }
                else {
                    $location.path('/login');
                }
               
            })
            .error(function (response) {
                toaster.error("Error", message[response.message]);
            })


        })


    });

});