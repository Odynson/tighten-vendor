﻿define(['app'], function (app) {
    //(function () {
    //    'use strict';

    app.controller('globalController', function ($scope, $filter, DropBoxSettings, $sce, $window, toaster, $confirm, lkGoogleSettings,
                    $parse, $compile, $http, $timeout, userFeedbackFactory, clientDashboardFactory, clientFeedbackFactory, userDashboardFactory,
                    assigneeFactory, subtodoFactory, projectUsersFactory, teamFactory, accountFactory, notificationFactory, emailFactory,
                    sectionsFactory, followersFactory, usersFactory, attachmentsFactory, projectsFactory, todosFactory, commonFactory, $upload,
                    uiCalendarConfig, todoNotificationsFactory, CalendarEventsFactory, companiesFactory) {

        $('#divTeam').show();
        $scope.filteredPeople = [];
        $scope.stretchCentreWindow = true;
        $scope.hideRightWindow = true;
        $scope.selectedAssignee = undefined;
        $scope.selectedToDo = {};
        $scope.todoNotificationsData = {};
        $scope.to1dotypeflag = 0;
        $scope.todo = {};
        $scope.subtodo = {};
        $scope.team = {};
        $scope.project = {};
        $scope.user = {};
        $scope.userinfo = {};
        $scope.MyProfileObject = {};
        $scope.notification = {};
        $scope.projectUsers = {};
        $scope.CompanyObject = {};
        $scope.flag = false;
        getUserDetail();


        /* Generate GUID in Javascript*/

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
              s4() + '-' + s4() + s4() + s4();
        }

        /* Common Function for closing all windows */
        function closeCentreSections() {

            $scope.selectedUserFeedback = "";
            $scope.selectedTodo = "";
            $scope.selectedProject = "";
            $scope.selectedNavigation = "";
            $scope.selectedWindow = "";
            $scope.stretchCentreWindow = true;
            $scope.hideRightWindow = true;
            $('#divProjectOverview').hide();
            $('#divToDoDescriptionSection').hide();
            $('#divTodoManagementSection').hide();
            //$('#divMyDashboard').hide();
            $('#divMyInbox').hide();
            $('#divNotifications').hide();
            $('#divProfile').hide();
            $('#divCompanyProfile').hide();
            $('#divMyTodoes').hide();
            $('#divCalendar').hide();
            $('#divTeam').hide();
            $('#divNotes').hide();
            $('#divLinks').hide();
            $('#divFiles').hide();
            $('#divFavourites').hide();
            $('#divUserManagementSection').hide();
            $('#divTeamManagementSection').hide();
            $('#divProjectManagementSection').hide();
            $('#divMyTodosManagementSection').hide();
            $('#divClientDashboard').hide();
            $('#divUserFeedbackManagementSection').hide();
            $('#divNotificationManagementSection').hide();
            $('#divInboxManagementSection').hide();

        };

        /* Common Function for closing Right Sections */
        function closeRightSections() {

            $('#divToDoDescriptionSection').hide();
            $('#divEventDescriptionSection').hide();
            $('#divProjectOverview').hide();
            $('#divUserFeedbackDescriptionSection').hide();
            $('#divTeamDescriptionSection').hide();
            $('#divProjectDescriptionSection').hide();

        }

        /* Getting the details of Logged in user */
        function getUserDetail() {
            
            $scope.temporaryId = 1;

            function hideloader() {
                closeCentreSections();
                $('#divMasterLoader').hide();
                $scope.selectedNavigation = "MyDashboard";
               // $('#divMyDashboard').show();

                $scope.setOverFlowY = "auto !important";
            };


            /*  Fetching User Details */
            usersFactory.getUser($scope.temporaryId)
                       .success(function (data) {

                           if (data.success) {

                               $scope.NotifyCount = data.responseData.notificationCount == 0 ? null : data.responseData.notificationCount;
                               $scope.InboxCount = data.responseData.inboxCount == 0 ? null : data.responseData.inboxCount;
                               $scope.hideNotifyCount = data.responseData.notificationCount == 0 ? true : false;
                               $scope.hideInboxCount = data.responseData.inboxCount == 0 ? true : false;
                               $scope.CompanyObject.companyId = data.responseData.companyId;
                               $scope.CompanyObject.companyName = data.responseData.companyName;
                               $scope.IsAdminLoggedIn = data.responseData.isAdminLoggedIn;
                               $scope.SendNotification = data.responseData.sendNotificationEmail;
                               $scope.MyProfileObject = data.responseData;
                               $scope.teamName = data.responseData.teamName;

                               $timeout(function () {
                                   $scope.$apply();

                                   /* Fetching User Teams  */
                                   teamFactory.getLoggedInUserTeams().success(function (data) {

                                       if (data.success) {
                                           $scope.TeamsObject = data.responseData;

                                           $scope.activeTeam = $scope.TeamsObject[0];

                                           $timeout(function () {
                                               $scope.$apply();

                                               /* Getting User Projects for Selected Team */
                                               projectsFactory.getProjects($scope.activeTeam.teamId).success(function (data) {

                                                   if (data.success) {

                                                       $scope.projects = data.responseData;


                                                       /*Getting User Upcoming Todos*/

                                                       userDashboardFactory.getUserUpcomingTodos().success(function (data) {

                                                           if (data.success) {
                                                               $scope.userUpcomingTodosObject = data.responseData;

                                                               /* Getting User Upcoming Events */

                                                               userDashboardFactory.getUserUpcomingEvents().success(function (data) {

                                                                   if (data.success) {
                                                                       $scope.userUpcomingEventsObject = data.responseData;



                                                                       /* Getting User T Line */

                                                                       userDashboardFactory.getUserTLine($scope.MyProfileObject.userId).success(function (data) {

                                                                           if (data.success) {
                                                                               $scope.userTLineObject = data.responseData;


                                                                               /* Getting Client Feedbacks associated with the User  */

                                                                               clientFeedbackFactory.getUserClientFeedbacks().success(function (data) {

                                                                                   if (data.success) {
                                                                                       $scope.userClientFeedbacksObject = data.responseData;

                                                                                       $timeout(function () {
                                                                                           $scope.$apply();

                                                                                           $timeout(hideloader, 1000);
                                                                                       });



                                                                                   }
                                                                                   else {

                                                                                       toaster.error("Error", data.message);
                                                                                   }

                                                                               }).error(function () {

                                                                                   toaster.error("Error", "Some Error Occured !");
                                                                               });

                                                                           }
                                                                           else {

                                                                               toaster.error("Error", data.message);
                                                                           }

                                                                       }).error(function () {

                                                                           toaster.error("Error", "Some Error Occured !");
                                                                       });


                                                                   }
                                                                   else {

                                                                       toaster.error("Error", data.message);
                                                                   }

                                                               }).error(function () {

                                                                   toaster.error("Error", "Some Error Occured !");
                                                               });


                                                           }
                                                           else {

                                                               toaster.error("Error", data.message);
                                                           }

                                                       }).error(function () {

                                                           toaster.error("Error", "Some Error Occured !");
                                                       });






                                                   }
                                                   else {
                                                       toaster.error("Error", data.message);
                                                   }

                                               }).error(function () {
                                                   toaster.error("Error", "Some Error Occured !");
                                               });

                                           });
                                       }
                                       else {
                                           toaster.error("Error", data.message);
                                       }


                                   }).error(function () {

                                       toaster.error("Error", "Some Error Occured!");
                                   });
                               });



                           }
                           else {

                               toaster.error("Error", data.message);
                           }


                       })
                        .error(function () {
                            toaster.error("Error", "Some Error Occured !");
                        });
        }

        /*  Getting projects related to Selected Team */
        function getProjects() {


            projectsFactory.getProjects($scope.activeTeam.teamId).success(function (data) {

                if (data.success) {

                    $scope.projects = data.responseData;
                }
                else {
                    toaster.error("Error", data.message);
                }

            }).error(function () {
                toaster.error("Error", "Some Error Occured !");
            });

        }


        /* Getting the list of Teams in the company */
        function getTeams() {
            $scope.projectsloader = true;
            teamFactory.getLoggedInUserTeams().success(function (data) {

                if (data.success) {
                    $scope.TeamsObject = data.responseData;

                    $scope.activeTeam = $scope.TeamsObject[0];

                    $timeout(function () {
                        $scope.$apply();

                        getProjects();

                    });
                }
                else {
                    toaster.error("Error", data.message);
                }


            }).error(function () {

                toaster.error("Error", "Some Error Occured!");
            });
        }


        /* Showing unassigned todos of the logged in user on default window */
        function getMyTodosDetail() {


            $scope.selectedWindow = "unassigned";
            $scope.selectedtodo = "unassigned";
            $scope.selectedNavigation = 'MyTodos';
            $scope.myunassignedtodos = "";
            $scope.hideRightWindow = true;
            $scope.stretchCentreWindow = true;
            $('#divMyTodoes').show();



            todosFactory.getMyUnassignedToDos()
                                .success(function (data) {
                                    $scope.myunassignedtodos = data;
                                    if (data.length == 0) {

                                        $scope.showNoTodosRecordsMessage = true;
                                    }
                                    $timeout(function () {
                                        $scope.$apply();
                                        $scope.mytodoswindowloader = false;
                                        $scope.myunassignedtodoswindow = true;
                                    });


                                })
                                 .error(function (error) {
                                     $scope.status = 'Unable to load TODOs: ' + error.message;
                                 });
        }



        /* Get ToDos corresponding to project */
        $scope.getToDos = function getToDos(item) {
            closeCentreSections();
            $scope.editProjectUsers = item.display;
            $scope.selectedWindow = "defaultTodosWindow";
            $scope.hideRightWindow = false;
            $scope.stretchCentreWindow = false;
            $('#divrightloader').show();
            $scope.selectedtodo = "list";

            $scope.selectedNavigation = '';
            $scope.selectedProject = item.id;
            $scope.selectedTodo = '';
            $('#divTodoManagementSection').show();
            $scope.projectId = item.id;
            $scope.projectName = item.name;
            $scope.projectDescription = item.description;
            $scope.IsProjectPrivate = item.isPrivate;
            $scope.todotypeflag = 0;
            // $('#ajax_loader').show();

            todosFactory.getToDos($scope.projectId).success(function (data) {
                GetSections();
                $scope.todos = data;

                $timeout(function () {
                    $scope.$apply();

                    /* Getting Members related to this project */
                    projectUsersFactory.getProjectUsers($scope.projectId).success(function (data) {
                        $scope.projectUsersCount = data.length > 1 ? data.length + " Members" : data.length + " Member";
                        $scope.existingProjectUsers = data;

                        $timeout(function () {
                            $scope.$apply();

                            /* Getting other details of this project */
                            projectsFactory.getProject($scope.projectId).success(function (data) {

                                $scope.TotalTodosCount = data[0].totalTodosCount;
                                $scope.CompletedTodosCount = data[0].completedTodosCount;
                                $scope.InProgressTodosCount = data[0].inProgressTodosCount;
                                $scope.PendingTodosCount = data[0].pendingTodosCount;
                                $timeout(function () {
                                    $scope.$apply();
                                    $scope.txtSearchProject = '';

                                    $timeout(hideloader, 500);
                                });

                                function hideloader() {
                                    //$('#ajax_loader').hide();


                                    $('#divProjectOverview').show();
                                    $('#divrightloader').hide();
                                };
                            });
                        });

                    });
                });
            })
                                   .error(function (error) {
                                   });
        }


        /* Get Only Todo Comments in arranged mode */

        function getTodoCommentsInArrangedMode(todoid) {

            todosFactory.getToDoComments(todoid).success(function (data) {
                $scope.todocomments = [];
                var totalLoop = data.length;
                var FinalDescription = [];
                if (totalLoop > 0) {

                    for (var i = 0; i < totalLoop; i++) {
                        var description = data[i].description;
                        var createdBy = data[i].createdBy;
                        var isOwner = data[i].isOwner;
                        var createdDate = data[i].createdDate;
                        var creatorProfilePhoto = data[i].creatorProfilePhoto;
                        var id = data[i].id;
                        var splittedDesc = description.split('@@');
                        var temsplittedDescFiles = description.split('##');
                        var newUserCount = splittedDesc.length - 1;
                        var newFileCount = temsplittedDescFiles.length - 1;
                        var splittedDescForFiles = '';
                        var newCommentWithUserReplacements = '';
                        var newCommentWithFileReplacements = '';

                        if (newUserCount > 0) {
                            for (var j = 0; j < newUserCount; j++) {
                                newCommentWithUserReplacements += splittedDesc[j] + "<a class='taggedUsers' href='#'> <i class='fa fa-user'></i> " + data[i].usersTagged[j].fullName + "</a>";

                            }
                            newCommentWithUserReplacements += splittedDesc[newUserCount];
                            splittedDescForFiles = newCommentWithUserReplacements.split('##');
                        }
                        else {
                            newCommentWithUserReplacements = description;
                            splittedDescForFiles = description.split('##');
                        }

                        if (newFileCount > 0) {
                            for (var k = 0; k < newFileCount; k++) {

                                if (data[i].filesTagged[k].isGif == false) {
                                    newCommentWithFileReplacements += splittedDescForFiles[k] + "<a class='taggedUsers' href='" + data[i].filesTagged[k].filePath + "' target='_blank' > <i class='fa fa-file'></i> " + data[i].filesTagged[k].originalName + "</a>";
                                }
                                else {
                                    newCommentWithFileReplacements += splittedDescForFiles[k] + "<a class='taggedUsers' href='" + data[i].filesTagged[k].filePath + "' target='_blank' > <i class='fa fa-file-movie-o'></i> gif:" + data[i].filesTagged[k].originalName + "</a>" + "<img class='taggedGifInComment' width='371' height='200' src='" + data[i].filesTagged[k].filePath + "'  />";
                                }

                            }
                            newCommentWithFileReplacements += splittedDescForFiles[newFileCount];


                        }
                        else {

                            newCommentWithFileReplacements = newCommentWithUserReplacements;
                        }
                        $scope.pushComment = {};
                        $scope.pushComment.description = newCommentWithFileReplacements;
                        $scope.pushComment.createdBy = createdBy;
                        $scope.pushComment.id = id;
                        $scope.pushComment.isOwner = isOwner;
                        $scope.pushComment.createdDate = createdDate;
                        $scope.pushComment.creatorProfilePhoto = creatorProfilePhoto;

                        $scope.todocomments.push($scope.pushComment);
                        //var lastDesc = { description: newCommentWithFileReplacements, createdBy: createdBy, id: id, creatorProfilePhoto: creatorProfilePhoto };
                        //FinalDescription.push(lastDesc);


                    }


                }
            })

        };

        /* Get ToDo Comments corresponding to ToDo/Task and Attachmets corresponding to ToDo/Task */
        $scope.getToDoDetails = function (toDoId, item, projectid) {

            if ($scope.selectedWindow == "defaultTodosWindow") {
                //$scope.indexOfTodo = $scope.todos.indexOf(item);
            }

            $('#divProjectOverview').hide();
            $scope.stretchCentreWindow = false;
            $scope.hideRightWindow = false;
            $('#txtComments').html('');
            $('#txtOthersToDoComments').html('');
            $scope.showassigneesearch = false;
            $scope.showattachmentsearch = false;
            $scope.AllAssigneesForComment = null;
            $scope.AllAttachmentsForComment = null;
            $('#divrightloader').show();
            $('#divToDoDescriptionSection').show();
            //$('#divToDoDescriptionSection').fadeOut(300);
            $scope.selectedTodo = toDoId;
            $scope.hideFollower = false;
            $scope.toDoId = toDoId;
            $scope.ToDoProjectName = $scope.projectName;


            /*Getting ToDo Details Of Particular ToDo*/
            todosFactory.getSingleToDo($scope.toDoId)
                                              .success(function (data) {
                                                  $scope.singleTodoDescription = data;
                                                  $scope.ToDoTaskName = data[0].name;
                                                  $scope.projectName = data[0].projectName;
                                                  $scope.txtAssignees = data[0].assignedTo;
                                                  $scope.inboxToDoDescription = data[0].description;
                                                  $scope.dt1 = data[0].deadlineDate;
                                                  $scope.hideEditFollower = data[0].display;
                                                  $scope.assigneeEmailId = data[0].email;
                                                  $scope.InProgress = data[0].inProgress;
                                                  $scope.IsDone = data[0].isDone;
                                                  $scope.sectionName = data[0].sectionName;
                                                  $timeout(function () {
                                                      $scope.$apply();

                                                      todoNotificationsFactory.getTodoNotifications($scope.toDoId).success(function (data) {
                                                          $scope.todoNotificationsData = data;

                                                          $timeout(function () {
                                                              $scope.$apply();

                                                              projectUsersFactory.getProjectUsers(projectid).success(function (data) {

                                                                  $scope.existingProjectUsers = data;
                                                                  $scope.AllAssigneesForComment = $scope.existingProjectUsers;
                                                                  $timeout(function () {
                                                                      $scope.$apply();

                                                                      subtodoFactory.getSubToDos($scope.toDoId).success(function (data) {

                                                                          $scope.SubTodosList = data;
                                                                          $timeout(function () {
                                                                              $scope.$apply();



                                                                              /* Getting Attachments Corresponding To ToDo/Task*/
                                                                              attachmentsFactory.getToDoAttachments($scope.toDoId).success(function (data) {
                                                                                  $scope.uploads = data;
                                                                                  $scope.AllAttachmentsForComment = $scope.uploads;
                                                                                  $scope.LocalAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'computer.png' });
                                                                                  $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'logo-drive.png' });
                                                                                  $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'drop-box.png' });
                                                                                  $scope.BoxAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'box-icon.png' });
                                                                                  $timeout(function () {
                                                                                      $scope.$apply();

                                                                                      /*Getting ToDo Followers Of Particular ToDo*/
                                                                                      $scope.getFollowers(toDoId);

                                                                                  });

                                                                                  $timeout(function () {
                                                                                      $scope.$apply();
                                                                                      /*Getting Todo Comments*/
                                                                                      getTodoCommentsInArrangedMode(toDoId);

                                                                                  });

                                                                                  $timeout(function () {
                                                                                      $scope.$apply();
                                                                                      $timeout(hideloader, 500);
                                                                                  });

                                                                                  function hideloader() {

                                                                                      $('#divrightloader').hide();
                                                                                  };
                                                                              })
                                                                                                      .error(function (error) {
                                                                                                      });



                                                                          });

                                                                      });
                                                                  });
                                                              });

                                                          });
                                                      });
                                                  });

                                              })
                                               .error(function (error) {

                                               });

        }


        /*Opening SubTodo add Modal*/
        $scope.ShowSubTodoAddModal = function () {

            $timeout(function () {
                $scope.$apply();

                $('#modalSubTodo').modal('show');

            });

        };


        /* Save new SubToDo.  */
        $scope.saveSubToDo = function saveToDo() {

            /* This is the global Loader which will be visible until the SubTodo is added and todo list is refreshed*/
            $('#globalLoader').show();

            $scope.subtodo.toDoId = $scope.toDoId;

            subtodoFactory.insertSubToDo($scope.subtodo)
                                     .success(function (data) {
                                         subtodoFactory.getSubToDos($scope.toDoId)
                                    .success(function (data) {

                                        $scope.SubTodosList = data;

                                        $timeout(function () {
                                            $scope.$apply();
                                            $scope.subtodo.name = "";
                                            toaster.success("TODO Saved Successfully");
                                            $('#globalLoader').hide();
                                            $('#modalSubTodo').modal('hide');
                                        });
                                    });

                                     })
                                      .error(function (error) {
                                          $scope.status = 'Unable to save ToDo : ' + error.message;
                                          $('#globalLoader').hide();
                                      });
        }

        /* Update SubToDo */
        $scope.updateSubTodo = function (item) {
            $('#globalLoader').show();
            $scope.updateSubTodoObject = {};

            $scope.updateSubTodoObject.id = item.id;
            $scope.updateSubTodoObject.isDone = !item.isDone;

            subtodoFactory.updateSubToDo($scope.updateSubTodoObject).success(function (data) {
                $('#globalLoader').hide();
                item.isDone = !item.isDone;
                toaster.success("SubTodo updated Successfully");

            });

        };

        /* Confirm Delete SUbTodo*/
        $scope.confirmDeleteSubToDo = function (item) {

            item.ConfirmDeleteSubTodoStatus = true;

        };


        /* Delete SubToDo  */
        $scope.deleteSubToDo = function deleteToDo(item, index) {

            $('#globalLoader').show();

            subtodoFactory.deleteSubToDo(item.id).success(function (data) {

                $scope.SubTodosList.splice(index, 1);
                toaster.success("SubTodo Deleted Successfully", "Success");
                $('#globalLoader').hide();


            }).error(function (error) { });

        }

        /* Cancel Delete Subtodo */
        $scope.CancelDeleteSubTodo = function (item) {

            item.ConfirmDeleteSubTodoStatus = false;

        };

        /* Get Notifications todo details*/
        $scope.getNotificationsTodoDetail = function (item) {

            $('#divProjectOverview').hide();
            $scope.hideRightWindow = false;
            $scope.stretchCentreWindow = false;
            $scope.selectedNotification = item.id;
            $('#txtComments').html('');
            $('#txtOthersToDoComments').html('');
            $scope.showassigneesearch = false;
            $scope.showattachmentsearch = false;
            $scope.AllAssigneesForComment = null;
            $scope.AllAttachmentsForComment = null;
            $('#divrightloader').show();
            $('#divToDoDescriptionSection').show();
            //$('#divToDoDescriptionSection').fadeOut(300);
            $scope.selectedTodo = item.todoId;
            $scope.hideFollower = false;
            $scope.toDoId = item.todoId;
            $scope.ToDoProjectName = $scope.projectName;

            if (item.isRead == false) {

                item.isRead = true;
                /* Updating Notification IsRead*/
                $scope.NotifyCount = $scope.NotifyCount - 1;
                if ($scope.NotifyCount == 0) {

                    $scope.hideNotifyCount = true;
                }
                notificationFactory.updateNotification(item.id).success(function () {

                    //notificationFactory.getNotifications().success(function (data) {

                    //    $scope.notificationdata = data;

                    //});

                });
            }



            /*Getting ToDo Details Of Particular ToDo*/
            todosFactory.getSingleToDo($scope.toDoId)
                                              .success(function (data) {
                                                  $scope.singleTodoDescription = data;
                                                  $scope.ToDoTaskName = data[0].name;
                                                  $scope.projectName = data[0].projectName;
                                                  $scope.txtAssignees = data[0].assignedTo;
                                                  $scope.todoCreatedBy = data[0].assignedBy;
                                                  $scope.inboxToDoDescription = data[0].description;
                                                  $scope.dt1 = data[0].deadlineDate;
                                                  $scope.hideEditFollower = data[0].display;
                                                  $scope.assigneeEmailId = data[0].email;
                                                  $scope.InProgress = data[0].inProgress;
                                                  $scope.IsDone = data[0].isDone;
                                                  $scope.sectionName = data[0].sectionName;
                                                  $timeout(function () {
                                                      $scope.$apply();

                                                      projectUsersFactory.getProjectUsers(item.projectId).success(function (data) {

                                                          $scope.existingProjectUsers = data;
                                                          $timeout(function () {
                                                              $scope.$apply();

                                                              /* Getting Comments Corresponding To ToDo/Task*/
                                                              todosFactory.getToDoComments($scope.toDoId)
                                                                                     .success(function (data) {

                                                                                         $scope.todocomments = [];
                                                                                         var totalLoop = data.length;
                                                                                         var FinalDescription = [];
                                                                                         if (totalLoop > 0) {

                                                                                             for (var i = 0; i < totalLoop; i++) {
                                                                                                 var description = data[i].description;
                                                                                                 var createdBy = data[i].createdBy;
                                                                                                 var id = data[i].id;
                                                                                                 var usersloop = data[i].usersTagged.length;
                                                                                                 var filesloop = data[i].filesTagged.length;
                                                                                                 var splittedDesc = description.split('@@');
                                                                                                 var temsplittedDescFiles = description.split('##');
                                                                                                 var newUserCount = splittedDesc.length - 1;
                                                                                                 var newFileCount = temsplittedDescFiles.length - 1;
                                                                                                 var intForExtraDescForUsers = usersloop;
                                                                                                 var intForExtraDescForFile = filesloop;
                                                                                                 var newComment = '';
                                                                                                 if (splittedDesc.length > 0 && temsplittedDescFiles.length > 0) {

                                                                                                     for (var j = 0; j < newUserCount ; j++) {

                                                                                                         newComment += splittedDesc[j] + " <a>" + data[i].usersTaggedFirstNames[j].firstName + "</a>";

                                                                                                     }
                                                                                                     newComment += splittedDesc[newUserCount];
                                                                                                     var splittedDescForFile = newComment.split('##');
                                                                                                     var newfilecount = splittedDescForFile.length - 1;
                                                                                                     var veryNewComment = '';
                                                                                                     if (splittedDescForFile.length > 0) {

                                                                                                         if (filesloop > 0) {
                                                                                                             for (var j = 0; j < newfilecount; j++) {

                                                                                                                 veryNewComment += splittedDescForFile[j] + " <a href='" + data[i].path[j].filePath + "' target='_blank' >" + data[i].originalName[j].originalName + "</a>";


                                                                                                             }
                                                                                                             veryNewComment += splittedDescForFile[newfilecount];
                                                                                                         }

                                                                                                     }
                                                                                                     var lastDesc = { description: veryNewComment, createdBy: createdBy, id: id };
                                                                                                     FinalDescription.push(lastDesc);
                                                                                                 }
                                                                                                 else if (splittedDesc.length > 0 && temsplittedDescFiles.length == 0) {

                                                                                                     for (var j = 0; j < newUserCount ; j++) {

                                                                                                         if (data[i].usersTaggedFirstNames[j].firstName == null) {


                                                                                                         }
                                                                                                         else {

                                                                                                             newComment += splittedDesc[j] + " <a>" + data[i].usersTaggedFirstNames[j].firstName + "</a>";
                                                                                                         }

                                                                                                     }
                                                                                                     newComment += splittedDesc[newUserCount];


                                                                                                     var lastDesc = { description: newComment, createdBy: createdBy, id: id };
                                                                                                     FinalDescription.push(lastDesc);
                                                                                                 }
                                                                                                 else if (splittedDesc.length == 0 && temsplittedDescFiles.length > 0) {

                                                                                                     var veryNewComment = '';
                                                                                                     if (splittedDescForFile.length > 0) {

                                                                                                         if (filesloop > 0) {
                                                                                                             for (var j = 0; j < newFileCount; j++) {

                                                                                                                 veryNewComment += temsplittedDescFiles[j] + " <a href='" + data[i].path[j].filePath + "' target='_blank' >" + data[i].originalName[j].originalName + "</a>";


                                                                                                             }
                                                                                                             veryNewComment += temsplittedDescFiles[newfilecount];
                                                                                                         }

                                                                                                     }
                                                                                                     var lastDesc = { description: veryNewComment, createdBy: createdBy, id: id };
                                                                                                     FinalDescription.push(lastDesc);
                                                                                                 }
                                                                                                 else {

                                                                                                     var NoTagDescription = { description: description, createdBy: createdBy, id: id };
                                                                                                     FinalDescription.push(NoTagDescription);

                                                                                                 }
                                                                                             }

                                                                                         }

                                                                                         $scope.todocomments = FinalDescription;

                                                                                         $timeout(function () {

                                                                                             $scope.$apply();

                                                                                             /* Getting Attachments Corresponding To ToDo/Task*/
                                                                                             attachmentsFactory.getToDoAttachments($scope.toDoId).success(function (data) {
                                                                                                 $scope.uploads = data;

                                                                                                 $timeout(function () {
                                                                                                     $scope.$apply();

                                                                                                     /*Getting ToDo Followers Of Particular ToDo*/
                                                                                                     $scope.getFollowers(toDoId);

                                                                                                 });

                                                                                                 //$timeout(function () {
                                                                                                 //    $scope.$apply();

                                                                                                 //    $('#divToDoDescriptionSection').show(400);

                                                                                                 //});

                                                                                                 $timeout(function () {
                                                                                                     $scope.$apply();
                                                                                                     $timeout(hideloader, 500);
                                                                                                 });

                                                                                                 function hideloader() {
                                                                                                     //$('#ajax_loader').hide();

                                                                                                     $('#divrightloader').hide();
                                                                                                 };
                                                                                             })
                                                                                                                     .error(function (error) {
                                                                                                                     });
                                                                                         });

                                                                                     })
                                                                                      .error(function (error) {
                                                                                      });


                                                          });
                                                      });



                                                  });

                                              })
                                               .error(function (error) {

                                               });

        }

        /* Save Todo Comment Function */
        function SaveToDoCommentFunction() {

            $scope.showassigneesearch = false;
            $scope.showattachmentsearch = false;
            $('#globalLoader').show();

            var arrayNames = [];
            $('#txtComments div').each(function (index) {
                arrayNames[index] = $(this).attr('name');
            });

            var arrayEmails = [];
            $('#txtComments div').each(function (index) {
                arrayEmails[index] = $(this).attr('email');
            });

            var arrayUsers = [];
            $('#txtComments div').each(function (index) {
                arrayUsers[index] = $(this).attr('userId');
            });

            var arrayAttachments = [];
            $('#txtComments span').each(function (index) {
                arrayAttachments[index] = $(this).attr('fileName');
            });


            var arrayOriginalFileNames = [];
            $('#txtComments span').each(function (index) {
                arrayOriginalFileNames[index] = $(this).attr('OriginalName');
            });

            var arrayFilePaths = [];
            $('#txtComments span').each(function (index) {
                arrayFilePaths[index] = $(this).attr('Path');
            });

            var arrayIsGif = [];
            $('#txtComments span').each(function (index) {
                arrayIsGif[index] = $(this).attr('IsGif');
            });


            var CommentText = $('#txtComments').text();
            var CommentHtml = $('#txtComments').html();
            var CommentHtmlWithoutAnchors = CommentHtml.replace(/<\/?a[^>]*>[^>]*>/g, '');
            // $('#txtComments').html(CommentHtmlWithoutAnchors);
            var newCommentText = $('#txtComments').text();
            var contentSansAnchors = CommentHtmlWithoutAnchors.replace(/<\/?div[^>]*>[^>]*>/g, '@@');
            var finalstring = contentSansAnchors.replace(/<\/?span[^>]*>[^>]*>/g, '##');
            //var newstring = contentSansAnchors.split('@@');
            //  alert(userIds.length);
            // var finalString = newstring[0] + "Aryan Deol" + newstring[1] + "Naaz Bhullar";
            //  alert(finalString);
            $scope.todocomment = {};
            $scope.todocomment.description = finalstring;
            $scope.todocomment.todoid = $scope.todoDescriptionObject.todoId;
            $scope.todocomment.userTag = arrayUsers;
            $scope.todocomment.fileTag = arrayAttachments;
            $scope.todocomment.originalFileName = arrayOriginalFileNames;
            $scope.todocomment.filePath = arrayFilePaths;
            $scope.todocomment.isGif = arrayIsGif;
            $('#txtComments').text('');
            todosFactory.insertTodoComment($scope.todocomment)
                                     .success(function (data) {
                                         $scope.status = data;

                                         /* Getting Comments Corresponding To ToDo/Task*/
                                         getTodoCommentsInArrangedMode($scope.todoDescriptionObject.todoId);
                                         $scope.todocomment.description = "";
                                         $('#txtComments').html('');
                                         $('#txtOthersToDoComments').html('');
                                         $('#globalLoader').hide();

                                         toaster.success("You Commented Successfully");
                                     })
                                      .error(function (error) {
                                          $scope.status = 'unable to save todo comment : ' + error.message;
                                      });

            for (var i = 0; i < arrayUsers.length; i++) {
                $scope.email = {};
                $scope.email.emailId = arrayEmails[i];
                $scope.email.username = arrayNames[i];
                $scope.email.todo = $scope.ToDoTaskName;
                $scope.email.projectName = $scope.projectName;
                $scope.email.comment = newCommentText;
                $scope.email.flag = 5;
                emailFactory.sendEmail($scope.email).success(function () { });
            }



        };

        /* Save new ToDo Comment */
        $scope.saveToDoComment = function saveToDoComment() {
            SaveToDoCommentFunction();
        };

        /*  Confirm Delete Comment */
        $scope.ConfirmDeleteComment = function (item) {

            item.ConfirmDeleteCommentStatus = true;
        };

        /* Cancel Delete Comment */
        $scope.CancelDeleteComment = function (item) {

            item.ConfirmDeleteCommentStatus = false;
        };

        /* Delete ToDo Comment */
        $scope.deleteToDoComment = function deleteToDoComment(id, index) {
            //  $('#globalLoader').show();
            todosFactory.deleteTodoComment(id).success(function (data) {

                if (data.success) {
                    toaster.success("Comment Deleted Successfully");
                    $('#globalLoader').hide();
                    /* Getting Comments Corresponding To ToDo/Task*/
                    // getTodoCommentsInArrangedMode($scope.toDoId);


                    $scope.todocomments.splice(index, 1);



                }

            }).error(function (error) { });
        }

        /* Universal Add function for adding file in comment */
        function AddFileInCommentOnTabClick(x) {
            if (x != null) {
                if (x.length > 0) {
                    $scope.fileInfo = x;

                    var Id = guid();
                    var data = "<span  class='glyphicon glyphicon-file spanFileInComment' IsGif='false' OriginalName='" + $scope.fileInfo[0].originalName + "' Path='" + $scope.fileInfo[0].path + "' fileName='" + $scope.fileInfo[0].name + "'  id='" + Id + "'   contenteditable='false'  >" + $scope.fileInfo[0].originalName + "<a id='" + Id + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></span>";
                    var compiledElement = $compile(data)($scope);

                    var htmlnode = $('#txtComments').html();

                    if ($scope.IsItStartingOfComment == true) {

                        var stringTobeReplaced = "#" + $scope.lastCharactersOfLastWord;

                    }
                    else {
                        var stringTobeReplaced = " #" + $scope.lastCharactersOfLastWord;
                    }

                    var res = htmlnode.replace(stringTobeReplaced, " " + data);


                    $('#txtComments').html("");
                    $('#txtComments').append(res);



                    var newhtml = "<c>" + $('#txtComments').html() + "</c>";
                    var newcompilehtml = $compile(newhtml)($scope);

                    $('#txtComments').html("");
                    $('#txtComments').append(newcompilehtml);
                    $('#txtComments').append("&nbsp;");

                    $scope.showassigneesearch = false;
                    $scope.showattachmentsearch = false;
                    $scope.searchassigneefortagging = null;
                    $scope.searchattachmentfortagging = null;

                    placeCursorAtEnd(document.getElementById("txtComments"));

                    $timeout(function () {
                        $scope.$apply();

                    });

                }
            }
        }

        /* Universal Add function for adding Gif File in comment */
        function AddGifFileInCommentOnTabClick(x) {
            if (x != null) {
                if (x.length > 0) {

                    $scope.fileInfo = x;

                    var Id = guid();
                    var data = "<span  class='glyphicon glyphicon-file spanGifFileInComment' IsGif='true' OriginalName='" + $scope.fileInfo[0].name + "' Path='" + $scope.fileInfo[0].url + "' fileName='" + $scope.fileInfo[0].name + "'  id='" + Id + "'   contenteditable='false'  >" + "gif:" + $scope.fileInfo[0].name + "<a id='" + Id + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></span>";
                    var compiledElement = $compile(data)($scope);

                    var htmlnode = $('#txtComments').html();

                    if ($scope.IsItStartingOfComment == true) {

                        var stringTobeReplaced = "#" + $scope.lastCharactersOfLastWord;

                    }
                    else {
                        var stringTobeReplaced = " #" + $scope.lastCharactersOfLastWord;
                    }

                    var res = htmlnode.replace(stringTobeReplaced, " " + data);


                    $('#txtComments').html("");
                    $('#txtComments').append(res);



                    var newhtml = "<c>" + $('#txtComments').html() + "</c>";
                    var newcompilehtml = $compile(newhtml)($scope);

                    $('#txtComments').html("");
                    $('#txtComments').append(newcompilehtml);
                    $('#txtComments').append("&nbsp;");

                    $scope.showassigneesearch = false;
                    $scope.showattachmentsearch = false;
                    $scope.searchassigneefortagging = null;
                    $scope.searchattachmentfortagging = null;

                    placeCursorAtEnd(document.getElementById("txtComments"));

                    $timeout(function () {
                        $scope.$apply();
                    });

                }
            }
        }

        /* Allowing tab for selecting the searched user */
        $scope.TabKeyPressEvent = function (e) {

            var TABKEY = 9;
            var ENTERKEY = 13;

            if (e.keyCode == TABKEY) {

                if ($scope.showassigneesearch == true) {
                    
                    var x = $filter('filter')($scope.projectMembersObject, $scope.searchassigneefortagging);
                    if (x != null) {
                        if (x.length > 0) {
                            
                            $scope.people = x;

                            var Id = guid();
                            var data = "<div class='glyphicon glyphicon-user divUserInComment' name='" + $scope.people[0].memberName + "' email='" + $scope.people[0].memberEmail + "' userId='" + $scope.people[0].memberUserId + "'  id='" + Id + "'   contenteditable='false'  >" + $scope.people[0].memberName + "<a id='" + Id + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></div>";
                            var compiledElement = $compile(data)($scope);
                            var htmlnode = $('#txtComments').html();

                            if ($scope.IsItStartingOfComment == true) {
                                var stringTobeReplaced = "@" + $scope.lastCharactersOfLastWord;
                            }
                            else {
                                var stringTobeReplaced = " @" + $scope.lastCharactersOfLastWord;
                            }
                            var res = htmlnode.replace(stringTobeReplaced, " " + data);
                            $('#txtComments').html("");
                            $('#txtComments').append(res);
                            var newhtml = "<c>" + $('#txtComments').html() + "</c>";
                            var newcompilehtml = $compile(newhtml)($scope);
                            $('#txtComments').html("");
                            $('#txtComments').append(newcompilehtml);
                            $('#txtComments').append("&nbsp;");
                            $scope.showassigneesearch = false;
                            $scope.showattachmentsearch = false;
                            $scope.searchassigneefortagging = null;
                            $scope.searchattachmentfortagging = null;
                            placeCursorAtEnd(document.getElementById("txtComments"));

                            $timeout(function () {
                                $scope.$apply();

                            });

                        }
                    }
                }
                else if ($scope.showattachmentsearch == true) {
                    var ActiveTab = $scope.selectedCommentTab;
                    switch (ActiveTab) {
                        case "GifFiles":
                            var GifImageData = $filter('filter')($scope.GiphyImages, { 'name': $scope.searchGiffortagging });
                            AddGifFileInCommentOnTabClick(GifImageData);
                            break;
                        case "GoogleDriveFiles":
                            var GoogleDriveFilesData = $filter('filter')($scope.GoogleDriveAttachmentsForComment, { 'originalName': $scope.searchGoogleDrivefortagging });
                            AddFileInCommentOnTabClick(GoogleDriveFilesData);
                            break;
                        case "DropBoxFiles":
                            var DropBoxFilesData = $filter('filter')($scope.DropBoxAttachmentsForComment, { 'originalName': $scope.searchDropBoxfortagging });
                            AddFileInCommentOnTabClick(DropBoxFilesData);
                            break;
                        case "BoxFiles":
                            var BoxFilesData = $filter('filter')($scope.BoxAttachmentsForComment, { 'originalName': $scope.searchBoxfortagging });
                            AddFileInCommentOnTabClick(BoxFilesData);
                            break;
                        case "LocalFiles":
                            var LocalFilesData = $filter('filter')($scope.LocalAttachmentsForComment, { 'originalName': $scope.searchLocalfortagging });
                            AddFileInCommentOnTabClick(LocalFilesData);
                            break;
                        case "AllFiles":
                            var AllFilesData = $filter('filter')($scope.AllAttachmentsForComment, { 'originalName': $scope.searchattachmentfortagging });
                            AddFileInCommentOnTabClick(AllFilesData);
                            break;
                    }
                }
                if (e.preventDefault) {
                    e.preventDefault();
                }
                return false;
            }

            else if (e.shiftKey == false && e.keyCode == ENTERKEY) {

                SaveToDoCommentFunction();

            }
            else if (e.shiftKey == true && e.keyCode == ENTERKEY) {
                $('#txtComments').append("\r");
            }

        };

        /* Making a scope for giphy images */
        $scope.GiphyImages = [{ name: 'Happy', url: 'Uploads/Attachments/GiphyImages/happy.gif' }, { name: 'GudLuck', url: 'Uploads/Attachments/GiphyImages/gudluck.gif' }, { name: 'BestWishes', url: 'Uploads/Attachments/GiphyImages/bestwishes.gif' }, { name: 'WellDone', url: 'Uploads/Attachments/GiphyImages/welldone.gif' }, { name: 'Nice', url: 'Uploads/Attachments/GiphyImages/nice.gif' }, { name: 'Confused', url: 'Uploads/Attachments/GiphyImages/confused.gif' }, { name: 'Excited', url: 'Uploads/Attachments/GiphyImages/excited.gif' }, { name: 'Frustrated', url: 'Uploads/Attachments/GiphyImages/frustrated.gif' }];

        /* Hide Comment Box  */
        $scope.hideCommentBox = function () {
            $scope.showassigneesearch = false;
            $scope.showattachmentsearch = false;

        };


        /* Showing All Images tab in comment */
        $scope.showAllImagesTabInComment = function () {
            $scope.showGifImages = false;
            $scope.searchattachmentfortagging = "";
            $scope.selectedCommentTab = "AllFiles";
        };

        /* Showing GIF images tab in comment */
        $scope.showGifImagesTabInComment = function () {
            $scope.showGifImages = true;
            $scope.searchGiffortagging = "";
            $scope.selectedCommentTab = "GifFiles";
        };

        /* Showing COmputer Files tab in comment */
        $scope.showLocalFilesTabInComment = function () {

            $scope.selectedCommentTab = "LocalFiles";
            $scope.searchLocalfortagging = "";
        };


        /* Showing DropBox Files tab in comment */
        $scope.showDropBoxFilesTabInComment = function () {

            $scope.selectedCommentTab = "DropBoxFiles";
            $scope.searchDropBoxfortagging = "";
        };

        /* Showing Box Files tab in comment */
        $scope.showBoxFilesTabInComment = function () {

            $scope.selectedCommentTab = "BoxFiles";
            $scope.searchBoxfortagging = "";

        };

        /* Showing GoogleDrive FIles tab in comment */
        $scope.showGoogleDriveFilesTabInComment = function () {

            $scope.selectedCommentTab = "GoogleDriveFiles";
        };

        /*Showing users in the list when anyone enter @ in comment textbox in my todo Description*/
        $scope.ShowUsersorAttachmentsForTagging = function (e) {


            function getCaretCharacterOffsetWithin(element) {
                var caretOffset = 0;
                if (typeof window.getSelection != "undefined") {
                    var range = window.getSelection().getRangeAt(0);
                    var preCaretRange = range.cloneRange();
                    preCaretRange.selectNodeContents(element);
                    preCaretRange.setEnd(range.endContainer, range.endOffset);
                    caretOffset = preCaretRange.toString().length;
                }

                else if (typeof document.selection != "undefined" && document.selection.type != "Control") {
                    var textRange = document.selection.createRange();
                    var preCaretTextRange = document.body.createTextRange();
                    preCaretTextRange.moveToElementText(element);
                    preCaretTextRange.setEndPoint("EndToEnd", textRange);
                    caretOffset = preCaretTextRange.text.length;
                }
                return caretOffset;
            }


            var el = document.getElementById("txtComments");

            var CaretPositionIndex = getCaretCharacterOffsetWithin(el);
            $scope.CaretPositionIndex = CaretPositionIndex;

            var divText = $('#txtComments').text();
            var characterMain = divText.substring(0, CaretPositionIndex)


            var value = characterMain;

            var segmentsDividedBySpace = [];

            segmentsDividedBySpace = value.split(" ");

            if (segmentsDividedBySpace.length == 1) {
                $scope.IsItStartingOfComment = true;
            }
            else {
                $scope.IsItStartingOfComment = false;
            }

            var lastWord = segmentsDividedBySpace[segmentsDividedBySpace.length - 1];
            var firstCharacterOfLastWord = lastWord.substring(0, 1);
            var lastCharactersOfLastWord = lastWord.substring(1, lastWord.length);
            $scope.lastCharactersOfLastWord = lastCharactersOfLastWord;
            if (firstCharacterOfLastWord == "@") {
                $scope.searchassigneefortagging = lastCharactersOfLastWord;

                $scope.searchattachmentfortagging = null;
                $scope.showassigneesearch = true;
                $scope.showattachmentsearch = false;
            }
            else if (firstCharacterOfLastWord == "#") {

                $scope.searchassigneefortagging = null;
                $scope.showassigneesearch = false;
                $scope.showattachmentsearch = true;
                var middleFourCharactersOfWord = lastCharactersOfLastWord.substring(0, 4);

                switch (middleFourCharactersOfWord) {
                    case "gif:":
                        $scope.selectedCommentTab = "GifFiles";
                        $scope.showGifImages = true;
                        var searchForGif = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                        $scope.searchGiffortagging = searchForGif;
                        $scope.searchattachmentfortagging = "";
                        $scope.searchGoogleDrivefortagging = "";
                        $scope.searchDropBoxfortagging = "";
                        $scope.searchBoxfortagging = "";
                        $scope.searchLocalfortagging = "";
                        break;
                    case "gdf:":
                        $scope.selectedCommentTab = "GoogleDriveFiles";
                        $scope.searchGoogleDrivefortagging = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                        $scope.searchattachmentfortagging = "";
                        $scope.searchattachmentfortagging = "";
                        $scope.searchGiffortagging = "";

                        $scope.searchDropBoxfortagging = "";
                        $scope.searchBoxfortagging = "";
                        $scope.searchLocalfortagging = "";
                        break;
                    case "dbf:":
                        $scope.selectedCommentTab = "DropBoxFiles";
                        var search = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                        $scope.searchDropBoxfortagging = search;
                        $scope.searchattachmentfortagging = "";
                        $scope.searchGiffortagging = "";
                        $scope.searchGoogleDrivefortagging = "";

                        $scope.searchBoxfortagging = "";
                        $scope.searchLocalfortagging = "";
                        break;
                    case "box:":
                        $scope.selectedCommentTab = "BoxFiles";
                        $scope.searchBoxfortagging = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                        $scope.searchattachmentfortagging = "";
                        $scope.searchGiffortagging = "";
                        $scope.searchGoogleDrivefortagging = "";
                        $scope.searchDropBoxfortagging = "";

                        $scope.searchLocalfortagging = "";
                        break;
                    case "loc:":
                        $scope.selectedCommentTab = "LocalFiles";
                        $scope.searchLocalfortagging = lastCharactersOfLastWord.substring(4, lastCharactersOfLastWord.length);
                        $scope.searchattachmentfortagging = "";
                        $scope.searchGiffortagging = "";
                        $scope.searchGoogleDrivefortagging = "";
                        $scope.searchDropBoxfortagging = "";
                        $scope.searchBoxfortagging = "";

                        break;
                    default:
                        $scope.selectedCommentTab = "AllFiles";
                        $scope.searchGiffortagging = "";
                        $scope.searchGoogleDrivefortagging = "";
                        $scope.searchDropBoxfortagging = "";
                        $scope.searchBoxfortagging = "";
                        $scope.searchLocalfortagging = "";
                        $scope.showGifImages = false;
                        $scope.searchattachmentfortagging = lastCharactersOfLastWord;
                }


            }
            else {
                $scope.showassigneesearch = false;
                $scope.showattachmentsearch = false;
            }


            if (e.shiftKey == true && e.keyCode == 50) {

                var characterUser = divText.substring(CaretPositionIndex - 2, CaretPositionIndex - 1)


                if (characterUser == " " || characterUser == "") {

                    $scope.showassigneesearch = true;
                    $scope.showattachmentsearch = false;
                }

            }
            else if (e.shiftKey == true && e.keyCode == 51) {
                $scope.showassigneesearch = false;
                $scope.showattachmentsearch = true;
                $scope.AllAttachmentsForComment = $scope.uploads;
                $scope.LocalAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'computer.png' });
                $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'logo-drive.png' });
                $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'drop-box.png' });
                $scope.BoxAttachmentsForComment = $filter('filter')($scope.uploads, { 'sourceImage': 'box-icon.png' });
            }
            else if (e.keyCode == 32) {
                $scope.showassigneesearch = false;
                $scope.showattachmentsearch = false;
            }

        };

        /* Adding User In Comment in my todo*/
        $scope.AddUserInComment = function (userId, FirstName, LastName, Emailid) {

            var Id = guid();
            var data = "<div class='glyphicon glyphicon-user divUserInComment' name='" + FirstName + "' email='" + Emailid + "' userId='" + userId + "'  id='" + Id + "'   contenteditable='false'  >" + FirstName + ' ' + LastName + "<a id='" + Id + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></div>";

            var compiledElement = $compile(data)($scope);
            var htmlnode = $('#txtComments').html();

            if ($scope.IsItStartingOfComment == true) {

                var stringTobeReplaced = "@" + $scope.lastCharactersOfLastWord;

            }
            else {
                var stringTobeReplaced = " @" + $scope.lastCharactersOfLastWord;
            }

            var res = htmlnode.replace(stringTobeReplaced, " " + data);


            $('#txtComments').html("");
            $('#txtComments').append(res);



            var newhtml = "<c>" + $('#txtComments').html() + "</c>";
            var newcompilehtml = $compile(newhtml)($scope);

            $('#txtComments').html("");
            $('#txtComments').append(newcompilehtml);
            $('#txtComments').append("&nbsp;");

            $scope.showassigneesearch = false;
            $scope.showattachmentsearch = false;
            $scope.searchassigneefortagging = "";
            $scope.searchattachmentfortagging = "";

            placeCursorAtEnd(document.getElementById("txtComments"));

            $timeout(function () {
                $scope.$apply();

            });
        };


        /* Adding Attachment In Comment in my todo*/
        $scope.AddFileInComment = function (Name, OriginalName, Path) {

            var Id = guid();
            var data = "<span  class='glyphicon glyphicon-file spanFileInComment' IsGif='false' OriginalName='" + OriginalName + "' Path='" + Path + "' fileName='" + Name + "'  id='" + Id + "'   contenteditable='false'  >" + OriginalName + "<a id='" + Id + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></span>";
            var compiledElement = $compile(data)($scope);

            var htmlnode = $('#txtComments').html();

            if ($scope.IsItStartingOfComment == true) {

                var stringTobeReplaced = "#" + $scope.lastCharactersOfLastWord;

            }
            else {
                var stringTobeReplaced = " #" + $scope.lastCharactersOfLastWord;
            }

            var res = htmlnode.replace(stringTobeReplaced, " " + data);


            $('#txtComments').html("");
            $('#txtComments').append(res);



            var newhtml = "<c>" + $('#txtComments').html() + "</c>";
            var newcompilehtml = $compile(newhtml)($scope);

            $('#txtComments').html("");
            $('#txtComments').append(newcompilehtml);
            $('#txtComments').append("&nbsp;");

            $scope.showassigneesearch = false;
            $scope.showattachmentsearch = false;
            $scope.searchassigneefortagging = "";
            $scope.searchattachmentfortagging = "";

            placeCursorAtEnd(document.getElementById("txtComments"));

            $timeout(function () {
                $scope.$apply();

            });

        };

        /* Adding Attachment In Comment in my todo*/
        $scope.AddGifFileInComment = function (Name, Path) {

            var Id = guid();
            var data = "<span  class='glyphicon glyphicon-file spanFileInComment' IsGif='true' OriginalName='" + Name + "' Path='" + Path + "' fileName='" + Name + "'  id='" + Id + "'   contenteditable='false'  >" + "gif:" + Name + "<a id='" + Id + "' style='cursor:pointer;border-left:1px solid grey;margin-left:6px;margin-right:3px;padding-left:3px;font-weight:bold;color:white!important;' ng-click='DeleteTaggedItem($event)'  >x</a></span>";

            var htmlnode = $('#txtComments').html();

            if ($scope.IsItStartingOfComment == true) {

                var stringTobeReplaced = "#" + $scope.lastCharactersOfLastWord;

            }
            else {
                var stringTobeReplaced = " #" + $scope.lastCharactersOfLastWord;
            }

            var res = htmlnode.replace(stringTobeReplaced, " " + data);


            $('#txtComments').html("");
            $('#txtComments').append(res);



            var newhtml = "<c>" + $('#txtComments').html() + "</c>";
            var newcompilehtml = $compile(newhtml)($scope);

            $('#txtComments').html("");
            $('#txtComments').append(newcompilehtml);
            $('#txtComments').append("&nbsp;");

            $scope.showassigneesearch = false;
            $scope.showattachmentsearch = false;
            $scope.searchassigneefortagging = "";
            $scope.searchattachmentfortagging = "";

            placeCursorAtEnd(document.getElementById("txtComments"));

            $timeout(function () {
                $scope.$apply();

            });

        };


        /* Code For Placing Cursor at the end of div*/
        function placeCursorAtEnd(el) {
            el.focus();
            if (typeof window.getSelection != "undefined"
                    && typeof document.createRange != "undefined") {
                var range = document.createRange();
                range.selectNodeContents(el);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (typeof document.body.createTextRange != "undefined") {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(el);
                textRange.collapse(false);
                textRange.select();
            }
        }


        /* Removing tagged items from comment*/
        $scope.DeleteTaggedItem = function (event) {

            $('#' + $(event.target).attr('id')).remove();

        };


        /* Uploading Attachment Corresponding To ToDo/Task  */
        $scope.upload = [];
        $scope.max = 100;
        $scope.dynamic = 0;

        $scope.onFileSelect = function ($files) {

            $scope.progressperc = true;
            var j = ($files.length) - 1;

            //$files: an array of files selected, each file has name, size, and type.
            for (var i = 0; i < $files.length; i++) {


                var $file = $files[i];

                if (i == j) {
                    (function (index) {

                        $scope.upload[index] = $upload.upload({
                            url: WebDomainPath + "/API/files/upload", // webapi url
                            method: "POST",
                            data: { UploadedAttachment: $file, ToDoId: $scope.toDoId },
                            file: $file
                        }).progress(function (evt) {

                            // get upload percentage
                            console.log(parseInt(100.0 * evt.loaded / evt.total));
                            $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);

                        }).success(function (data, status, headers, config) {
                            // file is uploaded successfully

                            $timeout(function () {
                                $scope.$apply();
                                $scope.progressperc = false;

                                /* Sending Notification to the related users*/
                                var fileCount = $files.length == 1 ? $files.length + " file from Computer" : $files.length + " files from Computer";
                                $scope.notification.name = "has uploaded " + fileCount;
                                $scope.notification.toDoId = $scope.toDoId;
                                $scope.notification.flag = 1;
                                notificationFactory.insertNotification($scope.notification).success(function () {
                                    $scope.temporaryId = 1;
                                    usersFactory.getUser($scope.temporaryId)
                                               .success(function (data) {
                                                   $scope.NotifyCount = data.notificationCount == 0 ? null : data.notificationCount;
                                                   $scope.hideNotifyCount = data.notificationCount == 0 ? true : false;
                                               })
                                                .error(function (error) {
                                                    //$scope.status = 'Unable to load Projects data: ' + error.message;
                                                });
                                });

                                $scope.todoNotificationsArray = {};
                                $scope.todoNotificationsArray.action = "";
                                $scope.todoNotificationsArray.userId = null;
                                $scope.todoNotificationsArray.todoId = $scope.toDoId;
                                $scope.todoNotificationsArray.name = "has uploaded " + fileCount;

                                todoNotificationsFactory.insertTodoNotification($scope.todoNotificationsArray).success(function () {
                                    var date = new Date();
                                    var hours = date.getHours();
                                    var minutes = date.getMinutes();
                                    var ampm = hours >= 12 ? 'PM' : 'AM';
                                    hours = hours % 12;
                                    hours = hours ? hours : 12; // the hour '0' should be '12'
                                    minutes = minutes < 10 ? '0' + minutes : minutes;
                                    var strTime = hours + ':' + minutes + ' ' + ampm;
                                    $scope.pushdata = {};
                                    $scope.pushdata.notification = $scope.MyProfileObject.firstName + " " + $scope.todoNotificationsArray.name;
                                    $scope.pushdata.profilePhoto = "";
                                    $scope.pushdata.createdDate = strTime;
                                    $scope.todoNotificationsData.push($scope.pushdata);
                                });


                                /* Empty Input After Success*/
                                angular.forEach(
                                 angular.element("input[type='file']"),
                                         function (inputElem) {
                                             angular.element(inputElem).val(null);
                                         });
                                attachmentsFactory.getToDoAttachments($scope.toDoId).success(function (data) {
                                    $scope.uploads = data;
                                })
                                     .error(function (error) {
                                     });
                            });
                            $timeout(function () {
                                $scope.$apply();
                                toaster.success("Computer", "Attachments uploaded Successfully");

                            });


                        }).error(function (data, status, headers, config) {
                            // file failed to upload
                            console.log(data);
                        });
                    })(i);
                }
                else {
                    (function (index) {

                        $scope.upload[index] = $upload.upload({
                            url: WebDomainPath + "/API/files/upload", // webapi url
                            method: "POST",
                            data: { UploadedAttachment: $file, ToDoId: $scope.toDoId },
                            file: $file
                        }).progress(function (evt) {

                            // get upload percentage
                            console.log(parseInt(100.0 * evt.loaded / evt.total));
                            $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);

                        }).success(function (data, status, headers, config) {
                            // file is uploaded successfully


                        }).error(function (data, status, headers, config) {
                            // file failed to upload 
                            console.log(data);
                        });
                    })(i);

                }
            }

        }

        $scope.abortUpload = function (index) {
            $scope.upload[index].abort();
        };










        /* Confirm Delete Attachment*/
        $scope.confirmDeleteAttachment = function (id, attachmentname) {

            $('#divConfirmFooter').html('');
            $scope.confirmMessage = "Are you sure you want to delete <b style='color:red;'>" + attachmentname + "</b> Attachment?";
            var data = '<button ng-click="deleteAttachment(' + id + ')" style="background: #58BD4B;" class="btn btn-default" type="button">Yes</button><button type="button" class="btn btn-default" style="background: #FF7F7F; " data-dismiss="modal">No</button>';

            var compiledFooter = $compile(data)($scope);


            $('#divConfirmFooter').append(compiledFooter);

            $('#confirmModal').modal('show');

        };


        /* Deleting Attachment Corresponding to ToDo/Task*/
        $scope.deleteAttachment = function (id) {

            attachmentsFactory.deleteTodoAttachments(id).success(function () {
                attachmentsFactory.getToDoAttachments($scope.toDoId).success(function (data) {
                    $scope.uploads = data;
                    $timeout(function () {
                        $scope.$apply();
                        $('#confirmModal').modal('hide');
                        toaster.success("Attachment Deleted Successfully");


                    });
                })
                             .error(function (error) {
                             });

            }).error(function () {
                alert('Some error occured');
            });

        }


        /* Fetching My TODOs that are unassigned*/
        $scope.MyTodoes = function MyTodoes(source) {
            closeCentreSections();
            if (source == "main") {
                $scope.hideRightWindow = true;
                $scope.stretchCentreWindow = true;
            }

            $scope.selectedWindow = "unassigned";
            $scope.mypersonalassignedtodos = null;
            $scope.myassignedtodos = null;
            $scope.myunassignedtodos = "";

            $scope.mytodoswindowloader = true;
            $scope.showNoTodosRecordsMessage = false;
            $scope.myunassignedtodoswindow = false;
            $scope.myassignedtodoswindow = false;
            $scope.mypersonaltodoswindow = false;
            $scope.myarchivedwindow = false;
            $scope.myfollowingwindow = false;
            $scope.selectedtodo = "unassigned";
            $scope.selectedNavigation = 'MyTodos';
            $scope.selectedTodo = '';
            $scope.selectedProject = '';

            $('#divMyTodoes').show();



            todosFactory.getMyUnassignedToDos()
                                .success(function (data) {
                                    $scope.myunassignedtodos = data;
                                    if (data.length == 0) {
                                        $scope.showNoTodosRecordsMessage = true;
                                    }
                                    $timeout(function () {
                                        $scope.$apply();
                                        $scope.mytodoswindowloader = false;
                                        $scope.myunassignedtodoswindow = true;
                                    });

                                })
                                 .error(function (error) {
                                     $scope.status = 'Unable to load TODOs: ' + error.message;
                                 });

        };


        /* Fetching My TODOs that are assigned to others*/
        $scope.MyAssignedTODOs = function () {
            $scope.selectedWindow = "assigned";
            $scope.mypersonalassignedtodos = null;
            $scope.myassignedtodos = null;
            $scope.myunassignedtodos = null;
            $('#divProjectOverview').hide();
            $('#divToDoDescriptionSection').hide();
            $scope.showNoTodosRecordsMessage = false;
            $scope.mytodoswindowloader = true;
            $scope.myassignedtodoswindow = false;
            $scope.myunassignedtodoswindow = false;
            $scope.mypersonaltodoswindow = false;
            $scope.myarchivedwindow = false;
            $scope.myfollowingwindow = false;
            $scope.selectedtodo = "assigned";
            todosFactory.getMyAssignedToDos()
                                    .success(function (data) {
                                        $scope.myassignedtodos = data;
                                        if (data.length == 0) {

                                            $scope.showNoTodosRecordsMessage = true;
                                        }

                                        $timeout(function () {
                                            $scope.$apply();
                                            $scope.mytodoswindowloader = false;
                                            $scope.myassignedtodoswindow = true;
                                        });
                                    })
                                     .error(function (error) {
                                         $scope.status = 'Unable to load TODOs: ' + error.message;
                                     });
        };


        /* Fetching My TODOs that are assigned to me that are my personalassignedTodos*/
        $scope.MyPersonalAssignedTODOs = function () {
            $scope.selectedWindow = "personal";
            $scope.mypersonalassignedtodos = null;
            $scope.myassignedtodos = null;
            $scope.myunassignedtodos = null;
            $('#divProjectOverview').hide();
            $('#divToDoDescriptionSection').hide();
            $scope.showNoTodosRecordsMessage = false;
            $scope.mytodoswindowloader = true;
            $scope.mypersonaltodoswindow = false;
            $scope.myassignedtodoswindow = false;
            $scope.myunassignedtodoswindow = false;
            $scope.myarchivedwindow = false;
            $scope.myfollowingwindow = false;
            $scope.selectedtodo = "personal";
            todosFactory.getMyPersonalAssignedToDos()
                                    .success(function (data) {
                                        $scope.mypersonalassignedtodos = data;
                                        if (data.length == 0) {

                                            $scope.showNoTodosRecordsMessage = true;
                                        }
                                        $timeout(function () {
                                            $scope.$apply();
                                            $scope.mytodoswindowloader = false;
                                            $scope.mypersonaltodoswindow = true;
                                        });

                                    })
                                     .error(function (error) {
                                         $scope.status = 'Unable to load TODOs: ' + error.message;
                                     });
        };


        /* Fetching My Archived TODOs that are assigned to me by the others and assigned by me to others*/
        $scope.GetMyArchivedTodos = function GetMyArchivedTodos() {

            $scope.selectedtodo = "archived";
            $scope.selectedWindow = "archived";
            $scope.MyInboxTodos = null;
            $scope.MyArchivedTodos = null;
            $scope.MyFollowedTodos = null;
            $scope.showNoTodosRecordsMessage = false;
            $scope.mytodoswindowloader = true;

            $scope.myarchivedwindow = false;
            $scope.myfollowingwindow = false;
            $scope.myassignedtodoswindow = false;
            $scope.myunassignedtodoswindow = false;
            $scope.mypersonaltodoswindow = false;

            $('#divProjectOverview').hide();
            $('#divToDoDescriptionSection').hide();
            $scope.selectedTodo = '';


            todosFactory.getMyArchivedToDos()
                                    .success(function (data) {
                                        $scope.MyArchivedTodos = data;

                                        if (data.length == 0) {

                                            $scope.showNoTodosRecordsMessage = true;
                                        }
                                        $timeout(function () {
                                            $scope.$apply();
                                            $scope.mytodoswindowloader = false;
                                            $scope.myarchivedwindow = true;
                                        });

                                    })
                                     .error(function (error) {
                                         $scope.status = 'Unable to load TODOs: ' + error.message;
                                     });
        };


        /* Fetching My Todos that are followed by me*/
        $scope.GetMyFollowedTodos = function GetMyFollowedTodos() {

            $scope.selectedtodo = "following";
            $scope.selectedWindow = "following";
            $scope.MyInboxTodos = null;
            $scope.MyArchivedTodos = null;
            $scope.MyFollowedTodos = null;

            $scope.mytodoswindowloader = true;
            $scope.showNoTodosRecordsMessage = false;
            $scope.myarchivedwindow = false;
            $scope.myfollowingwindow = false;
            $scope.myassignedtodoswindow = false;
            $scope.myunassignedtodoswindow = false;
            $scope.mypersonaltodoswindow = false;
            $('#divProjectOverview').hide();
            $('#divToDoDescriptionSection').hide();

            $scope.selectedTodo = '';


            followersFactory.getMyFollowedToDos()
                                    .success(function (data) {
                                        $scope.MyFollowedTodos = data;

                                        if (data.length == 0) {

                                            $scope.showNoTodosRecordsMessage = true;
                                        }

                                        $timeout(function () {
                                            $scope.$apply();
                                            $scope.mytodoswindowloader = false;
                                            $scope.myfollowingwindow = true;
                                        });


                                    })
                                     .error(function (error) {
                                         $scope.status = 'Unable to load TODOs: ' + error.message;
                                     });
        };


        /* Fetching My Inbox TODOs that are assigned to me by the others*/
        $scope.GetMyInbox = function () {
            closeCentreSections();
            $scope.showInboxArchivedWindow = false;
            $scope.inboxFilter = "";
            $scope.selectedInbox = 0;
            $scope.selectedtodo = "inbox";
            $scope.selectedWindow = "inbox";
            $scope.MyInboxTodos = null;
            $scope.MyArchivedTodos = null;
            $scope.MyFollowedTodos = null;
            $scope.showNoInboxRecordsMessage = false;
            $scope.myinboxwindowloader = true;
            $scope.myinboxwindow = false;
            $scope.myarchivedwindow = false;
            $scope.myfollowingwindow = false;
            $scope.selectedNavigation = 'MyInbox';
            $scope.selectedTodo = '';
            $scope.selectedProject = '';
            $('#divMyInbox').show();
            todosFactory.getMyInboxToDos()
                                    .success(function (data) {
                                        $scope.MyInboxTodos = data;
                                        if (data.length == 0) {
                                            $scope.showNoInboxRecordsMessage = true;
                                        }
                                        $timeout(function () {
                                            $scope.$apply();
                                            $scope.myinboxwindowloader = false;
                                            $scope.myinboxwindow = true;
                                        });


                                    })
                                     .error(function (error) {
                                         $scope.status = 'Unable to load TODOs: ' + error.message;
                                     });
        };


        /* Fetching My Inbox Archived TODOs that are assigned to me by the others*/
        $scope.GetMyInboxArchived = function () {
            $scope.showNoInboxRecordsMessage = false;
            $scope.showInboxArchivedWindow = true;
            $scope.selectedInbox = 0;
            $scope.selectedtodo = "inboxarchived";
            $scope.myinboxwindowloader = true;
            $scope.myinboxwindow = false;
            $scope.stretchCentreWindow = true;
            $scope.hideRightWindow = true;
            todosFactory.getMyInboxArchivedToDos()
                                    .success(function (data) {
                                        $scope.MyInboxArchivedTodos = data;
                                        if (data.length == 0) {
                                            $scope.showNoInboxRecordsMessage = true;
                                        }
                                        $timeout(function () {
                                            $scope.$apply();
                                            $scope.myinboxwindowloader = false;
                                            $scope.myinboxwindow = true;
                                        });


                                    })
                                     .error(function (error) {
                                         $scope.status = 'Unable to load TODOs: ' + error.message;
                                     });
        };


        /* Get Inbox Todo Details  */
        $scope.getInboxTodoDetail = function (item) {

            $('#divProjectOverview').hide();
            $scope.hideRightWindow = false;
            $scope.stretchCentreWindow = false;
            $('#txtComments').html('');
            $('#txtOthersToDoComments').html('');
            $scope.showassigneesearch = false;
            $scope.showattachmentsearch = false;
            $scope.AllAssigneesForComment = null;
            $scope.AllAttachmentsForComment = null;
            $('#divrightloader').show();
            $('#divToDoDescriptionSection').show();
            $scope.selectedInbox = item.id;
            $scope.toDoId = item.id;


            if (item.isRead == false) {

                item.isRead = true;
                /* Updating Inbox IsRead*/
                $scope.InboxCount = $scope.InboxCount - 1;
                if ($scope.InboxCount == 0) {
                    $scope.hideInboxCount = true;
                }
                $scope.todo.updateFlag = 7;
                $scope.todo.id = item.id;
                todosFactory.updateTodo($scope.todo).success(function (data) {

                }).error(function () { });
            }



            /*Getting ToDo Details Of Particular ToDo*/
            todosFactory.getSingleToDo($scope.toDoId)
                                              .success(function (data) {
                                                  $scope.singleTodoDescription = data;
                                                  $scope.ToDoTaskName = data[0].name;
                                                  $scope.projectName = data[0].projectName;
                                                  $scope.txtAssignees = data[0].assignedTo;
                                                  $scope.todoCreatedBy = data[0].assignedBy;
                                                  $scope.inboxToDoDescription = data[0].description;
                                                  $scope.dt1 = data[0].deadlineDate;
                                                  $scope.hideEditFollower = data[0].display;
                                                  $scope.assigneeEmailId = data[0].email;
                                                  $scope.InProgress = data[0].inProgress;
                                                  $scope.IsDone = data[0].isDone;
                                                  $scope.sectionName = data[0].sectionName;
                                                  $timeout(function () {
                                                      $scope.$apply();

                                                      projectUsersFactory.getProjectUsers(item.projectId).success(function (data) {

                                                          $scope.existingProjectUsers = data;
                                                          $timeout(function () {
                                                              $scope.$apply();
                                                              /* Getting Comments Corresponding To ToDo/Task*/
                                                              todosFactory.getToDoComments($scope.toDoId)
                                                                                     .success(function (data) {

                                                                                         $scope.todocomments = [];
                                                                                         var totalLoop = data.length;
                                                                                         var FinalDescription = [];
                                                                                         if (totalLoop > 0) {

                                                                                             for (var i = 0; i < totalLoop; i++) {
                                                                                                 var description = data[i].description;
                                                                                                 var createdBy = data[i].createdBy;
                                                                                                 var id = data[i].id;
                                                                                                 var usersloop = data[i].usersTagged.length;
                                                                                                 var filesloop = data[i].filesTagged.length;
                                                                                                 var splittedDesc = description.split('@@');
                                                                                                 var temsplittedDescFiles = description.split('##');
                                                                                                 var newUserCount = splittedDesc.length - 1;
                                                                                                 var newFileCount = temsplittedDescFiles.length - 1;
                                                                                                 var intForExtraDescForUsers = usersloop;
                                                                                                 var intForExtraDescForFile = filesloop;
                                                                                                 var newComment = '';
                                                                                                 if (splittedDesc.length > 0 && temsplittedDescFiles.length > 0) {

                                                                                                     for (var j = 0; j < newUserCount ; j++) {

                                                                                                         newComment += splittedDesc[j] + " <a>" + data[i].usersTaggedFirstNames[j].firstName + "</a>";

                                                                                                     }
                                                                                                     newComment += splittedDesc[newUserCount];
                                                                                                     var splittedDescForFile = newComment.split('##');
                                                                                                     var newfilecount = splittedDescForFile.length - 1;
                                                                                                     var veryNewComment = '';
                                                                                                     if (splittedDescForFile.length > 0) {

                                                                                                         if (filesloop > 0) {
                                                                                                             for (var j = 0; j < newfilecount; j++) {

                                                                                                                 veryNewComment += splittedDescForFile[j] + " <a href='" + data[i].path[j].filePath + "' target='_blank' >" + data[i].originalName[j].originalName + "</a>";


                                                                                                             }
                                                                                                             veryNewComment += splittedDescForFile[newfilecount];
                                                                                                         }

                                                                                                     }
                                                                                                     var lastDesc = { description: veryNewComment, createdBy: createdBy, id: id };
                                                                                                     FinalDescription.push(lastDesc);
                                                                                                 }
                                                                                                 else if (splittedDesc.length > 0 && temsplittedDescFiles.length == 0) {

                                                                                                     for (var j = 0; j < newUserCount ; j++) {

                                                                                                         if (data[i].usersTaggedFirstNames[j].firstName == null) {


                                                                                                         }
                                                                                                         else {

                                                                                                             newComment += splittedDesc[j] + " <a>" + data[i].usersTaggedFirstNames[j].firstName + "</a>";
                                                                                                         }

                                                                                                     }
                                                                                                     newComment += splittedDesc[newUserCount];


                                                                                                     var lastDesc = { description: newComment, createdBy: createdBy, id: id };
                                                                                                     FinalDescription.push(lastDesc);
                                                                                                 }
                                                                                                 else if (splittedDesc.length == 0 && temsplittedDescFiles.length > 0) {

                                                                                                     var veryNewComment = '';
                                                                                                     if (splittedDescForFile.length > 0) {

                                                                                                         if (filesloop > 0) {
                                                                                                             for (var j = 0; j < newFileCount; j++) {

                                                                                                                 veryNewComment += temsplittedDescFiles[j] + " <a href='" + data[i].path[j].filePath + "' target='_blank' >" + data[i].originalName[j].originalName + "</a>";


                                                                                                             }
                                                                                                             veryNewComment += temsplittedDescFiles[newfilecount];
                                                                                                         }

                                                                                                     }
                                                                                                     var lastDesc = { description: veryNewComment, createdBy: createdBy, id: id };
                                                                                                     FinalDescription.push(lastDesc);
                                                                                                 }
                                                                                                 else {

                                                                                                     var NoTagDescription = { description: description, createdBy: createdBy, id: id };
                                                                                                     FinalDescription.push(NoTagDescription);

                                                                                                 }
                                                                                             }

                                                                                         }

                                                                                         $scope.todocomments = FinalDescription;

                                                                                         $timeout(function () {

                                                                                             $scope.$apply();

                                                                                             /* Getting Attachments Corresponding To ToDo/Task*/
                                                                                             attachmentsFactory.getToDoAttachments($scope.toDoId).success(function (data) {
                                                                                                 $scope.uploads = data;

                                                                                                 $timeout(function () {
                                                                                                     $scope.$apply();

                                                                                                     /*Getting ToDo Followers Of Particular ToDo*/
                                                                                                     $scope.getFollowers($scope.toDoId);

                                                                                                 });

                                                                                                 //$timeout(function () {
                                                                                                 //    $scope.$apply();

                                                                                                 //    $('#divToDoDescriptionSection').show(400);

                                                                                                 //});

                                                                                                 $timeout(function () {
                                                                                                     $scope.$apply();
                                                                                                     $timeout(hideloader, 500);
                                                                                                 });

                                                                                                 function hideloader() {
                                                                                                     //$('#ajax_loader').hide();

                                                                                                     $('#divrightloader').hide();
                                                                                                 };
                                                                                             })
                                                                                                                     .error(function (error) {
                                                                                                                     });
                                                                                         });

                                                                                     })
                                                                                      .error(function (error) {
                                                                                      });

                                                          });
                                                      });
                                                  });



                                              })
                                               .error(function (error) {
                                               });

        };


        /* Getting My Dashboard Details*/
        /*        $scope.GetMyDashboard = function () {
                    closeCentreSections();
                    $scope.showNoTodosInProgressRecordsMessage = false;
                    $scope.showNoTodosPendingRecordsMessage = false;
                    $scope.mydashboardwindowloader = true;
                    $scope.hideMyDashboard = true;
                    $scope.selectedNavigation = 'MyDashboard';
                    $scope.selectedWindow = "MyDashboard";
                    $('#divMyDashboard').show();
                    $scope.selectedTodo = 0;
                    $scope.selectedProject = 0;
                    todosFactory.getToDosInProgress().success(function (data) {
                        $scope.todosInProgress = data;
                        if (data.length == 0) {
                            $scope.showNoTodosInProgressRecordsMessage = true;
                        }
                        $timeout(function () {
                            $scope.$apply();
                            todosFactory.getPendingToDos().success(function (datas) {
                                $scope.pendingTodos = datas;
                                if (datas.length == 0) {
                                    $scope.showNoTodosPendingRecordsMessage = true;
                                }
                                $timeout(function () {
                                    $scope.$apply();
                                    $scope.mydashboardwindowloader = false;
                                    $scope.hideMyDashboard = false;
                                });
                            });
                        });
                    });
                };
        */

        /* Getting My Notifications */
        $scope.MyNotifications = function () {
            closeCentreSections();
            $scope.notificationdata = "";
            $scope.notificationFilter = "";
            $scope.showNoNotificationRecordsMessage = false;
            $scope.notificationswindowloader = true;
            $scope.notificationsWindow = false;
            $scope.selectedNavigation = 'Notifications';
            $scope.selectedWindow = "MyNotifications";
            $scope.selectedNotification = 0;
            $scope.selectedProject = 0;
            $('#divNotifications').show();
            notificationFactory.getNotifications().success(function (data) {


                $scope.notificationdata = data;
                $scope.notificationswindowloader = false;
                if (data.length == 0) {
                    $scope.showNoNotificationRecordsMessage = true;
                }

            });
        };


        /* Updating IsDone field in ToDos table*/
        $scope.updateTodoIsDone = function ($event, id) {
            $('#divToDoDescriptionSection').hide();
            var checkbox = $event.target;
            var action = (checkbox.checked ? 'true' : 'false');
            if (action == 'true') {
                $('#ajax_loader').show();
                $scope.selectedToDo.id = id;

                $scope.selectedToDo.isDone = "Y";
                $scope.selectedToDo.updateFlag = 4;
                todosFactory.updateTodo($scope.selectedToDo)
                                     .success(function (data) {

                                         switch ($scope.selectedWindow) {

                                             case "assigned": {

                                                 todosFactory.getMyAssignedToDos()
                                                    .success(function (data) {
                                                        $scope.myassignedtodos = data;

                                                        $timeout(function () {
                                                            $scope.$apply();

                                                        });


                                                    })
                                                     .error(function (error) {
                                                         $scope.status = 'Unable to load TODOs: ' + error.message;
                                                     });

                                                 break;
                                             };
                                             case "unassigned": {

                                                 todosFactory.getMyUnassignedToDos()
                                                  .success(function (data) {
                                                      $scope.myunassignedtodos = data;

                                                      $timeout(function () {
                                                          $scope.$apply();

                                                      });


                                                  })
                                                   .error(function (error) {
                                                       $scope.status = 'Unable to load TODOs: ' + error.message;
                                                   });

                                                 break;
                                             };
                                             case "personal": {

                                                 todosFactory.getMyPersonalAssignedToDos()
                                                 .success(function (data) {
                                                     $scope.mypersonalassignedtodos = data;

                                                     $timeout(function () {
                                                         $scope.$apply();

                                                     });


                                                 })
                                                  .error(function (error) {
                                                      $scope.status = 'Unable to load TODOs: ' + error.message;
                                                  });

                                                 break;
                                             };
                                             case "defaultTodosWindow": {

                                                 $scope.GetToDos();

                                                 break;
                                             };

                                             default: {

                                                 break;
                                             };

                                         }

                                         $timeout(function () {
                                             $scope.$apply();
                                             $('#ajax_loader').hide();

                                             toaster.success("ToDo is completed and moved to archived Successfully");
                                         });


                                     })
                                      .error(function (error) {
                                          //   $scope.status = 'Unable to save ToDo : ' + error.message;
                                      });
            }
            else {
                $('#ajax_loader').show();
                $scope.selectedToDo.id = id;

                $scope.selectedToDo.isDone = "N";
                $scope.selectedToDo.updateFlag = 4;
                todosFactory.updateTodo($scope.selectedToDo)
                                     .success(function (data) {

                                         todosFactory.getMyArchivedToDos()
                                         .success(function (data) {
                                             $scope.MyArchivedTodos = data;
                                         })
                                          .error(function (error) {
                                              $scope.status = 'Unable to load TODOs: ' + error.message;
                                          });

                                         $timeout(function () {
                                             $scope.$apply();
                                             $('#ajax_loader').hide();

                                             toaster.success("ToDo is uncompleted and removed from archived Successfully");

                                         });


                                     })
                                      .error(function (error) {
                                          //   $scope.status = 'Unable to save ToDo : ' + error.message;
                                      });
            }



        };


        /* Hiding the Users when the user is selected as Assignee*/
        $scope.hideAssignees = function () {

            $scope.AllAssignees = null;

        };


        /* Closing the Assignee Window*/
        $scope.removeWindow = function () {

            $scope.AllAssignees = null;
            $scope.assigneediv = false;
            $scope.txtAssignees1 = "";
        };

        /* Preventing Blur on Mouse Over */
        $scope.preventBlur = function ($event) {

            $event.preventDefault();
            $event.stopPropagation();

        };


        /* Updating the assignee for the Selected Todo*/
        $scope.updateAssignee = function (assigneeId, Firstname, Lastname, EmailId, profilePhoto, isThereProfilePhoto) {

            var Name = Firstname + ' ' + Lastname;
            if ($scope.txtAssignees == Name) {
                $scope.assigneediv = false;
                toaster.error("It is already assigned to this user");
            }
            else {

                //$scope.todos[$scope.indexOfTodo].assigned = true;
                //  $scope.todos[$scope.indexOfTodo].inProgress = false;
                if (isThereProfilePhoto == 'True') {
                    // $scope.todos[$scope.indexOfTodo].displayPhoto = true;
                    // $scope.todos[$scope.indexOfTodo].profilePhoto = profilePhoto;
                }
                else {

                    //  $scope.todos[$scope.indexOfTodo].displayPhoto = false;
                }
                // $scope.todos[$scope.indexOfTodo].firstName = Firstname;
                // $scope.todos[$scope.indexOfTodo].lastName = Lastname;

                $('#globalLoader').show();

                $scope.AllAssignees = null;
                $scope.txtAssignees = Name;
                $scope.selectedToDo.assigneeId = assigneeId;
                $scope.selectedToDo.id = $scope.toDoId;
                $scope.selectedToDo.name = Name;
                $scope.selectedToDo.selfAssigned = false;
                $scope.selectedToDo.updateFlag = 2;
                todosFactory.updateTodo($scope.selectedToDo)
                                     .success(function (data) {

                                         $scope.todoNotificationsArray = {};
                                         $scope.todoNotificationsArray.action = "Assigned";
                                         $scope.todoNotificationsArray.userId = assigneeId;
                                         $scope.todoNotificationsArray.todoId = $scope.toDoId;

                                         todoNotificationsFactory.insertTodoNotification($scope.todoNotificationsArray).success(function () {

                                             var date = new Date();
                                             var hours = date.getHours();
                                             var minutes = date.getMinutes();
                                             var ampm = hours >= 12 ? 'PM' : 'AM';
                                             hours = hours % 12;
                                             hours = hours ? hours : 12; // the hour '0' should be '12'
                                             minutes = minutes < 10 ? '0' + minutes : minutes;
                                             var strTime = hours + ':' + minutes + ' ' + ampm;
                                             $scope.pushdata = {};
                                             $scope.pushdata.notification = $scope.MyProfileObject.firstName + " has assigned this todo to " + Firstname;
                                             $scope.pushdata.profilePhoto = "";
                                             $scope.pushdata.createdDate = strTime;
                                             var guidId = guid();
                                             $scope.newNotificationId = guidId;
                                             $scope.pushdata.id = "hello";

                                             $scope.todoNotificationsData.push($scope.pushdata);
                                             $('#hello').focus();


                                         });

                                         todosFactory.getSingleToDo($scope.toDoId)
                                               .success(function (data) {
                                                   $scope.singleTodoDescription = data;
                                                   $scope.ToDoTaskName = data[0].name;
                                                   $scope.ToDoProjectName = data[0].projectName;
                                                   $scope.txtAssignees = data[0].assignedTo;
                                                   $scope.todoCreatedBy = data[0].assignedBy;
                                                   $scope.inboxToDoDescription = data[0].description;
                                                   $scope.dt1 = data[0].deadlineDate;
                                                   $scope.assigneeEmailId = data[0].email;

                                               });


                                         $timeout(function () {
                                             $scope.$apply();
                                             $('#globalLoader').hide();
                                             $scope.assigneediv = false;
                                             toaster.success("Todo Assigned Successfully");
                                         });




                                     })
                                      .error(function (error) {
                                          //   $scope.status = 'Unable to save ToDo : ' + error.message;
                                      });

                $scope.email = {};
                $scope.email.emailId = EmailId;
                $scope.email.username = Name;
                $scope.email.todo = $scope.ToDoTaskName;
                $scope.email.projectName = $scope.projectName;
                $scope.email.flag = 1;


                emailFactory.sendEmail($scope.email).success(function () { });


                $scope.notification.name = "has assigned you a task";
                $scope.notification.toDoId = $scope.toDoId;
                $scope.notification.notifyingUserId = assigneeId;

                notificationFactory.insertNotification($scope.notification).success(function () {

                    $scope.temporaryId = 1;
                    usersFactory.getUser($scope.temporaryId)
                               .success(function (data) {
                                   $scope.NotifyCount = data.notificationCount == 0 ? null : data.notificationCount;
                                   $scope.hideNotifyCount = data.notificationCount == 0 ? true : false;
                               })
                                .error(function (error) {
                                    //$scope.status = 'Unable to load Projects data: ' + error.message;
                                });

                });
            }
        };


        /*  Removing the assignee to whom the task was assigned*/
        $scope.RemoveAssignee = function (item) {
            $('#globalLoader').show();
            $scope.selectedToDo.assigneeId = '';
            $scope.selectedToDo.id = $scope.toDoId;
            $scope.selectedToDo.updateFlag = 6;
            todosFactory.updateTodo($scope.selectedToDo)
                                 .success(function (data) {
                                     item.taskStatus = "Pending";
                                     $scope.InProgress = false;
                                     $scope.IsDone = false;
                                     $scope.todos[$scope.indexOfTodo].assigned = false;
                                     $scope.todos[$scope.indexOfTodo].inProgress = false;
                                     $scope.todoNotificationsArray = {};
                                     $scope.todoNotificationsArray.action = "Unassigned";
                                     $scope.todoNotificationsArray.userId = item.assigneeId;
                                     $scope.todoNotificationsArray.todoId = $scope.toDoId;

                                     todoNotificationsFactory.insertTodoNotification($scope.todoNotificationsArray).success(function () {

                                         var date = new Date();
                                         var hours = date.getHours();
                                         var minutes = date.getMinutes();
                                         var ampm = hours >= 12 ? 'PM' : 'AM';
                                         hours = hours % 12;
                                         hours = hours ? hours : 12; // the hour '0' should be '12'
                                         minutes = minutes < 10 ? '0' + minutes : minutes;
                                         var strTime = hours + ':' + minutes + ' ' + ampm;
                                         $scope.pushdata = {};
                                         $scope.pushdata.notification = $scope.MyProfileObject.firstName + " has unassigned this todo to " + item.assigneeFirstName;
                                         $scope.pushdata.profilePhoto = "";
                                         $scope.pushdata.createdDate = strTime;

                                         $scope.todoNotificationsData.push($scope.pushdata);


                                     });


                                     $timeout(function () {
                                         $scope.$apply();
                                         todosFactory.getSingleToDo($scope.toDoId)
                                            .success(function (data) {
                                                $scope.singleTodoDescription = data;
                                                $scope.ToDoTaskName = data[0].name;
                                                $scope.ToDoProjectName = data[0].projectName;
                                                $scope.txtAssignees = data[0].assignedTo;
                                                $scope.todoCreatedBy = data[0].assignedBy;
                                                $scope.inboxToDoDescription = data[0].description;
                                                $scope.dt1 = data[0].deadlineDate;
                                                $scope.assigneeEmailId = data[0].email;
                                                $timeout(function () {
                                                    $scope.$apply();

                                                })
                                            });

                                     });

                                     $timeout(function () {
                                         $scope.$apply();
                                         $('#globalLoader').hide();
                                         $scope.assigneediv = false;
                                         toaster.success("Todo Unassigned Successfully");
                                     });

                                 })
                                  .error(function (error) {
                                      //   $scope.status = 'Unable to save ToDo : ' + error.message;
                                  });

            $scope.email = {};
            $scope.email.emailId = $scope.assigneeEmailId;
            // $scope.email.username = Name;
            $scope.email.todo = $scope.ToDoTaskName;
            $scope.email.projectName = $scope.projectName;
            $scope.email.flag = 2;
            emailFactory.sendEmail($scope.email).success(function () { });

        };


        /* Updating the Description for the Selected Todo*/
        $scope.updateTodoDescription = function () {

            $scope.selectedToDo.id = $scope.toDoId;

            $scope.selectedToDo.description = $scope.inboxToDoDescription;

            $scope.selectedToDo.updateFlag = 3;
            todosFactory.updateTodo($scope.selectedToDo)
                                 .success(function (data) {
                                     if ($scope.selectedWindow == "defaultTodosWindow") {
                                         $scope.todos[$scope.indexOfTodo].description = $scope.inboxToDoDescription;
                                     }
                                     toaster.success("Description Updated Successfully");
                                 })
                                  .error(function (error) {
                                      //   $scope.status = 'Unable to save ToDo : ' + error.message;
                                  });
        };


        /* Getting  Todos with respect to a particular project */
        $scope.GetToDos = function GetToDos() {


            $('#globalLoader').show();

            $scope.todotypeflag = 0;


            todosFactory.getToDos($scope.projectId).success(function (data) {
                GetSections();
                $scope.todos = data;
                $('#globalLoader').hide();

            })
                                   .error(function (error) {
                                   });
        }


        /* Followers Functionality starts here */
        /* SHow Followers in the Todo*/
        $scope.showFollowers = function showFollowers() {
            $scope.searchFollower = '';

            followersFactory.getUsers($scope.toDoId).success(function (data) {
                $scope.AllFollowers = data;

                $timeout(function () {
                    $scope.$apply();
                    $scope.hideFollower = true;

                });
                $timeout(function () {
                    $scope.$apply();
                    $('#txtFollower').focus();
                });
                $timeout(function () {
                    $scope.$apply();

                });
            });

        };


        /* Insert Follower for the Todo*/
        $scope.insertFollower = function (UserId, Name, EmailId) {
            $('#globalLoader').show();
            $scope.searchFollower = '';
            $scope.follower = {};
            $scope.follower.toDoId = $scope.toDoId;
            $scope.follower.userId = UserId;
            followersFactory.insertFollower($scope.follower).success(function (data) {

                $scope.getFollowers($scope.toDoId);
                $scope.showFollowers();
                $timeout(function () {
                    $scope.$apply();

                    $timeout(function () {
                        $scope.$apply();
                        $('#txtFollower').focus();
                    });
                    $timeout(function () {
                        $scope.$apply();
                        $('#globalLoader').hide();
                        toaster.success("Follower added Successfully");
                    });
                });

            });

            $scope.email = {};
            $scope.email.emailId = EmailId;
            $scope.email.username = Name;
            $scope.email.todo = $scope.ToDoTaskName;
            $scope.email.projectName = $scope.projectName;
            $scope.email.flag = 3;
            emailFactory.sendEmail($scope.email).success(function () { });

            $scope.notification.name = "has added you as follower";
            $scope.notification.toDoId = $scope.toDoId;
            $scope.notification.notifyingUserId = UserId;

            notificationFactory.insertNotification($scope.notification).success(function () {

                $scope.temporaryId = 1;
                usersFactory.getUser($scope.temporaryId)
                           .success(function (data) {
                               $scope.NotifyCount = data.notificationCount == 0 ? null : data.notificationCount;
                               $scope.hideNotifyCount = data.notificationCount == 0 ? true : false;
                           })
                            .error(function (error) {
                                //$scope.status = 'Unable to load Projects data: ' + error.message;
                            });

            });
        };


        /* Delete Follower from the Todo*/
        $scope.deleteFollower = function (Id, Name, EmailId) {
            $('#globalLoader').show();
            followersFactory.deleteFollower(Id).success(function (data) {

                $scope.getFollowers($scope.toDoId);
                $scope.showFollowers();
                $timeout(function () {
                    $scope.$apply();
                    $('#globalLoader').hide();
                    toaster.success("Follower removed Successfully");
                });
                $timeout(function () {
                    $scope.$apply();
                    $('#txtFollower').focus();
                });
                $timeout(function () {
                    $scope.$apply();
                });
            });

            $scope.email = {};
            $scope.email.emailId = EmailId;
            $scope.email.username = Name;
            $scope.email.todo = $scope.ToDoTaskName;
            $scope.email.projectName = $scope.projectName;
            $scope.email.flag = 4;
            emailFactory.sendEmail($scope.email).success(function () { });
        };


        /* Get Followers for the Todo*/
        $scope.getFollowers = function getFollowers(ToDoId) {

            followersFactory.getFollowers(ToDoId).success(function (data) {

                $scope.followers = data;
                $timeout(function () {
                    $scope.$apply();
                });

            });
        };


        /*Close Follower Block on blur*/
        $scope.closeFollowerBlock = function () {
            $scope.hideFollower = false;
        };
        /* Followers Functionality ends here */


        /* SHow Section Modal Pop Up*/
        $scope.ShowSectionModal = function () {
            GetUpdatableSections();
            $timeout(function () {
                $scope.$apply();
                $('#modalSection').modal('show');
            });

        };


        /* Getting Sections For The Selected Project*/
        function GetSections() {

            sectionsFactory.getSections($scope.projectId).success(function (data) {

                $scope.Sections = data;
            });

        };


        /*Getting Updatable Sections For The Selected Project*/
        function GetUpdatableSections() {

            sectionsFactory.getUpdatableSections($scope.projectId).success(function (data) {

                $scope.UpdatableSections = data;
            });

        };


        /* Saving New Sections for a particular Project*/
        $scope.saveSection = function () {
            $('#globalLoader').show();
            $scope.section = {};
            $scope.section.sectionName = $scope.txtSectionName;
            $scope.section.projectId = $scope.projectId;
            sectionsFactory.insertSection($scope.section).success(function (data) {

                GetUpdatableSections();
                $scope.GetToDos();
                $timeout(function () {
                    $scope.$apply();
                    $scope.txtSectionName = '';
                    $('#globalLoader').hide();
                    toaster.success("Section Saved Successfully");
                });
            });

        };





        /* Deleting Section */
        $scope.deleteSection = function (Id) {
            $('#globalLoader').show();
            sectionsFactory.deleteSection(Id).success(function (data) {
                GetUpdatableSections();
                $scope.GetToDos();
                $timeout(function () {
                    $scope.$apply();
                    $('#globalLoader').hide();
                    toaster.success("Section Deleted Successfully");
                });
            });

        };


        /*Drag Drop Functionality on Section Change*/
        $scope.onSectionChange = function (index, obj, evt, sectionId, sectionName) {

            if (obj.sectionId != sectionId) {
                obj.sectionId = sectionId;
                if (obj.id == $scope.selectedTodo) {
                    $scope.sectionName = sectionName;
                }
                toaster.success("Moved To Other Section");
                $scope.selectedToDo.id = obj.id;
                $scope.selectedToDo.sectionId = sectionId;
                $scope.selectedToDo.updateFlag = 5;
                todosFactory.updateTodo($scope.selectedToDo)
                                     .success(function (data) {
                                         // $scope.GetToDos();
                                         $timeout(function () {
                                             $scope.$apply();

                                         });
                                     })
                                      .error(function (error) {

                                      });
            }
            else {
                toaster.error("It is already in this section");

            }

        }


        /*Drag Drop Functionality on Team User addition*/
        $scope.onTeamUserAdd = function (item) {

            if (item.color == "red") {

                item.color = "";
            }
            else {
                item.color = "red";
            }

        };

        // Disable weekend selection
        //$scope.disabled = function (date, mode) {
        //    return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        //};

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };

        $scope.opendate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openeddt = true;
        };

        $scope.closedate = function () {
            $scope.openeddt = false;

        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MM.dd.yyyy', 'shortDate'];
        $scope.format = $scope.formats[2];


        /*  SHow Remove Assignee Button on Mouse Over*/
        $scope.showRemoveButton = function () {

            $scope.showremoveassignee = true;

        };


        /*  Hide Remove Assignee Button on Mouse Over*/
        $scope.hideRemoveButton = function () {

            $scope.showremoveassignee = false;
        };


        /* Showing Assignee Div */
        $scope.showAssigneeDiv = function () {

            $scope.AllAssignees = $scope.assignee;


            $scope.assigneediv = true;

            $timeout(function () {

                $scope.$apply();

                $('#txtAssigneesInput').focus();
            });

            $timeout(function () {
                $scope.$apply();

            });
        };



        /* Getting Google Drive Files*/
        $scope.files = [];
        $scope.$watchCollection('files', function () {


            if ($scope.files.length > 0) {
                $('#globalLoader').show();
                for (var i = 0; i < $scope.files.length; i++) {
                    $scope.attachment = {};
                    $scope.attachment.toDoId = $scope.todoDescriptionObject.toDoId;
                    $scope.attachment.originalName = $scope.files[i].name;
                    $scope.attachment.path = $scope.files[i].url;
                    $scope.attachment.sourceImage = 'logo-drive.png';
                    attachmentsFactory.saveTodoAttachments($scope.attachment).success(function (data) {

                        $timeout(function () {
                            $scope.$apply();
                            attachmentsFactory.getToDoAttachments($scope.todoDescriptionObject.toDoId).success(function (data) {
                                $scope.attachmentsObject = data;
                                $timeout(function () {
                                    $scope.$apply();

                                });
                            })
                                        .error(function (error) {
                                        });
                        });




                    });
                }
                //var fileCount = $scope.files.length == 1 ? $scope.files.length + " file from Google Drive" : $scope.files.length + " files from  Google Drive";
                //$scope.notification.name = "has uploaded " + fileCount;
                //$scope.notification.toDoId = $scope.toDoId;
                //$scope.notification.flag = 1;
                //notificationFactory.insertNotification($scope.notification).success(function () {

                //    $scope.temporaryId = 1;
                //    usersFactory.getUser($scope.temporaryId)
                //               .success(function (data) {
                //                   $scope.NotifyCount = data.notificationCount == 0 ? null : data.notificationCount;
                //                   $scope.hideNotifyCount = data.notificationCount == 0 ? true : false;
                //               })
                //                .error(function (error) {
                //                    //$scope.status = 'Unable to load Projects data: ' + error.message;
                //                });

                //});


                $('#globalLoader').hide();
                toaster.success("Google Drive", "Files Saved Successfully");
            }
            //$scope.files = [];

        });

        $scope.ShowFiles = function () {

            alert($scope.files);

        };

        $scope.languages = [
          { code: 'en', name: 'English' },
          { code: 'fr', name: 'Français' },
          { code: 'ja', name: '日本語' },
          { code: 'ko', name: '한국' },
        ]

        // Check for the current language depending on lkGoogleSettings.locale
        $scope.initialize = function () {

            angular.forEach($scope.languages, function (language, index) {
                if (lkGoogleSettings.locale === language.code) {
                    $scope.selectedLocale = $scope.languages[index];
                }
            });

        }

        // Define the locale to use
        $scope.changeLocale = function (locale) {
            lkGoogleSettings.locale = locale.code;
        }












        /* Getting Drop Box */
        $scope.dpfiles = [];
        $scope.$watchCollection('dpfiles', function () {

            if ($scope.dpfiles.length > 0) {
                $('#globalLoader').show();
                for (var i = 0; i < $scope.dpfiles.length; i++) {
                    $scope.attachment = {};
                    $scope.attachment.toDoId = $scope.todoDescriptionObject.todoId;
                    $scope.attachment.originalName = $scope.dpfiles[i].name;
                    $scope.attachment.path = $scope.dpfiles[i].link;
                    $scope.attachment.sourceImage = 'drop-box.png';
                    attachmentsFactory.saveTodoAttachments($scope.attachment).success(function (data) {

                        $timeout(function () {
                            $scope.$apply();
                            attachmentsFactory.getToDoAttachments($scope.todoDescriptionObject.todoId).success(function (data) {
                                $scope.attachmentsObject = data;

                                $timeout(function () {
                                    $scope.$apply();

                                });
                            })
                                        .error(function (error) {
                                        });
                        });



                    });
                }

                var fileCount = $scope.dpfiles.length == 1 ? $scope.dpfiles.length + " file from Dropbox" : $scope.dpfiles.length + " files from Dropbox";
                $scope.notification.name = "has uploaded " + fileCount;
                $scope.notification.toDoId = $scope.toDoId;
                $scope.notification.flag = 1;
                notificationFactory.insertNotification($scope.notification).success(function () {

                    $scope.temporaryId = 1;
                    usersFactory.getUser($scope.temporaryId)
                               .success(function (data) {
                                   $scope.NotifyCount = data.notificationCount == 0 ? null : data.notificationCount;
                                   $scope.hideNotifyCount = data.notificationCount == 0 ? true : false;
                               })
                                .error(function (error) {
                                    //$scope.status = 'Unable to load Projects data: ' + error.message;
                                });

                });

                $('#globalLoader').hide();
                toaster.success("Drop Box", "Files Saved Successfully");

            }

            $scope.dpfiles = [];
        });
        $scope.remove = function (idx) {
            $scope.dpfiles.splice(idx, 1);
        }


        /* Getting  Box Files */
        $scope.boxfiles = [];
        $scope.$watchCollection('boxfiles', function () {

            if ($scope.boxfiles.length > 0) {
                $('#globalLoader').show();
                for (var i = 0; i < $scope.boxfiles.length; i++) {
                    $scope.attachment = {};
                    $scope.attachment.toDoId = $scope.todoDescriptionObject.todoId;
                    $scope.attachment.originalName = $scope.boxfiles[i].name;
                    $scope.attachment.path = $scope.boxfiles[i].url;
                    $scope.attachment.sourceImage = 'box-icon.png';
                    attachmentsFactory.saveTodoAttachments($scope.attachment).success(function (data) {
                        $timeout(function () {
                            $scope.$apply();
                            attachmentsFactory.getToDoAttachments($scope.todoDescriptionObject.todoId).success(function (data) {
                                $scope.attachmentsObject = data;
                                $timeout(function () {
                                    $scope.$apply();
                                });
                            })
                                        .error(function (error) {
                                        });
                        });

                    });
                }

                var fileCount = $scope.boxfiles.length == 1 ? $scope.boxfiles.length + " file from Box" : $scope.boxfiles.length + " files from Box";
                $scope.notification.name = "has uploaded " + fileCount;
                $scope.notification.toDoId = $scope.toDoId;
                $scope.notification.flag = 1;
                notificationFactory.insertNotification($scope.notification).success(function () {

                    $scope.temporaryId = 1;
                    usersFactory.getUser($scope.temporaryId)
                               .success(function (data) {
                                   $scope.NotifyCount = data.notificationCount == 0 ? null : data.notificationCount;
                                   $scope.hideNotifyCount = data.notificationCount == 0 ? true : false;
                               })
                                .error(function (error) {
                                    //$scope.status = 'Unable to load Projects data: ' + error.message;
                                });

                });


                $('#globalLoader').hide();
                toaster.success("BOX", "FIles Saved Successfully");
            }
            $scope.boxfiles = [];

        });
        $scope.removeboxfiles = function (idx) {
            $scope.boxfiles.splice(idx, 1);
        }


        /*   Applying Toasters for notifications and messages*/
        $scope.bar = 'Hi';
        $scope.pop = function () {

            toaster.warning({ title: "Smart Admin", body: "Your Record has been Saved" });
            //toaster.error("title", "text2");
            //toaster.pop({ type: 'wait', title: "title", body: "text" });
            //toaster.pop('success', "title", '<ul><li>Render html</li></ul>', 5000, 'trustedHtml');
            //toaster.pop('error', "title", '<ul><li>Render html</li></ul>', null, 'trustedHtml');
            //toaster.pop('wait', "title", null, null, 'template');
            //toaster.pop('warning', "title", "myTemplate.html", null, 'template');
            //toaster.pop('note', "title", "text");
            toaster.pop('success', "title", 'its address is https://google.com.', 5000, 'trustedhtml', function (toaster) {
                var match = toaster.body.match(/http[s]?:\/\/[^\s]+/);
                if (match) $window.open(match[0]);
                return true;
            });

            //toaster.pop('warning', "Hi ", "{template: 'myTemplateWithData.html', data: 'MyData'}", 15000, 'templateWithData');

        };
        $scope.goToLink = function (toaster) {
            var match = toaster.body.match(/http[s]?:\/\/[^\s]+/);
            if (match) $window.open(match[0]);
            return true;
        };
        $scope.clear = function () {
            toaster.clear();
        };


        /*  Select List in default todo Window*/
        $scope.selectList = function () {

            $scope.selectedtodo = "list";


            $scope.selectedTodo = "";
            $scope.stretchCentreWindow = true;
            $scope.hideRightWindow = true;
        };

        /* Select KanBan view in default todo Window */
        $scope.selectKanBanView = function () {

            $scope.selectedtodo = "kanban";
            $scope.stretchCentreWindow = true;
            $scope.hideRightWindow = true;

        };

        /* Get attachments in todo window for particular project*/
        $scope.getProjectAttachments = function () {

            $scope.stretchCentreWindowLoader = true;
            $scope.selectedtodo = "attachment";
            $scope.stretchCentreWindow = true;
            $scope.hideRightWindow = true;


            attachmentsFactory.getProjectAttachments($scope.projectId).success(function (data) {

                $scope.projectAttachments = data;
                $scope.stretchCentreWindowLoader = false;

            });

        };


        /*  Logout */
        $scope.logout = function () {

            accountFactory.Logout().success(function (data) {
                //window.location = "Account/Login";
                window.location = "/Login";
            });

        };


        /*   Notification Setting on cHANGE EVENT*/
        $scope.NotificationDdlChange = function () {

            alert('sdfsdfs');
            toaster.success("Notification Setting Changed Successfully");

        };








        /* Opening Users view for editing Users*/
        $scope.EditUsers = function () {

            $window.location = "/account/users";

        };


        /* Opening Team Users modal pop up for editing users in Team */
        $scope.EditTeamUsers = function () {

            $('#modalTeamUsers').modal('show');


        };


        /* Showing Settings Section */
        $scope.ShowSettingsSection = function () {

            $scope.notificationswindowloader = true;

            $timeout(hideloader, 1000);
            function hideloader() {
                $scope.notificationswindowloader = false;
                $scope.notificationsWindow = true;
            };


        };


        /* SHowing Notifications Section*/
        $scope.ShowNotificationsSection = function () {

            $scope.notificationswindowloader = true;

            $timeout(hideloader, 1000);
            function hideloader() {
                $scope.notificationswindowloader = false;
                $scope.notificationsWindow = false;
            };

        };


        /* Change Notification Setting for email*/
        $scope.ChangeNotificationSetting = function (command) {

            $('#globalLoader').show();
            $scope.notificationemail = {};
            $scope.notificationemail.sendNotificationEmail = command;
            $scope.notificationemail.updateflag = 3;
            usersFactory.updateUser($scope.notificationemail).success(function () {

                $scope.SendNotification = command;
                $('#globalLoader').hide();
            });
            /* Command is 1 when email notification is ON and it is 0 WHen it is off*/


        };





        /* UserProfile is a CLASS which has all the FUNCTIONS related to USER PROFILE */

        $scope.UserProfile = {

            /* Showing User profile with Details and Edit Options*/
            showUserProfile: function () {

                $scope.UserProfileObject = $scope.MyProfileObject;
                $scope.UserTeamsObject = $scope.TeamsObject;
                closeCentreSections();
                $scope.otherUser = false;
                $scope.showCenterLoader = true;
                $scope.selectedProject = 0;
                $scope.selectedTodo = 0;
                $scope.selectedNavigation = "MyProfile";

                /* Getting projects in which user is project member irrespective of teamid */
                projectsFactory.getUserProjects($scope.MyProfileObject.userId).success(function (data) {

                    if (data.success) {
                        $scope.MyProjectsObject = data.responseData;
                        $scope.showCenterLoader = false;
                        $('#divProfile').show();
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });
            },


            /* Showing Third User Prifile with Details Only  */
            showOtherUserProfile: function (userId) {

                if (userId == null || userId == undefined) {

                    return;

                }

                if (userId == $scope.MyProfileObject.userId) {
                    $scope.UserProfileObject = $scope.MyProfileObject;
                    $scope.UserTeamsObject = $scope.TeamsObject;
                    closeCentreSections();
                    $scope.showCenterLoader = true;
                    $scope.selectedProject = 0;
                    $scope.selectedTodo = 0;
                    $scope.selectedNavigation = "MyProfile";
                    $scope.otherUser = false;

                    /* Getting projects in which user is project member irrespective of teamid */
                    projectsFactory.getUserProjects(userId).success(function (data) {

                        if (data.success) {

                            $scope.MyProjectsObject = data.responseData;
                            $scope.showCenterLoader = false;
                            $('#divProfile').show();
                        }
                        else {

                            toaster.error("Error", data.message);
                        }

                    }).error(function () {

                        toaster.error("Error", "Some Error Occured !");

                    });

                }
                else {
                    closeCentreSections();
                    $scope.showCenterLoader = true;
                    $scope.selectedProject = 0;
                    $scope.selectedTodo = 0;
                    $scope.selectedNavigation = "MyProfile";
                    $scope.otherUser = true;

                    /* Getting User Details  */
                    usersFactory.getUser(userId).success(function (data) {

                        if (data.success) {
                            $scope.UserProfileObject = data.responseData;



                            /* Getting projects in which user is project member irrespective of teamid */
                            projectsFactory.getUserProjects(userId).success(function (data) {

                                if (data.success) {
                                    $scope.MyProjectsObject = data.responseData;



                                    /*  Getting User Teams */
                                    teamFactory.getUserTeams(userId).success(function (data) {

                                        if (data.success) {
                                            $scope.UserTeamsObject = data.responseData;
                                            $scope.showCenterLoader = false;
                                            $('#divProfile').show();

                                        }
                                        else {

                                            toaster.error("Error", data.message);
                                        }

                                    }).error(function () {

                                        toaster.error("Error", "Some Error Occured !");

                                    });



                                }
                                else {

                                    toaster.error("Error", data.message);
                                }

                            }).error(function () {

                                toaster.error("Error", "Some Error Occured !");

                            });




                        }
                        else {

                            toaster.error("Error", data.message);
                        }

                    }).error(function () {

                        toaster.error("Error", "Some Error Occured !");

                    });






                }

            },

            /* Open Modal popup for editing User Profile */
            editUserProfile: function () {

                $scope.MyProfileEditObject = {};
                $scope.MyProfileEditObject.firstName = $scope.MyProfileObject.firstName;
                $scope.MyProfileEditObject.lastName = $scope.MyProfileObject.lastName;
                $scope.MyProfileEditObject.gender = $scope.MyProfileObject.gender;
                $scope.MyProfileEditObject.address = $scope.MyProfileObject.address;
                $scope.MyProfileEditObject.dateOfBirth = $scope.MyProfileObject.dateOfBirth;
                $scope.MyProfileEditObject.phoneNo = $scope.MyProfileObject.phoneNo;
                $scope.MyProfileEditObject.designation = $scope.MyProfileObject.designation;
                $scope.MyProfileEditObject.department = $scope.MyProfileObject.department;
                $scope.MyProfileEditObject.experience = $scope.MyProfileObject.experience;
                $scope.MyProfileEditObject.expertise = $scope.MyProfileObject.expertise;
                $scope.MyProfileEditObject.aboutMe = $scope.MyProfileObject.aboutMe;
                $scope.MyProfileEditObject.email = $scope.MyProfileObject.email;

                $('#modalMyProfileEdit').modal('show');
            },


            /*  Update User Profile */
            updateUserProfile: function () {


                $scope.MyProfileEditObject.updateflag = 4;

                usersFactory.updateUser($scope.MyProfileEditObject).success(function (data) {

                    if (data.success) {

                        $scope.MyProfileObject.firstName = $scope.MyProfileEditObject.firstName;
                        $scope.MyProfileObject.lastName = $scope.MyProfileEditObject.lastName;
                        $scope.MyProfileObject.gender = $scope.MyProfileEditObject.gender;
                        $scope.MyProfileObject.address = $scope.MyProfileEditObject.address;
                        $scope.MyProfileObject.dateOfBirth = $scope.MyProfileEditObject.dateOfBirth;
                        $scope.MyProfileObject.phoneNo = $scope.MyProfileEditObject.phoneNo;
                        $scope.MyProfileObject.designation = $scope.MyProfileEditObject.designation;
                        $scope.MyProfileObject.department = $scope.MyProfileEditObject.department;
                        $scope.MyProfileObject.experience = $scope.MyProfileEditObject.experience;
                        $scope.MyProfileObject.expertise = $scope.MyProfileEditObject.expertise;
                        $scope.MyProfileObject.aboutMe = $scope.MyProfileEditObject.aboutMe;


                        $('#modalMyProfileEdit').modal('hide');
                        toaster.success("Success", data.message);

                    }
                    else {
                        if (data.message == undefined) {

                            toaster.error("Error", data.errors.toString());

                        }
                        else {
                            toaster.error("Error", data.message);
                        }

                    }
                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");


                });
            },


            /* Updating User Profile Photo*/
            updateUserPhoto: function ($files) {


                //$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {

                    var $file = $files[i];
                    (function (index) {
                        $scope.upload[index] = $upload.upload({
                            url: WebDomainPath + "/API/UsersAPI/UploadProfile", // webapi url
                            method: "POST",
                            data: { UploadedProfile: $file },
                            file: $file
                        }).progress(function (evt) {

                            // get upload percentage
                            $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);

                        }).success(function (data, status, headers, config) {
                            // file is uploaded successfully

                            if (data.success) {

                                toaster.success("Success", data.message);

                                angular.forEach(
                                angular.element("input[type='file']"),
                                        function (inputElem) {
                                            angular.element(inputElem).val(null);
                                        });


                                /* Getting Updated User Details */
                                usersFactory.getUser($scope.temporaryId).success(function (data) {

                                    if (data.success) {

                                        $scope.MyProfileObject = data.responseData;

                                    }
                                    else {

                                        toaster.error("Error", data.message);
                                    }


                                }).error(function () {
                                    toaster.error("Error", "Some Error Occured !");
                                });



                                $timeout(function () {
                                    $scope.$apply();
                                    projectsFactory.getUserProjects($scope.MyProfileObject.userId).success(function (data) {

                                        if (data.success) {

                                            $scope.MyProjectsObject = data.responseData;

                                            $timeout(function () {
                                                $scope.$apply();
                                                teamFactory.getLoggedInUserTeams($scope.MyProfileObject.userId).success(function (data) {

                                                    if (data.success) {
                                                        $scope.TeamsObject = data.responseData;
                                                        $scope.activeTeam = $scope.TeamsObject[0];
                                                    }
                                                    else {

                                                        toaster.error("Error", data.message);
                                                    }

                                                }).error(function () {

                                                    toaster.error("Error", "Some Error Occured !")
                                                });


                                            });
                                        }
                                        else {

                                            toaster.error("Error", data.message);
                                        }

                                        $scope.MyProjectsObject = data;



                                    });
                                });

                            }

                            else {

                                angular.forEach(
                                 angular.element("input[type='file']"),
                                         function (inputElem) {
                                             angular.element(inputElem).val(null);
                                         });

                                toaster.error("Error", data.message);

                            }
                        }).error(function (data, status, headers, config) {
                            // file failed to upload
                            toaster.error("Error", "Some Error Occured !");
                        });
                    })(i);

                }


            },


            /* Show Modal For Changing User Login Password*/
            ShowChangePasswordModal: function () {
                $scope.ChangePasswordMessage = "";
                $scope.changePasswordForm.inputCurrentPassword.$dirty = false;
                $scope.changePasswordForm.inputNewPassword.$dirty = false;
                $scope.changePasswordForm.inputConfirmNewPassword.$dirty = false;
                $scope.changePasswordObject = {};
                $('#modalChangePassword').modal('show');
            },


            /* Change Password Function */
            ChangePassword: function (isCurrentPasswordInvalid, isNewPasswordInvalid, isConfirmNewPasswordInvalid) {

                if (isCurrentPasswordInvalid || isNewPasswordInvalid || isConfirmNewPasswordInvalid) {

                    $scope.changePasswordForm.inputCurrentPassword.$dirty = true;
                    $scope.changePasswordForm.inputNewPassword.$dirty = true;
                    $scope.changePasswordForm.inputConfirmNewPassword.$dirty = true;

                }

                else if ($scope.changePasswordObject.NewPassword != $scope.changePasswordObject.ConfirmNewPassword) {
                    $scope.ChangePasswordMessage = "New Passwords must match.";
                }
                else {
                    $scope.ChangePasswordMessage = "";
                    accountFactory.ChangePasswordFunction($scope.changePasswordObject).success(function (data) {

                        if (data.success) {
                            toaster.success("Success", data.message);

                            $('#modalChangePassword').modal('hide');
                        }
                        else {

                            toaster.error("Error", data.message[0]);

                        }

                    }).error(function () {

                        toaster.error("Error", "Some Error Occured");
                    });

                }

            }

        };





        /* CompanyProfile is a CLASS which has all the FUNCTIONS related to COMPANY PROFILE */
        $scope.CompanyProfile = {

            /* Showing Company profile to see the information*/
            showCompanyProfile: function () {
                closeCentreSections();
                $scope.showCenterLoader = true;
                $scope.selectedProject = 0;


                $scope.selectedTodo = 0;
                $scope.selectedNavigation = "CompanyProfile";



                /* Getting Company Details */
                companiesFactory.getCompanyDetails($scope.CompanyObject.companyId).success(function (data) {

                    if (data.success) {

                        $scope.CompanyObject = data.responseData[0];

                        /* Getting Company Teams with Users */
                        teamFactory.getCompanyTeams($scope.CompanyObject.companyId).success(function (data) {

                            if (data.success) {
                                $scope.CompanyTeamsObject = data.responseData;
                                $scope.showCenterLoader = false;
                                $('#divCompanyProfile').show();

                            }
                            else {

                                toaster.error("Error", data.message);

                            }



                        }).error(function () {

                            toaster.error("Error", "Some Error Occured");
                        });



                    }
                    else {

                        toaster.error("Error", "Some Error Occured");
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured");

                });




            },


            /* Opening Company Edit Modal pop up */
            editCompanyProfile: function () {

                $scope.CompanyEditObject = {};
                $scope.CompanyEditObject.companyId = $scope.CompanyObject.companyId;
                $scope.CompanyEditObject.companyName = $scope.CompanyObject.companyName;
                $scope.CompanyEditObject.companyEmail = $scope.CompanyObject.companyEmail;
                $scope.CompanyEditObject.companyWebsite = $scope.CompanyObject.companyWebsite;
                $scope.CompanyEditObject.companyPhoneNo = $scope.CompanyObject.companyPhoneNo;
                $scope.CompanyEditObject.companyFax = $scope.CompanyObject.companyFax;
                $scope.CompanyEditObject.companyAddress = $scope.CompanyObject.companyAddress;
                $scope.CompanyEditObject.companyDescription = $scope.CompanyObject.companyDescription;


                $('#modalEditCompanyProfile').modal('show');

            },


            /* Update Company Details */
            updateCompanyProfile: function () {

                companiesFactory.updateCompanyDetails($scope.CompanyEditObject).success(function (data) {

                    if (data.success) {
                        $scope.CompanyObject.companyName = $scope.CompanyEditObject.companyName;
                        $scope.CompanyObject.companyEmail = $scope.CompanyEditObject.companyEmail;
                        $scope.CompanyObject.companyWebsite = $scope.CompanyEditObject.companyWebsite;
                        $scope.CompanyObject.companyPhoneNo = $scope.CompanyEditObject.companyPhoneNo;
                        $scope.CompanyObject.companyFax = $scope.CompanyEditObject.companyFax;
                        $scope.CompanyObject.companyAddress = $scope.CompanyEditObject.companyAddress;
                        $scope.CompanyObject.companyDescription = $scope.CompanyEditObject.companyDescription;
                        toaster.success("Success", data.message);
                        $('#modalEditCompanyProfile').modal('hide');
                    }
                    else {

                        if (data.message == undefined) {

                            toaster.error("Error", data.errors.toString());
                        }
                        else {


                            toaster.error("Error", data.message);

                        }
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured");

                });

            },


            /* Updating Company Logo*/
            updateCompanyLogo: function ($files) {

                $scope.showCompanyLogoLoader = true;
                //$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {

                    var $file = $files[i];
                    (function (index) {
                        $scope.upload[index] = $upload.upload({
                            url: WebDomainPath + "/API/CompaniesAPI/UploadLogo", // webapi url
                            method: "POST",
                            data: { UploadedProfile: $file, Companyid: $scope.CompanyObject.companyId },
                            file: $file
                        }).progress(function (evt) {

                            // get upload percentage
                            $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);

                        }).success(function (data) {
                            // file is uploaded successfully

                            if (data.success) {
                                angular.forEach(
                                 angular.element("input[type='file']"),
                                         function (inputElem) {
                                             angular.element(inputElem).val(null);
                                         });
                                $timeout(function () {
                                    $scope.$apply();
                                    companiesFactory.getCompanyDetails($scope.CompanyObject.companyId).success(function (companydata) {
                                        if (companydata.success) {
                                            $scope.CompanyObject = companydata.responseData[0];
                                            $timeout(function () {
                                                $scope.$apply();
                                                $scope.showCompanyLogoLoader = false;
                                                toaster.success("Success", data.message);

                                            });
                                        }
                                        else {
                                            $scope.showCompanyLogoLoader = false;
                                            toaster.error("Error", "Some Error Occured");
                                        }

                                    }).error(function () {
                                        $scope.showCompanyLogoLoader = false;
                                        toaster.error("Error", "Some error occured");
                                    });

                                });
                            }
                            else {
                                angular.forEach(
                                 angular.element("input[type='file']"),
                                         function (inputElem) {
                                             angular.element(inputElem).val(null);
                                         });
                                $timeout(function () {
                                    $scope.$apply();
                                    $scope.showCompanyLogoLoader = false;
                                    toaster.error("Error", data.message);
                                });

                            }

                        }).error(function (data, status, headers, config) {
                            // file failed to upload
                            console.log(data);
                        });
                    })(i);
                }
            }


        };




        /* UserManagement is a CLASS which has all the FUNCTIONS related to Management of User i.e Addition of new users, Managing Existing Users, Changing Roles of Users and 
        Deletion of existing Users etc.
        */
        $scope.UserManagement = {

            /* Showing User Management Section to see the information*/
            showUserManagementSection: function () {
                closeCentreSections();
                $scope.showCenterLoader = true;
                $scope.UsersObject = {};
                $scope.inputSearchUser = "";
                $scope.selectedProject = 0;

                $scope.selectedTodo = 0;
                $scope.selectedNavigation = "UserManagementSection";



                /* Getting Users Details */

                usersFactory.getUsers().success(function (data) {

                    if (data.success) {

                        $scope.UsersObject = data.responseData;

                        $timeout(function () {

                            $scope.$apply();
                            $scope.showCenterLoader = false;
                            $('#divUserManagementSection').show();
                        });
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some error occured");
                });


            },


            /* SHowing Add New User Modal Pop Up*/
            showAddNewUserModal: function () {

                $scope.userModel = {};
                $scope.userModel.email = "";
                $scope.addNewUserForm.Email.$dirty = false;
                $scope.addNewUserForm.FirstName.$dirty = false;
                $scope.addNewUserForm.LastName.$dirty = false;
                $scope.addNewUserForm.UserRole.$dirty = false;
                $('#modalAddNewUser').modal('show');
            },


            /* Showing Edit User ROle Modal Pop Up*/
            showModalUserRoleEdit: function (userObject) {
                $scope.UserRoleEditObject = {};

                $scope.UserRoleEditObject.firstName = userObject.firstName;
                $scope.UserRoleEditObject.lastName = userObject.lastName;
                $scope.UserRoleEditObject.email = userObject.email;
                $scope.UserRoleEditObject.userId = userObject.userId;
                $scope.UserRoleEditObject.userRole = userObject.userRole;

                $('#modalUserRoleEdit').modal('show');

            },


            /* Updating User Role */
            updateUserRole: function () {

                usersFactory.updateUserRole($scope.UserRoleEditObject).success(function (data) {


                    if (data.success) {

                        toaster.success("Success", data.message);
                        $('#modalUserRoleEdit').modal('hide');
                    }
                    else {

                        toaster.error("Error", data.errors.toString());

                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });



            },


            /* Saving New User*/
            addNewUser: function (isEmailInvalid, isFirstNameInvalid, isLastNameInvalid, isUserRoleInvalid) {

                debugger;
                if (isEmailInvalid || isFirstNameInvalid || isLastNameInvalid || isUserRoleInvalid) {
                    $scope.addNewUserForm.Email.$dirty = true;
                    $scope.addNewUserForm.FirstName.$dirty = true;
                    $scope.addNewUserForm.LastName.$dirty = true;
                    $scope.addNewUserForm.UserRole.$dirty = true;
                }
                else {

                    $('#globalLoader').show();

                    usersFactory.insertUser($scope.userModel).success(function (data) {

                        if (data.success == false) {

                            toaster.error("Error", data.errors.toString());

                        }
                        else {

                            toaster.success("New User Added Successfully");
                            $('#modalAddNewUser').modal('hide');

                            /* Getting Users Details */
                            usersFactory.getUsers().success(function (data) {

                                if (data.success) {
                                    $scope.UsersObject = data.responseData;
                                    $('#globalLoader').hide();
                                }
                                else {

                                    toaster.error("Error", data.message);
                                    $('#globalLoader').hide();
                                }

                            }).error(function () {
                                $('#globalLoader').hide();
                                toaster.error("Error", "Some error occured");
                            });

                        }


                    }).error(function (data) {
                        $('#globalLoader').hide();
                        toaster.error("Some Problem Occured");

                    });
                }

            }


        };



        /* NotificationManagement is a CLASS which has all the FUNCTIONS related to Management of Notifications i.e Notification settings, reading notifications  etc.*/
        $scope.NotificationManagement = {

            /* Showing Notification Management Section to see the information*/
            showNotificationManagementSection: function () {
                closeCentreSections();
                $scope.showCenterLoader = true;
                $scope.selectedNotification = "";
                $scope.inputSearchNotification = "";
                $scope.selectedProject = 0;
                $scope.selectedNotification = "";
                $scope.selectedTodo = 0;
                $scope.selectedNavigation = "NotificationManagementSection";

                notificationFactory.getNotifications().success(function (data) {

                    if (data.success) {
                        $scope.NotificationsObject = data.responseData;
                        $scope.showCenterLoader = false;
                        $('#divNotificationManagementSection').show();
                    }

                    else {

                        toaster.error("Error", data.message);

                    }


                }).error(function () {


                    toaster.error("Error", "Some Error Occured !");

                });





            },

            /* Showing Notification Description Section to see full information  */
            showNotificationDescriptionSection: function (notificationObject) {

                $scope.selectedNotification = notificationObject.id;

                if (notificationObject.type == 1) {

                    closeRightSections();

                    $scope.stretchCentreWindow = false;
                    $scope.hideRightWindow = false;


                    teamFactory.getTeam(notificationObject.concernedSectionId).success(function (data) {

                        if (data.success) {
                            $scope.teamDescriptionObject = data.responseData;
                            $('#divTeamDescriptionSection').show();

                        }

                        else {
                            toaster.error("Error", data.message);

                        }

                    }).error(function () {

                        toaster.error("Error", "Some Error Occured !");

                    });





                }

                else if (notificationObject.type == 2) {
                    $scope.projectDescriptionObject = notificationObject;
                    closeRightSections();

                    $scope.stretchCentreWindow = false;
                    $scope.hideRightWindow = false;

                    $('#divProjectDescriptionSection').show();


                }

                else if (notificationObject.type == 6) {

                    closeRightSections();

                    $scope.stretchCentreWindow = false;
                    $scope.hideRightWindow = false;

                    CalendarEventsFactory.getSingleCalendarEvent(notificationObject.concernedSectionId).success(function (data) {

                        if (data.success) {
                            $scope.eventDescriptionObject = {};
                            $scope.eventDescriptionObject = data.responseData;


                            $('#divEventDescriptionSection').show();
                        }
                        else {
                            toaster.error("Error", data.message);

                        }



                    }).error(function () {


                        toaster.error("Error", "Some Error Occured!");

                    });





                }


            }



        };




        /* InboxManagement is a CLASS which has all the FUNCTIONS related to Management of Inbox i.e Inbox settings, reading Inbox messages  etc.*/
        $scope.InboxManagement = {

            /* Showing Inbox Management Section to see the information*/
            showInboxManagementSection: function () {
                closeCentreSections();
                $scope.showCenterLoader = true;
                $scope.UsersObject = {};
                $scope.inputSearchUser = "";
                $scope.selectedProject = 0;

                $scope.selectedTodo = 0;
                $scope.selectedNavigation = "InboxManagementSection";

                $('#divInboxManagementSection').show();


            }



        };





        /* TeamManagement is a CLASS which has all the FUNCTIONS related to Team i.e Team Creation, Deletion, adding
            Users to teams etc*/
        $scope.TeamManagement = {

            /* Showing Team Management Section to see the information*/
            showTeamManagementSection: function () {
                closeCentreSections();
                $scope.showCenterLoader = true;
                $scope.selectedProject = 0;
                $scope.inputSearchTeam = "";

                $scope.selectedTodo = 0;
                $scope.selectedNavigation = "TeamManagementSection";

                /* Getting Company Teams with Users */
                teamFactory.getCompanyTeams($scope.CompanyObject.companyId).success(function (data) {
                    if (data.success) {

                        $scope.CompanyTeamsObject = data.responseData;

                        /* Getting Users Details in which all users are there so that they can be added for adding new team */
                        usersFactory.getUsers().success(function (data) {
                            if (data.success) {

                                $scope.UsersObject = data.responseData;
                                $scope.showCenterLoader = false;
                                $('#divTeamManagementSection').show();
                            }
                            else {

                                toaster.error("Error", data.message);

                            }



                        }).error(function () {
                            toaster.error("Error", "Some error occured");
                        });



                    }
                    else {

                        toaster.error("Error", data.message);

                    }

                }).error(function () {
                    toaster.error("Error", "Some Error Occured");
                });




            },


            /* SHowing Add Update Team Modal Pop Up*/
            showTeamAddUpdateModal: function () {
                $scope.teamButton = false;
                $scope.teamAddUpdateModel = {};
                $scope.teamAddUpdateForm.teamName.$dirty = false;
                $scope.teamAddUpdateForm.teamDescription.$dirty = false;
                $('#modalTeamAddUpdate').modal('show');
            },


            /* Add new Team */
            addNewTeam: function (isTeamNameInvalid, isTeamDescriptionInvalid) {

                if (isTeamNameInvalid || isTeamDescriptionInvalid) {
                    $scope.teamAddUpdateForm.teamName.$dirty = true;
                    $scope.teamAddUpdateForm.teamDescription.$dirty = true;

                }
                else {

                    $('#globalLoader').show();
                    teamFactory.insertTeam($scope.teamAddUpdateModel).success(function (data) {

                        if (data.success) {


                            $timeout(function () {
                                $scope.$apply();

                                /* Getting Teams in which user is Team Member */
                                teamFactory.getLoggedInUserTeams().success(function (data) {
                                    if (data.success) {
                                        $scope.TeamsObject = data.responseData;
                                        $scope.activeTeam = $scope.TeamsObject[0];
                                    }
                                    else {

                                        toaster.error("Error", data.message);
                                    }

                                }).error(function () {
                                    toaster.error("Error", "Some Error Occured !");
                                });

                            });

                            $timeout(function () {
                                $scope.$apply();

                                /* Getting Company Teams with Users */
                                teamFactory.getCompanyTeams($scope.CompanyObject.companyId).success(function (data) {
                                    if (data.success) {

                                        $scope.CompanyTeamsObject = data.responseData;
                                    }
                                    else {
                                        toaster.error("Error", data.message);
                                    }

                                }).error(function () {
                                    toaster.error("Error", "Some Error Occured");
                                });
                            });


                            $timeout(function () {
                                $scope.$apply();
                                $('#modalTeamAddUpdate').modal('hide');
                                $('#globalLoader').hide();
                                toaster.success("Success", data.message);
                            });

                        }
                        else {

                            toaster.error("Error", data.message);

                        }


                    }).
                              error(function (error) {
                                  toaster.error("Error", "Some Error Occured!");
                              });
                }
            },


            /* Edit Team*/
            editTeam: function (teamObject) {

                $scope.teamButton = true;
                $scope.teamAddUpdateModel = {};
                $scope.teamAddUpdateModel.teamName = teamObject.teamName;
                $scope.teamAddUpdateModel.teamDescription = teamObject.teamDescription;
                $scope.teamAddUpdateModel.teamId = teamObject.teamId;

                var teamMembers = [];

                if (teamObject.teamMembers.length > 0) {
                    for (var i = 0; i < teamObject.teamMembers.length; i++) {
                        teamMembers.push(teamObject.teamMembers[i].memberUserId)
                    }
                }

                $scope.teamAddUpdateModel.teamMembers = teamMembers;

                $('#modalTeamAddUpdate').modal('show');


            },


            /* Update Team*/
            updateTeam: function (isTeamNameInvalid, isTeamDescriptionInvalid) {
                if (isTeamNameInvalid || isTeamDescriptionInvalid) {
                    $scope.teamAddUpdateForm.teamName.$dirty = true;
                    $scope.teamAddUpdateForm.teamDescription.$dirty = true;

                }
                else {
                    $('#globalLoader').show();

                    teamFactory.updateTeam($scope.teamAddUpdateModel).success(function (data) {

                        if (data.success) {


                            toaster.success("Success", data.message);
                            $('#modalTeamAddUpdate').modal('hide');
                            $('#globalLoader').hide();





                            /* Getting Teams in which user is Team Member */
                            teamFactory.getLoggedInUserTeams().success(function (data) {
                                if (data.success) {
                                    $scope.TeamsObject = data.responseData;
                                    $scope.activeTeam = $scope.TeamsObject[0];
                                }
                                else {

                                    toaster.error("Error", data.message);
                                }

                            }).error(function () {
                                toaster.error("Error", "Some Error Occured !");
                            });



                            /* Getting Company Teams with Users */
                            teamFactory.getCompanyTeams($scope.CompanyObject.companyId).success(function (data) {
                                if (data.success) {

                                    $scope.CompanyTeamsObject = data.responseData;
                                }
                                else {
                                    toaster.error("Error", data.message);
                                }

                            }).error(function () {
                                toaster.error("Error", "Some Error Occured");
                            });



                        }
                        else {

                            toaster.error("Error", data.message);
                        }

                    }).error(function () {

                        toaster.error("Error", "Some Error Occured !");

                    });
                }
            },


            /* Delete Team */
            deleteTeam: function (teamId, index) {

                $confirm({ text: 'Are you sure you want to delete this Team?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
                   .then(function () {

                       teamFactory.deleteTeam(teamId).success(function (data) {

                           if (data.success) {

                               toaster.success("Success", data.message);

                               $scope.CompanyTeamsObject.splice(index, 1);

                               /* Getting Teams in which user is Team Member */
                               teamFactory.getLoggedInUserTeams().success(function (data) {
                                   if (data.success) {
                                       $scope.TeamsObject = data.responseData;
                                       $scope.activeTeam = $scope.TeamsObject[0];
                                   }
                                   else {

                                       toaster.error("Error", data.message);
                                   }

                               }).error(function () {
                                   toaster.error("Error", "Some Error Occured !");
                               });

                           }
                           else {

                               toaster.error("Error", data.message);
                           }

                       }).error(function () {

                           toaster.error("Error", "Some Error Occured!");

                       });

                   })

            },


            /*  CHange Selected Team */
            changeSelectedTeam: function (activeTeamObject) {

                closeCentreSections();
                $('#divTeam').show();
                $('#globalLoader').show();
                $scope.activeTeam = activeTeamObject;
                $scope.teamName = activeTeamObject.teamName;
                $scope.Users = {};
                $scope.Users.teamId = activeTeamObject.teamId;
                $scope.Users.updateflag = 2;
                usersFactory.updateUser($scope.Users).success(function () {

                    getProjects();

                    $timeout(function () {
                        $scope.$apply();
                        $('#globalLoader').hide();
                    });
                });
            },

            /* Configuration for adding teammembers from Team Add Update Modal */
            teamMembersConfig: {
                create: false,
                // maxItems: 1,
                required: false,
                plugins: ['remove_button']
            }

        };


        /* ProjectManagement is a CLASS which has all the FUNCTIONS related to Project i.e Project Creation, Deletion, adding
           Members to Projects etc*/
        $scope.ProjectManagement = {


            /* Showing Project Management Section to see the information*/
            showProjectManagementSection: function () {
                closeCentreSections();
                $scope.showCenterLoader = true;
                $scope.selectedProject = 0;
                $scope.inputSearchProject = "";
                $scope.selectedTodo = 0;
                $scope.selectedNavigation = "ProjectManagementSection";


                /* Getting User Owned Projects */
                projectsFactory.getUserOwnedProjects($scope.activeTeam.teamId).success(function (data) {

                    if (data.success) {

                        $scope.UserOwnedProjectsObject = data.responseData;
                        $scope.showCenterLoader = false;
                        $('#divProjectManagementSection').show();
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured!");

                });


            },


            /* SHowing Add Update Project Modal Pop Up*/
            showProjectAddUpdateModal: function () {
                $scope.projectButton = false;
                $scope.projectAddUpdateModel = {};
                $scope.projectAddUpdateForm.projectName.$dirty = false;
                $scope.projectAddUpdateForm.projectDescription.$dirty = false;
                $('#modalProjectAddUpdate').modal('show');
            },



            /* Add new Project */
            addNewProject: function (isProjectNameInvalid, isProjectDescriptionInvalid) {

                if (isProjectNameInvalid || isProjectDescriptionInvalid) {
                    $scope.projectAddUpdateForm.projectName.$dirty = true;
                    $scope.projectAddUpdateForm.projectDescription.$dirty = true;

                }
                else {

                    $('#globalLoader').show();

                    $scope.projectAddUpdateModel.teamId = $scope.activeTeam.teamId;

                    projectsFactory.insertProject($scope.projectAddUpdateModel)
                              .success(function (data) {

                                  if (data.success) {

                                      /* Getting Projects of User for a particular team in which user is project member */
                                      getProjects();


                                      /* Getting User Owned Projects */
                                      projectsFactory.getUserOwnedProjects($scope.activeTeam.teamId).success(function (data) {

                                          if (data.success) {

                                              $scope.UserOwnedProjectsObject = data.responseData;
                                          }
                                          else {

                                              toaster.error("Error", data.message);
                                          }

                                      }).error(function () {

                                          toaster.error("Error", "Some Error Occured!");

                                      });


                                      $timeout(function () {
                                          $scope.$apply();
                                          $('#modalProjectAddUpdate').modal('hide');
                                          $('#globalLoader').hide();
                                          toaster.success("Project Saved Successfully");

                                      });
                                  }
                              }).
                              error(function (error) {
                                  $scope.status = 'Unable to insert user: ' + error.message;
                              });
                }
            },


            /* Edit Project*/
            editProject: function (projectObject) {

                $scope.projectButton = true;
                $scope.projectAddUpdateModel = {};
                $scope.projectAddUpdateModel.projectName = projectObject.projectName;
                $scope.projectAddUpdateModel.projectDescription = projectObject.projectDescription;
                $scope.projectAddUpdateModel.projectId = projectObject.projectId;

                var projectMembers = [];

                if (projectObject.projectMembers.length > 0) {

                    for (var i = 0; i < projectObject.projectMembers.length; i++) {

                        projectMembers.push(projectObject.projectMembers[i].memberUserId)

                    }

                }

                $scope.projectAddUpdateModel.projectMembers = projectMembers;

                $('#modalProjectAddUpdate').modal('show');


            },


            /* Update Project*/
            updateProject: function (isProjectNameInvalid, isProjectDescriptionInvalid) {

                if (isProjectNameInvalid || isProjectDescriptionInvalid) {
                    $scope.addUpdateProjectForm.projectName.$dirty = true;
                    $scope.addUpdateProjectForm.projectDescription.$dirty = true;

                }
                else {
                    $('#globalLoader').show();

                    projectsFactory.updateProject($scope.projectAddUpdateModel).success(function (data) {

                        getProjects();

                        /* Getting User Owned Projects */
                        projectsFactory.getUserOwnedProjects($scope.activeTeam.teamId).success(function (data) {

                            if (data.success) {

                                $scope.UserOwnedProjectsObject = data.responseData;
                            }
                            else {

                                toaster.error("Error", data.message);
                            }

                        }).error(function () {

                            toaster.error("Error", "Some Error Occured!");

                        });

                        $timeout(function () {
                            $scope.$apply();
                            $('#globalLoader').hide();
                            $('#modalProjectAddUpdate').modal('hide');

                            toaster.success("Project Updated Successfully");
                        });
                    });
                }
            },


            /* Delete Project */
            deleteProject: function (projectId, index) {
                $confirm({ text: 'Are you sure you want to delete this Project?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
              .then(function () {

                  projectsFactory.deleteProject(projectId).success(function (data) {

                      if (data.success) {
                          toaster.success("Success", data.message);

                          $scope.UserOwnedProjectsObject.splice(index, 1);


                          /* Getting Projects of User for a particular team in which user is project member */
                          getProjects();

                      }
                      else {
                          toaster.error("Error", data.message);
                      }


                  }).error(function () {

                      toaster.error("Error", "Some Error Occured !");

                  });
              });

            },


            /* Configuration for adding teammembers from Team Add Update Modal */
            projectMembersConfig: {
                create: false,
                // maxItems: 1,
                required: false,
                plugins: ['remove_button']
            }

        };



        /* TodoManagement is a CLASS which has all the FUNCTIONS related to Todo i.e Todo Creation, Deletion etc*/
        $scope.TodoManagement = {

            /* Showing Todo Management Section to see the information*/
            showTodoManagementSection: function (projectObject) {

                closeCentreSections();
                $scope.Sections = {};

                $scope.inProgressFilter = '';
                $scope.isDoneFilter = '';
                $scope.statusFilter = '';
                $scope.inputProjectFilter = '';

                $scope.projectMembersObject = projectObject.projectMembers;
                $scope.projectDescriptionObject = projectObject;
                $scope.editProjectUsers = projectObject.display;
                $scope.selectedWindow = "defaultTodosWindow";
                $scope.hideRightWindow = true;
                $scope.stretchCentreWindow = true;
                $scope.selectedTodoView = "list";
                $scope.selectedNavigation = '';
                $scope.selectedProject = projectObject;
                $scope.selectedTodo = '';
                $('#divTodoManagementSection').show();
                $scope.projectId = projectObject.projectId;
                $scope.projectName = projectObject.projectName;
                $scope.projectDescription = projectObject.projectDescription;

                $scope.todotypeflag = 0;




                /* It fetches the Todos with sections as an array in it */
                todosFactory.getTodosWithSections($scope.projectId).success(function (data) {

                    if (data.success) {
                        $scope.Sections = data.responseData;


                        $timeout(function () {
                            $scope.$apply();

                        });

                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });


            },


            /* Showing Todos In List View */
            showTodosInListView: function () {

                $scope.selectedTodo = "";
                $scope.stretchCentreWindow = true;
                $scope.hideRightWindow = true;
                $scope.selectedTodoView = "list";

            },


            /* Showing Todos In List View */
            showTodosInKanBanView: function () {

                $scope.selectedTodo = "";
                $scope.stretchCentreWindow = true;
                $scope.hideRightWindow = true;
                $scope.selectedTodoView = "kanban";

            },


            /* SHowing Add Update Todo Modal Pop Up*/
            showTodoAddUpdateModal: function (sectionId) {


                $scope.todoUpdateButton = false;
                $scope.addUpdateTodoForm.name.$dirty = false;
                $scope.addUpdateTodoForm.section.$dirty = false;

                $scope.todoAddUpdateModel = {};
                if (sectionId != null) {
                    $scope.todoAddUpdateModel.sectionId = sectionId;
                }
                $('#modalTodoAddUpdate').modal('show');

            },


            /* Add new Todo */
            addNewTodo: function (isTodoNameInvalid, isSectionInvalid) {


                if (isTodoNameInvalid || isSectionInvalid) {

                    $scope.addUpdateTodoForm.name.$dirty = true;
                    $scope.addUpdateTodoForm.section.$dirty = true;

                    return;
                }

                $scope.todoAddUpdateModel.projectId = $scope.projectId;

                todosFactory.insertTodo($scope.todoAddUpdateModel).success(function (data) {

                    if (data.success) {

                        $scope.selectedtodo = "list";

                        var IndexOfSection = (document.getElementById("ddlAddNewTodoSection").selectedIndex) - 1;

                        var AssigneeProfilePhoto;
                        if ($scope.todoAddUpdateModel.assigneeId == undefined) {

                            AssigneeProfilePhoto = "Uploads/Default/profile.png";

                        }
                        else {
                            var FilterProfilePhotoOfAssignee = $filter('filter')($scope.projectMembersObject, { 'memberUserId': $scope.todoAddUpdateModel.assigneeId }, true);

                            AssigneeProfilePhoto = FilterProfilePhotoOfAssignee[0].memberProfilePhoto;
                        }


                        $scope.todoAddUpdateModel.todoId = data.message;
                        $scope.todoAddUpdateModel.isDone = false;
                        $scope.todoAddUpdateModel.inProgress = false;
                        $scope.todoAddUpdateModel.assigneeProfilePhoto = AssigneeProfilePhoto;


                        $scope.Sections[IndexOfSection].todos.push($scope.todoAddUpdateModel);

                        $timeout(function () {
                            $scope.$apply();

                            toaster.success("Success", "Todo Added Successfully !");
                            $('#modalTodoAddUpdate').modal('hide');
                        });



                    }
                    else {
                        if (data.message == null) {
                            toaster.error("Error", data.errors.toString());
                        }
                        else {
                            toaster.error("Error", data.message);
                        }
                    }

                })
                                          .error(function () {


                                              toaster.error("Error", "Some Error Occured !")
                                          });
            },


            /* Opening Todo Modal For Editing Todo */
            editTodo: function (todoObject) {

                $scope.todoDescriptionObject = todoObject;

                $scope.parentSectionEditIndex = todoObject.parentIndex;
                $scope.todoEditIndex = todoObject.childIndex;
                $scope.filterProjectId = todoObject.projectId;

                $scope.todoUpdateButton = true;
                $scope.myTodoUpdate = false;

                $scope.todoAddUpdateModel = {};
                $scope.todoAddUpdateModel.todoId = todoObject.todoId;
                $scope.todoAddUpdateModel.name = todoObject.name;
                $scope.todoAddUpdateModel.deadlineDate = todoObject.deadlineDate;
                $scope.todoAddUpdateModel.description = todoObject.description;
                $scope.todoAddUpdateModel.sectionId = todoObject.sectionId;
                $scope.todoAddUpdateModel.assigneeId = todoObject.assigneeId;
                $scope.todoAddUpdateModel.inProgress = todoObject.inProgress;
                $scope.todoAddUpdateModel.isDone = todoObject.isDone;

                $scope.previousAssigneeId = todoObject.assigneeId;

                $('#modalTodoAddUpdate').modal('show');


            },


            /* Update Todo*/
            updateTodo: function (isTodoNameInvalid, isSectionInvalid) {

                if (isTodoNameInvalid || isSectionInvalid) {

                    $scope.addUpdateTodoForm.name.$dirty = true;
                    $scope.addUpdateTodoForm.section.$dirty = true;

                    return;
                }

                todosFactory.updateTodo($scope.todoAddUpdateModel).success(function (data) {


                    if (data.success) {



                        /* Updating Current Todo Row through angular */
                        $scope.todoDescriptionObject.name = $scope.todoAddUpdateModel.name;
                        $scope.todoDescriptionObject.sectionId = $scope.todoAddUpdateModel.sectionId;
                        $scope.todoDescriptionObject.description = $scope.todoAddUpdateModel.description;
                        $scope.todoDescriptionObject.deadlineDate = $scope.todoAddUpdateModel.deadlineDate;
                        $scope.todoDescriptionObject.assigneeId = $scope.todoAddUpdateModel.assigneeId;
                        //$scope.Sections[$scope.parentSectionEditIndex].todos[$scope.todoEditIndex].name = $scope.todoAddUpdateModel.name;
                        //$scope.Sections[$scope.parentSectionEditIndex].todos[$scope.todoEditIndex].sectionId = $scope.todoAddUpdateModel.sectionId;
                        //$scope.Sections[$scope.parentSectionEditIndex].todos[$scope.todoEditIndex].description = $scope.todoAddUpdateModel.description;
                        //$scope.Sections[$scope.parentSectionEditIndex].todos[$scope.todoEditIndex].deadlineDate = $scope.todoAddUpdateModel.deadlineDate;
                        //$scope.Sections[$scope.parentSectionEditIndex].todos[$scope.todoEditIndex].assigneeId = $scope.todoAddUpdateModel.assigneeId;


                        var currentAssigneeId = $scope.todoAddUpdateModel.assigneeId;
                        var previousAssigneeId = $scope.previousAssigneeId;
                        if (currentAssigneeId != previousAssigneeId) {

                            var AssigneeProfilePhoto;
                            if ($scope.todoAddUpdateModel.assigneeId == undefined) {

                                AssigneeProfilePhoto = "Uploads/Default/profile.png";

                            }
                            else {
                                var FilterProfilePhotoOfAssignee = $filter('filter')($scope.projectMembersObject, { 'memberUserId': $scope.todoAddUpdateModel.assigneeId });

                                AssigneeProfilePhoto = FilterProfilePhotoOfAssignee[0].memberProfilePhoto;
                            }
                            $scope.todoDescriptionObject.assigneeProfilePhoto = AssigneeProfilePhoto;
                        }


                        $timeout(function () {
                            $scope.$apply();

                            toaster.success("Success", data.message);
                            $('#modalTodoAddUpdate').modal('hide');
                        });


                    }
                    else {

                        if (data.message == null) {
                            toaster.error("Error", data.errors.toString());

                        }
                        else {

                            toaster.error("Error", data.message);
                        }
                    }


                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });
            },


            /* Delete Todo */
            deleteTodo: function (todoId, sectionIndex, todoIndex) {

                $confirm({ text: 'Are you sure you want to delete?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
                    .then(function () {


                        todosFactory.deleteTodo(todoId).success(function (data) {

                            if (data.success) {

                                $scope.Sections[sectionIndex].todos.splice(todoIndex, 1);
                                toaster.success("Success", data.message);


                            }
                            else {

                                toaster.error("Error", data.message);
                            }

                        }).error(function () {

                            toaster.error("Error", "Some Error Occured !");

                        });
                    });



            },


            /* Showing Todo Description Window */
            showTodoDescriptionSection: function (todoObject) {
                debugger;
                $scope.toDoId = todoObject.todoId;
                $scope.myTodoUpdate = false;
                $scope.selectedTodo = todoObject.todoId;
                $scope.hideRightWindow = false;
                $scope.stretchCentreWindow = false;
                $scope.todoDescriptionObject = todoObject;

                closeRightSections();
                $('#divToDoDescriptionSection').show();


                /* Getting Attachments Corresponding To ToDo/Task*/
                attachmentsFactory.getToDoAttachments(todoObject.todoId).success(function (data) {

                    if (data.success) {

                        $scope.attachmentsObject = data.responseData;

                        $scope.AllAttachmentsForComment = data.responseData;

                        $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/computer.png' });
                        $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/logo-drive.png' });
                        $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/drop-box.png' });
                        $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/box-icon.png' });


                    }
                    else {
                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });

                /* Get Followers for the Todo*/
                followersFactory.getFollowers(todoObject.todoId).success(function (data) {

                    if (data.success) {
                        $scope.followersObject = data.responseData;
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });


                /*Getting Todo Comments*/
                getTodoCommentsInArrangedMode(todoObject.todoId);



                /* Getting Todo Notification or We can Say History  */
                todoNotificationsFactory.getTodoNotifications(todoObject.todoId).success(function (data) {

                    if (data.success) {

                        $scope.todoNotificationObject = data.responseData;

                    }
                    else {

                        toaster.error("Error", data.message);

                    }



                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");


                });



            },


            /* Filtering Todos with Status for eg: Completed Todos, InProgress Todos, IsDone Todos */
            filterTodosWithStatus: function (filterValue) {

                if (filterValue == "Completed") {
                    $scope.inProgressFilter = false;
                    $scope.isDoneFilter = true;

                }
                else if (filterValue == "Pending") {
                    $scope.inProgressFilter = false;
                    $scope.isDoneFilter = false;
                }
                else if (filterValue == "InProgress") {
                    $scope.inProgressFilter = true;
                    $scope.isDoneFilter = false;

                }
                else {

                    $scope.inProgressFilter = '';
                    $scope.isDoneFilter = '';
                }

            },

            /* Filtering Todos with Role for eg: As Assignee, as Reporter etc */
            filterTodosWithRole: function (filterValue) {

                if (filterValue == "Assignee") {
                    $scope.assigneeFilter = $scope.MyProfileObject.userId;
                    $scope.reporterFilter = "";
                }
                else if (filterValue == "Reporter") {
                    $scope.assigneeFilter = "";
                    $scope.reporterFilter = $scope.MyProfileObject.userId;
                }

                else if (filterValue == "AssigneeReporter") {
                    $scope.assigneeFilter = $scope.MyProfileObject.userId;
                    $scope.reporterFilter = $scope.MyProfileObject.userId;
                }
                else {
                    $scope.assigneeFilter = "";
                    $scope.reporterFilter = "";
                }

            },


            /* Show Project Members Modal Popup */
            showProjectMembersModal: function () {

                $('#modalProjectMembers').modal('show');

            },


            /* Start Progress Of Todo */
            startProgress: function (todoObject) {

                $scope.todoAddUpdateModel = {};
                $scope.todoAddUpdateModel.todoId = todoObject.todoId;
                $scope.todoAddUpdateModel.name = todoObject.name;
                $scope.todoAddUpdateModel.deadlineDate = todoObject.deadlineDate;
                $scope.todoAddUpdateModel.description = todoObject.description;
                $scope.todoAddUpdateModel.sectionId = todoObject.sectionId;
                $scope.todoAddUpdateModel.assigneeId = todoObject.assigneeId;
                $scope.todoAddUpdateModel.inProgress = true;
                $scope.todoAddUpdateModel.isDone = todoObject.isDone;

                todosFactory.updateTodo($scope.todoAddUpdateModel).success(function (data) {

                    if (data.success) {
                        $scope.todoDescriptionObject.inProgress = true;
                        $scope.todoDescriptionObject.modifiedDate = new Date();
                        toaster.success("Success", "Todo is in Progress ");
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });
            },


            /* Stop Progress Of Todo */
            stopProgress: function (todoObject) {

                $scope.todoAddUpdateModel = {};
                $scope.todoAddUpdateModel.todoId = todoObject.todoId;
                $scope.todoAddUpdateModel.name = todoObject.name;
                $scope.todoAddUpdateModel.deadlineDate = todoObject.deadlineDate;
                $scope.todoAddUpdateModel.description = todoObject.description;
                $scope.todoAddUpdateModel.sectionId = todoObject.sectionId;
                $scope.todoAddUpdateModel.assigneeId = todoObject.assigneeId;
                $scope.todoAddUpdateModel.inProgress = false;
                $scope.todoAddUpdateModel.isDone = todoObject.isDone;

                todosFactory.updateTodo($scope.todoAddUpdateModel).success(function (data) {

                    if (data.success) {

                        $scope.todoDescriptionObject.inProgress = false;
                        $scope.todoDescriptionObject.modifiedDate = new Date();
                        toaster.success("Success", "Progress is stopped ");
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });
            },


            /* Mark Todo As Done */
            isDone: function (todoObject) {

                $scope.todoAddUpdateModel = {};
                $scope.todoAddUpdateModel.todoId = todoObject.todoId;
                $scope.todoAddUpdateModel.name = todoObject.name;
                $scope.todoAddUpdateModel.deadlineDate = todoObject.deadlineDate;
                $scope.todoAddUpdateModel.description = todoObject.description;
                $scope.todoAddUpdateModel.sectionId = todoObject.sectionId;
                $scope.todoAddUpdateModel.assigneeId = todoObject.assigneeId;
                $scope.todoAddUpdateModel.inProgress = false;
                $scope.todoAddUpdateModel.isDone = true;

                todosFactory.updateTodo($scope.todoAddUpdateModel).success(function (data) {

                    if (data.success) {

                        $scope.todoDescriptionObject.isDone = true;
                        $scope.todoDescriptionObject.inProgress = false;
                        $scope.todoDescriptionObject.modifiedDate = new Date();
                        toaster.success("Success", "Todo is Complete !");
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });
            },


            /* Reopen Todo And Start Progress */
            reOpenTodoAndStartProgress: function (todoObject) {

                $scope.todoAddUpdateModel = {};
                $scope.todoAddUpdateModel.todoId = todoObject.todoId;
                $scope.todoAddUpdateModel.name = todoObject.name;
                $scope.todoAddUpdateModel.deadlineDate = todoObject.deadlineDate;
                $scope.todoAddUpdateModel.description = todoObject.description;
                $scope.todoAddUpdateModel.sectionId = todoObject.sectionId;
                $scope.todoAddUpdateModel.assigneeId = todoObject.assigneeId;
                $scope.todoAddUpdateModel.inProgress = true;
                $scope.todoAddUpdateModel.isDone = false;

                todosFactory.updateTodo($scope.todoAddUpdateModel).success(function (data) {

                    if (data.success) {

                        $scope.todoDescriptionObject.isDone = false;
                        $scope.todoDescriptionObject.inProgress = true;
                        $scope.todoDescriptionObject.modifiedDate = new Date();
                        toaster.success("Success", "Todo is in Progress");
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });
            },


            /* Showing Date calendar for Deadline Date While Saving New TOdo*/
            showCalendar: function ($event) {

                $event.preventDefault();
                $event.stopPropagation();
                $scope.deadlineCalendarOpened = true;
            },


            /* Showing MyTodosManagementSection*/
            showMyTodosManagementSection: function () {
                closeCentreSections();
                $scope.assigneeFilter = "";
                $scope.reporterFilter = "";
                $scope.statusFilter = "";
                $scope.roleFilter = "";
                $scope.inProgressFilter = "";
                $scope.isDoneFilter = "";
                $scope.Sections = {};
                $scope.selectedProjectInMyTodosSection = {};
                $scope.selectedTeamInMyTodosSection = {};
                $scope.selectedNavigation = "MyTodos";

                $('#divMyTodosManagementSection').show();


                /* It gets MyTodosWithSections */
                todosFactory.getMyTodosWithSections().success(function (data) {
                    if (data.success) {

                        $scope.MyTodosObject = data.responseData;
                    }
                    else {

                        toaster.error("Error", data.message);
                    }


                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });

                /* It Gets My Projects With Teams*/
                projectsFactory.getMyProjectsWithTeams().success(function (data) {
                    if (data.success) {

                        $scope.MyTeamsAndProjectsObject = data.responseData;
                    }
                    else {

                        toaster.error("Error", data.message);
                    }


                }).error(function () {


                    toaster.error("Error", "Some Error Occured !");
                });


            },


            /* Edit Todo From MyTodosManagement Section*/
            editMyTodo: function (todoObject) {

                $scope.todoDescriptionObject = todoObject;
                //$scope.todoEditIndex = todoObject.todoIndex;
                $scope.filterProjectId = todoObject.projectId;
                $scope.Sections = {};

                $scope.todoUpdateButton = true;
                $scope.myTodoUpdate = true;

                $scope.todoAddUpdateModel = {};
                $scope.todoAddUpdateModel.todoId = todoObject.todoId;
                $scope.todoAddUpdateModel.name = todoObject.name;
                $scope.todoAddUpdateModel.deadlineDate = todoObject.deadlineDate;
                $scope.todoAddUpdateModel.description = todoObject.description;
                $scope.todoAddUpdateModel.sectionId = todoObject.sectionId;
                $scope.todoAddUpdateModel.assigneeId = todoObject.assigneeId;
                $scope.todoAddUpdateModel.inProgress = todoObject.inProgress;
                $scope.todoAddUpdateModel.isDone = todoObject.isDone;

                $scope.previousAssigneeId = todoObject.assigneeId;




                /* It fetches Sections for a particular Project */
                sectionsFactory.getSections(todoObject.projectId).success(function (data) {

                    if (data.success) {

                        $scope.Sections = data.responseData;


                        $timeout(function () {
                            $scope.$apply();

                        });

                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });


                /* It fetches Project Members */
                projectUsersFactory.getProjectMembers(todoObject.projectId).success(function (data) {

                    if (data.success) {

                        $scope.projectMembersObject = data.responseData;

                        $timeout(function () {
                            $scope.$apply();
                            $('#modalTodoAddUpdate').modal('show');
                        });

                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });

            },


            /* Update Todo From MyTodosManagement*/
            updateMyTodo: function (isTodoNameInvalid, isSectionInvalid) {

                if (isTodoNameInvalid || isSectionInvalid) {

                    $scope.addUpdateTodoForm.name.$dirty = true;
                    $scope.addUpdateTodoForm.section.$dirty = true;

                    return;
                }

                todosFactory.updateTodo($scope.todoAddUpdateModel).success(function (data) {


                    if (data.success) {



                        /* Updating Current Todo Row through angular */
                        $scope.todoDescriptionObject.name = $scope.todoAddUpdateModel.name;
                        $scope.todoDescriptionObject.sectionId = $scope.todoAddUpdateModel.sectionId;
                        $scope.todoDescriptionObject.description = $scope.todoAddUpdateModel.description;
                        $scope.todoDescriptionObject.deadlineDate = $scope.todoAddUpdateModel.deadlineDate;
                        $scope.todoDescriptionObject.assigneeId = $scope.todoAddUpdateModel.assigneeId;


                        /* Updating Calendar Title */
                        $scope.calendarTodoDescription.title = $scope.todoAddUpdateModel.name;

                        //$scope.MyTodosObject[$scope.todoEditIndex].name = $scope.todoAddUpdateModel.name;
                        //$scope.MyTodosObject[$scope.todoEditIndex].sectionId = $scope.todoAddUpdateModel.sectionId;
                        //$scope.MyTodosObject[$scope.todoEditIndex].description = $scope.todoAddUpdateModel.description;
                        //$scope.MyTodosObject[$scope.todoEditIndex].deadlineDate = $scope.todoAddUpdateModel.deadlineDate;
                        //$scope.MyTodosObject[$scope.todoEditIndex].assigneeId = $scope.todoAddUpdateModel.assigneeId;


                        var currentAssigneeId = $scope.todoAddUpdateModel.assigneeId;
                        var previousAssigneeId = $scope.previousAssigneeId;
                        if (currentAssigneeId != previousAssigneeId) {

                            var AssigneeProfilePhoto;
                            if ($scope.todoAddUpdateModel.assigneeId == undefined) {

                                AssigneeProfilePhoto = "Uploads/Default/profile.png";

                            }
                            else {
                                var FilterProfilePhotoOfAssignee = $filter('filter')($scope.projectMembersObject, { 'memberUserId': $scope.todoAddUpdateModel.assigneeId });

                                AssigneeProfilePhoto = FilterProfilePhotoOfAssignee[0].memberProfilePhoto;
                            }
                            $scope.todoDescriptionObject.assigneeProfilePhoto = AssigneeProfilePhoto;
                        }


                        $timeout(function () {
                            $scope.$apply();

                            toaster.success("Success", data.message);
                            $('#modalTodoAddUpdate').modal('hide');
                        });


                    }
                    else {

                        if (data.message == null) {
                            toaster.error("Error", data.errors.toString());

                        }
                        else {

                            toaster.error("Error", data.message);
                        }
                    }


                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });
            },


            /* Showing Todo Description Window */
            showMyTodoDescriptionSection: function (todoObject) {

                $scope.myTodoUpdate = true;
                $scope.selectedTodo = todoObject.todoId;
                $scope.hideRightWindow = false;
                $scope.stretchCentreWindow = false;
                $scope.todoDescriptionObject = todoObject;

                closeRightSections();
                $('#divToDoDescriptionSection').show();


                /* Getting Attachments Corresponding To ToDo/Task*/
                attachmentsFactory.getToDoAttachments(todoObject.todoId).success(function (data) {

                    if (data.success) {

                        $scope.attachmentsObject = data.responseData;

                        $scope.AllAttachmentsForComment = data.responseData;

                        $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/computer.png' });
                        $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/logo-drive.png' });
                        $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/drop-box.png' });
                        $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/box-icon.png' });


                    }
                    else {
                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });

                /* Get Followers for the Todo*/
                followersFactory.getFollowers(todoObject.todoId).success(function (data) {

                    if (data.success) {
                        $scope.followersObject = data.responseData;
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });


                /*Getting Todo Comments*/
                getTodoCommentsInArrangedMode(todoObject.todoId);


                /* Getting Project Members */
                projectUsersFactory.getProjectMembers(todoObject.projectId).success(function (data) {
                    debugger;
                    if (data.success) {

                        $scope.projectMembersObject = data.responseData;

                        $timeout(function () {
                            $scope.$apply();
                        });

                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });

            },


            /* Change Team Filter */
            changeTeamFilter: function () {

                $scope.selectedProjectInMyTodosSection = {};

            },


            /* Configuration for adding assignee for the Todo */
            todoAssigneeConfig: {
                create: false,
                maxItems: 1,
                required: true,
                delimiter: '|'

            }

        };





        /* SectionManagement is a CLASS which has all the FUNCTIONS related to Section i.e Section Creation, Deletion etc*/
        $scope.SectionManagement = {

            /* Showing Section Management Modal Pop Up */
            showSectionManagementModal: function () {

                $('#modalSectionManagement').modal('show');


            },

            /* SHow Section Add Update Modal Pop Up */
            showAddUpdateSectionModal: function () {
                $scope.showUpdateSectionButton = false;
                $scope.sectionAddUpdateModel = {};
                $('#modalAddUpdateSection').modal('show');
            },


            /* Adding New Section */
            addNewSection: function () {

                $scope.sectionAddUpdateModel.projectId = $scope.projectId;
                sectionsFactory.insertSection($scope.sectionAddUpdateModel).success(function (data) {

                    if (data.success) {

                        /* data.message is the unique id of newly created section */
                        $scope.sectionAddUpdateModel.id = data.message;
                        $scope.sectionAddUpdateModel.todos = [];
                        $scope.sectionAddUpdateModel.sectionCreator = $scope.MyProfileObject.profilePhoto;
                        $scope.sectionAddUpdateModel.text = $scope.sectionAddUpdateModel.sectionName;
                        $scope.sectionAddUpdateModel.value = data.message;
                        $scope.Sections.push($scope.sectionAddUpdateModel);

                        toaster.success("Success", "Section Added Successfully !");
                        $('#modalAddUpdateSection').modal('hide');
                    }
                    else {
                        if (data.message == null) {

                            toaster.error("Error", data.errors.toString());
                        }
                        else {

                            toaster.error("Error", data.message)
                        }

                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !")
                });


            },


            /* Edit Section */
            editSection: function (sectionObject, index) {

                $scope.sectionEditIndex = index;
                $scope.showUpdateSectionButton = true;
                $scope.sectionAddUpdateModel = {};
                $scope.sectionAddUpdateModel.sectionId = sectionObject.id;
                $scope.sectionAddUpdateModel.sectionName = sectionObject.sectionName;
                $('#modalAddUpdateSection').modal('show');

            },


            /* Update Section*/
            updateSection: function () {

                sectionsFactory.updateSection($scope.sectionAddUpdateModel).success(function (data) {

                    if (data.success) {
                        toaster.success("Success", data.message);

                        $scope.Sections[$scope.sectionEditIndex].sectionName = $scope.sectionAddUpdateModel.sectionName;
                        $scope.Sections[$scope.sectionEditIndex].text = $scope.sectionAddUpdateModel.sectionName;
                        $('#modalAddUpdateSection').modal('hide');
                    }

                    else {
                        if (data.message == null) {

                            toaster.error("Error", data.errors.toString());
                        }
                        else {

                            toaster.error("Error", data.message);
                        }

                    }


                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });


            },


            /* Delete Section */
            deleteSection: function (sectionId, index) {

                $confirm({ text: 'Are you sure you want to delete this Section?', title: 'Delete Section', ok: 'Yes', cancel: 'No' })
                 .then(function () {


                     sectionsFactory.deleteSection(sectionId).success(function (data) {

                         if (data.success) {

                             $scope.Sections.splice(index, 1);

                             toaster.success("Success", data.message);


                         }

                         else {

                             toaster.error("Error", data.message);
                         }

                     }).error(function () {

                         toaster.error("Error", "Some Error Occured !");

                     });

                 });



            }


        };





        /* AttachmentManagement is a CLASS which has all the FUNCTIONS related to Attachment i.e Add or Delete or Fetch Attachments etc*/
        $scope.AttachmentManagement = {

            /* Showing Attach Modal Pop Up */
            showAttachFileModal: function () {

                $('#modalAttachFile').modal('show');

            },


            /* Show all attachments for the Project */
            showProjectAttachments: function (projectObject) {


                $scope.selectedTodoView = "attachment";
                $scope.stretchCentreWindow = true;
                $scope.hideRightWindow = true;

                /* Fetching Attachments of a particular Project */
                attachmentsFactory.getProjectAttachments(projectObject.projectId).success(function (data) {

                    if (data.success) {

                        $scope.projectAttachmentsObject = data.responseData;
                    }
                    else {

                        toaster.error("Error", data.message);
                    }




                }).error(function () { toaster.error("Error", "Some Error Occured !") });

            },


            /* Attach file from Local for example Computer */
            attachFileFromLocal: function ($files) {
                $('#globalLoader').show();

                $scope.progressperc = true;
                var j = ($files.length) - 1;

                //$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {


                    var $file = $files[i];

                    if (i == j) {
                        (function (index) {

                            $scope.upload[index] = $upload.upload({
                                url: WebDomainPath + "/API/files/upload", // webapi url
                                method: "POST",
                                data: { UploadedAttachment: $file, ToDoId: $scope.toDoId },
                                file: $file
                            }).progress(function (evt) {

                                // get upload percentage
                                console.log(parseInt(100.0 * evt.loaded / evt.total));
                                $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);

                            }).success(function (data, status, headers, config) {
                                // file is uploaded successfully

                                $timeout(function () {
                                    $scope.$apply();

                                    /* Empty Input After Success*/
                                    angular.forEach(
                                     angular.element("input[type='file']"),
                                             function (inputElem) {
                                                 angular.element(inputElem).val(null);
                                             });

                                    /* Getting Attachments Corresponding To ToDo/Task*/
                                    attachmentsFactory.getToDoAttachments($scope.todoDescriptionObject.todoId).success(function (data) {

                                        if (data.success) {

                                            $scope.attachmentsObject = data.responseData;

                                            $scope.AllAttachmentsForComment = data.responseData;

                                            $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/computer.png' });
                                            $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/logo-drive.png' });
                                            $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/drop-box.png' });
                                            $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/box-icon.png' });
                                            $('#globalLoader').hide();

                                        }
                                        else {
                                            toaster.error("Error", data.message);
                                            $('#globalLoader').hide();
                                        }

                                    }).error(function () {

                                        toaster.error("Error", "Some Error Occured !");
                                        $('#globalLoader').hide();

                                    });




                                });
                                $timeout(function () {
                                    $scope.$apply();
                                    toaster.success("Computer", "Attachments uploaded Successfully");

                                });


                            }).error(function (data, status, headers, config) {
                                // file failed to upload
                                console.log(data);
                                toaster.error("Error", "Some Error Occured !");
                                $('#globalLoader').hide();
                            });
                        })(i);
                    }
                    else {
                        (function (index) {

                            $scope.upload[index] = $upload.upload({
                                url: WebDomainPath + "/API/files/upload", // webapi url
                                method: "POST",
                                data: { UploadedAttachment: $file, ToDoId: $scope.toDoId },
                                file: $file
                            }).progress(function (evt) {

                                // get upload percentage
                                console.log(parseInt(100.0 * evt.loaded / evt.total));
                                $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);

                            }).success(function (data, status, headers, config) {
                                // file is uploaded successfully


                            }).error(function (data, status, headers, config) {
                                // file failed to upload 
                                console.log(data);
                            });
                        })(i);

                    }
                }

            },


            /*  Deleting Attachment from Todo */
            deleteAttachment: function (attachmentId, index) {

                $confirm({ text: 'Are you sure you want to delete this File?', title: 'Delete it', ok: 'Yes', cancel: 'No' })
               .then(function () {

                   $('#globalLoader').show();

                   /* Deleting attachment */
                   attachmentsFactory.deleteTodoAttachment(attachmentId).success(function (data) {

                       if (data.success) {
                           $('#globalLoader').hide();
                           $scope.attachmentsObject.splice(index, 1);
                           toaster.success("Success", data.message);
                       }

                       else {
                           $('#globalLoader').hide();
                           toaster.error("Error", data.message);
                       }


                   }).error(function () {
                       $('#globalLoader').hide();
                       toaster.error("Error", "Some Error Occured!")

                   });



               });


            }

        };




        /* Saving DropBox Files */
        DropBoxSettings.dropboxsuccess = function (files) {
            $('#globalLoader').show();

            attachmentsFactory.saveDropBoxFiles($scope.todoDescriptionObject.todoId, files).success(function (data) {

                if (data.success) {

                    toaster.success("Success", data.message);

                    /* Getting Attachments Corresponding To ToDo/Task*/
                    attachmentsFactory.getToDoAttachments($scope.todoDescriptionObject.todoId).success(function (data) {

                        if (data.success) {

                            $scope.attachmentsObject = data.responseData;

                            $scope.AllAttachmentsForComment = data.responseData;

                            $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/computer.png' });
                            $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/logo-drive.png' });
                            $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/drop-box.png' });
                            $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/box-icon.png' });

                            $('#globalLoader').hide();
                        }
                        else {
                            toaster.error("Error", data.message);
                            $('#globalLoader').hide();
                        }

                    }).error(function () {

                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();

                    });

                }
                else {

                    toaster.error("Error", data.message);
                    $('#globalLoader').hide();
                }


            }).error(function () {

                toaster.error("Error", "Some Error Occured !");
                $('#globalLoader').hide();

            });


        };


        /* Saving Box Files */
        DropBoxSettings.boxsuccess = function (files) {
            $('#globalLoader').show();
            attachmentsFactory.saveBoxFiles($scope.todoDescriptionObject.todoId, files).success(function (data) {

                if (data.success) {

                    toaster.success("Success", data.message);

                    /* Getting Attachments Corresponding To ToDo/Task*/
                    attachmentsFactory.getToDoAttachments($scope.todoDescriptionObject.todoId).success(function (data) {

                        if (data.success) {

                            $scope.attachmentsObject = data.responseData;

                            $scope.AllAttachmentsForComment = data.responseData;

                            $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/computer.png' });
                            $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/logo-drive.png' });
                            $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/drop-box.png' });
                            $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/box-icon.png' });
                            $('#globalLoader').hide();

                        }
                        else {
                            toaster.error("Error", data.message);
                            $('#globalLoader').hide();
                        }

                    }).error(function () {

                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();
                    });

                }
                else {

                    toaster.error("Error", data.message);
                    $('#globalLoader').hide();
                }


            }).error(function () {

                toaster.error("Error", "Some Error Occured !");
                $('#globalLoader').hide();

            });

        };


        /* Saving Google Drive Files */
        lkGoogleSettings.googledrivesuccess = function (data) {

            $('#modalAttachFile').modal('hide');

            if (data.docs.length > 0) {
                $('#globalLoader').show();
                attachmentsFactory.saveGoogleDriveFiles($scope.todoDescriptionObject.todoId, data.docs).success(function (data) {

                    if (data.success) {

                        toaster.success("Success", data.message);

                        /* Getting Attachments Corresponding To ToDo/Task*/
                        attachmentsFactory.getToDoAttachments($scope.todoDescriptionObject.todoId).success(function (data) {

                            if (data.success) {

                                $scope.attachmentsObject = data.responseData;

                                $scope.AllAttachmentsForComment = data.responseData;

                                $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/computer.png' });
                                $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/logo-drive.png' });
                                $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/drop-box.png' });
                                $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/box-icon.png' });
                                $('#globalLoader').hide();

                            }
                            else {
                                toaster.error("Error", data.message);
                                $('#globalLoader').hide();
                            }

                        }).error(function () {

                            toaster.error("Error", "Some Error Occured !");
                            $('#globalLoader').hide();

                        });

                    }
                    else {

                        toaster.error("Error", data.message);
                        $('#globalLoader').hide();
                    }


                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                    $('#globalLoader').hide();

                });

            }

        }




        /* FollowerManagement is a CLASS which has all the FUNCTIONS related to Followers i.e Follower Creation, Deletion etc*/
        $scope.FollowerManagement = {

            /* Showing Follower Modal For editing */
            showFollowerEditModal: function (followersObject) {

                $('#modalFollowerEdit').modal('show');

                var currentFollowers = [];

                if (followersObject.length > 0) {
                    for (var i = 0; i < followersObject.length; i++) {
                        currentFollowers.push(followersObject[i].value)
                    }
                }
                $scope.followerAddUpdateModel = {};
                $scope.followerAddUpdateModel.followers = currentFollowers;
                $scope.followerAddUpdateModel.todoId = $scope.todoDescriptionObject.todoId;
            },


            /* Update Followers For a particular Todo */
            updateFollower: function () {

                followersFactory.updateFollowers($scope.followerAddUpdateModel).success(function (dataUpdate) {

                    if (dataUpdate.success) {


                        /* Get Updated Followers for the Todo*/
                        followersFactory.getFollowers($scope.todoDescriptionObject.todoId).success(function (data) {

                            if (data.success) {
                                $scope.followersObject = data.responseData;

                                $timeout(function () {
                                    $scope.$apply();
                                    toaster.success("Success", dataUpdate.message);
                                    $('#modalFollowerEdit').modal('hide');
                                });

                            }
                            else {

                                toaster.error("Error", data.message);
                            }

                        }).error(function () {

                            toaster.error("Error", "Some Error Occured !");

                        });



                    }
                    else {

                        toaster.error("Error", dataUpdate.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });

            },


            /* Configuration for adding follower for the Todo */
            followerConfig: {
                create: false,
                required: true,
                delimiter: '|',
                plugins: ['remove_button']
            }

        };


        /* AssigneeManagement is a CLASS which has all the FUNCTIONS related to Assignee i.e Update Assignee, Show assignee update section etc*/
        $scope.AssigneeManagement = {

            /* Showing Assignee Modal For editing */
            showAssigneeEditModal: function () {

                $('#modalAssigneeEdit').modal('show');

                $scope.assigneeAddUpdateModel = {};
                $scope.assigneeAddUpdateModel.assigneeId = $scope.todoDescriptionObject.assigneeId;
                $scope.assigneeAddUpdateModel.todoId = $scope.todoDescriptionObject.todoId;
                $scope.previousAssigneeId = $scope.todoDescriptionObject.assigneeId;
            },


            /* Update Assignee For a particular Todo */
            updateAssignee: function () {

                if ($scope.assigneeAddUpdateModel.assigneeId == $scope.previousAssigneeId) {
                    toaster.error("Error", "Already assigned to this User !");
                }
                else {

                    assigneeFactory.updateTodoAssignee($scope.assigneeAddUpdateModel).success(function (data) {

                        if (data.success) {

                            toaster.success("Success", data.message);
                            $('#modalAssigneeEdit').modal('hide');


                            var currentAssigneeId = $scope.assigneeAddUpdateModel.assigneeId;
                            var previousAssigneeId = $scope.previousAssigneeId;
                            if (currentAssigneeId != previousAssigneeId) {

                                var AssigneeProfilePhoto;
                                var AssigneeName;
                                var AssigneeEmail;
                                var AssigneeId;
                                if ($scope.assigneeAddUpdateModel.assigneeId == undefined) {

                                    AssigneeProfilePhoto = "Uploads/Default/profile.png";
                                    AssigneeName = "Not Assigned";
                                    AssigneeId = null;
                                }
                                else {
                                    var FilterProfilePhotoOfAssignee = $filter('filter')($scope.projectMembersObject, { 'memberUserId': $scope.assigneeAddUpdateModel.assigneeId });

                                    AssigneeProfilePhoto = FilterProfilePhotoOfAssignee[0].memberProfilePhoto;
                                    AssigneeName = FilterProfilePhotoOfAssignee[0].memberName;
                                    AssigneeEmail = FilterProfilePhotoOfAssignee[0].memberEmail;
                                    AssigneeId = $scope.assigneeAddUpdateModel.assigneeId;
                                }
                                $scope.todoDescriptionObject.assigneeProfilePhoto = AssigneeProfilePhoto;
                                $scope.todoDescriptionObject.assigneeName = AssigneeName;
                                $scope.todoDescriptionObject.assigneeEmail = AssigneeEmail;
                                $scope.todoDescriptionObject.assigneeId = AssigneeId;

                            }



                        }

                        else {

                            toaster.error("Error", data.message);
                        }

                    }).error(function () {

                        toaster.error("Error", "Some Error Occured !");

                    });

                }

            },


            /* Configuration for adding assignee for the Todo */
            assigneeConfig: {
                create: false,
                maxItems: 1,
                required: true,
                delimiter: '|'

            }

        };




        /* UserFeedbackManagement is a CLASS which has all the FUNCTIONS related to UserFeedbackManagement i.e Save feedback,get feedback etc*/
        $scope.UserFeedbackManagement = {

            /* Showing User Feedback Management Section*/
            showUserFeedbackManagementSection: function () {

                $scope.showCenterLoader = true;

                $scope.inputSearchFeedback = "";
                closeCentreSections();

                /* Getting User Feedbacks  */
                userFeedbackFactory.getUserFeedbacks().success(function (data) {

                    if (data.success) {

                        $scope.UserFeedbacksObject = data.responseData;
                        $scope.showCenterLoader = false;
                        $('#divUserFeedbackManagementSection').show();
                    }
                    else {

                        toaster.error("Error", data.message);

                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });

            },


            /* Showing User Feedback Modal For adding new Feedback */
            showModalUserFeedback: function () {
                $scope.userFeedbackAddUpdateModel = {};

                $scope.userFeedbackAddUpdateModel.priority = 1;
                $scope.userFeedbackAddUpdateModel.isPrivate = "False";
                $scope.userFeedbackAddUpdateModel.companyId = $scope.MyProfileObject.companyId;
                $('#modalUserFeedback').modal('show');

            },


            /* saving User Feedback */
            saveUserFeedback: function () {


                userFeedbackFactory.insertUserFeedback($scope.userFeedbackAddUpdateModel).success(function (data) {

                    if (data.success) {

                        toaster.success("Success", data.message);
                        $('#modalUserFeedback').modal('hide');


                        /* Getting Updated User Feedbacks  */
                        userFeedbackFactory.getUserFeedbacks().success(function (data) {

                            if (data.success) {

                                $scope.UserFeedbacksObject = data.responseData;

                            }
                            else {

                                toaster.error("Error", data.message);

                            }

                        }).error(function () {

                            toaster.error("Error", "Some Error Occured !");

                        });


                    }
                    else {

                        if (data.message == undefined) {
                            toaster.error("Error", data.errors.toString());
                        }
                        else {

                            toaster.error("Error", data.message);
                        }
                    }



                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });
            },


            /*  Showing User Feedback Description in third Section   */
            showUserFeedbackDescription: function (userfeedbackObject) {

                $scope.selectedUserFeedback = userfeedbackObject.userFeedbackId;
                $scope.userFeedbackDescriptionObject = userfeedbackObject;
                closeRightSections();
                $scope.stretchCentreWindow = false;
                $scope.hideRightWindow = false;


                /* Fetching User Feedback Comments */
                userFeedbackFactory.getUserFeedbackComments(userfeedbackObject.userFeedbackId).success(function (data) {

                    if (data.success) {
                        $scope.userFeedbackCommentsObject = {};
                        $scope.userFeedbackCommentsObject = data.responseData;
                        $('#divUserFeedbackDescriptionSection').show();
                    }

                    else {

                        toaster.error("Error", data.message);

                    }



                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");


                });






            },



            /* Vote User Feedback */
            voteUserFeedback: function (UserFeedbackObject, index) {

                if ($scope.flag) {
                    return;
                }
                $scope.flag = true;

                if (!UserFeedbackObject.hasVoted) {

                    $scope.userFeedbackVoterUpdateModel = {};
                    $scope.userFeedbackVoterUpdateModel.vote = true;
                    $scope.userFeedbackVoterUpdateModel.userFeedbackId = UserFeedbackObject.userFeedbackId;


                    /* Voting User Feedback */
                    userFeedbackFactory.updateUserFeedbackVoter($scope.userFeedbackVoterUpdateModel).success(function (data) {

                        if (data.success) {

                            toaster.success("Success", data.message);

                            $scope.newVoterToBeAddedModel = {};
                            $scope.newVoterToBeAddedModel.voterName = $scope.MyProfileObject.firstName + " " + $scope.MyProfileObject.lastName;
                            $scope.newVoterToBeAddedModel.voterEmail = $scope.MyProfileObject.email;
                            $scope.newVoterToBeAddedModel.voterProfilePhoto = $scope.MyProfileObject.profilePhoto;

                            $scope.UserFeedbacksObject[index].voters.unshift($scope.newVoterToBeAddedModel);
                            $scope.UserFeedbacksObject[index].hasVoted = true;
                            $scope.flag = false;

                        }
                        else {

                            toaster.error("Error", data.message);
                            $scope.flag = false;

                        }


                    }).error(function () {

                        toaster.error("Error", "Some Error Occured !");
                        $scope.flag = false;

                    });
                }

            },


            /*UnVote User Feedback */
            unVoteUserFeedback: function (UserFeedbackObject, index) {
                if ($scope.flag) {
                    return;
                }
                $scope.flag = true;

                if (UserFeedbackObject.hasVoted) {

                    $scope.userFeedbackVoterUpdateModel = {};
                    $scope.userFeedbackVoterUpdateModel.vote = false;
                    $scope.userFeedbackVoterUpdateModel.userFeedbackId = UserFeedbackObject.userFeedbackId;


                    /* UnVoting User Feedback */
                    userFeedbackFactory.updateUserFeedbackVoter($scope.userFeedbackVoterUpdateModel).success(function (data) {

                        if (data.success) {

                            toaster.success("Success", data.message);
                            $scope.UserFeedbacksObject[index].voters.splice(0, 1);
                            $scope.UserFeedbacksObject[index].hasVoted = false;
                            $scope.flag = false;
                        }
                        else {

                            toaster.error("Error", data.message);
                            $scope.flag = false;

                        }


                    }).error(function () {

                        toaster.error("Error", "Some Error Occured !");
                        $scope.flag = false;

                    });
                }

            },


            /* Saving Comment on User Feedback */
            saveCommentUserFeedback: function (UserFeedbackId) {

                $scope.userFeedbackCommentAddUpdateModel = {};
                $scope.userFeedbackCommentAddUpdateModel.userFeedbackId = UserFeedbackId;
                $scope.userFeedbackCommentAddUpdateModel.comment = $scope.userFeedbackComment;

                /* saving User Feedback Comment */
                userFeedbackFactory.insertUserFeedbackComment($scope.userFeedbackCommentAddUpdateModel).success(function (data) {

                    if (data.success) {

                        toaster.success("Success", data.message);

                        $scope.newUserFeedbackCommentToBeAddedModel = {};
                        $scope.newUserFeedbackCommentToBeAddedModel.comment = $scope.userFeedbackComment;
                        $scope.newUserFeedbackCommentToBeAddedModel.creatorName = $scope.MyProfileObject.firstName + " " + $scope.MyProfileObject.lastName;
                        $scope.newUserFeedbackCommentToBeAddedModel.creatorProfilePhoto = $scope.MyProfileObject.profilePhoto;
                        $scope.newUserFeedbackCommentToBeAddedModel.createdDate = new Date();

                        $scope.userFeedbackCommentsObject.push($scope.newUserFeedbackCommentToBeAddedModel);

                        $scope.userFeedbackComment = "";
                    }
                    else {

                        if (data.message == undefined) {

                            toaster.error("Error", data.errors.toString());
                        }
                        else {
                            toaster.error("Error", data.message);
                        }

                    }


                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });

            },

        };




        /* ClientFeedbackManagement is a CLASS which has all the FUNCTIONS related to ClientFeedbackManagement i.e Save feedback,get feedback etc*/
        $scope.ClientFeedbackManagement = {

            /* Showing Client Feedback Modal*/
            showClientFeedbackModal: function (IsTodoFeedback, projectId, todoId) {

                $scope.clientFeedbackAddUpdateModel = {};

                $scope.clientFeedbackAddUpdateModel.rating = 2;



                if (IsTodoFeedback) {

                    $scope.clientFeedbackAddUpdateModel.isTodoFeedback = true;
                    $scope.clientFeedbackAddUpdateModel.todoId = todoId;
                }
                else {

                    $scope.clientFeedbackAddUpdateModel.isTodoFeedback = false;
                    $scope.clientFeedbackAddUpdateModel.projectId = projectId;
                }


                $('#modalClientFeedback').modal('show');
            },

            /* saving client feedback */
            save: function () {


                clientFeedbackFactory.saveClientFeedback($scope.clientFeedbackAddUpdateModel).success(function (data) {

                    if (data.success) {

                        toaster.success("Success", data.message);
                        $('#modalClientFeedback').modal('hide');
                    }
                    else {

                        toaster.error("Error", data.message);
                    }



                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });
            },


            /* Hovering Over Rating */
            hoveringOver: function (value) {
                $scope.overStar = value;
                $scope.percent = 100 * (value / $scope.max);
            }


        };

        $scope.ratingStates = [
         { stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle' },
         { stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty' },
         { stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle' },
         { stateOn: 'glyphicon-heart' },
         { stateOff: 'glyphicon-off' }
        ];

        $scope.max = 5;
        $scope.isReadonly = false;



        /* UserDashboardManagement is a CLASS which has all the FUNCTIONS related to User Dashboard */
        $scope.UserDashboardManagement = {

            /* Showing User Dashboard*/
            showUserDashboard: function () {
                closeCentreSections();
                $scope.showCenterLoader = true;
                $scope.selectedNavigation = 'MyDashboard';


                /*Getting User Upcoming Todos*/

                userDashboardFactory.getUserUpcomingTodos().success(function (data) {

                    if (data.success) {
                        $scope.userUpcomingTodosObject = data.responseData;

                        /* Getting User Upcoming Events */

                        userDashboardFactory.getUserUpcomingEvents().success(function (data) {

                            if (data.success) {
                                $scope.userUpcomingEventsObject = data.responseData;



                                /* Getting User T Line */

                                userDashboardFactory.getUserTLine($scope.MyProfileObject.userId).success(function (data) {

                                    if (data.success) {
                                        $scope.userTLineObject = data.responseData;


                                        /* Getting Client Feedbacks associated with the User  */

                                        clientFeedbackFactory.getUserClientFeedbacks().success(function (data) {

                                            if (data.success) {
                                                $scope.userClientFeedbacksObject = data.responseData;
                                                $scope.showCenterLoader = false;
                                                $('#divMyDashboard').show();

                                            }
                                            else {

                                                toaster.error("Error", data.message);
                                            }

                                        }).error(function () {

                                            toaster.error("Error", "Some Error Occured !");
                                        });

                                    }
                                    else {

                                        toaster.error("Error", data.message);
                                    }

                                }).error(function () {

                                    toaster.error("Error", "Some Error Occured !");
                                });


                            }
                            else {

                                toaster.error("Error", data.message);
                            }

                        }).error(function () {

                            toaster.error("Error", "Some Error Occured !");
                        });


                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });










            },

        };


        /* ClientDashboardManagement is a CLASS which has all the FUNCTIONS related to ClientDashboardManagement */
        $scope.ClientDashboardManagement = {

            /* Showing Client Dashboard*/
            showClientDashboard: function () {
                closeCentreSections();
                $scope.activeClientDashboardPanel = "Todo";
                $scope.selectedNavigation = 'ClientDashboard';
                $scope.showUserTLine = false;
                $('#divClientDashboard').show();
                $scope.hideClientDashboardTodoDescriptionPanel = true;
                $scope.hideClientDashboardProjectDescriptionPanel = true;
                $scope.stretchClientDashboardTodoPanel = true;



                /*Getting Client Upcoming Todos*/
                clientDashboardFactory.getClientUpcomingTodos().success(function (data) {

                    if (data.success) {
                        $scope.clientUpcomingTodosObject = data.responseData;

                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });


                /* Getting Client Projects */

                clientDashboardFactory.getClientProjects().success(function (data) {

                    if (data.success) {
                        $scope.clientProjectsObject = data.responseData;

                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });



                /* Getting Client Concerned Users */

                clientDashboardFactory.getClientConcernedUsers().success(function (data) {

                    if (data.success) {
                        $scope.clientConcernedUsersObject = data.responseData;

                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });



            },


            /* Viewing User T Line */
            viewUserTLine: function (userId) {

                /* Getting User T Line */

                userDashboardFactory.getUserTLine(userId).success(function (data) {

                    if (data.success) {
                        $scope.userTLineObject = data.responseData;
                        $scope.showUserTLine = true;
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");
                });


            },


            /* Hiding User T Line */
            hideUserTLine: function () {

                $scope.showUserTLine = false;

            },


            /* Showing Todo Description on Client Dashboard */
            showTodoDescription: function (todoObject) {
                $scope.stretchClientDashboardTodoPanel = false;

                $scope.todoDescriptionObject = todoObject;
                $scope.hideClientDashboardTodoDescriptionPanel = false;

            },


            /* Showing Project Description on Client Dashboard */
            showProjectDescription: function (projectObject) {
                $scope.stretchClientDashboardTodoPanel = false;

                $scope.projectDescriptionObject = projectObject;
                $scope.hideClientDashboardProjectDescriptionPanel = false;

            },


            /* Showing Todo Panel */
            showTodoPanel: function () {

                $scope.activeClientDashboardPanel = "Todo";
                $scope.hideClientDashboardTodoDescriptionPanel = true;
                $scope.hideClientDashboardProjectDescriptionPanel = true;
                $scope.stretchClientDashboardTodoPanel = true;

            },

            /* Showing Todo Panel */
            showProjectPanel: function () {

                $scope.activeClientDashboardPanel = "Project";
                $scope.hideClientDashboardTodoDescriptionPanel = true;
                $scope.hideClientDashboardProjectDescriptionPanel = true;
                $scope.stretchClientDashboardTodoPanel = true;

            }

        };



        $scope.myDataSource = {
            chart: {
                caption: "Deep Dhindsa",
                subCaption: "Last Five Months Growth",
                numberPrefix: "$",
                theme: "ocean"
            },
            data: [{
                label: "Nathan's Project",
                value: "2423423"
            },
            {
                label: "Garden Groove harbour",
                value: "730000"
            },
            {
                label: "Los Angeles Topanga",
                value: "590000"
            },
            {
                label: "Compton-Rancho Dom",
                value: "520000"
            },
            {
                label: "Daly City Serramonte",
                value: "330000"
            }]
        };


        /* Hiding right window and stretching center window */
        $scope.hideRightWindowFunction = function () {

            $scope.hideRightWindow = true;
            $scope.stretchCentreWindow = true;
            $scope.selectedTodo = "";
            $scope.selectedNotification = "";
            $scope.selectedInbox = "";
            $scope.selectedUserFeedback = "";

        };


        /* Getting Team Window */
        $scope.getTeamWindow = function () {
            closeCentreSections();
            $('#divTeam').show();
        };

        /* Getting Notes Window */
        $scope.getNotesWindow = function () {
            closeCentreSections();
            $scope.selectedWindow = 'Notes';

            $('#divNotes').show();

        };

        /* Getting Links Window */
        $scope.getLinksWindow = function () {
            closeCentreSections();
            $scope.selectedWindow = 'Links';
            $('#divLinks').show();

        };

        /* Getting FIles Window */
        $scope.getFilesWindow = function () {
            closeCentreSections();
            $scope.selectedWindow = 'Files';
            $('#divFiles').show();
        };

        /* Getting Favourites Window */
        $scope.getFavouritesWindow = function () {
            closeCentreSections();
            $scope.selectedWindow = 'Favourites';
            $('#divFavourites').show();
        };

        /* Getting Calendar Window*/
        $scope.getCalendarWindow = function () {
            closeCentreSections();

            $scope.selectedWindow = 'Calendar';
            CalendarEventsFactory.getCalendarEvents($scope.activeTeam.teamId).success(function (data) {
                $scope.CalendarEvents = data;
                $timeout(function () {
                    $scope.$apply();
                    $scope.CalendarTasks = [{ title: "Task No 1", start: new Date(y, m, 5, 6, 0), end: new Date(y, m, 5, 18, 0), allDay: true, className: 'taskInCalendar' },
                         { title: 'Task No 2', start: new Date(y, m, d - 5), end: new Date(y, m, d - 2), className: 'taskInCalendar' },
                         { id: 999, title: 'Task No 3', start: new Date(y, m, d - 3, 16, 0), allDay: false, className: 'taskInCalendar' },
                         { id: 999, title: 'Task No 4', start: new Date(y, m, d + 4, 16, 0), allDay: false, className: 'taskInCalendar' },
                         { title: 'Task No 5', start: new Date(y, m, d + 1, 19, 0), end: new Date(y, m, d + 1, 22, 30), allDay: false, className: 'taskInCalendar', editable: false },
                         { title: 'Task No 6', start: new Date(y, m, 28), end: new Date(y, m, 29), url: 'http://google.com/', className: 'taskInCalendar' },
                         { title: "Task No 7", start: new Date(y, 10, 5, 6, 0), end: new Date(y, 10, 5, 18, 0), allDay: true, className: 'taskInCalendar' },
                         { title: "Task No 8", start: new Date(y, 10, 5, 6, 0), end: new Date(y, 10, 11, 10, 0), allDay: false, className: 'taskInCalendar' },
                         { title: "Task No 9", start: new Date(y, 10, 5, 6, 0), end: new Date(y, 10, 5, 18, 0), allDay: true, className: 'taskInCalendar' },
                         { title: "Task No 10", start: new Date(y, 10, 5, 6, 0), end: new Date(y, 10, 5, 18, 0), allDay: true, className: 'taskInCalendar' },
                         { title: "Task No 11", start: new Date(y, 10, 5, 6, 0), end: new Date(y, 10, 5, 18, 0), allDay: true, url: '', className: 'taskInCalendar' }
                    ];

                    $timeout(function () {
                        $scope.$apply();

                        uiCalendarConfig.calendars['myCalendar2'].fullCalendar('refetchEvents');
                    });
                });
            });
            $('#divCalendar').show();
            uiCalendarConfig.calendars['myCalendar2'].fullCalendar('render');
            var currentView = "month";

        };


        /* Calendar and Event Functionality */
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var currentView = "month";

        $scope.changeTo = 'Hungarian';


        /* event source that pulls from google.com */
        $scope.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'India/Delhi' // an option!
        };

        $scope.CalendarTasks = [
          { title: 'All Day Event', start: new Date(y, m, 24) }
        ];

        /* event source that contains custom events on the scope */
        $scope.events = [
          { title: 'All Day Event', start: new Date(y, m, 24) }
        ];
        /* event source that calls a function on every view switch */
        $scope.eventsF = function (start, end, timezone, callback) {

            var s = new Date(start).getTime() / 1000;
            var e = new Date(end).getTime() / 1000;
            var m = new Date(start).getMonth();

            var events = $scope.CalendarEvents;
            var tasks = $scope.CalendarTasks;

            callback(events);
            $timeout(function () {
                $scope.$apply();
            });

        };

        $scope.calEventsExt = {
            color: '#f00',
            textColor: 'yellow',
            events: [
               { type: 'party', title: 'Lunch', start: new Date(y, m, d, 12, 0), end: new Date(y, m, d, 14, 0), allDay: false },
               { type: 'party', title: 'Lunch 2', start: new Date(y, m, d, 12, 0), end: new Date(y, m, d, 14, 0), allDay: false },
               { type: 'party', title: 'Click for Google', start: new Date(y, m, 28), end: new Date(y, m, 29), url: 'http://google.com/' }
            ]
        };
        /* alert on eventClick */
        $scope.alertOnEventClick = function (data, jsEvent, view) {

            if (data.type == "Task") {
                closeRightSections();
                $scope.stretchCentreWindow = false;
                $scope.hideRightWindow = false;

                $scope.myTodoUpdate = true;

                //$scope.calendarTodoDescription = jsEvent;

                /* Fetching todo details */
                todosFactory.getTodo(data.todoId).success(function (data) {

                    if (data.success) {

                        $scope.todoDescriptionObject = data.responseData;
                        jsEvent.title = $scope.todoDescriptionObject.name;
                        $('#divToDoDescriptionSection').show();
                    }
                    else {

                        toaster.error("Error", data.message);

                    }

                }).error(function () {

                    toaster.error("Error", "Some error occured !");

                });

                /* Getting Attachments Corresponding To ToDo/Task*/
                attachmentsFactory.getToDoAttachments(data.todoId).success(function (data) {

                    if (data.success) {

                        $scope.attachmentsObject = data.responseData;

                        $scope.AllAttachmentsForComment = data.responseData;

                        $scope.LocalAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/computer.png' });
                        $scope.GoogleDriveAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/logo-drive.png' });
                        $scope.DropBoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/drop-box.png' });
                        $scope.BoxAttachmentsForComment = $filter('filter')($scope.attachmentsObject, { 'sourceImage': 'img/source/box-icon.png' });


                    }
                    else {
                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });

                /* Get Followers for the Todo*/
                followersFactory.getFollowers(data.todoId).success(function (data) {

                    if (data.success) {
                        $scope.followersObject = data.responseData;
                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });


                /*Getting Todo Comments*/
                getTodoCommentsInArrangedMode(data.todoId);


                /* Getting Project Members */
                projectUsersFactory.getProjectMembers(data.todoId).success(function (data) {

                    if (data.success) {

                        $scope.projectMembersObject = data.responseData;

                        $timeout(function () {
                            $scope.$apply();
                        });

                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });


                /* Getting history or Todo Notifications */
                todoNotificationsFactory.getTodoNotifications(data.todoId).success(function (data) {

                    if (data.success) {

                        $scope.todoNotificationObject = data.responseData;

                        $timeout(function () {
                            $scope.$apply();
                        });

                    }
                    else {

                        toaster.error("Error", data.message);
                    }

                }).error(function () {

                    toaster.error("Error", "Some Error Occured !");

                });



            }
            else if (data.type == "Event") {

                jsEvent.preventDefault();

                closeRightSections();
                $scope.stretchCentreWindow = false;
                $scope.hideRightWindow = false;
                $scope.eventDescriptionObject = {};
                $scope.eventDescriptionObject = data;
                //$scope.eventDescriptionObject.start = data._start._i;
                //$scope.eventDescriptionObject.end = data._end._i;
                $('#divEventDescriptionSection').show();
                //CalendarEventsFactory.getSingleCalendarEvent(data.id).success(function (data) {

                //    if (data.success) {
                //        $scope.eventDescriptionObject = {};
                //        $scope.eventDescriptionObject = data.responseData;

                //        $('#divEventDescriptionSection').show();
                //    }
                //    else {
                //        toaster.error("Error", data.message);

                //    }
                //}).error(function () {


                //    toaster.error("Error", "Some Error Occured!");

                //});


            }
            // alert(date.type);
            // $scope.alertMessage = (date.title + ' was clicked ');
        };
        /* alert on Drop */
        $scope.alertOnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {

            $scope.calendarEventModel = {};
            $scope.calendarEventModel.Id = event.id;
            $scope.calendarEventModel.Start = event.start._d;
            event.start._i = event.start._d;
            if (event.end == null) {
                $scope.calendarEventModel.End = "";
            }
            else {

                $scope.calendarEventModel.End = event.end._d;
                event.end._i = event.end._d;
            }

            $scope.calendarEventModel.AllDay = event.allDay;


            CalendarEventsFactory.updateCalendarEvent($scope.calendarEventModel).success(function (data) {

                if (data.success) {

                }
                else {

                    toaster.error("Error", "Some Error Occured!");
                }

            }).error(function () {

                toaster.error("Error", "Some Error Occured!");
            });


        };
        /* alert on Resize */
        $scope.alertOnResize = function (event, delta, revertFunc, jsEvent, ui, view) {


            $scope.calendarEventModel = {};
            $scope.calendarEventModel.Id = event.id;
            $scope.calendarEventModel.Start = event.start._d;
            $scope.calendarEventModel.End = event.end._d;
            $scope.calendarEventModel.AllDay = event.allDay;


            CalendarEventsFactory.updateCalendarEvent($scope.calendarEventModel).success(function (data) {

                if (data.success) {

                }
                else {

                    toaster.error("Error", "Some Error Occured!");
                }

            }).error(function () {

                toaster.error("Error", "Some Error Occured!");
            });

        };
        /* add and removes an event source of choice */
        $scope.addRemoveEventSource = function (sources, source) {
            var canAdd = 0;
            angular.forEach(sources, function (value, key) {
                if (sources[key] === source) {
                    sources.splice(key, 1);
                    canAdd = 1;
                }
            });
            if (canAdd === 0) {
                sources.push(source);
            }
        };
        /* add custom event*/
        $scope.addEvent = function () {
            $scope.events.push({
                title: 'Open Sesame',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                className: ['openSesame']
            });
        };

        /* remove event */
        $scope.remove = function (index) {
            $scope.events.splice(index, 1);
        };

        /* Change View */
        $scope.changeView = function (view, calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
        };

        /* Change View */
        $scope.renderCalender = function (calendar) {

        };

        /* Render Tooltip */
        $scope.eventRender = function (event, element, view) {

            element.attr({
                'tooltip': event.title,
                // 'tooltip-append-to-body': true
            });
            $compile(element)($scope);
        };

        $scope.openEventDateFrom = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openedEventDateFrom = true;
        };

        $scope.openEventDateTo = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openedEventDateTo = true;
        };


        /* Day Click */
        $scope.dayClick = function (calendarDate, allDay, jsEvent, view) {


            var myDateFrom = new Date(calendarDate._d);
            var myDateTo = new Date(calendarDate._d);

            myDateFrom.setTime(myDateFrom.getTime() + myDateFrom.getTimezoneOffset() * 60 * 1000);

            myDateTo.setTime(myDateTo.getTime() + myDateTo.getTimezoneOffset() * 60 * 1000);
            myDateTo.setHours(myDateTo.getHours() + 1);

            $scope.calendarEventModel = {};
            $scope.calendarEventModel.eventTimeFrom = myDateFrom;
            $scope.calendarEventModel.eventTimeTo = myDateTo;
            $scope.calendarEventModel.eventDateFrom = myDateFrom;
            $scope.calendarEventModel.eventDateTo = myDateTo;


            $scope.hstep = 1;
            $scope.mstep = 15;
            $scope.ismeridian = true;
            $('#modalCalendarEvent').modal('show');

        };

        /* Adding Calendar Event */
        $scope.AddCalendarEvent = function () {

            var startDate = $filter('date')($scope.calendarEventModel.eventDateFrom, 'fullDate') + " " + $filter('date')($scope.calendarEventModel.eventTimeFrom, 'shortTime');
            var endDate = $filter('date')($scope.calendarEventModel.eventDateTo, 'fullDate') + " " + $filter('date')($scope.calendarEventModel.eventTimeTo, 'shortTime');
            var startExactDate = new Date(startDate);
            var endExactDate = new Date(endDate);

            if (startExactDate >= endExactDate) {

                toaster.error("Error", "End Date must be greater than Start Date");
                return;
            }

            $scope.calendarEventModel.Start = startDate;
            $scope.calendarEventModel.End = endDate;
            $scope.calendarEventModel.TeamId = $scope.activeTeam.teamId;
            CalendarEventsFactory.insertCalendarEvent($scope.calendarEventModel).success(function (data) {

                if (data.success) {
                    CalendarEventsFactory.getCalendarEvents($scope.activeTeam.teamId).success(function (data) {

                        $scope.CalendarEvents = data;

                        $timeout(function () {
                            $scope.$apply();
                            uiCalendarConfig.calendars['myCalendar2'].fullCalendar('refetchEvents');
                            $("#modalCalendarEvent").modal('hide');
                        });
                    });
                };
            });
        };








        /* config object */
        $scope.uiConfig = {
            calendar: {
                height: 700,
                editable: true,
                weekends: true,
                header: {
                    left: 'today prev,next',
                    center: 'title',
                    right: 'month agendaWeek agendaDay '
                },
                dayClick: $scope.dayClick,
                eventClick: $scope.alertOnEventClick,
                eventDrop: $scope.alertOnDrop,
                eventResize: $scope.alertOnResize,
                eventRender: $scope.eventRender,
                viewRender: $scope.renderView,


            }
        };



        /* Change Calendar Weekend Option */
        $scope.calendarWeekendsFunction = function (status) {

            $timeout(function () {
                $scope.$apply();
                $scope.uiConfig.calendar.weekends = status;
            });
            $timeout(function () {
                $scope.$apply();
            });

        };

        $scope.changeLang = function () {
            if ($scope.changeTo === 'Hungarian') {
                $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
                $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
                $scope.changeTo = 'English';
            } else {
                $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                $scope.changeTo = 'Hungarian';
            }
        };
        /* event sources array*/
        $scope.eventSources = [$scope.eventSource, $scope.eventsF];
        //  $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];








        $scope.nestedOptions = {

            dragStart: function (event) {


            },

            accept: function (sourceNodeScope, destNodesScope, destIndex) {
                var destType = destNodesScope.$element.attr('data-type');
                var sourceType = sourceNodeScope.$element.attr('data-type');

                if (sourceType == "Section") {
                    if (sourceType == destType) {
                        return true;
                    }
                    return false;
                }
                else {

                    if (destType == "Section") {
                        return false;
                    }
                    return true;
                }

                //  if (destNodesScope.nodrop || destNodesScope.outOfDepth(sourceNodeScope)) {
                //    return false;
                //   }
                //return true;
                //  var data = sourceNodeScope.$modelValue.datatype,
                // destType = destNodesScope.$nodeScope.$modelValue.datatype;//destNodesScope.$element.attr('data-type');//destNodes.$element.attr('data-type');
                //  alert(data + " and " + destType);

                return true;//(data != destType); // only accept the same type
            },
            dropped: function (event) {



                //alert('working');
                /* to check if it moves in same parent node */
                var MovedObject = event.source.nodeScope.$element.attr('data-type');
                var isParent = event.dest.nodesScope.isParent(event.source.nodeScope);


                /*  To Check whether its position was changed or it was not moved */
                if (event.source.nodesScope.$modelValue == event.dest.nodesScope.$modelValue) {
                    if (event.source.index == event.dest.index) {
                        return;
                    }

                }


                /* It means it is just sorting */
                if (isParent == true) {

                    if (MovedObject == "Section") {

                        sectionsFactory.updateSectionOrder(event.dest.nodesScope.$modelValue).success(function (data) {
                            toaster.success("Success", "Moved Successfully");
                        });
                        //  alert("You Moved Section");
                    }
                    else {
                        sectionsFactory.updateTodoOrder(event.dest.nodesScope.$modelValue).success(function (data) {
                            toaster.success("Success", "Moved Successfully");
                        });
                        // alert("You Moved Todo");
                    }

                }
                    /* It means it is sorting and dragging element from other object */
                else {
                    event.source.nodeScope.$modelValue.sectionId = event.dest.nodesScope.$parent.node.id;
                    event.source.nodeScope.$modelValue.sectionName = event.dest.nodesScope.$parent.node.sectionName;
                    sectionsFactory.updateTodoOrder(event.dest.nodesScope.$modelValue).success(function (data) {
                        toaster.success("Success", "Moved Successfully");
                    });
                    //  alert("You Moved Todo from Other Element");
                }

                //alert(k);
                //// event.source.nodeScope.$modelValue.sectionId = event.dest.nodesScope.$parent.node.id;

                //console.log(event);
                //var sourceNode = event.source.nodeScope,
                //    destNodes = event.dest.nodesScope,
                //    group;
                //// update changes to server
                //if (destNodes.isParent(sourceNode)
                //  && destNodes.$element.attr('data-type') == 'category') { // If it moves in the same group, then only update group
                //    group = destNodes.$nodeScope.$modelValue;
                //    group.save();
                //} else { // save all
                //    $scope.saveGroups();
                //}
            },
            beforeDrop: function (event) {
                //if (!window.confirm('Are you sure you want to drop it here?')) {
                //    event.source.nodeScope.$$apply = false;
                //}
            }
        };


        $scope.removeTodoFromList = function (scope) {
            var TodoId = scope.$nodeScope.$modelValue.todoId;
            //alert(TodoId);

            scope.remove();
        };

        $scope.newSubItem = function (scope) {

            var nodeData = scope.$modelValue;
            nodeData.nodes.push({
                id: nodeData.id * 10 + nodeData.nodes.length,
                title: nodeData.title + '.' + (nodeData.nodes.length + 1),
                nodes: []
            });
            //var nodeData = scope.$modelValue;
            //nodeData.todos.push({
            //    todoId: 1,
            //    name: "New Todo",
            //    profilePhoto: "Uploads/Default/profile.png"

            //});
        };

        $scope.visible = function (item) {

            return !($scope.query && $scope.query.length > 0
            && item.title.indexOf($scope.query) == -1);

        };

        $scope.findNodes = function () {

        };

        $scope.data = [{
            'id': 1,
            'title': 'node1',
            'nodes': [
              {
                  'id': 11,
                  'title': 'node1.1',
                  'nodes': [
                    {
                        'id': 111,
                        'title': 'node1.1.1',
                        'nodes': []
                    }
                  ]
              },
              {
                  'id': 12,
                  'title': 'node1.2',
                  'nodes': []
              }
            ]
        }, {
            'id': 2,
            'title': 'node2',
            'nodes': [
              {
                  'id': 21,
                  'title': 'node2.1',
                  'nodes': []
              },
              {
                  'id': 22,
                  'title': 'node2.2',
                  'nodes': []
              }
            ]
        }, {
            'id': 3,
            'title': 'node3',
            'nodes': [
              {
                  'id': 31,
                  'title': 'node3.1',
                  'nodes': []
              }
            ]
        }, {
            'id': 4,
            'title': 'node4',
            'nodes': [
              {
                  'id': 41,
                  'title': 'node4.1',
                  'nodes': []
              }
            ]
        }];




        /* Angular Timer */
        $scope.clock = "loading clock..."; // initialise the time variable
        $scope.tickInterval = 1000 //ms

        var tick = function () {
            $scope.clock = Date.now() // get the current time
            $timeout(tick, $scope.tickInterval); // reset the timer
        }

        // Start the timer
        $timeout(tick, $scope.tickInterval);


        $scope.barConfig = {
            group: 'shared',

            animation: 150,
            sort: true,
            draggable: false,
            onSort: function (/** ngSortEvent */evt) {
                
                // @see https://github.com/RubaXa/Sortable/blob/master/ng-sortable.js#L18-L24
            }
        };
        $scope.sortTodos = {

            animation: 150,
            sort: true,
            dataIdAttr: 'todo.id',
            onSort: function (/** ngSortEvent */evt) {
                alert('Sorting Todos');
                
                // @see https://github.com/RubaXa/Sortable/blob/master/ng-sortable.js#L18-L24
            }
        };

        //$scope.todos = [
        //	{text: 'learn angular', done: true},
        //	{text: 'build an angular app', done: false}
        //];
        $scope.todos = [{ "id": 2, "sectionName": "No Section", "projectId": 2, "todos": [{ "todoId": 24, "name": "Deep", "sectionId": 2, "profilePhoto": "Uploads/Default/profile.png" }] },
            { "id": 4, "sectionName": "Low Priority", "projectId": 2, "todos": [] },
            { "id": 5, "sectionName": "High Priority", "projectId": 2, "todos": [] },
            { "id": 6, "sectionName": "Intermediate Priority", "projectId": 2, "todos": [{ "todoId": 19, "name": "Ubhawal", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 11, "name": "Second Task", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 14, "name": "Fifth Task", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 16, "name": "Hardeepinder Singh", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 6, "name": "koghioioo", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 12, "name": "Third Task", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 15, "name": "Deep Dhindsa", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 17, "name": "Dhindsa", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 18, "name": "Singh", "sectionId": 6, "profilePhoto": "Uploads/Default/profile.png" }] },
            { "id": 7, "sectionName": "Low Priority", "projectId": 2, "todos": [{ "todoId": 23, "name": "Deep", "sectionId": 7, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 13, "name": "FourthTask", "sectionId": 7, "profilePhoto": "Uploads/Default/profile.png" }] },
        { "id": 8, "sectionName": "High Cost", "projectId": 2, "todos": [] },
        { "id": 9, "sectionName": "Low Cost", "projectId": 2, "todos": [{ "todoId": 25, "name": "Deep", "sectionId": 9, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 10, "name": "First Task", "sectionId": 9, "profilePhoto": "Uploads/Default/profile.png" }, { "todoId": 8, "name": "A Totally New Task", "sectionId": 9, "profilePhoto": "Uploads/Default/profile.png" }] }]
        //$scope.addTodo = function () {
        //    alert('It is working');
        //    $scope.todos.push({ text: $scope.todoText, done: false });
        //    $scope.todoText = '';
        //};
        $scope.addTodo = function () {
            alert('It is working');
            $scope.todos.push({ sectionName: $scope.todoText });
            $scope.todoText = '';
        };
        $scope.remaining = function () {
            var count = 0;
            angular.forEach($scope.todos, function (todo) {
                count += todo.done ? 0 : 1;
            });
            return count;
        };

        $scope.archive = function () {
            var oldTodos = $scope.todos;
            $scope.todos = [];
            angular.forEach(oldTodos, function (todo) {
                if (!todo.done) $scope.todos.push(todo);
            });
        };


        $scope.remaining = function () {
            var count = 0;
            angular.forEach($scope.todos, function (todo) {
                count += todo.done ? 0 : 1;
            });
            return count;
        };

        $scope.sortableConfig = { group: 'todo', animation: 150 };
        'Start End Add Update Remove Sort'.split(' ').forEach(function (name) {
            $scope.sortableConfig['on' + name] = console.log.bind(console, name);
        });



        /* Update section name on blur in Kanban View */
        $scope.updateSectionNameOnBlur = function (id, sectionName, item) {

            var newSectionText = $('#' + id).text();
            if (newSectionText != sectionName) {
                item.sectionName = newSectionText;
                $scope.updateSectionModel = {};
                $scope.updateSectionModel.id = id;
                $scope.updateSectionModel.sectionName = newSectionText;

                sectionsFactory.updateSection($scope.updateSectionModel).success(function (data) {
                    if (data.success) {
                        toaster.success("Section Updated Successfully", "Success");

                    }
                    else {

                        toaster.error("Some Error Occured", "Error");
                        item.sectionName = sectionName;
                    }

                });
            }
        };


        /* Show Task Add Section on AddNewTask Click In KanBan View */
        $scope.ShowTaskAddSection = function (column) {
            column.ShowTaskAddSectionStatus = true;
            var id = "input" + column.id;
            $('#' + id).focus();
        };


        /* Hide input on blur in Kanban View */
        $scope.HideAddNewTodoInputKanban = function (column) {

            column.ShowTaskAddSectionStatus = false;
            var id = "input" + column.id;
            $('#' + id).val("");
        };

        /*  It includes KanBan Operations like Dragging and Sorting */
        $scope.kanbanSortOptionsForSection = {

            //restrict move across columns. move only within column.
            /*accept: function (sourceItemHandleScope, destSortableScope) {
             return sourceItemHandleScope.itemScope.sortableScope.$id !== destSortableScope.$id;
             },*/

            dragMove: function (itemPosition, containment, eventObj) {

                if (eventObj) {
                    var targetY,
                    contatiner = document.getElementById("board").offsetLeft,
                    targetY = eventObj.pageX - (window.pageXOffset);
                    document.getElementById("board").offsetLeft = eventObj.offsetX;

                    if (targetY < contatiner.top) {
                        contatiner.scrollLeft = contatiner.scrollLeft - 20;
                    } else if (targetY > contatiner.bottom) {
                        contatiner.scrollLeft = contatiner.scrollLeft + 20;
                    }
                }

            },

            itemMoved: function (event) {
                
                event.source.itemScope.modelValue.sectionId = event.dest.sortableScope.$parent.column.id;
                sectionsFactory.updateListOrderOrSectionIdInSection(event.dest.sortableScope.modelValue);
            },

            orderChanged: function (event) {
                alert('Working');
                
                sectionsFactory.updateListOrderOrSectionIdInSection(event.dest.sortableScope.modelValue);
            },

            containment: '.kanbanPanel',

            removeTodo: function (column, todo) {
                if (confirm("Are you sure")) {
                    column.todos.splice(column.todos.indexOf(todo), 1);
                    todosFactory.deleteTodo(todo.todoId);
                }
            }
        };

        /*  It includes KanBan Operations like Dragging and Sorting */
        $scope.kanbanSortOptions = {

            //restrict move across columns. move only within column.
            /*accept: function (sourceItemHandleScope, destSortableScope) {
             return sourceItemHandleScope.itemScope.sortableScope.$id !== destSortableScope.$id;
             },*/

            dragMove: function (itemPosition, containment, eventObj) {

                if (eventObj) {

                    var targetY,
                    contatiner = document.getElementById("board"),
                    targetY = eventObj.pageY - ($window.pageYOffset);
                    if (targetY < contatiner.top) {
                        contatiner.scrollTop = contatiner.scrollTop - Configuration.ScrollSpeed;
                    } else if (targetY > contatiner.bottom) {
                        contatiner.scrollTop = contatiner.scrollTop + Configuration.ScrollSpeed;
                    }
                }

            },

            itemMoved: function (event) {

                event.source.itemScope.modelValue.sectionId = event.dest.sortableScope.$parent.column.id;
                sectionsFactory.updateListOrderOrSectionIdInSection(event.dest.sortableScope.modelValue);
            },

            orderChanged: function (event) {

                sectionsFactory.updateListOrderOrSectionIdInSection(event.dest.sortableScope.modelValue);
            },

            containment: '.kanbanPanel',

            removeTodo: function (column, todo) {
                if (confirm("Are you sure")) {
                    column.todos.splice(column.todos.indexOf(todo), 1);
                    todosFactory.deleteTodo(todo.todoId);
                }
            }
        };

        //$scope.removeCard = function (column, todo) {

        //    if (confirm("Are you sure")) {
        //        column.todos.splice(column.todos.indexOf(todo), 1);

        //        todosFactory.deleteTodo(todo.todoId);
        //    }

        //}


        /* Add New Task From Kanban View */
        $scope.AddNewTaskFromKanbanView = function (column) {
            var id = "input" + column.id;
            var TodoName = $('#' + id).val();
            $scope.newTask = {};
            $scope.newTask.name = TodoName;
            $scope.newTask.sectionId = column.id;
            $scope.newTask.listOrder = column.todos.length;
            todosFactory.insertTodo($scope.newTask).success(function (data) {
                if (data.success) {
                    $scope.newTask.profilePhoto = "Uploads/Default/profile.png";
                    $scope.newTask.todoId = data.message;
                    column.ShowTaskAddSectionStatus = false;

                    column.todos.push($scope.newTask);
                    $scope.newTask.focusInput = true;
                    $('#' + id).val("");
                }
            });

        }

        $scope.addNewCard = function (column) {

            $scope.newTask = {};
            $scope.newTask.todoId = 4;
            $scope.newTask.name = "Hardeepinder";
            $scope.newTask.profilePhoto = "Uploads/Profile/Thumbnail/41a7c97b.jpg";
            $scope.sectionId = 8;
            column.push($scope.newTask);
        }


        /*  Angular Selectize */
        $scope.disable = false;


        //Rakesh: below code is for the progress chart
        //=======================================================
        //Angular Form Bindings
        //=======================================================
        $scope.myModel = [6, 8];

        $scope.myOptions = [{ value: '1', text: 'Hardeep Singh (deep@weexcel.in)' }, { value: '3', text: 'Deep Dhindsa (er.deepdhindsa@gmail.com)' }, { value: '4', text: 'Aryan Deol (gulshandeep.deol@gmail.com)' }, { value: '5', text: 'Rakesh Kumar (rakesh.kumar@weexcel.in)' }
        , { value: '6', text: 'Rakesh London (rakesh.kumar@weexcelindia.com)' }, { value: '7', text: 'SP GIll' }, { value: '8', text: 'Jordy' }];

        $scope.changeOptions = function () {
            $scope.myOptions = [{ value: '1', text: 'Kirk' }];
        }

        $scope.changeValue = function () {
            $scope.myModel = '2';
        }

        $scope.myConfig = {
            create: false,
            onChange: function (value) {
                console.log('onChange', value)
            },
            // maxItems: 1,
            required: false,
        }





    });

    //})();
});

