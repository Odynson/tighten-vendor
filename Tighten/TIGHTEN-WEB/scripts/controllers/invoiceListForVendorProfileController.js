﻿define(['app'], function () {


    app.controller("invoiceListForVendorProfileController", function ($scope, $location, $timeout, sharedService, vendorInvoiceFactory, invoiceFactory, paymentDetailFactory, toaster, $rootScope, $stateParams, $location, apiURL, $http) {

        $scope.imgURL = apiURL.imageAddress;
        var CompanyId;
        var UserId;
        var VendorId = 0;
        $scope.invoiceCompanyList = [];
        $scope.invoiceListOutstanding = [];
        $scope.invoiceListPaid = [];
        $scope.invoiceListRejected = [];
        $scope.TotalOutstandingInvoiceAmount = 0;
        $scope.TotalPaidInvoiceAmount = 0;
        $scope.TotalRejectedInvoiceAmount = 0
        $scope.filterInvocieModal = { fromDate: "", toDate: "", CompanyId: "" }

        $scope.maxSize = 5;     // Limit number for pagination display number.  
        $scope.totalCountOutstanding = 0;  // Total number of items in all pages. initialize as a zero  
        $scope.totalCountPaid = 0;
        $scope.totalCountRejected = 0;

        $scope.pageIndexOutstanding = 1;   // Current page number. First page is 1.-->  
        $scope.pageIndexPaid = 1;
        $scope.pageIndexRejected = 1;

        $scope.pageSizeSelectedOutstanding = 5; // Maximum number of items per page. 
        $scope.pageSizeSelectedPaid = 5;
        $scope.pageSizeSelectedRejected = 5;


        $scope.pagesizeListOutstanding = [
            { PageSizeOutstanding: 5 },
            { PageSizeOutstanding: 10 },
            { PageSizeOutstanding: 25 },
            { PageSizeOutstanding: 50 },

        ];
        $scope.PageSizeOutstanding = $scope.pagesizeListOutstanding[0];


        $scope.pagesizeListPaid = [
            { PageSizePaid: 5 },
            { PageSizePaid: 10 },
            { PageSizePaid: 25 },
            { PageSizePaid: 50 },

        ];
        $scope.PageSizePaid = $scope.pagesizeListPaid[0];


        $scope.pagesizeListRejected = [
            { PageSizeRejected: 5 },
            { PageSizeRejected: 10 },
            { PageSizeRejected: 25 },
            { PageSizeRejected: 50 },

        ];
        $scope.PageSizeRejected = $scope.pagesizeListRejected[0];



        $scope.totalPageCountOutstanding = 0;
        $scope.totalPageCountPaid = 0;
        $scope.totalPageCountRejected = 0;

        $scope.pagesizeList = [
            { PageSize: 5 },
            { PageSize: 10 },
            { PageSize: 25 },
            { PageSize: 50 },

        ];

        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }

            $scope.getYearsList = function (range) {
                var currentYear = new Date().getFullYear();
                var years = [];
                for (var i = 0; i < range; i++) {
                    years.push(currentYear + i);
                }
                return years;
            };

            CompanyId = sharedService.getCompanyId();
            VendorId = sharedService.getVendorId()
            UserId = sharedService.getUserId();
            // getAllInvoiceDetailList(VendorId);
            getAllInvoiceDetailListOutstanding(VendorId);
            getAllInvoiceDetailListPaid(VendorId);
            getAllInvoiceDetailListRejected(VendorId);
            getcompanyForDropFilterInvoice(CompanyId);

        })

        $scope.format = 'MM/dd/yyyy';

        $scope.popup = {
            opened1: false,
            opened2: false
        };

        $scope.open1 = function () {
            $scope.popup.opened1 = true;

        };

        $scope.open2 = function () {
            $scope.popup.opened2 = true;
        };

        function getcompanyForDropFilterInvoice(CompanyId) {
            debugger;
            $('#globalLoader').show();
            vendorInvoiceFactory.getcompanyForDropFilterInvoice(CompanyId)
                .success(function (data) {
                    if (data.success) {
                        $scope.invoiceCompanyList = data.ResponseData;
                        $('#globalLoader').hide();

                    }
                    else {
                        toaster.error("Error", message.error);
                        $('#globalLoader').hide();
                    }

                }), Error(function () {
                    error
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        $scope.FilterInvoiceByCompany = function (selectedCompany) {
            debugger;
            $scope.filterInvocieModal.CompanyId = selectedCompany;
        }

        $scope.SearchInvoiceVendorProfile = function () {

            // getAllInvoiceDetailList(VendorId);
            getAllInvoiceDetailListOutstanding(VendorId);
            getAllInvoiceDetailListPaid(VendorId);
            getAllInvoiceDetailListRejected(VendorId);
        }

        ////////////////////////////////////getTotalAmountForNotPaidInvoice//////////////////////////////////////////////////////////////
        function getTotalAmountForNotPaidInvoice() {
            debugger;
            if ($scope.invoiceListOutstanding.length > 0) {
                for (var i = 0; i < $scope.invoiceListOutstanding.length; i++) {
                    var Inv = $scope.invoiceListOutstanding[i];
                    if (Inv.IsPaid == false && Inv.IsRejected == false) {
                        //  $scope.TotalOutstandingInvoiceAmount += (Inv.TotalAmount).toFixed(2);
                        $scope.TotalOutstandingInvoiceAmount = (parseFloat($scope.TotalOutstandingInvoiceAmount)+ parseFloat(Inv.TotalAmount)).toFixed(2);
                    }
                }
            }
        }

        ////////////////////////////////////getTotalAmountForPaidInvoice//////////////////////////////////////////////////////////////
        function getTotalAmountForPaidInvoice() {
            debugger;
            if ($scope.invoiceListPaid.length > 0) {
                for (var i = 0; i < $scope.invoiceListPaid.length; i++) {
                    var Inv = $scope.invoiceListPaid[i];
                    if (Inv.IsPaid == true && Inv.IsRejected == false) {
                        //  $scope.TotalPaidInvoiceAmount += (Inv.TotalAmount).toFixed(2);
                        $scope.TotalPaidInvoiceAmount = (parseFloat($scope.TotalPaidInvoiceAmount) + parseFloat(Inv.TotalAmount)).toFixed(2);;
                    }
                }
            }

        }

        ////////////////////////////////////getTotalAmountForPaidInvoice//////////////////////////////////////////////////////////////
        function getTotalAmountRejectedForPaidInvoice() {
            debugger;
            var total_Amount = 0;
            if ($scope.invoiceListRejected.length > 0) {
                for (var i = 0; i < $scope.invoiceListRejected.length; i++) {
                    var Inv = $scope.invoiceListRejected[i];
                    if (Inv.IsRejected == true) {
                      //  $scope.TotalRejectedInvoiceAmount += (Inv.TotalAmount).toFixed(2);
                        $scope.TotalRejectedInvoiceAmount = (parseFloat($scope.TotalRejectedInvoiceAmount) + parseFloat(Inv.TotalAmount)).toFixed(2);;
                    }
                }
            }

        }


        ////////////////////////////////////getAllInvoiceDetailList//////////////////////////////////////////////////////////////
        function getAllInvoiceDetailList(VendorId) {
            debugger;
            $('#globalLoader').show();
            $scope.TotalOutstandingInvoiceAmount = 0;
            $scope.TotalPaidInvoiceAmount = 0;
            $scope.TotalRejectedInvoiceAmount = 0

            var data = {}
            data.VendorId = VendorId;
            data.FromDate = $scope.filterInvocieModal.fromDate;
            data.ToDate = $scope.filterInvocieModal.toDate;
            data.CompanyId = $scope.filterInvocieModal.CompanyId;
            data.PageSizeSelected = $scope.pageSizeSelected;
            data.PageIndex = $scope.pageIndex;
            vendorInvoiceFactory.getAllInvoiceDetailList(data)
                .success(function (data) {
                    if (data.ResponseData != null) {

                        $scope.invoiceList = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCount = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getcompanyForDropFilterInvoice(CompanyId);
                        getTotalAmountForNotPaidInvoice();
                        getTotalAmountForPaidInvoice();
                        getTotalAmountRejectedForPaidInvoice();
                        $('#globalLoader').hide();
                    }
                    else {

                        $('#globalLoader').hide();
                    }
                }), Error(function () {
                    error
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        $scope.previewInvoiceAftersend = function (item) {
            debugger;
            vendorInvoiceFactory.getPreviewInvoicePaidOutstandingReject(item.Id, VendorId)
                .success(function (data) {
                    if (data.success) {

                        $scope.vedorInvoicePaidOutstandRejectPreview = data.ResponseData;
                        $('#PreviewAfterRasisingInvoiceVendorProfile').modal('show')
                    }
                    else {
                        toaster.error("Error", message.error);
                    }
                })

            $('#InvoicePreviewAfterRasisingInvoice').modal('show')
            $scope.paymentDetailAddUpdateModel = {};
        }

        function getAllInvoiceDetailListOutstanding(VendorId) {
            debugger;
            $('#globalLoader').show();
            $scope.TotalOutstandingInvoiceAmount = 0;
            var data = {}
            if ($scope.filterInvocieModal.CompanyId==undefined||$scope.filterInvocieModal.CompanyId==null|| $scope.filterInvocieModal.CompanyId == "") {

            }
            //alert($scope.filterInvocieModal.fromDate + ' ' + $scope.filterInvocieModal.toDate);
            data.VendorId = VendorId;
            data.FromDate = $scope.filterInvocieModal.fromDate;
            data.ToDate = $scope.filterInvocieModal.toDate;
            data.VendorCompanyId = $scope.filterInvocieModal.CompanyId;
            
            data.PageSizeSelected = $scope.pageSizeSelectedOutstanding;
            data.PageIndex = $scope.pageIndexOutstanding;
            vendorInvoiceFactory.getAllInvoiceDetailListOutstanding(data)
                .success(function (data) {
                    debugger;
                    if (data.ResponseData != null) {
                        $scope.invoiceListOutstanding = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCountOutstanding = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountForNotPaidInvoice();
                        $('#globalLoader').hide();
                    }
                    else {
                        $('#globalLoader').hide();
                    }
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        function getAllInvoiceDetailListPaid(VendorId) {
            debugger;
            $('#globalLoader').show();
            $scope.TotalPaidInvoiceAmount = 0;
            var data = {}
            data.VendorId = VendorId;
            data.FromDate = $scope.filterInvocieModal.fromDate;
            data.ToDate = $scope.filterInvocieModal.toDate;
            data.CompanyId = $scope.filterInvocieModal.CompanyId;
            data.PageSizeSelected = $scope.pageSizeSelectedPaid;
            data.PageIndex = $scope.pageIndexPaid;
            vendorInvoiceFactory.getAllInvoiceDetailListPaid(data)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.invoiceListPaid = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCountPaid = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountForPaidInvoice();
                        $('#globalLoader').hide();
                    }
                    else {
                        $('#globalLoader').hide();
                    }
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        function getAllInvoiceDetailListRejected(VendorId) {
            debugger;
            $('#globalLoader').show();
            $scope.TotalRejectedInvoiceAmount = 0
            var data = {}
            data.VendorId = VendorId;
            data.FromDate = $scope.filterInvocieModal.fromDate;
            data.ToDate = $scope.filterInvocieModal.toDate;
            data.CompanyId = $scope.filterInvocieModal.CompanyId;
            data.PageSizeSelected = $scope.pageSizeSelectedRejected;
            data.PageIndex = $scope.pageIndexRejected;
            vendorInvoiceFactory.getAllInvoiceDetailListRejected(data)
                .success(function (data) {
                    if (data.ResponseData != null) {
                        $scope.invoiceListRejected = data.ResponseData;
                        if (data.ResponseData.length > 0) {
                            $scope.totalCountRejected = data.ResponseData[0].TotalPageCount;
                        }
                        toaster.success("Success", message.successdataLoad);
                        getTotalAmountRejectedForPaidInvoice();
                        $('#globalLoader').hide();
                    }
                    else {
                        $('#globalLoader').hide();
                    }
                }), Error(function () {
                    toaster.error("Error", message.error);
                    $('#globalLoader').hide();
                })
        }

        ////////////////////////////////////------------paging code/////////////////////////////////

        $scope.changePageSize = function (pageSizeSelected, param) {
            debugger;
            if (param == 'Outstanding') {
                $scope.pageSizeSelectedOutstanding = pageSizeSelected.PageSizeOutstanding;
                getAllInvoiceDetailListOutstanding(CompanyId)

            }
            else if (param == 'Paid') {
                $scope.pageSizeSelectedPaid = pageSizeSelected.PageSizePaid;
                getAllInvoiceDetailListPaid(CompanyId)
            }
            else if (param == 'Rejected') {
                $scope.pageSizeSelectedRejected = pageSizeSelected.PageSizeRejected;
                getAllInvoiceDetailListRejected(CompanyId)

            }
        }

        $scope.pageChanged = function (pageIndex, param) {
            debugger;
            if (param == 'Outstanding') {
                $scope.pageIndexOutstanding = pageIndex;
                getAllInvoiceDetailListOutstanding(CompanyId)

            }
            else if (param == 'Paid') {
                $scope.pageIndexPaid = pageIndex;
                getAllInvoiceDetailListPaid(CompanyId)
            }
            else if (param == 'Rejected') {
                $scope.pageIndexRejected = pageIndex;
                getAllInvoiceDetailListRejected(CompanyId)

            }
        }


        ////////////////////////////////////------------paging code/////////////////////////////////
    })


})