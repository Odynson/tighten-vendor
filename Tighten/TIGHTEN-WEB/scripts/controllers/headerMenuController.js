﻿define(['app','ngCookies'], function (app) {
    app.controller("headerMenuController", function ($scope, $rootScope, $timeout, $http, sharedService, toaster, $location, $cookies, $cookieStore) {

        $scope.logout = function () {
            $cookieStore.put('globals', {});
            $cookieStore.put('sharedService', {});
            //$('#headerMenu').hide();
            //$('#sideMenu').hide();
            $rootScope.showSideMenu = true;
            $rootScope.headerMenu = true;
            $location.path('/login');
        }
    })
});