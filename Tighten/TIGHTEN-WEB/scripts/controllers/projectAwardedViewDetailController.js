﻿define(['app'], function () {

    app.controller('projectAwardedViewDetailController', function ($scope, sharedService, $rootScope, toaster, $filter, $http, $location, vendorProjectFactory, apiURL, vendorFactory, $stateParams) {


        var ProjectId;
        var CompanyId;
        $scope.IsSaveSpentHour = false;
        $scope.projectAwardedViewDetailList = [];
        var VendorId = sharedService.getVendorId()

        angular.element(document).ready(function () {
            ProjectId = $stateParams.pid;
            CompanyId = sharedService.getCompanyId();
            getProjectAwardedViewDetail(ProjectId, VendorId)
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
            }

        })


        function getProjectAwardedViewDetail(ProjectId, VendorId) {
            debugger;
            $('#globalLoader').show();
            vendorFactory.getProjectAwardedViewDetail(ProjectId, VendorId)
             .success(function (data) {
                 if (data.success) {
                     $scope.projectAwardedViewDetailList = data.ResponseData
                     toaster.success("Success", message.successdataLoad);

                     //------getting custom control area ---------//
                    
                     $scope.customcontrolmodel = JSON.parse(data.ResponseData.CustomControlValueModel.CustomControlData);

                     //-----------------Custom cuntrol is not use by Company---------------------------------------///
                     countUpaidInvoiceVendorProfile();
                     $scope.CustomcontrolList = data.ResponseData.CustomControlUIList;
                     $scope.CustomDirectiveReady = true;
                 }
                 else {
                     toaster.error("Error", data.error);
                 }
                 $('#globalLoader').hide();
             }),
            Error(function (data, status, headers, config) {
                toaster.error("Error", data.error);
                $('#globalLoader').hide();
            })
        }


        // custom control
        $scope.CustomDirectiveReady = false;
        $scope.ProjectIsEdit = true;
        $scope.customcontrolmodel = {};


        $scope.countUnpaidInvoice = 0
        function countUpaidInvoiceVendorProfile() {
            debugger;
            $scope.projectAwardedViewDetailList.VendorProjectProgressList;

            for (var i = 0; i < $scope.projectAwardedViewDetailList.VendorProjectProgressList.length; i++) {
                if ($scope.projectAwardedViewDetailList.VendorProjectProgressList[i].IsPaid == false) {
                    $scope.countUnpaidInvoice += 1

                }
            }
        }



        //////////////////////////////////////////////////////modalMilestoneDetail///////////////////////////////////////////////////////////////////////
        $scope.PhaseIndex
        $scope.modalMilestoneAcceptReject = function (index) {
            debugger;
            $scope.PhaseIndex = index;
            $('#modalshowMilestoneFromAwarded').modal('show')

        }


        $scope.downloadProjectandPhaseFile = function (FilePath, FileName, ActualFileName) {
            debugger;
            $('#globalLoader').show();

            var url = apiURL.baseAddress + "VendorApi/";

            $http({
                method: 'Post',
                url: url + "GetFiledownload",
                data: { FileName: FileName, FilePath: FilePath },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers) {
                headers = headers();
                debugger;
                //var filename = headers['x-filename'];
                var contentType = headers['content-type'];

                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", ActualFileName);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    $('#globalLoader').hide();

                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    console.log(ex);
                    $('#globalLoader').hide();
                }
            }).error(function (data, status) {
                console.log(data);
                $('#globalLoader').hide();
            });
        };






    })



})