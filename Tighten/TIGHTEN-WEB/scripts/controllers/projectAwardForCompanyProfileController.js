﻿define(['app'], function () {
    app.controller("projectAwardForCompanyProfileController",
        function ($scope, sharedService, $rootScope, toaster, apiURL, projectsFactory, $http, $sce) {

            var baseurl = apiURL.baseAddress;
            var CompanyId = 0
            var dataModal = { CompanyId: CompanyId, VendorCompanyId: 0, Name: "", UnpaidInvoice: false, RequestType: 3, PageIndex: 0, PageSize: 0 }


            $scope.maxSize = 5;     // Limit number for pagination display number.  
            $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
            $scope.pageIndex = 5;   // Current page number. First page is 1.-->  
            $scope.pageSizeSelected = 5; // Maximum number of items per page.  


            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu');
                $rootScope.activeTeam = [];
            }

            angular.element(document).ready(function () {
                CompanyId = sharedService.getCompanyId();
                getVendorCompanyListForDrop(CompanyId)
                dataModal = { CompanyId: CompanyId, VendorCompanyId: 0, Name: "", UnpaidInvoice: false, RequestType: 3, RequestType: 3, PageIndex: 1, PageSize: 5 }
            })

            dataModal = { CompanyId: CompanyId, VendorCompanyId: 0, Name: "", UnpaidInvoice: false, RequestType: 3, RequestType: 3, PageIndex: 1, PageSize: 5 }
            $scope.vendorDropList = []
            $scope.projectAwardedToVendorModal = { VendorCompanyId: 0, Name: "--Select All--" };


            function getVendorCompanyListForDrop(CompanyId) {
                //$('#globalLoader').show();

                debugger;
                projectsFactory.getVendorListForDrop(CompanyId)
                .success(function (data) {
                    if (data.success) {

                        $scope.vendorDropList = data.ResponseData;
                        $scope.vendorDropList.push($scope.projectAwardedToVendorModal)
                        getProjectAwardedForcompanyProfile();
                        toaster.success("Success", message.successdataLoad);
                        $('#globalLoader').hide();
                    }
                    else {
                        $('#globalLoader').hide();
                        toaster.error("Error", data.error);
                    }
                }), Error(function (data, status, headers, config) {
                    $('#globalLoader').hide();
                    toaster.error("Error", data.error);
                })

            }


            function getProjectAwardedForcompanyProfile() {
                debugger;
                $('#globalLoader').show();
                projectsFactory.getProjectAwardedForcompanyProfile(dataModal)
                .success(function (data) {
                    if (data.success) {
                        if (data.ResponseData != null) {
                            $scope.projectAwardedList = data.ResponseData;

                            if (data.ResponseData.length > 0) {
                                $scope.totalCount = data.ResponseData[0].TotalPageCount;
                            }
                        }

                        toaster.success("Success", message.successdataLoad);
                        $('#globalLoader').hide();
                    }
                }),
                Error(function (data, status, headers, config) {
                    $('#globalLoader').hide();
                    toaster.error("Error", data.error);
                })

            }


            //////////////////////////////////////////////////////modalMilestoneDetail///////////////////////////////////////////////////////////////////////
            $scope.PhaseIndex
            $scope.modalMilestoneDetail = function (index) {
                debugger;
                $scope.PhaseIndex = index;
                $('#modalshowMilestoneDetail').modal('show')

            }



            $scope.performanceStatusList = [
                    { Id: 0, Status: "--Select All--" },
                    { Id: 1, Status: "--well in range--" },
                    { Id: 2, Status: "--neck to neck--" },
                    { Id: 3, Status: "--Over Spent--" },

            ];


            $scope.filterPerformanceModal = $scope.performanceStatusList[0];


            /////////////////////////////////////--Search by project Name////////////////////////////////////////////////////////////////////////////////
            //$scope.searchProjectByName = function (searchText) {
            //    debugger
            //    dataModal.Name = searchText;
            //    getProjectAwardedForcompanyProfile();
            //}


            ///////////////////////////////////-filter Project by vendor///////////////////////////////////////////////////////////////////////////////
            $scope.filterAwardedProjectByVendor = function (projectAwardedToVendorModal) {
                debugger
                dataModal.VendorCompanyId = projectAwardedToVendorModal.VendorCompanyId;
                getProjectAwardedForcompanyProfile();
            }



            //////////////////////////////////////////--filterVendorperformancestatus////////////////////////////////////////////////////
            $scope.filterVendorPerformance = function (filterPerformanceModal) {
                debugger
                dataModal.Id = filterPerformanceModal.Id;
                getProjectAwardedForcompanyProfile();
            }

            //////////////////////////////////////////--showUpdaidVendorInvoice---////////////////////////////////////////////////////
            $scope.showUpdaidVendorInvoice = function (unpaidInvoice) {
                debugger;
                dataModal.unpaidInvoice = unpaidInvoice;
                getProjectAwardedForcompanyProfile();
            }

            var autoModal = {}
            //////////////////////////////////////////--autoCompleteOptions---////////////////////////////////////////////////////
            $scope.autoCompleteOptions = {

                minimumChars: 3,
                dropdownWidth: '500px',
                dropdownHeight: '200px',
                data: function (term) {
                    return $http.post(baseurl + 'ProjectsApi/GetProjectName',
                        autoModal = { CompanyId: CompanyId, Name: term, VendorId: 0, ProjectId: 0, RequestType: 3 })
                        .then(function (data) {
                            debugger;
                            return data.data.ResponseData;
                        });
                },
                renderItem: function (item) {
                    return {
                        value: item.Name,
                        label: $sce.trustAsHtml(
                        "<p class='auto-complete'>"
                        + item.Name +
                        "</p>")

                    };

                },
                itemSelected: function (item) {
                    debugger;
                    dataModal.ProjectId = item.item.ProjectId
                    $scope.searchAwarededProjectName = item.item.Name;
                    getProjectAwardedForcompanyProfile()
                }
            }

            var isCtrlPressed;
            var isAPressed;
            /////////////////////////////empty text box project to quote companyProfile//////////////////////////////////////////////////////
            $scope.searchAwarededProjectNameEmpty = function (searchAwarededProjectName, event) {
                debugger;
                if (searchAwarededProjectName != undefined && searchAwarededProjectName.length <= 1 && event.keyCode == 8) {

                    dataModal.Name = "";
                    dataModal.ProjectId = 0
                    getProjectAwardedForcompanyProfile()
                }
                // if key is pressed
                if (event.keyCode == 17 || isCtrlPressed) {
                    isCtrlPressed = true;
                }
                else {
                    isCtrlPressed = false;
                }

                // other key after the control
                if (isCtrlPressed) {
                    if (event.keyCode == 97 || event.keyCode == 65) {
                        isAPressed = true;

                    }
                }
                else {
                    isAPressed = false;
                }


                if (isCtrlPressed && isAPressed) {
                    if (event.keyCode == 46 || event.keyCode == 8) {
                        dataModal.Name = "";
                        dataModal.ProjectId = 0
                        getProjectAwardedForcompanyProfile()

                        isCtrlPressed = false;
                        isAPressed = false;
                    }

                }
                var text = window.getSelection().toString()
                if (text != "" && (event.keyCode == 8 || event.keyCode == 46)) {
                    dataModal.Name = "";
                    dataModal.ProjectId = 0
                    getProjectAwardedForcompanyProfile()
                }
            }


            $scope.cutSearchAwardedProjectByProjectName = function () {
                debugger
                dataModal.Name = "";
                dataModal.ProjectId = 0
                getProjectAwardedForcompanyProfile()

            }


            $scope.changePageSize = function (PageSize) {
                debugger;
                dataModal.PageSize = PageSize;
                getProjectAwardedForcompanyProfile()
            }

            $scope.pageChanged = function (PageIndex) {
                dataModal.PageIndex = PageIndex;
                getProjectAwardedForcompanyProfile()
            }

            ////////////////////////////////////------------paging code/////////////////////////////////
            $scope.pagesizeList = [
           { PageSize: 5 },
           { PageSize: 10 },
           { PageSize: 25 },
           { PageSize: 50 },

            ];

            $scope.PageSize = $scope.pagesizeList[0];




            $scope.changePageSize = function (pageSizeSelected) {
                debugger;
                dataModal.PageSize = pageSizeSelected.PageSize;;
                $scope.pageSizeSelected = pageSizeSelected.PageSize;
                getProjectAwardedForcompanyProfile()
            }

            $scope.pageChanged = function (pageIndex) {
                debugger;
                dataModal.PageIndex = pageIndex;
                getProjectAwardedForcompanyProfile()
            }


            ////////////////////////////////////------------paging code/////////////////////////////////

        })

})