﻿
define(['app'], function (app) {

    app.controller("sideMenuController", function ($scope, $rootScope, $timeout, $http, $location, sharedService, toaster, projectsFactory, usersFactory, teamFactory) {
        $scope.CompanyObject = {};
        $rootScope.CompanyObject = {};
        $rootScope.TeamsObject = {};
        // call when the controller loaded its data
        //getUserDetail();

        /*  CHange Selected Team */
        $scope.changeSelectedTeam = function (activeTeamObject) {
  
            alert();
          
            $('#globalLoader').show();
            $rootScope.activeTeam = activeTeamObject;
            $scope.activeTeam = activeTeamObject;
            $scope.teamName = activeTeamObject.TeamName;
            var Users = {};
            Users.TeamId = activeTeamObject.TeamId;
            Users.updateflag = 2;
            Users.userId = sharedService.getUserId();

            usersFactory.updateUser(Users).success(function () {

                getProjects();

                $timeout(function () {
                    $scope.$apply();
                    $('#globalLoader').hide();
                });
            });
        };

        /*  Getting projects related to Selected Team */
        function getProjects() {
            if ($scope.activeTeam.TeamId == undefined) {
                $scope.activeTeam.TeamId = sharedService.getDefaultTeamId();
            }
            projectsFactory.getProjects(sharedService.getUserId(), $scope.activeTeam.TeamId).success(function (data) {
                if (data.success) {
                    $scope.projects = data.ResponseData;
                    $timeout(function () {
                        $scope.$apply();
                        $('#globalLoader').hide();
                    });
                }
                else {
                    toaster.error("Error", data.message);
                }
            }).error(function () {
                toaster.error("Error", "Some Error Occured !");
            });
        }
        // get the user necessary detail
        function getUserDetail() {
            function hideloader() {
                //closeCentreSections();
                //$('#divMasterLoader').hide();
                $scope.selectedNavigation = "MyDashboard";
                //$('#divMyDashboard').show();
                $scope.setOverFlowY = "auto !important";
            };
            /*  Fetching User Details */
            usersFactory.getUser(sharedService.getUserId())
                       .success(function (data) {
                           if (data.success) {
                               //$scope.NotifyCount = data.ResponseData.NotificationCount == 0 ? null : data.ResponseData.NotificationCount;
                               //$scope.InboxCount = data.ResponseData.inboxCount == 0 ? null : data.ResponseData.inboxCount;
                               $scope.hideNotifyCount = data.ResponseData.NotificationCount == undefined ? 0 : data.ResponseData.NotificationCount == 0 ? false : true;
                               $scope.hideInboxCount = data.ResponseData.inboxCount == undefined ? 0 : data.ResponseData.inboxCount == 0 ? false : true;

                               $rootScope.NotificationCount = data.ResponseData.NotificationCount == undefined ? 0 : data.ResponseData.NotificationCount == null ? 0 : data.ResponseData.NotificationCount;
                               $rootScope.InboxCount = data.ResponseData.InboxCount == undefined ? 0 : data.ResponseData.InboxCount == null ? 0 : data.ResponseData.InboxCount;

                               // adding to the shared service
                               sharedService.setNotificationCount($rootScope.NotificationCount);
                               sharedService.setInboxCount($rootScope.InboxCount);

                               $scope.CompanyObject.companyId = data.ResponseData.CompanyId;
                               $scope.CompanyObject.companyName = data.ResponseData.CompanyName;

                               $rootScope.CompanyObject.companyId = data.ResponseData.CompanyId;
                               $rootScope.CompanyObject.companyName = data.ResponseData.CompanyName;


                               $scope.IsAdminLoggedIn = data.ResponseData.IsAdminLoggedIn;
                               $scope.SendNotification = data.ResponseData.SendNotificationEmail;

                               $scope.MyProfileObject = data.ResponseData;
                               $rootScope.MyProfileObject = data.ResponseData;

                               $scope.teamName = data.ResponseData.teamName;
                               $scope.TeamId = data.ResponseData.TeamId;
                               $timeout(function () {
                                   $scope.$apply();
                                   /* Fetching User Teams  */
                                   teamFactory.getLoggedInUserTeams(sharedService.getUserId()).success(function (data) {
                                       if (data.success) {
                                           $scope.TeamsObject = data.ResponseData;
                                           $scope.activeTeam = $scope.TeamsObject[0];
                                           sharedService.setDefaultTeamId($scope.activeTeam.TeamId);
                                           $rootScope.activeTeam = $scope.activeTeam;
                                           $rootScope.TeamsObject = $scope.TeamsObject;
                                           $timeout(function () {
                                               $scope.$apply();
                                               /* Getting User Projects for Selected Team */
                                               getProjects();
                                               //projectsFactory.getProjects(sharedService.getUserId(), $scope.activeTeam.TeamId)
                                               //    .success(function (data) {
                                               //        
                                               //        if (data.success) {
                                               //            $scope.projects = data.ResponseData;
                                               //        }
                                               //        else {
                                               //            toaster.error("Error", data.message);
                                               //        }
                                               //    })
                                               //    .error(function () {
                                               //        toaster.error("Error", "Some Error Occured !");
                                               //    });
                                           });
                                       }
                                       else {
                                           toaster.error("Error", data.message);
                                       }
                                   }).error(function () {
                                       toaster.error("Error", "Some Error Occured!");
                                   });
                               });
                           }
                           else {
                               toaster.error("Error", data.message);
                           }
                       })
                        .error(function () {
                            toaster.error("Error", "Some Error Occured !");
                        });
        }
        // on load
        angular.element(document).ready(function () {
            if (sharedService.getUserId() != undefined) {
                getUserDetail();
            }
        });
        //$scope.$on('loadSideMenu', function (event, args) {
        //    alert('process');
        //    getUserDetail();
        //});
        $scope.showTodoManagementSection = function (projectItem) {
            $scope.defaultProject = projectItem;
            $rootScope.defaultProject = projectItem;
            $scope.myTodo = true;
            var newLocation = '/todo/' + projectItem.ProjectId;
            $location.path(newLocation);
        }

    })
});