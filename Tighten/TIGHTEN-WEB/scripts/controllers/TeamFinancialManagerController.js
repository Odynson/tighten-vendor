﻿define(['app', 'teamFinancialManagerFactory'], function (app) {
    app.controller("teamFinancialManagerController", function ($scope, $rootScope, $timeout, $location, $stateParams, $http, sharedService, toaster, apiURL, teamFinancialManagerFactory) {
        $scope.teamFinancialManager = [];
        $scope.$emit('showMenu', []);

        $scope.getAllTeamProjectForFinancialManagerDisplay = function () {
            teamFinancialManagerFactory.getAllTeamProjectForFinancialManager(sharedService.getCompanyId(), sharedService.getUserId()).success(function (result) {
                $scope.teamFinancialManager = result.ResponseData;
                $('#globalLoader').hide();
            })
            .error(function () {
                $('#globalLoader').hide();
                toaster.error("Error", message[response.message]);
            })
        };

        $scope.getAllTeamProjectForFinancialManagerDisplay();
    });
});