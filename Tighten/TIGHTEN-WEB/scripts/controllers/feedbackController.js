﻿define(['app', 'userFeedbackFactory'], function (app) {
    app.controller("feedbackController", function ($scope, $rootScope,apiURL, $timeout, $http, sharedService, toaster, $filter, userFeedbackFactory) {
        $scope.imgURL = apiURL.imageAddress;

        $scope.UserFeedbackComment = {};
        /* show hide the right container window*/
        var closeRightSections = function () {
            $('#divToDoDescriptionSection').hide();
            $('#divEventDescriptionSection').hide();
            $('#divProjectOverview').hide();
            $('#divUserFeedbackDescriptionSection').hide();
            $('#divTeamDescriptionSection').hide();
            $('#divProjectDescriptionSection').hide();
        }

        $scope.hideRightWindowFunction = function () {
            $scope.hideRightWindow = true;
            $scope.stretchCentreWindow = true;
            $scope.selectedTodo = "";
            $scope.selectedNotification = "";
            $scope.selectedInbox = "";
            $scope.selectedUserFeedback = "";
        }
        /*End Of show hide right container window*/

        /* UserFeedbackManagement is a CLASS which has all the FUNCTIONS related to UserFeedbackManagement i.e Save feedback,get feedback etc*/
        $scope.UserFeedbackManagement = {

            /* Showing User Feedback Management Section*/
            showUserFeedbackManagementSection: function () {

                $('#globalLoader').show();
                $scope.showCenterLoader = true;
                $scope.InputSearchFeedback = "";
                //closeCentreSections();
                /* Getting User Feedbacks  */
                userFeedbackFactory.getUserFeedbacks(sharedService.getUserId())
                    .success(function (data) {
                        if (data.success) {
                            
                            $scope.UserFeedbacksObject = data.ResponseData;
                            $scope.showCenterLoader = false;
                            $('#divUserFeedbackManagementSection').show();
                            $('#globalLoader').hide();


                            $timeout(function () {
                                $scope.$apply();
                                $('#divUserFeedbackDescriptionSection').slimScroll({
                                    //position: 'left',
                                    height: '95%',
                                    railVisible: false,
                                    alwaysVisible: false
                                });
                                $('#divUserFeedbackDescriptionSection').css('width', '97%');
                            });

                        }
                        else {
                            toaster.error("Error", data.message);
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                    });
            },

            /* Showing User Feedback Modal For adding new Feedback */
            showModalUserFeedback: function () {
                $scope.userFeedbackAddUpdateModel = {};
                //$scope.userFeedbackAddUpdateModel.Priority = 1;
                $scope.userFeedbackAddUpdateModel.IsPrivate = "False";
                $scope.userFeedbackAddUpdateModel.CompanyId = $scope.MyProfileObject.CompanyId;
                $('#modalUserFeedback').modal('show');

            },


            /* saving User Feedback */
            saveUserFeedback: function (isTitleInvalid, isPriorityInvalid, userFeedbackForm) {
                
                if (isTitleInvalid || isPriorityInvalid) {
                    userFeedbackForm.Title.$dirty = true;
                    userFeedbackForm.Priority.$dirty = true;
                }
                else {
                    var model = { 'UserFeedbackAddModel': $scope.userFeedbackAddUpdateModel, 'UserId': sharedService.getUserId() };
                    $('#globalLoader').show();
                    userFeedbackFactory.insertUserFeedback(model)
                        .success(function (data) {
                            if (data.success) {
                                toaster.success("Success", data.message);
                                $('#modalUserFeedback').modal('hide');

                                /* Getting Updated User Feedbacks  */
                                userFeedbackFactory.getUserFeedbacks()
                                    .success(function (data) {
                                        if (data.success) {
                                            $scope.UserFeedbacksObject = data.ResponseData;
                                            $('#globalLoader').hide();
                                        }
                                        else {
                                            toaster.error("Error", data.message);
                                            $('#globalLoader').hide();
                                        }
                                    })
                                    .error(function () {
                                        toaster.error("Error", "Some Error Occured !");
                                        $('#globalLoader').hide();
                                    });
                            }
                            else {
                                if (data.message == undefined) {
                                    toaster.error("Error", data.errors.toString());
                                    $('#globalLoader').hide();
                                }
                                else {
                                    toaster.error("Error", data.message);
                                    $('#globalLoader').hide();
                                }
                            }
                        })
                        .error(function () {
                            toaster.error("Error", "Some Error Occured !");
                            $('#globalLoader').hide();
                        });
                }
            },

            /*  Showing User Feedback Description in third Section   */
            showUserFeedbackDescription: function (userfeedbackObject) {
                
                $scope.selectedUserFeedback = userfeedbackObject.UserFeedbackId;
                $scope.userFeedbackDescriptionObject = userfeedbackObject;
                closeRightSections();
                $scope.stretchCentreWindow = false;
                $scope.hideRightWindow = false;

                /* Fetching User Feedback Comments */
                userFeedbackFactory.getUserFeedbackComments(userfeedbackObject.UserFeedbackId)
                    .success(function (data) {
                        if (data.success) {
                            $scope.userFeedbackCommentsObject = [];
                            $scope.userFeedbackCommentsObject = data.ResponseData;
                            $('#divUserFeedbackDescriptionSection').show();
                            $('#hideRightWindow').show();
                            $scope.hideRightWindow = false;
                        }
                        else {
                            toaster.error("Error", data.message);
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                    });
            },

            /* Vote User Feedback */
            voteUserFeedback: function (UserFeedbackObject, index) {
                if ($scope.Flag) {
                    return;
                }
                $scope.Flag = true;
                if (!UserFeedbackObject.HasVoted) {
                    $scope.userFeedbackVoterUpdateModel = {};
                    $scope.userFeedbackVoterUpdateModel.Vote = true;
                    $scope.userFeedbackVoterUpdateModel.UserFeedbackId = UserFeedbackObject.UserFeedbackId;
                    var model = { 'UserFeedbackVoterUpdateModel': $scope.userFeedbackVoterUpdateModel, 'UserId': sharedService.getUserId() };
                    /* Voting User Feedback */
                    userFeedbackFactory.updateUserFeedbackVoter(model)
                        .success(function (data) {
                            if (data.success) {
                                
                                toaster.success("Success", data.message);
                                $scope.newVoterToBeAddedModel = {};
                                $scope.newVoterToBeAddedModel.VoterName = $scope.MyProfileObject.firstName + " " + $scope.MyProfileObject.lastName;
                                $scope.newVoterToBeAddedModel.VoterEmail = $scope.MyProfileObject.email;
                                $scope.newVoterToBeAddedModel.VoterProfilePhoto = $scope.MyProfileObject.profilePhoto;

                                $scope.UserFeedbacksObject[index].Voters.unshift($scope.newVoterToBeAddedModel);
                                $scope.UserFeedbacksObject[index].HasVoted = true;
                                $scope.Flag = false;
                            }
                            else {
                                toaster.error("Error", data.message);
                                $scope.Flag = false;
                            }


                        }).error(function () {
                            toaster.error("Error", "Some Error Occured !");
                            $scope.Flag = false;
                        });
                }
            },

            /*UnVote User Feedback */
            unVoteUserFeedback: function (UserFeedbackObject, index) {
                if ($scope.Flag) {
                    return;
                }
                $scope.Flag = true;
                if (UserFeedbackObject.HasVoted) {
                    $scope.userFeedbackVoterUpdateModel = {};
                    $scope.userFeedbackVoterUpdateModel.Vote = false;
                    $scope.userFeedbackVoterUpdateModel.UserFeedbackId = UserFeedbackObject.UserFeedbackId;
                    var model = { 'UserFeedbackVoterUpdateModel': $scope.userFeedbackVoterUpdateModel, 'UserId': sharedService.getUserId() }
                    /* UnVoting User Feedback */
                    userFeedbackFactory.updateUserFeedbackVoter(model)
                        .success(function (data) {
                            if (data.success) {
                                toaster.success("Success", data.message);
                                $scope.UserFeedbacksObject[index].Voters.splice(0, 1);
                                $scope.UserFeedbacksObject[index].HasVoted = false;
                                $scope.Flag = false;
                            }
                            else {
                                toaster.error("Error", data.message);
                                $scope.Flag = false;
                            }
                        })
                        .error(function () {
                            toaster.error("Error", "Some Error Occured !");
                            $scope.Flag = false;
                        });
                }
            },

            /* Saving Comment on User Feedback */
            saveCommentUserFeedback: function (UserFeedbackId ) {
                
                var Comment = $scope.UserFeedbackComment.Comment;
                $scope.userFeedbackCommentAddUpdateModel = {};
                $scope.userFeedbackCommentAddUpdateModel.UserFeedbackId = UserFeedbackId;
                //$scope.UserFeedbackComment = Comment;
                $scope.userFeedbackCommentAddUpdateModel.Comment = Comment;//$scope.UserFeedbackComment;
                $('#globalLoader').show();
                var model = { 'UserFeedbackCommentAddModel': $scope.userFeedbackCommentAddUpdateModel, 'UserId': sharedService.getUserId() };
                /* saving User Feedback Comment */
                userFeedbackFactory.insertUserFeedbackComment(model)
                    .success(function (data) {
                        
                        if (data.success) {
                            toaster.success("Success", data.message);
                            $scope.UserFeedbackComment.Comment = "";
                            $scope.newUserFeedbackCommentToBeAddedModel = {};
                            $scope.newUserFeedbackCommentToBeAddedModel.Comment = Comment;
                            $scope.newUserFeedbackCommentToBeAddedModel.CreatorName = $scope.MyProfileObject.firstName + " " + $scope.MyProfileObject.lastName;
                            $scope.newUserFeedbackCommentToBeAddedModel.CreatorProfilePhoto = $scope.MyProfileObject.profilePhoto;
                            $scope.newUserFeedbackCommentToBeAddedModel.CreatedDate = new Date();
                            $scope.userFeedbackCommentsObject.push($scope.newUserFeedbackCommentToBeAddedModel);
                            
                            $timeout(function () {
                            
                                $scope.$apply();
                            }, 100);
                            
                            $('#globalLoader').hide();
                        }
                        else {
                            if (data.message == undefined) {
                                toaster.error("Error", data.errors.toString());
                                $('#globalLoader').hide();
                                $scope.UserFeedbackComment.Comment = "";
                            }
                            else {
                                toaster.error("Error", data.message);
                                $('#globalLoader').hide();
                                $scope.UserFeedbackComment.Comment = "";
                            }
                        }
                    })
                    .error(function () {
                        toaster.error("Error", "Some Error Occured !");
                        $('#globalLoader').hide();
                    });
            },

        };


        $scope.TabKeyPressEvent = function (e, UserFeedbackId) {
            
            var UserFeedbackComment = $scope.UserFeedbackComment.Comment;
            var ENTERKEY = 13;
            if (e.shiftKey == false && e.keyCode == ENTERKEY) {
                $scope.UserFeedbackManagement.saveCommentUserFeedback(UserFeedbackId, UserFeedbackComment);
            }
            else if (e.shiftKey == true && e.keyCode == ENTERKEY) {
                //$('#txtMessage2').append("\r");
                //$scope.invitationMessageObject.NewMsg = $scope.invitationMessageObject.NewMsg + "\r";
            }
        };


        angular.element(document).ready(function () {
            if ($rootScope.loadSideMenu) {
                $scope.$emit('showMenu', []);
            }
            $scope.UserFeedbackManagement.showUserFeedbackManagementSection();

        });
    });
});