﻿
define(['app'], function (app) {

    app.controller("invitationController", function ($scope, $rootScope, $stateParams, $http, sharedService, toaster, apiURL, $location, invitationFactory, $confirm, projectsFactory) {

        if ($rootScope.loadSideMenu) {
            $scope.$emit('showMenu', []);
        }
        $scope.invitationNotificationObject = {};
        $scope.FreelancerInvitationDetailsObject = {};

        $scope.InvitaitonStatus = {
            allInvitaiton: [
                { id: 1, invitaitonType: 'ALL' },
                { id: 2, invitaitonType: 'Accepted' },
                { id: 3, invitaitonType: 'Rejected' },
                { id: 4, invitaitonType: 'Pending' }
            ]
        };

        $scope.selectedInvitaitonStatus = 1;

        $scope.showFreelancerInvitationDetails = function (item) {

            $scope.FreelancerInvitationDetailsObject = item;
            $('#modalInvitationDetail').modal('show');
        };

        $scope.acceptFreelancerInvitation = function (freelancerInvitationObject) {

            $('#globalLoader').show();
            freelancerInvitationObject.FreelancerUserId = sharedService.getUserId();
            invitationFactory.acceptInvitation(freelancerInvitationObject)
           .success(function (response) {
               $scope.$emit('showMenu', []);
               $scope.getInvitationNotification();
               toaster.success("Success", message[response.ResponseData]);
               freelancerInvitationObject.IskeyActive = false;
               //$('#modalInvitationMessage').modal('hide');
               $('#globalLoader').hide();
           })
           .error(function (response) {
               toaster.error("Error", message[response.message]);
               $('#globalLoader').hide();
           })


        };


        $scope.goToCompanyProfile = function (companyId) {
            $location.path('/company/profile/' + companyId);
        };


        //Rejecting the invitation sent by Admin
        $scope.rejectFreelancerInvitation = function (item) {
            
            $('#modalInvitationMessage').modal('hide');
            $scope.FreelancerInvitationDetailsObject = item;
            $scope.Reason = "";
            $('#modalInvitationReject').modal('show');
            $('#globalLoader').hide();
        };



        //Rejecting the invitation sent by Admin
        $scope.rejectInvitation = function (Reason, ReasonInvalid, invitationRejectForm) {

            if (ReasonInvalid) {
                invitationRejectForm.Reason.$dirty = true;
            }
            else {
                $('#modalInvitationReject').modal('hide');
                $confirm({ text: 'Are you sure you want to reject this Project Invitation ?', title: 'Reject it', ok: 'Yes', cancel: 'No' })
             .then(function () {
                 $('#globalLoader').show();
                 $scope.FreelancerInvitationDetailsObject.FreelancerUserId = sharedService.getUserId();
                 $scope.FreelancerInvitationDetailsObject.RejectionReason = Reason;

                 invitationFactory.rejectInvitation($scope.FreelancerInvitationDetailsObject)
                .success(function (response) {
                    if (response.ResponseData == "Some Error Occured !") {
                        toaster.error("Error", message[response.ResponseData]);
                        $('#modalInvitationReject').modal('hide');
                        $('#globalLoader').hide();
                    }
                    else {
                        $scope.getInvitationNotification();
                        toaster.success("Success", message[response.ResponseData]);
                        $('#modalInvitationReject').modal('hide');
                        $('#globalLoader').hide();
                    }
                })
                .error(function (response) {
                    toaster.error("Error", message[response.message]);
                    $('#globalLoader').hide();
                })

             });

            }
        };



        $scope.getInvitationNotification = function () {
            $('#globalLoader').show();
            invitationFactory.GetInvitationNotification(sharedService.getUserId(), $scope.selectedInvitaitonStatus)
           .success(function (response) {
               $scope.invitationNotificationObject = response.ResponseData;
               $('#globalLoader').hide();
           })
           .error(function (response) {
               toaster.error("Error", message[response.ResponseData]);
               $('#globalLoader').hide();
           })
        }


        angular.element(document).ready(function () {
            
            $scope.SelectedProjectId = $stateParams.pid;
            $scope.getInvitationNotification();
            $scope.UserId = sharedService.getUserId();
        })



        $scope.weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        $scope.convertTo = function (arr, key, dayWise) {
            
            var groups = [];
            for (var i = 0; l = arr.length, i < l; i++) {
                var newkey = '';
                
                    var weekday_value = $scope.weekdays[new Date(arr[i][key]).getDay()];
                    var date = new Date(arr[i][key]).toUTCString();
                    var newdate = date.substring(0, 16)
                    //arr[i][key] = [weekday_value] + ', ' + date;
                     
                    arr[i][key] = newdate;
                    //groups.push(arr);
            }
            return arr;
        };




        $scope.viewJobOfferClicked = function (item) {
            
            $('#globalLoader').show();
            $scope.currentFreelancerObject = item;

            // Having Parallel requests to get required data on job offer popup
            projectsFactory.getProject(item.ProjectId)
           .success(function (projectResponse) {
               $scope.invitationProjectObject = projectResponse.ResponseData;
           })
           .error(function (projectResponse) {
               toaster.error("Error", message["error"]);
               $('#globalLoader').hide();
           })


            invitationFactory.getFreelancerInfo(item.FreelancerInvitationId)
           .success(function (freelancerInfotResponse) {
               $scope.invitationFreelancerObject = freelancerInfotResponse.ResponseData;

               invitationFactory.getUserMessage(item.FreelancerInvitationId, sharedService.getUserId())
               .success(function (response) {
                   $scope.invitationMessageObject = [];
                   //$scope.invitationMessageObject.Messages = response.ResponseData;

                   var Messages = response.ResponseData;
                   $scope.invitationMessageObject.Messages = $scope.convertTo(Messages, 'CreatedDate', true);

                   $scope.invitationMessageObject.offerSentBy_UserId = item.SentBy;
                   $scope.invitationMessageObject.InvitationId = item.FreelancerInvitationId;
                   $scope.invitationMessageObject.isProjectExist = true;
                   $('#modalInvitationMessage').modal('show');
                   $('#globalLoader').hide();
               })
               .error(function (response) {
                   toaster.error("Error", message["error"]);
                   $('#globalLoader').hide();
               })
           })
           .error(function (freelancerInfotResponse) {
               toaster.error("Error", message["error"]);
               $('#globalLoader').hide();
           })
        }


        $scope.sendMessgae = function (NewMsg) {
            
            $('#globalLoader').show(); 
            if (NewMsg != "") {
                var asd = $scope.invitationMessageObject.NewMsg;
                var data = { UserId: $scope.invitationMessageObject.offerSentBy_UserId, Message: NewMsg, SentBy: sharedService.getUserId(), InvitationId: $scope.invitationMessageObject.InvitationId };
                invitationFactory.sendMessgae(data)
               .success(function (response) {
                   
                   invitationFactory.getUserMessage($scope.invitationMessageObject.InvitationId, sharedService.getUserId())
                   .success(function (response) {
                       //$scope.invitationMessageObject.Messages = response.ResponseData;
                       var Messages = response.ResponseData;
                       $scope.invitationMessageObject.Messages = $scope.convertTo(Messages, 'CreatedDate', true);
                       $scope.invitationMessageObject.NewMsg = "";
                       $('#globalLoader').hide();
                   })
                   .error(function (response) {
                       toaster.error("Error", message["error"]);
                       $('#globalLoader').hide();
                   })


               })
               .error(function (response) {
                   toaster.error("Error", message["error"]);
                   $('#globalLoader').hide();
               })
            }

        }


        //Download Freelancer Attachments File with original name 
        $scope.downloadFreelancerAttachments = function (freelancer) {
            
            var data = { FileName: freelancer.FileName, ActualFileName: freelancer.ActualFileName, FilePath: freelancer.FilePath }
            projectsFactory.downloadFreelancerAttachments(data)
                .success(function (data, status, headers, config) {

                    var file = new Blob([data], {
                        type: 'application/csv'
                    });
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = freelancer.ActualFileName;
                    document.body.appendChild(a);
                    a.click();
                })
                .error(function (data, status, headers, config) {
                    toaster.error("Error", message["error"]);
                })
        }


        $scope.TabKeyPressEvent = function (e) {
            
            var ENTERKEY = 13;
            if (e.shiftKey == false && e.keyCode == ENTERKEY) {
                $scope.sendMessgae($scope.invitationMessageObject.NewMsg);
            }
            else if (e.shiftKey == true && e.keyCode == ENTERKEY) {
                //$('#txtMessage2').append("\r");
                //$scope.invitationMessageObject.NewMsg = $scope.invitationMessageObject.NewMsg + "\r";
            }
        };


    });

});

