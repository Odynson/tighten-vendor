﻿define(['app'], function (app) {


    app.factory('teamCountFactory', function ($http, apiURL) {
        var teamCountDirectiveUrl = apiURL.baseAddress + 'TeamCountDirectiveAPI/';
        var Api = {
          
            getCompanyTeams: function (CompanyId, UserId) {
                return $http.get(teamCountDirectiveUrl + "CompanyTeams/" + CompanyId + "/" + UserId);
            }
        };
        return Api;
    });


    app.directive('teamCountDirective', function ($interval, sharedService, toaster, $location, teamCountFactory) {
        return {
            scope: {
                refreshinterval: "@refreshinterval",
                imgurl: "@imgurl",
                title : "@"
            },
            templateUrl: "scripts/directives/teamCountDirective/teamCountDirectiveModal.html",
            link: function (scope, element, attrs) {
               
                // calling method to get teams after a particular interval 
                //var interval = $interval(scope.getTeamCount, 60000);

                var interval = $interval(scope.getTeamCount, scope.refreshinterval);

                // imgURL is for default url showing the images
                //scope.imgURL = apiURL.imageAddress;


                scope.showOtherUserProfile = function (id) {
                    $location.path('/user/profile/' + id);
                }

                scope.showTeamDetail = function () {
                    $('#modalTeamDetail').modal('show');
                }

                scope.teamDetailClose = function () {
                    

                }


                scope.getTeamCount = function () {
                    $('#globalLoader').show();
                    teamCountFactory.getCompanyTeams(sharedService.getCompanyId(), sharedService.getUserId())
                      .success(function (data) {
                          scope.teamsObject = data.ResponseData;
                          $('#globalLoader').hide();
                      })
                      .error(function (data) {
                          $('#globalLoader').hide();
                          toaster.error("Error", "Some Error Occured !");
                      })
                }





                scope.getTeamCount();

            }
        };
    })


    
        
});