﻿define(['app'], function (app) {


    app.factory('comingtodosCountFactory', function ($http, apiURL) {
        var comingtodosCountDirectiveUrl = apiURL.baseAddress + 'ComingtodosCountDirectiveAPI/';
        var Api = {
          
            getUserComingtodos: function (CompanyId, UserId) {
                return $http.get(comingtodosCountDirectiveUrl + "GetUserComingtodos/" + UserId + "/" + CompanyId);
            }
        };
        return Api;
    });


    app.directive('comingtodosCountDirective', function ($interval, sharedService,toaster, $location, comingtodosCountFactory) {
        return {
            scope: {
                refreshinterval: "@refreshinterval",
                imgurl: "@imgurl",
                title : "@"
            },
            templateUrl: "scripts/directives/comingtodosCountDirective/comingtodosCountDirectiveModal.html",
            link: function (scope, element, attrs) {
               
                // calling method to get teams after a particular interval 
                //var interval = $interval(scope.getTeamCount, 60000);

                var interval = $interval(scope.getComingtodosCount, scope.refreshinterval);

                scope.showOtherUserProfile = function (id) {
                    $location.path('/user/profile/' + id);
                }

                scope.showComingtodosDetail = function () {
                    $('#modalTeamDetail').modal('show');
                }

                scope.comingtodosDetailClose = function () {
                    

                }


                scope.getComingtodosCount = function () {
                    debugger;
                    $('#globalLoader').show();
                    comingtodosCountFactory.getUserComingtodos(sharedService.getCompanyId(), sharedService.getUserId())
                   
                      .success(function (data) {
                          debugger;
                          scope.comingTodosObject = data.ResponseData;
                          $('#globalLoader').hide();
                      })
                      .error(function (data) {
                          $('#globalLoader').hide();
                          toaster.error("Error", "Some Error Occured !");
                      })
                }





                scope.getComingtodosCount();

            }
        };
    })


    
        
});