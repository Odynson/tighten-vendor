﻿define(['app'], function (app) {


    app.factory('overbudgetedprojectsCountFactory', function ($http, apiURL) {
        var overbudgetedprojectsCountDirectiveUrl = apiURL.baseAddress + 'OverBudgetedProjectsCountDirectiveAPI/';
        var Api = {
          
            GetUserOverBudgetedProjects: function (CompanyId, UserId) {
                return $http.get(overbudgetedprojectsCountDirectiveUrl + "GetUserOverBudgetedProjects/" + UserId + "/" + CompanyId);
            }
        };
        return Api;
    });


    app.directive('overbudgetedprojectsCountDirective', function ($interval,$filter, sharedService, toaster, $location, overbudgetedprojectsCountFactory) {
        return {
            scope: {
                refreshinterval: "@refreshinterval",
                imgurl: "@imgurl",
                title : "@"
            },
            templateUrl: "scripts/directives/overbudgetedprojectsCountDirective/overbudgetedprojectsCountDirectiveModal.html",
            link: function (scope, element, attrs) {
               
                // calling method to get teams after a particular interval 
                //var interval = $interval(scope.getTeamCount, 60000);

                var interval = $interval(scope.getOverBudgetedProjectsCount, scope.refreshinterval);

                scope.showOtherUserProfile = function (id) {
                    $location.path('/user/profile/' + id);
                }

                scope.showOverBudgetedProjectsDetail = function () {
                    $('#modalOverBudgetedProjectsDetail').modal('show');
                }

                scope.overBudgetedProjectsDetailClose = function () {
                    

                }


                scope.getOverBudgetedProjectsCount = function () {
                    $('#globalLoader').show();
                    overbudgetedprojectsCountFactory.GetUserOverBudgetedProjects(sharedService.getCompanyId(), sharedService.getUserId())
                      .success(function (data) {
                          scope.allProjectsObject = data.ResponseData;
                          scope.overBudgetedProjectsObject = $filter('filter')(scope.allProjectsObject, { 'IsTodoBudgetExceedsProjectBudget': true }, true);
                          $('#globalLoader').hide();
                      })
                      .error(function (data) {
                          $('#globalLoader').hide();
                          toaster.error("Error", "Some Error Occured !");
                      })
                }





                scope.getOverBudgetedProjectsCount();

            }
        };
    })


    
        
});