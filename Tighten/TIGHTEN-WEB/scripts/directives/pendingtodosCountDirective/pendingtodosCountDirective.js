﻿
define(['app'], function (app) {


    app.factory('pendingtodosCountFactory', function ($http, apiURL) {
        var pendingtodosCountDirectiveApiUrl = apiURL.baseAddress + 'PendingtodosCountDirectiveApi/';
        var Api = {
            getUserPendingtodos: function (UserId, CompanyId) {
                return $http.get(pendingtodosCountDirectiveApiUrl + "GetUserPendingtodos/" + UserId + "/" + CompanyId);
            },

        };

        return Api;
    });


    app.directive('pendingtodosCountDirective', function ($interval, sharedService, apiURL, toaster, $location, pendingtodosCountFactory) {
        return {
            scope: {
                refreshinterval: "@",
                imgurl: "@",
                title: "@"
            },
            templateUrl: "scripts/directives/pendingtodosCountDirective/pendingtodosCountDirectiveModal.html",
            link: function (scope, element, attrs) {

                // calling method to get teams after a particular interval 
                var interval = $interval(scope.getPendingtodosCount, scope.refreshinterval);

                // imgURL is for default url showing the images
                //scope.imgURL = apiURL.imageAddress;

                scope.showOtherUserProfile = function (id) {
                    $location.path('/user/profile/' + id);
                }

                scope.showPendingtodosDetail = function () {
                    $('#modalPendingtodosDetail').modal('show');
                }

                scope.pendingtodosDetailClose = function () {


                }


                scope.getPendingtodosCount = function () {
                    $('#globalLoader').show();
                    
                    /* Getting User Owned Projects */
                    pendingtodosCountFactory.getUserPendingtodos(sharedService.getUserId(), sharedService.getCompanyId())
                        .success(function (data) {
                            if (data.success) {

                                scope.UsersPendingtodosObject = data.ResponseData;
                                $('#globalLoader').hide();
                            }
                            else {
                                $('#globalLoader').hide();
                                toaster.error("Error", data.message);
                            }
                        })
                        .error(function () {
                            $('#globalLoader').hide();
                            toaster.error("Error", "Some Error Occured!");
                        });

                }





                scope.getPendingtodosCount();

            }
        };
    })




});