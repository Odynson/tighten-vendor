﻿define(['app'], function (app) {


    app.factory('freelancersHiredFactory', function ($http, apiURL) {
        var freelancersHiredDirectiveUrl = apiURL.baseAddress + 'FreelancersHiredDirectiveAPI/';
        var Api = {
          
            GetFreelancersHired: function (CompanyId, UserId) {
                return $http.get(freelancersHiredDirectiveUrl + "GetFreelancersHired/" + UserId + "/" + CompanyId);
            }
        };
        return Api;
    });


    app.directive('freelancersHiredCountDirective', function ($interval, sharedService, toaster, $location, freelancersHiredFactory) {
        return {
            scope: {
                refreshinterval: "@refreshinterval",
                imgurl: "@imgurl",
                title : "@"
            },
            templateUrl: "scripts/directives/freelancersHiredDirective/freelancersHiredDirectiveModal.html",
            link: function (scope, element, attrs) {
               
                // calling method to get teams after a particular interval 
                //var interval = $interval(scope.getTeamCount, 60000);

                var interval = $interval(scope.getFreelancersHired, scope.refreshinterval);

                scope.showOtherUserProfile = function (id) {
                    $location.path('/user/profile/' + id);
                }

                scope.showFreelancersHiredDetail = function () {
                    $('#modalFreelancersHired').modal('show');
                }

                scope.freelancersHiredDetailClose = function () {
                    

                }


                scope.getFreelancersHired = function () {
                    $('#globalLoader').show();
                    freelancersHiredFactory.GetFreelancersHired(sharedService.getCompanyId(), sharedService.getUserId())
                      .success(function (data) {
                          scope.freelancersHiredObject = data.ResponseData;
                          $('#globalLoader').hide();
                      })
                      .error(function (data) {
                          $('#globalLoader').hide();
                          toaster.error("Error", "Some Error Occured !");
                      })
                }





                scope.getFreelancersHired();

            }
        };
    })


    
        
});