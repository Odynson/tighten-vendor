﻿
define(['app'], function (app) {


    app.factory('expiredduedatetodosCountFactory', function ($http, apiURL) {
        var expiredduedatetodosCountDirectiveApiUrl = apiURL.baseAddress + 'ExpiredduedatetodosCountDirectiveApi/';
        var Api = {
            getUserExpiredDueDateTodos: function (UserId, CompanyId) {
                return $http.get(expiredduedatetodosCountDirectiveApiUrl + "GetUserExpiredDueDateTodos/" + UserId + "/" + CompanyId);
            },

        };

        return Api;
    });


    app.directive('expiredduedatetodosCountDirective', function ($interval, sharedService, apiURL, toaster, $location, expiredduedatetodosCountFactory) {
        return {
            scope: {
                refreshinterval: "@",
                imgurl: "@",
                title: "@"
            },
            templateUrl: "scripts/directives/expiredduedatetodosCountDirective/expiredduedatetodosCountDirectiveModal.html",
            link: function (scope, element, attrs) {

                // calling method to get teams after a particular interval 
                var interval = $interval(scope.getExpiredDueDateTodosCount, scope.refreshinterval);

                scope.showOtherUserProfile = function (id) {
                    $location.path('/user/profile/' + id);
                }

                scope.showExpiredDueDateTodosDetail = function () {
                    $('#modalExpiredDueDateTodosDetail').modal('show');
                }

                scope.expiredDueDateTodosDetailClose = function () {


                }


                scope.getExpiredDueDateTodosCount = function () {
                    $('#globalLoader').show();
                    
                    /* Getting User Owned Projects */
                    expiredduedatetodosCountFactory.getUserExpiredDueDateTodos(sharedService.getUserId(), sharedService.getCompanyId())
                        .success(function (data) {
                            if (data.success) {

                                scope.UserExpiredDueDateTodosObject = data.ResponseData;
                                $('#globalLoader').hide();
                            }
                            else {
                                $('#globalLoader').hide();
                                toaster.error("Error", data.message);
                            }
                        })
                        .error(function () {
                            $('#globalLoader').hide();
                            toaster.error("Error", "Some Error Occured!");
                        });

                }





                scope.getExpiredDueDateTodosCount();

            }
        };
    })




});