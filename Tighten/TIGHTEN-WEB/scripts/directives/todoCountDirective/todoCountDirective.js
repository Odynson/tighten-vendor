﻿
define(['app'], function (app) {


    app.factory('todoCountFactory', function ($http, apiURL) {
        var TodoCountDirectiveApiUrl = apiURL.baseAddress + 'TodoCountDirectiveApi/';
        var Api = {
            
            getUserCompletedTodos: function (UserId, CompanyId) {
                return $http.get(TodoCountDirectiveApiUrl + "GetUserCompletedTodos/" + UserId + "/" + CompanyId);
            },

        };

        return Api;
    });



    app.directive('todoCountDirective', function ($interval, sharedService,toaster, $location, todoCountFactory) {
        return {
            scope: {
                refreshinterval : "@",
                imgurl : "@",
                title : "@",
            },
            templateUrl: "scripts/directives/todoCountDirective/todoCountDirectiveModal.html",
            link: function (scope, element, attrs) {

                // calling method to get teams after a particular interval 
                var interval = $interval(scope.getCompletedTodoCount, 60000);


                scope.showOtherUserProfile = function (id) {
                    $location.path('/user/profile/' + id);
                }

                scope.showCompletedTodoDetail = function () {
                    $('#modalCompletedTodoDetail').modal('show');
                }

                scope.completedTodoDetailClose = function () {


                }


                scope.getCompletedTodoCount = function () {
                    $('#globalLoader').show();
                    
                    /* Getting User Owned Projects */
                    todoCountFactory.getUserCompletedTodos(sharedService.getUserId(), sharedService.getCompanyId())
                        .success(function (data) {
                            if (data.success) {

                                scope.completedTodoObject = data.ResponseData;
                                $('#globalLoader').hide();
                            }
                            else {
                                $('#globalLoader').hide();
                                toaster.error("Error", data.message);
                            }
                        })
                        .error(function () {
                            $('#globalLoader').hide();
                            toaster.error("Error", "Some Error Occured!");
                        });

                }





                scope.getCompletedTodoCount();

            }
        };
    })




});