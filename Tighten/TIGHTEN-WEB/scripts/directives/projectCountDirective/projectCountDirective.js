﻿
define(['app'], function (app) {


    app.factory('projectCountFactory', function ($http, apiURL) {
        var projectCountDirectiveApiUrl = apiURL.baseAddress + 'ProjectCountDirectiveApi/';
        var Api = {
            getUserProjects: function (UserId, CompanyId) {
                return $http.get(projectCountDirectiveApiUrl + "GetUserProjects/" + UserId + "/" + CompanyId);
            },

        };

        return Api;
    });


    app.directive('projectCountDirective', function ($interval, sharedService, apiURL, toaster, $location, projectCountFactory) {
        return {
            scope: {
                refreshinterval: "@",
                imgurl: "@",
                title: "@"
            },
            templateUrl: "scripts/directives/projectCountDirective/projectCountDirectiveModal.html",
            link: function (scope, element, attrs) {

                // calling method to get teams after a particular interval 
                var interval = $interval(scope.getProjectCount, scope.refreshinterval);

                // imgURL is for default url showing the images
                //scope.imgURL = apiURL.imageAddress;

                scope.showOtherUserProfile = function (id) {
                    $location.path('/user/profile/' + id);
                }

                scope.showProjectDetail = function () {
                    $('#modalProjectDetail').modal('show');
                }

                scope.projectDetailClose = function () {


                }


                scope.getProjectCount = function () {
                    $('#globalLoader').show();
                    
                    /* Getting User Owned Projects */
                    projectCountFactory.getUserProjects(sharedService.getUserId(), sharedService.getCompanyId())
                        .success(function (data) {
                            if (data.success) {

                                scope.UserOwnedProjectsObject = data.ResponseData;
                                $('#globalLoader').hide();
                            }
                            else {
                                $('#globalLoader').hide();
                                toaster.error("Error", data.message);
                            }
                        })
                        .error(function () {
                            $('#globalLoader').hide();
                            toaster.error("Error", "Some Error Occured!");
                        });

                }





                scope.getProjectCount();

            }
        };
    })




});