﻿define(['app'], function (app) {


    app.factory('topFiveTasksFactory', function ($http, apiURL) {
        var topFiveTasksDirectiveUrl = apiURL.baseAddress + 'TopFiveTasksDirectiveAPI/';
        var Api = {
          
            GetTopFiveTasks: function (CompanyId, UserId) {
                return $http.get(topFiveTasksDirectiveUrl + "GetTopFiveTasks/" + UserId + "/" + CompanyId);
            }
        };
        return Api;
    });


    app.directive('topFiveTasksDirective', function ($interval, sharedService, toaster, $location, topFiveTasksFactory) {
        return {
            scope: {
                refreshinterval: "@refreshinterval",
                imgurl: "@imgurl",
                title : "@"
            },
            templateUrl: "scripts/directives/topFiveTasksDirective/topFiveTasksDirectiveModal.html",
            link: function (scope, element, attrs) {
               
                // calling method to get teams after a particular interval 
                //var interval = $interval(scope.getTeamCount, 60000);

                var interval = $interval(scope.getTopFiveTasks, scope.refreshinterval);

                scope.showOtherUserProfile = function (id) {
                    $location.path('/user/profile/' + id);
                }

                scope.showTopFiveTasks = function () {
                    //$('#modalTopFiveTasksDetail').modal('show');
                }

                scope.topFiveTasksDetailClose = function () {
                    

                }


                scope.getTopFiveTasks = function () {
                    $('#globalLoader').show();
                    topFiveTasksFactory.GetTopFiveTasks(sharedService.getCompanyId(), sharedService.getUserId())
                      .success(function (data) {
                          scope.topFiveTasksObject = data.ResponseData;
                          $('#globalLoader').hide();
                      })
                      .error(function (data) {
                          $('#globalLoader').hide();
                          toaster.error("Error", "Some Error Occured !");
                      })
                }





                scope.getTopFiveTasks();

            }
        };
    })


    
        
});