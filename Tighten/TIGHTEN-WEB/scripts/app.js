﻿define(['ui-router', 'ngStorage', 'ngCookies', 'jquery', 'ngAnimate', 'toaster', 'selectize', 'ngSanitize', 'ui.bootstrap', 'angular-confirm', 'angular-ui-tree', 'angularFileUpload', 'multiple-select', 'calendar', 'dropbox-picker',
    'lk-google-picker', 'angularTrix', 'contextMenuApp', 'ngResource', 'autoCompleteModule', 'ngRateIt', 'ngdatatables', 'angularTreeview'], function (Selectize) {
        //window.Selectize = Selectize;
        //defining angularjs module
        var app = angular.module('app', ['ui.router', 'ngCookies', 'ngStorage', 'ngAnimate', 'toaster', 'selectize', 'ngSanitize', 'ui.bootstrap', 'angular-confirm', 'ui.tree',
            'angularFileUpload', 'isteven-multi-select', 'ui.calendar', 'dropbox-picker', 'lk-google-picker', 'ngTagsInput', 'angularTrix', 'contextMenuApp', 'ngResource', 'autoCompleteModule', 'ngRateIt', 'datatables', 'angularTreeview']);

        //global service
        app.constant('apiURL',
            {
               



                // baseAddress: "http://tightennewapi.azurewebsites.net/api/",
                //imageAddress: "http://tightennewapi.azurewebsites.net/",
                //webAddress: "http://tightennewapp.azurewebsites.net/#/",
                baseAddress: "http://localhost:55580/api/",
                imageAddress: "http://localhost:55580/",
                webAddress: "http://localhost:54485/#/"
            });

        app.constant('appInfoForAttachments',
            {
                boxClientId: "ll08q8l7lrs09eta7x1r97l22eqlbjz6",
                dropBoxClientId: "dqa3b7ve1qmmcuf",
                googleDriveClientId: "935871113692-efhdbnrlcokhsb6b8m0mc9renomurbe7.apps.googleusercontent.com",
                googleDriveApiKey: "AIzaSyCOmTTaE3qUkiZQnxjpyhvYXGU5X-J83kw",
                googleDriveappId: "935871113692"


            });

        app.constant('stripSetting', {
            stripKey: "pk_test_vlfopRnE9Tff7fQBrEPagmeR"
        })

        //manual bootstrap
        app.init = function () {
            angular.bootstrap(document, ['app']);
        };

        // checking the authentication 
        app.run(['$rootScope', '$timeout', '$location', '$cookies', '$cookieStore', '$http', 'sharedService',
            function ($rootScope, $timeout, $location, $cookies, $cookieStore, $http, sharedService) {

                // keep user logged in after page refresh
                //$rootScope.globals = JSON.parse($cookies.getObject('globals')) || {};

                $rootScope.globals = $cookieStore.get('globals') || {};

                //   (!jQuery.isEmptyObject($rootScope.globals))
                if ($rootScope.globals) {
                    var sharedServiceValue = $cookieStore.get('sharedService') || {};
                    if (!jQuery.isEmptyObject(sharedServiceValue)) {
                        sharedService.setShared(sharedServiceValue);
                        // sshow the menus   
                        $timeout(function () {
                            //$scope.$apply(); 
                            //$rootScope.$apply();
                            $rootScope.headerMenu = false;
                            $rootScope.showSideMenu = false;
                            //$('#headerMenu').show();
                            //$('#sideMenu').show();
                        });
                    }
                }
                else {
                    sharedService.setShared({});
                    $rootScope.globals = {};
                    $timeout(function () {
                        $rootScope.headerMenu = true;
                        $rootScope.showSideMenu = true;
                    });
                }
                if ($rootScope.globals.currentUser) {
                    $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
                }

                //routeChangeStart
                $rootScope.$on('$locationChangeStart', function (event, next, current) {
                    $('#progress').hide();
                    $('.modal-backdrop').remove();
                    if (next == current) {
                        $rootScope.loadSideMenu = true;
                    }
                    else {
                        $rootScope.loadSideMenu = false;
                    }


                    // redirect to login page if not logged in
                    if (($location.path() !== '/login') && ($location.path() !== '/signup') && ($location.path().search('confirmemail') !== 1) && ($location.path().search('confirmfreelancer') !== 1) && ($location.path().search('setpassword') !== 1)
                        && ($location.path().search('user/profile') !== 1) && ($location.path().search('SubscriptionRecurring') !== 1)
                        && ($location.path() !== '/vendorcompany/invitation') && ($location.path().search('registration/vendorcompany') !== 1) && (!$rootScope.globals.currentUser)) {
                        $timeout(function () {
                            $rootScope.headerMenu = true;
                            $rootScope.showSideMenu = true;
                            $rootScope.globals = {};
                        });
                        $location.path('/login');
                    }



                    if (($location.path().search('user/profile') == 1) && (jQuery.isEmptyObject(sharedServiceValue))) {

                        $timeout(function () {

                            $rootScope.headerMenu = false;
                            $rootScope.showSideMenu = false;
                            $rootScope.loadSideMenu = false;
                            //$rootScope.$emit('showHeaderMenu', []);

                            //$rootScope.globals = {};
                        });
                    }
                    //else if ($location.path() !== '/login')
                    //{
                    //    $timeout(function () {
                    //        $rootScope.headerMenu = true;
                    //        $rootScope.showSideMenu = true;
                    //    });
                    //}
                });

            }]);

        return app;
    });