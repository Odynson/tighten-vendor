﻿define(['app'], function (app) {

    app.constant('ngSortableConfig', {
        onEnd: function () {
            console.log('default onEnd()');
        }
    })
    .constant("apiURL",
        {
            baseAddress: "http://localhost:5100/api/"
        });
});