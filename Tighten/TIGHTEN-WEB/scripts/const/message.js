﻿message = {
    error: 'Some Error occured',
    successdataLoad: 'Data loaded successfully',
    saveSccessFullyComman: 'Data Save Successfully',

    resetPassword:'An Email has been sent to you with a link to reset your password ! Please follow the link and change your password.',
    wrongEmailForResetPassword: 'Oops !! Invalid Email. Please try again.',
    wrongPassword: 'Oops !! Invalid credentials. Please try again.',
    emptyPasswordField: "Please provide password",
    passwordAndConfirmPasswordNotMatched : "Password and confirm password not matched",
    userNotExist: 'Sorry, You are not a registered user with us. Kindly register yourself and start managing your work with TIGHTEN.',
    userNotActive: "You are not an active user.Please contact your company's administrator.",
    
    EmailConfirmed : " Congrats, Your email address has been successfully confirmed, You can now use your TIGHTEN account. Please go to the login page and start managing your tasks",
    EmailConfirmationError: "OOPs!! Your account is not activated. Please contact the site administrator.",
    EmailConfirmationErrorNotAllowed: "This operation is not allowed. Please contact the site administrator",
    DuplicateUser: "<b> <i> <a href='#/user/profile/##DuplicateUserId' style='color:cornflowerblue;'>  ##firstName ##lastName  </a> </i> </b> (<b><i><a href='#/user/profile/##DuplicateUserId' style='color:cornflowerblue;'>##email</a> </i> </b>) already exist in ##CompanyName.",

    DuplicateTeam: 'Team already exist with this name',
    TeamAddedSuccess: 'New team is added successfully',
    TeamUpdatedSuccess: 'Team is updated successfully',
    TeamDeletedSuccessfully: 'Team is deleted successfully',
    ProjectPresentInTeam: 'Project present in this team',

    //TeamProjectsBudgetExceed: 'Projects Budget Exceeds the Team Budget!',
    TeamProjectsBudgetExceed: 'Team budget is lesser then the budget of the project(s) added under it. Please choose a higher value',
    DuplicateProject: 'Project already exist with this name',
    DuplicateInitiative: 'Initiative already exist with this title',
    ProjectAddedSuccess: 'New project is added successfully',
    ProjectUpdatedSuccess: 'Project is updated successfully',
    ProjectDeletedSuccessfully: 'Project is deleted successfully',
    InvoicePresentInProject: 'Sorry, this project can not be deleted because there are invoice(s) associated with it.',

    DuplicateSection: 'Section already exist with this name',
    SectionAddedSuccess: 'New section is added successfully',
    SectionUpdatedSuccess: 'Section is updated successfully',
    SectionNotExist: 'Section does not exist',

    ForwardInvoiceSuccess : 'Invoice is forwarded Successfully',
    ApproveInvoiceSuccess: 'Invoice is paid Successfully',
    userDeleteSuccess: 'User deleted Successfully',
    todoAttachmentDeleteSuccess: 'Attachment deleted Successfully',
    InvitationSentToFreelancer: 'Invitation is sent Successfully',

    freelancerRegistrationSuccess: 'Registration completed Successfully',
    freelancerRegistrationKeyUsed: 'Registration key is already used',
    freelancerRegistrationDateExpires: 'Registration date already expired',
    freelancerEmailAddressAllreadyExist: 'This email address already in use',
    invitaionAccepted: 'Invitation is accepted',
    freelancerProjectAllreadyAssigned: 'Project is already assigned to freelancer',
    freelancerAllreadyAttachedToCompanyWithoutAnyProject: 'Freelancer is already attached with company with no assigned project',
    
    // For User Profile
    SelectFavProjectWarning: 'Select atleast one project',
    MaxFavProjectWarning: 'Select upto 3 favorite projects only',
    SelectTagWarning: 'Select atleast one tag',
    MaxSelectTagWarning: 'Select upto 3 Tags only',

    
    EducationDetailUpdateSuccess: 'education detail is updated Successfully!',
    educationDetailDeleted: 'User education detail is deleted Successfully!',

    professionalSaveDetailSuccess: 'User professional detail is saved Successfully!',
    professionalDetailUpdateSuccess: 'professional detail is updated Successfully!',
    professionalDetailDeleted: 'User professional detail is deleted Successfully!',


    SelectTestimonialWarning: 'Select atleast one Testimonial',

    ProjectCompleted: 'Project marked as complete!',
    ProjectNotCompleted: "Project can't be Completed!",

    //payment
    creditcardDuplicateName: 'Duplicate Card Name',
    creditcardDuplicateNumber: 'Duplicate Card Number',
    creditcardUpdateSuccessfully: 'Payment Detail Updated Successfully',




    // for Role manage screen 
    DuplicateRole: "Role is already present with this name",
    DuplicateDesignation: "Designation is already present with this name",
    DuplicateRecord: "Record is already present with this name",

    RoleAddedSuccess: "New role is added successfully !!",
    DesignationAddedSuccess: "New designation is added successfully !!",
    BothAddedSuccess: "New record is added successfully !!",

    DuplicatePriority: "Role is already present with this priority",
    DuplicateDesignationPriority: "Designation is already present with this priority",
    DuplicateRecordPriority: "Record is already present with this priority",

    RoleUpdatedSuccess: "Role is updated successfully !!",
    DesignationUpdatedSuccess: "Designation is updated successfully !!",
    RecordUpdatedSuccess: "Record is updated successfully !!",



    roleDeleteSuccess: "Role is deleted Successfully !!",


    // for Stripe Account Settings screen 
    stripeAccountVerified: "Account is updated successfully",


    // for Payment Detail screen 
    DuplicatePaymentDetail: "This Card Number is already present",
    DuplicateCardName: "This Card Name is already present",
    PaymentDetailAddedSuccess: "New Payment Detail is added Successfully !!",
    PaymentDetailUpdatedSuccess: "Payment Detail is updated Successfully !!",



    // for feedback screen 
    ProjectMarkedAsComplete: "Project marked as complete successfully!",
    ProjectManagerNotCommented: "Project Manager or Admin's feedback is necessary for each freelancer",

    FileSizeExceeds : "File size exceeds !!",
    ExtensionMisMatch: "File extension is not matched",



    // vendor registration
    InvitationToVendor: 'Invitation is sent Successfully',
    InvitationError: 'You have already sent the Invitation.',
    InvitationAccepted: 'you are now a vendor of ',
    InvitationRejected: 'you are now a vendor of',
    InvitationAlreadySend: 'Selected vender is already added as a vendor to logged in company',
    DomainError:'Entered email is not a domain email',
    VendorDetailDeletedSuccessfully: 'Vendor is deleted successfully',
    ProjectPresentInVendorDetail: 'Project present in this Vendor',

    UpdateError: 'entered email is not a domain email',
    NotValidDomainEmail: 'entered email is not a domain email',
    AlreadyExistPOC: 'entered email is already added as POC with Logged in Company',
    UpdateToVendor: 'Vendor is updated Successfully',
    //newParoject

    NewProjectCreateSuccesfully: 'Project created successfully.',
    NewProjectUpdateSuccesfully: 'Project update successfully.',
    NewInitiativeCreateSuccesfully: 'Initiative created successfully.',
    InitiativeEditSuccesfully: 'Initiative edited successfully.',
    DeleteInitiativeSuccesfully: 'Initiative deleted successfully.',
    FileDeleted:  'File remove successfully.',
    NewProjectCreatedAndEmailSent: 'Project created successfully. Email has been sent to respective vendors.',
    notSelectedVenodr: 'Please Select vendor for Project !',
    QuoteAcceptByvendorSuccess: 'Your Quote has been successfully submitted to the company.',
    VendorInvoiceSentSuccessfully: 'Invoice is sent Successfully',
    VendorInvoiceApproved: 'Invoice Approved Successfully',
    VendorInvoiceRejected: 'Invoice Approved Successfully',

    //newAccount

    NewAccountSavedSuccesfully: 'Account saved successfully.',
    DuplicateAccount: 'Account of selected team and vendor company is already exist',
    noPOCAdded: 'Please pick atleast one POC',
    CompanyAccDetailDeletedSuccessfully:'Company Account Deleted Successfully',
    CompanyAccDetailUpdatedSuccessfully: 'Company Account Updated Successfully',
    CompanyAccountNotExists: 'Company Account does not exist',
        ParentAccDetailDeletedSuccessfully: 'Parent Account Deleted Successfully',
        ParentAccDetailUpdatedSuccessfully: 'Parent Account Updated Successfully',
        ParentAccountNotExists: 'Parent Account does not exist',
    
};


