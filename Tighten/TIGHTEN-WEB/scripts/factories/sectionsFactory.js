﻿define(['app'], function (app) {
    app.factory('sectionsFactory', function ($http, apiURL) {
        var sectionUrl = apiURL.baseAddress + 'SectionsAPI';
        var Api = {
            getSections: function (ProjectId) {
                return $http.get(sectionUrl + "/" + ProjectId);
            },

            insertSection: function (section) {
                return $http.post(sectionUrl, section);
            },

            updateSection: function (section) {
                return $http.put(sectionUrl, section);
            },
            updateSectionOrder: function (section) {
                return $http.put(sectionUrl + "/" + "UpdateSectionOrder", section);

            },
            updateTodoOrder: function (section) {
                return $http.put(sectionUrl + "/" + "UpdateSectionTodos", section);

            },
            deleteSection: function (Id) {
                return $http.delete(sectionUrl + "/" + Id);
            },

            getUpdatableSections: function (Id) {
                return $http.get(sectionUrl + "/GetSections/" + Id);
            },
        };
        return Api;
    });
});
