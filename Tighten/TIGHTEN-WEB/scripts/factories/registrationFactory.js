﻿define(['app'], function (app) {

    app.factory('registrationFactory', function ($http, apiURL) {
        var registrationURL = apiURL.baseAddress + 'Account/';
        var api = {

            Registration: function (registerationData, callback) {
                //
                /* Use this for real registration
                 ----------------------------------------------*/
                return $http.post(registrationURL + 'Registration', registerationData)
                        .success(function (response) {
                            //
                            callback(response);
                        });
            },

            chkUserNameExist: function (data) {
                return $http.post(registrationURL + 'chkUserNameExist/', data);
            },

            registerInvitationVendorCompany: function (dataModal) {
                
                return $http.post(registrationURL + "RegisterInvitationVendorCompany/", dataModal);


            },

            getVendorCompanyRecord: function (registrationData) {

                return $http.post(registrationURL + "GetVendorCompanyRecordForRegistration/", registrationData);


            },

            registerVendorCompany: function (dataModal) {

                return $http.post(registrationURL + "RegistrationsVendorCompany/", dataModal);


            },


        };
        
        return api;

    });
});