﻿define(['app'], function (app) {
    app.factory('todosFactory', function ($http, apiURL) {
        var toDosApiUrl = apiURL.baseAddress + 'ToDoAPI/';
        var toDoCommentsApiUrl = apiURL.baseAddress + 'ToDoCommentsAPI/';
            var Api = {
                getMyTodosWithSections: function (userId) {
                    return $http.get(toDosApiUrl + 'getMyTodos/' + userId);
                },
                getTodosWithSections: function (projectId,userId) {
                    return $http.get(toDosApiUrl + "TodosWithSections/" + projectId + "/" + userId);
                },
                getTodosWithSectionsForAllProjects: function (CompanyId) {
                    return $http.get(toDosApiUrl + "TodosWithSectionsForAllProjects/" + CompanyId);
                },
                getTodo: function (todoId,userId) {
                    return $http.get(toDosApiUrl + "getTodo/" + todoId + "/" + userId);
                },
                getCompletedToDos: function (Id) {
                    return $http.get(toDosApiUrl + "/getCompletedToDos" + "/" + Id);
                },
                getToDosInProgress: function () {
                    return $http.get(toDosApiUrl + "/getToDosInProgress");
                },
                getPendingToDos: function () {
                    return $http.get(toDosApiUrl + "/getPendingToDos");
                },
                getSingleToDo: function (Id) {
                    return $http.get(toDosApiUrl + "/GetSingleToDo" + "/" + Id);
                },

                getMyUnassignedToDos: function () {
                    return $http.get(toDosApiUrl + "/GetMyUnassignedToDos");
                },
                getMyAssignedToDos: function () {
                    return $http.get(toDosApiUrl + "/GetMyAssignedToDos");
                },
                getMyPersonalAssignedToDos: function () {
                    return $http.get(toDosApiUrl + "/GetMyPersonalAssignedToDos");
                },
                getMyInboxToDos: function () {
                    return $http.get(toDosApiUrl + "/GetMyInboxToDos");
                },
                getMyInboxArchivedToDos: function () {
                    return $http.get(toDosApiUrl + "/GetMyInboxArchived");
                },
                getMyArchivedToDos: function () {
                    return $http.get(toDosApiUrl + "/GetMyArchivedToDos");
                },
                insertTodo: function (todo) {
                    return $http.post(toDosApiUrl, todo);
                },
                updateTodo: function (data) {
                    return $http.put(toDosApiUrl, data);
                },
                deleteTodo: function (Id) {
                    return $http.delete(toDosApiUrl + "/" + Id);
                },
                getToDoComments: function (todoId,userId) {
                    return $http.get(toDoCommentsApiUrl + "/" + todoId+"/"+userId);
                },
                insertTodoComment: function (todo) {
                    return $http.post(toDoCommentsApiUrl, todo);
                },
                deleteTodoComment: function (Id) {
                    return $http.delete(toDoCommentsApiUrl + "/" + Id);
                },
                getAssigneeUsers: function (userId) {
                    return $http.get(toDosApiUrl + "getAssigneeUsers" + "/"+userId);
                },
                getLoggedTime: function (todoId, userId) {
                    return $http.get(toDosApiUrl + "getLoggedTime/" + todoId + "/" + userId);
                },
                getInProgressTask: function () {
                    return $http.get(toDosApiUrl + "getLoggedTime/" + todoId + "/" + userId);
                },
                getTodoDetailForNotification: function (TodoId) {
                    return $http.get(toDosApiUrl + "getTodoDetailForNotification/" + TodoId )
                },
                getTaskTypeForCreatingTodo: function (TodoId) {
                    return $http.get(toDosApiUrl + "getTaskTypeForCreatingTodo/" +  TodoId)
                },
                getTodoTags: function (TaskTypeIds,TodoId) {
                    return $http.get(toDosApiUrl + "getTodoTags/" + TaskTypeIds + "/" + TodoId)
                },
                saveTodoDescription: function (model) {
                    return $http.post(toDosApiUrl + "SaveTodoDescription/", model);
                },
                updateTodoName: function (model) {
                    return $http.post(toDosApiUrl + "UpdateTodoName/", model);
                },
                updateTodoReporter : function (model) {
                    return $http.post(toDosApiUrl + "UpdateTodoReporter/", model);
                },
                updateTodoDeadlineDate: function (model) {
                    return $http.put(toDosApiUrl + "UpdateTodoDeadlineDate/", model);
                },
                updateTodoEstimatedTime: function (model) {
                    return $http.put(toDosApiUrl + "UpdateTodoEstimatedTime/", model);
                },
                updateTodoTaskType: function (model) {
                    return $http.put(toDosApiUrl + "UpdateTodoTaskType/", model);
                },
                

            };
            return Api;
        });
    });