﻿define(['app'], function (app) {
    app.factory('userMessagesFactory', function ($http, apiURL) {
        var userMessagesUrl = apiURL.baseAddress + 'Messages/';

        var Api = {
            getUsersMessages: function (reciverId, senderId) {
                return $http.get(userMessagesUrl + 'GetUsersMessages/' + reciverId + '/' + senderId);
            },
            getSenderUsers: function (userId) {
                return $http.get(userMessagesUrl + 'GetSenderUsers/' + userId);
            },
            getMessagesCount: function (userId) {
                return $http.get(userMessagesUrl + 'GetMessagesCount/' + userId);
            },
            insertUserMessages: function (message) {
                return $http.post(userMessagesUrl + 'InsertUserMessages', message);
            },
            getUpdateMessageIsRead: function (message) {
                return $http.post(userMessagesUrl + 'GetAndUpdateMessageIsReadStatus', message);
            },
            updateMessageIsReadStatus: function (message) {
                return $http.post(userMessagesUrl + 'UpdateMessageIsReadStatus', message);
            },
            getAllSenders: function (senderId,companyId, uid) {
                return $http.get(userMessagesUrl + 'GetAllSenders/' + senderId + "/" + companyId + "/" + uid);
            },
        };
        return Api;
    });
});
