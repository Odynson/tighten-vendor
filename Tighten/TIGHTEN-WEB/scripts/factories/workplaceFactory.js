﻿app.factory('workplaceFactory', ['$http', function ($http) {
    var workplaceurl = WebDomainPath + '/api/WorkplacesAPI';
    var Api = {
        getWorkplaces: function () {
            return $http.get(workplaceurl);
        },

        getWorkplace: function (Id) {
            return $http.get(workplaceurl + "/" + Id);
        },
        insertWorkplace: function (workplace) {
            return $http.post(workplaceurl, workplace);
        },
        updateWorkplace: function (workplace) {
            return $http.put(workplaceurl, workplace);
        },

    };

    return Api;
}]);