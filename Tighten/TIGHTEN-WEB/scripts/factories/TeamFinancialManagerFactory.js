﻿define(['app'], function (app) {
    app.factory('teamFinancialManagerFactory', function ($http, apiURL) {

        var sectionUrl = apiURL.baseAddress + 'teamFinancialManagerApi';

        var Api = {
            getAllTeamProjectForFinancialManager: function (CompanyId, UserId) {
                return $http.get(sectionUrl + "/" + CompanyId + "/" + UserId);
            }
        };

        return Api;
    });
});
