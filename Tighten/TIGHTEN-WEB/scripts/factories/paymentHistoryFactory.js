﻿
define(['app'], function (app) {
    app.factory('paymentHistoryFactory', function ($http, apiURL) {
        var paymentHistoryApiUrl = apiURL.baseAddress + 'PaymentHistoryApi';
        var Api = {

            getpaymentHistory: function (UserId, isFreelancer, TeamId, CompanyId, subscriptionType, ProjectId, FreelancerUserId) {
                return $http.get(paymentHistoryApiUrl + "/" + UserId + "/" + isFreelancer + "/" + TeamId + "/" + CompanyId + "/" + subscriptionType + "/" + ProjectId + "/" + FreelancerUserId);
            },
            getFreelancersForSelectedProject: function (ProjectId, CompanyId) {
                return $http.get(paymentHistoryApiUrl + "/GetFreelancersForSelectedProject/" + ProjectId + "/" + CompanyId);
            },

            
        };
        return Api;
    });
});
