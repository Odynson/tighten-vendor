﻿define(['app'], function (app) {

    app.factory('setPasswordFactory', function ($http, apiURL) {
        var ApiURL = apiURL.baseAddress + 'Account/';
        return {
            setPassword: function (model, callback) {
                /* Use this for real registration
                 ----------------------------------------------*/
                $http.post(ApiURL + 'ResetPassword', model)
                    .success(function (response) {
                        callback(response);
                    });
            },
            // Use this process when user signup from linkedIn
            setPasswordFromUserProfile: function (model, callback) {
                $http.post(ApiURL + 'SetPasswordForLinkedInUser', model)
                    .success(function (response) {
                        callback(response);
                    });
            },

            chkLinkExpiry: function (model, callback) {
                $http.post(ApiURL + 'chkPasswordLinkExpiry', model)
                    .success(function (response) {
                        if (response.success == false) {
                            callback(response);
                        }
                    });
            }
            

        }
    });
});