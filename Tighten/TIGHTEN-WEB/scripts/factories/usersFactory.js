﻿define(['app'], function (app) {
    app.factory('usersFactory', function ($http, apiURL) {
        var userurl = apiURL.baseAddress + 'UsersAPI/';
        var Api = {
            getUsers: function (userId, checkEmailConfirmed, CompanyId) {
                return $http.get(userurl + "getUsers/" + userId + "/" + checkEmailConfirmed + "/" + CompanyId);
            },
            getFreelancers: function (userId, checkEmailConfirmed, CompanyId) {
                return $http.get(userurl + "getFreelancers/" + userId + "/" + checkEmailConfirmed + "/" + CompanyId);
            },
            getActiveUsersCount: function (CompanyId) {
                return $http.get(userurl + "getActiveUsersCount/" + CompanyId);
            },
            
            getUser: function (userId, CompanyId) {
                return $http.get(userurl + userId + "/" + CompanyId);
            },
            getUserloginUserCardDetail: function (userId, CompanyId) {
                return $http.get(userurl + "UserCardInfo/" + userId + "/" + CompanyId);
            },
            isUserCardInfoExist: function (userId, CompanyId) {
                return $http.get(userurl + "IsUserCardInfoExist/" + userId + "/" + CompanyId);
            },
            insertUser: function (user) {
                return $http.post(userurl, user);
            },
            updateUser: function (user) {
                return $http.post(userurl + 'updateDefaultTeam', user);
            },
            updateUserProfile: function (user) {
                return $http.post(userurl + 'updateUserProfile', user);
            },
            updateUserRole: function (userModel) {
                return $http.put(userurl + "UpdateUserRole", userModel);
            },
            deleteUser: function (userId) {
                return $http.delete(userurl + userId);
            },
            GetAllProjectsOfCompany: function (objForAllProjectsOfCompany) {
                return $http.post(userurl + "GetAllProjects", objForAllProjectsOfCompany);
            },
            insertFreelancer: function (model) {
                return $http.post(userurl + "insertFreelancer/", model);
            },
            getCompanyAndFreelancerName: function (invitationType, EmailOrHandle, CompanyId) {
                return $http.get(userurl + "GetCompanyAndFreelancerName/" + invitationType + "/" + EmailOrHandle + "/" + CompanyId);
            },
            updateUserFirstName: function (userModel) {
                return $http.put(userurl + "UpdateUserFirstName", userModel);
            },
            updateUserLastName: function (userModel) {
                return $http.put(userurl + "UpdateUserLastName", userModel);
            },
            updateUserAddress: function (userModel) {
                return $http.put(userurl + "UpdateUserAddress", userModel);
            },
            updateUserPhoneNo: function (userModel) {
                return $http.put(userurl + "UpdateUserPhoneNo", userModel);
            },
            updateUserDesignation: function (userModel) {
                return $http.put(userurl + "UpdateUserDesignation", userModel);
            },
            updateUserAboutMe: function (userModel) {
                return $http.put(userurl + "UpdateUserAboutMe", userModel);
            },
            updateUserGender: function (userModel) {
                return $http.put(userurl + "UpdateUserGender", userModel);
            },

            updateUserDateOfBirth: function (userModel) {
                return $http.put(userurl + "UpdateUserDateOfBirth", userModel);
            },
            checkIfCompanyIsVendor: function (CompanyId) {
                return $http.get(userurl + "CheckIfCompanyIsVendor/" + CompanyId);
            },
            updateGeneralCompanyToVendorCompany: function (userId, CompanyId) {
                return $http.get(userurl + "UpdateGeneralCompanyToVendorCompany/" + userId + "/" + CompanyId);
            },
            getUsersForTeam: function (CompanyId) {
                return $http.get(userurl + "GetUsersForTeam/" + CompanyId );
            }

        };
        return Api;
    });
});