﻿define(['app'], function (app) {
    app.factory('clientFeedbackFactory', function ($http, apiURL) {
        var clientFeedbackUrl = apiURL.baseAddress + 'ClientFeedbackApi/';
        var Api = {

            getUserClientFeedbacks: function (userId) {
                return $http.get(clientFeedbackUrl+userId);
            },

            saveClientFeedback: function (feedbackModel) {
                return $http.post(clientFeedbackUrl, feedbackModel);
            }

        };
        return Api;
    });
});