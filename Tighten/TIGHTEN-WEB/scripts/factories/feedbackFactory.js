﻿
define(['app'], function (app) {
    app.factory('feedbackFactory', function ($http, apiURL) {
        var url = apiURL.baseAddress + "FeedbackApi/";
        var Api = {
            getProjects: function (CompanyId,UserId) {
                return $http.get(url + CompanyId + "/" + UserId);
            },
            showAllFeedbacksOfProject: function (ProjectId, UserId) {
                return $http.get(url + "ShowAllFeedbacksOfProject/" + ProjectId + "/" + UserId);
            },
            showOnProfile: function (model) {
                return $http.post(url + "ShowOnProfile" , model);
            },



        };
        return Api;
    });
});