﻿define(['app'], function (app) {
    app.factory('companiesFactory', function ($http, apiURL) {
        var companiesApiUrl = apiURL.baseAddress + 'CompaniesApi';
        var Api = {

            updateCompanyDetails: function (companiesObject) {
                return $http.put(companiesApiUrl, companiesObject);
            },
            getCompanyDetails: function (companyId) {
                return $http.get(companiesApiUrl + "/" + companyId);
            },
            clearCompanyLogo: function (companyId) {

                return $http.delete(companiesApiUrl + "/" + companyId)
            },
            updateCompanyName: function (companiesObject) {
                return $http.put(companiesApiUrl + '/UpdateCompanyName', companiesObject);
            },
            updateCompanyWebAddress: function (companiesObject) {
                return $http.put(companiesApiUrl + '/UpdateCompanyWebAddress', companiesObject);
            },
            updateCompanyPhoneNo: function (companiesObject) {
                return $http.put(companiesApiUrl + '/UpdateCompanyPhoneNo', companiesObject);
            },
            updateCompanyFax: function (companiesObject) {
                return $http.put(companiesApiUrl + '/UpdateCompanyFax', companiesObject);
            },
            updateCompanyEmail: function (companiesObject) {
                return $http.put(companiesApiUrl + '/UpdateCompanyEmail', companiesObject);
            },
            updateCompanyAddress: function (companiesObject) {
                return $http.put(companiesApiUrl + '/UpdateCompanyAddress', companiesObject);
            },
            //updateCompanyDescription: function (companiesObject) {
            //    return $http.put(companiesApiUrl + '/UpdateCompanyDescription', companiesObject);
            //},
            updateCompanyDescription: function (companiesObject) {
                return $http.put(companiesApiUrl + '/UpdateCompanyDescription', companiesObject);
            },
            





        };
        return Api;
    });
});