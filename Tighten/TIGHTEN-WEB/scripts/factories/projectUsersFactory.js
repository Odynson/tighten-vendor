﻿define(['app'], function (app) {
    app.factory('projectUsersFactory', function ($http, apiURL) {

        var userurl = apiURL.baseAddress + '/API/ProjectUsersApi';
        var Api = {
            getProjectMembers: function (ProjectId) {
                return $http.get(userurl + "/" + ProjectId);
            },
            getNonProjectUsers: function (Id) {
                return $http.get(userurl + "/getNonProjectUsers/" + Id);
            },
            addProjectUser: function (user) {
                return $http.post(userurl, user);
            },

            deleteProjectUser: function (Id) {
                return $http.delete(userurl + "/" + Id);
            },
        };

        return Api;
    });
});