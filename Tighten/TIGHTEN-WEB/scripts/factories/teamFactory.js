﻿
define(['app'], function (app) {
    app.factory('teamFactory', function ($http, apiURL) {
        var teamurl = apiURL.baseAddress + 'TeamAPI/';
        var Api = {
            getLoggedInUserTeams: function (userId, CompanyId) {
                return $http.get(teamurl + 'getTeamByUserId/' + userId + "/" + CompanyId);
            },
            getUserTeams: function (userId, CompanyId, isOtherUser) {
                return $http.get(teamurl + "UserTeams/" + userId + "/" + CompanyId + "/" + isOtherUser);
            },
            getTeam: function (teamId, userId) {
                return $http.get(teamurl + "?teamId=" + teamId + "&userId=" + userId);
            },
            getCompanyTeams: function (CompanyId, UserId, PageIndex, PageSizeSelected) {
                
                return $http.get(teamurl + "CompanyTeams/" + CompanyId + "/" + UserId + "/" + PageIndex + "/" + PageSizeSelected);
            },
            insertTeam: function (obj) {
                return $http.post(teamurl, obj);
            },
            updateTeam: function (team) {
                return $http.put(teamurl, team);
            },
            deleteTeam: function (teamId) {
               // alert(teamurl + teamId);
                return $http.delete(teamurl + teamId);
            },

            getAllDesignation: function () {
                return $http.get(teamurl + "GetAllDesignations");
            },
            getAllTeams: function (CompanyId, UserId) {
                return $http.get(teamurl + "GetAllTeams/" + CompanyId + "/" + UserId);
            },
            getTeamMembersByTeamId: function (CompanyId, TeamId) {
                return $http.get(teamurl + "GetTeamMembersByTeamId/" + CompanyId + "/" + TeamId);
            },
            CompanyNestedTeamsChart: function (CompanyId, UserId, PageIndex, PageSizeSelected,ParentTeamId) {

                return $http.get(teamurl + "CompanyNestedTeamsChart/" + CompanyId + "/" + UserId + "/" + PageIndex + "/" + PageSizeSelected + "/" + ParentTeamId);
            },
        };
        return Api;
    });
});