﻿define(['app'], function (app) {

    app.factory('userManagementFactory', function ($http, apiURL) {

        var VendorUserManagementApiUrl = apiURL.baseAddress + 'UserManagementVendorAdminProfileApi/';

        var Api = {

            getUserAndResourceVendorAdminProfile: function (VendorCompanyId) {

                return $http.get(VendorUserManagementApiUrl + "GetUserAndResourceVendorAdminProfile/" + VendorCompanyId);

            },
            getExistingUserRoleVendorAdminProfile: function (VendorCompanyId) {

                return $http.get(VendorUserManagementApiUrl + "GetExistingUserRoleVendorAdminProfile/" + VendorCompanyId);

            },
            getResourceVendorAdminProfile: function (item) {
                return $http.post(VendorUserManagementApiUrl + "GetResourceVendorAdminProfile", item);

            },
            saveExistingUser: function (saveExistingUserRole) {

                return $http.post(VendorUserManagementApiUrl + "SaveExistingUserAdminProfile", saveExistingUserRole);

            }
            ,
            inviteResourceAsVendor: function (vendorInvitationModel) {

                return $http.post(VendorUserManagementApiUrl + "InviteResourceAsVendor", vendorInvitationModel);

            },
            activeInactiveUserVendorAdminProfile: function (item) {

                return $http.post(VendorUserManagementApiUrl + "ActiveInactiveUserVendorAdminProfile", item);

            },
            getUserForVendorAdminProfile: function (VendorCompanyId, PageIndex, PageSizeSelected) {

                return $http.get(VendorUserManagementApiUrl + "GetUserForVendorAdminProfile/" + VendorCompanyId + '/' + PageIndex + '/' + PageSizeSelected);

            },
            getResourceForVendorAdminProfile: function (VendorCompanyId, PageIndex, PageSizeSelected) {

                return $http.get(VendorUserManagementApiUrl + "GetResourcesForVendorAdminProfile/" + VendorCompanyId + '/' + PageIndex + '/' + PageSizeSelected);

            },
            getAllExistingInternalUsers: function (CompanyId, PageIndex, PageSizeSelected) {

                return $http.get(VendorUserManagementApiUrl + "GetAllExistingInternalUsers/" + CompanyId + '/' + PageIndex + '/' + PageSizeSelected);

            },
            activeInactiveInternalUsersVendorAdminProfile: function (item) {

                return $http.post(VendorUserManagementApiUrl + "ActiveInactiveInternalUsersVendorAdminProfile", item);

            },

        }
        return Api;
    })
})