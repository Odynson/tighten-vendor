﻿

define(['app'], function (app) {

    app.factory('vendorAdminProfileFactory', function ($http, apiURL) {

        var VendorAdminApiUrl = apiURL.baseAddress + 'VendorAdminProfileApi/';

        var Api = {

            getInvitationToVendor: function (dataModal) {

                return $http.post(VendorAdminApiUrl + "GetInvitationToVendor/", dataModal);

            },

            getCompanyForInvitaionDrop: function (VendorCompanyId) {

                return $http.get(VendorAdminApiUrl + "GetCompanyForInvitaionDropDown/" + VendorCompanyId);

            },
            getPocsforDrops: function (VendorCompanyId) {

                return $http.get(VendorAdminApiUrl + "GetPocsforDrops/" + VendorCompanyId)

            },
            getCompanyForProjectToQuoteDrop: function (VendorCompanyId) {
                return $http.get(VendorAdminApiUrl + "GetCompanyForFilterVendorAdminProfile/" + VendorCompanyId);
            },
            getProjectToQuoteListVendorAdminProfile: function (dataModel) {
                return $http.post(VendorAdminApiUrl + "GetProjectToQuoteListVendorAdminProfile/", dataModel);
            },
            getProjectToQuoteViewDetailAdminProfile: function (dataModel) {

                return $http.post(VendorAdminApiUrl + "GetProjectToQuoteViewDetailAdminProfile/", dataModel);

            },
            getCompanyForProjectAwardedAdminProfileDrop: function (VendorCompanyId) {
                return $http.get(VendorAdminApiUrl + "GetCompanyForFilterVendorAdminProfile/" + VendorCompanyId);
            },
            getAwardedProjectListAdminProfile: function (dataModel) {

                return $http.post(VendorAdminApiUrl + "GetAwardedProjectListAdminProfile/", dataModel);

            },

            getAwardedProjectViewDetailVendorAdminProfile: function (ProjectId, VendorId) {

                return $http.get(VendorAdminApiUrl + "GetAwardedProjectViewDetailVendorAdminProfile/"+  ProjectId  + '/'+ VendorId);

            },

            getProjectCompletionFeedBackVendorAdminProfile: function (ProjectId, VendorId) {

                return $http.get(VendorAdminApiUrl + "GetProjectCompletionFeedBackVendorAdminProfile/" + ProjectId + '/' + VendorId);

            },
            getAllInvoiceListVendorAdminProfile: function (dataModel) {

                return $http.post(VendorAdminApiUrl + "GetAllInvoiceListVendorAdminProfile/", dataModel);

        },
            getCompanyInvoiceVendorAdminProfileDrop: function (VendorCompanyId) {
                return $http.get(VendorAdminApiUrl + "GetCompanyForFilterVendorAdminProfile/" + VendorCompanyId)
            },

            previewInvoiceVendorAdminProfile: function (InvoiceId) {
                return $http.get(VendorAdminApiUrl + "InvoicePreviewVendorAdminProfile/" + InvoiceId);
            },
            getAllInvoiceListVendorAdminProfileOutstanding: function (dataModel) {

                return $http.post(VendorAdminApiUrl + "GetAllInvoiceListVendorAdminProfileOutstanding/", dataModel);

            },
            getAllInvoiceListVendorAdminProfilePaid: function (dataModel) {

                return $http.post(VendorAdminApiUrl + "GetAllInvoiceListVendorAdminProfilePaid/", dataModel);

            },
            getAllInvoiceListVendorAdminProfileRejected: function (dataModel) {

                return $http.post(VendorAdminApiUrl + "GetAllInvoiceListVendorAdminProfileRejected/", dataModel);

            },
        }
        return Api;

    })

})