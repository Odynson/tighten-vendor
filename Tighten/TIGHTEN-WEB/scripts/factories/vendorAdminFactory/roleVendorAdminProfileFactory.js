﻿define(['app'],function () {
  
    
    app.factory('roleVendorAdminProfileFactory', function ($http, apiURL) {


        var VendorAdminApiUrl = apiURL.baseAddress + 'RoleManageVendorAdminProfileApi/';


        var Api = {


            getAllRolesForVendorAdminProfile: function (VendorCompanyId) {

                return $http.get(VendorAdminApiUrl + VendorCompanyId)

            },

            insertRoleVendorAdminProfile: function (obj) {
                return $http.post(VendorAdminApiUrl, obj);
            },
            updateRoleVendorAdminProfile: function (obj) {
                return $http.put(VendorAdminApiUrl, obj);
            },
            deleteRoleVendorAdminProfile: function (RoleId) {
                return $http.delete(VendorAdminApiUrl + RoleId);
            },
        }

        return Api;

    })



})