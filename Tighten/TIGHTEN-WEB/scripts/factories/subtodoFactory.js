﻿define(['app'], function (app) {
    app.factory('subtodoFactory', function ($http, apiURL) {
        var subtodourl = apiURL.baseAddress + '/api/SubToDoAPI';
        var Api = {
            getSubToDos: function (todoId) {
                return $http.get(subtodourl + "/" + todoId);
            },

            insertSubToDo: function (object) {
                return $http.post(subtodourl, object);
            },
            updateSubToDo: function (object) {
                return $http.put(subtodourl, object);
            },
            deleteSubToDo: function (subtodoId) {
                return $http.delete(subtodourl + "/" + subtodoId);
            },

        };

        return Api;
    });
});