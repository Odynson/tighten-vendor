﻿

define(['app'], function (app) {
    app.factory('userProfileFactory', function ($http, apiURL) {

        var userProfileUrl = apiURL.baseAddress + 'UserProfileAPI/';
        var Api = {
            getUserEducationDetail: function (UserId) {
                return $http.get(userProfileUrl + UserId);
            },
            getUserProfessionalDetail: function (UserId) {
                return $http.get(userProfileUrl + "getUserProfessionalDetail/" + UserId);
            },
            saveUserProfessionalDetail: function (ProfessionalDetailObject) {
                return $http.post(userProfileUrl + "saveUserProfessionalDetail/", ProfessionalDetailObject);
            },
            updateUserProfessionalDetail: function (ProfessionalDetailObject) {
                return $http.put(userProfileUrl + "updateUserProfessionalDetail/", ProfessionalDetailObject);
            },
            saveUserEducationDetail: function (EducationDetailObject) {
                return $http.post(userProfileUrl + "saveUserEducationDetail/", EducationDetailObject);
            },
            updateUserEducationDetail: function (EducationDetailObject) {
                return $http.put(userProfileUrl + "updateUserEducationDetail/", EducationDetailObject);
            },
            deleteUserEducationDetail: function (EducationDetailObjectId) {
                return $http.delete(userProfileUrl + "deleteUserEducationDetail/"+ EducationDetailObjectId);
            },

            deleteUserProfessionalDetail: function (ProfessionalDetailObjectId) {
                return $http.delete(userProfileUrl + "deleteUserProfessionalDetail/"+ ProfessionalDetailObjectId);
            },

            updateUserHourlyRate: function (UserId, newHourlyRate) {
                return $http.get(userProfileUrl + "updateUserHourlyRate/"+ UserId + "/" + newHourlyRate);
            },
            updateUserAvailabilty: function (UserId, AvailableHours) {
                return $http.get(userProfileUrl + "updateUserAvailabilty/" + UserId + "/" + AvailableHours);
            },
            getUserAvailabilty: function (UserId) {
                return $http.get(userProfileUrl + "getUserAvailabilty/" + UserId );
            },

            getUserFavouriteProjects: function (UserId) {
                return $http.get(userProfileUrl + "getUserFavouriteProjects/" + UserId );
            },
            saveUserFavouriteProjects: function (data) {
                return $http.post(userProfileUrl + "saveUserFavouriteProjects/", data);
            },

            getUserStatistics: function (UserId, CurrentUserId) {
                return $http.get(userProfileUrl + "getUserStatistics/" + UserId + "/" + CurrentUserId);
            },
            getUserEfficiency: function (UserId,CurrentUserId) {
                return $http.get(userProfileUrl + "getUserEfficiency/" + UserId + "/" + CurrentUserId);
            },

            getUserTags: function (UserId) {
                return $http.get(userProfileUrl + "getUserTags/" + UserId);
            },
            getUserSkillSetTags: function (UserId) {
                return $http.get(userProfileUrl + "getUserSkillSetTags/" + UserId);
            },

            saveUserTags: function (data) {
                return $http.post(userProfileUrl + "saveUserTags", data);
            },
            getTagsForProfessionalDetails: function (UserId) {
                return $http.get(userProfileUrl + "getTagsForProfessionalDetails" +"/"+  UserId);
            },
            getUserEfficiencyForStrongAndWeekTechnology: function (UserId) {
                return $http.get(userProfileUrl + "getUserEfficiencyForStrongAndWeekTechnology" + "/" + UserId);
            },
            getUserAvgRate: function (UserId) {
                return $http.get(userProfileUrl + "getUserAvgRate" + "/" + UserId);
            },
            getUserTeamAndProjectCount: function (UserId) {
                return $http.get(userProfileUrl + "getUserTeamAndProjectCount" + "/" + UserId);
            },
            getUserTestimonials: function (UserId) {
                return $http.get(userProfileUrl + "getUserTestimonials" + "/" + UserId);
            },
            getUserFeedbacks: function (UserId) {
                return $http.get(userProfileUrl + "getUserFeedbacks" + "/" + UserId);
            },
            saveUserTestimonials: function (data) {
                return $http.post(userProfileUrl + "saveUserTestimonials/", data);
            },
            changeUserProfile: function (data) {
                return $http.post(userProfileUrl + "changeUserProfile/", data);
            },
            getOtherUserProfileStatus: function (UserId) {
                return $http.get(userProfileUrl + "getOtherUserProfileStatus" + "/" + UserId);
            },
            getEducationalStreams: function (UserId) {
                return $http.get(userProfileUrl + "getEducationalStreams" + "/" + UserId);
            },
            getUserEmailFromUserId: function (UserId) {
                return $http.get(userProfileUrl + "getUserEmailFromUserId" + "/" + UserId);
            },

           
        };

        return Api;
    });
});
