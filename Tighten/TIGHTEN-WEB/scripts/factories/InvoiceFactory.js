﻿

define(['app'], function (app) {
    app.factory('invoiceFactory', function ($http, apiURL) {

        var sectionUrl = apiURL.baseAddress + 'InvoiceApi';

        var Api = {
            getAllInvoicesForUser: function (data) {
                return $http.post(sectionUrl, data);
            },
            getInvoiceDetailForUser: function (data) {
                return $http.post(sectionUrl + "/GetInvoiceDetail", data);
            },
            approveInvoice: function (data) {
                return $http.put(sectionUrl, data);
            },
            forwardInvoice: function (InvoiceItem) {
                return $http.post(sectionUrl + "/ForwardInvoice", InvoiceItem);
            },
            getProjectMembersForUser: function (data) {
                return $http.post(sectionUrl + "/GetProjectMembersForUser", data);
            },
            getAllFreelancerForAdmin: function (CompanyId) {
                return $http.get(sectionUrl + "/"  + CompanyId);
            },
            getCardDetailForPayment: function (PaymentDetailId, CompanyId, InvoiceId) {
                return $http.get(sectionUrl + "/GetCardDetailForPayment/" + PaymentDetailId + "/" + CompanyId + "/" + InvoiceId);
            },
            getAllCardsForPaymentDetail: function (CompanyId) {
                return $http.get(sectionUrl + "/GetAllCardsForPaymentDetail/" + CompanyId );
            },
            onlyApproveInvoice: function (data) {
                return $http.put(sectionUrl + "/OnlyApproveInvoice", data);
            },
            rejectInvoice: function (data) {
                return $http.put(sectionUrl + "/RejectInvoice", data);
            },
    

            

        };

        return Api;
    });
});



