﻿//app.constant('app.urls', {
//    project: WebDomainPath + '/api/ProjectsAPI',

//})
define(['app'], function (app) {
    app.factory('projectsFactory', function ($http, apiURL) {
        var projecturl = apiURL.baseAddress + "ProjectsApi/";
        var Api = {
            getProjects: function (userId, teamId, CompanyId, IsFreelancer) {
                return $http.get(projecturl + "getProjectsUsingTeamId/" + userId + "/" + teamId + "/" + CompanyId + "/" + IsFreelancer);
            },

            getProject: function (Id) {
                return $http.get(projecturl + "GetProject/" + Id);
            },

            getMyProjectsWithTeams: function (userId) {
                return $http.get(projecturl + userId);
            },

            getProjectsForSelectedTeam: function (TeamId, CompanyId) {
                return $http.get(projecturl + "/" + "GetProjectsForSelectedTeam/" + TeamId + "/" + CompanyId);
            },

            getUserProjects: function (userId, CompanyId) {
                return $http.get(projecturl + "UserProjects/" + userId + "/" + CompanyId);
            },

            getUserOwnedProjects: function (teamId, userId, RoleId, CompanyId) {
                return $http.get(projecturl + "UserOwnedProjects/" + teamId + "/" + userId + "/" + RoleId + "/" + CompanyId);
            },

            insertProject: function (project) {
                return $http.post(projecturl, project);
            },

            updateProject: function (project) {
                return $http.put(projecturl, project);
            },

            deleteProject: function (Id, CompanyId, IsFreelancer) {
                return $http.delete(projecturl + Id + "/" + CompanyId + "/" + IsFreelancer);
            },
            projectComplete: function (Id) {
                return $http.get(projecturl + "ProjectComplete/" + Id);
            },
            insertFeedback: function (NewFeedbackObj) {
                return $http.post(projecturl + "InsertFeedback/", NewFeedbackObj);
            },
            getAllActiveFreelancerOfProject: function (ProjectId) {
                return $http.get(projecturl + "GetAllFreelancerOfProject/" + ProjectId);
            },
            GetAllProjectsOfCompany: function (CompanyId, TeamId) {
                return $http.get(projecturl + "GetAllProjectsOfCompany/" + CompanyId + "/" + TeamId);
            },
            saveProjectOptionsForFreelancer: function (model) {
                return $http.post(projecturl + "SaveProjectOptionsForFreelancer", model);
            },
            getFreelancerDetails: function (UserId, projectId) {

                return $http.get(projecturl + "GetFreelancerDetails/" + UserId + "/" + projectId);
            },
            deleteAttachmentFileFromProject: function (projectId) {
                return $http.get(projecturl + "DeleteAttachmentFileFromProject/" + projectId);
            },
            downloadAttachmentFileFromProject: function (projectId) {
                return $http.get(projecturl + "DownloadAttachmentFileFromProject/" + projectId);
            },
            getInvitedFreelamcersOnlyNotAccepted: function (projectId) {
                return $http.get(projecturl + "GetInvitedFreelamcersOnlyNotAccepted/" + projectId);
            },
            getInternalUserDetails: function (UserId, projectId) {
                return $http.get(projecturl + "GetInternalUserDetails/" + UserId + "/" + projectId);
            },
            downloadProjectDocument: function (projectId) {
                return $http.get(projecturl + "DownloadProjectDocument/" + projectId)
            },
            downloadFreelancerAttachments: function (data) {
                return $http.post(projecturl + "DownloadFreelancerAttachments", data)
            },
            createNewProject: function (model) {
                return $http.post(projecturl + "CreateNewProject", model);
            },

            /////////////////////////vendor code/////////////////

            getProjectVendor: function (CompanyId, ProjectId) {
                return $http.get(projecturl + "GetProjectVendor/" + CompanyId + '/' + ProjectId);
            },

            /////////////////////////getting projectList on company profile/////////////////

            getQuotedProjectList: function (dataModal) {

                return $http.post(projecturl + "GetQuotedProjectList/", dataModal)
            },

            getQuotedSubmitbyVendor: function (ProjectId) {

                return $http.get(projecturl + "GetQuotedSubmitbyVendor/" + ProjectId)
            },

            getProjectQuotedByVendor: function (CompanyId, ProjectId, VendorId) {
                return $http.get(projecturl + "GetProjectQuotedByVendor/" + CompanyId + "/" + ProjectId + "/" + VendorId)

            },

            saveReasonAcceptOrRejct: function (data) {
                return $http.post(projecturl + "saveReasonAcceptOrRejectQuote", data)

            },
            getProjectAwardedForcompanyProfile: function (dataModal) {

                return $http.post(projecturl + "GetProjectAwardedForcompanyProfile/", dataModal)
            }
            ,
            getProjectAwardedViewDetailCompanyProfile: function (ProjectId, VendorId,CompanyId) {
                return $http.get(projecturl + "GetProjectAwardedViewDetailCompanyProfile/" + ProjectId + "/" + VendorId + "/" + CompanyId)

            },
            getProjectforCompletion: function (ProjectId, CompanyId,VendorId) {
                return $http.get(projecturl + "GetVendorProjectforCompletion/" + ProjectId + "/" + CompanyId + '/' + VendorId)
            },
            completionProject: function (data) {
                return $http.post(projecturl + "CompletionVendorProject",data)
            },
            getVendorListForDrop: function (CompanyId) {
                return $http.get(projecturl + "GetVendorListForDrop" + '/' + CompanyId)
            },
            getCustomControl: function (CompanyId) {
                return $http.get(projecturl + "GetCustomControlProjectCreation/" + CompanyId)


            },
            getProjectVendorByCompIdId: function (CompanyId, AccId) {
                return $http.get(projecturl + "GetProjectVendorByCompIdId/" + CompanyId + "/" + AccId)


            },
            getApprovedProjectByUserId: function (UserId) {
                return $http.get(projecturl + "GetApprovedProjectByUserId/" + UserId)


            },
            getProjectMilestonesByProjectId: function (ProjectId) {
                return $http.get(projecturl + "GetProjectMilestonesByProjectId/" + ProjectId)


            },
            getApprovedBillableProjectByUserId: function (UserId) {
                return $http.get(projecturl + "GetApprovedBillableProjectByUserId/" + UserId)


            },
            GetProjectsOfCompany: function (CompanyId) {
                return $http.get(projecturl + "GetProjectsOfCompany/" + CompanyId);
            },


    };

    return Api;
});
});