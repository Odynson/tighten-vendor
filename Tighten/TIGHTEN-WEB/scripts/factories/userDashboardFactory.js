﻿define(['app'], function (app) {
    app.factory('userDashboardFactory', function ($http,apiURL) {
        var userDashboardApiUrl = apiURL.baseAddress + 'UserDashboardApi/';
        var teamUrl = apiURL.baseAddress + 'TeamApi/';
        var Api = {
            getUserUpcomingTodos: function (userId) {
                return $http.get(userDashboardApiUrl + userId);
            },

            getUserUpcomingEvents: function (userId) {
                return $http.get(userDashboardApiUrl + "getUserUpcomingEvents/" + userId);
            },

            getUserTLine: function (userId) {
                return $http.get(userDashboardApiUrl + "getUserTLine/" + userId);
            },

            getNonTeamUsers: function (Id) {
                return $http.get(teamUrl + "getNonTeamUsers/" + Id);
            },

            getCompanyTeams: function (companyId) {
                return $http.get(teamUrl + "CompanyTeams/" + companyId);
            },

            getUserTeams: function (userId) {
                return $http.get(teamurl + "UserTeams/" + userId);
            },

            insertTeam: function (team) {
                return $http.post(teamUrl, team);
            },

            updateTeam: function (team) {
                return $http.put(teamUrl, team);
            },
            getAllPackages: function (CompanyId) {
                debugger;
                return $http.get(userDashboardApiUrl + "GetAllPackages/" + CompanyId);
            },
            upgradePackage: function (PackageObject) {
                return $http.post(userDashboardApiUrl + "UpgradePackage", PackageObject);
            },

            // For DashBoard Directives
            getUserProjects: function (UserId, CompanyId) {
                return $http.get(userDashboardApiUrl + "GetUserProjects/" + UserId + "/" + CompanyId);
            },
            getUserCompletedTodos: function (UserId, CompanyId) {
                return $http.get(userDashboardApiUrl + "GetUserCompletedTodos/" + UserId + "/" + CompanyId);
            },

        };

        return Api;
    });
});