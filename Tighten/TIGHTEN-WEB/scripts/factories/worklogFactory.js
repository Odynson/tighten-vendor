﻿//app.constant('app.urls', {
//    project: WebDomainPath + '/api/ProjectsAPI',

//})
define(['app'], function (app) {
    app.factory('worklogFactory', function ($http, apiURL) {
        var worklogurl = apiURL.baseAddress + "WorkLogApi/";
        var Api = {
            getWorkLogList: function (userId) {
                return $http.get(worklogurl + "GetWorkLogList/" + userId);
            },
            saveWorkLog: function (model) {
                return $http.post(worklogurl + "SaveWorkLog", model);
            },
            
            editWorkLog: function (model) {
                return $http.post(worklogurl + "EditWorkLog", model);
            },
        getWorkLogById: function (Id,UserId) {
            return $http.get(worklogurl + "GetWorkLogById/" + Id + "/" + UserId);
        },
        deleteWorkLogById: function (Id) {
            return $http.get(worklogurl + "DeleteWorkLogById/" + Id);
        },
        getWorkLogEnteryList: function (workLogId) {
            return $http.get(worklogurl + "GetWorkLogEnteryList/" + workLogId);
        },
        saveWorkLogEntry: function (model) {
            return $http.post(worklogurl + "SaveWorkLogEntry", model);
            },
        editWorkLogEntry: function (model) {
            return $http.post(worklogurl + "EditWorkLogEntry", model);
        },
        getWorkLogEntryById: function (Id) {
            return $http.get(worklogurl + "GetWorkLogEntryById/" + Id);
        },
        deleteWorkLogEntryById: function (Id) {
            return $http.get(worklogurl + "DeleteWorkLogEntryById/" + Id);
        },
        GetWorkLogListByProjectFromTo: function (UserId,ProjectId,FromDate,ToDate) {
            return $http.get(worklogurl + "GetWorkLogListByProjectFromTo/" + UserId+"/"+ProjectId+"/"+FromDate+"/"+ToDate);
        },
        };

        return Api;
    });
});