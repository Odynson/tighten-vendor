﻿define(['app'], function (app) {
    app.factory('followersFactory', function ($http, apiURL) {
        var follower = apiURL.baseAddress + 'FollowerAPI';
        var Api = {
            getFollowers: function (Id) {
                return $http.get(follower + "/" + Id);
            },
            getMyFollowedToDos: function () {
                return $http.get(follower);
            },
            getUsers: function (Id) {
                return $http.get(follower + "/GetUsers" + "/" + Id);
            },
            insertFollower: function (followerObject) {
                return $http.post(follower, followerObject);
            },
            updateFollowers: function (followerObject) {
                return $http.put(follower, followerObject);
            },

            deleteFollower: function (Id) {
                return $http.delete(follower + "/" + Id);
            },
        };
        return Api;
    });
});