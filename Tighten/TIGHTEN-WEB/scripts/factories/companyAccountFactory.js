﻿
define(["app"], function (app) {

    app.factory('companyAccountFactory', function ($http, apiURL) {

        var companyAccountApiUrl = apiURL.baseAddress + "CompanyAccountApi/";

        var Api = {


            getCompanyAccountList: function (CompanyId) {
                return $http.get(companyAccountApiUrl + "GetCompanyAccountList/" + CompanyId);

            },
            getVendorList: function (CompanyId) {
                return $http.get(companyAccountApiUrl + "GetVendorList/" + CompanyId);

            },
            GetVendorPOCList: function (CompanyId,VendorId) {
                return $http.get(companyAccountApiUrl + "GetVendorPOCList/" + CompanyId + "/" + VendorId);

            },
             DownloadDocAttachments: function (AccId) {
                return $http.get(companyAccountApiUrl + "DownloadDocs/" + AccId);

            },
             GetCompanyAccount: function (AccId) {
                 //alert(companyAccountApiUrl + "GetCompanyAccount/" + AccId);
                return $http.get(companyAccountApiUrl + "GetCompanyAccount/" + AccId);

            },
            deleteCompanyAccountDetail: function (Id) {
                // alert(teamurl + teamId);
                return $http.delete(companyAccountApiUrl + Id);
            },
            updateCompanyAccountDetail: function (companyAccount) {
                return $http.put(companyAccountApiUrl, companyAccount);
            },
            GetSingleAttachmentownload: function (attachId) {
                //alert(companyAccountApiUrl + "GetCompanyAccount/" + AccId);
                return $http.get(companyAccountApiUrl + "GetSingleAttachmentownload/" + attachId);

            },
            GetAttachedDocumentDetail: function (attachId) {
             
                return $http.get(companyAccountApiUrl + "GetAttachedDocumentDetail/" + attachId);

            },
            getAccountDetailByVendorId: function (VendorId) {

                return $http.get(companyAccountApiUrl + "getAccountDetailByVendorId/" + VendorId);

            },
            getAccountDetailByCompanyId: function (CompanyId,VendorId) {

                return $http.get(companyAccountApiUrl + "getAccountDetailByCompanyId/" + CompanyId + "/" + VendorId);

            },
            getAccountDetailById: function (Id) {

                return $http.get(companyAccountApiUrl + "getAccountDetailById/" + Id);

            },
            getMyEnterprisesByCompanyName: function (dataModel) {

                return $http.post(companyAccountApiUrl + "GetMyEnterpriseByCompanyName/", dataModel)

            },
            GetCompanyAccByCompanyId: function (CompanyId) {

                return $http.get(companyAccountApiUrl + "GetCompanyAccByCompanyId/" + CompanyId);

            },
        }

        return Api;

    })


})