﻿define(['app'], function (app) {
    app
        .service('sharedService', function () {
            var property = {};
            var freelancerInfo = {};      // used to set and get freelancer info 
            var internalPermission = {};      // used to set and get internalPermission info for  current user

            return {
                getShared: function () {
                    return property;
                },
                getUserId: function () {
                    return property.userId;
                },
                getRoleId: function () {
                    return property.RoleId;
                },
                setRoleId: function (RoleId) {
                    //alert(RoleId);
                    property.RoleId = RoleId;
                },
                getCompanyId: function () {
                    return property.CompanyId;
                },
                setCompanyId: function (CompanyId) {
                    property.CompanyId = CompanyId;
                },
                getIsFreelancer: function () {
                    return property.isFreelancer;
                },
                getNameId: function () {
                    return property.firstName;
                },
                getLastNameId: function () {
                    return property.lastName;
                },
                getEmailId: function () {
                    return property.email;
                },
                getDefaultTeamId: function () {
                    return property.TeamId;
                },
                setDefaultTeamId: function (teamId) {
                    property.TeamId = teamId;
                },
                setShared: function (value) {
                    property = value;
                },
                getNotificationCount: function () {
                    return property.NotificationCount;
                },
                setNotificationCount: function (value) {
                    property.NotificationCount = value;
                },
                getNewMessagesCount: function () {
                    return property.NewMessagesCount;
                },
                setNewMessagesCount: function (value) {
                    property.NewMessagesCount = value;
                },
                setVendorId: function (value) {
                    property.VendorId = value;
                },
                getVendorId: function () {
                    return property.VendorId;
                },
                getDefaultProfilePic: function () {
                    return property.originalProfilePhoto;
                },
                getInboxCount: function () {
                    return property.InboxCount;
                },
                setInboxCount: function (value) {
                    property.InboxCount = value;
                },
                setFreelancerInfoInShared: function (value) {
                    freelancerInfo = value;
                },
                getFreelancerInfoInShared: function () {
                    return freelancerInfo;
                },
                getIsSubscriptionsEnds: function () {
                    return property.IsSubscriptionsEnds;
                },
                setInternalPermissionInShared: function (value) {
                    internalPermission = value;
                },
                getInternalPermissionInShared: function () {
                    return internalPermission;
                },
                setIsLoggedInFirstTimeByLinkedIn: function (value) {
                    return property.IsLoggedInFirstTimeByLinkedIn = value;
                },
                getIsLoggedInFirstTimeByLinkedIn: function () {
                    return property.IsLoggedInFirstTimeByLinkedIn;
                },

                //$rootScope.RoleId = response.ResponseData.RoleId;
                //$rootScope.CompanyId = response.ResponseData.CompanyId;
                //$rootScope.Token = response.ResponseData.Token;
                //$rootScope.email = response.ResponseData.email;
                //$rootScope.firstName = response.ResponseData.firstName;
                //$rootScope.userId = response.ResponseData.userId;
            };
        });
});