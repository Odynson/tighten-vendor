﻿
define(["app"], function (app) {

    app.factory('vendorFactory', function ($http, apiURL) {

        var vendorApiUrl = apiURL.baseAddress + "VendorApi/";

        var Api = {


            getCompanyVendorList: function (CompanyId) {
                return $http.get(vendorApiUrl + "GetCompanyVendorList/" + CompanyId);

            },

            getQuotedProjectList: function (CompanyId) {

                return $http.get(vendorApiUrl + "GetQuotedProjectList/" + CompanyId)
            },

            inviteVendor: function (data) {
                return $http.post(vendorApiUrl, data)
            },
            getInvitaionFromCompany: function (dataModel) {
                return $http.post(vendorApiUrl + "GetInvitaionFromCompany/", dataModel);

            },
            acceptRejectInvitaionAsVendor: function (data) {
                return $http.post(vendorApiUrl + "AcceptRejectInvitaionAsVendor", data)

            },

            getMyVendor: function (dataModel) {
             
                return $http.post(vendorApiUrl + "GetMyVendor/", dataModel)

            },
            getProjectAndVendorDetail: function (ProjectId, CompanyId) {
                return $http.get(vendorApiUrl + "GetProjectAndVendorDetail/" + ProjectId + "/" + CompanyId)

            },

            ///////////////////////////////////////vendor view detail start///////////////////////////////////////////////////////

            getProjectTOQuote: function (dataModal) {
                return $http.post(vendorApiUrl + "GetProjectTOQuote/", dataModal)

            },

            getProjectDetailForVendor: function (dataModal) {
                return $http.post(vendorApiUrl + "GetProjectDetailForVendor/", dataModal)

            },
            ///////////////////////////////////////vendor view detail End///////////////////////////////////////////////////////

            getVendorResourceRole: function () {

                return $http.get(vendorApiUrl + "GetVendorResourceRole")

            },

            saveQuoteAcceptByvendor: function (data) {

                return $http.post(vendorApiUrl + "SaveQuoteAcceptByvendor", data)

            },

            editSaveQuoteAcceptByvendor: function (data) {
                return $http.post(vendorApiUrl + "EditSaveQuoteAcceptByvendor", data)
            },
            saveRejectQuoteByVendor: function (data) {
                return $http.post(vendorApiUrl + "RejectQuoteByVendor", data)
            },
            //////////////////////////////////////getProjectAwardedViewDetail////////////////////////////////////////////////////
            getProjectAwardedViewDetail: function (ProjectId, VendorId) {
                return $http.get(vendorApiUrl + "GetAwardedProjectViewDetail/" + ProjectId + "/" + VendorId)

            },
            ////////////////////////////////////for Invoice raise screein///////////////////////////////////////////////////
            getProjectForInvoiceRaise: function (ProjectId, VendorId) {
                return $http.get(vendorApiUrl + "GetSpecificProjectForInvoiceRaise/" + ProjectId + "/" + VendorId)
            },

            getMilestoneforProject: function (Id) {
                return $http.get(vendorApiUrl + "GetMilestoneforProject/" + Id)
            },
            getCompanyVendorDropList: function (Id) {
                return $http.get(vendorApiUrl + "GetCompanyVendorDropList/" + Id)
            },
            getCompanyForDropFilter: function (CompanyId) {
                return $http.get(vendorApiUrl + "GetCompanyForDrop/" + CompanyId);

            },
            getVendorDetail: function (CompanyId, VendorId) {
                return $http.get(vendorApiUrl + "GetVendorDetail/" + CompanyId + "/" + VendorId);

            },

            getVendorCompanyPoc: function (CompanyId, VendorId) {
                return $http.get(vendorApiUrl + "GetVendorCompanyPoc/" + CompanyId);

            },


            ////////////new functionality to invite new user//////////

            GetNewUserRole: function (CompanyId) {

                return $http.get(vendorApiUrl + "GetNewUserRole/" + CompanyId)

            },

            inviteUser: function (data) {
                return $http.post(vendorApiUrl + "InviteUser/", data)
            },

            GetAllNewUsers: function (CompanyId, UserId) {
                return $http.get(vendorApiUrl + "GetAllNewUsers/" + CompanyId + "/" + UserId);
            },
            GetCompanyEmailForInviteNewUser: function (CompanyName) {

                return $http.get(vendorApiUrl + "GetCompanyEmailForInviteNewUser/" + CompanyName)

            },
            updateExistingInternalUsers: function (data) {

                return $http.post(vendorApiUrl + "UpdateExistingInternalUsers", data);

            },
            GetCompaniesForVenderCompanyName: function () {

                return $http.get(vendorApiUrl + "GetCompaniesForVenderCompanyName");

            },
            getSelectedVendorDetail: function ( VendorCompanyId) {
                return $http.get(vendorApiUrl + "GetSelectedVendorDetail/" + VendorCompanyId);

            },
            deleteVendorDetail: function (Id) {
                // alert(teamurl + teamId);
                return $http.delete(vendorApiUrl + Id);
            },
            getSelectedVendorDetailById: function (CVId) {
                return $http.get(vendorApiUrl + "getSelectedVendorDetailById/" + CVId);

            },
            updateVendor: function (dataModel) {
                return $http.post(vendorApiUrl + "updateVendor/", dataModel);

            },
            ValidationEmailForPOC: function (pocEmail, VendorCompanyId) {
               
                return $http.get(vendorApiUrl + "ValidationEmailForPOC/" +pocEmail+"/"+ VendorCompanyId);

            },
            getMyEnterpriseCompanies: function (CompanyId) {

                return $http.get(vendorApiUrl + "GetGeneralCompanyListOfVendor/"+ CompanyId)

            },
        }

        return Api;

    })


})