﻿define(['app'], function (app) {
    app.factory('transactionsFactory', function ($http, apiURL) {
        var transactionsUrl = apiURL.baseAddress + 'transactionsAPI/';
        var Api = {
            insertTransaction: function (model) {
                return $http.post(transactionsUrl + 'insertTransactions', model);
            },
            getTransactions: function (SubscriptionID) {
                return $http.get(transactionsUrl + 'GetTransactions/' + SubscriptionID);
            },
        }
        return Api;
    });
});