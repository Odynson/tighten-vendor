﻿

define(['app'], function (app) {
    app.factory('rolePermissionFactory', function ($http, apiURL) {
        var rolePermissionApiUrl = apiURL.baseAddress + 'RolePermissionApi';
        var Api = {

            getAllRolesForCompany: function (CompanyId) {
                return $http.get(rolePermissionApiUrl + "/" + CompanyId);
            },
            getPermissionForSelectedRole: function (RoleId,CompanyId) {
                return $http.get(rolePermissionApiUrl + "/GetPermissionForSelectedRole/" + RoleId + "/" + CompanyId);
            },
            savePermissionsForSelectedRole: function (model) {
                return $http.post(rolePermissionApiUrl, model);
            },
            getAllInternalPermissionsForPermission: function (MenuId, RoleId, CompanyId) {
                return $http.get(rolePermissionApiUrl + "/GetAllInternalPermissionsForPermission/" + MenuId + "/" + RoleId + "/" + CompanyId);
            },
            saveInternalPermissions: function (model) {
                return $http.post(rolePermissionApiUrl + "/SaveInternalPermissions", model);
            },
            getInternalPermissionsForCurrentUser: function (RoleId, CompanyId) {
                return $http.get(rolePermissionApiUrl + "/GetInternalPermissionsForCurrentUser/" + RoleId + "/" + CompanyId);
            },



        };
        return Api;
    });
});

