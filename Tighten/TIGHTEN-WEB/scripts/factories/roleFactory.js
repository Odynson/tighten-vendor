﻿//app.constant('app.urls', {
//    project: WebDomainPath + '/api/ProjectsAPI',

//})
define(['app'], function (app) {
    app.factory('roleFactory', function ($http, apiURL) {
        var url = apiURL.baseAddress + "Role/";
        var Api = {
            getRoles: function (CompanyId, RoleId) {
            
                return $http.get(url + "GetRoles" + "/" + CompanyId + "/" + RoleId);
            },

            showSideMenuRolewise: function (RoleId, UserId, CompanyId) {
                //alert(RoleId + " " + UserId + " " + CompanyId);
              //  RoleId = 0;
                return $http.get(url + RoleId + "/" + UserId + "/" + CompanyId);
            },
            showSideMenuForVendorCompany: function (RoleId, UserId, CompanyId) {

                return $http.get(url +"showSideMenuForVendorCompany/"+ RoleId + "/" + UserId + "/" + CompanyId);
            },

        };
        return Api;
    });
}); 