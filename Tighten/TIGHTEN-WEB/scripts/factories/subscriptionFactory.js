﻿define(['app'], function (app) {
    app.factory('subscriptionFactory', function ($http, apiURL) {
        var subscriptionsUrl = apiURL.baseAddress + 'subscriptionsAPI/';
        var Api = {
            insertSubscription: function (model) {
                return $http.post(subscriptionsUrl + 'insertSubscription', model);
            },
            GetUserSubscriptions: function (UserId, CompanyId) {
                return $http.get(subscriptionsUrl + 'GetUserSubscriptions/' + UserId + "/" + CompanyId);
            },
            SubsctriptionPayment: function (SubsctriptionPaymentModel) {
                return $http.post(subscriptionsUrl + 'SubsctriptionPayment', SubsctriptionPaymentModel);
            },
        }
        return Api;
    });
});