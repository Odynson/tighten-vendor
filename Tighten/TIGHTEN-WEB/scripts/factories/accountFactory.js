﻿define(['app'], function (app) {
    app.factory('accountFactory', function ($http, apiURL) {

        var AccountApiUrl = apiURL.baseAddress + 'api/AccountApi';
        var Api = {
            Logout: function (Id) {
                return $http.post("Account/Logout");
            },

            ChangePasswordFunction: function (object) {
                return $http.post(apiURL.baseAddress + "Account/ChangePassword", object);
            }
        };

        return Api;
    });
});