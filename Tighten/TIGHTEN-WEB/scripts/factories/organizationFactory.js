﻿define(['app'], function (app) {

    app.factory("organizationFactory", function ($http, apiURL) {

        var organizationUrl = apiURL.baseAddress + "OrganizationApi/";
        var apiObject = {
            getAllOrganizations: function (UserId) {
                return $http.get(organizationUrl + UserId);
            },
            getOrganization: function (UserId, CompanyId) {
                return $http.get(organizationUrl + "GetOrganization/" + UserId + "/" + CompanyId);
            },
            getOrganizationTeams: function (CompanyId, UserId, PageIndex, PageSizeSelected, ParentTeamId) {
            
                return $http.get(apiURL.baseAddress+"TeamApi/CompanyTeamsChart/"+CompanyId+"/"+UserId+"/"+PageIndex+"/"+PageSizeSelected+"/"+ParentTeamId);
            },
             CompanyNestedTeamsChart: function (CompanyId, UserId, PageIndex, PageSizeSelected,ParentTeamId) {

                 return $http.get(apiURL.baseAddress + "TeamApi/CompanyNestedTeamsChart/" + CompanyId + "/" + UserId + "/" + PageIndex + "/" + PageSizeSelected + "/" + ParentTeamId);
            },
        };

        return apiObject;


    });

});