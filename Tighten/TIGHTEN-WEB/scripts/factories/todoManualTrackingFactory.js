﻿define(['app'], function (app) {
    app.factory('todoManualTrackingFactory', function ($http, apiURL) {
        var toDoManualApiUrl = apiURL.baseAddress + 'TodoManualTime/';
        var toDosApiUrl = apiURL.baseAddress + 'ToDos/';
       
            var Api = {
                getMyTodos: function (userId,RoleId) {
                    return $http.get(toDoManualApiUrl + 'getMyTodos/' + userId + "/" + RoleId );
                },
                
                insertTodo: function (todo) {
                    return $http.post(toDoManualApiUrl, todo);
                },
                // this is for the manual adding the time to the PMS todos
                updateTodo: function (data) {
                    return $http.put(toDoManualApiUrl, data);
                },
                deleteTodo: function (Id) {
                    return $http.delete(toDosApiUrl + "/" + Id);
                }
            };
            return Api;
        });
    });