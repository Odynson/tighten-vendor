﻿
define(['app'], function (app) {
    app.factory('projectFeedbackFactory', function ($http, apiURL) {
        var projectFeedbackApiUrl = apiURL.baseAddress + 'ProjectFeedbackApi';
        var Api = {

            getProjectByProjectId: function (ProjectId,UserId,RoleId) {
                return $http.get(projectFeedbackApiUrl + "/" + ProjectId + "/" + UserId + "/" + RoleId);
            },
            getUserRolePriority: function (ProjectId, UserId, RoleId) {
                return $http.get(projectFeedbackApiUrl + "/GetUserRolePriority/" + ProjectId + "/" + UserId + "/" + RoleId);
            },
            addUpdateFeedback: function (model) {
                return $http.post(projectFeedbackApiUrl , model);
            },
            saveProjectRemarks: function (model) {
                return $http.post(projectFeedbackApiUrl + "/SaveProjectRemarks", model);
            },
            markAsComplete: function (model) {
                return $http.post(projectFeedbackApiUrl + "/MarkAsComplete", model);
            },


        };
        return Api;
    });
});
