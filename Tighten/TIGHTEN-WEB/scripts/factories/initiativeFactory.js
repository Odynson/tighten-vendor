﻿
define(["app"], function (app) {

    app.factory('initiativeFactory', function ($http, apiURL) {

        var InitiativeApiUrl = apiURL.baseAddress + "InitiativeApi/";

        var Api = {


            getInitiativeList: function (CompanyId) {
                return $http.get(InitiativeApiUrl + "GetInitiativeList/" + CompanyId);

            },
            GetVendorProjectInitiativeList: function (InitiativeId) {
                return $http.get(InitiativeApiUrl + "GetVendorProjectInitiativeList/" + InitiativeId);

            },
            getVendorList: function (CompanyId) {
                return $http.get(InitiativeApiUrl + "GetVendorList/" + CompanyId);

            },
            DeleteVendorProjectInitiativeFromList: function (Id) {
                return $http.get(InitiativeApiUrl + "DeleteVendorProjectInitiativeFromList/" + Id);

            },
            DeleteInitiative: function (Id) {
                return $http.get(InitiativeApiUrl + "DeleteInitiative/" + Id);

            },
            GetInitiativeById: function (InitiativeId) {
                return $http.get(InitiativeApiUrl + "GetInitiativeById/" + InitiativeId);

            },
            GetVendorPOCList: function (CompanyId,VendorId) {
                return $http.get(InitiativeApiUrl + "GetVendorPOCList/" + CompanyId + "/" + VendorId);

            },
             DownloadDocAttachments: function (AccId) {
                return $http.get(InitiativeApiUrl + "DownloadDocs/" + AccId);

            },
             GetCompanyAccount: function (AccId) {
                 //alert(InitiativeApiUrl + "GetCompanyAccount/" + AccId);
                return $http.get(InitiativeApiUrl + "GetCompanyAccount/" + AccId);

            },
            deleteCompanyAccountDetail: function (Id) {
                // alert(teamurl + teamId);
                return $http.delete(InitiativeApiUrl + Id);
            },
            updateCompanyAccountDetail: function (companyAccount) {
                return $http.put(InitiativeApiUrl, companyAccount);
            },
            GetSingleAttachmentownload: function (attachId) {
                //alert(InitiativeApiUrl + "GetCompanyAccount/" + AccId);
                return $http.get(InitiativeApiUrl + "GetSingleAttachmentownload/" + attachId);

            },
            GetAttachedDocumentDetail: function (attachId) {
             
                return $http.get(InitiativeApiUrl + "GetAttachedDocumentDetail/" + attachId);

            },
            getAccountDetailByVendorId: function (VendorId) {

                return $http.get(InitiativeApiUrl + "getAccountDetailByVendorId/" + VendorId);

            },
            getAccountDetailByCompanyId: function (CompanyId,VendorId) {

                return $http.get(InitiativeApiUrl + "getAccountDetailByCompanyId/" + CompanyId + "/" + VendorId);

            },
            getAccountDetailById: function (Id) {

                return $http.get(InitiativeApiUrl + "getAccountDetailById/" + Id);

            },
            getMyEnterprisesByCompanyName: function (dataModel) {

                return $http.post(InitiativeApiUrl + "GetMyEnterpriseByCompanyName/", dataModel)

            },
            GetCompanyAccByCompanyId: function (CompanyId) {

                return $http.get(InitiativeApiUrl + "GetCompanyAccByCompanyId/" + CompanyId);

            },
        }

        return Api;

    })


})