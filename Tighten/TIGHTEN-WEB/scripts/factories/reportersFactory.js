﻿define(['app'], function (app) {
    app.factory('reportersFactory', function ($http, apiURL) {


        var sectionUrl = apiURL.baseAddress + 'ReportersApi';
        var Api = {
            getAllProjectUsersForTodoes: function (UserId, CompanyId, ProjectId, RoleId) {
                return $http.get(sectionUrl + "/" + UserId + "/" + CompanyId + "/" + ProjectId + "/" + RoleId);

                //return $http.get(sectionUrl);
            },

            sendMailToProjectUsersForTodoes: function (data) {
                return $http.post(sectionUrl + "/SendMail" , data);
                //return $http.post(sectionUrl + "/SaveInvoice", data);
            },

            SaveInvoice: function (data) {
                //return $http.post(sectionUrl + "/SendMail" , data);
                return $http.post(sectionUrl + "/SaveInvoice", data);
            },

            getAllTodosForPreview: function (data) {
                //getAllTodosForPreview
                return $http.post(sectionUrl + '/getAllTodosForPreview', data);

            }

        };
        return Api;
    });
});
