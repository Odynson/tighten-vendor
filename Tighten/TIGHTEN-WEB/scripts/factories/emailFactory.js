﻿define(['app'], function (app) {
    app.factory('emailFactory', function ($http, apiURL) {
        var toDosApiUrl = apiURL.baseAddress + 'EmailAPI/';
        var Api = {
            sendEmail: function (emaildata) {
                //
                return $http.post(emaildata);
            }
        };
        return Api;
    });
});