﻿define(['app'], function (app) {
    app.factory('userTodoApprovalFactory', function ($http, apiURL) {
        var toDoApiUrl = apiURL.baseAddress + 'userToDoApproval/';

        var Api = {
            getAllTodos: function (search) {
                return $http.post(toDoApiUrl + 'GetToDo', search);
            },

            getUserProjects: function (companyId, userId) {
                return $http.get(toDoApiUrl + "getUserProjects/" + companyId + "/" + userId);
            },
            //Delete the particular time slot 
            DeleteUserLoggedTimeSlot: function (Id) {
                return $http.post(toDoApiUrl + "DeleteUserLoggedTimeSlot/" + Id);
            },

            UpdateUserLoggedTimeSlot: function (userId, todoDetailId, minutes) {
                return $http.post(toDoApiUrl + "UpdateUserLoggedTimeSlot/" + userId + "/" + todoDetailId + "/" + minutes);
            },
            getUserCompanies: function (UserId) {
                return $http.get(toDoApiUrl + "getUserCompanies/" + UserId)
            },
            getUserProjectsForAllCompanies: function (userId,companyId) {
                return $http.get(toDoApiUrl + "getUserProjectsForAllCompanies/" + userId + "/" + companyId);
            }

        };
        return Api;
    });
});