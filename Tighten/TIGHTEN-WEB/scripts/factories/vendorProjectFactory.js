﻿define(["app"], function (app) {

    app.factory('vendorProjectFactory', function ($http, apiURL) {

 
        var projectApiUrl = apiURL.baseAddress + "ProjectsApi/";

        var Api = {


            getNewProjectForEdit: function (ProjectId,CompanyId) {

                return $http.get(projectApiUrl + "GetNewProjectForEdit/" + ProjectId + "/" + CompanyId)
            },
            /////////////////////////vendor code/////////////////

            getProjectVendor: function (CompanyId,ProjectId) {
                return $http.get(projectApiUrl + "GetProjectVendor/" + CompanyId + "/" + ProjectId);
            },
                    /////////////////////////vendor code/////////////////

            deleteEditProjectFiles: function (data) {
                return $http.post(projectApiUrl + "DeleteEditProjectFiles/",data);
            },

            getProjectAwarded: function (dataModal) {
                return $http.post(projectApiUrl + "GetProjectAwardedForVendor/", dataModal)

            },
            
            companyListForDrop: function (VendorId) {
                return $http.get(projectApiUrl + "CompanyListForAwardedProjectDropVendorProfile/" + VendorId)

        }

        }

        return Api;

    })


})