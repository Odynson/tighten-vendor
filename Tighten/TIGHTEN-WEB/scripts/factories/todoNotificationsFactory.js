﻿define(['app'], function (app) {
    app.factory('todoNotificationsFactory', function ($http, apiURL) {
        var TodoNotificationApiUrl = apiURL.baseAddress + 'TodoNotificationsApi';
        var Api = {
            getTodoNotifications: function (todoId) {
                return $http.get(TodoNotificationApiUrl + "/" + todoId);
            },
            insertTodoNotification: function (todonotification) {
                return $http.post(TodoNotificationApiUrl, todonotification);
            }
        };
        return Api;
    });
});


