﻿
define(['app'], function (app) {
    app.factory('accountSettingsFactory', function ($http, apiURL) {

        var AccountSettingsApiUrl = apiURL.baseAddress + 'AccountSettingsApi';
        var Api = {
            getAccountSettings: function (UserId) {
                return $http.get(AccountSettingsApiUrl + "/" + UserId);
            },

            updateAccountSettings: function (AccountSettingsModel) {
                
                return $http.post(AccountSettingsApiUrl , AccountSettingsModel);
            },
            updateUserRoutingNumber: function (stripeObject) {
                return $http.put(AccountSettingsApiUrl + "/UpdateUserRoutingNumber", stripeObject);
            },

            UpdateUserRoutingNumberVendorAdminProfile: function (stripeObject) {
                return $http.post(AccountSettingsApiUrl + "/UpdateUserRoutingNumberVendorAdminProfile", stripeObject);
            },

            updateUserRoutingNumberVendorProfile: function (stripeObject) {
                return $http.post(AccountSettingsApiUrl + "/UpdateUserRoutingNumberVendorPOCsProfile", stripeObject);
            },


             
        };

        return Api;
    });
});
