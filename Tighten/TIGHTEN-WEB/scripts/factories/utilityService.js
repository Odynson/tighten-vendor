﻿//app.constant('app.urls', {
//    project: WebDomainPath + '/api/ProjectsAPI',

//})
define(['app'], function (app) {
    app.service('utilityService', function ($http, apiURL) {

        return {

            createMinutesDifferenceFromTimeString: function (LoggedTimeString, RemovedTimeString) {
                //

                var totalString = '';
                var splitedHourLoggedTimeString = ''
                var splitedHourRemovedTimeString = ''
                var splitedMinutesLoggedTimeString = ''
                var splitedMinutesRemovedTimeString = ''

                //hour array
                var splitedHourLoggedTimeStringArr = LoggedTimeString.split('h');
                var splitedHourRemovedTimeStringArr = RemovedTimeString.split('h');

                //hours 
                splitedHourLoggedTimeString = splitedHourLoggedTimeStringArr[0];
                splitedHourRemovedTimeString = splitedHourRemovedTimeStringArr[0];

                // minute array
                var splitedMinuteLoggedTimeStringArr = splitedHourLoggedTimeString[1].split('m');
                var splitedMinuteRemovedTimeStringArr = splitedHourRemovedTimeString[1].split('m');

                //minutes
                splitedMinutesLoggedTimeString = splitedMinuteLoggedTimeStringArr[0];
                splitedMinutesRemovedTimeString = splitedMinuteRemovedTimeStringArr[0];

                // Total Minutes
                LoggedTimeMinutes = (splitedHourLoggedTimeString * 60) + (splitedMinutesLoggedTimeString);
                RemovedTimeMinutes = (splitedHourRemovedTimeString * 60) + (splitedMinutesRemovedTimeString);

                return ConvertMinutesToFormatedTimeLogString(parseInt(LoggedTimeMinutes) - parseInt(RemovedTimeMinutes));
            },

            ConvertMinutesToFormatedTimeLogString: function (TotalMinutes) {
                var FormatedString = "";
                var Hours = TotalMinutes / 60;
                var Minutes = TotalMinutes % 60;
                Hours = parseInt(Hours);
                Minutes = parseInt(Minutes);
                /*   Below lines are commented because we need time with hours and minutes both  e.g: 0h 0m   */
                //if (Hours == 0)
                //    FormatedString = Minutes + "m";
                //else if (Hours != 0 && Minutes == 0)
                //    FormatedString = Hours + "h ";
                //else
                    FormatedString = Hours + "h " + Minutes + "m";
                return FormatedString;
            },
            createMinutesFromTimeString: function (LoggedTimeString) {

                //var splitedHourLoggedTimeString = ''
                //var splitedMinutesLoggedTimeString = ''
                ////hour array
                //var splitedHourLoggedTimeStringArr = LoggedTimeString.split('h');
                ////hours 
                //splitedHourLoggedTimeString = splitedHourLoggedTimeStringArr[0];
                //// minute array
                //var splitedMinuteLoggedTimeStringArr = splitedHourLoggedTimeStringArr[1].split('m');
                ////minutes
                //splitedMinutesLoggedTimeString = splitedMinuteLoggedTimeStringArr[0];
                //// Total Minutes
                //LoggedTimeMinutes = (splitedHourLoggedTimeString * 60) + (splitedMinutesLoggedTimeString);
                //return LoggedTimeMinutes;


                var splitedHourLoggedTimeString = 0;
                var splitedMinutesLoggedTimeString = 0;
                //hour array
                var splitedHourLoggedTimeStringArr = [];
                // minute array
                var splitedMinuteLoggedTimeStringArr = [];
                if (LoggedTimeString.indexOf('h') != -1 && LoggedTimeString.indexOf('m') != -1) {
                    //hour array
                    splitedHourLoggedTimeStringArr = LoggedTimeString.split('h');
                    //hours 
                    splitedHourLoggedTimeString = splitedHourLoggedTimeStringArr[0];
                    // minute array
                    splitedMinuteLoggedTimeStringArr = splitedHourLoggedTimeStringArr[1].split('m');
                    //minutes
                    splitedMinutesLoggedTimeString = splitedMinuteLoggedTimeStringArr[0].trim();
                }
                else if (LoggedTimeString.indexOf('h') != -1 && LoggedTimeString.indexOf('m') == -1) {
                    splitedHourLoggedTimeStringArr = LoggedTimeString.split('h');
                    //hours 
                    splitedHourLoggedTimeString = splitedHourLoggedTimeStringArr[0];
                }
                else if (LoggedTimeString.indexOf('h') == -1 && LoggedTimeString.indexOf('m') != -1) {
                    splitedMinuteLoggedTimeStringArr = LoggedTimeString.split('m');
                    //minutes
                    splitedMinutesLoggedTimeString = splitedMinuteLoggedTimeStringArr[0].trim();
                }
                else if (LoggedTimeString.indexOf('h') == -1 && LoggedTimeString.indexOf('m') == -1) {
                    return false;
                }
                // Total Minutes
                LoggedTimeMinutes = parseInt(splitedHourLoggedTimeString * 60) + parseInt(splitedMinutesLoggedTimeString);
                return LoggedTimeMinutes;






            }
        };

    });
});