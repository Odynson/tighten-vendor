﻿define(['app', 'ngCookies'], function (app) {

    app.factory('AuthenticationFactory',

    function ($http, $cookies, $cookieStore, $rootScope, apiURL) {
        return {

            Login: function (loginObject, callback) {
                debugger;
                //http://localhost:55580/api/Authentication'
                //alert(apiURL.baseAddress);
                $http.post(apiURL.baseAddress + 'authenticate', { "Email": loginObject.Email, "Password": loginObject.Password, "IsLoginWithLinedIn": loginObject.IsLoginWithLinedIn })//JSON.stringify( loginObject)
                    .success(function (response) {
                        callback(response);
                    });
            },
            test: function (loginObject, callback) {
                $http.post(apiURL.baseAddress + 'test')//JSON.stringify( loginObject)
                    .success(function (response) {
                        callback(response);
                    });
            },

            SetCredentials: function (response) {
                //var authdata = Base64Factory.encode(email + ':' + token);

                //$rootScope.globals = {};
                //var currentUser = { username: email, authdata: token };
                //$rootScope.globals.currentUser = currentUser;
                $rootScope.globals = {
                    currentUser: response
                };
                $http.defaults.headers.common['Authorization'] = 'Basic ' + response.Token; // jshint ignore:line
                //$cookies.putObject('globals',JSON.stringify( $rootScope.globals),'/');
                //$cookies.putObject('globals', angular.fromJson($rootScope.globals));
                $cookieStore.put('globals', $rootScope.globals);
            },

            ClearCredentials: function () {
                $rootScope.globals = {};
                $cookies.remove('globals');
                $http.defaults.headers.common.Authorization = 'Basic';
            }


        };

    });

});