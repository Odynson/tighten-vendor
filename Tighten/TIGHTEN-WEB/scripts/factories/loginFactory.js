﻿define(['app','ngCookies'], function (app) {

    app.factory('loginFactory', function ($http, apiURL,$cookies) {
        //
        var loginApi = apiURL.baseAddress + 'LoginApi';
        //var Api = {

        //    Logout: function (Id) {
        //        return $http.post("Account/Logout");
        //    },

        //    Login: function (object) {

        //        return $http.post("Account/Login", object);
        //    }

        //};

        //return Api;

        var service = {};
 
        service.Login = Login;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
 
        return service;
 
        function Login(username, password, callback) {
 
            /* Dummy authentication for testing, uses $timeout to simulate api call
             ----------------------------------------------*/
            //$timeout(function () {
            //    var response;
            //    UserService.GetByUsername(username)
            //        .then(function (user) {
            //            if (user !== null && user.password === password) {
            //                response = { success: true };
            //            } else {
            //                response = { success: false, message: 'Username or password is incorrect' };
            //            }
            //            callback(response);
            //        });
            //}, 1000);
 
            /* Use this for real authentication
             ----------------------------------------------*/
            $http.post(apiURL.baseAddress + 'authenticate', { username: username, password: password })
                .success(function (response) {
                    callback(response);
                });
 
        }
 
        function SetCredentials(username, token) {
            var authdata = Base64.encode(username + ':' + token);
 
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata
                }
            };
 
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
        }
 
        function ClearCredentials() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic';
        }
    
 
    });
});