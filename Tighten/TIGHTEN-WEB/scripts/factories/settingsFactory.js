﻿
define(['app'], function (app) {
    app.factory('settingsFactory', function ($http, apiURL) {

        var SettingsApiUrl = apiURL.baseAddress + 'SettingsApi/';
        var ProjectCustomizationControlApi = apiURL.baseAddress + 'ProjectCustomizationControlApi/';
        var Api = {
            getAllEmailSettingsOption: function (UserId, CompanyId, ProjectId, RoleId) {
                return $http.get(SettingsApiUrl + UserId + "/" + CompanyId + "/" + ProjectId + "/" + RoleId);
            },
            changeEmailSettingsOption: function (data) {
                return $http.post(SettingsApiUrl, data);
            },
            getProjectsForCurrentCompany: function (UserId, CompanyId) {
                return $http.get(SettingsApiUrl + "GetProjectsForCurrentCompany/" + UserId + "/" + CompanyId);
            },
            getEmailSettingsForProjectLevel: function (UserId, CompanyId) {
                return $http.get(SettingsApiUrl + "GetEmailSettingsForProjectLevel/" + UserId + "/" + CompanyId);
            },
            changeAllProjectSetting: function (data) {
                return $http.post(SettingsApiUrl + "ChangeAllProjectSetting/", data);
            },
            getEmailSettingsForTodoLevel: function (UserId, CompanyId) {
                return $http.get(SettingsApiUrl + "GetEmailSettingsForTodoLevel/" + UserId + "/" + CompanyId);
            },
            changeAllTodoSetting: function (data) {
                return $http.post(SettingsApiUrl + "ChangeAllTodoSetting/", data);
            },
            changeProjectSetting: function (data) {
                return $http.post(SettingsApiUrl + "ChangeProjectSetting/", data);
            },
            changeTodoSetting: function (data) {
                return $http.post(SettingsApiUrl + "ChangeTodoSetting/", data);
            },

            changeVendorSetting: function (CompanyId, IsVendor) {
                return $http.post(SettingsApiUrl + "ChangeVendorSetting/" + CompanyId + "/" + IsVendor)
            },

            changeVendorSetting: function (CompanyId, IsVendor) {
                return $http.post(SettingsApiUrl + "ChangeVendorSetting/" + CompanyId + "/" + IsVendor)
            },
            savecustomcontrol: function (dataModel) {
                return $http.post(ProjectCustomizationControlApi, dataModel);
            },


            GetCustomControl: function (CompanyId) {
                return $http.get(ProjectCustomizationControlApi +  "GetCustomControl/" + CompanyId);
            },

            deleteCustomControl: function (dataModel) {
                return $http.put(ProjectCustomizationControlApi + "deleteCustomControl/", dataModel);
        },

            updatePriority: function (dataModel) {
                return $http.post(ProjectCustomizationControlApi + "UpdatePriority/", dataModel);
        },
        };

        return Api;
    });
});