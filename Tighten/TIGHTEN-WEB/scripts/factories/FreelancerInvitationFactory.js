﻿define(['app'], function (app) {
    app.factory('FreelancerInvitationFactory', function ($http, apiURL) {
        var confirmFreelancerUrl = apiURL.baseAddress + "FreelancerAccount/"; 
        var Api = {

            getFreelancerInfoForLinkConfirmation: function (userName,CompanyId) {
                return $http.get(confirmFreelancerUrl + "getFreelancerInfo/" + userName + "/" + CompanyId);
            },
            getAllFreelancer: function () {
                return $http.get(confirmFreelancerUrl + "GetAllFreelancer");
            },
            confirmFreelancerEmailOrHandle: function (data) {
                return $http.post(confirmFreelancerUrl + "confirmEmailOrHandle/", data);
            }

            
            
            

            };

        return Api;
    });
});