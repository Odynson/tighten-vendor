﻿define(['app'], function (app) {
    app.factory('FreelancerAccountFactory', function ($http, apiURL) {
        var FreelancerUrl = apiURL.baseAddress + "FreelancerAccount/";
        var Api = {

            getFreelancerInfoForRegistration: function (key) {
                return $http.get(FreelancerUrl + key);
            },
            RegisterFreelancer: function (ModelToInsert) {
                return $http.post(FreelancerUrl, ModelToInsert)
            },

            getCompaniesForFreelancer: function (UserId) {
                return $http.get(FreelancerUrl + "getCompaniesForFreelancer/" + UserId)
            },
            GetFreelancerRoleForCompany: function (UserId, CompanyId) {
                return $http.get(FreelancerUrl + "GetFreelancerRoleForCompany/" + UserId + "/" + CompanyId)
            },
            getAllFreelancerInProject: function (ProjectId) {
                return $http.get(FreelancerUrl + "GetAllFreelancerInProject/" + ProjectId)
            }



        };

        return Api;
    });
});