﻿define(['app'], function (app) {
    app.factory('todoApprovalFactory', function ($http, apiURL) {
        var toDoApiUrl = apiURL.baseAddress + 'ToDoApproval/';
        var toDosApiUrl = apiURL.baseAddress + 'ToDos/';

        var Api = {
            getAllTodos: function (search) {
                return $http.post(toDoApiUrl + 'GetToDo', search);
            },
            ApproveTodo: function (todo) {
                //
                return $http.post(toDoApiUrl + 'ApproveTodo', todo);
            },
            // this is for the manual adding the time to the PMS todos
            rejectTodo: function (data) {
                return $http.put(toDoManualApiUrl, data);
            },
            deleteTodo: function (Id) {
                return $http.delete(toDosApiUrl + "/" + Id);
            },
            getCompanyUsers: function (companyId,RoleId,UserId) {
                return $http.get(toDoApiUrl + "getCompanyUsers/" + companyId + "/" + RoleId + "/" + UserId)
            }

        };
        return Api;
    });
});