﻿define(['app'], function (app) {
    app.factory('CalendarEventsFactory', function ($http, apiURL) {
        var calendarEventsUrl = apiURL.baseAddress + 'CalendarEventsApi/';
        var Api = {
            getCalendarEvents: function (CompanyId, UserId, TeamId, ProjectId) {
                return $http.get(calendarEventsUrl + CompanyId + '/' + UserId + '/' + TeamId + '/' + ProjectId);
            },
            getSingleCalendarEvent: function (CalendarEventId) {
                return $http.get(calendarEventsUrl + "GetEvent/" + CalendarEventId);
            },
            insertCalendarEvent: function (calendarEventObject) {
                return $http.post(calendarEventsUrl, calendarEventObject);
            },
            updateCalendarEvent: function (calendarEventObject) {
                return $http.put(calendarEventsUrl, calendarEventObject);
            },
            updateCalendarEventOnDrop: function (data) {
                return $http.put(calendarEventsUrl + "UpdateOnDrop/", data);
            },
            updateCalendarEventOnResize: function (data) {
                return $http.put(calendarEventsUrl + "UpdateOnResize/", data);
            },
            deleteSection: function (CalendarEventId) {
                return $http.delete(calendarEventsUrl + CalendarEventId);
            }
        };
        return Api;
    });
});