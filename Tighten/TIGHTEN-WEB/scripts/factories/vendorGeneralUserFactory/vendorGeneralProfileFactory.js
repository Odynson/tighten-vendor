﻿define(['app'], function () {

    app.factory('vendorGeneralProfileFactory', function ($http, apiURL) {



        var VendorAdminApiUrl = apiURL.baseAddress + 'VendorGeneralProfileApi/';
        var Api = {

            getMyProjectVendorGeneralProfile: function (dataModelProject) {
                return $http.post(VendorAdminApiUrl + "GetMyProject/", dataModelProject);
            },

            getVendorGeneraUserWorkNote: function (ProjectId, VendorGeneralUserId) {
                return $http.get(VendorAdminApiUrl + "GetWorkNoteForVendorGeneralUser/" + ProjectId + "/" + VendorGeneralUserId);
            },

            addWorkDiaryVendorGeneralUser: function (dataModelSaveNote) {
                return $http.post(VendorAdminApiUrl + "InsertNoteVendorGeneralUser", dataModelSaveNote);

            },
            getMyProjectViewDetailVendorGeneralProfile: function (ProjectId, UserId) {

                return $http.get(VendorAdminApiUrl + "GetMyProjectViewDetailVendorGeneralProfile" + '/' + ProjectId + '/' + UserId);
            }

        }

        return Api;
    })
})