﻿//app.constant('app.urls', {
//    project: WebDomainPath + '/api/ProjectsAPI',

//})
define(['app'], function (app) {
    app.factory('globalFactory', function ($http, apiURL) {
        var url = apiURL.baseAddress + "ToDoAPI/";
        var Api = {
            getRunningTaskDetail: function (userId) {
                //
                return $http.get(url + "getRunningTaskDetail/" + userId);
            }
        };

        return Api;
    });
});