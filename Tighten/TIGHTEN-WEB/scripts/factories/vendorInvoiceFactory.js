﻿define(['app'], function (app) {

    app.factory('vendorInvoiceFactory', function ($http, apiURL) {
        var vendorInvoiceUrl = apiURL.baseAddress + 'VendorInvoiceApi/';

        var Api = {

            saveVendorInvoice: function (dataModal) {
                return $http.post(vendorInvoiceUrl + "/SaveInvoiceByVendor", dataModal);
            },
            //////////////////// Get Invoice List//////////////////////////
            getAllInvoiceDetailList: function (data) {
                return $http.post(vendorInvoiceUrl + "GetAllInvoiceDetailList/", data)

            },
            getPhaseAndResourceforProject: function (ProjectId, VendorId) {
                return $http.get(vendorInvoiceUrl + "GetPhaseAndResourceforProject/" + ProjectId + "/" + VendorId)

            },
            getAllVendorInvoiceList: function (dataModal) {
                return $http.post(vendorInvoiceUrl + "GetAllVendorInvoiceList/", dataModal)
            },
            getVendorInvoicePreview: function (CompanyId, Id) {
                return $http.get(vendorInvoiceUrl + "GetVendorInvoicePreview/" + CompanyId + "/" + Id)
            },
            getPreviewInvoicePaidOutstandingReject: function (Id, CompanyId) {
                return $http.get(vendorInvoiceUrl + "GetPreviewInvoicePaidOutstandingReject/" + Id + "/" + CompanyId)
            },
            approvedVendorInvoice: function (data) {
                return $http.post(vendorInvoiceUrl + "SaveApproveVendorInvoice/", data)
            },
            rejectInvoice: function (data) {
                return $http.post(vendorInvoiceUrl + "/RejectInvoice", data);
            },
            getCardDetailForVendorPayment: function (PaymentDetailId, CompanyId, InvoiceId) {
                return $http.get(vendorInvoiceUrl + "/GetCardDetailForVendorPayment/" + PaymentDetailId + "/" + CompanyId + "/" + InvoiceId);
            },
            getProjectForDrop: function (CompanyId,VendorId) {
                return $http.get(vendorInvoiceUrl + "ProjectForInvoiceDrop/" + CompanyId + "/" + VendorId)
            }
            , GetVendorForDrop: function (CompanyId) {
                return $http.get(vendorInvoiceUrl + "GetVendorForDrop/" + CompanyId)
            },

            getcompanyForDropFilterInvoice: function (CompanyId) {
                return $http.get(vendorInvoiceUrl + "GetCompanyForDrop/" + CompanyId)
            },
            approveInvoice: function (data) {
                return $http.put(vendorInvoiceUrl, data);
            },
            getAllVendorInvoiceListOutstanding: function (dataModal) {
                return $http.post(vendorInvoiceUrl + "GetAllVendorInvoiceListOutstanding/", dataModal)
            },
            getAllVendorInvoiceListPaid: function (dataModal) {
                return $http.post(vendorInvoiceUrl + "GetAllVendorInvoiceListPaid/", dataModal)
            },
            getAllVendorInvoiceListRejected: function (dataModal) {
                return $http.post(vendorInvoiceUrl + "GetAllVendorInvoiceListRejected/", dataModal)
            },

            getAllInvoiceDetailListOutstanding: function (data) {
                return $http.post(vendorInvoiceUrl + "GetAllInvoiceDetailListOutstanding/", data)

            },
            getAllInvoiceDetailListPaid: function (data) {
                return $http.post(vendorInvoiceUrl + "GetAllInvoiceDetailListPaid/", data)

            },
            getAllInvoiceDetailListRejected: function (data) {
                return $http.post(vendorInvoiceUrl + "GetAllInvoiceDetailListRejected/", data)

            },
        }
        return Api;
    });
})