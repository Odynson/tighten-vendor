﻿define(['app'], function (app) {
    app.factory('teamdetailFactory', function ($http, apiURL) {
        var teamdetailurl = apiURL.baseAddress + 'TeamDetailAPI/';
        var Api = {
            GetTeamDetail: function (teamId, CompanyId) {
                return $http.get(teamdetailurl + 'GetTeamDetail/' + teamId);
            }
        };
        return Api;
    });
});