﻿

define(['app'], function (app) {
    app.factory('adminInvitationFactory', function ($http, apiURL) {

        var InvitationApiUrl = apiURL.baseAddress + 'AdminInvitationApi/';
        var Api = {
            getAllSentRequest: function (CompanyId, PageIndex, PageSizeSelected) {
                return $http.get(InvitationApiUrl + CompanyId + "/" + PageIndex + "/" + PageSizeSelected);
            },


        };

        return Api;
    });
});