﻿
define(["app"], function (app) {

    app.factory('parentAccountFactory', function ($http, apiURL) {

        var parentAccountApiUrl = apiURL.baseAddress + "ParentAccountApi/";

        var Api = {


            getParentAccountList: function (CompanyId) {
                debugger;
                return $http.get(parentAccountApiUrl + "GetParentAccountList/" + CompanyId);

            },
            GetParentAccountById: function (Id) {
                return $http.get(parentAccountApiUrl + "GetParentAccountById/" + Id);

            },
            saveParentAccount: function (model) {
                return $http.post(parentAccountApiUrl + "SaveNewParentAccount", model);
            },
            deleteParentAccountDetail: function (Id) {
                // alert(teamurl + teamId);
                return $http.delete(parentAccountApiUrl + Id);
            },
            updateParnetAccountDetail: function (companyAccount) {
                return $http.put(parentAccountApiUrl, companyAccount);
            },
            getVendorList: function (CompanyId) {
                return $http.get(parentAccountApiUrl + "GetVendorList/" + CompanyId);

            },
            GetVendorPOCList: function (CompanyId,VendorId) {
                return $http.get(parentAccountApiUrl + "GetVendorPOCList/" + CompanyId + "/" + VendorId);

            },
             DownloadDocAttachments: function (AccId) {
                return $http.get(parentAccountApiUrl + "DownloadDocs/" + AccId);

            },
             GetCompanyAccount: function (AccId) {
                 //alert(parentAccountApiUrl + "GetCompanyAccount/" + AccId);
                return $http.get(parentAccountApiUrl + "GetCompanyAccount/" + AccId);

            },
            deleteCompanyAccountDetail: function (Id) {
                // alert(teamurl + teamId);
                return $http.delete(parentAccountApiUrl + Id);
            },
            updateCompanyAccountDetail: function (companyAccount) {
                return $http.put(parentAccountApiUrl, companyAccount);
            },
            GetSingleAttachmentownload: function (attachId) {
                //alert(parentAccountApiUrl + "GetCompanyAccount/" + AccId);
                return $http.get(parentAccountApiUrl + "GetSingleAttachmentownload/" + attachId);

            },
            GetAttachedDocumentDetail: function (attachId) {
             
                return $http.get(parentAccountApiUrl + "GetAttachedDocumentDetail/" + attachId);

            },
            getAccountDetailByVendorId: function (VendorId) {

                return $http.get(parentAccountApiUrl + "getAccountDetailByVendorId/" + VendorId);

            },
            getAccountDetailByCompanyId: function (CompanyId,VendorId) {

                return $http.get(parentAccountApiUrl + "getAccountDetailByCompanyId/" + CompanyId + "/" + VendorId);

            },
            getAccountDetailById: function (Id) {

                return $http.get(parentAccountApiUrl + "getAccountDetailById/" + Id);

            },
            getMyEnterprisesByCompanyName: function (dataModel) {

                return $http.post(parentAccountApiUrl + "GetMyEnterpriseByCompanyName/", dataModel)

            },
            GetCompanyAccByCompanyId: function (CompanyId) {

                return $http.get(parentAccountApiUrl + "GetCompanyAccByCompanyId/" + CompanyId);

            },
            getParentAccountForVendorList: function (VendorId) {
                debugger;
                return $http.get(parentAccountApiUrl + "GetParentAccountForVendorList/" + VendorId);

            },
        }

        return Api;

    })


})