﻿

define(['app'], function (app) {
    app.factory('roleManageFactory', function ($http, apiURL) {
        var roleManageApiUrl = apiURL.baseAddress + 'RoleManageApi';
        var Api = {

            getCompanyRoles: function (CompanyId) {
                return $http.get(roleManageApiUrl + "/" + CompanyId);
            },
            insertRole: function (obj) {
                return $http.post(roleManageApiUrl, obj);
            },
            updateRole: function (obj) {
                return $http.put(roleManageApiUrl, obj);
            },
            deleteRole: function (RoleId,CompanyId) {
                return $http.delete(roleManageApiUrl + "/" + RoleId + "/" + CompanyId);
            },
            activeInactiveRole: function (RoleId, CompanyId) {
                return $http.get(roleManageApiUrl + "/activeInactiveRole/" + RoleId + "/" + CompanyId);
            },



        };
        return Api;
    });
});

