﻿define(['app'], function (app) {
    app.factory('clientDashboardFactory',function ($http, apiURL) {
        var clientDashboardApiUrl = apiURL.baseAddress + 'ClientDashboardApi/';
        var Api = {
            getClientUpcomingTodos: function (userId) {
                return $http.get(clientDashboardApiUrl + userId);
            },
            getClientProjects: function (userId) {
                return $http.get(clientDashboardApiUrl + "Projects/" + userId);
            },
            getClientConcernedUsers: function (userId) {
                return $http.get(clientDashboardApiUrl + "Users/" + userId)
            }
        };
        return Api;
    });
});