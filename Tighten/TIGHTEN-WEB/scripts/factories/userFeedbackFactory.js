﻿define(['app'], function (app) {
    app.factory('userFeedbackFactory', function ($http, apiURL) {
        var userFeedbackApiUrl = apiURL.baseAddress + 'UserFeedbackApi/';
        var userFeedbackVoterApiUrl = apiURL.baseAddress + 'UserFeedbackVoterApi/';
        var userFeedbackCommentApiUrl = apiURL.baseAddress + 'UserFeedbackCommentApi/';

        var Api = {

            insertUserFeedback: function (UserFeedbackModel) {
                return $http.post(userFeedbackApiUrl, UserFeedbackModel);
            },
            getUserFeedbacks: function (userId) {
                return $http.get(userFeedbackApiUrl + userId);
            },
            getUserFeedbackComments: function (userFeedbackId) {
                return $http.get(userFeedbackCommentApiUrl + userFeedbackId);
            },
            updateUserFeedbackVoter: function (UserFeedbackVoterModel) {
                return $http.put(userFeedbackVoterApiUrl, UserFeedbackVoterModel);
            },
            insertUserFeedbackComment: function (UserFeedbackCommentModel) {
                return $http.post(userFeedbackCommentApiUrl, UserFeedbackCommentModel);
            },

        };

        return Api;
    });
});