﻿
define(['app'], function (app) {
    app.factory('invitationFactory', function ($http, apiURL) {

        var InvitationApiUrl = apiURL.baseAddress + 'InvitationApi/';
        var Api = {
                     GetInvitationNotification: function (UserId,InvitaitonStatus) {
                         return $http.get(InvitationApiUrl + UserId + "/" + InvitaitonStatus);
                     },
                     acceptInvitation: function (freelancerInvitationObject) {
                         return $http.post(InvitationApiUrl, freelancerInvitationObject)
                     },
                     rejectInvitation: function (freelancerInvitationObject) {
                         return $http.put(InvitationApiUrl, freelancerInvitationObject)
                     },
                     getUserMessage: function (FreelancerInvitationId,UserId) {
                         return $http.get(InvitationApiUrl + "GetUserMessage/" + FreelancerInvitationId + "/" + UserId);
                     },
                     sendMessgae: function (model) {
                         return $http.post(InvitationApiUrl + "SendMessgae", model)
                     },
                     getFreelancerInfo: function (InvitationId) {
                         return $http.get(InvitationApiUrl + "GetFreelancerInfo/" + InvitationId)
                     },


                  };

        return Api;
    });
});