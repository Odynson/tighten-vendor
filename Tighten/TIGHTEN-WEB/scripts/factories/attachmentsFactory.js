﻿define(['app'], function (app) {
    app.factory('attachmentsFactory', function ($http, apiURL) {

        var toDoattachmentsApiUrl = apiURL.baseAddress + 'ToDoAttachmentsApi';
        var uploadToDoAttachmentsApiUrl = apiURL.baseAddress + 'FilesApi';

        var Api = {

            getToDoAttachments: function (todoId, userId) {
                return $http.get(toDoattachmentsApiUrl + "/" + todoId+"/"+userId);
            },

            deleteTodoAttachments: function (Id) {
                return $http.delete(toDoattachmentsApiUrl + "/" + Id);
            },
            saveDropBoxFiles: function (Model) {
                return $http.post(uploadToDoAttachmentsApiUrl + "/DropBoxFiles", Model);

            },
            saveBoxFiles: function (Model) {
                return $http.post(uploadToDoAttachmentsApiUrl + "/BoxFiles", Model);

            },
            saveGoogleDriveFiles: function (Model) {
                return $http.post(uploadToDoAttachmentsApiUrl + "/GoogleDriveFiles", Model);

            },
            saveTodoAttachments: function (attachment) {
                return $http.post(toDoattachmentsApiUrl, attachment);
            },
            getProjectAttachments: function (Id) {
                return $http.get(toDoattachmentsApiUrl + "/Attachments" + "/" + Id);
            },
        };

        return Api;
    });
});