﻿
define(['app'], function (app) {
    app.factory('userInvoiceFactory', function ($http, apiURL) {

        var sectionUrl = apiURL.baseAddress + 'UserInvoiceApi';

        var Api = {

            getAllInvoicesForUser: function (data) {
                return $http.post(sectionUrl, data);
            },

            getInvoiceDetailForUser: function (data) {
                return $http.post(sectionUrl + "/GetInvoiceDetail", data);
            },

            forwardInvoice: function (InvoiceItem) {
                return $http.post(sectionUrl + "/ForwardInvoice" , InvoiceItem );
            },
            getProjectMembersForUser: function (data) {
                return $http.post(sectionUrl + "/GetProjectMembersForUser", data);
            },
     


        };



        return Api;

    });
});
