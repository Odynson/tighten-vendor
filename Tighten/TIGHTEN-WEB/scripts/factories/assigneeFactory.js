﻿define(['app'], function (app) {
    app.factory('assigneeFactory', function ($http, apiURL) {
        var assigneeUrl = apiURL.baseAddress + 'AssigneeApi';
        var Api = {
            updateTodoAssignee: function (assigneeObject) {
                return $http.put(assigneeUrl, assigneeObject);
            }
        };
        return Api;
    });
});