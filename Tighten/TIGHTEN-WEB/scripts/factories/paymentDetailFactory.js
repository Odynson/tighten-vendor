﻿
define(['app'], function (app) {
    app.factory('paymentDetailFactory', function ($http, apiURL) {
        var paymentDetailApiUrl = apiURL.baseAddress + 'PaymentDetailApi';
        var Api = {

            getpaymentDetail: function (CompanyId) {
                return $http.get(paymentDetailApiUrl + "/" + CompanyId);
            },
            insertPaymentDetail: function (obj) {
                return $http.post(paymentDetailApiUrl, obj);
            },
            updatePaymentDetail: function (obj) {
                return $http.put(paymentDetailApiUrl, obj);
            },
            deletePaymentDetail: function (Id) {
                return $http.delete(paymentDetailApiUrl + "/" + Id);
            }


        };
        return Api;
    });
});
