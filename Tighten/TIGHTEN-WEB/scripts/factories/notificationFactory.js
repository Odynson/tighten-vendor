﻿define(['app'], function (app) {

    app.factory('notificationFactory', function ($http, apiURL) {
        var NotificationApiUrl = apiURL.baseAddress + 'NotificationsApi/';
        var Api = {
            getNotifications: function (UserId) {
                return $http.get(NotificationApiUrl+UserId);
            },
            insertNotification: function (notification) {
                return $http.post(NotificationApiUrl, notification);
            },
            updateNotification: function (id) {
                return $http.put(NotificationApiUrl + "/" + id);
            },
            resetNotification: function (UserId) {
                return $http.put(NotificationApiUrl + "reset/" + UserId);
            }

        };
        return Api;
    });
});