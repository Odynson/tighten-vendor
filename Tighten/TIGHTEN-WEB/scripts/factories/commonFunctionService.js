﻿define(['app'], function (app) {
    app.factory("commonFunctionService", function ($scope, $rootScope, $timeout, $http, sharedService, toaster) {

        return {
            hideRightWindowFunction: function () {
                $scope.hideRightWindow = true;
                $scope.stretchCentreWindow = true;
                $scope.selectedTodo = "";
                $scope.selectedNotification = "";
                $scope.selectedInbox = "";
                $scope.selectedUserFeedback = "";
            }
        };
       
    });
});