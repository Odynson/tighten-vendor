﻿define(['app'], function (app) {

    app.factory('confirmEmailFactory', function ($http, apiURL) {
        var ApiURL = apiURL.baseAddress + 'Account/';
        return {
            ConfirmEmail: function (userId, emailCode, callback) {
                //
                /* Use this for real registration
                 ----------------------------------------------*/
                $http.get(ApiURL + 'confirmemail/'+userId+'/'+emailCode)
                    .success(function (response) {
                        
                        callback(response);
                    });
            }
        }
    });
});