﻿USE [Tighten]
 
GO
/****** Object:  Table [dbo].[EducationalStream]    Script Date: 12/5/2016 4:02:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EducationalStream](
	[Id] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InternalPermissions]    Script Date: 12/5/2016 4:02:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InternalPermissions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PermissionName] [nvarchar](max) NULL,
	[MenuId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[isActive] [bit] NULL,
	[isDeleted] [bit] NULL,
 CONSTRAINT [PK_dbo.InternalPermissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Menus]    Script Date: 12/5/2016 4:02:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[ParentId] [int] NOT NULL,
	[URL] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Menus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Packages]    Script Date: 12/5/2016 4:02:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Packages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Price] [decimal](18, 2) NULL,
	[DurationInDays] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Ispromotional] [bit] NOT NULL,
	[Discount] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_dbo.Packages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 12/5/2016 4:02:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserRole] [int] NOT NULL,
	[Link] [nvarchar](max) NULL,
	[MenuId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL DEFAULT ((0)),
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_dbo.Permissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 12/5/2016 4:02:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[isActive] [bit] NULL,
	[Description] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](max) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CompanyId] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SettingConfigs]    Script Date: 12/5/2016 4:02:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingConfigs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NotificationName] [nvarchar](max) NULL,
	[NotificationDescription] [nvarchar](max) NULL,
	[NotificationText] [nvarchar](max) NULL,
	[SettingSectionId] [int] NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_dbo.SettingConfigs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SettingSections]    Script Date: 12/5/2016 4:02:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingSections](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SectionName] [nvarchar](max) NULL,
	[SectionDescription] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.SettingSections] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tags]    Script Date: 12/5/2016 4:02:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tags](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TagName] [nvarchar](max) NULL,
	[TagDescription] [nvarchar](max) NULL,
	[TagType] [bit] NOT NULL,
	[IsActive] [bit] NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_dbo.Tags] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

 GO
INSERT [dbo].[EducationalStream] ([Id], [Name], [Description], [IsActive]) VALUES (NULL, N'CSE', N'ComputerSceince', 1)
GO
INSERT [dbo].[EducationalStream] ([Id], [Name], [Description], [IsActive]) VALUES (NULL, N'IT', N'Information Technology', 1)
GO
SET IDENTITY_INSERT [dbo].[InternalPermissions] ON 

GO
INSERT [dbo].[InternalPermissions] ([Id], [PermissionName], [MenuId], [RoleId], [CompanyId], [isActive], [isDeleted]) VALUES (1, N'Edit Company Profile', 1, 0, 0, 1, 0)
GO
INSERT [dbo].[InternalPermissions] ([Id], [PermissionName], [MenuId], [RoleId], [CompanyId], [isActive], [isDeleted]) VALUES (2, N'Add New Team', 3, 0, 0, 1, 0)
GO
INSERT [dbo].[InternalPermissions] ([Id], [PermissionName], [MenuId], [RoleId], [CompanyId], [isActive], [isDeleted]) VALUES (3, N'Update Team', 3, 0, 0, 1, 0)
GO
INSERT [dbo].[InternalPermissions] ([Id], [PermissionName], [MenuId], [RoleId], [CompanyId], [isActive], [isDeleted]) VALUES (4, N'Add New Project', 4, 0, 0, 1, 0)
GO
INSERT [dbo].[InternalPermissions] ([Id], [PermissionName], [MenuId], [RoleId], [CompanyId], [isActive], [isDeleted]) VALUES (5, N'Update Project', 4, 0, 0, 1, 0)
GO
INSERT [dbo].[InternalPermissions] ([Id], [PermissionName], [MenuId], [RoleId], [CompanyId], [isActive], [isDeleted]) VALUES (6, N'Approve Invoice', 14, 0, 0, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[InternalPermissions] OFF
GO
SET IDENTITY_INSERT [dbo].[Menus] ON 

GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (1, N'Company', 0, N'/company/profile', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (2, N'UserProfile', 0, N'/user/profile/', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (3, N'Team', 0, N'/team', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (4, N'Projects', 0, N'/project', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (5, N'Financial', 0, NULL, 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (6, N'Budget', 5, N'/team/financial', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (7, N'TaskApprovalForUser', 0, NULL, 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (8, N'TodoApproval', 7, N'/todo/approval', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (9, N'Invoice', 7, N'/invoice', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (10, N'Invitation', 7, N'/invitation', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (11, N'Organization', 7, N'/organization', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (12, N'TaskApprovalForAdmin', 0, NULL, 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (13, N'TodoApproval', 12, N'/admin/todo/approval', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (14, N'Invoice', 12, N'/admin/invoice', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (15, N'Invitation', 12, N'/admin/invitation', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (16, N'Account', 0, NULL, 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (17, N'Account Settings', 16, N'/account/settings', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (18, N'RoleManage', 12, N'/role/manage', 1)
GO
INSERT [dbo].[Menus] ([Id], [Name], [ParentId], [URL], [IsActive]) VALUES (19, N'RolePermission', 12, N'/role/permission', 1)
GO
SET IDENTITY_INSERT [dbo].[Menus] OFF
GO
SET IDENTITY_INSERT [dbo].[Packages] ON 

GO
INSERT [dbo].[Packages] ([Id], [Name], [Description], [Price], [DurationInDays], [IsActive], [Ispromotional], [Discount]) VALUES (2, N'Basic', N'this is basic package', CAST(10.00 AS Decimal(18, 2)), 30, 1, 1, CAST(0.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[Packages] ([Id], [Name], [Description], [Price], [DurationInDays], [IsActive], [Ispromotional], [Discount]) VALUES (3, N'Pro', N'this is pro package', CAST(50.00 AS Decimal(18, 2)), 30, 1, 1, CAST(0.00 AS Decimal(18, 2)))
GO
SET IDENTITY_INSERT [dbo].[Packages] OFF
GO
SET IDENTITY_INSERT [dbo].[Permissions] ON 

GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (1, 1, N'/company/profile', 1, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (2, 1, N'/user/profile/', 2, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (3, 1, N'/team', 3, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (4, 1, N'/project', 4, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (5, 1, NULL, 5, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (6, 1, N'/team/financial', 6, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (7, 1, NULL, 12, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (8, 1, N'/admin/todo/approval', 13, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (9, 1, N'/admin/invoice', 14, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (10, 1, N'/admin/invitation', 15, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (11, 2, N'/company/profile', 1, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (12, 2, N'/user/profile/', 2, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (13, 2, N'/team', 3, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (14, 2, N'/project', 4, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (15, 2, NULL, 12, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (16, 2, N'/admin/todo/approval', 13, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (17, 2, N'/admin/invoice', 14, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (18, 3, N'/company/profile', 1, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (19, 3, N'/user/profile/', 2, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (20, 3, N'/team', 3, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (21, 3, N'/project', 4, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (22, 4, N'/company/profile', 1, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (23, 4, N'/user/profile/', 2, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (24, 4, N'/team', 3, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (25, 4, N'/project', 4, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (26, 4, NULL, 7, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (27, 4, N'/todo/approval', 8, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (28, 4, N'/invoice', 9, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (29, 4, N'/invitation', 10, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (30, 4, N'/organization', 11, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (31, 4, NULL, 16, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (32, 4, N'/account/settings', 17, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (33, 5, N'/company/profile', 1, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (34, 5, N'/user/profile/', 2, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (35, 5, N'/team', 3, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (36, 5, N'/project', 4, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (37, 5, NULL, 5, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (38, 5, N'/team/financial', 6, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (39, 1, N'/role/manage', 18, 0, 1, 0)
GO
INSERT [dbo].[Permissions] ([Id], [UserRole], [Link], [MenuId], [CompanyId], [IsActive], [IsDeleted]) VALUES (40, 1, N'/role/permission', 19, 0, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[Permissions] OFF
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

GO
INSERT [dbo].[Roles] ([RoleId], [Name], [isActive], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyId]) VALUES (1, N'Administrator', 1, N'', N'1', CAST(N'2016-12-02 13:04:25.1216889' AS DateTime2), NULL, NULL, 0)
GO
INSERT [dbo].[Roles] ([RoleId], [Name], [isActive], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyId]) VALUES (2, N'Team/Project Manager', 1, N'', N'1', CAST(N'2016-12-02 13:04:25.1216889' AS DateTime2), NULL, NULL, 0)
GO
INSERT [dbo].[Roles] ([RoleId], [Name], [isActive], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyId]) VALUES (3, N'General User', 1, N'', N'1', CAST(N'2016-12-02 13:04:25.1216889' AS DateTime2), NULL, NULL, 0)
GO
INSERT [dbo].[Roles] ([RoleId], [Name], [isActive], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyId]) VALUES (4, N'Freelancer', 1, N'', N'1', CAST(N'2016-12-02 13:04:25.1216889' AS DateTime2), NULL, NULL, 0)
GO
INSERT [dbo].[Roles] ([RoleId], [Name], [isActive], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyId]) VALUES (5, N'Financial Manager', 1, NULL, N'1', CAST(N'2016-10-20 16:41:01.0641977' AS DateTime2), NULL, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
SET IDENTITY_INSERT [dbo].[SettingConfigs] ON 

GO
INSERT [dbo].[SettingConfigs] ([Id], [NotificationName], [NotificationDescription], [NotificationText], [SettingSectionId], [CreatedBy], [CreatedDate], [IsActive]) VALUES (1, N'Subscription Expiry', N'This is Description about Subscription Expiry', N'Subscription Expiry', 1, NULL, NULL, 1)
GO
INSERT [dbo].[SettingConfigs] ([Id], [NotificationName], [NotificationDescription], [NotificationText], [SettingSectionId], [CreatedBy], [CreatedDate], [IsActive]) VALUES (2, N'Team Creation', N'This is Description about Team Creation', N'Team Creation', 1, NULL, NULL, 1)
GO
INSERT [dbo].[SettingConfigs] ([Id], [NotificationName], [NotificationDescription], [NotificationText], [SettingSectionId], [CreatedBy], [CreatedDate], [IsActive]) VALUES (3, N'Team Update', N'This is Description about Team Update', N'Team Update', 1, NULL, NULL, 1)
GO
INSERT [dbo].[SettingConfigs] ([Id], [NotificationName], [NotificationDescription], [NotificationText], [SettingSectionId], [CreatedBy], [CreatedDate], [IsActive]) VALUES (4, N'Project Creation', N'This is Description about Project Creation', N'Project Creation', 1, NULL, NULL, 1)
GO
INSERT [dbo].[SettingConfigs] ([Id], [NotificationName], [NotificationDescription], [NotificationText], [SettingSectionId], [CreatedBy], [CreatedDate], [IsActive]) VALUES (5, N'Project Update', N'This is Description about Project Update', N'Project Update', 1, NULL, NULL, 1)
GO
INSERT [dbo].[SettingConfigs] ([Id], [NotificationName], [NotificationDescription], [NotificationText], [SettingSectionId], [CreatedBy], [CreatedDate], [IsActive]) VALUES (6, N'Invoice Raise', N'This is Description about Invoice Raise', N'Invoice Raise', 1, NULL, NULL, 1)
GO
INSERT [dbo].[SettingConfigs] ([Id], [NotificationName], [NotificationDescription], [NotificationText], [SettingSectionId], [CreatedBy], [CreatedDate], [IsActive]) VALUES (7, N'Todo Update', N'This is Description about Todo Update', N'Todo Update', 1, NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[SettingConfigs] OFF
GO
SET IDENTITY_INSERT [dbo].[SettingSections] ON 

GO
INSERT [dbo].[SettingSections] ([Id], [SectionName], [SectionDescription], [IsActive]) VALUES (1, N'Email Notification', N'This is about email notification', 1)
GO
INSERT [dbo].[SettingSections] ([Id], [SectionName], [SectionDescription], [IsActive]) VALUES (2, N'Messages', N'This is about messages', 1)
GO
SET IDENTITY_INSERT [dbo].[SettingSections] OFF
GO
SET IDENTITY_INSERT [dbo].[Tags] ON 

GO
INSERT [dbo].[Tags] ([Id], [TagName], [TagDescription], [TagType], [IsActive], [Priority]) VALUES (1, N'Asp.Net', N'this is Asp.Net', 1, NULL, NULL)
GO
INSERT [dbo].[Tags] ([Id], [TagName], [TagDescription], [TagType], [IsActive], [Priority]) VALUES (3, N'Angular', N'this is Angular', 1, NULL, NULL)
GO
INSERT [dbo].[Tags] ([Id], [TagName], [TagDescription], [TagType], [IsActive], [Priority]) VALUES (4, N'MVC', N'this is MVC', 1, NULL, NULL)
GO
INSERT [dbo].[Tags] ([Id], [TagName], [TagDescription], [TagType], [IsActive], [Priority]) VALUES (5, N'Mean', N'this is Mean', 1, NULL, NULL)
GO
INSERT [dbo].[Tags] ([Id], [TagName], [TagDescription], [TagType], [IsActive], [Priority]) VALUES (6, N'NOde', N'this is NOde', 1, NULL, NULL)
GO
INSERT [dbo].[Tags] ([Id], [TagName], [TagDescription], [TagType], [IsActive], [Priority]) VALUES (7, N'EMarketing', N'this is EMarketing', 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Tags] OFF
GO
