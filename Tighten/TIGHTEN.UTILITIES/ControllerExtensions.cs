﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;

namespace TIGHTEN.UTILITIES
{
    public static class ControllerExtensions
    {
        public static JsonResponse JsonSuccess(string message = null)
        {
            return new JsonResponse(true) { message = message };
        }

        public static JsonResponse JsonError(string message = null)
        {
            return new JsonResponse(false) { message = message };
        }
        public static JsonResponse JsonModelErrorAPI(ApiController controller)
        {
            return new JsonResponse(false)
            {
                errors = controller.ModelState.SelectMany(x => x.Value.Errors.Select(error => error.ErrorMessage))
            };
        }

        public static JsonResponse JsonCustomError(object Error = null, string message = null)
        {
            return new JsonResponse(false)
            {
                errors = Error,
                message = message
            };
        }

        public static JsonResponse JsonCustomSuccess(object ResponseData = null, string message = null)
        {
            return new JsonResponse(true)
            {
                ResponseData = ResponseData,
                message = message
            };
        }


    }
}
