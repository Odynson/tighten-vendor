﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.UTILITIES
{
    public  class EmailUtility
    {


        public static async Task<bool> Send(string RecieverEmail, string emailsubject, string emailbody)
        {

            try
            {
                MailMessage md = new MailMessage();

                md.To.Add(new MailAddress(RecieverEmail));

                md.From = new MailAddress(appSettingValue("fromEmail"));
                md.Subject = emailsubject;

                md.Body = emailbody;
                md.IsBodyHtml = true;

                //// smtp settings
                Object mailState = md;
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = appSettingValue("smtpHost");
                    smtp.Port = Convert.ToInt32(appSettingValue("smtpPort"));
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(appSettingValue("smtpEmail"),
                        appSettingValue("smtpPassword"));
                    smtp.Timeout = 20000;
                }
                smtp.SendCompleted += new SendCompletedEventHandler(OnSendCompleted);

                await smtp.SendMailAsync(md);
                return true;
            }
            catch (Exception)
            {
                return true;
            }
        }


        public static void SendMailInThread(string RecieverEmail, string emailsubject, string emailbody)
        {

            try
            {
                MailMessage md = new MailMessage();

                md.To.Add(new MailAddress(RecieverEmail));

                md.From = new MailAddress(appSettingValue("fromEmail"));
                md.Subject = emailsubject;

                md.Body = emailbody;
                md.IsBodyHtml = true;

                //// smtp settings
                Object mailState = md;
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = appSettingValue("smtpHost");
                    smtp.Port = Convert.ToInt32(appSettingValue("smtpPort"));
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(appSettingValue("smtpEmail"),
                        appSettingValue("smtpPassword"));
                    smtp.Timeout = 20000;
                }
                smtp.SendCompleted += new SendCompletedEventHandler(OnSendCompleted);

                smtp.SendMailAsync(md);
                //await smtp.SendMailAsync(md);
                //return true;
            }
            catch (Exception)
            {
                //return true;
            }
        }


        public class ProjectFileModel
        {
            public string FilePath { get; set; }
            public string FileName { get; set; }
            public string ActualFileName { get; set; }
        }


        public static void SendMailInThreadWithAttachment(string RecieverEmail, string emailsubject, string emailbody,List<ProjectFileModel> FilePathList)
        {

            try
            {
                MailMessage md = new MailMessage();

                md.To.Add(new MailAddress(RecieverEmail));

                md.From = new MailAddress(appSettingValue("fromEmail"));
                md.Subject = emailsubject;

                md.Body = emailbody;
                md.IsBodyHtml = true;

                System.Net.Mail.Attachment attachment;
                foreach (var item in FilePathList)
                {
                    attachment = new System.Net.Mail.Attachment(item.FilePath);
                    attachment.Name = item.ActualFileName;
                    md.Attachments.Add(attachment);
                }
                

                //// smtp settings
                Object mailState = md;
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = appSettingValue("smtpHost");
                    smtp.Port = Convert.ToInt32(appSettingValue("smtpPort"));
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(appSettingValue("smtpEmail"),
                        appSettingValue("smtpPassword"));
                    smtp.Timeout = 20000;
                }
                smtp.SendCompleted += new SendCompletedEventHandler(OnSendCompleted);

                smtp.SendMailAsync(md);
                //await smtp.SendMailAsync(md);
                //return true;
            }
            catch (Exception)
            {
                //return true;
            }
        }


        public static string appSettingValue(string key)
        {
            return System.Web.Configuration.WebConfigurationManager.AppSettings[key];
        }


        static void OnSendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            MailMessage mailMessage = e.UserState as MailMessage;
            if (!e.Cancelled && e.Error != null)
            {
                //Response.Write("Email sent successfully");
            }
            else
            {
                //Response.Write(e.Error.Message);
                //Response.Write(e.Error.StackTrace);
            }

        }

        public static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
    }
}

