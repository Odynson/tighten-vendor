﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.UTILITIES
{
    public class JsonResponse
    {
        public JsonResponse()
        {
        }
        public JsonResponse(bool isSuccess)
        {
            success = isSuccess;
        }
        public bool success { get; set; }
        public string message { get; set; }
        public object ResponseData { get; set; }
        public object errors { get; set; }
        
    }
}
