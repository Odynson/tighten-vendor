﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIGHTEN.UTILITIES
{
	public static class Utility {
		public static string AbsoluteUrl(string relativeUrl) {
			if (string.IsNullOrEmpty(relativeUrl))
				return relativeUrl;

			var httpContext =   HttpContext.Current;
			if (httpContext == null)
				return relativeUrl;

			if (relativeUrl.StartsWith("/"))
				relativeUrl = relativeUrl.Insert(0, "~");
			if (!relativeUrl.StartsWith("~/"))
				relativeUrl = relativeUrl.Insert(0, "~/");

			var url = httpContext.Request.Url;
			var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

			return String.Format("{0}://{1}{2}{3}",
				url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
		}

        public static string ConvertMinutesToFormatedTimeLogString(int TotalMinutes)
        {
            string FormatedString = "";
            int Hours = TotalMinutes / 60;
            int Minutes = TotalMinutes % 60;
            FormatedString = Hours + "h " + Minutes + "m";
            return FormatedString;
        }

        public static int ConvertTimeLogStringToFormatedMinutes(string TimeLoged)
        {

            int TotalTimeDuration = 0;

            if (TimeLoged != string.Empty && TimeLoged != null)
            {
                TimeLoged = TimeLoged.Trim().ToLower();

                if (TimeLoged.Contains("h"))
                {
                    string[] TotalTimeArray = TimeLoged.Split('h');
                    string TotalHours = TotalTimeArray[0];
                    string TotalMinutes = TotalTimeArray[1] == string.Empty ? "0" : TotalTimeArray[1].Replace("m", "");

                    int Hours;
                    if (int.TryParse(TotalHours, out Hours))
                    {
                        TotalTimeDuration = (Hours * 60);
                        int Minutes;
                        if (int.TryParse(TotalMinutes, out Minutes))
                        {
                            TotalTimeDuration += Minutes;
                        }
                    }
                }
                else if (TimeLoged.Contains("m"))
                {
                    TimeLoged = TimeLoged.Replace("m", "");
                    int Minutes;
                    if (int.TryParse(TimeLoged, out Minutes))
                    {
                        TotalTimeDuration = Minutes;
                    }
                }

            }


            //var Array = TimeLoged.ToLower().Split('h');
            //var hours = Convert.ToInt32(Array[0].Trim());
            //var minutes = Convert.ToInt32(((Array[1].Replace("m", "").Trim())==""?"0": (Array[1].Replace("m", "").Trim())));
            //TotalMinutes = (hours * 60) + minutes;

            return TotalTimeDuration;
        }



    }
}