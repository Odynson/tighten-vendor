﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class TodoNotification : BaseClass
    {
        AppContext dbcontext;
        public List<TodoNotificationModel.TodoNotificationDetailModel> GetTodoNotifications(int TodoId)
        {
            //dbcontext = new AppContext();
            //List<TodoNotificationModel.TodoNotificationDetailModel> notObject = (from m in dbcontext.TodoNotifications
            //                                                                     join m1 in dbcontext.UserDetails on m.CreatedBy equals m1.UserId

            //                                                                     where m.TodoId == TodoId && m.Type == 1 && m1.IsDeleted == false

            //                                                                     select new TodoNotificationModel.TodoNotificationDetailModel
            //                                                                     {
            //                                                                         Id = m.Id,
            //                                                                         ConcernedSectionId = m.TodoId,
            //                                                                         ConcernedSectionName = m.Description,
            //                                                                         FollowerName = "",
            //                                                                         FollowerProfilePhoto = "",
            //                                                                         FilePath = "#",
            //                                                                         CreatorUserId = m1.UserId,
            //                                                                         CreatorName = m1.FirstName + " " + (string.IsNullOrEmpty(m1.LastName) ? "" : m1.LastName),
            //                                                                         CreatorProfilePhoto = m1.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m1.ProfilePhoto,
            //                                                                         Description = m.Description,
            //                                                                         Type = 1,
            //                                                                         CreatedDate = m.CreatedDate
            //                                                                     }

            //                                                               ).Union(from m in dbcontext.TodoNotifications
            //                                                                       join m1 in dbcontext.ToDoAttachments on m.AttachmentId equals m1.Id
            //                                                                       join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId

            //                                                                       where m.TodoId == TodoId && m.Type == 2 && m2.IsDeleted == false

            //                                                                       select new TodoNotificationModel.TodoNotificationDetailModel
            //                                                                       {
            //                                                                           Id = m.Id,
            //                                                                           ConcernedSectionId = m1.Id,
            //                                                                           ConcernedSectionName = m1.OriginalName,
            //                                                                           FollowerName = "",
            //                                                                           FollowerProfilePhoto = "",
            //                                                                           FilePath = m1.Path,
            //                                                                           CreatorUserId = m2.UserId,
            //                                                                           CreatorName = m2.FirstName + " " + (string.IsNullOrEmpty(m2.LastName) ? "" : m2.LastName),
            //                                                                           CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                                           Description = m.Description,
            //                                                                           Type = 2,
            //                                                                           CreatedDate = m.CreatedDate
            //                                                                       }

            //                                                                       ).Union(from m in dbcontext.TodoNotifications
            //                                                                               join m1 in dbcontext.UserDetails on m.ConcernedUserId equals m1.UserId
            //                                                                               join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId
            //                                                                               where m.TodoId == TodoId && m.Type == 3 && m1.IsDeleted == false && m2.IsDeleted == false
            //                                                                               select new TodoNotificationModel.TodoNotificationDetailModel
            //                                                                               {
            //                                                                                   Id = m.Id,
            //                                                                                   ConcernedSectionId = m1.Id,
            //                                                                                   ConcernedSectionName = "",

            //                                                                                   FollowerName = m1.FirstName + " " + (string.IsNullOrEmpty(m1.LastName) ? "" : m1.LastName),
            //                                                                                   FollowerProfilePhoto = m1.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m1.ProfilePhoto,
            //                                                                                   FilePath = "#",
            //                                                                                   CreatorUserId = m2.UserId,
            //                                                                                   CreatorName = m2.FirstName + " " + m2.LastName,
            //                                                                                   CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                                                   Description = m.Description,
            //                                                                                   Type = 3,
            //                                                                                   CreatedDate = m.CreatedDate
            //                                                                               }) 

            //                                                                               .Union(from m in dbcontext.TodoNotifications
            //                                                                                        join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId

            //                                                                                        where m.TodoId == TodoId && m.Type == 4 && m2.IsDeleted == false

            //                                                                                        select new TodoNotificationModel.TodoNotificationDetailModel
            //                                                                                        {
            //                                                                                            Id = m.Id,
            //                                                                                            ConcernedSectionId = m.Id,
            //                                                                                            ConcernedSectionName = "",
            //                                                                                            FollowerName = "",
            //                                                                                            FollowerProfilePhoto = "",
            //                                                                                            FilePath = "#",
            //                                                                                            CreatorUserId = m2.UserId,
            //                                                                                            CreatorName = m2.FirstName + " " + (string.IsNullOrEmpty(m2.LastName) ? "" : m2.LastName),
            //                                                                                            CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                                                            Description = m.Description,
            //                                                                                            Type = 4,
            //                                                                                            CreatedDate = m.CreatedDate
            //                                                                                        }).OrderByDescending(x => x.CreatedDate).ToList();


            List<TodoNotificationModel.TodoNotificationDetailModel> notObject;
            using (var con = new SqlConnection(ConnectionString))
            {
                notObject = con.Query<TodoNotificationModel.TodoNotificationDetailModel>("usp_GetTodoNotifications", new { TodoId = TodoId }, commandType: CommandType.StoredProcedure).ToList();
            }

            notObject = notObject.OrderByDescending(x => x.CreatedDate).ToList();

            return notObject;

        }


    }
}
