﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class UserToDoApproval : BaseClass
    {
        AppContext dbcontext;

        //public List<TodoApproval> getTodos(TodoApprovalSearch search)
        //{
        //    search.From = (search.From == DateTime.MinValue ? DateTime.Now.AddDays(-7) : search.From);
        //    search.To = (search.To == DateTime.MinValue ? DateTime.Now : search.To);
        //    //Query to search based on different columns 
        //    //from o in context.Orders.include("customers")
        //    //where o.city == (city == null ? o.city : city) && o.firstname == (firstname == null ? o.firstname : firstname)
        //    //select o;
        //    List<TodoApproval> Todos = (from c in dbcontext.ToDos
        //                                join c3 in dbcontext.UserDetails on c.AssigneeId equals c3.UserId
        //                                join pu in dbcontext.ProjectUsers on c.AssigneeId equals pu.UserId
        //                                join p in dbcontext.Projects on pu.ProjectId equals p.Id
        //                                //where c3.CompanyId == search.CompanyId //&& c.IsDone == true//
        //                                where p.Id == (search.ProjectId == 0 ? p.Id : search.ProjectId)
        //                                && c3.UserId == (string.IsNullOrEmpty(search.AssigneeId) ? c3.UserId : search.AssigneeId)
        //                                && (c.ModifiedDate >= search.From.Date
        //                                && c.ModifiedDate <= search.To.Date)
        //                                && c.TaskType == 5

        //                                orderby c.ModifiedDate descending

        //                                let AssigneeProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
        //                                let AssigneeName = c3.FirstName //+' ' +( string.IsNullOrEmpty( c2.LastName)?"": c2.LastName)

        //                                select new TodoApproval
        //                                {
        //                                    Name = c.Name,
        //                                    SectionId = c.SectionId,
        //                                    TodoId = c.Id,
        //                                    AssigneeId = c.AssigneeId,
        //                                    AssigneeProfilePhoto = AssigneeProfilePhoto,
        //                                    AssigneeName = AssigneeName,
        //                                    AssigneeEmail = c3.Email,
        //                                    //ReporterId = c3.UserId,
        //                                    //ReporterEmail = c3.Email,
        //                                    //ReporterName = ReporterName,
        //                                    //ReporterProfilePhoto = ReporterProfilePhoto,
        //                                    Description = c.Description,
        //                                    DeadlineDate = c.DeadlineDate,
        //                                    InProgress = c.InProgress,
        //                                    IsDone = c.IsDone,
        //                                    //SectionName = t1.SectionName,
        //                                    //TeamId = t0.Id,
        //                                    //TeamName = t0.TeamName,
        //                                    //ProjectId = t.Id,
        //                                    ProjectName = p.Name,
        //                                    CreatedDate = c.CreatedDate,
        //                                    ModifiedDate = c.ModifiedDate,
        //                                    LoggedTime = c.LoggedTime,
        //                                    EstimatedTime = c.EstimatedTime,
        //                                    IsApproved = c.IsApproved,
        //                                    ApprovedBy = c.ApprovedBy,
        //                                    Reason = c.Reason,
        //                                    TodoType = (c.TaskType == 5 ? "PMS Task" : c.TaskType == 4 ? "Admin Duties" : c.TaskType == 3 ? "Meeting" : c.TaskType == 2 ? "Research & Development" : "phone Calls")
        //                                }).ToList();

        //    return Todos;
        //}

        /// <summary>
        /// This function fetches my Todos for MyTOdos Tab
        /// </summary>
        /// <param name="UserId">It is unique id of logged in user</param>
        /// <returns>returns todos in list</returns>
        /// 
        public List<TodoApproval> getTodos(TodoApprovalSearch search)
        {
            //search.ProjectId = 0;
            search.From = (search.From == DateTime.MinValue ? DateTime.Now.AddDays(-7) : search.From);
            search.To = (search.To == DateTime.MinValue ? DateTime.Now : search.To);
            //Query to search based on different columns 
            //from o in context.Orders.include("customers")
            //where o.city == (city == null ? o.city : city) && o.firstname == (firstname == null ? o.firstname : firstname)
            //select o;
            var TodosList = (from AllTodos in (from c in dbcontext.ToDos.AsEnumerable()
                             join sec in dbcontext.Sections on c.SectionId equals sec.Id
                             join p in dbcontext.Projects on sec.ProjectId equals p.Id
                             join pu in dbcontext.ProjectUsers on p.Id equals pu.ProjectId
                             join c3 in dbcontext.UserDetails on pu.UserId equals c3.UserId
                             
                             //where c3.CompanyId == search.CompanyId //&& c.IsDone == true//
                             where p.Id == (search.ProjectId == 0 ? p.Id : search.ProjectId)
                             && (search.CompanyId == 0 ? p.CompanyId == p.CompanyId : p.CompanyId == search.CompanyId )
                             && c3.UserId == (string.IsNullOrEmpty(search.AssigneeId) ? c3.UserId : search.AssigneeId)
                             && c.AssigneeId == (string.IsNullOrEmpty(search.AssigneeId) ? c.AssigneeId : search.AssigneeId)
                             && (c.ModifiedDate.Date >= search.From.Date
                             && c.ModifiedDate.Date <= search.To.Date)
                             && c.IsDone == true && c.IsDeleted == false
                             && c.TaskType == (search.TaskType == 0 ? c.TaskType : search.TaskType)
                             && (search.TaskStatus == 1 ? (c.IsApproved == false && c.ApprovedBy == null) : search.TaskStatus == 2 ? (c.IsApproved == true && c.ApprovedBy != null) : search.TaskStatus == 3 ? (c.IsApproved == false && c.ApprovedBy != null) : (c.IsApproved == c.IsApproved))
                             && c.AssigneeId != c.ReporterId
                             && c3.IsDeleted == false && p.IsDeleted == false
                                               //&& c.TaskType == 5
                                               orderby c.ModifiedDate descending

                             let AssigneeProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
                             let AssigneeName = c3.FirstName //+' ' +( string.IsNullOrEmpty( c2.LastName)?"": c2.LastName)

                             select new {c,c3,pu,p, AssigneeProfilePhoto, AssigneeName }
                             )

                             group AllTodos by new { c_Name = AllTodos.c.Name, c_SectionId = AllTodos.c.SectionId, c_Id =  AllTodos.c.Id,c_AssigneeId = AllTodos.c.AssigneeId,
                                 AssigneeProfilePhoto = AllTodos.AssigneeProfilePhoto,AssigneeName =  AllTodos.AssigneeName,c3_Email = AllTodos.c3.Email,c_Description = AllTodos.c.Description,
                                 c_DeadlineDate = AllTodos.c.DeadlineDate,c_InProgress = AllTodos.c.InProgress,c_IsDone = AllTodos.c.IsDone,p_Name = AllTodos.p.Name,
                                 c_CreatedDate = AllTodos.c.CreatedDate,c_ModifiedDate = AllTodos.c.ModifiedDate,c_LoggedTime = AllTodos.c.LoggedTime,c_EstimatedTime = AllTodos.c.EstimatedTime,
                                 c_IsApproved = AllTodos.c.IsApproved,c_ApprovedBy = AllTodos.c.ApprovedBy,c_Reason = AllTodos.c.Reason,
                                 c_TaskType = AllTodos.c.TaskType

                             }   into filteredTodo

                             select new
                             {
                                 Name = filteredTodo.Key.c_Name,
                                 SectionId = filteredTodo.Key.c_SectionId,
                                 TodoId = filteredTodo.Key.c_Id,
                                 AssigneeId = filteredTodo.Key.c_AssigneeId,
                                 AssigneeProfilePhoto = filteredTodo.Key.AssigneeProfilePhoto,
                                 AssigneeName = filteredTodo.Key.AssigneeName,
                                 AssigneeEmail = filteredTodo.Key.c3_Email,
                                 Description = filteredTodo.Key.c_Description,
                                 DeadlineDate = filteredTodo.Key.c_DeadlineDate,
                                 InProgress = filteredTodo.Key.c_InProgress,
                                 IsDone = filteredTodo.Key.c_IsDone,
                                 ProjectName = filteredTodo.Key.p_Name,
                                 CreatedDate = filteredTodo.Key.c_CreatedDate,
                                 ModifiedDate = filteredTodo.Key.c_ModifiedDate,
                                 LoggedTime = filteredTodo.Key.c_LoggedTime,
                                 EstimatedTime = filteredTodo.Key.c_EstimatedTime,
                                 IsApproved = filteredTodo.Key.c_IsApproved,
                                 ApprovedBy = filteredTodo.Key.c_ApprovedBy,
                                 Reason = filteredTodo.Key.c_Reason,
                                 TodoType = (filteredTodo.Key.c_TaskType == 5 ? "PMS Task" : filteredTodo.Key.c_TaskType == 4 ? "Admin Duties" : filteredTodo.Key.c_TaskType == 3 ? "Meeting" : filteredTodo.Key.c_TaskType == 2 ? "Research & Development" : "phone Calls"),
                                 TodoDetails = (from tl in dbcontext.TodoTimeLogs
                                                where tl.TodoId == filteredTodo.Key.c_Id
                                                select new TodoDetail
                                                {
                                                    Id = tl.Id,
                                                    TodoId = tl.TodoId,
                                                    IsAutomaticLogged = tl.IsAutomaticLogged,
                                                    Duration = tl.Duration,
                                                    loggingTime = tl.loggingTime,
                                                    StartDateTime = tl.StartDateTime,
                                                    StopDateTime = tl.StopDateTime,
                                                })
                             });

            List<TodoApproval> Todos = TodosList.AsEnumerable().Select(item =>
                                                            new TodoApproval()
                                                            {
                                                                Name = item.Name,
                                                                SectionId = item.SectionId,
                                                                TodoId = item.TodoId,
                                                                AssigneeId = item.AssigneeId,
                                                                AssigneeProfilePhoto = item.AssigneeProfilePhoto,
                                                                AssigneeName = item.AssigneeName,
                                                                AssigneeEmail = item.AssigneeEmail,
                                                                Description = item.Description,
                                                                DeadlineDate = item.DeadlineDate,
                                                                InProgress = item.InProgress,
                                                                IsDone = item.IsDone,
                                                                ProjectName = item.ProjectName,
                                                                CreatedDate = item.CreatedDate,
                                                                ModifiedDate = item.ModifiedDate,
                                                                LoggedTime = item.LoggedTime,
                                                                EstimatedTime = item.EstimatedTime,
                                                                IsApproved = item.IsApproved,
                                                                ApprovedBy = item.ApprovedBy,
                                                                Reason = item.Reason,
                                                                TodoType = item.TodoType,
                                                                TodoDetails = item.TodoDetails.ToList()
                                                            }).ToList();
            return Todos;
        }

        /// <summary>
        ///Return the list of the user of the particular company 
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        /// 
        public List<ProjectResult> UserProjects(int CompanyId, string UserId)
        {
            List<ProjectResult> ProjectList = (from project in dbcontext.Projects
                                               join user in dbcontext.ProjectUsers on project.Id equals user.ProjectId
                                               where user.UserId == UserId && project.IsDeleted == false
                                               && (CompanyId == 0 ? project.CompanyId == project.CompanyId : project.CompanyId == CompanyId )
                                               select new ProjectResult
                                               {
                                                   Name = project.Name,
                                                   Value = project.Id
                                               }).ToList();
            //CompanyUserList.Insert(0, new ProjectResult() { Name = "All", Value = 0 });
            return ProjectList;
        }

        // this is to delete the timelog entry
        public string DeleteUserLoggedTimeSlot(int TodoDetailId)
        {
            int sumOfTime = 0;
            string Time = "";
            TodoTimeLogs TodoTimeLog = (from e in dbcontext.TodoTimeLogs
                                        where e.Id == TodoDetailId
                                        select e).SingleOrDefault();
            int todoId = TodoTimeLog.TodoId.HasValue ? TodoTimeLog.TodoId.Value : 0;
            string userId = TodoTimeLog.StartBy;
            /*  Delete ToDo  */
            dbcontext.TodoTimeLogs.Remove(TodoTimeLog);
            dbcontext.SaveChanges();
            if (todoId != 0)
            {
                sumOfTime = (from t in dbcontext.TodoTimeLogs where t.TodoId == todoId && t.StartBy == userId select t.Duration).Sum();
                // assigning the converted time string
                ToDo todo = dbcontext.ToDos.Where(x => x.Id == todoId && x.IsDeleted == false).SingleOrDefault();
                todo.LoggedTime = Utility.ConvertMinutesToFormatedTimeLogString(sumOfTime);
                Time = todo.LoggedTime;
                dbcontext.SaveChanges();
            }
            return Time;
        }

        // edit hours of the manual logging of the task
        public string UpdateUserLoggedTimeSlot(string UserId, int TodoDetailId, int minutes)
        {
            int sumOfTime = 0;
            string Time = "";
            TodoTimeLogs TodoTimeLog = (from e in dbcontext.TodoTimeLogs
                                        where e.Id == TodoDetailId
                                        select e).SingleOrDefault();
            int todoId = TodoTimeLog.TodoId.HasValue ? TodoTimeLog.TodoId.Value : 0;
            string userId = TodoTimeLog.StartBy;
            TodoTimeLog.Duration = minutes;
            // updating the logging time of the time entry
            TodoTimeLog.loggingTime = DateTime.Now;
            dbcontext.SaveChanges();
            if (todoId != 0)
            {
                sumOfTime = (from t in dbcontext.TodoTimeLogs where t.TodoId == todoId && t.StartBy == userId select t.Duration).Sum();
                // assigning the converted time string
                ToDo todo = dbcontext.ToDos.Where(x => x.Id == todoId && x.IsDeleted == false).SingleOrDefault();
                todo.ModifiedDate = DateTime.Now;
                todo.LoggedTime = Utility.ConvertMinutesToFormatedTimeLogString(sumOfTime);
                Time = todo.LoggedTime;
                dbcontext.SaveChanges();
            }
            return Time;
        }

        public List<ProjectResult> getUserProjectsForAllCompanies(string UserId, int CompanyId)
        {
            //List<ProjectResult> ProjectList = (from prjct in dbcontext.Projects
            //                                   join user in dbcontext.ProjectUsers on project.Id equals user.ProjectId
            //                                   where user.UserId == UserId && project.IsDeleted == false
            //                                   && (project.CompanyId == 0 ? project.CompanyId == project.CompanyId : project.CompanyId == CompanyId)
            //                                   select new ProjectResult
            //                                   {
            //                                       Name = project.Name ,
            //                                       Value = project.Id
            //                                   }).ToList();


            List<ProjectResult> ProjectList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select prjct.Name , prjct.Id as Value  from Projects prjct inner join ProjectUsers usr on prjct.Id = usr.ProjectId where usr.UserId = @UserId and prjct.IsDeleted = 0  
                                    and prjct.CompanyId = case when @CompanyId = 0 then prjct.CompanyId  else @CompanyId end         ";
                ProjectList = con.Query<ProjectResult>(sqlquery, new { UserId = UserId , CompanyId = CompanyId }).ToList();
            }



            return ProjectList;
        }

        public List<ProjectResult> getUserCompanies(string UserId)
        {
            //List<ProjectResult> CompanyList = (from cmpny in dbcontext.Companies
            //                                   join UCR in dbcontext.UserCompanyRoles on cmpny.Id equals UCR.CompanyId
            //                                   join usr_detail in dbcontext.UserDetails on cmpny.Id equals usr_detail.CompanyId
            //                                   where UCR.UserId == UserId && usr_detail.IsDeleted == false
            //                                   && usr_detail.IsFreelancer == false
            //                                   select new ProjectResult
            //                                   {
            //                                    Name = cmpny.Name,
            //                                    Value = cmpny.Id   
            //                                   }).Distinct().ToList();

            List<ProjectResult> CompanyList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select Distinct cmpny.Name,cmpny.Id as Value  from  Companies cmpny inner join UserCompanyRoles UCR on cmpny.Id = UCR.CompanyId inner join UserDetails usr_detail on cmpny.Id = usr_detail.CompanyId 
                                where UCR.UserId = @UserId and usr_detail.IsDeleted = 0 and usr_detail.IsFreelancer = 0";
                CompanyList = con.Query<ProjectResult>(sqlquery, new { UserId = UserId }).ToList();
            }

            
            
            return CompanyList;
        }

    }
}
