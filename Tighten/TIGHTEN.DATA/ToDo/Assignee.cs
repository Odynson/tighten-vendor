﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class Assignee : BaseClass
    {
        AppContext dbcontext;

        public void UpdateAssignee(string UserId, AssigneeModel.AssigneeUpdateModel model)
        {

            //ToDo todoEntity = (from m in dbcontext.ToDos where m.Id == model.TodoId && m.IsDeleted == false select m).SingleOrDefault();

            //todoEntity.ModifiedBy = UserId;
            //todoEntity.ModifiedDate = DateTime.Now;
            //todoEntity.AssigneeId = model.AssigneeId;

            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update ToDoes set ModifiedBy = @UserId,ModifiedDate = GETDATE(),AssigneeId = @AssigneeId where Id = @TodoId and IsDeleted = 0 ";
                con.Execute(sqlquery, new { UserId = UserId, AssigneeId = model.AssigneeId, TodoId = model.TodoId });
            }

        }

    }
}
