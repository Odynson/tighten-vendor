﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class ToDoAttachments : BaseClass
    {
        AppContext dbcontext;
        /// <summary>
        /// This function fetches Attachments for particular Todo
        /// </summary>
        /// <param name="ToDoId">It is unique id of Todo</param>
        /// <param name="userId">It is unique id of logged in user</param>
        /// <returns>List of attachments related to particular Todo</returns>
        public List<AttachmentModel.AttachmentDetailsModel> getAttachments(int ToDoId, string userId)
        {
            //List<AttachmentModel.AttachmentDetailsModel> attachmentsObject = (from p in dbcontext.ToDoAttachments
            //                                                                  join p1 in dbcontext.UserDetails on p.CreatedBy equals p1.UserId
            //                                                                  where p.ToDoId == ToDoId && p1.IsDeleted == false
            //                                                                  orderby p.CreatedDate descending
            //                                                                  let Id = (p.CreatedBy == userId ? p.Id : 0)
            //                                                                  let SourceImage = ("img/source/" + p.SourceImage)
            //                                                                  let CreatorProfilePhoto = (p1.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + p1.ProfilePhoto)

            //                                                                  select new AttachmentModel.AttachmentDetailsModel
            //                                                                  {
            //                                                                      Id = Id,
            //                                                                      CreatorName = p1.FirstName + " " + p1.LastName,
            //                                                                      CreatorUserId = p1.UserId,
            //                                                                      CreatorProfilePhoto = CreatorProfilePhoto,
            //                                                                      Name = p.Name,
            //                                                                      OriginalName = p.OriginalName,
            //                                                                      Path = p.Path,
            //                                                                      SourceImage = SourceImage,
            //                                                                      CreatedDate = p.CreatedDate

            //                                                                  }).ToList();


            List<AttachmentModel.AttachmentDetailsModel> attachmentsObject;
            using (var con = new SqlConnection(ConnectionString))
            {
                attachmentsObject = con.Query<AttachmentModel.AttachmentDetailsModel>("usp_GetAttachments", new { userId= userId, ToDoId= ToDoId }, commandType: CommandType.StoredProcedure).ToList();
            }

            return attachmentsObject;
        }



        /// <summary>
        /// This funtion fetches attachments related to a Project
        /// </summary>
        /// <param name="ProjectId">It is unique Id of Project</param>
        /// <returns>List of attachemnts of a particular project</returns>
        public List<AttachmentModel.AttachmentDetailsModel> getAttachmentsForParticularProject(int ProjectId)
        {
            //List<AttachmentModel.AttachmentDetailsModel> result = (from p in dbcontext.ToDoAttachments
            //                                                       join p1 in dbcontext.ToDos on p.ToDoId equals p1.Id
            //                                                       join p3 in dbcontext.Sections on p1.SectionId equals p3.Id
            //                                                       join p2 in dbcontext.Projects on p3.ProjectId equals p2.Id
            //                                                       join p4 in dbcontext.UserDetails on p.CreatedBy equals p4.UserId
            //                                                       where p2.Id == ProjectId && p4.IsDeleted == false
            //                                                       && p2.IsDeleted == false && p1.IsDeleted == false
            //                                                       orderby p.CreatedDate descending
            //                                                       let SourceImage = ("img/source/" + p.SourceImage)

            //                                                       select new AttachmentModel.AttachmentDetailsModel
            //                                                       {
            //                                                           Id = p.Id,
            //                                                           CreatorName = p4.FirstName + " " + p4.LastName,
            //                                                           CreatorProfilePhoto = p4.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + p4.ProfilePhoto,
            //                                                           Name = p.Name,
            //                                                           OriginalName = p.OriginalName,
            //                                                           Path = p.Path,
            //                                                           SourceImage = SourceImage,
            //                                                           CreatedDate = p.CreatedDate,
            //                                                           TodoName=p1.Name,
            //                                                           TodoId=p1.Id

            //                                                       }).ToList();


            List<AttachmentModel.AttachmentDetailsModel> result;
            using (var con = new SqlConnection(ConnectionString))
            {
                result = con.Query<AttachmentModel.AttachmentDetailsModel>("usp_GetAttachmentsForParticularProject", new { ProjectId = ProjectId }, commandType: CommandType.StoredProcedure).ToList();
            }


            return result;
        }

        public string saveToDoAttachments(string AttachmentType, int ToDoId, string AttachmentName, string OriginalName, string userId,string SourceImage)
        {
            //string[] TodoFollowers = (from m in dbcontext.Projects
            //                          join m1 in dbcontext.Sections on m.Id equals m1.ProjectId
            //                          join m2 in dbcontext.ToDos on m1.Id equals m2.SectionId
            //                          join m3 in dbcontext.Followers on m2.Id equals m3.TodoId
            //                          where m2.Id == ToDoId && m.IsDeleted == false
            //                          && m2.IsDeleted == false
            //                          select m3.UserId).ToArray();

            //string AttachmentPath = AttachmentName;
            //if (AttachmentType == "Local")
            //{
            //    AttachmentPath = "Uploads/Attachments/ToDo/" + AttachmentName;
            //}

            //ToDoAttachment todoattachment = new ToDoAttachment();
            //{
            //    todoattachment.Name = AttachmentName;
            //    todoattachment.OriginalName = OriginalName;
            //    todoattachment.ToDoId = ToDoId;
            //    todoattachment.CreatedBy = userId;
            //    todoattachment.CreatedDate = DateTime.Now;
            //    todoattachment.SourceImage = SourceImage;
            //    todoattachment.Path = AttachmentPath;
            //};

            ///* Save ToDoAttachments */
            //dbcontext.ToDoAttachments.Add(todoattachment);
            //dbcontext.SaveChanges();


            ///* Saving Notification for Todo */
            //TodoNotifications objtodoNotifications = new TodoNotifications
            //{
            //    TodoId = ToDoId,
            //    Description = "has attached a File from "+ AttachmentType + ":",
            //    CreatedBy = userId,
            //    CreatedDate = DateTime.Now,
            //    Type = 2,
            //    AttachmentId = todoattachment.Id
            //};

            ///* Save TodoNotification for Todo */
            //dbcontext.TodoNotifications.Add(objtodoNotifications);
            //dbcontext.SaveChanges();



            //for (int j = 0; j < TodoFollowers.Count(); j++)
            //{
            //    /* Saving notification for newly added Team for team members */
            //    Notification objNotification = new Notification
            //    {

            //        Name = "has attached a File from " + AttachmentType + ":",
            //        CreatedBy = userId,
            //        CreatedDate = DateTime.Now,
            //        IsRead = false,
            //        Type = 4,
            //        NotifyingUserId = TodoFollowers[j],
            //        AttachmentId = todoattachment.Id


            //    };

            //    dbcontext.Notifications.Add(objNotification);
            //    dbcontext.SaveChanges();

            //}

            string AttachmentPath = AttachmentName;
            if (AttachmentType == "Local")
            {
                AttachmentPath = "Uploads/Attachments/ToDo/" + AttachmentName;
            }

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("usp_ToDoAttachmentsInsert", new { AttachmentName = AttachmentName, OriginalName = OriginalName, ToDoId  = ToDoId, userId = userId, SourceImage = SourceImage, AttachmentPath = AttachmentPath, AttachmentType = AttachmentType }, commandType: CommandType.StoredProcedure);
            }

            return "";
        }


        public void saveCloudToDoAttachments(TodoModel.ToDoAttachments model, string UserId)
        {
            ToDoAttachment todoattachment = new ToDoAttachment();
            {
                todoattachment.Name = model.OriginalName;
                todoattachment.OriginalName = model.OriginalName;
                todoattachment.ToDoId = model.ToDoId;
                todoattachment.CreatedBy = UserId;
                todoattachment.Path = model.Path;
                todoattachment.CreatedDate = DateTime.Now;
                todoattachment.SourceImage = model.SourceImage;
            };

            /* Save ToDoAttachments */
            dbcontext.ToDoAttachments.Add(todoattachment);
            dbcontext.SaveChanges();

        }

        public string deleteAttachment(int Id)
        {

            ToDoAttachment todoAttachmententity = (from e in dbcontext.ToDoAttachments
                                                   where e.Id == Id
                                                   select e).SingleOrDefault();

            /* Getting Attachment Name To delete it from file system*/

            //string attachmentName = todoAttachmententity.Name;


            ///* Delete Project */

            //dbcontext.ToDoAttachments.Remove(todoAttachmententity);

            //dbcontext.SaveChanges();

            string attachmentName;

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_getAttachmentName = @"select Name from ToDoAttachments where Id = @Id ";
                attachmentName = con.Query<string>(sqlquery_getAttachmentName, new { Id = Id }).SingleOrDefault();

                string sqlquery_deleteAttachment = @"delete from ToDoAttachments where Id = @Id ";
                con.Execute(sqlquery_deleteAttachment, new { Id = Id });
            }


            return attachmentName;
        }


        /// <summary>
        ///  It saves the DropBox files Link which the user has added from DropBox 
        /// </summary>
        ///  <param name="UserId">It is Unique Id of Logged In User</param>
        /// <param name="TodoId">It is Unique Id of Todo</param>
        /// <param name="model">It is list of files being selected with their names and link on dropbox</param>
        public void saveDropBoxFiles(string UserId, int TodoId, List<CloudFilesModel.DropBoxFilesModel> model)
        {
            string[] TodoFollowers = (from m in dbcontext.Projects
                                      join m1 in dbcontext.Sections on m.Id equals m1.ProjectId
                                      join m2 in dbcontext.ToDos on m1.Id equals m2.SectionId
                                      join m3 in dbcontext.Followers on m2.Id equals m3.TodoId
                                      where m2.Id == TodoId && m.IsDeleted == false
                                      && m2.IsDeleted == false
                                      select m3.UserId).ToArray();



            for (int i = 0; i < model.Count(); i++)
            {
                ToDoAttachment todoattachment = new ToDoAttachment();
                {
                    todoattachment.Name = model[i].Name;
                    todoattachment.OriginalName = model[i].Name;
                    todoattachment.ToDoId = TodoId;
                    todoattachment.CreatedBy = UserId;
                    todoattachment.Path = model[i].Link;
                    todoattachment.CreatedDate = DateTime.Now;
                    todoattachment.SourceImage = "drop-box.png";
                };

                /* Save ToDoAttachments */
                dbcontext.ToDoAttachments.Add(todoattachment);
                dbcontext.SaveChanges();


                /* Saving Notification for Todo */
                TodoNotifications objtodoNotifications = new TodoNotifications
                {
                    TodoId = TodoId,
                    Description = "has attached a File from DropBox:.",
                    CreatedBy = UserId,
                    CreatedDate = DateTime.Now,
                    Type = 2,
                    AttachmentId = todoattachment.Id
                };

                /* Save TodoNotification for Todo */
                dbcontext.TodoNotifications.Add(objtodoNotifications);
                dbcontext.SaveChanges();



                for (int j = 0; j < TodoFollowers.Count(); j++)
                {
                    /* Saving notification for newly added Team for team members */
                    Notification objNotification = new Notification
                    {

                        Name = "has attached a File from DropBox:",
                        CreatedBy = UserId,
                        CreatedDate = DateTime.Now,
                        IsRead = false,
                        Type = 4,
                        NotifyingUserId = TodoFollowers[j],
                        AttachmentId = todoattachment.Id


                    };

                    dbcontext.Notifications.Add(objNotification);
                    dbcontext.SaveChanges();

                }

            }


        }


        /// <summary>
        ///  It saves the Box files Link which the user has added from Box 
        /// </summary>
        ///  <param name="UserId">It is Unique Id of Logged In User</param>
        /// <param name="TodoId">It is Unique Id of Todo</param>
        /// <param name="model">It is list of files being selected with their names and url on box</param>
        public void saveBoxFiles(string UserId, int TodoId, List<CloudFilesModel.BoxFilesModel> model)
        {
            string[] TodoFollowers = (from m in dbcontext.Projects
                                      join m1 in dbcontext.Sections on m.Id equals m1.ProjectId
                                      join m2 in dbcontext.ToDos on m1.Id equals m2.SectionId
                                      join m3 in dbcontext.Followers on m2.Id equals m3.TodoId
                                      where m2.Id == TodoId && m.IsDeleted == false
                                      && m2.IsDeleted == false
                                      select m3.UserId).ToArray();


            for (int i = 0; i < model.Count(); i++)
            {
                ToDoAttachment todoattachment = new ToDoAttachment();
                {
                    todoattachment.Name = model[i].Name;
                    todoattachment.OriginalName = model[i].Name;
                    todoattachment.ToDoId = TodoId;
                    todoattachment.CreatedBy = UserId;
                    todoattachment.Path = model[i].Url;
                    todoattachment.CreatedDate = DateTime.Now;
                    todoattachment.SourceImage = "box-icon.png";
                };

                /* Save ToDoAttachments */
                dbcontext.ToDoAttachments.Add(todoattachment);
                dbcontext.SaveChanges();



                /* Saving Notification for Todo */
                TodoNotifications objtodoNotifications = new TodoNotifications
                {
                    TodoId = TodoId,
                    Description = "has attached a File from Box:.",
                    CreatedBy = UserId,
                    CreatedDate = DateTime.Now,
                    Type = 2,
                    AttachmentId = todoattachment.Id
                };

                /* Save TodoNotification for Todo */
                dbcontext.TodoNotifications.Add(objtodoNotifications);
                dbcontext.SaveChanges();



                for (int j = 0; j < TodoFollowers.Count(); j++)
                {
                    /* Saving notification for newly added Team for team members */
                    Notification objNotification = new Notification
                    {

                        Name = "has attached a File from Box:",
                        CreatedBy = UserId,
                        CreatedDate = DateTime.Now,
                        IsRead = false,
                        Type = 4,
                        NotifyingUserId = TodoFollowers[j],
                        AttachmentId = todoattachment.Id


                    };

                    dbcontext.Notifications.Add(objNotification);
                    dbcontext.SaveChanges();

                }


            }


        }


        /// <summary>
        ///  It saves the Google Drive files Link which the user has added from Google Drive 
        /// </summary>
        ///  <param name="UserId">It is Unique Id of Logged In User</param>
        /// <param name="TodoId">It is Unique Id of Todo</param>
        /// <param name="model">It is list of files being selected with their names and Url on Google Drive</param>
        public void saveGoogleDriveFiles(string UserId, int TodoId, List<CloudFilesModel.GoogleDriveFilesModel> model)
        {
            string[] TodoFollowers = (from m in dbcontext.Projects
                                      join m1 in dbcontext.Sections on m.Id equals m1.ProjectId
                                      join m2 in dbcontext.ToDos on m1.Id equals m2.SectionId
                                      join m3 in dbcontext.Followers on m2.Id equals m3.TodoId
                                      where m2.Id == TodoId && m.IsDeleted == false
                                      && m2.IsDeleted == false
                                      select m3.UserId).ToArray();



            for (int i = 0; i < model.Count(); i++)
            {
                ToDoAttachment todoattachment = new ToDoAttachment();
                {
                    todoattachment.Name = model[i].Name;
                    todoattachment.OriginalName = model[i].Name;
                    todoattachment.ToDoId = TodoId;
                    todoattachment.CreatedBy = UserId;
                    todoattachment.Path = model[i].Url;
                    todoattachment.CreatedDate = DateTime.Now;
                    todoattachment.SourceImage = "logo-drive.png";
                };

                /* Save ToDoAttachments */
                dbcontext.ToDoAttachments.Add(todoattachment);
                dbcontext.SaveChanges();


                /* Saving Notification for Todo */
                TodoNotifications objtodoNotifications = new TodoNotifications
                {
                    TodoId = TodoId,
                    Description = "has attached a File from Google Drive:",
                    CreatedBy = UserId,
                    CreatedDate = DateTime.Now,
                    Type = 2,
                    AttachmentId = todoattachment.Id
                };

                /* Save TodoNotification for Todo */
                dbcontext.TodoNotifications.Add(objtodoNotifications);
                dbcontext.SaveChanges();

                for (int j = 0; j < TodoFollowers.Count(); j++)
                {
                    /* Saving notification for newly added Team for team members */
                    Notification objNotification = new Notification
                    {

                        Name = "has attached a File from Google Drive:",
                        CreatedBy = UserId,
                        CreatedDate = DateTime.Now,
                        IsRead = false,
                        Type = 4,
                        NotifyingUserId = TodoFollowers[j],
                        AttachmentId = todoattachment.Id


                    };

                    dbcontext.Notifications.Add(objNotification);
                    dbcontext.SaveChanges();

                }

            }


        }

    }
}
