﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;

namespace TIGHTEN.DATA
{
    public class FileTag : Common
    {
        public object GetFilesTagged(int CommentId)
        {

            return (from m in dbcontext.FileTags where m.CommentId == CommentId select m).ToList();
        }

        public void SaveFilesTagged(int commentId, string FileName, string OriginalFileName, string FilePath, bool IsGif)
        {

            TIGHTEN.ENTITY.FileTag filetag = new TIGHTEN.ENTITY.FileTag
            {
                FileName = FileName,
                CommentId = commentId,
                OriginalName = OriginalFileName,
                FilePath = FilePath,
                IsGif = IsGif
            };
            dbcontext.FileTags.Add(filetag);
            dbcontext.SaveChanges();
        }

    }
}
