﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
//using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using System.Threading;
using System.Data.Entity;
using System.Web;
using System.Xml.Linq;
using Dapper;
using System.Data.SqlClient;
using TIGHTEN.MODEL;

namespace TIGHTEN.DATA
{
    public class ReportersForTodoes : BaseClass
    {
        AppContext dbcontext;

        public List<MODEL.ReportersForTodoesModel.ReportersDetail> getToDoReporters(string UserId, int CompanyId, int ProjectId, int RoleId)
        {
            //UserDetail userEntity = (from m in dbcontext.UserDetails where m.UserId == UserId && m.IsDeleted == false select m).SingleOrDefault();

            UserDetail userEntity;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from UserDetails m where m.UserId = @UserId and m.IsDeleted = 0 ";
                userEntity = con.Query<UserDetail>(sqlquery,new { UserId = UserId }).SingleOrDefault();
            }



            /* Now we are using this Method to bind Reporters to generate Invoice*/
            //List<MODEL.ReportersForTodoesModel.ReportersDetail> ToDoReportersObject = (from grp in
            //                                                                           (from prjct in dbcontext.Projects
            //                                                                            join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId
            //                                                                            //join tm in dbcontext.Teams on prjct.TeamId equals tm.Id
            //                                                                            //join tmusr in dbcontext.TeamUsers on tm.Id equals   tmusr.TeamId
            //                                                                            join usrdet in dbcontext.UserDetails on prjctusr.UserId equals usrdet.UserId
            //                                                                            join usrcomrol in dbcontext.UserCompanyRoles on usrdet.UserId equals usrcomrol.UserId
            //                                                                            join rol in dbcontext.Roles on usrcomrol.RoleId equals rol.RoleId
            //                                                                            where usrcomrol.CompanyId == CompanyId && prjct.Id == ProjectId && usrdet.IsDeleted == false
            //                                                                            && rol.Name != "General User" && rol.Name != "Freelancer" && prjct.IsDeleted == false
            //                                                                            && rol.IsDeleted == false
            //                                                                            select new { usrdet }
            //                                                                            )
            //                                                                           group grp by new { grp.usrdet.UserId, grp.usrdet.Email, grp.usrdet.FirstName, grp.usrdet.LastName, grp.usrdet.ProfilePhoto } into grpData
            //                                                                           select new MODEL.ReportersForTodoesModel.ReportersDetail
            //                                                                           {
            //                                                                               UserId = grpData.Key.UserId,
            //                                                                               Email = grpData.Key.Email,
            //                                                                               FirstName = grpData.Key.FirstName,
            //                                                                               LastName = grpData.Key.LastName,
            //                                                                               ProfilePhoto = grpData.Key.ProfilePhoto
            //                                                                           }
            //                                                                          ).ToList();


            List<MODEL.ReportersForTodoesModel.ReportersDetail> ToDoReportersObject;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select usrdet.UserId, usrdet.Email, usrdet.FirstName, usrdet.LastName, usrdet.ProfilePhoto 
                               from Projects prjct inner join ProjectUsers prjctusr on prjct.Id = prjctusr.ProjectId inner join UserDetails usrdet on prjctusr.UserId = usrdet.UserId 
                               inner join UserCompanyRoles usrcomrol on usrdet.UserId = usrcomrol.UserId inner join Roles rol on usrcomrol.RoleId = rol.RoleId where usrcomrol.CompanyId = @CompanyId and prjct.Id = @ProjectId and usrdet.IsDeleted = 0
                               and rol.Name <> 'General User' and rol.Name <> 'Freelancer' and prjct.IsDeleted = 0 and rol.IsDeleted = 0 
                               group by usrdet.UserId,usrdet.Email,usrdet.FirstName,usrdet.LastName, usrdet.ProfilePhoto       
                                                                                            ";
                ToDoReportersObject = con.Query<MODEL.ReportersForTodoesModel.ReportersDetail>(sqlquery, new { CompanyId= CompanyId, ProjectId = ProjectId }).ToList();

            }                                                    


            return ToDoReportersObject;
        }


        public string SaveInvoiceDetails(MODEL.ReportersForTodoesModel.ReportersEmailData model)
        {
            string Id = string.Empty;

            //UserDetail user = (from usr in dbcontext.UserDetails
            //                   where usr.UserId == model.UserId
            //                   && usr.IsDeleted == false
            //                   select usr).FirstOrDefault();

            UserDetail user;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from UserDetails usr where usr.UserId = @UserId and usr.IsDeleted = 0 ";
                user = con.Query<UserDetail>(sqlquery, new { UserId = model.UserId }).FirstOrDefault();
            }

            if (user.IsStripeAccountVerified != true)
            {
                Id = "Account Not Verified";
                return Id;
            }
            else
            {
                if (model.AllTodosForPreview.AllTodos.Count > 0)
                {
                    List<TIGHTEN.ENTITY.Invoice> lastInvoice;
                    string NewInvoiceNumber = "inv#1";
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlquery = @"select * from Invoices";
                        lastInvoice = con.Query<TIGHTEN.ENTITY.Invoice>(sqlquery).ToList();

                        if (lastInvoice.Count() > 0)
                        {
                            var lastInvoiceInsertedId = (lastInvoice).Max(x => x.Id);
                            if (lastInvoiceInsertedId > 0)
                            {
                                TIGHTEN.ENTITY.Invoice invoice;
                                string sqlquery_lastInvoice = @"select * from Invoices v where v.Id = @lastInvoiceInsertedId ";
                                invoice = con.Query<TIGHTEN.ENTITY.Invoice>(sqlquery_lastInvoice, new { lastInvoiceInsertedId = lastInvoiceInsertedId }).FirstOrDefault();

                                string OldInvoiceNumber = invoice.InvoiceNumber;
                                string[] OldNumberForInvoice = OldInvoiceNumber.Split('#');
                                if (OldNumberForInvoice.Length > 1)
                                {
                                    int NewNumberForInvoice = Convert.ToInt32(OldNumberForInvoice[1]);
                                    NewNumberForInvoice += 1;
                                    NewInvoiceNumber = "inv#" + NewNumberForInvoice;
                                }
                            }
                        }
                    }


                    List<MODEL.ReportersForTodoesModel.SaveInvoiceDetailsModal> InvoiceDetail = new List<MODEL.ReportersForTodoesModel.SaveInvoiceDetailsModal>();

                    foreach (var item in model.AllTodosForPreview.AllTodos)
                    {
                        MODEL.ReportersForTodoesModel.SaveInvoiceDetailsModal obj = new ReportersForTodoesModel.SaveInvoiceDetailsModal
                        {
                            TodoId = item.TodoId,
                            TodoDescription = item.TodoDescription,
                            TodoName = item.TodoName,
                            hours = item.hours,
                            Amount = item.Amount,
                            FromDate = item.FromDate,
                            ToDate = item.ToDate,
                            TodoType = item.TodoType
                        };

                        InvoiceDetail.Add(obj);
                    }

                    
                    string Invoice_Id = string.Empty;
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        Invoice_Id = con.Query<string>("usp_SaveInvoice", new { NewInvoiceNumber = NewInvoiceNumber, UserId= model.UserId, CompanyId= model.CompanyId, model_FromDate= model.FromDate, model_ToDate= model.ToDate, Reporters= model.Reporters, Message= model.Message, TotalHoursForAllTodos= model.AllTodosForPreview.TotalHoursForAllTodos, TotalAmountForAllTodos= model.AllTodosForPreview.TotalAmountForAllTodos,
                                        AllInvoiceDetails = InvoiceDetail.AsTableValuedParameter("InvoiceDetails", new[] { "TodoId", "TodoDescription", "TodoName", "hours", "Amount", "FromDate", "ToDate", "TodoType" })
                                    }, commandType: System.Data.CommandType.StoredProcedure).Single();  
                    }

                    int InvoiceId = Convert.ToInt32(Invoice_Id);

                    if (InvoiceId > 0)
                    {
                        HttpContext ctx = HttpContext.Current;
                        Thread childref = new Thread(new ThreadStart(() =>
                        {
                            HttpContext.Current = ctx;
                            ThreadForSaveInvoice(model, NewInvoiceNumber, user.FirstName);
                        }
                        ));
                        childref.Start();
                    }



                    //ENTITY.Invoice inv = new ENTITY.Invoice
                    //{
                    //    InvoiceNumber = NewInvoiceNumber,
                    //    UserId = model.UserId,
                    //    CompanyId = model.CompanyId,
                    //    FromDate = model.FromDate,
                    //    ToDate = model.ToDate,
                    //    SendTo = model.Reporters,
                    //    Message = model.Message,
                    //    InvoiceDate = DateTime.Now,
                    //    TotalHours = model.AllTodosForPreview.TotalHoursForAllTodos,
                    //    TotalAmount = model.AllTodosForPreview.TotalAmountForAllTodos,
                    //    InvoiceApprovedStatus = false
                    //};

                    //dbcontext.Invoices.Add(inv);
                    //dbcontext.SaveChanges();

                    //Id = inv.Id.ToString();



                    //foreach (var item in model.AllTodosForPreview.AllTodos)
                    //{
                    //    ToDo TodoEntity = (from t in dbcontext.ToDos
                    //                       where t.Id == item.TodoId
                    //                       && t.IsDeleted == false
                    //                       select t).FirstOrDefault();

                    //    TodoEntity.IsAvailableForInvoice = false;

                    //    string TodoTimeLogId = string.Empty;

                    //    List<TodoTimeLogs> TodoTimeLog = (from tt in dbcontext.TodoTimeLogs
                    //                                      where tt.TodoId == item.TodoId
                    //                                      && tt.IsInvoiceGenerated == false
                    //                                      select tt).ToList();
                    //    foreach (var TodoTimeLog_item in TodoTimeLog)
                    //    {
                    //        TodoTimeLog_item.IsInvoiceGenerated = true;
                    //        if (TodoTimeLogId == string.Empty)
                    //        {
                    //            TodoTimeLogId = TodoTimeLog_item.Id.ToString();
                    //        }
                    //        else
                    //        {
                    //            TodoTimeLogId = TodoTimeLogId + "," +TodoTimeLog_item.Id.ToString();
                    //        }
                    //    }

                    //    InvoiceDetail invcdetail = new InvoiceDetail
                    //    {
                    //        InvoiceId = inv.Id,
                    //        TodoId = item.TodoId,
                    //        TodoDescription = item.TodoDescription,
                    //        TodoName = item.TodoName,
                    //        hours = item.hours,

                    //        Amount = item.Amount,
                    //        FromDate = item.FromDate,
                    //        ToDate = item.ToDate,
                    //        TodoType = item.TodoType,
                    //        TodoTimeLogIds = TodoTimeLogId
                    //    };

                    //    dbcontext.InvoiceDetails.Add(invcdetail);
                    //    dbcontext.SaveChanges();
                    //}



                }

                return Id;
            }

        }

        //private void ThreadForSaveInvoice(MODEL.ReportersForTodoesModel.ReportersEmailData model)
        //{
        //    List<string> AllAdmins = (from userdet in dbcontext.UserDetails
        //                              join com in dbcontext.Companies on userdet.CompanyId equals com.Id
        //                              join usrcom in dbcontext.UserCompanyRoles on userdet.UserId equals usrcom.UserId
        //                              join Rol in dbcontext.Roles on usrcom.RoleId equals Rol.RoleId
        //                              where Rol.RoleId == 1 && com.Id == model.CompanyId
        //                              select userdet.UserId).ToList();

        //    if (!AllAdmins.Contains(model.Reporters))
        //    {
        //        AllAdmins.Add(model.Reporters);
        //    }

        //    foreach (var item in AllAdmins)
        //    {
        //        bool IsSettingActivated = false;
        //        /*  This function is used for checking wheather mail setting is activated for assignee or not*/
        //        if (!string.IsNullOrEmpty(item))
        //        {
        //            IsSettingActivated = (from usrSetting in dbcontext.UserSettingConfigs
        //                                  join dfltSet in dbcontext.SettingConfigs on usrSetting.NotificationId equals dfltSet.Id
        //                                  where dfltSet.NotificationName == "Invoice Raise"
        //                                  && usrSetting.UserId == item && usrSetting.CompanyId == model.CompanyId
        //                                  && usrSetting.ProjectId == model.ProjectId
        //                                  select usrSetting.IsActive).FirstOrDefault();

        //        }


        //        var details = (from usrdtls in dbcontext.UserDetails where usrdtls.UserId == item select usrdtls).SingleOrDefault();

        //        string UserName = (from usrdt in dbcontext.UserDetails
        //                           where usrdt.UserId == model.UserId
        //                           select usrdt.FirstName).SingleOrDefault();

        //        String emailbody = "<p>Hello " + details.FirstName + " </p>";
        //        emailbody += "<p>A new invoice is raised by " + UserName + " </p>";

        //        //EmailUtility.Send(details.Email, "New Todos is Assigned", "");
        //        if (IsSettingActivated)
        //        {
        //            EmailUtility.SendMailInThread(details.Email, "New Invoice", emailbody);
        //        }
        //    }
        //}


        public void ThreadForSaveInvoice(MODEL.ReportersForTodoesModel.ReportersEmailData model, string NewInvoiceNumber,string FreelancerName)
        {
            //string ProjectName = (from p in dbcontext.Projects
            //                      where p.Id == model.ProjectId
            //                      select p.Name).FirstOrDefault();

            //List<string> AllAdmins = (from userdet in dbcontext.UserDetails
            //                          join com in dbcontext.Companies on userdet.CompanyId equals com.Id
            //                          join usrcom in dbcontext.UserCompanyRoles on userdet.UserId equals usrcom.UserId
            //                          join Rol in dbcontext.Roles on usrcom.RoleId equals Rol.RoleId
            //                          where Rol.RoleId == 1 && com.Id == model.CompanyId
            //                          && userdet.IsDeleted == false && Rol.IsDeleted == false
            //                          select userdet.UserId).ToList();


            string ProjectName;
            List<string> AllAdmins;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select p.Name from Projects p where p.Id = @ProjectId ";
                ProjectName = con.Query<string>(sqlquery, new { ProjectId = model.ProjectId }).FirstOrDefault();

                string sqlquery_ALLAdmins = @"select userdet.UserId from UserDetails userdet inner join Companies com on userdet.CompanyId = com.Id inner join UserCompanyRoles usrcom  on userdet.UserId = usrcom.UserId 
                                          inner join Roles Rol on usrcom.RoleId = Rol.RoleId where Rol.RoleId = 1 and com.Id = @CompanyId and userdet.IsDeleted = 0 and Rol.IsDeleted = 0 ";

                AllAdmins = con.Query<string>(sqlquery_ALLAdmins, new { CompanyId= model.CompanyId }).ToList();

            }

           

           
            if (!AllAdmins.Contains(model.Reporters))
            {
                AllAdmins.Add(model.Reporters);
            }

            if (AllAdmins.Count() > 0)
            {
                string AllAdminsUserId = string.Join(",", AllAdmins.Select(x => "'" + x + "'"));

                //List<MODEL.ReportersForTodoesModel.ReportersDetail> SelectedReporters = (from reporters in dbcontext.UserDetails
                //                                                                         where AllAdmins.Contains(reporters.UserId) && reporters.IsDeleted == false
                //                                                                         select new MODEL.ReportersForTodoesModel.ReportersDetail
                //                                                                         { UserId = reporters.UserId,
                //                                                                           Email = reporters.Email,
                //                                                                           FirstName = reporters.FirstName }
                //                                                                         ).ToList();

                List<MODEL.ReportersForTodoesModel.ReportersDetail> SelectedReporters;
                using (var con = new SqlConnection(ConnectionString))
                {   
                    string sqlquery_reporters = @"select reporters.UserId,reporters.Email,reporters.FirstName  from UserDetails reporters where reporters.UserId in (" + AllAdminsUserId + ") and reporters.IsDeleted = 0 ";
                    SelectedReporters = con.Query<MODEL.ReportersForTodoesModel.ReportersDetail>(sqlquery_reporters).ToList();
                }
                


                foreach (var reporter in SelectedReporters)
                {

                    bool IsSettingActivated = false;
                    /*  This function is used for checking wheather mail setting is activated for assignee or not*/
                    if (!string.IsNullOrEmpty(reporter.UserId))
                    {
                        //IsSettingActivated = (from usrSetting in dbcontext.UserSettingConfigs
                        //                      join dfltSet in dbcontext.SettingConfigs on usrSetting.NotificationId equals dfltSet.Id
                        //                      where dfltSet.NotificationName == "Invoice Raise"
                        //                      && usrSetting.UserId == reporter.UserId && usrSetting.CompanyId == model.CompanyId
                        //                      //&& usrSetting.ProjectId == model.ProjectId
                        //                      select usrSetting.IsActive).FirstOrDefault();

                        using (var con = new SqlConnection(ConnectionString))
                        {
                            string sqlquery_sett = @"select  usrSetting.IsActive from UserSettingConfigs usrSetting inner join SettingConfigs dfltSet on usrSetting.NotificationId = dfltSet.Id 
                                                     where dfltSet.NotificationName = 'Invoice Raise' and usrSetting.UserId = @UserId and usrSetting.CompanyId = @CompanyId  ";

                            IsSettingActivated = con.Query<bool>(sqlquery_sett, new { UserId= reporter.UserId, CompanyId= model.CompanyId }).Single();
                        }

                    }

                    if (IsSettingActivated || reporter.UserId == model.Reporters)
                    {
                        
                        String emailbody = null;
                        string Subject = string.Empty;

                        XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                        var MailModule = from r in xdoc.Descendants("MailModule").
                                         Where(r => (string)r.Attribute("id") == "SaveInvoice")
                                             //select r;
                                         select new
                                         {
                                             mailbody = r.Element("Body").Value,
                                             subject = r.Element("Subject").Value
                                         };

                        foreach (var item in MailModule)
                        {
                            emailbody = item.mailbody;
                            Subject = item.subject;
                        }

                        if (model.Message == "")
                        {
                            model.Message = "No Message found";
                        }

                        emailbody = emailbody.Replace("@@Reporter_FirstName", reporter.FirstName).Replace("@@FromName", model.AllTodosForPreview.FromName).Replace("@@FromDate", model.FromDate.ToString());
                        emailbody = emailbody.Replace("@@ToDate", model.ToDate.ToString()).Replace("@@TodoCount", model.AllTodosForPreview.AllTodos.Count.ToString());
                        emailbody = emailbody.Replace("@@TotalHoursForAllTodos", model.AllTodosForPreview.TotalHoursForAllTodos).Replace("@@Rate", model.AllTodosForPreview.AllTodos[0].Rate.ToString()).Replace("@@TotalAmountForAllTodos", model.AllTodosForPreview.TotalAmountForAllTodos.ToString());
                        emailbody = emailbody.Replace("@@Message", model.Message);
                        Subject = Subject.Replace("@@NewInvoiceNumber", NewInvoiceNumber).Replace("@@Freelancername", FreelancerName).Replace("@@fromdate", model.FromDate.ToString("MM/dd/yyyy")).Replace("@@ToDate", model.ToDate.ToString("MM/dd/yyyy")).Replace("@@ProjectName", ProjectName);

                        EmailUtility.SendMailInThread(reporter.Email, Subject, emailbody);
                    }

                }
            }

        }


        public MODEL.ReportersForTodoesModel.TodosForPdf_List getAllTodosForPreview(MODEL.ReportersForTodoesModel.TodosForPreview_RequiredParams model)
        {
            MODEL.ReportersForTodoesModel.TodosForPdf_List ReturnModel = new MODEL.ReportersForTodoesModel.TodosForPdf_List();

            //var FromName = (from ussr in dbcontext.UserDetails
            //                where ussr.UserId == model.userId && ussr.IsDeleted == false
            //                select ussr.FirstName).SingleOrDefault();

            //var ToName = (from ussr in dbcontext.UserDetails
            //              where ussr.UserId == model.ReportersID && ussr.IsDeleted == false
            //              select ussr.FirstName).SingleOrDefault();

            //var CompanyProfilePic = (from com in dbcontext.Companies
            //                         where com.Id == model.CompanyId
            //                         select new
            //                         {
            //                             //CompanyUserId = usr.UserId,
            //                             CompanyUserId = com.Id,
            //                             //conpanyprofilepic = "Uploads/CompanyLogos/" + (string.IsNullOrEmpty(com.Logo) ? "" : com.Logo),
            //                             conpanyprofilepic = com.Logo == null ? "Uploads/Default/nologo.png" : "Uploads/CompanyLogos/" + com.Logo,
            //                             CompanyName = com.Name
            //                         }).SingleOrDefault();

            InvoiceDetailReturnModel obj;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select usr.Id as CompanyUserId,case when usr.Logo is null then 'Uploads/Default/nologo.png' else 'Uploads/CompanyLogos/'+ usr.Logo end as conpanyprofilepic,usr.Name as CompanyName ,
	                                    (select FirstName from UserDetails where UserId = @UserId and IsDeleted = 0)  as FromName,(select FirstName from UserDetails where UserId = @SendToUserId and IsDeleted = 0)  as ToName
	                                    from Companies usr where usr.Id = @CompanyId ";

                obj = con.Query<InvoiceDetailReturnModel>(sqlquery, new { UserId = model.userId, CompanyId = model.CompanyId, SendToUserId = model.ReportersID }).SingleOrDefault();
            }


            List<TIGHTEN.ENTITY.Invoice> lastInvoice;
            string NewInvoiceNumber = "inv#1";
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from Invoices";
                lastInvoice = con.Query<TIGHTEN.ENTITY.Invoice>(sqlquery).ToList();

                if (lastInvoice.Count() > 0)
                {
                    var lastInvoiceInsertedId = (lastInvoice).Max(x => x.Id);
                    if (lastInvoiceInsertedId > 0)
                    {
                        TIGHTEN.ENTITY.Invoice invoice;
                        string sqlquery_lastInvoice = @"select * from Invoices v where v.Id = @lastInvoiceInsertedId ";
                        invoice = con.Query<TIGHTEN.ENTITY.Invoice>(sqlquery_lastInvoice,new { lastInvoiceInsertedId = lastInvoiceInsertedId }).FirstOrDefault();

                        string OldInvoiceNumber = invoice.InvoiceNumber;
                        string[] OldNumberForInvoice = OldInvoiceNumber.Split('#');
                        if (OldNumberForInvoice.Length > 1)
                        {
                            int NewNumberForInvoice = Convert.ToInt32(OldNumberForInvoice[1]);
                            NewNumberForInvoice += 1;
                            NewInvoiceNumber = "inv#" + NewNumberForInvoice;
                        }
                    }
                }
            }
           
            
            

            ReturnModel.InvoiceNumber = NewInvoiceNumber;
            ReturnModel.FromName = obj.FromName;
            ReturnModel.ToName = obj.ToName;
            ReturnModel.CompanyProfilePic = obj.conpanyprofilepic;
            ReturnModel.CompanyUserId = obj.CompanyUserId.ToString();
            ReturnModel.CompanyName = obj.CompanyName;
            ReturnModel.DateIssued = DateTime.Now;
            ReturnModel.Message = model.message;

            //var inv_details = (from invDetails in dbcontext.InvoiceDetails
            //                   select invDetails.TodoId).ToList();



            //var Customlist = (from t in dbcontext.ToDos
            //                      //join ttmlog in dbcontext.TodoTimeLogs on t.Id equals ttmlog.TodoId
            //                  join u in dbcontext.UserDetails on t.AssigneeId equals u.UserId
            //                  join s in dbcontext.Sections on t.SectionId equals s.Id
            //                  join p in dbcontext.Projects on s.ProjectId equals p.Id
            //                  where t.AssigneeId == model.userId && u.IsDeleted == false && p.IsDeleted == false
            //                  && p.CompanyId == model.CompanyId && t.IsDeleted == false
            //                  && (model.ProjectId == 0 ? p.Id == p.Id : p.Id == model.ProjectId)
            //                  && DbFunctions.TruncateTime(t.ModifiedDate) >= model.fromDate.Date
            //                  && DbFunctions.TruncateTime(t.ModifiedDate) <= model.toDate.Date
            //                  && t.IsApproved != true
            //                  && t.IsDone == true
            //                  && t.IsAvailableForInvoice == true
            //                  //&& ttmlog.IsInvoiceGenerated == false
            //                  //&& !inv_details.Contains(t.Id)
            //                  select new
            //                  {
            //                      ProjectName = p.Name,
            //                      TodoId = t.Id,
            //                      TodoName = t.Name,
            //                      TodoDescription = t.Description,
            //                      Rate = u.Rate,
            //                      //hours = t.LoggedTime,
            //                      HoursDetail = (from ttl in dbcontext.TodoTimeLogs
            //                                     where ttl.TodoId == t.Id
            //                                     && ttl.IsInvoiceGenerated == false
            //                                     select new MODEL.ReportersForTodoesModel.TodoTimeLogsForPreview_List
            //                                     {
            //                                         TodoId = t.Id,
            //                                         TodoTimeLogsId = ttl.Id,
            //                                         DurationInMinutes = ttl.Duration
            //                                     }
            //                                       ).ToList(),
                                                  
            //                      FromDate = t.ModifiedDate,
            //                      ToDate = t.ModifiedDate,
            //                      TodoType = t.TaskType,
            //                      TaskType = (t.TaskType == 1 ? "Phone Call" : t.TaskType == 2 ? " Research & Development " : t.TaskType == 3 ? "Meeting" : t.TaskType == 4 ? " Admin Duties " : "PMS Task")

            //                  }).ToList();




            //List<MODEL.ReportersForTodoesModel.TodosForPreview_List> list = Customlist.AsEnumerable().Select(x =>
            //new MODEL.ReportersForTodoesModel.TodosForPreview_List
            //{
            //    ProjectName = x.ProjectName,
            //    TodoId = x.TodoId,
            //    TodoName = x.TodoName,
            //    TodoDescription = x.TodoDescription,
            //    Rate = x.Rate,
            //    HoursDetail = x.HoursDetail,
            //    FromDate = x.FromDate,
            //    ToDate = x.ToDate,
            //    TodoType = x.TodoType,
            //    TaskType = x.TaskType

            //}).ToList();


            List<MODEL.ReportersForTodoesModel.TodosForPreview_List> list;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetAllTodosForPreview", new { userId= model.userId, CompanyId= model.CompanyId, ProjectId= model.ProjectId, fromDate= model.fromDate, toDate= model.toDate },commandType:System.Data.CommandType.StoredProcedure);
                list = result.Read<MODEL.ReportersForTodoesModel.TodosForPreview_List>().ToList();
                if (list != null)
                {
                    var HoursDetail = result.Read<MODEL.ReportersForTodoesModel.TodoTimeLogsForPreview_List>().ToList();
                    foreach (var item in list)
                    {
                        item.HoursDetail = HoursDetail.Where(x => x.TodoId == item.TodoId).ToList();
                    }
                }
            }

            // This need to be changed after some time ,  this is a temporary fix
            // Here we have to find some other method to find Total Hours 

            #region Changable Data

            int TotalMinutesForTodos = 0;
            decimal rate = 0;

            foreach (var item in list)
            {
                int LoggedTime = 0;

                foreach (var item2 in item.HoursDetail)
                {
                    LoggedTime += item2.DurationInMinutes;
                }
                decimal amt = 0;
                item.hours = Utility.ConvertMinutesToFormatedTimeLogString(LoggedTime);
                //int LoggedTimeInMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(item.hours);
                int LoggedTimeInMinutes = LoggedTime;
                int hours = (LoggedTimeInMinutes / 60);
                amt = (hours * item.Rate);
                int minutes = (LoggedTimeInMinutes % 60);
                if (minutes > 0)
                {
                    decimal AmtForMinutes = (Convert.ToDecimal(minutes / 60.00) * item.Rate);
                    amt = amt + AmtForMinutes;

                }

                item.hours = item.hours;
                item.Amount = Math.Round(amt, 2);

                TotalMinutesForTodos += LoggedTimeInMinutes;
                rate = item.Rate;

            }


            decimal totalamt = 0;
            int TotalHours = (TotalMinutesForTodos / 60);
            totalamt = (TotalHours * rate);

            int TotalMinutes = (TotalMinutesForTodos % 60);
            if (TotalMinutes > 0)
            {
                decimal totalAmtForMinutes = (Convert.ToDecimal(TotalMinutes / 60.00) * rate);
                totalamt = totalamt + totalAmtForMinutes;
            }

            ReturnModel.TotalHoursForAllTodos = TotalHours + "h " + TotalMinutes + "m";

            ReturnModel.TotalAmountForAllTodos = Math.Round(totalamt, 2);


            #endregion

            ReturnModel.AllTodos = list;


            return ReturnModel;
        }




    }
}
