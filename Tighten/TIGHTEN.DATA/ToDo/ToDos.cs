﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.UTILITIES;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using System.Data;
using System.Data.Entity;
using System.Xml.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class ToDos : BaseClass
    {
        AppContext dbcontext;

        /// <summary>
        /// Getting All Todos for a particular Project
        /// </summary>
        /// <param name="ProjectId">It is unique Id of Project</param>
        /// <returns>Returns List Of Todos with Sections</returns>
        public List<TodoModel.SectionData> GetAllTodosWithSections(int ProjectId)
        {

            //var SectionsWithTodosResult = (from t0 in dbcontext.Teams
            //                               join t in dbcontext.Projects on t0.Id equals t.TeamId
            //                               join t1 in dbcontext.Sections on t.Id equals t1.ProjectId
            //                               join t2 in dbcontext.UserDetails on t1.CreatedBy equals t2.UserId
            //                               where t.Id == ProjectId && t2.IsDeleted == false
            //                               && t0.IsDeleted == false && t.IsDeleted == false
            //                               orderby t1.ListOrder ascending
            //                               select new
            //                               {
            //                                   Id = t1.Id,
            //                                   SectionName = t1.SectionName,
            //                                   SectionListOrder = t1.ListOrder,
            //                                   Text = t1.SectionName,
            //                                   Value = t1.Id,
            //                                   ProjectId = t1.ProjectId,
            //                                   SectionCreator = t2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + t2.ProfilePhoto,

            //                                   Todos = from c in dbcontext.ToDos
            //                                               //join ttype in dbcontext.TaskTypes on c.TodoTaskTypeId equals ttype.Id
            //                                           join c3 in dbcontext.UserDetails on c.ReporterId equals c3.UserId
            //                                           join c1 in dbcontext.UserDetails on c.AssigneeId equals c1.UserId
            //                                           into b
            //                                           from c2 in b.DefaultIfEmpty()
            //                                           let AssigneeProfilePhoto = c2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c2.ProfilePhoto

            //                                           let ReporterProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
            //                                           let AssigneeName = c2.FirstName + " " + (string.IsNullOrEmpty(c2.LastName) ? "" : c2.LastName)
            //                                           let ReporterName = c3.FirstName + " " + (string.IsNullOrEmpty(c3.LastName) ? "" : c3.LastName)
            //                                           // taskType=5 is for the PMS task
            //                                           where t1.Id == c.SectionId && c.TaskType == 5
            //                                           && c3.IsDeleted == false && c.IsDeleted == false   /*&& c1.IsDeleted == false*/
            //                                           orderby c.ListOrder ascending
            //                                           select new TodoModel.TodosData
            //                                           {
            //                                               Name = 
            //                                               SectionId = 
            //                                               TodoId = 
            //                                               TodoListOrder = 
            //                                               AssigneeId = 
            //                                               AssigneeProfilePhoto =
            //                                               AssigneeName = 
            //                                               AssigneeEmail = 
            //                                               ReporterId = 
            //                                               ReporterEmail = 
            //                                               ReporterName = 
            //                                               ReporterProfilePhoto = 
            //                                               Description = 
            //                                               DeadlineDate = 
            //                                               InProgress = 
            //                                               IsDone = 
            //                                               SectionName = t1.SectionName,
            //                                               TeamName = t0.TeamName,
            //                                               ProjectId = t.Id,
            //                                               ProjectName = t.Name,
            //                                               CreatedDate = 
            //                                               ModifiedDate = 
            //                                               EstimatedTime = 
            //                                               LoggedTime = 
            //                                               //TaskType = ttype.Text,
            //                                               TaskTypeId = 
            //                                           }
            //                               });



            //List<TodoModel.SectionData> SectionsWithTodos = SectionsWithTodosResult.AsEnumerable().Select(item =>
            //                                              new TodoModel.SectionData()
            //                                              {
            //                                                  Id = item.Id,
            //                                                  SectionName = item.SectionName,
            //                                                  SectionListOrder = item.SectionListOrder,
            //                                                  Text = item.SectionName,
            //                                                  Value = item.Id,
            //                                                  ProjectId = item.ProjectId,
            //                                                  SectionCreator = item.SectionCreator,
            //                                                  Todos = item.Todos.ToList()
            //                                              }).ToList();

            List<TodoModel.SectionData> SectionsWithTodos;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetAllTodosWithSections",new { ProjectId = ProjectId },commandType : CommandType.StoredProcedure);
                SectionsWithTodos = result.Read<TodoModel.SectionData>().ToList();
                if (SectionsWithTodos != null)
                {
                    var todos = result.Read<TodoModel.TodosData>().ToList();

                    foreach (var item in SectionsWithTodos)
                    {
                        item.Todos = todos.Where(x => x.SectionId == item.Id).ToList();
                    }
                }
            }


            return SectionsWithTodos;
        }


        /// <summary>
        /// Getting All Todos for All Project
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>Returns List Of Todos with Sections  for All Project</returns>
        public List<TodoModel.SectionData> TodosWithSectionsForAllProjects(int CompanyId)
        {
            //var SectionsWithTodosResult = (from t0 in dbcontext.Teams
            //                               join t in dbcontext.Projects on t0.Id equals t.TeamId
            //                               join t1 in dbcontext.Sections on t.Id equals t1.ProjectId
            //                               join t2 in dbcontext.UserDetails on t1.CreatedBy equals t2.UserId
            //                               where t.CompanyId == CompanyId && t2.IsDeleted == false
            //                               && t0.IsDeleted == false && t.IsDeleted == false
            //                               orderby t1.ListOrder ascending

            //                               select new
            //                               {
            //                                   Id = t1.Id,
            //                                   SectionName = t1.SectionName,
            //                                   Text = t1.SectionName,
            //                                   Value = t1.Id,
            //                                   ProjectId = t1.ProjectId,
            //                                   SectionCreator = t2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + t2.ProfilePhoto,

            //                                   Todos = from c in dbcontext.ToDos
            //                                               //join ttype in dbcontext.TaskTypes on c.TodoTaskTypeId equals ttype.Id
            //                                           join c3 in dbcontext.UserDetails on c.CreatedBy equals c3.UserId
            //                                           join c1 in dbcontext.UserDetails on c.AssigneeId equals c1.UserId
            //                                           into b
            //                                           from c2 in b.DefaultIfEmpty()
            //                                           let AssigneeProfilePhoto = c2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c2.ProfilePhoto

            //                                           let ReporterProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
            //                                           let AssigneeName = c2.FirstName + " " + (string.IsNullOrEmpty(c2.LastName) ? "" : c2.LastName)
            //                                           let ReporterName = c3.FirstName + " " + (string.IsNullOrEmpty(c3.LastName) ? "" : c3.LastName)
            //                                           // taskType=5 is for he PMS task
            //                                           where t1.Id == c.SectionId && c.TaskType == 5 
            //                                           && c3.IsDeleted == false && c.IsDeleted == false
            //                                           orderby c.ListOrder ascending
            //                                           select new TodoModel.TodosData
            //                                           {
            //                                               Name = c.Name,
            //                                               SectionId = c.SectionId,
            //                                               TodoId = c.Id,
            //                                               AssigneeId = c.AssigneeId,
            //                                               AssigneeProfilePhoto = AssigneeProfilePhoto,
            //                                               AssigneeName = AssigneeName,
            //                                               AssigneeEmail = c2.Email,
            //                                               ReporterId = c3.UserId,
            //                                               ReporterEmail = c3.Email,
            //                                               ReporterName = ReporterName,
            //                                               ReporterProfilePhoto = ReporterProfilePhoto,
            //                                               Description = c.Description,
            //                                               DeadlineDate = c.DeadlineDate,
            //                                               InProgress = c.InProgress,
            //                                               IsDone = c.IsDone,
            //                                               SectionName = t1.SectionName,
            //                                               TeamName = t0.TeamName,
            //                                               ProjectId = t.Id,
            //                                               ProjectName = t.Name,
            //                                               CreatedDate = c.CreatedDate,
            //                                               ModifiedDate = c.ModifiedDate,
            //                                               EstimatedTime = c.EstimatedTime,
            //                                               LoggedTime = c.LoggedTime,
            //                                               //TaskType = ttype.Text,
            //                                               TaskTypeId = c.TodoTaskTypeId
            //                                           }
            //                               });

            //List<TodoModel.SectionData> SectionsWithTodos = SectionsWithTodosResult.AsEnumerable().Select(item =>
            //                                              new TodoModel.SectionData()
            //                                              {
            //                                                  Id = item.Id,
            //                                                  SectionName = item.SectionName,
            //                                                  Text = item.SectionName,
            //                                                  Value = item.Id,
            //                                                  ProjectId = item.ProjectId,
            //                                                  SectionCreator = item.SectionCreator,
            //                                                  Todos = item.Todos.ToList()
            //                                              }).ToList();


            List<TodoModel.SectionData> SectionsWithTodos;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetTodosWithSectionsForAllProjects", new { CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                SectionsWithTodos = result.Read<TodoModel.SectionData>().ToList();
                if (SectionsWithTodos != null)
                {
                    var todos = result.Read<TodoModel.TodosData>().ToList();

                    foreach (var item in SectionsWithTodos)
                    {
                        item.Todos = todos.Where(x => x.SectionId == item.Id).ToList();
                    }
                }
            }

            return SectionsWithTodos;
        }


        /// <summary>
        /// This function fetches my Todos for MyTOdos Tab
        /// </summary>
        /// <param name="UserId">It is unique id of logged in user</param>
        /// <returns>returns todos in list</returns>
        public List<TodoModel.TodosData> getMyTodos(string UserId)
        {
            List<TodoModel.TodosData> SectionsWithTodos = (from t0 in dbcontext.Teams
                                                           join t3 in dbcontext.TeamUsers on t0.Id equals t3.TeamId
                                                           join t in dbcontext.Projects on t0.Id equals t.TeamId
                                                           join t4 in dbcontext.ProjectUsers on t.Id equals t4.ProjectId
                                                           join t1 in dbcontext.Sections on t.Id equals t1.ProjectId
                                                           join c in dbcontext.ToDos on t1.Id equals c.SectionId
                                                           join c3 in dbcontext.UserDetails on c.CreatedBy equals c3.UserId
                                                           join c1 in dbcontext.UserDetails on c.AssigneeId equals c1.UserId
                                                           into b
                                                           where t3.UserId == UserId && t4.UserId == UserId && c.TaskType == 5 
                                                           && c3.IsDeleted == false && t0.IsDeleted == false && t.IsDeleted == false
                                                           && c.IsDeleted == false
                                                           orderby c.CreatedDate descending

                                                           from c2 in b.DefaultIfEmpty()
                                                           let AssigneeProfilePhoto = c2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c2.ProfilePhoto

                                                           let ReporterProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
                                                           let AssigneeName = c2.FirstName
                                                           let ReporterName = c3.FirstName
                                                           select new TodoModel.TodosData
                                                           {
                                                               Name = c.Name,
                                                               SectionId = c.SectionId,
                                                               TodoId = c.Id,
                                                               AssigneeId = c.AssigneeId,
                                                               AssigneeProfilePhoto = AssigneeProfilePhoto,
                                                               AssigneeName = AssigneeName,
                                                               AssigneeEmail = c2.Email,
                                                               ReporterId = c3.UserId,
                                                               ReporterEmail = c3.Email,
                                                               ReporterName = ReporterName,
                                                               ReporterProfilePhoto = ReporterProfilePhoto,
                                                               Description = c.Description,
                                                               DeadlineDate = c.DeadlineDate,
                                                               InProgress = c.InProgress,
                                                               IsDone = c.IsDone,
                                                               SectionName = t1.SectionName,
                                                               TeamId = t0.Id,
                                                               TeamName = t0.TeamName,
                                                               ProjectId = t.Id,
                                                               ProjectName = t.Name,
                                                               CreatedDate = c.CreatedDate,
                                                               ModifiedDate = c.ModifiedDate
                                                           }).ToList();

            return SectionsWithTodos;
        }




        /// <summary>
        /// This function fetches details of a particular Todo
        /// </summary>
        /// <param name="TodoId">It is unique id of Todo</param>
        /// <returns>returns todos in list</returns>
        public TodoModel.TodosData getTodo(int TodoId, string userId)
        {
            TodoModel.TodosData todoObject = (from t0 in dbcontext.Teams
                                              join t in dbcontext.Projects on t0.Id equals t.TeamId
                                              join t1 in dbcontext.Sections on t.Id equals t1.ProjectId
                                              join c in dbcontext.ToDos on t1.Id equals c.SectionId
                                              join c3 in dbcontext.UserDetails on c.CreatedBy equals c3.UserId
                                              join c1 in dbcontext.UserDetails on c.AssigneeId equals c1.UserId
                                              into b
                                              where c.Id == TodoId && c3.IsDeleted == false && t0.IsDeleted == false
                                              && t.IsDeleted == false && c.IsDeleted == false
                                              orderby c.CreatedDate descending

                                              from c2 in b.DefaultIfEmpty()
                                              let AssigneeProfilePhoto = c2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c2.ProfilePhoto

                                              let ReporterProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
                                              let AssigneeName = c2.FirstName + " " + (string.IsNullOrEmpty(c2.LastName) ? "" : c2.LastName)
                                              let ReporterName = c3.FirstName + " " + (string.IsNullOrEmpty(c3.LastName) ? "" : c2.LastName)
                                              select new TodoModel.TodosData
                                              {
                                                  Name = c.Name,
                                                  SectionId = c.SectionId,
                                                  TodoId = c.Id,
                                                  AssigneeId = c.AssigneeId,
                                                  AssigneeProfilePhoto = AssigneeProfilePhoto,
                                                  AssigneeName = AssigneeName,
                                                  AssigneeEmail = c2.Email,
                                                  ReporterId = c3.UserId,
                                                  ReporterEmail = c3.Email,
                                                  ReporterName = ReporterName,
                                                  ReporterProfilePhoto = ReporterProfilePhoto,
                                                  Description = c.Description,
                                                  DeadlineDate = c.DeadlineDate,
                                                  InProgress = c.InProgress,
                                                  IsDone = c.IsDone,
                                                  SectionName = t1.SectionName,
                                                  TeamId = t0.Id,
                                                  TeamName = t0.TeamName,
                                                  ProjectId = t.Id,
                                                  ProjectName = t.Name,
                                                  CreatedDate = c.CreatedDate,
                                                  ModifiedDate = c.ModifiedDate

                                              }).SingleOrDefault();

            return todoObject;
        }

        public string getLoggedTime(int TodoId, string userId)
        {
            //ToDo todo = (from item in dbcontext.ToDos where item.Id == TodoId && item.IsDeleted == false select item).SingleOrDefault();

            ToDo todo;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from ToDoes item where item.Id = @TodoId and item.IsDeleted = 0 ";
                todo = con.Query<ToDo>(sqlquery, new { TodoId = TodoId }).SingleOrDefault();
            }

            return string.IsNullOrEmpty(todo.LoggedTime) ? "0h 0m" : todo.LoggedTime;
        }


        public object getSingleToDo(int toDoId, string UserId)
        {
            return (from e in dbcontext.ToDos
                    join e7 in dbcontext.Sections on e.SectionId equals e7.Id
                    join e1 in dbcontext.Projects on e7.ProjectId equals e1.Id
                    join e2 in dbcontext.UserDetails on e.CreatedBy equals e2.UserId
                    join e4 in dbcontext.Teams on e1.TeamId equals e4.Id

                    join e3 in dbcontext.UserDetails on e.AssigneeId equals e3.UserId
                    into email
                    where e.Id == toDoId && e2.IsDeleted == false && e4.IsDeleted == false 
                    && e1.IsDeleted == false && e.IsDeleted == false

                    from e5 in dbcontext.ToDos
                    join e6 in dbcontext.UserDetails on e5.AssigneeId equals e6.UserId into Inners
                    where e5.Id == toDoId && e5.IsDeleted == false

                    from od in Inners.DefaultIfEmpty()

                    from e6 in email.DefaultIfEmpty()
                    let AssignedBy = (e2.FirstName + " " + e2.LastName + " ( " + e2.Email + " )")
                    let ProjectName = e1.Name
                    let AssignedTo = (od.FirstName == null ? "" : od.FirstName + " " + od.LastName)

                    let Assigned = (e.AssigneeId == null ? "False" : "True")

                    let DisplayPhoto = (e2.ProfilePhoto == null ? "False" : "True")
                    let DisplayAssigneePhoto = (od.ProfilePhoto == null ? "False" : "True")
                    let CreatorProfilePhoto = (e2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + e2.ProfilePhoto)
                    let AssigneePhoto = (od.ProfilePhoto == null ? null : "Uploads/Profile/Icon/" + od.ProfilePhoto)
                    let AssigneeFirstName = (od.FirstName)
                    let AssigneeLastName = (od.LastName)
                    let Display = (e.CreatedBy == UserId ? "False" : "True")
                    let TaskStatus = e.InProgress == false && e.IsDone == false ? "Pending" : (e.InProgress == true && e.IsDone == false ? "InProgress" : "Done")
                    let IsAssigneeLoggedIn = e.AssigneeId == UserId ? "True" : "False"
                    select new { e.Id, e.Name, e2.FirstName, e2.LastName, e.CreatedBy, e.Description, AssignedBy, ProjectName, AssignedTo, Assigned, DisplayPhoto, CreatorProfilePhoto, e.DeadlineDate, AssigneePhoto, AssigneeFirstName, AssigneeLastName, DisplayAssigneePhoto, Display, e2.Email, e.InProgress, e.IsDone, e4.TeamName, e7.SectionName, TaskStatus, e.AssigneeId, IsAssigneeLoggedIn }).ToList();
        }



        /*  This function is used for saving the new Todo*/
        public int saveToDo(TodosModel.TodoAddModel model, string UserId, int CompanyId)
        {
            //DateTime CurrentDate = DateTime.Now;
            //ToDo todoEntity = new ToDo
            //{
            //    Name = model.Name,
            //    ListOrder = model.TodoListOrder,
            //    CreatedDate = CurrentDate,
            //    CreatedBy = UserId,
            //    ModifiedBy = UserId,
            //    ModifiedDate = CurrentDate,
            //    SectionId = Convert.ToInt32(model.SectionId),
            //    //Description = model.Description,
            //    //AssigneeId = model.AssigneeId,
            //    ReporterId = UserId,
            //    //DeadlineDate = model.DeadlineDate,
            //    //EstimatedTime = model.EstimatedTime,
            //    TaskType = 5,
            //    IsAvailableForInvoice = true
            //};

            //dbcontext.ToDos.Add(todoEntity);
            //dbcontext.SaveChanges();

            //if (model.addType == "middle")   // when we are adding row in between existing rows
            //{
            //    var AllTodosInSection = (from t in dbcontext.ToDos
            //                             where t.SectionId == todoEntity.SectionId
            //                             && t.Id != todoEntity.Id && (t.ListOrder >= model.TodoListOrder)
            //                             && t.IsDeleted == false
            //                             orderby t.ListOrder
            //                             select t).ToList();

            //    int counter = model.TodoListOrder + 1;
            //    foreach (var item in AllTodosInSection)
            //    {
            //        item.ListOrder = counter;
            //        ++counter;
            //        dbcontext.Entry<ToDo>(item).State = EntityState.Modified;
            //    }
            //    dbcontext.SaveChanges();
            //}

            //Follower follower = new Follower
            //{
            //    TodoId = todoEntity.Id,
            //    CreatedDate = DateTime.Now,
            //    CreatedBy = UserId,
            //    UserId = UserId
            //};

            ///* Save Follower */
            //dbcontext.Followers.Add(follower);


            //#region Adding TodoId in ProjectId column in UserSettingConfig for Todo

            ///* getting Settings for the user  */
            //UserSettingConfig usrsetting = (from usrset in dbcontext.UserSettingConfigs
            //                                join set in dbcontext.SettingConfigs on usrset.NotificationId equals set.Id
            //                                where set.IsActive == true
            //                                && usrset.CompanyId == CompanyId
            //                                && usrset.UserId == UserId
            //                                && set.NotificationName == "Todo Update"
            //                                select usrset).FirstOrDefault();

            //if (usrsetting.TodoId != null && usrsetting.TodoId != string.Empty)
            //{
            //    usrsetting.TodoId += "," + todoEntity.Id.ToString();
            //}
            //else
            //{
            //    usrsetting.TodoId = todoEntity.Id.ToString();
            //}

            //#endregion

            //TodoNotifications objtodoNotifications = new TodoNotifications
            //{
            //    TodoId = todoEntity.Id,
            //    Description = "has created this Todo",
            //    CreatedBy = UserId,
            //    CreatedDate = DateTime.Now,
            //    Type = 1
            //};

            ///* Save TodoNotification */
            //dbcontext.TodoNotifications.Add(objtodoNotifications);

            ///* Saving notification for newly added Todo for assignee */
            //Notification objNotification = new Notification
            //{
            //    Name = "has Assigned you a Todo:",
            //    CreatedBy = UserId,
            //    CreatedDate = DateTime.Now,
            //    IsRead = false,
            //    Type = 3,
            //    NotifyingUserId = model.AssigneeId,
            //    TodoId = todoEntity.Id
            //};

            //dbcontext.Notifications.Add(objNotification);
            //dbcontext.SaveChanges();

            int TodoId = 0;
            using (var con = new SqlConnection(ConnectionString))
            {
                TodoId = con.Query<int>("usp_ToDoInsert", new { Name = model.Name, TodoListOrder = model.TodoListOrder, UserId = UserId, AssigneeId = model.AssigneeId, addType = model.addType, SectionId = Convert.ToInt32(model.SectionId), CompanyId = CompanyId }, commandType: CommandType.StoredProcedure).Single();
            }

            return TodoId;
        }



        //public async Task SendMailForSaveNewTodo(TodosModel.TodoAddModel model)
        //{
        //    var details = (from usrdtls in dbcontext.UserDetails where usrdtls.UserId == model.AssigneeId select usrdtls).SingleOrDefault();

        //    String emailbody = "<p>Hello  " + details.FirstName + " </p>";
        //    emailbody += "<p>A new todo is assigned to you </p>";

        //    await EmailUtility.Send(details.Email, "New Todos is Assigned", "");
        //}


        /*  This function is used for Send Mail for saving the new Todo*/
        public void ThreadForSendMailForSaveNewTodo(TodosModel.TodoAddModel model, string UserId, int CompanyId, int ProjectId)
        {

            var details = (from usrdtls in dbcontext.UserDetails where usrdtls.UserId == model.AssigneeId && usrdtls.IsDeleted == false select usrdtls).SingleOrDefault();

            int SectionId = Convert.ToInt32(model.SectionId);

            var Projct = (from sec in dbcontext.Sections
                          join prjct in dbcontext.Projects on sec.ProjectId equals prjct.Id
                          where sec.Id == SectionId && prjct.IsDeleted == false
                          select prjct).FirstOrDefault();

            string ReporterName = (from r in dbcontext.UserDetails
                                   where r.UserId == model.ReporterId
                                   && r.IsDeleted == false
                                   select r.FirstName).FirstOrDefault();

            string CreatorName = (from r in dbcontext.UserDetails
                                  where r.UserId == UserId
                                  && r.IsDeleted == false
                                  select r.FirstName).FirstOrDefault();

            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SaveNewTodo")
                                 //select r;
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var item in MailModule)
            {
                emailbody = item.mailbody;
                Subject = item.subject;
            }

            emailbody = emailbody.Replace("@@ProjectName", Projct.Name).Replace("@@TodoName", model.Name).Replace("@@Receiver_FirstName", details.FirstName).Replace("@@CreatorName", CreatorName);
            emailbody = emailbody.Replace("@@Deadline", model.DeadlineDate.Value.ToString("MM/dd/yyyy")).Replace("@@ReporterName", ReporterName);
            Subject = Subject.Replace("@@ProjectName", Projct.Name).Replace("@@TodoName", model.Name);

            EmailUtility.SendMailInThread(details.Email, Subject, emailbody);

        }


        /* This function is used for Editing Todo */

        public string editToDo(TodosModel.TodoUpdateModel model, string UserId)
        {
            string ValueForSendMailTo = string.Empty;
            string totalLoggedMinutes = "0m";

            if (model.IsEdit == false)
            {
                if (model.InProgress == false && model.IsDone == false || model.IsDone == true)
                {
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlquery = @"select ISNULL(sum(t.Duration),0 ) from TodoTimeLogs t where t.TodoId = @TodoId and t.StartBy = @UserId";
                        int sumOfTime = con.Query<int>(sqlquery, new { TodoId = model.TodoId, UserId = UserId }).Single();
                        totalLoggedMinutes = Utility.ConvertMinutesToFormatedTimeLogString(sumOfTime);
                    }
                }
            }

            //ToDo todoEntity = (from m in dbcontext.ToDos where m.Id == model.TodoId && m.IsDeleted == false select m).SingleOrDefault();

            //todoEntity.Name = model.Name;
            ////todoEntity.Description = model.Description;
            ////todoEntity.DeadlineDate = model.DeadlineDate;
            ////todoEntity.EstimatedTime = model.EstimatedTime;
            ////todoEntity.AssigneeId = model.AssigneeId;
            //////todoEntity.ReporterId = model.ReporterId;
            //todoEntity.ModifiedBy = UserId;
            //todoEntity.ModifiedDate = DateTime.Now;
            ////todoEntity.SectionId = model.SectionId;
            //todoEntity.InProgress = model.InProgress;
            //todoEntity.IsDone = model.IsDone;
            //todoEntity.IsAvailableForInvoice = model.IsAvailableForInvoice;
            //dbcontext.SaveChanges();
            ///*
            //Start progress=> insert new entry in timelog table
            //Stop progress => update entry in timelog table end date time
            //done => calculate the time difference in hours and minutes and update the todo table with this time
            //*/
            //// if todo is not editing then the below code should not be run
            //if (model.IsEdit == false)
            //{
            //    // when user click on the start progress button
            //    if (model.InProgress == true && model.IsDone == false)
            //    {
            //        TodoTimeLogs times = new TodoTimeLogs
            //        {
            //            TodoId = model.TodoId,
            //            StartDateTime = DateTime.Now,
            //            IsAutomaticLogged = true,
            //            loggingTime = DateTime.Now,
            //            StartBy = UserId
            //        };
            //        //dbcontext.TodoTimeLogs.Add(times);
            //        dbcontext.Entry(times).State = EntityState.Added;
            //        dbcontext.SaveChanges();

            //        ValueForSendMailTo = "start progress";

            //    }
            //    // when user click on the stop progress button
            //    if (model.InProgress == false && model.IsDone == false)
            //    {
            //        // getting the last inserted record
            //        TodoTimeLogs timelog = (from m in dbcontext.TodoTimeLogs where m.TodoId == model.TodoId orderby m.Id descending select m).Take(1).SingleOrDefault();
            //        timelog.StopDateTime = DateTime.Now;
            //        TimeSpan interval = timelog.StopDateTime.Value.Subtract(timelog.StartDateTime);
            //        int TotalMinutes = interval.Days * 24 * 60 + interval.Hours * 60 + interval.Minutes;
            //        timelog.Duration = TotalMinutes;
            //        timelog.IsInvoiceGenerated = false;
            //        dbcontext.SaveChanges();

            //        //updating logged ours in todo table
            //        int sumOfTime = (from t in dbcontext.TodoTimeLogs where t.TodoId == model.TodoId && t.StartBy == UserId select t.Duration).Sum();
            //        ToDo todo = (from t in dbcontext.ToDos where t.Id == model.TodoId && t.IsDeleted == false select t).SingleOrDefault();
            //        int totalLoggedMinutes = 0;
            //        totalLoggedMinutes = sumOfTime;
            //        // assigning the converted time string
            //        todo.LoggedTime = Utility.ConvertMinutesToFormatedTimeLogString(totalLoggedMinutes);
            //        dbcontext.SaveChanges();

            //        ValueForSendMailTo = "stop progress";

            //    }
            //    if (model.IsDone == true)
            //    {

            //        //if task is in progress then
            //        if (model.PreviouslyInProgress == true)
            //        {
            //            // getting the last inserted record
            //            TodoTimeLogs timelog = (from m in dbcontext.TodoTimeLogs
            //                                    where m.TodoId == model.TodoId
            //                                    orderby m.Id descending
            //                                    select m).Take(1).SingleOrDefault();
            //            timelog.StopDateTime = DateTime.Now;
            //            timelog.StopBy = UserId;
            //            TimeSpan interval = timelog.StopDateTime.Value.Subtract(timelog.StartDateTime);
            //            int TotalMinutes = interval.Days * 24 * 60 + interval.Hours * 60 + interval.Minutes;
            //            timelog.Duration = TotalMinutes;
            //            timelog.IsInvoiceGenerated = false;
            //            dbcontext.SaveChanges();
            //        }
            //        else
            //        {
            //            //Update the stopped User
            //            TodoTimeLogs timelog = (from m in dbcontext.TodoTimeLogs where m.TodoId == model.TodoId orderby m.Id descending select m).Take(1).SingleOrDefault();
            //            if (timelog != null)
            //            {
            //                timelog.StopBy = UserId;
            //                dbcontext.SaveChanges();
            //            }
            //            else
            //            {
            //                TodoTimeLogs times = new TodoTimeLogs
            //                {
            //                    TodoId = model.TodoId,
            //                    StartDateTime = DateTime.Now,
            //                    IsAutomaticLogged = true,
            //                    loggingTime = DateTime.Now,
            //                    StartBy = UserId,
            //                    StopDateTime = DateTime.Now,
            //                    Duration = 0,
            //                    IsInvoiceGenerated = false
            //                };
            //                //dbcontext.TodoTimeLogs.Add(times);
            //                dbcontext.Entry(times).State = EntityState.Added;
            //                dbcontext.SaveChanges();
            //            }
            //        }

            //        var TimeToCalculateSum = (from t in dbcontext.TodoTimeLogs where t.TodoId == model.TodoId && t.StartBy == UserId select t.Duration).ToList();
            //        int sumOfTime = (TimeToCalculateSum).Sum();
            //        //int sumOfTime = (from t in dbcontext.TodoTimeLogs where t.TodoId == model.TodoId && t.StartBy == UserId select t.Duration).Sum();
            //        ToDo todo = (from t in dbcontext.ToDos where t.Id == model.TodoId && t.IsDeleted == false select t).SingleOrDefault();
            //        int totalLoggedMinutes = 0;

            //        totalLoggedMinutes = sumOfTime;
            //        // assigning the converted time string
            //        todo.LoggedTime = Utility.ConvertMinutesToFormatedTimeLogString(totalLoggedMinutes);
            //        dbcontext.SaveChanges();

            //        ValueForSendMailTo = "done";
            //    }
            //}
            //else
            //{
            //    ValueForSendMailTo = "TodoEditUpdate";
            //    if (todoEntity.InProgress == true)
            //    {
            //        ValueForSendMailTo = "TodoInProgess";
            //        return ValueForSendMailTo;
            //    }
            //}
            ///* Saving Notification for Todo */
            //TodoNotifications objtodoNotifications = new TodoNotifications
            //{
            //    TodoId = model.TodoId,
            //    Description = "has updated the Todo",
            //    CreatedBy = UserId,
            //    CreatedDate = DateTime.Now,
            //    Type = 1
            //};

            ///* Save TodoNotification */
            //dbcontext.TodoNotifications.Add(objtodoNotifications);
            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                ValueForSendMailTo = con.Query<string>("usp_ToDoUpdate", new { TodoId = model.TodoId, Name = model.Name, UserId = UserId, InProgress = model.InProgress, IsDone = model.IsDone, IsAvailableForInvoice = model.IsAvailableForInvoice, IsEdit = model.IsEdit, totalLoggedMinutes = totalLoggedMinutes, PreviouslyInProgress = model.PreviouslyInProgress }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return ValueForSendMailTo;
        }
     
        // Convert the minutes into formated hour minute string( 0h 0m)
        //private string ConvertMinutesToFormatedTimeLogString(int TotalMinutes)
        //{
        //    string FormatedString = "";
        //    int Hours = TotalMinutes / 60;
        //    int Minutes = TotalMinutes % 60;
        //    FormatedString = Hours + "h " + Minutes + "m";
        //    return FormatedString;
        //}


        public async Task SendMailForStartStopTodoProgress(TodosModel.NewTodoUpdateModel obj, string ValueForSendMailTo)
        {

            if (ValueForSendMailTo != string.Empty || ValueForSendMailTo != null)
            {
                // sending mail to Reporter for todo
                var TodosDetails = (from m in dbcontext.ToDos
                                    join u in dbcontext.UserDetails on m.CreatedBy equals u.UserId
                                    where m.Id == obj.TodoUpdateModel.TodoId && u.IsDeleted == false
                                    && m.IsDeleted == false
                                    select new
                                    {
                                        Name = m.Name,
                                        ReporterName = u.FirstName,
                                        ReporterEmail = u.Email
                                    }).SingleOrDefault();

                String emailbody = "<p>Hello  " + TodosDetails.ReporterName + " </p>";
                emailbody += "<p>This is to inform you that progress status has changed for " + TodosDetails.Name + " Todo </p>";

                if (ValueForSendMailTo == "start progress")
                {
                    emailbody += "<p>from Stop to Start</p>";
                }
                else if (ValueForSendMailTo == "stop progress")
                {
                    emailbody += "<p>from Start to Stop</p>";
                }

                /*Send Email to user and inform about Task Assigned */
                await EmailUtility.Send(TodosDetails.ReporterEmail, "Progress status changed for Todos", emailbody);

            }




        }




        public void ThreadForSendMailForStartStopTodoProgress(string url, TodosModel.NewTodoUpdateModel obj, string ValueForSendMailTo, int CompanyId, int ProjectId)
        {
            //Geting info to send in mail
            Project prjct = (from p in dbcontext.Projects where p.Id == ProjectId && p.IsDeleted == false select p).FirstOrDefault();
            string ProjectName = prjct.Name;
            var callbackUrl = url + "#/todo/" + ProjectId + "/" + prjct.TeamId;
            string UserName = (from u in dbcontext.UserDetails where u.UserId == obj.userId && u.IsDeleted == false select u.FirstName).FirstOrDefault();
            string AssigneeName = (from t in dbcontext.ToDos
                                   join u in dbcontext.UserDetails on t.AssigneeId equals u.UserId
                                   where t.Id == obj.TodoUpdateModel.TodoId && u.IsDeleted == false
                                   && t.IsDeleted == false
                                   select u.FirstName).FirstOrDefault();

            List<TodosModel.MailReceiverInfo> AllReceivers = null;

            TodosModel.MailReceiverInfo TodosDetails = (from m in dbcontext.ToDos
                                                        join u in dbcontext.UserDetails on m.ReporterId equals u.UserId
                                                        join usrcomrole in dbcontext.UserCompanyRoles on u.UserId equals usrcomrole.UserId
                                                        where m.Id == obj.TodoUpdateModel.TodoId && usrcomrole.CompanyId == CompanyId 
                                                        && u.IsDeleted == false && m.IsDeleted == false
                                                        select new TodosModel.MailReceiverInfo
                                                        {
                                                            TodoName = m.Name,
                                                            ReceiverUserId = u.UserId,
                                                            ReceiverName = u.FirstName,
                                                            ReceiverEmail = u.Email,
                                                        }).SingleOrDefault();


            AllReceivers = (from p in dbcontext.Followers
                            join p2 in dbcontext.UserDetails on p.UserId equals p2.UserId
                            where p.TodoId == obj.TodoUpdateModel.TodoId && p2.IsDeleted == false
                            select new TodosModel.MailReceiverInfo
                            {
                                TodoName = TodosDetails.TodoName,
                                ReceiverUserId = p.UserId,
                                ReceiverName = p2.FirstName + " " + p2.LastName,
                                ReceiverEmail = p2.Email
                            }).ToList();


            if (!AllReceivers.Any(x => x.ReceiverUserId == TodosDetails.ReceiverUserId))
            {
                AllReceivers.Add(TodosDetails);
            }


            foreach (var item in AllReceivers)
            {
                bool IsSettingActivated = false;

                /*  This function is used for checking wheather mail setting is activated for assignee or not*/
                if (!string.IsNullOrEmpty(item.ReceiverUserId))
                {
                    string UsrActiveTodos = (from usrSetting in dbcontext.UserSettingConfigs
                                             join dfltSet in dbcontext.SettingConfigs on usrSetting.NotificationId equals dfltSet.Id
                                             where dfltSet.NotificationName == "Todo Update"
                                             && usrSetting.UserId == item.ReceiverUserId && usrSetting.CompanyId == CompanyId
                                             select usrSetting.TodoId).FirstOrDefault();

                    int[] AllTodoIds = null;
                    if (UsrActiveTodos != null && UsrActiveTodos != string.Empty)
                    {
                        AllTodoIds = UsrActiveTodos.Split(',').Select(str => int.Parse(str)).ToArray();

                        foreach (var todoItem in AllTodoIds)
                        {
                            if (todoItem == obj.TodoUpdateModel.TodoId)
                            {
                                IsSettingActivated = true;
                            }
                        }
                    }
                }

                if (ValueForSendMailTo != string.Empty || ValueForSendMailTo != null)
                {
                    // sending mail to Reporter for todo and to All Admins 

                    if (IsSettingActivated || item.ReceiverUserId == TodosDetails.ReceiverUserId)
                    {

                        //String emailbody = "<p>Hello  " + item.ReceiverName + " </p>";
                        //emailbody += "<p>This is to inform you that progress status has changed for " + TodoName + " Todo </p>";

                        String emailbody = null;
                        string Subject = string.Empty;

                        if (ValueForSendMailTo == "start progress")
                        {
                            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                            var MailModule = from r in xdoc.Descendants("MailModule").
                                             Where(r => (string)r.Attribute("id") == "TodoStartProgress")
                                                 //select r;
                                             select new
                                             {
                                                 mailbody = r.Element("Body").Value,
                                                 subject = r.Element("Subject").Value
                                             };

                            foreach (var Mailitem in MailModule)
                            {
                                emailbody = Mailitem.mailbody;
                                Subject = Mailitem.subject;
                            }

                        }
                        else if (ValueForSendMailTo == "stop progress")
                        {
                            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                            var MailModule = from r in xdoc.Descendants("MailModule").
                                             Where(r => (string)r.Attribute("id") == "TodoStopProgress")
                                                 //select r;
                                             select new
                                             {
                                                 mailbody = r.Element("Body").Value,
                                                 subject = r.Element("Subject").Value
                                             };

                            foreach (var Mailitem in MailModule)
                            {
                                emailbody = Mailitem.mailbody;
                                Subject = Mailitem.subject;
                            }

                        }
                        else if (ValueForSendMailTo == "done")
                        {
                            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                            var MailModule = from r in xdoc.Descendants("MailModule").
                                             Where(r => (string)r.Attribute("id") == "TodoDoneProgress")
                                                 //select r;
                                             select new
                                             {
                                                 mailbody = r.Element("Body").Value,
                                                 subject = r.Element("Subject").Value
                                             };

                            foreach (var Mailitem in MailModule)
                            {
                                emailbody = Mailitem.mailbody;
                                Subject = Mailitem.subject;
                            }

                        }
                        else if (ValueForSendMailTo == "TodoEditUpdate")
                        {
                            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                            var MailModule = from r in xdoc.Descendants("MailModule").
                                             Where(r => (string)r.Attribute("id") == "TodoEditUpdate")
                                                 //select r;
                                             select new
                                             {
                                                 mailbody = r.Element("Body").Value,
                                                 subject = r.Element("Subject").Value
                                             };

                            foreach (var Mailitem in MailModule)
                            {
                                emailbody = Mailitem.mailbody;
                                Subject = Mailitem.subject;
                            }

                            emailbody = emailbody.Replace("@@UserName", UserName);
                        }

                        //emailbody = emailbody.Replace("@@ReceiverName", item.ReceiverName).Replace("@@ProjectName", ProjectName).Replace("@@TodoName", TodosDetails.TodoName);
                        //emailbody = emailbody.Replace("@@AssigneeName", AssigneeName).Replace("@@Deadline", obj.TodoUpdateModel.DeadlineDate.Value.ToString("MM/dd/yyyy")).Replace("@@ReporterName", TodosDetails.ReceiverName);
                        //emailbody = emailbody.Replace("@@callbackUrl", callbackUrl);

                        //Subject = Subject.Replace("@@ProjectName", ProjectName).Replace("@@TodoName", TodosDetails.TodoName);

                        ///*Send Email to user and inform about Task Assigned */
                        //EmailUtility.SendMailInThread(item.ReceiverEmail, Subject, emailbody);
                    }
                }
            }

            if (ValueForSendMailTo == "TodoEditUpdate")
            {
                TodosModel.MailReceiverInfo AssigneeDetails = (from m in dbcontext.ToDos
                                                               join u in dbcontext.UserDetails on m.AssigneeId equals u.UserId
                                                               join usrcomrole in dbcontext.UserCompanyRoles on u.UserId equals usrcomrole.UserId
                                                               where m.Id == obj.TodoUpdateModel.TodoId && usrcomrole.CompanyId == CompanyId 
                                                               && u.IsDeleted == false && m.IsDeleted == false
                                                               select new TodosModel.MailReceiverInfo
                                                               {
                                                                   TodoName = m.Name,
                                                                   ReceiverUserId = u.UserId,
                                                                   ReceiverName = u.FirstName,
                                                                   ReceiverEmail = u.Email,
                                                               }).FirstOrDefault();
                if (AssigneeDetails != null)
                {
                    String emailbody = null;
                    string Subject = string.Empty;
                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                    var MailModule = from r in xdoc.Descendants("MailModule").
                                     Where(r => (string)r.Attribute("id") == "TodoEditUpdate")
                                         //select r;
                                     select new
                                     {
                                         mailbody = r.Element("Body").Value,
                                         subject = r.Element("Subject").Value
                                     };

                    foreach (var Mailitem in MailModule)
                    {
                        emailbody = Mailitem.mailbody;
                        Subject = Mailitem.subject;
                    }

                    emailbody = emailbody.Replace("@@ReceiverName", AssigneeDetails.ReceiverName).Replace("@@ProjectName", ProjectName).Replace("@@TodoName", TodosDetails.TodoName);
                    emailbody = emailbody.Replace("@@AssigneeName", AssigneeName).Replace("@@Deadline", obj.TodoUpdateModel.DeadlineDate.Value.ToString("MM/dd/yyyy")).Replace("@@ReporterName", TodosDetails.ReceiverName);
                    emailbody = emailbody.Replace("@@callbackUrl", callbackUrl).Replace("@@UserName", UserName);

                    Subject = Subject.Replace("@@ProjectName", ProjectName).Replace("@@TodoName", TodosDetails.TodoName);

                    /*Send Email to user and inform about Task Assigned */
                    EmailUtility.SendMailInThread(AssigneeDetails.ReceiverEmail, Subject, emailbody);

                }
            }


        }



        public List<TaskTypeForCreatingTodo> getTodoTags(string TaskTypeIds, int TodoId)
        {

            List<TaskTypeForCreatingTodo> TodoTaskType = new List<TaskTypeForCreatingTodo>();

            //var TaskType = (from todo in dbcontext.ToDos where todo.Id == TodoId && todo.IsDeleted == false select todo.TodoTaskTypeId).SingleOrDefault();

            string TaskType = string.Empty;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select TodoTaskTypeId from ToDoes  where Id = @TodoId and IsDeleted = 0";
                TaskType = con.Query<string>(sqlquery, new { TodoId = TodoId }).FirstOrDefault();

                if (TaskType != string.Empty && TaskType != null)
                {

                    //int[] TaskTypeArray = TaskType.Split(',').Select(str => int.Parse(str)).ToArray();

                    //TodoTaskType = (from list in dbcontext.Tags
                    //                where TaskTypeArray.Contains(list.Id)
                    //                select new TaskTypeForCreatingTodo
                    //                {
                    //                    Id = list.Id,
                    //                    Text = list.TagName
                    //                }).ToList();

                    string sqlquery_TodoTaskType = @"select Id,TagName as Text from Tags where Id in ("+ TaskType+")";

                    TodoTaskType = con.Query<TaskTypeForCreatingTodo>(sqlquery_TodoTaskType).ToList();

                }


            }

                

            return TodoTaskType;
        }



        public async Task updateToDo(TodoModel.ToDo model)
        {

            ToDo todoentity = (from e in dbcontext.ToDos
                               where e.Id == model.Id
                               && e.IsDeleted == false
                               select e).SingleOrDefault();


            /*  updateFlag is 2 for updating only Assignee*/
            if (model.updateFlag == 2)
            {
                todoentity.ModifiedDate = DateTime.Now.ToUniversalTime();
                todoentity.AssigneeId = model.AssigneeId;
                todoentity.IsRead = false;
                todoentity.InProgress = false;

                /* Getting User Details*/

                var user = (from u in dbcontext.UserDetails where u.UserId == model.AssigneeId && u.IsDeleted == false select u)
                         .SingleOrDefault();

                /* Send Email to new Assignee */
                // await sendAssigneeEmail(user.Email, model.Name);

                /* Update ToDos */
                dbcontext.SaveChanges();
            }

            /*  updateFlag is 6 for removing the assignee */
            else if (model.updateFlag == 6)
            {
                todoentity.ModifiedDate = DateTime.Now.ToUniversalTime();
                todoentity.AssigneeId = null;
                todoentity.InProgress = false;
                todoentity.IsDone = false;
                /* Update ToDos */
                dbcontext.SaveChanges();



            }

            /*  updateFlag is 3 for updating only Description*/
            else if (model.updateFlag == 3)
            {
                todoentity.ModifiedDate = DateTime.Now.ToUniversalTime();
                todoentity.Description = model.Description;
                /* Update ToDos */
                dbcontext.SaveChanges();
            }

            /*  updateFlag is 4 for updating only IsDone*/
            else if (model.updateFlag == 4)
            {
                todoentity.ModifiedDate = DateTime.Now.ToUniversalTime();
                // todoentity.IsDone = model.IsDone;
                todoentity.DateOfCompletion = DateTime.Now.ToUniversalTime();
                /* Update ToDos */
                dbcontext.SaveChanges();
            }

            /*  updateFlag is 5 for updating only Section Id*/
            else if (model.updateFlag == 5)
            {
                todoentity.ModifiedDate = DateTime.Now.ToUniversalTime();
                todoentity.SectionId = model.SectionId;
                /* Update ToDos */
                dbcontext.SaveChanges();
            }
            /*  updateFlag is 7 for updating only IsRead*/
            else if (model.updateFlag == 7)
            {

                todoentity.IsRead = true;
                /* Update ToDos */
                dbcontext.SaveChanges();
            }
            /*  updateFlag is 8 for updating only InProgress*/
            else if (model.updateFlag == 8)
            {

                todoentity.InProgress = true;
                /* Update ToDos */
                dbcontext.SaveChanges();
            }
            else
            {
                if (model.selfAssigned)
                    model.AssigneeId = model.ModifiedBy;

                if (!String.IsNullOrEmpty(model.AssigneeId))
                {

                    bool sendEmail = false;

                    if (String.IsNullOrEmpty(todoentity.AssigneeId))
                        sendEmail = true;
                    else if (!(Convert.ToString(todoentity.AssigneeId).Equals(Convert.ToString(model.AssigneeId))))
                        sendEmail = true;

                    if (sendEmail)
                    {

                        var user = (from u in dbcontext.UserDetails where u.UserId == model.AssigneeId && u.IsDeleted == false select u)
                            .SingleOrDefault();

                        /* Send Email to new Assignee */
                        //await sendAssigneeEmail(user.Email, model.Name);

                    }
                }


                todoentity.Name = model.Name;
                todoentity.ModifiedDate = DateTime.Now.ToUniversalTime();
                todoentity.Description = model.Description;
                todoentity.DeadlineDate = model.DeadlineDate;

                int intSectionId;
                if (model.SectionId == 0)
                {
                    Section objsec2 = (from m in dbcontext.Sections where m.ProjectId == model.ProjectId && m.IsDefault == true select m).FirstOrDefault();
                    intSectionId = objsec2.Id;
                    todoentity.SectionId = intSectionId;
                }
                else
                {
                    todoentity.SectionId = model.SectionId;

                }

                /* Update ToDos */
                dbcontext.SaveChanges();

            }
        }

        public void deleteToDo(int Id)
        {
            //int SectionId = 0;
            //ToDo todoentity = (from e in dbcontext.ToDos
            //                   where e.Id == Id
            //                   && e.IsDeleted == false
            //                   select e).SingleOrDefault();

            //SectionId = todoentity.SectionId;

            ///*  Delete ToDo  */
            ////dbcontext.ToDos.Remove(todoentity);
            //todoentity.IsDeleted = true;
            //dbcontext.SaveChanges();

            //var AllTodosInSection = (from t in dbcontext.ToDos
            //                         where t.SectionId == SectionId
            //                         && t.IsDeleted == false
            //                         orderby t.ListOrder
            //                         select t).ToList();

            //int counter = 0;
            //foreach (var item in AllTodosInSection)
            //{
            //    item.ListOrder = counter;
            //    ++counter;
            //    dbcontext.Entry<ToDo>(item).State = EntityState.Modified;
            //}
            //dbcontext.SaveChanges();

            int Todo_Id = Id;

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("usp_ToDoDelete", new { Todo_Id = Todo_Id },commandType:CommandType.StoredProcedure);
            }

        }

        public object getAssigneeUsers(string userId)
        {
            var result = (from e in dbcontext.UserDetails where e.UserId == userId && e.IsDeleted == false select e)
                      .SingleOrDefault();

            if (String.IsNullOrEmpty(result.ParentUserId))
            {
                return (from e in dbcontext.UserDetails

                        join e4 in dbcontext.UserDetails on e.UserId equals e4.UserId
                        where (e.ParentUserId == userId || e.UserId == userId) && e.IsDeleted == false

                        from e1 in dbcontext.UserDetails
                        where e1.UserId == userId && e1.IsDeleted == false


                        let loggedInUser = (e1.FirstName)
                        let Display = (e.ProfilePhoto == null ? "False" : "True")
                        let ProfilePhoto = (e.ProfilePhoto == null ? null : "Uploads/Profile/Icon/" + e.ProfilePhoto)
                        let ProfileThumbnail = (e.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + e.ProfilePhoto)
                        let color = ""
                        select new { e.UserId, e.CompanyId, e.FirstName, e.Gender, e.Id, e.IsSubscribed, e.LastName, e.ParentUserId, e4.Email, loggedInUser, ProfilePhoto, Display, e.Department, e.Designation, e.PhoneNo, e.Expertise, e.Experience, ProfileThumbnail, color }
                       );
            }
            else
            {
                return (from e in dbcontext.UserDetails
                            //join e4 in dbcontext.UserDetails on e.UserId equals e4.Id
                        where (e.ParentUserId == result.ParentUserId || e.UserId == result.ParentUserId) && e.IsDeleted == false
                        from e1 in dbcontext.UserDetails
                        where e1.UserId == userId && e1.IsDeleted == false

                        let loggedInUser = (e1.FirstName)
                        let Display = (e.ProfilePhoto == null ? "False" : "True")
                        let ProfilePhoto = (e.ProfilePhoto == null ? null : "Uploads/Profile/Icon/" + e.ProfilePhoto)
                        let ProfileThumbnail = (e.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + e.ProfilePhoto)
                        let color = ""
                        select new { e.UserId, e.CompanyId, e.FirstName, e.Gender, e.Id, e.IsSubscribed, e.LastName, e.ParentUserId, e.Email, loggedInUser, ProfilePhoto, Display, e.Department, e.Designation, e.PhoneNo, e.Expertise, e.Experience, ProfileThumbnail, color });
            }

        }

        public async Task sendAssigneeEmail(string destinationemail, string taskName)
        {

            String emailbody = "<p>A new Task has been assigned to you.</p>";
            emailbody += "<p>Task by name <strong>" + taskName + "</strong>  has been assigned to you.</p>";
            emailbody += "<p>Login to your Smart Admin account to update your progress.</p>";
            /*Send Email to user and inform about Task Assigned */
            await EmailUtility.Send(destinationemail, "Task Assigned", emailbody);

        }


        //Get current Running task of the user
        public TodoTimeLogModel getRunningTaskDetail(string UserId)
        {
            //TodoTimeLogModel TodoLogTime = (from log in dbcontext.TodoTimeLogs
            //                                join todo in dbcontext.ToDos on log.TodoId equals todo.Id
            //                                where log.StopDateTime == null && log.StopBy == null && todo.InProgress == true
            //                                && log.StartBy == UserId && todo.IsDeleted == false
            //                                select new TodoTimeLogModel
            //                                {
            //                                    TodoId = log.TodoId,
            //                                    TodoName = todo.Name,
            //                                    Description = todo.Description,
            //                                    EstimatedTime = todo.EstimatedTime,
            //                                    LoggedTime = todo.LoggedTime,
            //                                    StartDateTime = log.StartDateTime,
            //                                    loggingTime = log.loggingTime,
            //                                    StartBy = log.StartBy,
            //                                    StopBy = log.StopBy,
            //                                    StopDateTime = log.StopDateTime,
            //                                }).SingleOrDefault();

            TodoTimeLogModel TodoLogTime;
            using (var con = new SqlConnection(ConnectionString))
            {
                TodoLogTime = con.Query<TodoTimeLogModel>("usp_GetRunningTaskDetail", new { UserId = UserId }, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }

            // Calculating the time to insert into the todo and log table
            if (TodoLogTime != null)
            {
                TimeSpan interval = DateTime.Now.Subtract(TodoLogTime.StartDateTime);
                int TotalMinutes = interval.Days * 24 * 60 + interval.Hours * 60 + interval.Minutes;
                TodoLogTime.CurrentSessionTime = TotalMinutes;
            }

            return TodoLogTime;
        }


        //public List<TodoDetailForNotification> getTodoDetailForNotification(int TodoId)
        //{
        //    var model = (from todo in dbcontext.ToDos
        //                 where todo.Id == TodoId
        //                 select new
        //                 {
        //                     TodoName = todo.Name,
        //                     TodoDescription = todo.Description,
        //                     LoggedTime = todo.LoggedTime,
        //                     AssignedByName = (from assignname in dbcontext.UserDetails
        //                                       where assignname.UserId == todo.CreatedBy
        //                                       select assignname.FirstName).FirstOrDefault(),
        //                     AssignedToName = (from assignname in dbcontext.UserDetails
        //                                       where assignname.UserId == todo.AssigneeId
        //                                       select assignname.FirstName).FirstOrDefault(),
        //                     Followers = (from flwr in dbcontext.Followers
        //                                  join usr_details in dbcontext.UserDetails on flwr.UserId equals usr_details.UserId
        //                                  where flwr.TodoId == todo.Id
        //                                  select new FollowersForNotification
        //                                  {
        //                                      FollowersName = usr_details.FirstName,
        //                                      FollowersUserId = usr_details.UserId,
        //                                      ProfilePic = string.IsNullOrEmpty(usr_details.ProfilePhoto) ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + usr_details.ProfilePhoto,

        //                                  }
        //                                 )
        //                 });

        //    List<TodoDetailForNotification> mdl = model.AsEnumerable().Select(item =>
        //    new TodoDetailForNotification
        //    {
        //        TodoName = item.TodoName,
        //        TodoDescription = item.TodoDescription,
        //        LoggedTime = item.LoggedTime,
        //        AssignedByName = item.AssignedByName,
        //        AssignedToName = item.AssignedToName,
        //        Followers = (item.Followers).ToList()
        //    }
        //     ).ToList();

        //    return mdl;

        //}


        public List<TodoDetailForNotification> getTodoDetailForNotification(int TodoId)
        {
            var model = (from todo in dbcontext.ToDos
                         where todo.Id == TodoId && todo.IsDeleted == false
                         select new
                         {
                             TodoName = todo.Name,
                             TodoDescription = todo.Description,
                             LoggedTime = todo.LoggedTime,
                             //AssignedByName = (from assignname in dbcontext.UserDetails
                             //                  where assignname.UserId == todo.CreatedBy
                             //                  select assignname.FirstName).FirstOrDefault(),
                             //AssignedByUserId = (from assignname in dbcontext.UserDetails
                             //                    where assignname.UserId == todo.CreatedBy
                             //                    select assignname.UserId).FirstOrDefault(),
                             //AssignedToName = (from assignname in dbcontext.UserDetails
                             //                  where assignname.UserId == todo.AssigneeId
                             //                  select assignname.FirstName).FirstOrDefault(),
                             //AssignedToUserId = (from assignname in dbcontext.UserDetails
                             //                    where assignname.UserId == todo.AssigneeId
                             //                    select assignname.UserId).FirstOrDefault(),


                             assignedTo = (from assignto in dbcontext.UserDetails
                                           where assignto.UserId == todo.AssigneeId && assignto.IsDeleted == false
                                           select new AssignedToForNotification
                                           {
                                               AssignedToName = assignto.FirstName,
                                               AssignedToUserId = assignto.UserId,
                                               AssignedToProfilePic = string.IsNullOrEmpty(assignto.ProfilePhoto) ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + assignto.ProfilePhoto,
                                           }).FirstOrDefault(),

                             assignedBy = (from assignby in dbcontext.UserDetails
                                           where assignby.UserId == todo.CreatedBy && assignby.IsDeleted == false
                                           select new AssignedByForNotification
                                           {
                                               AssignedByName = assignby.FirstName,
                                               AssignedByUserId = assignby.UserId,
                                               AssignedByProfilePic = string.IsNullOrEmpty(assignby.ProfilePhoto) ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + assignby.ProfilePhoto,
                                           }).FirstOrDefault(),

                             Followers = (from flwr in dbcontext.Followers
                                          join usr_details in dbcontext.UserDetails on flwr.UserId equals usr_details.UserId
                                          where flwr.TodoId == todo.Id && usr_details.IsDeleted == false
                                          select new FollowersForNotification
                                          {
                                              FollowersName = usr_details.FirstName,
                                              FollowersUserId = usr_details.UserId,
                                              ProfilePic = string.IsNullOrEmpty(usr_details.ProfilePhoto) ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + usr_details.ProfilePhoto,
                                          }
                                         )
                         });

            List<TodoDetailForNotification> mdl = model.AsEnumerable().Select(item =>
            new TodoDetailForNotification
            {
                TodoName = item.TodoName,
                TodoDescription = item.TodoDescription,
                LoggedTime = item.LoggedTime,
                AssignedTo = item.assignedTo,
                AssignedBy = item.assignedBy,
                Followers = (item.Followers).ToList()
            }
             ).ToList();

            return mdl;

        }


        public BothTaskTypeForSelectize getTaskTypeForCreatingTodo(int TodoId)
        {
            List<TaskTypeForCreatingTodo> AllTaskType = new List<TaskTypeForCreatingTodo>();
            List<TaskTypeForCreatingTodo> TodoTaskType = new List<TaskTypeForCreatingTodo>();

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select Id as Id,TagName as Text,TagDescription as Description from Tags ";
                AllTaskType = con.Query<TaskTypeForCreatingTodo>(sqlquery).ToList();
            }


            //AllTaskType = (from list in dbcontext.Tags
            //               select new TaskTypeForCreatingTodo
            //               {
            //                   Id = list.Id,
            //                   Text = list.TagName,
            //                   Description = list.TagDescription
            //               }).ToList();



            if (TodoId != 0)
            {

                //var TaskType = (from todo in dbcontext.ToDos where todo.Id == TodoId && todo.IsDeleted == false select todo.TodoTaskTypeId).SingleOrDefault();

                string TaskType = string.Empty;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"select todo.TodoTaskTypeId from ToDoes todo where todo.Id = @TodoId and todo.IsDeleted = 0";
                    TaskType = con.Query<string>(sqlquery, new { TodoId = TodoId }).SingleOrDefault();

                    if (TaskType != null && TaskType != string.Empty)
                    {

                        //int[] TaskTypeArray = TaskType.Split(',').Select(str => int.Parse(str)).ToArray();

                        string sqlquery_TodoTaskType = @"select Id,TagName as Text,TagDescription as Description from Tags where Id in (" + TaskType + ")";

                        TodoTaskType = con.Query<TaskTypeForCreatingTodo>(sqlquery_TodoTaskType).ToList();



                        //TodoTaskType = (from list in dbcontext.Tags
                        //                where TaskTypeArray.Contains(list.Id)
                        //                select new TaskTypeForCreatingTodo
                        //                {
                        //                    Id = list.Id,
                        //                    Text = list.TagName,
                        //                    Description = list.TagDescription
                        //                }).ToList();
                    }

                }

                

            }


            BothTaskTypeForSelectize taskList = new BothTaskTypeForSelectize();
            taskList.AllTaskTypes = AllTaskType;
            taskList.TodoTaskType = TodoTaskType;


            return taskList;
        }

        public void SaveTodoDescription(TodosModel.TodoUpdateModel model)
        {
            //ToDo tdo = (from t in dbcontext.ToDos
            //            where t.Id == model.TodoId && t.IsDeleted == false
            //            select t).FirstOrDefault();

            //if (tdo != null)
            //{
            //    tdo.Description = model.Description;
            //    tdo.ModifiedDate = DateTime.Now;
            //    dbcontext.SaveChanges();
            //}

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update ToDoes set Description = @Description, ModifiedDate = GETDATE() where Id = @TodoId and IsDeleted =0 ";
                con.Execute(sqlquery, new { Description = model.Description, TodoId = model.TodoId });
            }

        }

        public void UpdateTodoName(TodosModel.TodoUpdateModel model)
        {
            //ToDo tdo = (from t in dbcontext.ToDos
            //            where t.Id == model.TodoId && t.IsDeleted == false
            //            select t).FirstOrDefault();

            //if (tdo != null)
            //{
            //    tdo.Name = model.Name;
            //    tdo.ModifiedDate = DateTime.Now;
            //    dbcontext.SaveChanges();
            //}

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update ToDoes set Name = @Name, ModifiedDate = GETDATE() where Id = @TodoId and IsDeleted =0 ";
                con.Execute(sqlquery, new { Name = model.Name, TodoId = model.TodoId });
            }

        }

        public void UpdateTodoReporter(string UserId, TodosModel.ReporterUpdateModel  model)
        {

            //ToDo todoEntity = (from m in dbcontext.ToDos where m.Id == model.TodoId && m.IsDeleted == false select m).SingleOrDefault();

            //todoEntity.ModifiedBy = UserId;
            //todoEntity.ModifiedDate = DateTime.Now;
            //todoEntity.ReporterId = model.ReporterId;

            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update ToDoes set ModifiedBy = @UserId, ModifiedDate = GETDATE(),ReporterId = @ReporterId where Id = @TodoId and IsDeleted = 0 ";
                con.Execute(sqlquery, new { UserId = UserId, ReporterId = model.ReporterId, TodoId = model.TodoId });
            }

        }


        public void UpdateTodoDeadlineDate(TodosModel.TodoUpdateModel model)
        {
            //ToDo tdo = (from t in dbcontext.ToDos
            //            where t.Id == model.TodoId && t.IsDeleted == false
            //            select t).FirstOrDefault();

            //if (tdo != null)
            //{
            //    tdo.DeadlineDate = model.DeadlineDate;
            //    tdo.ModifiedDate = DateTime.Now;
            //    dbcontext.SaveChanges();
            //}

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update ToDoes set DeadlineDate = @DeadlineDate, ModifiedDate = GETDATE() where Id = @TodoId and IsDeleted = 0 ";
                con.Execute(sqlquery, new { DeadlineDate = model.DeadlineDate, TodoId = model.TodoId });
            }

        }


        public void UpdateTodoEstimatedTime(TodosModel.TodoUpdateModel model)
        {
            //ToDo tdo = (from t in dbcontext.ToDos
            //            where t.Id == model.TodoId && t.IsDeleted == false
            //            select t).FirstOrDefault();

            //if (tdo != null)
            //{
            //    tdo.EstimatedTime = model.EstimatedTime;
            //    tdo.ModifiedDate = DateTime.Now;
            //    dbcontext.SaveChanges();
            //}

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update ToDoes set EstimatedTime = @EstimatedTime, ModifiedDate = GETDATE() where Id = @TodoId and IsDeleted = 0 ";
                con.Execute(sqlquery, new { EstimatedTime = model.EstimatedTime, TodoId = model.TodoId });
            }

        }

        public void UpdateTodoTaskType(TodosModel.TodoUpdateModel model)
        {
            string TodoTaskTypeIds = string.Empty;
            foreach (var item in model.TaskTypeId)
            {
                if (TodoTaskTypeIds == string.Empty)
                {
                    TodoTaskTypeIds = item.Id.ToString();
                }
                else
                {
                    TodoTaskTypeIds += "," + item.Id.ToString();
                }
            }

            //ToDo tdo = (from t in dbcontext.ToDos
            //            where t.Id == model.TodoId && t.IsDeleted == false
            //            select t).FirstOrDefault();

            //if (tdo != null)
            //{
            //    tdo.TodoTaskTypeId = TodoTaskTypeIds;
            //    tdo.ModifiedDate = DateTime.Now; 
            //    dbcontext.SaveChanges();
            //}

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update ToDoes set TodoTaskTypeId = @TodoTaskTypeIds, ModifiedDate = GETDATE() where Id = @TodoId and IsDeleted = 0 ";
                con.Execute(sqlquery, new { TodoTaskTypeIds = TodoTaskTypeIds, TodoId = model.TodoId });
            }
        }





    }
}
