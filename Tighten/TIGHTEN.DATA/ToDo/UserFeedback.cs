﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using System.Data.SqlClient;
using Dapper;
using System.Data;

namespace TIGHTEN.DATA
{
    public class UserFeedback : BaseClass
    {
        AppContext dbcontext;


        /// <summary>
        /// This method saves the User Feedback
        /// </summary>
        /// <param name="userId">It is the unique id of logged in User</param>
        /// <param name="model">This model consists all new values of fields to be saved</param>
        public void SaveUserFeedback(string userId, UserFeedbackModel.UserFeedbackAddModel model)
        {
            dbcontext = new AppContext();

            //TIGHTEN.ENTITY.UserFeedback userFeedbackEntity = new TIGHTEN.ENTITY.UserFeedback
            //{

            //    Title = model.Title,
            //    Description = model.Description,
            //    IsPrivate = model.IsPrivate,
            //    Priority = model.Priority,
            //    CreatedDate = DateTime.Now,
            //    CreatedBy = userId,
            //    CompanyId = model.CompanyId

            //};


            //dbcontext.UserFeedback.Add(userFeedbackEntity);
            //dbcontext.SaveChanges();




            int isUpdate;

            {
                using (var con = new SqlConnection(ConnectionString))
                {

                    isUpdate = con.Query<int>("Usp_SaveUserFeedback", new
                    {
                        Title = model.Title,
                        Description = model.Description,
                        IsPrivate = model.IsPrivate,
                        Priority = model.Priority,
                        CreatedBy = userId,
                        CompanyId = model.CompanyId

                    },
                    commandType: System.Data.CommandType.StoredProcedure).SingleOrDefault();



                }




            }




        }
        public List<UserFeedbackModel.UserFeedbackDetailsModel> GetUserFeedbacks(string userId)
        {
            List<UserFeedbackModel.UserFeedbackDetailsModel> userFeedbacks;
            List<UserFeedbackModel.UserFeedbackVotersDetailsModel> userFeedbacksVoterDetail;
            using (var con = new SqlConnection(ConnectionString))
            {

                var result = con.QueryMultiple("Usp_GetUserFeedbacks", new { userId = userId }, commandType: CommandType.StoredProcedure);



                userFeedbacks = result.Read<UserFeedbackModel.UserFeedbackDetailsModel>().ToList();
                userFeedbacksVoterDetail = result.Read<UserFeedbackModel.UserFeedbackVotersDetailsModel>().ToList();

                if (userFeedbacks != null)
                {
                    foreach (var item in userFeedbacks)
                    {
                        item.Voters =  userFeedbacksVoterDetail.Where(x => x.UserFeedbackId == item.UserFeedbackId).ToList();

                    }

                }
            }

            return userFeedbacks;

        }




        /// <summary>
        /// It gets the User Feedbacks
        /// </summary>
        /// <param name="userId">It is the unique id of logged in user</param>
        //public List<UserFeedbackModel.UserFeedbackDetailsModel> GetUserFeedbacks1(string userId)
        //{
        //    dbcontext = new AppContext();

        //    var userFeedbacksResult = (from m in dbcontext.UserFeedback
        //                               join m1 in dbcontext.UserDetails on m.CreatedBy equals m1.UserId
        //                               join m2 in dbcontext.UserFeedbackVoters on m.Id equals m2.UserFeedbackId
        //                               into voters
        //                               where m1.IsDeleted == false
        //                               orderby m.CreatedDate descending, m.Priority descending
        //                               select new
        //                               {
        //                                   HasVoted = voters.Count(l => l.VoterUserId == userId) > 0 ? true : false,
        //                                   UserFeedbackId = m.Id,
        //                                   CreatorName = m1.FirstName + " " + (string.IsNullOrEmpty(m1.LastName) ? "" : m1.LastName),
        //                                   CreatorUserId = m1.UserId,
        //                                   CreatorProfilePhoto = m1.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m1.ProfilePhoto,
        //                                   CreatorEmail = m1.Email,
        //                                   Description = m.Description,
        //                                   Title = m.Title,
        //                                   IsPrivate = m.IsPrivate,
        //                                   Priority = m.Priority,
        //                                   CreatedDate = m.CreatedDate,
        //                                   Voters = (from v in dbcontext.UserFeedbackVoters
        //                                             join v1 in dbcontext.UserDetails on v.VoterUserId equals v1.UserId
        //                                             where v.UserFeedbackId == m.Id && v1.IsDeleted == false
        //                                             orderby v.VoterUserId == userId descending
        //                                             select new UserFeedbackModel.UserFeedbackVotersDetailsModel
        //                                             {
        //                                                 VoterEmail = v1.Email,
        //                                                 VoterName = v1.FirstName + " " + v1.LastName,
        //                                                 VoterProfilePhoto = v1.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + v1.ProfilePhoto
        //                                             })
        //                               });
        //    List<UserFeedbackModel.UserFeedbackDetailsModel> userFeedbacks = userFeedbacksResult.AsEnumerable().Select(item =>
        //                                                                    new UserFeedbackModel.UserFeedbackDetailsModel
        //                                                                    {
        //                                                                        HasVoted = item.HasVoted,
        //                                                                        UserFeedbackId = item.UserFeedbackId,
        //                                                                        CreatorName = item.CreatorName,
        //                                                                        CreatorUserId = item.CreatorUserId,
        //                                                                        CreatorProfilePhoto = item.CreatorProfilePhoto,
        //                                                                        CreatorEmail = item.CreatorEmail,
        //                                                                        Description = item.Description,
        //                                                                        Title = item.Title,
        //                                                                        IsPrivate = item.IsPrivate,
        //                                                                        Priority = item.Priority,
        //                                                                        CreatedDate = item.CreatedDate,
        //                                                                        Voters = item.Voters.ToList()
        //                                                                    }).ToList();
        //    return userFeedbacks;
        //}



        /// <summary>
        /// This method Updates the User Feedback Voter
        /// </summary>
        /// <param name="userId">It is the unique id of logged in User</param>
        /// <param name="model">This model consists all new values of fields to be saved</param>
        public void UpdateUserFeedbackVoter(string userId, UserFeedbackModel.UserFeedbackVoterUpdateModel model)
        {
            dbcontext = new AppContext();


            if (model.Vote)
            {
                UserFeedbackVoters userFeedbackVotersEntity = new UserFeedbackVoters
                {
                    UserFeedbackId = model.UserFeedbackId,
                    CreatedDate = DateTime.Now,
                    VoterUserId = userId
                };

                dbcontext.UserFeedbackVoters.Add(userFeedbackVotersEntity);
                dbcontext.SaveChanges();

            }
            else
            {
                UserFeedbackVoters userFeedbackVotersEntity = (from m in dbcontext.UserFeedbackVoters where m.UserFeedbackId == model.UserFeedbackId && m.VoterUserId == userId select m).SingleOrDefault();

                dbcontext.UserFeedbackVoters.Remove(userFeedbackVotersEntity);
                dbcontext.SaveChanges();

            }

        }



        /// <summary>
        /// Fetching Comments for a particular User Feedback
        /// </summary>
        /// <param name="UserFeedbackId">It is Unique id of User Feedback</param>
        /// <returns>It returns list of Comments for a particular User Feedback</returns>
        public List<UserFeedbackModel.UserFeedbackComments> GetUserFeedbackComment(int UserFeedbackId)
        {

            dbcontext = new AppContext();

            List<UserFeedbackModel.UserFeedbackComments> commentObject;
            using (var con = new SqlConnection(ConnectionString))
            {
                commentObject = con.Query<UserFeedbackModel.UserFeedbackComments>("Usp_GetUserFeedbackComment", new 
                { UserFeedbackId = UserFeedbackId }, commandType: CommandType.StoredProcedure).ToList();
           

            }
            //List<UserFeedbackModel.UserFeedbackComments> commentObject = (from m in dbcontext.UserFeedbackComments
            //                                                              join m1 in dbcontext.UserDetails on m.CreatedBy equals m1.UserId
            //                                                              where m.UserFeedbackId == UserFeedbackId && m1.IsDeleted == false
            //                                                              orderby m.CreatedDate
            //                                                              select new UserFeedbackModel.UserFeedbackComments
            //                                                              {
            //                                                                  Comment = m.Comment,
            //                                                                  CommentId = m.Id,
            //                                                                  CreatedDate = m.CreatedDate,
            //                                                                  CreatorName = m1.FirstName + " " + m1.LastName,
            //                                                                  CreatorProfilePhoto = m1.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m1.ProfilePhoto,
            //                                                                  CreatorUserId = m1.UserId
            //                                                              }

            //                                                                ).ToList();

            return commentObject;


        }



        /// <summary>
        /// This method saves the User Feedback Comment for a particualr User Feedback
        /// </summary>
        /// <param name="userId">It is the unique id of logged in User</param>
        /// <param name="model">This model consists all new values of fields to be saved</param>
        public void SaveUserFeedbackComment(string userId, UserFeedbackModel.UserFeedbackCommentAddModel model)
        {

            int isUpdate;

           
                using (var con = new SqlConnection(ConnectionString))
                {

                    isUpdate = con.Query<int>("USp_SaveUserFeedbackComment", new
                    {
                        UserFeedbackId = model.UserFeedbackId,
                        Comment = model.Comment,
                 
                        CreatedBy = userId

                    },
                    commandType: System.Data.CommandType.StoredProcedure).SingleOrDefault();



                }









                //dbcontext = new AppContext();

                //UserFeedbackComments userFeedbackCommentEntity = new UserFeedbackComments
                //{
                //    UserFeedbackId = model.UserFeedbackId,
                //    Comment = model.Comment,
                //    CreatedDate = DateTime.Now,
                //    CreatedBy = userId
                //};


                //dbcontext.UserFeedbackComments.Add(userFeedbackCommentEntity);
                //dbcontext.SaveChanges();

            }




    }
}
