﻿using System;
using System.Collections.Generic;
using System.Linq;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;

namespace TIGHTEN.DATA
{
    public class UserTag : Common
    {
        public object GetUsersTagged(int CommentId)
        {

            return (from m in dbcontext.UserTags where m.CommentId == CommentId select m).ToList();
        }


        public void SaveUsersTagged(int commentId, string userId)
        {

            TIGHTEN.ENTITY.UserTag usertag = new TIGHTEN.ENTITY.UserTag
            {
                UserId = userId,
                CommentId = commentId
            };
            dbcontext.UserTags.Add(usertag);
            dbcontext.SaveChanges();
        }

    }
}
