﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class TodoManual : BaseClass
    {
        AppContext dbcontext;

        /// <summary>
        /// This function fetches my Todos for MyTOdos Tab
        /// </summary>
        /// <param name="UserId">It is unique id of logged in user</param>
        /// <returns>returns todos in list</returns>
        public List<TodoManualModel.TodoAddModel> getMyTodos(string UserId, int RoleId)
        {
            //List<TodoManualModel.TodoAddModel> TodosList = (from t0 in dbcontext.Teams
            //                                                join t3 in dbcontext.TeamUsers on t0.Id equals t3.TeamId
            //                                                join t in dbcontext.Projects on t0.Id equals t.TeamId
            //                                                join t4 in dbcontext.ProjectUsers on t.Id equals t4.ProjectId
            //                                                join t1 in dbcontext.Sections on t.Id equals t1.ProjectId
            //                                                join c in dbcontext.ToDos on t1.Id equals c.SectionId
            //                                                join c3 in dbcontext.UserDetails on c.CreatedBy equals c3.UserId
            //                                                join c1 in dbcontext.UserDetails on c.AssigneeId equals c1.UserId
            //                                                //join ucr in dbcontext.UserCompanyRoles on c1.CompanyId equals ucr.CompanyId
            //                                                //join r in dbcontext.Roles on ucr.RoleId equals r.RoleId
            //                                                into b

            //                                                where t3.UserId == UserId && t4.UserId == UserId && c.TaskType == 5 && t0.IsDeleted == false
            //                                                && c3.IsDeleted == false && t.IsDeleted == false /* && c1.IsDeleted == false*/
            //                                                && (RoleId != 2 ? c.AssigneeId == UserId : c.AssigneeId == c.AssigneeId)
            //                                                && c.IsDeleted == false
            //                                                orderby c.ModifiedDate descending

            //                                                from c2 in b.DefaultIfEmpty()
            //                                                let AssigneeProfilePhoto = c2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c2.ProfilePhoto

            //                                                let ReporterProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
            //                                                let AssigneeName = c2.FirstName
            //                                                let ReporterName = c3.FirstName
            //                                                select new TodoManualModel.TodoAddModel
            //                                                {
            //                                                    Name = c.Name,
            //                                                    SectionId = c.SectionId,
            //                                                    TodoId = c.Id,
            //                                                    AssigneeId = c.AssigneeId,

            //                                                    ReporterId = c3.UserId,
            //                                                    ReporterEmail = c3.Email,
            //                                                    ReporterName = ReporterName,
            //                                                    //ReporterProfilePhoto = ReporterProfilePhoto,
            //                                                    Description = c.Description,
            //                                                    DeadlineDate = c.DeadlineDate,
            //                                                    InProgress = c.InProgress,
            //                                                    IsDone = c.IsDone,
            //                                                    EstimatedTime = c.EstimatedTime,
            //                                                    LoggedTime=c.LoggedTime,

            //                                                }).ToList();


            List<TodoManualModel.TodoAddModel> TodosList;

            using (var con = new SqlConnection(ConnectionString))
            {
                TodosList = con.Query<TodoManualModel.TodoAddModel>("usp_getMyTodos", new { UserId = UserId, RoleId = RoleId }, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }



            return TodosList;
        }

        /*  This function is used for saving the new Todo*/
        public int saveToDo(TodoManualModel.TodoAddModel model)
        {
            UserDetail userDetail;
            UserDetail parentDetail = null;
            string parentEmailId = "";

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_userDetail = @"select * from UserDetails Where UserId = @UserId and IsDeleted = 0";
                userDetail = con.Query<UserDetail>(sqlquery_userDetail, new { UserId = model.UserId }).SingleOrDefault();
                
                if (userDetail != null)
                {
                    if (userDetail.ParentUserId == null)
                    {
                        parentEmailId = userDetail.Email;
                    }
                    else {
                        //parentDetail = dbcontext.UserDetails.Where(x => x.UserId == userDetail.ParentUserId && x.IsDeleted == false).SingleOrDefault();
                        string sqlquery_parentDetail = @"select * from UserDetails Where UserId = @UserId and IsDeleted = 0";
                        parentDetail = con.Query<UserDetail>(sqlquery_parentDetail, new { UserId = userDetail.ParentUserId }).SingleOrDefault();

                        parentEmailId = parentDetail.Email;
                    }
                }
            }

            //// Calculating the time to insert into the todo and log table
            //TimeSpan interval = model.StopDate.Value.Subtract(model.StartDate.Value);
            //int TotalMinutes = interval.Days * 24 * 60 + interval.Hours * 60 + interval.Minutes;
            //string minuteString = Utility.ConvertMinutesToFormatedTimeLogString(TotalMinutes);
            int TotalMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(model.LoggedTime);

            //ToDo todoEntity = new ToDo
            //{
            //    Name = model.Name,
            //    CreatedDate = DateTime.Now,
            //    CreatedBy = parentDetail == null ? model.UserId : parentDetail.UserId,
            //    ModifiedBy = parentDetail == null ? model.UserId : parentDetail.UserId,
            //    ModifiedDate = DateTime.Now,
            //    SectionId = 0,// Convert.ToInt32(model.SectionId),
            //    Description = model.Description,
            //    AssigneeId = model.UserId,
            //    DeadlineDate = model.DeadlineDate,
            //    EstimatedTime = model.EstimatedTime,
            //    //ApprovedDate=DateTime.Now.Date
            //    TaskType = model.TaskType,
            //    InProgress = false,
            //    IsDone = true,
            //    LoggedTime = model.LoggedTime
            //};
            //dbcontext.ToDos.Add(todoEntity);
            //dbcontext.SaveChanges();

            //TodoTimeLogs timeLog = new TodoTimeLogs
            //{
            //    TodoId = todoEntity.Id,
            //    StartDateTime = model.StartDate.Value,
            //    StopDateTime = model.StopDate.Value,
            //    Duration = TotalMinutes,
            //    IsAutomaticLogged = false,
            //    loggingTime = DateTime.Now,
            //    StartBy = model.UserId,
            //    StopBy = model.UserId
            //};
            //dbcontext.TodoTimeLogs.Add(timeLog);
            //dbcontext.SaveChanges();

            //Follower follower = new Follower
            //{
            //    TodoId = todoEntity.Id,
            //    CreatedDate = DateTime.Now,
            //    CreatedBy = model.UserId,
            //    UserId = model.UserId
            //};
            ///* Save Follower */
            //dbcontext.Followers.Add(follower);
            //dbcontext.SaveChanges();

            //TodoNotifications objtodoNotifications = new TodoNotifications
            //{
            //    TodoId = todoEntity.Id,
            //    Description = "has created this Todo",
            //    CreatedBy = model.UserId,
            //    CreatedDate = DateTime.Now,
            //    Type = 1
            //};
            ///* Save TodoNotification */
            //dbcontext.TodoNotifications.Add(objtodoNotifications);
            //dbcontext.SaveChanges();

            //if (model.UserId != null)
            //{
            //    /* Saving notification for newly added Todo for assignee */
            //    Notification objNotification = new Notification
            //    {
            //        Name = "has Assigned you a Todo:",
            //        CreatedBy = model.UserId,
            //        CreatedDate = DateTime.Now,
            //        IsRead = false,
            //        Type = 3,
            //        NotifyingUserId = model.UserId,
            //        TodoId = todoEntity.Id
            //    };
            //    dbcontext.Notifications.Add(objNotification);
            //    dbcontext.SaveChanges();
            //}

            int TodoId = 0;

            using (var con = new SqlConnection(ConnectionString))
            {
                TodoId = con.Query<int>("usp_InsertMannualTime", new { Name = model.Name, CreatedBy = parentDetail == null ? model.UserId : parentDetail.UserId,
                    ModifiedBy = parentDetail == null ? model.UserId : parentDetail.UserId, Description = model.Description, UserId = model.UserId, DeadlineDate = model.DeadlineDate,
                    EstimatedTime = model.EstimatedTime, TaskType = model.TaskType, LoggedTime = model.LoggedTime, IsUserIdNull = model.UserId == null ? true : false,
                    StartDate = model.StartDate.Value, StopDate = model.StopDate.Value, TotalMinutes = TotalMinutes
                }, commandType: System.Data.CommandType.StoredProcedure).Single();
            }



            return TodoId;
        }


        /*  This function is used for saving the new Todo*/
        public string UpdateToDo(TodoManualModel.TodoAddModel model)
        {
            //ToDo todo = dbcontext.ToDos.Where(x => x.Id == model.TodoId && x.IsDeleted == false).SingleOrDefault();

            ToDo todo;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_Todo = @"select * from ToDoes Where Id = @TodoId and IsDeleted = 0  ";
                todo = con.Query<ToDo>(sqlquery_Todo,new { TodoId = model.TodoId }).SingleOrDefault();
            }

            if (todo.InProgress == false)
            {
                if (todo != null)
                {
                    
                    // Calculating the time to insert into the todo and log table
                    //TimeSpan interval = model.StopDate.Value.Subtract(model.StartDate.Value);
                    //int TotalMinutes = interval.Days * 24 * 60 + interval.Hours * 60 + interval.Minutes;
                    //string minuteString = Utility.ConvertMinutesToFormatedTimeLogString(TotalMinutes);
                    int TotalMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(model.LoggedTime);

                    //// insert new entry in the TodoTimeLogs table for hours used
                    //TodoTimeLogs timelog = new TodoTimeLogs
                    //{
                    //    TodoId = model.TodoId,
                    //    IsAutomaticLogged = false,
                    //    loggingTime = DateTime.Now,
                    //    Duration = TotalMinutes,
                    //    StartDateTime = model.StartDate.HasValue ? model.StartDate.Value : DateTime.Now.Date,
                    //    StopDateTime = model.StopDate.HasValue ? model.StopDate.Value : DateTime.Now.Date,
                    //    StartBy = model.UserId,
                    //    StopBy = model.UserId,
                    //    IsInvoiceGenerated = false
                    //};

                    //dbcontext.TodoTimeLogs.Add(timelog);
                    //dbcontext.SaveChanges();

                    ////updating logged ours in todo table
                    //int sumOfTime = (from t in dbcontext.TodoTimeLogs where t.TodoId == model.TodoId && t.StartBy == model.UserId select t.Duration).Sum();
                    //// assigning the converted time string
                    //todo.LoggedTime = Utility.ConvertMinutesToFormatedTimeLogString(sumOfTime);
                    //todo.IsAvailableForInvoice = true;
                    ////if task is done by user
                    //if (model.IsDone)
                    //{
                    //    todo.InProgress = false;
                    //}
                    ////todo.LoggedTime = minuteString;
                    //todo.ModifiedDate = DateTime.Now.Date;
                    //dbcontext.SaveChanges();


                    ///* Saving Notification for Todo */
                    //TodoNotifications objtodoNotifications = new TodoNotifications
                    //{
                    //    TodoId = model.TodoId,
                    //    Description = "has updated the Todo",
                    //    CreatedBy = model.UserId,
                    //    CreatedDate = DateTime.Now,
                    //    Type = 1
                    //};

                    ///* Save TodoNotification */
                    //dbcontext.TodoNotifications.Add(objtodoNotifications);
                    //dbcontext.SaveChanges();

              

                    int sumOfTime;
                    using (var con = new SqlConnection(ConnectionString))
                    {


                        con.Query("USp_InsertTodoTimeLogs", new
                        {

                            TodoId = model.TodoId,
                            Duration = TotalMinutes,
                            StartDateTime = model.StartDate.HasValue ? model.StartDate.Value : DateTime.Now.Date,
                            StopDateTime = model.StopDate.HasValue ? model.StopDate.Value : DateTime.Now.Date,
                            StartBy = model.UserId,
                            StopBy = model.UserId,
                  
                        },commandType:System.Data.CommandType.StoredProcedure);




                        string sqlquery_sumOfTime = @"select SUM(Duration) from TodoTimeLogs where TodoId = @TodoId and StartBy = @UserId";
                        sumOfTime = con.Query<int>(sqlquery_sumOfTime, new { TodoId = model.TodoId, UserId = model.UserId }).Single();
                        string LoggedTime = Utility.ConvertMinutesToFormatedTimeLogString(sumOfTime);

                        con.Execute("usp_InsertMannualTimeForPmsTasks", 
                            new { TodoId= model.TodoId, TotalMinutes= TotalMinutes, StartDate= model.StartDate.HasValue ? model.StartDate.Value : DateTime.Now.Date, StopDate= model.StopDate.HasValue ? model.StopDate.Value : DateTime.Now.Date, UserId = model.UserId, LoggedTime= LoggedTime, IsDone= model.IsDone },
                            commandType:System.Data.CommandType.StoredProcedure);

                    }



                    return model.TodoId.ToString();
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "Todo is still in Progress";
            }

        }
        //public int UpdateToDo(TodoManualModel.TodoAddModel model)
        //{

        //}
    }
}
