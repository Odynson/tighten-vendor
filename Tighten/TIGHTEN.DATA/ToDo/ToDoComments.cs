﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class ToDoComments : BaseClass
    {
        AppContext dbcontext;

        public object getToDoComments(int toDoId, string UserId)
        {

        //    var CommentsResult = (from t1 in dbcontext.ToDoComments
        //                          join t2 in dbcontext.UserDetails on t1.CreatedBy equals t2.UserId
        //                          where t1.ToDoId == toDoId && t2.IsDeleted == false
        //                          orderby t1.CreatedDate ascending
        //                          let CreatorProfilePhoto = (t2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + t2.ProfilePhoto)
        //                          let CreatedBy = (t2.FirstName + " " + t2.LastName)
        //                          let IsOwner = t1.CreatedBy == UserId ? true : false
        //                          let Id = t1.CreatedBy == UserId ? t1.Id : 0

        //                          select new
        //                          {
        //                              Id = Id,
        //                              Description = t1.Description,
        //                              CreatedBy = CreatedBy,
        //                              CreatedDate = t1.CreatedDate,
        //                              IsOwner = IsOwner,
        //                              CreatorProfilePhoto = CreatorProfilePhoto,
        //                              FilesTagged = from c in dbcontext.FileTags
        //                                            where t1.Id == c.CommentId
        //                                            select new TodoModel.FileTags
        //                                            {
        //                                                FileName = c.FileName,
        //                                                FilePath = c.FilePath,
        //                                                OriginalName = c.OriginalName,
        //                                                IsGif = c.IsGif
        //                                            },
        //                              UsersTagged = from c in dbcontext.UserTags
        //                                            join c1 in dbcontext.UserDetails on c.UserId equals c1.UserId
        //                                            where t1.Id == c.CommentId && c1.IsDeleted == false
        //                                            let Fullname = c1.FirstName + " " + (string.IsNullOrEmpty(c1.LastName) ? "" : c1.LastName)
        //                                            select new TodoModel.UsersTags
        //                                            {
        //                                                UserId = c.UserId,
        //                                                FullName = Fullname
        //                                            }

        //});
        //    List<TodoModel.Comment> Comments = CommentsResult.AsEnumerable().Select(item =>
        //                                                                        new TodoModel.Comment
        //                                                                        {
        //                                                                            Id = item.Id,
        //                                                                            Description = item.Description,
        //                                                                            CreatedBy = item.CreatedBy,
        //                                                                            CreatedDate = item.CreatedDate,
        //                                                                            IsOwner = item.IsOwner,
        //                                                                            CreatorProfilePhoto = item.CreatorProfilePhoto,
        //                                                                            FilesTagged = item.FilesTagged.ToList(),
        //                                                                            UsersTagged = item.UsersTagged.ToList()
        //                                                                        }).ToList();

            List<TodoModel.Comment> Comments;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetToDoComments", new { toDoId = toDoId, UserId = UserId }, commandType: CommandType.StoredProcedure);
                Comments = result.Read<TodoModel.Comment>().ToList();
                if (Comments != null)
                {
                    var FileTag = result.Read<TodoModel.FileTags>().ToList();
                    var UsersTag = result.Read<TodoModel.UsersTags>().ToList();

                    foreach (var item in Comments)
                    {
                        item.FilesTagged = FileTag.Where(x => x.CommentId == item.CommentId).ToList();
                        item.UsersTagged = UsersTag.Where(x => x.CommentId == item.CommentId).ToList();
                    }
                }
            }

            return Comments;
        }

        public void saveToDoComment(TodoModel.ToDoComment model)
        {
            //string[] TodoFollowers = (from m in dbcontext.Projects
            //                          join m1 in dbcontext.Sections on m.Id equals m1.ProjectId
            //                          join m2 in dbcontext.ToDos on m1.Id equals m2.SectionId
            //                          join m3 in dbcontext.Followers on m2.Id equals m3.TodoId
            //                          where m2.Id == model.ToDoId && m.IsDeleted == false
            //                          && m2.IsDeleted == false
            //                          select m3.UserId).ToArray();

            //for (int j = 0; j < TodoFollowers.Count(); j++)
            //{
            //    /* Saving notification for newly added Team for team members */
            //    Notification objNotification = new Notification
            //    {
            //        Name = "has commented on a Todo:",
            //        CreatedBy = model.CreatedBy,
            //        CreatedDate = DateTime.Now,
            //        IsRead = false,
            //        Type = 3,
            //        NotifyingUserId = TodoFollowers[j],
            //        TodoId = model.ToDoId
            //    };

            //    dbcontext.Notifications.Add(objNotification);
            //    dbcontext.SaveChanges();

            //}

            //ToDoComment todocomment = new ToDoComment
            //{
            //    Description = model.Description,
            //    ToDoId = model.ToDoId,
            //    CreatedDate = DateTime.Now,
            //    CreatedBy = model.CreatedBy
            //};

            ///* Save ToDo Comment */
            //dbcontext.ToDoComments.Add(todocomment);
            //dbcontext.SaveChanges();


            //int CommentId = todocomment.Id;
            //string[] Users = model.UserTag;
            //string[] Files = model.FileTag;
            //string[] OriginalFileNames = model.OriginalFileName;
            //string[] FilePaths = model.FilePath;
            //bool[] IsGif = model.IsGif;
            //UserTag objUserTag = new UserTag();
            //FileTag objFileTag = new FileTag();
            //if (Users.Length > 0)
            //{
            //    for (int i = 0; i < Users.Length; i++)
            //    {
            //        objUserTag.SaveUsersTagged(CommentId, Users[i]);

            //        /* Saving notification for newly added Team for team members */
            //        Notification objNotification = new Notification
            //        {

            //            Name = "has tagged you in a Comment",
            //            CreatedBy = model.CreatedBy,
            //            CreatedDate = DateTime.Now,
            //            IsRead = false,
            //            Type = 5,
            //            NotifyingUserId = Users[i],
            //            CommentId = todocomment.Id
            //        };

            //        dbcontext.Notifications.Add(objNotification);
            //        dbcontext.SaveChanges();

            //    }
            //}
            //if (Files.Length > 0)
            //{
            //    for (int i = 0; i < Files.Length; i++)
            //    {
            //        objFileTag.SaveFilesTagged(CommentId, Files[i], OriginalFileNames[i], FilePaths[i], IsGif[i]);
            //    }
            //}


            ///* Saving Notification for Todo */
            //TodoNotifications objtodoNotifications = new TodoNotifications
            //{
            //    TodoId = model.ToDoId,
            //    Description = "has added a new Comment:",
            //    CreatedBy = model.CreatedBy,
            //    CreatedDate = DateTime.Now,
            //    Type = 4,
            //    AttachmentId = todocomment.Id
            //};

            ///* Save TodoNotification for Todo */
            //dbcontext.TodoNotifications.Add(objtodoNotifications);
            //dbcontext.SaveChanges();

            string[] Users = model.UserTag;
            string[] Files = model.FileTag;
            string[] OriginalFileNames = model.OriginalFileName;
            string[] FilePaths = model.FilePath;
            bool[] IsGif = model.IsGif;

            string AllUserIds = "0";
            if (Users.Length > 0)
            {
                AllUserIds = string.Join(",", Users.Select(item => "'" + item + "'"));
            }
            

            int CommentId = 0;
            using (var con = new SqlConnection(ConnectionString))
            {
                CommentId = con.Query<int>("usp_ToDoCommentInsert", new { CreatedBy= model.CreatedBy, ToDoId = model.ToDoId, Description = model.Description, AllUserIds = AllUserIds }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (Files.Length > 0 && CommentId > 0)
                {
                    for (int i = 0; i < Files.Length; i++)
                    {
                        string query = @"insert into FileTags(FileName,CommentId,OriginalName,FilePath,IsGif ) values('"+ Files[i]+"' ,"+ CommentId+" ,'"+ OriginalFileNames[i] + "' ,'"+ FilePaths[i]+"' ,"+ 0 + ")";
                        con.Execute(query);
                    }
                }
            }

            

        }

        public void deleteToDoComment(int Id)
        {
            ToDoComment todocomment = (from e in dbcontext.ToDoComments where e.Id == Id select e)
                                     .SingleOrDefault();

            /* Delete To Do Comment */
            dbcontext.ToDoComments.Remove(todocomment);

            dbcontext.SaveChanges();



        }

        public void deleteAllToDoComments(int Id)
        {

            //var todocomments = dbcontext.ToDoComments.Where(e => e.ToDoId == Id);

            //foreach (ToDoComment item in todocomments)
            //{
            //    dbcontext.ToDoComments.Remove(item);
            //}

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"delete from ToDoComments Where ToDoId = @Id";
                con.Execute(sqlquery, new { Id = Id });
            }

                //dbcontext.SaveChanges();

        }
    }
}
