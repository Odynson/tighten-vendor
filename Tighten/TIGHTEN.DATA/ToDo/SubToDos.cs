﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;

namespace TIGHTEN.DATA
{
    public class SubToDos : Common
    {

        public object getSubTodos(string userId, int ToDoId)
        {

            var result = (from p in dbcontext.SubToDos
                          join p1 in dbcontext.UserDetails on p.CreatedBy equals p1.UserId
                          where p.ToDoId == ToDoId && p1.IsDeleted == false
                          orderby p.CreatedDate ascending
                          let Display = (p.CreatedBy == userId ? true : false)
                          let Id = (p.CreatedBy == userId ? p.Id : 0)
                          select new { Id, p.Name, p1.FirstName, p1.LastName, Display, p.IsDone }).ToList();
            return result;
        }

        public void saveSubTodo(string userId, TodoModel.SubToDo model)
        {

            SubToDo subToDo = new SubToDo
            {
                Name = model.Name,
                ToDoId = model.ToDoId,
                CreatedDate = DateTime.Now.ToUniversalTime(),
                CreatedBy = userId,
                ModifiedBy = userId,
                ModifiedDate = DateTime.Now.ToUniversalTime()
            };

            /* Save SubToDo */
            dbcontext.SubToDos.Add(subToDo);
            dbcontext.SaveChanges();


        }

        public void updateSubTodo(TodoModel.SubToDo model)
        {

            SubToDo subToDo = (from m in dbcontext.SubToDos where m.Id == model.Id select m).SingleOrDefault();

            subToDo.IsDone = model.IsDone;

            /* Update SubToDo */

            dbcontext.SaveChanges();


        }

        public void deleteSubTodo(int Id)
        {
            SubToDo subtodo = (from e in dbcontext.SubToDos where e.Id == Id select e)
                                     .SingleOrDefault();

            /* Delete To Do Comment */
            dbcontext.SubToDos.Remove(subtodo);

            dbcontext.SaveChanges();
        }



    }
}
