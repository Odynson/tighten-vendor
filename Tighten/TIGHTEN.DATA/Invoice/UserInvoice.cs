﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using Dapper;
using System.Web;
using System.Threading;
using System.Xml.Linq;
using System.Data;
using TIGHTEN.MODEL;

namespace TIGHTEN.DATA
{
    public class UserInvoice : BaseClass
    {
        AppContext dbcontext;

        public MODEL.UserInvoice.InvoiceModelBothApprovedandNotApproved GetInvoice(MODEL.UserInvoice.InvoiceQuery model)
        {

            model.FromDate = (model.FromDate == DateTime.MinValue ? DateTime.Now.AddDays(-7) : model.FromDate);
            model.ToDate = (model.ToDate == DateTime.MinValue ? DateTime.Now : model.ToDate);

            MODEL.UserInvoice.InvoiceModelBothApprovedandNotApproved ReturnModel = new MODEL.UserInvoice.InvoiceModelBothApprovedandNotApproved();

            

            #region New Code With Dapper
                List<MODEL.UserInvoice.InvoiceModel> invoiceList;
                using (var con = new SqlConnection(ConnectionString))
                {
                    var result = con.QueryMultiple("usp_GetAllInvoiceForFreelancer", new { InvoiceType = model.InvoiceType, UserId = model.UserId, CompanyId = model.CompanyId, FromDate = model.FromDate, ToDate = model.ToDate, InvoiceStatus = model.InvoiceStatus }, commandType: System.Data.CommandType.StoredProcedure);

                    invoiceList = result.Read<MODEL.UserInvoice.InvoiceModel>().ToList();

                    if (invoiceList != null)
                    {
                        var users = result.Read<MODEL.UserInvoice.UserDetailForInvoicePaidAndReject>().ToList();

                        foreach (var item in invoiceList)
                        {
                            item.PaidByUserDetail = users.Where(x => x.UserId == item.PaidBy).FirstOrDefault();
                            item.RejectedByUserDetail = users.Where(x => x.UserId == item.RejectedBy).FirstOrDefault();
                        }
                    }

                }


                // This code needs to be changed to dapper later on and SHOULD BE ADDED TO invoiceList
                #region Changable Code

                //    List<MODEL.UserInvoice.InvoiceModel> invoiceListForSentForwardedInvoice = new List<MODEL.UserInvoice.InvoiceModel>();

                //if (model.InvoiceType == 1)
                //{
                //    invoiceListForSentForwardedInvoice =

                //    (from allinvoices in dbcontext.Invoices
                //     join invforward in dbcontext.InvoiceForwards on allinvoices.Id equals invforward.InvoiceId
                //     join User_Detail in dbcontext.UserDetails on invforward.UserId equals User_Detail.UserId
                //     where invforward.UserId == model.UserId && User_Detail.IsDeleted == false
                //     //(model.InvoiceType == 0 ? invforward.ForwardTo == model.UserId : model.InvoiceType == 1 ? invforward.UserId == model.UserId : invforward.ForwardTo == model.UserId)
                //     && (model.CompanyId == 0 ? invforward.CompanyId == invforward.CompanyId : invforward.CompanyId == model.CompanyId)
                //     && DbFunctions.TruncateTime(invforward.ForwardDate) >= model.FromDate
                //     && DbFunctions.TruncateTime(invforward.ForwardDate) <= model.ToDate
                //     && allinvoices.InvoiceApprovedStatus == (model.InvoiceStatus == 1 ? true : model.InvoiceStatus == 2 ? false : allinvoices.InvoiceApprovedStatus)
                //     //orderby allinvoices.InvoiceDate
                //     select new MODEL.UserInvoice.InvoiceModel
                //     {
                //         Id = invforward.Id,
                //         CompanyId = invforward.CompanyId,
                //         InvoiceNumber = allinvoices.InvoiceNumber,
                //         UserId = allinvoices.UserId,
                //         FromDate = allinvoices.FromDate,
                //         ToDate = allinvoices.ToDate,
                //         SendTo = allinvoices.SendTo,
                //         Message = allinvoices.Message,
                //         InvoiceDate = allinvoices.InvoiceDate,
                //         TotalHours = allinvoices.TotalHours,
                //         TotalAmount = allinvoices.TotalAmount,
                //         InvoiceApprovedStatus = allinvoices.InvoiceApprovedStatus,
                //         UserName = User_Detail.FirstName,
                //         ProfilePic = User_Detail.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + User_Detail.ProfilePhoto,
                //         Rate = User_Detail.Rate,
                //         IsInvoiceForward = true,
                //         IsActiveInvoiceForward = invforward.IsCurrentlyActive,
                //         IsInvoiceForwardForDetails = true,
                //         IsPaid = allinvoices.IsPaid,
                //         PaidBy = allinvoices.PaidBy,
                //         IsRejected = allinvoices.IsRejected,
                //         RejectedBy = allinvoices.RejectedBy,
                //         RejectReason = allinvoices.RejectReason,
                //         PaidByUserDetail = (from u in dbcontext.UserDetails
                //                             where u.UserId == allinvoices.PaidBy
                //                             select new MODEL.UserInvoice.UserDetailForInvoicePaidAndReject
                //                             {
                //                                 UserId = u.UserId,
                //                                 UserName = u.FirstName,
                //                                 UserProfilePic = u.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + u.ProfilePhoto
                //                             }).FirstOrDefault(),

                //         RejectedByUserDetail = (from u in dbcontext.UserDetails
                //                                 where u.UserId == allinvoices.RejectedBy
                //                                 select new MODEL.UserInvoice.UserDetailForInvoicePaidAndReject
                //                                 {
                //                                     UserId = u.UserId,
                //                                     UserName = u.FirstName,
                //                                     UserProfilePic = u.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + u.ProfilePhoto
                //                                 }).FirstOrDefault()
                //     }).ToList();


                //    foreach (var item in invoiceListForSentForwardedInvoice)
                //    {
                //        invoiceList.Add(item);
                //    }

                //}



                #endregion

                ReturnModel.list_Paid = invoiceList.Where(x => x.IsPaid == true).ToList();
                ReturnModel.list_NotPaid = invoiceList.Where(x => x.IsPaid == false && x.IsRejected == false).ToList();
                ReturnModel.list_Rejected = invoiceList.Where(x => x.IsRejected == true).ToList();

            #endregion
            return ReturnModel;
        }


        public MODEL.ReportersForTodoesModel.TodosForPdf_List GetInvoiceDetail(MODEL.UserInvoice.AdminInvoiceDetailsRequiredParams model, string DefaultInvoiceNumber)
        {
            MODEL.ReportersForTodoesModel.TodosForPdf_List ReturnModel = new MODEL.ReportersForTodoesModel.TodosForPdf_List();

            string FromName = string.Empty;
            string ToName = string.Empty;
            string Message = string.Empty;
            DateTime DateIssued = DateTime.Now;
            string InvoiceNumber = string.Empty;
            int InvoiceId = model.InvoiceId;
            string RejectReason = string.Empty;
            bool IsRejected = false;

            if (model.IsInvoiceForwardForDetails == true)
            {
                //var invforward = (from invfrwrd in dbcontext.InvoiceForwards
                //                  join inv in dbcontext.Invoices on invfrwrd.InvoiceId equals inv.Id
                //                  where invfrwrd.Id == model.InvoiceId
                //                  select new { invfrwrd, inv }).FirstOrDefault();


                //var fromName = (from ussr in dbcontext.UserDetails
                //                where ussr.UserId == invforward.invfrwrd.UserId && ussr.IsDeleted == false
                //                select ussr.FirstName).SingleOrDefault();

                //var toName = (from ussr in dbcontext.UserDetails
                //              where ussr.UserId == invforward.invfrwrd.ForwardTo && ussr.IsDeleted == false
                //              select ussr.FirstName).SingleOrDefault();

                InvoiceForward invforward;
                TIGHTEN.ENTITY.Invoice inv;
                string fromName;
                string toName;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery_invforward = @"select invfrwrd.* from InvoiceForwards invfrwrd inner join Invoices inv on invfrwrd.InvoiceId = inv.Id where invfrwrd.Id = @InvoiceId
                                                   select * from Invoices where Id = @InvoiceId";
                    var result = con.QueryMultiple(sqlquery_invforward, new { InvoiceId = model.InvoiceId });

                    invforward = result.Read<InvoiceForward>().FirstOrDefault();
                    inv = result.Read<TIGHTEN.ENTITY.Invoice>().FirstOrDefault();

                    string sqlquery_fromName = @"select ussr.FirstName from UserDetails ussr where ussr.UserId = @invforward_UserId and ussr.IsDeleted = 0  ";
                    fromName = con.Query<string>(sqlquery_fromName, new { invforward_UserId = invforward.UserId }).FirstOrDefault();

                    string sqlquery_toName = @"select * from UserDetails ussr where ussr.UserId = @invforward_ForwardTo and ussr.IsDeleted = 0 ";
                    toName = con.Query<string>(sqlquery_toName, new { invforward_ForwardTo = invforward.ForwardTo }).FirstOrDefault();

                }

                FromName = fromName;
                ToName = toName;
                DateIssued = invforward.ForwardDate;
                Message = inv.Message;
                InvoiceNumber = inv.InvoiceNumber;
                RejectReason = inv.RejectReason;
                IsRejected = inv.IsRejected;
                InvoiceId = inv.Id;
            }
            else
            {
                //var invoices = (from inv in dbcontext.Invoices
                //                where inv.Id == model.InvoiceId
                //                select inv).SingleOrDefault();

                //var fromName = (from ussr in dbcontext.UserDetails
                //                where ussr.UserId == invoices.UserId && ussr.IsDeleted == false
                //                select ussr.FirstName).SingleOrDefault();

                //var toName = (from ussr in dbcontext.UserDetails
                //              where ussr.UserId == invoices.SendTo && ussr.IsDeleted == false
                //              select ussr.FirstName).SingleOrDefault();

                TIGHTEN.ENTITY.Invoice inv;
                string fromName;
                string toName;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"select * from Invoices inv where inv.Id = @InvoiceId";
                    inv = con.Query<TIGHTEN.ENTITY.Invoice>(sqlquery, new { InvoiceId = model.InvoiceId }).FirstOrDefault();

                    string sqlquery_fromname = @"select * from UserDetails ussr where ussr.UserId = @UserId and ussr.IsDeleted = 0";
                    fromName = con.Query<string>(sqlquery_fromname, new { UserId = inv.UserId }).FirstOrDefault();

                    string sqlquery_toname = @"select * from UserDetails ussr where ussr.UserId = @SendTo and ussr.IsDeleted = 0";
                    toName = con.Query<string>(sqlquery_toname, new { SendTo = inv.SendTo }).FirstOrDefault();
                }

                FromName = fromName;
                ToName = toName;
                DateIssued = inv.InvoiceDate;
                Message = inv.Message;
                InvoiceNumber = inv.InvoiceNumber;
                RejectReason = inv.RejectReason;
                IsRejected = inv.IsRejected;
            }



            //var CompanyProfilePic = (from cm in dbcontext.Companies
            //                         where cm.Id == model.CompanyId
            //                         select new
            //                         {
            //                             CompanyUserId = cm.Id,
            //                             conpanyprofilepic = cm.Logo == null ? "Uploads/Default/nologo.png" : "Uploads/CompanyLogos/" + cm.Logo,
            //                             CompanyName = cm.Name
            //                         }).SingleOrDefault();

            InvoiceDetailReturnModel obj;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select cm.Id as CompanyUserId,case when cm.Logo is null then 'Uploads/Default/nologo.png' else 'Uploads/CompanyLogos/'+ cm.Logo end as conpanyprofilepic,cm.Name as CompanyName 
	                                from Companies cm where cm.Id = @CompanyId ";

                obj = con.Query<InvoiceDetailReturnModel>(sqlquery, new { CompanyId = model.CompanyId }).SingleOrDefault();
            }


            ReturnModel.InvoiceNumber = InvoiceNumber;
            ReturnModel.RejectReason = RejectReason;
            ReturnModel.IsRejected = IsRejected;
            ReturnModel.FromName = FromName;
            ReturnModel.ToName = ToName;
            ReturnModel.CompanyProfilePic = obj.conpanyprofilepic;
            ReturnModel.CompanyUserId = obj.CompanyUserId.ToString();
            ReturnModel.CompanyName = obj.CompanyName;
            ReturnModel.DateIssued = DateIssued;
            ReturnModel.Message = Message;





            //List<MODEL.ReportersForTodoesModel.TodosForPreview_List> list = (from invcdetail in dbcontext.InvoiceDetails
            //                                                                 join todoss in dbcontext.ToDos on invcdetail.TodoId equals todoss.Id
            //                                                                 join u in dbcontext.UserDetails on todoss.AssigneeId equals u.UserId
            //                                                                 join s in dbcontext.Sections on todoss.SectionId equals s.Id
            //                                                                 join p in dbcontext.Projects on s.ProjectId equals p.Id
            //                                                                 where invcdetail.InvoiceId == InvoiceId && u.IsDeleted == false
            //                                                                 && p.IsDeleted == false && todoss.IsDeleted == false
            //                                                                 select new MODEL.ReportersForTodoesModel.TodosForPreview_List
            //                                                                 {
            //                                                                     ProjectName = p.Name,
            //                                                                     TodoName = invcdetail.TodoName,
            //                                                                     TodoDescription = invcdetail.TodoDescription,
            //                                                                     Rate = u.Rate,
            //                                                                     hours = invcdetail.hours,
            //                                                                     Amount = invcdetail.Amount,
            //                                                                     FromDate = invcdetail.FromDate,
            //                                                                     ToDate = invcdetail.ToDate,
            //                                                                     TodoType = invcdetail.TodoType,
            //                                                                     TaskType = (invcdetail.TodoType == 1 ? "Phone Call" : invcdetail.TodoType == 2 ? " Research & Development " : invcdetail.TodoType == 3 ? "Meeting" : invcdetail.TodoType == 4 ? " Admin Duties " : "PMS Task")


            //                                                                 }).ToList();

            List<MODEL.ReportersForTodoesModel.TodosForPreview_List> list;
            using (var con = new SqlConnection(ConnectionString))
            {
                list = con.Query<MODEL.ReportersForTodoesModel.TodosForPreview_List>("usp_GetInvoiceDetail", new { InvoiceId = model.InvoiceId }, commandType: CommandType.StoredProcedure).ToList();
            }

            // This need to be changed after some time ,  this is a temporary fix
            // Here we have to find some other method to find Total Hours 

            #region Changable Data

            int TotalMinutesForTodos = 0;
            decimal rate = 0;

            foreach (var item in list)
            {
                int LoggedTimeInMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(item.hours);

                TotalMinutesForTodos += LoggedTimeInMinutes;
                rate = item.Rate;
            }


            decimal totalamt = 0;
            int TotalHours = (TotalMinutesForTodos / 60);
            totalamt = (TotalHours * rate);

            int TotalMinutes = (TotalMinutesForTodos % 60);
            if (TotalMinutes > 0)
            {
                decimal totalAmtForMinutes = (Convert.ToDecimal(TotalMinutes / 60.00) * rate);
                totalamt = totalamt + totalAmtForMinutes;
            }

            ReturnModel.TotalHoursForAllTodos = TotalHours + "h " + TotalMinutes + "m";

            ReturnModel.TotalAmountForAllTodos = Math.Round(totalamt, 2);


            #endregion

            ReturnModel.AllTodos = list;


            return ReturnModel;

        }



        public List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> GetProjectMembersForUser(MODEL.UserInvoice.ProjectMembersToForwardInvoice_QueryParams model)
        {
            //List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> list = (from Membrs in (from inv in dbcontext.Invoices
            //                                                                               join invdtls in dbcontext.InvoiceDetails on inv.Id equals invdtls.InvoiceId
            //                                                                               join todo in dbcontext.ToDos on invdtls.TodoId equals todo.Id
            //                                                                               join sec in dbcontext.Sections on todo.SectionId equals sec.Id
            //                                                                               join prjct in dbcontext.Projects on sec.ProjectId equals prjct.Id
            //                                                                               join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId
            //                                                                               join usrdtls in dbcontext.UserDetails on prjctusr.UserId equals usrdtls.UserId
            //                                                                               join usrcmpnyroles in dbcontext.UserCompanyRoles on usrdtls.UserId equals usrcmpnyroles.UserId
            //                                                                               join roles in dbcontext.Roles on usrcmpnyroles.RoleId equals roles.RoleId
            //                                                                               where inv.Id == model.invoiceId && usrdtls.IsDeleted == false
            //                                                                               && usrdtls.UserId != model.UserId && prjct.IsDeleted == false
            //                                                                               && roles.RoleId == 1 && roles.IsDeleted == false
            //                                                                               && todo.IsDeleted == false
            //                                                                               select new { usrdtls }
            //                                                                                   )
            //                                                               group Membrs by new { Membrs.usrdtls.FirstName, Membrs.usrdtls.UserId } into PrjctMembrs
            //                                                               select new MODEL.UserInvoice.ProjectMembersToForwardInvoice
            //                                                               {
            //                                                                   NameWithEmail = PrjctMembrs.Key.FirstName,
            //                                                                   UserId = PrjctMembrs.Key.UserId
            //                                                               }).ToList();

            List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> list;
            using (var con = new SqlConnection(ConnectionString))
            {
                list = con.Query<MODEL.UserInvoice.ProjectMembersToForwardInvoice>("usp_GetProjectMembersForFreelancer", new { UserId = model.UserId, invoiceId = model.invoiceId }).ToList();
            }

            return list;
        }


        public string ForwardInvoice(MODEL.UserInvoice.ForwardInvoice model)
        {
            string ReturnMsg = string.Empty;

            if (model != null)
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    ReturnMsg = con.Query<string>("usp_ForwardInvoice", new { InvoiceId = model.InvoiceId, UserId = model.UserId, ForwardTo = model.ForwardTo, CompanyId = model.CompanyId }).Single();
                }

                if (ReturnMsg == "saved")
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForForwardInvoice(model);
                    }
                    ));
                    childref.Start();
                }

                //var DuplicateInvoiceForward = from dup in dbcontext.InvoiceForwards
                //                              where dup.InvoiceId == model.InvoiceId
                //                              && dup.UserId == model.UserId
                //                              && dup.ForwardTo == model.ForwardTo
                //                              && dup.CompanyId == model.CompanyId
                //                              select dup;

                //if (DuplicateInvoiceForward.Count() == 0)
                //{

                //    var OldInvoiceForwarded = (from invfrwrd in dbcontext.InvoiceForwards
                //                               where invfrwrd.InvoiceId == model.InvoiceId
                //                               && invfrwrd.CompanyId == model.CompanyId
                //                               select invfrwrd).SingleOrDefault();
                //    if (OldInvoiceForwarded != null)
                //    {
                //        OldInvoiceForwarded.IsCurrentlyActive = false;
                //        dbcontext.SaveChanges();
                //    }


                //    ENTITY.InvoiceForward FrwrdInv = new ENTITY.InvoiceForward()
                //    {
                //        InvoiceId = model.InvoiceId,
                //        UserId = model.UserId,
                //        CompanyId = model.CompanyId,
                //        ForwardTo = model.ForwardTo,
                //        ForwardDate = DateTime.Now,
                //        IsCurrentlyActive = true
                //    };

                //    dbcontext.InvoiceForwards.Add(FrwrdInv);
                //    dbcontext.SaveChanges();


                //    ReturnMsg = "saved";


            
            }
            return ReturnMsg;
        }


        private void ThreadForForwardInvoice(MODEL.UserInvoice.ForwardInvoice model)
        {
            //var ForwardTo = (from ForwardUser in dbcontext.UserDetails
            //                 where ForwardUser.UserId == model.ForwardTo
            //                 && ForwardUser.IsDeleted == false
            //                 select ForwardUser).FirstOrDefault();

            //var ForwardFrom = (from ForwardfromUser in dbcontext.UserDetails
            //                   where ForwardfromUser.UserId == model.UserId
            //                   && ForwardfromUser.IsDeleted == false
            //                   select ForwardfromUser).FirstOrDefault();

            UserDetail ForwardTo;
            UserDetail ForwardFrom;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_ForwardTo = @"select * from UserDetails ForwardUser where ForwardUser.UserId = @ForwardTo and ForwardUser.IsDeleted = 0";
                string sqlquery_ForwardFrom = @"select * from UserDetails ForwardfromUser where ForwardfromUser.UserId = @UserId and ForwardfromUser.IsDeleted = 0";

                ForwardTo = con.Query<UserDetail>(sqlquery_ForwardTo, new { ForwardTo = model.ForwardTo }).FirstOrDefault();
                ForwardFrom = con.Query<UserDetail>(sqlquery_ForwardFrom, new { UserId = model.UserId }).FirstOrDefault();

            }

            //String emailbody = "<p> Hello " + ForwardTo.FirstName + " <br/>";
            //emailbody += " Invoice is forwarded to you by "+ ForwardFrom.FirstName + " for approval :<br/>";

            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "ForwardInvoice")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var item in MailModule)
            {
                emailbody = item.mailbody;
                Subject = item.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverFirstName", ForwardTo.FirstName).Replace("@@ForwardFrom_FirstName", ForwardFrom.FirstName);


            /*Send Email to user and inform about Invoice Forwarded */
            EmailUtility.SendMailInThread(ForwardTo.Email, Subject, emailbody);

        }




    }
}
