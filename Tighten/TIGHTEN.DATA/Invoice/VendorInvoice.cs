﻿using Dapper;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using TIGHTEN.MODEL.Vendor;
using TIGHTEN.UTILITIES;
namespace TIGHTEN.DATA
{
    public class VendorInvoice : BaseClass
    {

        public List<InvoiceModel> GetAllInvoiceDetailList(InvoiceModel model)
        {
            List<InvoiceModel> objInvoiceModal;

            DateTime date1 = new DateTime(0001, 1, 1);
            int result = DateTime.Compare(date1, model.ToDate);
            int result1 = DateTime.Compare(date1, model.FromDate);

            if (result == 0)
            {
                model.ToDate = DateTime.Now;
            }

            if (result1 == 0)
            {
                model.FromDate = DateTime.Now.AddYears(-1);
            }


            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceModel>("Usp_GetAllInvoiceDetailList", new
                {
                    VendorId = model.VendorId,
                    CompanyId = model.CompanyId,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    PageIndex = model.PageIndex,
                    PageSizeSelected = model.PageSizeSelected


                }, commandType: CommandType.StoredProcedure).ToList();
            }
            return objInvoiceModal;

        }

        public InvoiceRaiseToCompanyModal GetPreviewInvoicePaidOutstandingReject(int Id, int VendorId)
        {
            InvoiceRaiseToCompanyModal ObjInvoiceModal;
            List<InvoiceForResourceModal> ObjInvoiceForResourceList;
            List<InvoiceToMilestoneModal> ObjInvoiceToMilestoneList;
            List<InvoiceExtraResourceModal> ObjInvoiceExtraResourceList;

            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("usp_GetInvoicePreviewVendorAndCompanyProfile", new { InvoiceId = Id }, commandType: CommandType.StoredProcedure);
                ObjInvoiceModal = Result.Read<InvoiceRaiseToCompanyModal>().FirstOrDefault();
                ObjInvoiceForResourceList = Result.Read<InvoiceForResourceModal>().ToList();
                ObjInvoiceToMilestoneList = Result.Read<InvoiceToMilestoneModal>().ToList();
                ObjInvoiceExtraResourceList = Result.Read<InvoiceExtraResourceModal>().ToList();
                ObjInvoiceModal.InvoiceForResourceList = ObjInvoiceForResourceList;
                ObjInvoiceModal.InvoiceToMilestoneList = ObjInvoiceToMilestoneList;
                ObjInvoiceModal.InvoiceToExtraResourceList = ObjInvoiceExtraResourceList;

            }
            return ObjInvoiceModal;

        }

        public int SaveInvoiceByVendor(InvoiceModel model)
        {

            int Id = 0;
            InvoiceMail ObjIvoiceMail;
            MailVendorAdminProfileModel ObjMailVendorAdminProfileList;

            StringBuilder MilestoneIdList = new StringBuilder();
            StringBuilder MilestoneHourList = new StringBuilder();
            StringBuilder ResourceIdList = new StringBuilder();
            StringBuilder ResourceHourList = new StringBuilder();
            StringBuilder WorkEntriesList = new StringBuilder();
            StringBuilder PhaseIds = new StringBuilder();
            decimal totalhrs = 0;
            for (int i = 0; i < model.WorkLogList.Count; i++)
            {
                WorkEntriesList.Append(model.WorkLogList[i].Id.ToString()+",");
               
                    PhaseIds.Append(model.WorkLogList[i].PhaseId.ToString() + ",");
                    MilestoneIdList.Append(model.WorkLogList[i].PhaseMilestoneId.ToString() + ",");
                    MilestoneHourList.Append(model.WorkLogList[i].TotalHrs.ToString() + ",");
                //totalhrs += Convert.ToDecimal(model.WorkLogList[i].TotalHrs);
            }

            //for (int i = 0; i < model.InvoiceToMilestoneList.Count; i++)
            //{
            //    MilestoneIdList.Append(model.InvoiceToMilestoneList[i].Id + ",");
            //    MilestoneHourList.Append(model.InvoiceToMilestoneList[i].Hours + ",");
            //}

            for (int i = 0; i < model.InvoiceForResourceList.Count; i++)
            {
                ResourceIdList.Append(model.InvoiceForResourceList[i].Id + ",");
                ResourceHourList.Append(model.InvoiceForResourceList[i].Hours + ",");
            }

            decimal TightenCommission = Convert.ToDecimal((model.TotalAmount * 5) / 100);
            model.InvoiceToExtraResourceList = new List<InvoiceExtraResourceModal>();
            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("Usp_SaveVedorInvoice", new
                {
                    InvoiceNumber = model.InvoiceNumber.ToString(),
                    UserId = model.UserId,
                    SendTo = model.SendTo,
                    CompanyId = model.CompanyId,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    TotalHours = model.TotalHours,
                    TotalAmount = model.TotalAmount,
                    Message = model.Message,
                    ProjectId = model.ProjectId,
                    VendorId = model.VendorId,
                   // PhaseId = model.PhaseId,
                    TightenCommission = TightenCommission,
                    ResourceIdList = ResourceIdList.ToString(),
                    ResourceHourList = ResourceHourList.ToString(),
                    MilestoneIdList = MilestoneIdList.ToString(),
                    MilestoneHourList = MilestoneHourList.ToString(),
                    WorkLogEntryList= WorkEntriesList.ToString(),
                    PhaseIdList = PhaseIds.ToString()
                    ,
                    InvoiceExtraResourceDetailList = model.InvoiceToExtraResourceList.AsTableValuedParameter("InvoiceExtraResourceDetail"
                   , new[] { "Id", "Amount", "Description" })

                }, commandType: CommandType.StoredProcedure);


                ObjIvoiceMail = Result.Read<InvoiceMail>().FirstOrDefault();
                ObjMailVendorAdminProfileList = Result.Read<MailVendorAdminProfileModel>().FirstOrDefault();
            }
            if (ObjIvoiceMail != null)
            {
                Id = 1;
                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForsendMailOnInvoiceRaiseByVendor(ObjIvoiceMail);

                }));
                childref.Start();
            }

            if (ObjMailVendorAdminProfileList != null)
            {
                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForsendMailToVendorAdminProfileRaiseInvoice(ObjMailVendorAdminProfileList, model.InvoiceNumber);
                }

                ));
                childref.Start();
            }

            return Id;
        }

        public void ThreadForsendMailOnInvoiceRaiseByVendor(InvoiceMail item)
        {
            String emailbody = null;
            string Subject = string.Empty;

            string subjectBysender = item.InvoiceNo + " | " + item.ProjectName + "|" + item.SenderName;



            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendInvoiceRaaiseMailByVendor")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", item.ReceiverName).Replace("@@SenderName", item.SenderName)
                .Replace("@@SenderCompanyName", item.SenderCompanyName).Replace("@@ProjectName", item.ProjectName)
                .Replace("@@subjectBysender", subjectBysender).Replace("@@InvoiceNo", item.InvoiceNo)
                .Replace("@@TotalAmount", item.TotalAmount).Replace("@@TotalHours", item.TotalHours);
            EmailUtility.SendMailInThread(item.Email, subjectBysender, emailbody);

        }

        public void ThreadForsendMailToVendorAdminProfileRaiseInvoice(MailVendorAdminProfileModel item, string InvoiceNumber)
        {
            String emailbody = null;
            string Subject = string.Empty;

            string subjectBysender = InvoiceNumber + " | " + item.ProjectName + " | " + item.SenderName;



            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "VendorAdminProfileProjectAwardedSendMailInvoiceRaise")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", item.ReceiverName).Replace("@@SenderName", item.SenderName)
             .Replace("@@ProjectName", item.ProjectName).Replace("@@Amount", item.Amount.ToString()).Replace("@@InvoiceNumber", InvoiceNumber);
            EmailUtility.SendMailInThread(item.Email, subjectBysender, emailbody);

        }

        public List<InvoiceModel> GetAllVendorInvoiceList(VendorInvoiceRequestFilterModel model)
        {
            List<InvoiceModel> objInvoiceModal;
            //DateTime date1 = new DateTime(0001, 1, 1);
            //int result = DateTime.Compare(date1, model.ToDate);
            // int result1 = DateTime.Compare(date1, model.FromDate);
            //if (result == 0)
            //{
            //    model.ToDate = DateTime.Now;
            //}

            //if (result1 == 0)
            //{
            //    model.FromDate = DateTime.Now.AddYears(-1);
            //}
            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceModel>("Usp_GetAllVendorInvoiceList", new
                {
                    CompanyId = model.CompanyId,
                    ToDate = model.ToDate,
                    FromDate = model.FromDate,
                    VendorCompanyId = model.VendorCompanyId,
                    PageIndex = model.PageIndex,
                    PageSizeSelected=model.PageSizeSelected

                }, commandType: CommandType.StoredProcedure).ToList();
            }
            return objInvoiceModal;
        }

        public InvoiceRaiseToProject GetPhaseAndResourceforProject(int ProjectId, int VendorId)
        {
            InvoiceRaiseToProject ObjInvoiceRaiseToProject = new InvoiceRaiseToProject();
            List<InvoiceToPhaseModal> ObjInvoiceToPhaseList;
            List<InvoiceRaiseToCompanyModal> ObjInvoiceRaiseToCompanyList;
            List<InvoiceForResourceModal> ObjInvoiceForResourceModalList;


            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select C.Id Id, C.Name Name, CONCAT(Ud.FirstName + ' ', Ud.LastName) CompanyUserName, Ud.UserId UserId 
                                ,'Uploads/Profile/Thumbnail/'+ Ud.ProfilePhoto CompanyProfilePic
                              
                                from Companies C
                                inner join UserDetails Ud on C.Id = Ud.CompanyId
                                inner join Projects P on C.Id = P.CompanyId where P.Id = @ProjectId

                                select Pph.Name Name,Pph.Id Id from Projects P Inner join ProjectPhases Pph on p.Id = Pph.ProjectId where Pph.ProjectId = @ProjectId

                                select PPR.Id,CONCAT(PPR.FirstName + ' ',PPR.LastName) Name   ,PPR.HourlyRate Rate from 
                                ProjectProposedResourcesByVendor PPR where PPR.ProjectId = @ProjectId and PPR.VendorId = @VendorId ";
                var Result = con.QueryMultiple(query, new { ProjectId = ProjectId, VendorId = VendorId });
                ObjInvoiceRaiseToCompanyList = Result.Read<InvoiceRaiseToCompanyModal>().ToList();
                ObjInvoiceToPhaseList = Result.Read<InvoiceToPhaseModal>().ToList();
                ObjInvoiceForResourceModalList = Result.Read<InvoiceForResourceModal>().ToList();
                ObjInvoiceRaiseToProject.InvoiceToPhaseList = ObjInvoiceToPhaseList;
                ObjInvoiceRaiseToProject.InvoiceRaiseToCompanyList = ObjInvoiceRaiseToCompanyList;
                ObjInvoiceRaiseToProject.InvoiceForResourceList = ObjInvoiceForResourceModalList;


            }
            return ObjInvoiceRaiseToProject;

        }

        public InvoiceRaiseToCompanyModal GetVendorInvoicePreview(int CompanyId, int Id)
        {

            InvoiceRaiseToCompanyModal ObjInvoiceRaise;
            {

                using (var con = new SqlConnection(ConnectionString))
                {
                    string query = @"select C.Id Id, CONCAT(Ud.FirstName + ' ', Ud.LastName) CompanyUserName, Ud.UserId UserId 
                                  ,I.TotalAmount TotalAmount,I.TotalHours TotalHours
                                  ,(select 'Uploads/Profile/Thumbnail/'+ Ud.ProfilePhoto CompanyProfilePic from  UserDetails Ud where Ud.CompanyId = @CompanyId) CompanyProfilePic
                                  ,(select c.Name from  Companies c where C.Id = @CompanyId) Name
                                  ,I.InvoiceDate  InvoiceDate ,I.InvoiceNumber InvoiceNumber      
                                  ,CONCAT(Ud.FirstName + ' ',Ud.LastName) VendorUserName,c.Name VendorCompanyName
                                   from Invoices I
                                 inner join UserDetails ud  on I.UserId = ud.UserId
                                 inner join VendorInvoiceDetails Vid on Vid.InvoiceId = I.Id
                                 inner join Companies C on C.Id = ud.CompanyId where I.Id = @Id";

                    var Result = con.QueryMultiple(query, new { CompanyId = CompanyId, Id = Id });
                    ObjInvoiceRaise = Result.Read<InvoiceRaiseToCompanyModal>().FirstOrDefault();
                }

            }
            return ObjInvoiceRaise;

        }

        public string SaveApproveVendorInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            string msg = string.Empty;
            using (var con = new SqlConnection(ConnectionString))
            {
               
                string sqlquery = @"update Invoices set InvoiceApprovedStatus = 1,ApprovedBy = @UserId where Id = @InvoiceId";
                int count = con.Query<int>(sqlquery, new { UserId = model.UserId, InvoiceId = model.InvoiceId }).SingleOrDefault();

                msg = "Invoice Approved Status Changed";

            }

            return msg;
        }

        public string RejectVendorInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            string msg = string.Empty;
            InvoiceMail ObjInvoiceModal = new InvoiceMail();
            InvoiceModel ObjInvoiceModal1;
            string Message = model.Reason;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update Invoices set IsRejected = 1,RejectReason = @Reason,RejectedBy = @UserId where Id = @InvoiceId
                                 
                                select I.InvoiceNumber InvoiceNo,I.Message Message,Ud.Email,I.TotalAmount
	                            ,CONCAT(Ud.FirstName + ' ',Ud.LastName) ReceiverName
                                ,(select c.Name from Companies c where c.Id = I.CompanyId ) SenderCompanyName 
                                from Invoices I inner join  UserDetails Ud on Ud.UserId = I.UserId where I.Id = @InvoiceId";

                ObjInvoiceModal = con.Query<InvoiceMail>(sqlquery, new { Reason = model.Reason, UserId = model.UserId, InvoiceId = model.InvoiceId }).SingleOrDefault();

            }
            if (ObjInvoiceModal != null)
            {

                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForsendMailOnInvoiceRejectByVendor(ObjInvoiceModal, Message);

                }

                ));
                childref.Start();

            }

            return msg;
        }

        public void ThreadForsendMailOnInvoiceRejectByVendor(InvoiceMail item, string Message)
        {
            String emailbody = null;
            string Subject = string.Empty;

            string subjectBysender = "Invoice Rejected | " + item.InvoiceNo + " | " + item.SenderCompanyName;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendRejectVendorInvoice")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@subjectBysender", subjectBysender).Replace("@@ReceiverName", item.ReceiverName)
                .Replace("@@InvoiceNo", item.InvoiceNo).Replace("@@TotalAmount", item.TotalAmount).Replace("@@Message", Message).Replace("@@Message", item.SenderCompanyName);

            EmailUtility.SendMailInThread(item.Email, subjectBysender, emailbody);

        }

        public MODEL.UserInvoice.UserCardPaymentModel GetCardDetailForVendorPayment(int PaymentDetailId, int CompanyId, int InvoiceId, int StripeApplicationFee)
        {

            MODEL.UserInvoice.UserCardPaymentModel list;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select pay.CreditCardNumber as Card,pay.ExpiryMonth as Month, pay.ExpiryYear as Year,pay.CvvNumber 
                                    as Cvv from PaymentDetails pay where pay.CompanyId = @CompanyId and pay.IsDefault = 1 and pay.IsDeleted = 0";
                list = con.Query<MODEL.UserInvoice.UserCardPaymentModel>(sqlquery, new { CompanyId = CompanyId }).FirstOrDefault();
            }

            if (InvoiceId == 0)
            {
                return list;
            }

            TIGHTEN.ENTITY.Invoice invoice;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from Invoices inv where inv.Id = @InvoiceId";
                invoice = con.Query<TIGHTEN.ENTITY.Invoice>(sqlquery, new { InvoiceId = InvoiceId }).FirstOrDefault();
            }

            decimal Amount = Math.Round(invoice.TotalAmount, 2);
            decimal StripeCharges = Convert.ToDecimal(Math.Round((Convert.ToDouble(Amount) * 0.029) + 0.30, 2));
            //decimal ApplicationFee = Math.Round((Amount * StripeApplicationFee) / 100, 2);
            //decimal NetAmount = Math.Round(Amount + StripeCharges + ApplicationFee, 2); /// 5 percent not use in case of vendor
            decimal NetAmount = Math.Round(Amount + StripeCharges, 2);

            if (list != null)
            {
                list.FreelancerInvoiceAmount = (Amount).ToString();
                list.StripeFees = StripeCharges.ToString();
                //list.TightenCharges = ApplicationFee.ToString();
                list.NetAmountForAdmin = NetAmount.ToString();
            }

            return list;
        }

        public List<InvoiceRaiseToCompanyModal> ProjectForInvoiceDrop(int CompanyId, int VendorId)
        {

            List<InvoiceRaiseToCompanyModal> ObjInvoiceToMilestoneList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"Select P.Id,P.Name from Projects P inner join ProjectVendor Pv on Pv.ProjectId = P.Id
                                where P.CompanyId = @CompanyId and Pv.IsHired = 1";
                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId });
                ObjInvoiceToMilestoneList = Result.Read<InvoiceRaiseToCompanyModal>().ToList();
            }
            return ObjInvoiceToMilestoneList;

        }


        public List<VendorCompanyModel> GetVendorForDrop(int CompanyId)
        {

            List<VendorCompanyModel> ObjVendorModal;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select Distinct C.Name  Name ,Ud.Id VendorCompanyId  from CompanyVendor CV
                        inner join UserDetails Ud  on CV.VendorId = Ud.CompanyId 
						inner join Companies C on C.Id = Ud.Id
						where CV.CompanyId = @CompanyId";
                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId });
                ObjVendorModal = Result.Read<VendorCompanyModel>().ToList();
            }
            return ObjVendorModal;

        }

        public List<CompanyModal> GetCompanyForDrop(int CompanyId)
        {

            List<CompanyModal> ObjCompanyModal;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select  Concat(Ud.FirstName + ' ',Ud.LastName) Name ,Ud.Id  from CompanyVendor CV
                                inner join UserDetails Ud  on CV.CompanyId = Ud.CompanyId where CV.VendorId = @CompanyId";
                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId });
                ObjCompanyModal = Result.Read<CompanyModal>().ToList();
            }
            return ObjCompanyModal;

        }

        public string ChangeInvoiceStatus(MODEL.UserInvoice.InvoiceStatus model, int StripeApplicationFee)
        {
            string msg = string.Empty;



            MODEL.VendorPaymentModel obj;
            PaymentEmailModel user;
            PaymentEmailModel ObjVendorPaymentAdmin = new PaymentEmailModel();
            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("usp_ApproveVendorInvoice", new { UserId = model.UserId, InvoiceId = model.InvoiceId }, commandType: CommandType.StoredProcedure);
                obj = Result.Read<MODEL.VendorPaymentModel>().FirstOrDefault();
                user = Result.Read<PaymentEmailModel>().FirstOrDefault();

                //IF Vendor firm register and accoount not reset than send mail
                if (user.IsExternalBankRoutUpdated == null)
                {
                    ObjVendorPaymentAdmin = Result.Read<PaymentEmailModel>().FirstOrDefault();

                }
            }


            #region Stripe Payment starts here 


            var myCharge = new StripeChargeCreateOptions();
            decimal Amount = obj.TotalAmount;
            decimal StripeCharges = ((Amount * (decimal)2.9) / 100 + (decimal).30);
            int ApplicationFee = Convert.ToInt32((Amount * StripeApplicationFee / 100) * 100); 

            // myCharge.Amount = Convert.ToInt32((Amount + StripeCharges) * 100);
            //myCharge.Amount = Convert.ToInt32(Amount * 100);
            //////////////////////////////////////////////////////////////////////////////////////
            //var myCharge = new StripeChargeCreateOptions();
            //int Amount = Convert.ToInt32(Math.Floor(obj.TotalAmount ));
            //int StripeCharges = Convert.ToInt32(Math.Floor(((Amount * 2.9) / 100) + .30));
            //myCharge.Amount = Amount + StripeCharges;
            //int ApplicationFee = ((Amount * StripeApplicationFee) / 100) + StripeCharges;
            ///////////////////////////////////////////////////////////////////////////////////////////
            myCharge.Currency = "usd";
            myCharge.Amount = Convert.ToInt32(Amount);
            //set this if you want to
            myCharge.Description = "Invoice : Payment of $ " + myCharge.Amount / 100 + " from " + obj.CompanyName + " to " + obj.FirstName;
            myCharge.SourceTokenOrExistingSourceId = model.Token;

            // set this if you have your own application fees (you must have your application configured first within Stripe)
            //myCharge.ApplicationFee = ApplicationFee;
            // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later

            myCharge.Capture = true;

            if (user.StripeUserId != string.Empty)
            {
                myCharge.Destination = user.StripeUserId;
                myCharge.ReceiptEmail = user.Email; // Receipt email of person who is receive the payment
                myCharge.ApplicationFee = ApplicationFee;
                myCharge.Amount = Convert.ToInt32(Amount*100);
                var chargeService = new StripeChargeService();
                StripeCharge stripeCharge = chargeService.Create(myCharge);

                if (stripeCharge.Status == "succeeded")
                {

                    #region Transaction
                    //inserting values in Transaction table

                    Guid obj_Guid = Guid.NewGuid();

                    using (var con = new SqlConnection(ConnectionString))
                    {
                        con.Execute("usp_InsertTransactionForApprovedInvoices", new
                        {
                            EventID = obj.Id,
                            TotalAmount = Convert.ToDecimal(obj.TotalAmount),
                            TransactionID = obj_Guid.ToString(),
                            TransactionBy = model.UserId,
                            TransactionFor = user.UserId,
                            StripeChargeId = stripeCharge.Id,
                            InvoiceId = model.InvoiceId,
                            UserId = model.UserId
                        }, commandType: CommandType.StoredProcedure);
                    }
                    #endregion
                    msg = "Payment successfull";

                    /* Sending Mail via Thread  if payment successfull  Vendor Company Admin   */
                    if (ObjVendorPaymentAdmin != null)
                    {
                        HttpContext ctx1 = HttpContext.Current;
                        Thread childref1 = new Thread(new ThreadStart(() =>
                        {
                            HttpContext.Current = ctx1;
                            ThreadForSendMailVendorPOCsCompany(user, ObjVendorPaymentAdmin, obj);
                        }
                        ));
                        childref1.Start();

                    }

                    /* Sending Mail via Thread  if payment successfull POC and Vendor Comapany Not Exist   */
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForApproveInvoice(user, obj);
                    }
                    ));
                    childref.Start();

                }
            }

            #endregion
            return msg;
        }

        // Mail for approve Invoice
        public void ThreadForApproveInvoice(MODEL.PaymentEmailModel EmmailModel, MODEL.VendorPaymentModel Model)
        {

            decimal PercetOfTotal = Convert.ToInt32(Math.Floor((Model.TotalAmount * 5) / 100));
            decimal AmountGot = Convert.ToInt32(Math.Floor(Model.TotalAmount - PercetOfTotal));


            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "ApproveVendorInvoice")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var item in MailModule)
            {
                emailbody = item.mailbody;
                Subject = item.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", EmmailModel.UserName).Replace("@@TotalAmount", Model.TotalAmount.ToString())
                .Replace("@@PercetOfTotal", PercetOfTotal.ToString()).Replace("@@AmountGot", AmountGot.ToString())
                .Replace("@@PaIdbyUserName", Model.CompanyName).Replace("@@InvoiceID", Model.InvoiceNumber)
                .Replace("@@InvoiceID", Model.InvoiceNumber);
            Subject = Subject.Replace("@@InvoiceID", Model.InvoiceNumber);
            EmailUtility.SendMailInThread(EmmailModel.Email, Subject, emailbody);
        }

        // Mail send for Vendor Poc company If Exists
        public void ThreadForSendMailVendorPOCsCompany(MODEL.PaymentEmailModel EmmailModel, MODEL.PaymentEmailModel EmmailModel1, MODEL.VendorPaymentModel Model)
        {



            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "VendorAdminProfileAccountNotSetPaymentSendToPOCs")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var item in MailModule)
            {
                emailbody = item.mailbody;
                Subject = item.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", EmmailModel1.UserName).Replace("@@InvoiceID", Model.InvoiceNumber)
                                 .Replace("@@PaymentReceiver", EmmailModel.UserName);
            Subject = Subject.Replace("@@InvoiceID", Model.InvoiceNumber);
            EmailUtility.SendMailInThread(EmmailModel1.Email, Subject, emailbody);
        }

        #region Paging 
        public List<InvoiceModel> GetAllVendorInvoiceListOutstanding(VendorInvoiceRequestFilterModel model)
        {
            List<InvoiceModel> objInvoiceModal;
            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceModel>("Usp_GetAllVendorInvoiceListOutstanding", new
                {
                    CompanyId = model.CompanyId,
                    ToDate = model.ToDate,
                    FromDate = model.FromDate,
                    VendorCompanyId = model.VendorCompanyId,
                    PageIndex = model.PageIndex,
                    PageSizeSelected = model.PageSizeSelected

                }, commandType: CommandType.StoredProcedure).ToList();
            }
            return objInvoiceModal;
        }

        public List<InvoiceModel> GetAllVendorInvoiceListPaid(VendorInvoiceRequestFilterModel model)
        {
            List<InvoiceModel> objInvoiceModal;
            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceModel>("Usp_GetAllVendorInvoiceListPaid", new
                {
                    CompanyId = model.CompanyId,
                    ToDate = model.ToDate,
                    FromDate = model.FromDate,
                    VendorCompanyId = model.VendorCompanyId,
                    PageIndex = model.PageIndex,
                    PageSizeSelected = model.PageSizeSelected

                }, commandType: CommandType.StoredProcedure).ToList();
            }
            return objInvoiceModal;
        }

        public List<InvoiceModel> GetAllVendorInvoiceListRejected(VendorInvoiceRequestFilterModel model)
        {
            List<InvoiceModel> objInvoiceModal;
            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceModel>("Usp_GetAllVendorInvoiceListRejected", new
                {
                    CompanyId = model.CompanyId,
                    ToDate = model.ToDate,
                    FromDate = model.FromDate,
                    VendorCompanyId = model.VendorCompanyId,
                    PageIndex = model.PageIndex,
                    PageSizeSelected = model.PageSizeSelected

                }, commandType: CommandType.StoredProcedure).ToList();
            }
            return objInvoiceModal;
        }

        public List<InvoiceModel> GetAllInvoiceDetailListOutstanding(InvoiceModel model)
        {
            List<InvoiceModel> objInvoiceModal;

            DateTime date1 = new DateTime(0001, 1, 1);
            int result = DateTime.Compare(date1, model.ToDate);
            int result1 = DateTime.Compare(date1, model.FromDate);

            if (result == 0)
            {
                model.ToDate = DateTime.Now;
            }

            if (result1 == 0)
            {
                model.FromDate = DateTime.Now.AddMonths(-1);
            }


            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceModel>("Usp_GetAllInvoiceDetailListOutstanding", new
                {
                    VendorId = model.VendorId,
                    CompanyId = model.CompanyId,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    PageIndex = model.PageIndex,
                    PageSizeSelected = model.PageSizeSelected


                }, commandType: CommandType.StoredProcedure).ToList();
            }
            return objInvoiceModal;

        }

        public List<InvoiceModel> GetAllInvoiceDetailListPaid(InvoiceModel model)
        {
            List<InvoiceModel> objInvoiceModal;

            DateTime date1 = new DateTime(0001, 1, 1);
            int result = DateTime.Compare(date1, model.ToDate);
            int result1 = DateTime.Compare(date1, model.FromDate);

            if (result == 0)
            {
                model.ToDate = DateTime.Now;
            }

            if (result1 == 0)
            {
                model.FromDate = DateTime.Now.AddYears(-1);
            }


            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceModel>("Usp_GetAllInvoiceDetailListPaid", new
                {
                    VendorId = model.VendorId,
                    CompanyId = model.CompanyId,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    PageIndex = model.PageIndex,
                    PageSizeSelected = model.PageSizeSelected


                }, commandType: CommandType.StoredProcedure).ToList();
            }
            return objInvoiceModal;

        }

        public List<InvoiceModel> GetAllInvoiceDetailListRejected(InvoiceModel model)
        {
            List<InvoiceModel> objInvoiceModal;
            DateTime date1 = new DateTime(0001, 1, 1);
            int result = DateTime.Compare(date1, model.ToDate);
            int result1 = DateTime.Compare(date1, model.FromDate);
            if (result == 0)
            {
                model.ToDate = DateTime.Now;
            }
            if (result1 == 0)
            {
                model.FromDate = DateTime.Now.AddYears(-1);
            }
            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceModel>("Usp_GetAllInvoiceDetailListRejected", new
                {
                    VendorId = model.VendorId,
                    CompanyId = model.CompanyId,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    PageIndex = model.PageIndex,
                    PageSizeSelected = model.PageSizeSelected


                }, commandType: CommandType.StoredProcedure).ToList();
            }
            return objInvoiceModal;
        }

        #endregion
    }




}

