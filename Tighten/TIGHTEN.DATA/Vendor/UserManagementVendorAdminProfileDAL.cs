﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL.Vendor.VendorAdminModel;
using Dapper;
using TIGHTEN.UTILITIES;
using System.Web;
using System.Threading;
using System.Xml.Linq;

namespace TIGHTEN.DATA.Vendor
{
    public class UserManagementVendorAdminProfileDAL : BaseClass
    {


        public UserManagementModel GetUserAndResourceVendorAdminProfile(int VendorCompanyId)
        {
            UserManagementModel ObjUserManagement = new UserManagementModel();
            List<ExistingUsersModel> ObjExistingUsersList;
            List<ResourceModel> ObjResourceList;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"select ud.Id VendorId,UD.FirstName,UD.LastName, CONCAT(UD.FirstName + ' ',UD.LastName) VendorName,UD.Email Email,
                                    UD.EmailConfirmationCodeCreatedDate JoinDate  , R.Name RoleName,R.RoleId
                                ,(select round(AVG(PV.Rating),0) from ProjectVendor PV where PV.VendorId = Ud.Id) Rating
                                ,ISNULL('Uploads/Profile/Thumbnail/'+ud.ProfilePhoto,'Uploads/Default/profile.png') ProfilePhoto
                                ,UD.IsActive, UD.Password,IsNull(UD.Indefinitely,1) Indefinitely ,IsNull( UD.ValidUntil,0) ValidUntil,UD.ValidUntilDate
                                from UserDetails UD  Inner join Roles R on R.RoleId = UD.RoleId where UD.CompanyId = @VendorCompanyId and UD.RoleId <> 1


                                 select Distinct  PPRV.FirstName FirstName, PPRV.LastName LastName, PPRV.Email Email, PPRV.Profile Profile,  UD.Password from ProjectProposedResourcesByVendor PPRV
                                 inner join UserDetails UD on PPRV.VendorId = UD.Id where UD.CompanyId = @VendorCompanyId and PPRV.Email Not In (select Email  from UserDetails where CompanyId = @VendorCompanyId)";


                var Result = con.QueryMultiple(query,
                    new { VendorCompanyId = VendorCompanyId });

                ObjExistingUsersList = Result.Read<ExistingUsersModel>().ToList();
                ObjResourceList = Result.Read<ResourceModel>().ToList();
            }

            ObjUserManagement.ExistingUsersList = ObjExistingUsersList;
            ObjUserManagement.ResourceList = ObjResourceList;
            return ObjUserManagement;


        }


        public List<RoleVendorAdminProfileModel> GetExistingUserRoleVendorAdminProfile(int VendorId)
        {

            List<RoleVendorAdminProfileModel> ObjRoleModelList;
            using (var con = new SqlConnection(ConnectionString))
            {
                //type 1,3 means its a role and 3 means its both role and job profile
                string query = @"select RoleId,Name AS RoleName from Roles WHERE isActive=1 and IsDeleted=0 and Type in (1,3) and CompanyId in(@VendorId,0)";
                ObjRoleModelList = con.Query<RoleVendorAdminProfileModel>(query, new { VendorId = VendorId }).ToList();
            }

            return ObjRoleModelList;
        }


        public ResourceStatisticsModel GetResourceVendorAdminProfile(ResourceRequestModel Model)
        {
            ResourceStatisticsModel ObjResourceStatistics;
            List<RoleVendorAdminProfileModel> ObjRoleModelList;
            List<VendorProjectDetailModel> ObjRoleVendorList;
            using (var con = new SqlConnection(ConnectionString))
            {


                string query = @"Declare @AllworkedRole  Nvarchar(max)
                            SELECT Top 1  @AllworkedRole = 
                                STUFF((select Distinct ', ' +  R.Name from ProjectProposedResourcesByVendor PPRV
                            inner join Roles R ON PPRV.RoleId = R.RoleId where Email =  @EmailId
                                      FOR XML PATH('')), 1, 2, '')
                            FROM ProjectProposedResourcesByVendor a  where Email = @EmailId

                            select Distinct
                            PPRV.FirstName
                            ,PPRV.LastName
                            ,PPRV.Email
                            ,PPRV.Profile
                            ,@AllworkedRole AllworkedRole
                            ,(select  AVG(PPRV2.Rating) from ProjectProposedResourcesByVendor PPRV2  where PPRV2.Email =  @EmailId) Rating
                            ,(select  sum(PPRV1.SpentHours) from ProjectProposedResourcesByVendor PPRV1  where PPRV1.Email =  @EmailId) HorsWorked
                              from ProjectProposedResourcesByVendor PPRV where Email = @EmailId
                            
                            select R.Name RoleName, R.RoleId from Roles R  where RoleId <> 1

                            Select P.Name ProjectName,P.IsComplete
                            , PPRV.Rating
                            ,R.Name
                            ,PPRV.SpentHours Hours
                            ,R.Name RoleName
                            ,P.IsComplete
                            from ProjectProposedResourcesByVendor  PPRV
                            inner join Projects P on PPRV.ProjectId = P.Id
                            inner join Roles R on R.RoleId = PPRV.RoleId where PPRV.Email = @EmailId";

                var Result = con.QueryMultiple(query, new { EmailId = Model.Email });


                ObjResourceStatistics = Result.Read<ResourceStatisticsModel>().FirstOrDefault();
                ObjRoleModelList = Result.Read<RoleVendorAdminProfileModel>().ToList();
                ObjRoleVendorList = Result.Read<VendorProjectDetailModel>().ToList();

                ObjResourceStatistics.RoleList = ObjRoleModelList;
                ObjResourceStatistics.VendorProjectList = ObjRoleVendorList;
            }

            return ObjResourceStatistics;
        }

        public int SaveExistingUserAdminProfile(SaveExistingUserModel Model)
        {

            int SaveId = 0;
            using (var con = new SqlConnection(ConnectionString))
            {
                if (Model.ValidUntil == false)
                {
                    string query = @"update UserDetails set RoleId = @RoleId, password= @Password, IsActive=@IsActive ,Indefinitely= @Indefinitely ,ValidUntil= @ValidUntil where Id = @VendorId";
                    con.Query(query, new { RoleId = Model.RoleId, Password = Model.Password, IsActive = Model.IsActive, Indefinitely = Model.Indefinitely, ValidUntil = Model.ValidUntil, VendorId = Model.VendorId });
                }
                else
                {
                    string query = @"update UserDetails set RoleId = @RoleId, password= @Password, IsActive=@IsActive ,Indefinitely= @Indefinitely ,ValidUntil= @ValidUntil,ValidUntilDate= @ValidUntilDate where Id = @VendorId";
                    con.Query(query, new { RoleId = Model.RoleId, Password = Model.Password, IsActive = Model.IsActive, Indefinitely = Model.Indefinitely, ValidUntil = Model.ValidUntil, ValidUntilDate = Model.ValidUntilDate, VendorId = Model.VendorId });
                }
                SaveId = 1;
            }

            return SaveId;
        }


        public int InviteResourceAsVendor(SaveResourceAsVendorRequestModel Model, string url)
        {

            int SaveId = 0;
            SaveResourceAsVendorRequestModel ObjSaveResource;

            string EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
            string UserId = Guid.NewGuid().ToString();
            using (var con = new SqlConnection(ConnectionString))
            {

                ObjSaveResource = con.Query<SaveResourceAsVendorRequestModel>("Usp_RegisterResourceAsVendor", new
                {
                    Email = Model.Email,
                    VendorCompanyId = Model.VendorCompanyId,
                    RoleId = Model.RoleId,
                    VendorUserId = UserId,
                    EmailConfirmationCode = EmailConfirmationCode,
                    UserId = Model.UserId
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                SaveId = 1;
            }


            HttpContext ctx = HttpContext.Current;
            Thread childref = new Thread(new ThreadStart(() =>
            {
                HttpContext.Current = ctx;
                ThreadForSendMailResourceAsVendor(ObjSaveResource, url);
            }

            ));
            childref.Start();

            return SaveId;
        }


        private void ThreadForSendMailResourceAsVendor(SaveResourceAsVendorRequestModel MailModel, string url)
        {

            String emailbody = null;
            string Subject = string.Empty;
            string subjectBysender = string.Empty;
            subjectBysender = "Invitation";

            var callbackUrl = url + "#/confirmemail/" + MailModel.UserId + "/" + MailModel.EmailConfirmationCode + "/" + 1;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendToVendorResourceInvitationMail")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 Subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.Subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", MailModel.ReceiverName).Replace("@@SenderName", MailModel.SenderName)
                            .Replace("@@callbackUrl", callbackUrl).Replace("@@SenderCompanyName", MailModel.SenderCompanyName);

            EmailUtility.SendMailInThread(MailModel.Email, subjectBysender, emailbody);


        }

        public int ActiveInactiveUserVendorAdminProfile(SaveExistingUserModel Model)
        {

            int SaveId = 0;

            Model.IsActive = Model.IsActive == false ? true : Model.IsActive == true ? false : false;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"update UserDetails set IsActive = @IsActive  where Id = @VendorId";
                con.Query(query, new { IsActive = Model.IsActive, VendorId = Model.VendorId });

                SaveId = 1;
            }

            return SaveId;
        }

        public List<ExistingUsersModel> GetUserForVendorAdminProfile(int VendorCompanyId, int PageIndex, int PageSizeSelected)
        {
            List<ExistingUsersModel> ObjExistingUsersList;
            using (var con = new SqlConnection(ConnectionString))
            {
                ObjExistingUsersList = con.Query<ExistingUsersModel>("usp_GetUsersForVendorAdminProfile", new
                {
                    VendorCompanyId = VendorCompanyId,
                    PageIndex = PageIndex,
                    PageSizeSelected = PageSizeSelected
                }, commandType: CommandType.StoredProcedure).ToList();

            }
            return ObjExistingUsersList;
        }

        public List<ResourceModel> GetResourcesForVendorAdminProfile(int VendorCompanyId, int PageIndex, int PageSizeSelected)
        {
            List<ResourceModel> ObjExistingResourceList;
            using (var con = new SqlConnection(ConnectionString))
            {
                ObjExistingResourceList = con.Query<ResourceModel>("usp_GetResourcesForVendorAdminProfile", new
                {
                    VendorCompanyId = VendorCompanyId,
                    PageIndex = PageIndex,
                    PageSizeSelected = PageSizeSelected
                }, commandType: CommandType.StoredProcedure).ToList();

            }
            return ObjExistingResourceList;
        }

        #region invitenewuserlist

        public List<MODEL.Vendor.UserInvitationModal> GetAllExistingInternalUsers(int CompanyId, int PageIndex, int PageSizeSelected)
        {
            List<MODEL.Vendor.UserInvitationModal> objInternalUsersModel = null;

            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("usp_GetAllExistingInternalUsers", new
                {
                    CompanyId = CompanyId,
                    PageIndex = PageIndex,
                    PageSizeSelected = PageSizeSelected
                }, commandType: CommandType.StoredProcedure);

                objInternalUsersModel = Result.Read<MODEL.Vendor.UserInvitationModal>().ToList();
                if (objInternalUsersModel != null)
                {
                    var objUserRoleModel = Result.Read<MODEL.UserRoleModel>().ToList();
                    foreach (var options in objInternalUsersModel)
                    {
                        options.UserRoleList = objUserRoleModel.Where(x => x.UserId == options.UserId).ToList();

                    }
                }
            }
            return objInternalUsersModel;
        }

        public int ActiveInactiveInternalUsersVendorAdminProfile(MODEL.Vendor.UserInvitationModal Model)
        {
            int SaveId = 0;
            Model.IsActive = Model.IsActive == false ? true : Model.IsActive == true ? false : false;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"update UserDetails set IsActive = @IsActive  where Id = @Id";
                con.Query(query, new { IsActive = Model.IsActive, Id = Model.Id});
                SaveId = 1;
            }
            return SaveId;
        }
        #endregion
    }
}
