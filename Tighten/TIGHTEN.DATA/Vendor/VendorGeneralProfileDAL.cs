﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using TIGHTEN.MODEL.VendorGeneralProfile;
using System.Data;

namespace TIGHTEN.DATA.Vendor
{
    public class VendorGeneralProfileDAL : BaseClass
    {

        public List<ProjectVendorGeneralProfile> GetMyProject(FilterProjectListVendorGeneralProfile Model)
        {
            List<ProjectVendorGeneralProfile> ObjProjectList = new List<ProjectVendorGeneralProfile>();
            //using (var con = new SqlConnection(ConnectionString))
            //{
            //    string query2 = string.Empty;

            //    if (Model.ProjectId != 0)
            //    {
            //        query2 = " and P.Id = @ProjectId";
            //    }
            //    else if (Model.ProjectId != 0 && (Model.ProjectStatus == 1 || Model.ProjectStatus == 3))
            //    {
            //        query2 = " and P.Id = @ProjectId and P.IsComplete = @ProjectStatus";
            //    }
            //    else if (Model.ProjectId == 0 && (Model.ProjectStatus == 1 || Model.ProjectStatus == 3))
            //    {
            //        query2 = " and P.IsComplete = @ProjectStatus";
            //    }
            //    if (Model.ProjectStatus == 3)
            //    {
            //        query2 = "and P.IsComplete = @ProjectStatus";
            //    }


            //    string query = @"select 
            //                    P.Id ProjectId
	           //                ,P.Name ProjectName
	           //                 ,PV.HiredOn ProjectStartDate
	           //                 ,ISNULL(PPR.SpentHours,0 ) HoursSpent 
            //                    ,PPR.ExpectedHours HoursAllotted
	           //                 ,R.Name RoleAllotted
	           //                 ,PPR.Rating  Rating
	           //                 ,(select Concat(UD.FirstName + ' ' ,UD.LastName)  from userDetails UD where  UD.Id = PV.vendorId ) VendorPoc
            //                    from ProjectProposedResourcesByVendor PPR
            //                    Inner JOin Projects P on P.Id = PPR.ProjectId 
            //                    Inner JOin ProjectVendor PV on PV.ProjectId = PPR.ProjectId and PV.VendorId = PPR.VendorId
            //                    Inner Join Roles R on R.RoleId = PPR.RoleId
            //                    inner join UserDetails UD on UD.Email = PPR.Email
            //                    where UD.UserId = @VendorGeneralUserId " + query2;

            //    ObjProjectList = con.Query<ProjectVendorGeneralProfile>(query, new
            //    {
            //        VendorGeneralUserId = Model.VendorGeneralUserId,
            //        ProjectId = Model.ProjectId,
            //        ProjectStatus = Model.ProjectStatus,
            //    }).ToList();

            //}
            using (var con = new SqlConnection(ConnectionString))
            {
                ObjProjectList = con.Query<ProjectVendorGeneralProfile>("usp_GetMyProjectsVendorGeneralProfile", new
                {
                    VendorGeneralUserId = Model.VendorGeneralUserId,
                    ProjectId = Model.ProjectId,
                    ProjectStatus = Model.ProjectStatus,
                    PageIndex = Model.PageIndex,
                    PageSizeSelected = Model.PageSizeSelected


                }, commandType: CommandType.StoredProcedure).ToList();

            }
            
            return ObjProjectList;


        }


        public List<ProjectModel> GetProjectNameForFilter(FilterProjectListVendorGeneralProfile Model)
        {
            List<ProjectModel> ObjProjectVendorList;
            SqlConnection Obj;
            using (Obj = new SqlConnection(ConnectionString))
            {

                string query = @"select p.Name ProjectName,P.Id ProjectId from ProjectProposedResourcesByVendor PPR
                                inner join UserDetails Ud on PPR.Email = Ud.Email inner join Projects P on P.Id = PPR.ProjectId
                                where ud.UserId =  @VendorGeneralUserId and p.Name like  @ProjectName";

                ObjProjectVendorList = Obj.Query<ProjectModel>(query, new
                {
                    VendorGeneralUserId = Model.VendorGeneralUserId,
                    ProjectName = "%" + Model.ProjectName + "%",

                }).ToList();

            }

            return ObjProjectVendorList;
        }


        public List<WorkDiaryModel> GetWorkNoteForVendorGeneralUser(int ProjectId, string VendorGeneralUserId)
        {
            List<WorkDiaryModel> ObjWorkDiary = new List<WorkDiaryModel>();
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select UWN.UserId VendorGeneralUserId, UWN.ProjectId,UWN.Note Note, UWN.Date NoteDate, UWN.Hour Hour from UserWorkNotes UWN
                where UWN.ProjectId = @ProjectId and UWN.UserId = @VendorGeneralUserId";
                ObjWorkDiary = con.Query<WorkDiaryModel>(query, new { ProjectId = ProjectId, VendorGeneralUserId = VendorGeneralUserId }).ToList();

            }

            return ObjWorkDiary;

        }


        public int InsertNoteVendorGeneralUser(WorkDiaryModel Model)
        {
            int WorkNoteId = 0;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"Insert into UserWorkNotes(UserId,ProjectId,Note,Date,Hour) values(@UserId,@ProjectId,@Note,Getdate(),@Hour) 
                                SELECT SCOPE_IDENTITY()";

                WorkNoteId = con.Query<int>(query, new
                {
                    ProjectId = Model.ProjectId,
                    UserId = Model.VendorGeneralUserId,
                    Note = Model.Note,
                    Hour = Model.Hour,

                }).FirstOrDefault();

                return WorkNoteId;
            }

        }



        public VendorGeneralProfProjectAwardedViewDetailModel GetMyProjectViewDetailVendorGeneralProfile(int ProjectId, string UserId)
        {
            VendorGeneralProfProjectAwardedViewDetailModel ObjProjectAwardedViewDetail = new VendorGeneralProfProjectAwardedViewDetailModel();
            List<VendorGeneralProfProjectDocument> ObjProjectDocList;
            List<VendorGeneralProfVendorPhaseModal> ObjVendorPhaseList;
            List<VendorGeneralProfPhaseDocument> ObjPhaseDocumentList;
            List<VendorGeneralProfMilestone> ObjVendorMileList;
            List<VendorGeneralProfResourceModal> ObjResourceList;
            List<VendorProjectProgress> ObjProjectProgress;
            List<VendorGeneralProfCustomControlUIModel> ObjCustomControlUIList;
            VendorGeneralProfCustomControlValueModel ObjCustomControlValueList;
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetAwardedProjectViewDetailVendorGeneralProfile", new { ProjectId = ProjectId, UserId = UserId }, commandType: CommandType.StoredProcedure);

                ObjProjectAwardedViewDetail = Result.Read<VendorGeneralProfProjectAwardedViewDetailModel>().FirstOrDefault();
                ObjProjectDocList = Result.Read<VendorGeneralProfProjectDocument>().ToList();
                ObjVendorPhaseList = Result.Read<VendorGeneralProfVendorPhaseModal>().ToList();
                ObjPhaseDocumentList = Result.Read<VendorGeneralProfPhaseDocument>().ToList();
                ObjVendorMileList = Result.Read<VendorGeneralProfMilestone>().ToList();
                ObjResourceList = Result.Read<VendorGeneralProfResourceModal>().ToList();
                ObjProjectProgress = Result.Read<VendorProjectProgress>().ToList();
                ObjCustomControlUIList = Result.Read<VendorGeneralProfCustomControlUIModel>().ToList();
                ObjCustomControlValueList = Result.Read<VendorGeneralProfCustomControlValueModel>().FirstOrDefault();
            }




            foreach (VendorGeneralProfVendorPhaseModal item in ObjVendorPhaseList)
            {

                List<VendorGeneralProfPhaseDocument> ObjPhasedocList = ObjPhaseDocumentList.Where(x => x.PhaseId == item.PhaseId).ToList();

                item.PhaseDocumentList = new List<VendorGeneralProfPhaseDocument>();
                foreach (VendorGeneralProfPhaseDocument item1 in ObjPhasedocList)
                {
                    item.PhaseDocumentList.Add(item1);
                }
                List<VendorGeneralProfMilestone> PMilestoneList = ObjVendorMileList.Where(x => x.PhaseId == item.PhaseId).ToList();

                item.MileStone = new List<VendorGeneralProfMilestone>();
                foreach (VendorGeneralProfMilestone item2 in PMilestoneList)
                {
                    item.MileStone.Add(item2);
                }
                int i = 0;
                foreach (VendorGeneralProfCustomControlUIModel item3 in ObjCustomControlUIList)
                {
                    List<VendorGeneralProfCustomUIDropDownModel> ObjCustDrop = new List<VendorGeneralProfCustomUIDropDownModel>();
                    List<VendorGeneralProfCustomUIRadioButtonModel> ObjRadioList = new List<VendorGeneralProfCustomUIRadioButtonModel>();
                    if (item3.Values != null && item3.ControlType == "Dropdown")
                    {
                        string[] drop = item3.Values.Split(',');
                        int j = 1;
                        foreach (string item1 in drop)
                        {
                            VendorGeneralProfCustomUIDropDownModel obj = new VendorGeneralProfCustomUIDropDownModel();
                            obj.Name = item1;
                            obj.Id = j;
                            ObjCustDrop.Add(obj);
                            j++;
                        }
                    }
                    if (item3.Values != null && item3.ControlType == "Radio buttons")
                    {
                        string[] drop = item3.Values.Split(',');
                        int k = 1;
                        foreach (string item1 in drop)
                        {
                            VendorGeneralProfCustomUIRadioButtonModel obj = new VendorGeneralProfCustomUIRadioButtonModel();
                            obj.Name = item1;
                            obj.Id = k;
                            ObjRadioList.Add(obj);
                            k++;
                        }
                    }

                    ObjCustomControlUIList[i].FieldModel = ObjCustomControlUIList[i].FieldName.Replace(' ', '_');
                    ObjCustomControlUIList[i].CustomDropDownList = ObjCustDrop;
                    ObjCustomControlUIList[i].CustomRadioButtonList = ObjRadioList;
                    i++;

                }

                //ObjProjectAwardedViewDetail.PhaseList = ObjPhasedocList;

            }
            ObjProjectAwardedViewDetail.ProjectDocumentList = ObjProjectDocList;
            ObjProjectAwardedViewDetail.PhaseList = ObjVendorPhaseList;
            ObjProjectAwardedViewDetail.CustomControlUIList = ObjCustomControlUIList;
            ObjProjectAwardedViewDetail.CustomControlValueModel = ObjCustomControlValueList;
            ObjProjectAwardedViewDetail.VendorProjectProgressList = ObjProjectProgress;
            ObjProjectAwardedViewDetail.ResourcesList = ObjResourceList;


            return ObjProjectAwardedViewDetail;
        }


       

    }
}
