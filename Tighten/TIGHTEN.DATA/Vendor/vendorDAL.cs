﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL.Vendor;
using TIGHTEN.UTILITIES;
using TIGHTEN.DATA;
using System.Data;
using TIGHTEN.MODEL;
using static TIGHTEN.MODEL.ProjectModel;
using System.Net.Http;

namespace TIGHTEN.DATA.Vendor
{

    public class vendorDAL : BaseClass
    {

        public List<vendorModel> GetCompanyVendorList(int CompanyId)
        {

            vendorModel obj = new vendorModel();
            List<vendorModel> objVM;

            //int PageIndex1 = Model.PageIndex == 0 ? 1 : Model.PageIndex;
            //int PageSize2 = Model.PageSize == 0 ? 10 : Model.PageSize;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT Id,CompanyId,VendorId,CreatedDate,IsActive FROM CompanyVendor  where CompanyId = @CompanyId";

                //string query = @"SELECT Id,CompanyId,VendorId,CreatedDate,IsActive FROM ( SELECT ROW_NUMBER() OVER(ORDER BY Id) AS NUMBER,
                //                          Id, CompanyId, VendorId, CreatedDate, IsActive  FROM CompanyVendor where CompanyId = @CompanyId
                //                                 ) AS TBL
                //                        WHERE NUMBER BETWEEN((@PageIndex - 1) * @PageSize + 1) AND(@PageIndex * @PageSize)
                //                    ORDER BY Id";
                objVM = con.Query<vendorModel>(query, new
                {
                    CompanyId = CompanyId,

                }).ToList();

            }

            return objVM;



        }



        public VendorCompanyMail SaveVendor(VendorInvitationModal modal, string url)
        {
            int Id = 0;
            string msg;

            string VendorUserId = Guid.NewGuid().ToString();
            UserDetail obj = new UserDetail();

            VendorCompanyMail objCompanysend;
            string keyy = UTILITIES.EmailUtility.GetUniqueKey(16);
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_RegisterVendor",
                    new
                    {
                        VendorUserId = VendorUserId,
                        UserId = modal.UserId,
                        FirstName = modal.FirstName,
                        LastName = modal.LastName,
                        CompanyName = modal.CompanyName,
                        EmaiAddress = modal.EmaiAddress,
                        CompanyId = modal.CompanyId,
                        Website=modal.Website,
                        PhoneNo=modal.PhoneNo,
                        MessageTextBox = modal.MessageTextBox,
                        EmailConfirmationCode = EmailUtility.GetUniqueKey(8),
                        CId=modal.CId
                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<VendorCompanyMail>().FirstOrDefault();
                


                if (objCompanysend.Id != -1)
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        objCompanysend.UserId = modal.UserId;
                        objCompanysend.EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
                        ThreadForInvitationVendor(objCompanysend, url);
                    }
                    ));
                    childref.Start();
                    msg = "Message Saved";

                }


            }

            return objCompanysend;
        }


        public List<VendorInvitationsModal> GetInvitaionFromCompany(VendorInvitationsModal Modal)
        {
            List<VendorInvitationsModal> objVenodrComp;
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetInvitaionFromCompany",
                    new
                    {
                        UserId = Modal.UserId,
                        CompanyId = Modal.CompanyId,
                        IsApproved = Modal.IsApproved,
                        Id = Modal.Id,
                        PageIndex = 1,
                        PageSize = 1

                    }, commandType: CommandType.StoredProcedure);
                objVenodrComp = Result.Read<VendorInvitationsModal>().ToList();


            }



            return objVenodrComp;
        }

        public VendorInvitationsModal AcceptRejectInvitaionAsVendor(VendorInvitationsModal modal) // false
        {
            string AcceptOrRjectMail = "";
            VendorInvitationsModal ObjVenodrComp = new VendorInvitationsModal();
            VendorInvitationsModal ObjVenodrComp1 = new VendorInvitationsModal();
            VendorCompanyMail ObjVendorMaiL;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = "";
                if (modal.IsApproved.Value)
                {
                    AcceptOrRjectMail = "SendToCompanyAcceptedInvitation";

                    query = @"insert into CompanyVendor (CompanyId,VendorId,CreatedDate,IsActive ) values(@CompanyId, @VendorId,getdate(),1)
                            update VendorInvitations set IsApproved = 1 where Id = @Id select Name from Companies where Id = @Id
                            select concat(ud.FirstName +' ',ud.LastName) ReceiverName,ud.Email  
                            ,(select concat(Ud1.FirstName +' ',Ud1.LastName ) Name from  UserDetails Ud1 where  Ud1.UserId = @UserId  ) SenderName
                            from VendorInvitations VI inner join UserDetails ud on VI.Invitedby = ud.UserId where VI.Id = @Id";
                }
                else
                {
                    AcceptOrRjectMail = "SendToCompanyRejectedInvitation";
                    query = @"update VendorInvitations set IsApproved = 0 ,ReasonReject = @ReasonReject where Id = @Id select 1 as Id
                            select concat(ud.FirstName +' ',ud.LastName) ReceiverName,ud.Email  
                            ,(select concat(Ud1.FirstName +' ',Ud1.LastName ) Name from  UserDetails Ud1 where  Ud1.UserId = @UserId  ) SenderName
                            from VendorInvitations VI inner join UserDetails ud on VI.Invitedby = ud.UserId where VI.Id = @Id ";
                }
                var Result = con.QueryMultiple(query, new { Id = modal.Id, CompanyId = modal.CompanyId, VendorId = modal.VendorId, ReasonReject = modal.ReasonReject, UserId = modal.UserId });
                ObjVenodrComp1 = Result.Read<VendorInvitationsModal>().FirstOrDefault();
                ObjVendorMaiL = Result.Read<VendorCompanyMail>().FirstOrDefault();

                ObjVenodrComp.Name = modal.Name;

                if (ObjVendorMaiL != null)
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForAcceptRejectInvitaionAsVendor(ObjVendorMaiL, AcceptOrRjectMail);
                    }
                    ));
                    childref.Start();

                }

            }
            return ObjVenodrComp;
        }



        // sending Accept Or Reject Mail
        private void ThreadForAcceptRejectInvitaionAsVendor(VendorCompanyMail objCompanysend, string AcceptOrRjectMail)
        {


            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == AcceptOrRjectMail)
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", objCompanysend.ReceiverName).Replace("@@SenderName", objCompanysend.SenderName)
                .Replace("@@message", objCompanysend.Message);

            //     Subject = Subject.Replace("@@SenderName", objCompanysend.Name);

            EmailUtility.SendMailInThread(objCompanysend.Email, Subject, emailbody);


        }


        // sending mail for registration
        private void ThreadForInvitationVendor(VendorCompanyMail objCompanysend, string url)
        {

            var callbackUrl = url + "#/confirmemail/" + objCompanysend.UserId + "/" + objCompanysend.EmailConfirmationCode + "/" + 1;
            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendToVendorInvitation")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", objCompanysend.Name).Replace("@@SenderCompanyName", objCompanysend.SenderCompanyName)
                .Replace("@@VendorCompanyName", objCompanysend.VendorCompany).Replace("@@message", objCompanysend.Message)
                .Replace("@@callbackUrl", callbackUrl);
            //     Subject = Subject.Replace("@@SenderName", objCompanysend.Name);

            EmailUtility.SendMailInThread(objCompanysend.Email, Subject, emailbody);


        }

        // sending mail for registration of new user
        private void ThreadForAddPOC(VendorCompanyMail objCompanysend, string url, int SendId)
        {

            
            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            if (SendId == 1)
            {
                var callbackUrl = url + "#/confirmemail/" + objCompanysend.UserId + "/" + objCompanysend.EmailConfirmationCode + "/" + 1;
                var MailModule = from r in xdoc.Descendants("MailModule").
                                 Where(r => (string)r.Attribute("id") == "SendToNewPOCUserInvitation")
                                 select new
                                 {
                                     mailbody = r.Element("Body").Value,
                                     subject = r.Element("Subject").Value
                                 };

                foreach (var Mailitem in MailModule)
                {
                    emailbody = Mailitem.mailbody;
                    Subject = Mailitem.subject;
                }

                emailbody = emailbody.Replace("@@ReceiverName", objCompanysend.Name).Replace("@@SenderCompanyName", objCompanysend.SenderCompanyName)

                    .Replace("@@callbackUrl", callbackUrl);


            }
            else
            {
                var MailModule = from r in xdoc.Descendants("MailModule").
                                 Where(r => (string)r.Attribute("id") == "SendToExistedPOCUserInvitation")
                                 select new
                                 {
                                     mailbody = r.Element("Body").Value,
                                     subject = r.Element("Subject").Value
                                 };

                foreach (var Mailitem in MailModule)
                {
                    emailbody = Mailitem.mailbody;
                    Subject = Mailitem.subject;
                }

                emailbody = emailbody.Replace("@@ReceiverName", objCompanysend.Name).Replace("@@SenderCompanyName", objCompanysend.SenderCompanyName);
            }
            EmailUtility.SendMailInThread(objCompanysend.Email, Subject, emailbody);
        }


        public List<vendorModel> GetMyVendor(VendorFilterModal model)
        {

            vendorModel obj = new vendorModel();
            List<vendorModel> objVM;

            using (var con = new SqlConnection(ConnectionString))
            {

                objVM = con.Query<vendorModel>("Usp_GetMyVendorCompanyProfile", new
                {
                    Name = model.Name,
                    CompanyId = model.CompanyId,
                    IsApproved = model.IsApproved,
                    VendorId = model.VendorId,
                    //Id = model.Id,
                    VendorCompanyId = model.VendorCompanyId
                }, commandType: CommandType.StoredProcedure).ToList();

            }

            return objVM;

        }

        public ProjectModel.NewCreateProjectModel GetProjectAndVendorDetail(int ProjectId, int CompanyId)
        {
            NewCreateProjectModel ObjProj;
            List<ProjectPhaseModel> ProjPhaseList = new List<ProjectPhaseModel>();
            List<ProjectMileStoneModel> ProjMilestoneList;
            List<PhaseDocumentsNameModel> PhaseDocList;
            List<VendorForCompany> VendorList;

            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("Usp_GetProjectAndVendorDetail", new { ProjectId = ProjectId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                ObjProj = Result.Read<NewCreateProjectModel>().FirstOrDefault();
                ProjPhaseList = Result.Read<ProjectPhaseModel>().ToList();
                ProjMilestoneList = Result.Read<ProjectMileStoneModel>().ToList();
                PhaseDocList = Result.Read<PhaseDocumentsNameModel>().ToList();
                VendorList = Result.Read<VendorForCompany>().ToList();
                ObjProj.PhaseList = new List<ProjectPhaseModel>();


                foreach (var item in ProjPhaseList)
                {
                    // phaseList Documenmt list
                    item.PhaseDocumentsName = new List<PhaseDocumentsNameModel>();
                    // loop phase document list 
                    List<PhaseDocumentsNameModel> dList = PhaseDocList.Where(x => x.PhaseId == item.PhaseId).ToList();
                    foreach (PhaseDocumentsNameModel obj in dList)
                    {
                        item.PhaseDocumentsName.Add(obj);
                    }

                    // loop on milestones
                    item.MileStone = new List<ProjectMileStoneModel>();
                    List<ProjectMileStoneModel> mList = ProjMilestoneList.Where(x => x.PhaseId == item.PhaseId).ToList();
                    foreach (ProjectMileStoneModel obj in mList)
                    {
                        item.MileStone.Add(obj);
                    }

                }
                // creating the vendorlist
                ObjProj.VendorList = VendorList;
                ObjProj.PhaseList = ProjPhaseList;


            }
            return ObjProj;
        }


        public List<vendorModel> GetProjectTOQuote(vendorModel Modal)
        {

            List<vendorModel> vendorModelList;

            using (var con = new SqlConnection(ConnectionString))
            {
                vendorModelList = con.Query<vendorModel>("Usp_GetProjectTOQuoteVendorProfile",
                    new
                    {
                        CompanyId = Modal.CompanyId,
                        Id = Modal.Id,
                        Name = Modal.Name,
                        VendorId = Modal.VendorId,
                        ProjectId = Modal.ProjectId,
                        UserId = Modal.UserId,
                        PageIndex = Modal.PageIndex,
                        PageSize = Modal.PageSize,
                    }, commandType: CommandType.StoredProcedure).ToList();
            }

            return vendorModelList;
        }

        public VendorQuoteModal GetProjectDetailForVendor(RequestProjectDetailforVendorModal Modal)
        {

            VendorQuoteModal ObjVendorQuoteModal = new VendorQuoteModal();
            List<ProjectDocument> ProjectDocList;
            List<vendorPhaseModal> ProjPhaseList;
            List<PhaseDocument> PhaseDocList;
            List<vendorMilestone> ProjMilestoneList;
            List<ResourceModal> ObjResourceList;
            CustomControlValueModel ObjCustomControlValueList;
            List<CustomControlUIModel> ObjCustomControlUIList;


            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetProjectDetailForVendorProfile",
                    new { ProjectId = Modal.ProjectId, CompanyId = Modal.CompanyId, UserId = Modal.UserId, VendorId = Modal.VendorId }, commandType: CommandType.StoredProcedure);
                ObjVendorQuoteModal = Result.Read<VendorQuoteModal>().FirstOrDefault();
                ProjectDocList = Result.Read<ProjectDocument>().ToList();
                ProjPhaseList = Result.Read<vendorPhaseModal>().ToList();
                PhaseDocList = Result.Read<PhaseDocument>().ToList();
                ProjMilestoneList = Result.Read<vendorMilestone>().ToList();
                ObjResourceList = Result.Read<ResourceModal>().ToList();
                ObjCustomControlUIList = Result.Read<CustomControlUIModel>().ToList();
                ObjCustomControlValueList = Result.Read<CustomControlValueModel>().FirstOrDefault();
                foreach (vendorPhaseModal item in ProjPhaseList)
                {

                    List<PhaseDocument> PdocList = PhaseDocList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.PhaseDocumentList = new List<PhaseDocument>();
                    foreach (PhaseDocument item1 in PdocList)
                    {
                        item.PhaseDocumentList.Add(item1);
                    }
                    List<vendorMilestone> PMilestoneList = ProjMilestoneList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.MileStone = new List<vendorMilestone>();
                    foreach (vendorMilestone item2 in PMilestoneList)
                    {
                        item.MileStone.Add(item2);
                    }
                    int i = 0;
                    foreach (CustomControlUIModel item3 in ObjCustomControlUIList)
                    {
                        List<CustomUIDropDownModel> ObjCustDrop = new List<CustomUIDropDownModel>();
                        List<CustomUIRadioButtonModel> ObjRadioList = new List<CustomUIRadioButtonModel>();
                        if (item3.Values != null && item3.ControlType == "Dropdown")
                        {
                            string[] drop = item3.Values.Split(',');
                            int j = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIDropDownModel obj = new CustomUIDropDownModel();
                                obj.Name = item1;
                                obj.Id = j;
                                ObjCustDrop.Add(obj);
                                j++;
                            }
                        }
                        if (item3.Values != null && item3.ControlType == "Radio buttons")
                        {
                            string[] drop = item3.Values.Split(',');
                            int k = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIRadioButtonModel obj = new CustomUIRadioButtonModel();
                                obj.Name = item1;
                                obj.Id = k;
                                ObjRadioList.Add(obj);
                                k++;
                            }
                        }

                        ObjCustomControlUIList[i].FieldModel = ObjCustomControlUIList[i].FieldName.Replace(' ', '_');
                        ObjCustomControlUIList[i].CustomDropDownList = ObjCustDrop;
                        ObjCustomControlUIList[i].CustomRadioButtonList = ObjRadioList;
                        i++;

                    }
                }
            }
            ObjVendorQuoteModal.ProjectDocumentList = ProjectDocList;
            ObjVendorQuoteModal.PhaseList = ProjPhaseList;
            ObjVendorQuoteModal.ResourcesList = ObjResourceList;
            ObjVendorQuoteModal.CustomControlValueModel = ObjCustomControlValueList;
            ObjVendorQuoteModal.CustomControlUIList = ObjCustomControlUIList;
            return ObjVendorQuoteModal;

        }

        public List<UserRoleModel> GetVendorResourceRole()
        {

            List<UserRoleModel> UserRoleList;


            using (var con = new SqlConnection(ConnectionString))
            {
                string query = "Select RoleId ,Name from Roles where  RoleId > 3";//where  RoleId > 5 or RoleId = 2
                UserRoleList = con.Query<UserRoleModel>(query).ToList();
            }

            return UserRoleList;
        }


        public int SaveQuoteAcceptByvendor(VendorQuoteModal modal)
        {

            string MileIdList = "";
            int Id = 1;
            List<VendorPhaseCustom> objItemList = new List<VendorPhaseCustom>();
            VendorCompanyMail VendorCompanyMailList;

            foreach (var item in modal.PhaseList)
            {
                VendorPhaseCustom Obj = new VendorPhaseCustom();

                Obj.Id = Id;
                Obj.PhaseId = item.PhaseId;
                Obj.Budget = item.PhaseVendorBudget;
                Obj.BudgetHour = item.PhaseVendorHour;


                foreach (var item1 in item.MileStone)
                {
                    Obj.MilestoneMileIdList += Convert.ToString(item1.MilestoneId == 0 ? item1.MilestoneId = 0 : item1.MilestoneId) + ',';
                    Obj.MilestoneBudgetList += Convert.ToString(item1.MilestoneVendorBudget == 0 ? item1.MilestoneVendorBudget = 0 : item1.MilestoneVendorBudget) + ',';
                    Obj.MilestoneHourList += Convert.ToString(item1.MilestoneVendorHour == 0 ? item1.MilestoneVendorHour = 0 : item1.MilestoneVendorHour) + ',';
                }
                Id++;

                objItemList.Add(Obj);
            }

            string RoleIdList = "";
            string FirstNameList = "";
            string LastNameList = "";
            string ProfileList = "";
            string HourlyList = "";
            string ExpectedHoursList = "";
            foreach (var item in modal.ResourcesList)
            {

                FirstNameList += Convert.ToString(item.FirstName) + ',';
                RoleIdList += Convert.ToString(item.RoleId) + ',';
                LastNameList += Convert.ToString(item.LastName) + ',';
                ProfileList += Convert.ToString(item.Profile) + ',';
                HourlyList += Convert.ToString(item.HourlyRate == null ? 0 : item.HourlyRate) + ',';
                ExpectedHoursList += Convert.ToString(item.ExpectedHours == null ? 0 : item.ExpectedHours) + ',';

            }

            VendorQuoteModal ObjVendorModal = new VendorQuoteModal();
            using (var con = new SqlConnection(ConnectionString))
            {

                VendorCompanyMailList = con.Query<VendorCompanyMail>("Usp_SaveQuoteAcceptByvendor", new
                {
                    VendorCompanyId = modal.VendorId,
                    UserId = modal.UserId,
                    ProjectId = modal.ProjectId,
                    TotalBudget = modal.TotalBudget,
                    TotalHours = modal.TotalHours,
                    AllDescription = modal.AllDescription,
                    FirstNameList = FirstNameList,
                    RoleIdList = RoleIdList,
                    LastNameList = LastNameList,
                    ProfileList = ProfileList,
                    HourlyList = HourlyList,
                    ExpectedHoursList = ExpectedHoursList,
                    PhaseList = objItemList.AsTableValuedParameter("ProjectVendorPhaseList", new[]
                    { "Id", "PhaseId", "Budget", "BudgetHour",  "MilestoneMileIdList", "MilestoneBudgetList", "MilestoneHourList",})
                },

                commandType: CommandType.StoredProcedure).FirstOrDefault();

            }


            if (VendorCompanyMailList != null)
            {



                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForionSaveQuoteAcceptByvendor(VendorCompanyMailList);
                }
                ));
                childref.Start();
                Id = VendorCompanyMailList.Id;
            }
            return Id;
        }


        // sending mail for registration
        private void ThreadForionSaveQuoteAcceptByvendor(VendorCompanyMail VendorCompanyMailList)
        {

            String emailbody = null;
            string Subject = string.Empty;

            string subjectBysender = VendorCompanyMailList.Name + " | Quote from " + VendorCompanyMailList.SenderName;
            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendToProjectOwnerSaveQuoteAcceptByvendor")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@subjectBysender", subjectBysender).Replace("@@ReceiverName", VendorCompanyMailList.ReceiverName).Replace("@@SenderName", VendorCompanyMailList.SenderName)
                .Replace("@@SenderCompanyName", VendorCompanyMailList.SenderCompanyName).Replace("@@Name", VendorCompanyMailList.Name);
            EmailUtility.SendMailInThread(VendorCompanyMailList.Email, subjectBysender, emailbody);


        }


        public int EditSaveQuoteAcceptByvendor(VendorQuoteModal modal)
        {

            string MileIdList = "";
            int Id = 1;
            List<VendorPhaseCustom> objItemList = new List<VendorPhaseCustom>();
            List<ProjectMileStoneModel> MiestoneList = new List<ProjectMileStoneModel>();

            VendorCompanyMail VendorCompanyMailList;

            foreach (var item in modal.PhaseList)
            {
                VendorPhaseCustom Obj = new VendorPhaseCustom();

                Obj.Id = item.Id;
                Obj.PhaseId = item.PhaseId;
                Obj.Budget = item.PhaseVendorBudget;
                Obj.BudgetHour = item.PhaseVendorHour;


                foreach (var item1 in item.MileStone)
                {
                    ProjectMileStoneModel Obj1 = new ProjectMileStoneModel();
                    Obj1.ProjectId = modal.ProjectId;
                    Obj1.PhaseId = item1.PhaseId;
                    Obj1.Name = item1.Name;
                    Obj1.Description = item1.Description;
                    Obj1.EstimatedHours = item1.MilestoneVendorHour;
                    Obj1.Budget = item1.MilestoneVendorBudget;
                    Obj1.MilestoneId = item1.MilestoneId;
                    Obj1.TrackId = 0;
                    Obj1.Id = item1.Id;
                    MiestoneList.Add(Obj1);
                }
                Id++;

                objItemList.Add(Obj);
            }


            string RoleIdList = "";
            string FirstNameList = "";
            string LastNameList = "";
            string ProfileList = "";
            string HourlyList = "";
            string ExpectedHoursList = "";
            foreach (var item in modal.ResourcesList)
            {

                FirstNameList += Convert.ToString(item.FirstName) + ',';
                RoleIdList += Convert.ToString(item.RoleId) + ',';
                LastNameList += Convert.ToString(item.LastName) + ',';
                ProfileList += Convert.ToString(item.Profile) + ',';
                HourlyList += Convert.ToString(item.HourlyRate == null ? 0 : item.HourlyRate) + ',';
                ExpectedHoursList += Convert.ToString(item.ExpectedHours == null ? 0 : item.ExpectedHours) + ',';

            }

            VendorQuoteModal ObjVendorModal = new VendorQuoteModal();
            using (var con = new SqlConnection(ConnectionString))
            {

                VendorCompanyMailList = con.Query<VendorCompanyMail>("Usp_EditSaveQuoteAcceptByvendor", new
                {

                    ProjectId = modal.ProjectId,
                    VendorId = modal.VendorId,
                    TotalBudget = modal.TotalBudget,
                    TotalHours = modal.TotalHours,
                    AllDescription = modal.AllDescription,
                    FirstNameList = FirstNameList,
                    RoleIdList = RoleIdList,
                    LastNameList = LastNameList,
                    ProfileList = ProfileList,
                    HourlyList = HourlyList,
                    ExpectedHoursList = ExpectedHoursList,
                    PhaseList = objItemList.AsTableValuedParameter("ProjectVendorPhaseList", new[]
                    { "Id", "PhaseId", "Budget", "BudgetHour",  "MilestoneMileIdList", "MilestoneBudgetList", "MilestoneHourList",}),
                    MiestoneList = MiestoneList.AsTableValuedParameter("PhaseMilestoneList", new[] { "TrackId", "ProjectId", "Id", "Name", "PhaseId", "MilestoneId",
                      "Budget",  "EstimatedHours", "Description"})
                },

                commandType: CommandType.StoredProcedure).FirstOrDefault();

            }


            if (VendorCompanyMailList != null)
            {



                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForionSaveQuoteAcceptByvendor(VendorCompanyMailList);
                }
                ));
                childref.Start();
                Id = VendorCompanyMailList.Id;
            }
            return Id;
        }


        public int RejectQuoteByVendor(VendorInvitationsModal modal)
        {
            int Id = 1;
            vendorModel obj = new vendorModel();
            List<vendorModel> objVM;
            VendorCompanyMail ObjVendorCompanyMail;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"update  VendorInvitations set IsRejectedByVendor = 1,
                                 ReasonByVendor = @ReasonByVendor where UserId = @UserId and ProjectId = @ProjectId
                                  Update ProjectVendor set IsProjectUpdated = 0 where ProjectId = @ProjectId and VendorId = @VendorId
                                  select p.Name ProjectName 
                                 ,CONCAT(Ud.FirstName+ ' ',Ud.LastName ) ReceiverName
                                 ,Ud.Email Email
                                 ,(select CONCAT(Ud.FirstName + ' ',Ud.LastName) SenderName from UserDetails Ud where UserId = @UserId) SenderName
                                 from Projects P
                                inner join Companies C on C.Id = P.CompanyId 
                                inner join UserDetails Ud on Ud.CompanyId = P.CompanyId where P.Id = @ProjectId";
                ObjVendorCompanyMail = con.Query<VendorCompanyMail>(query, new { ReasonByVendor = modal.ReasonReject, ProjectId = modal.ProjectId, UserId = modal.UserId, VendorId = modal.VendorId }).SingleOrDefault();


                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForSaveQuoteRejectByvendor(ObjVendorCompanyMail, modal.ReasonReject);
                }

                ));
                childref.Start();

            }
            return Id;



        }

        private void ThreadForSaveQuoteRejectByvendor(VendorCompanyMail VendorCompanymodal, string ReasonReject)
        {

            String emailbody = null;
            string Subject = string.Empty;
            string subjectBysender = string.Empty;
            subjectBysender = VendorCompanymodal.SenderName + " | " + VendorCompanymodal.ProjectName + " | REJECTED";
            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendToProjectOwnerQuoteRejectByvendor")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 Subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.Subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", VendorCompanymodal.ReceiverName).Replace("@@SenderName", VendorCompanymodal.SenderName)
               .Replace("@@ProjectName", VendorCompanymodal.ProjectName).Replace("@@ReasonReject", ReasonReject).Replace("@@subjectBysender", subjectBysender);
            EmailUtility.SendMailInThread(VendorCompanymodal.Email, subjectBysender, emailbody);


        }

        public VendorQuoteModal GetAwardedProjectViewDetail(int ProjectId, int VendorId)
        {

            int CompanyId = 0;  // Not in use 
            VendorQuoteModal ObjVendorQuote = new VendorQuoteModal();
            List<ProjectDocument> ObjProjectDocList;
            List<vendorPhaseModal> ObjvendorPhaseList;
            List<PhaseDocument> ObjPhaseDocumentList;
            List<vendorMilestone> ObjvendorMileList;
            List<ResourceModal> ObjResourceList;
            List<VendorProjectProgress> ObjProjectProgress;
            CustomControlValueModel ObjCustomControlValueList;
            List<CustomControlUIModel> ObjCustomControlUIList;


            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetQuotedProjectForVendorProfile", new { ProjectId = ProjectId, VendorId = VendorId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                ObjVendorQuote = Result.Read<VendorQuoteModal>().FirstOrDefault();
                ObjProjectDocList = Result.Read<ProjectDocument>().ToList();
                ObjvendorPhaseList = Result.Read<vendorPhaseModal>().ToList();
                ObjPhaseDocumentList = Result.Read<PhaseDocument>().ToList();
                ObjvendorMileList = Result.Read<vendorMilestone>().ToList();
                ObjResourceList = Result.Read<ResourceModal>().ToList();
                ObjProjectProgress = Result.Read<VendorProjectProgress>().ToList();
                ObjCustomControlUIList = Result.Read<CustomControlUIModel>().ToList();
                ObjCustomControlValueList = Result.Read<CustomControlValueModel>().FirstOrDefault();

                foreach (vendorPhaseModal item in ObjvendorPhaseList)
                {

                    List<PhaseDocument> ObjPhasedocList = ObjPhaseDocumentList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.PhaseDocumentList = new List<PhaseDocument>();
                    foreach (PhaseDocument item1 in ObjPhasedocList)
                    {
                        item.PhaseDocumentList.Add(item1);

                    }

                    List<vendorMilestone> PMilestoneList = ObjvendorMileList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.MileStone = new List<vendorMilestone>();
                    foreach (vendorMilestone item2 in PMilestoneList)
                    {
                        item.MileStone.Add(item2);
                    }

                    int i = 0;
                    foreach (CustomControlUIModel item3 in ObjCustomControlUIList)
                    {
                        List<CustomUIDropDownModel> ObjCustDrop = new List<CustomUIDropDownModel>();
                        List<CustomUIRadioButtonModel> ObjRadioList = new List<CustomUIRadioButtonModel>();
                        if (item3.Values != null && item3.ControlType == "Dropdown")
                        {
                            string[] drop = item3.Values.Split(',');
                            int j = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIDropDownModel obj = new CustomUIDropDownModel();
                                obj.Name = item1;
                                obj.Id = j;
                                ObjCustDrop.Add(obj);
                                j++;
                            }
                        }
                        if (item3.Values != null && item3.ControlType == "Radio buttons")
                        {
                            string[] drop = item3.Values.Split(',');
                            int k = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIRadioButtonModel obj = new CustomUIRadioButtonModel();
                                obj.Name = item1;
                                obj.Id = k;
                                ObjRadioList.Add(obj);
                                k++;
                            }
                        }

                        ObjCustomControlUIList[i].FieldModel = ObjCustomControlUIList[i].FieldName.Replace(' ', '_');
                        ObjCustomControlUIList[i].CustomDropDownList = ObjCustDrop;
                        ObjCustomControlUIList[i].CustomRadioButtonList = ObjRadioList;
                        i++;

                    }
                }
            }

            ObjVendorQuote.PhaseList = ObjvendorPhaseList;
            ObjVendorQuote.ProjectDocumentList = ObjProjectDocList;
            ObjVendorQuote.ResourcesList = ObjResourceList;
            ObjVendorQuote.VendorProjectProgressList = ObjProjectProgress;
            ObjVendorQuote.CustomControlValueModel = ObjCustomControlValueList;
            ObjVendorQuote.CustomControlUIList = ObjCustomControlUIList;
            return ObjVendorQuote;

        }


        public InvoiceRaiseToProject GetSpecificProjectForInvoiceRaise(int ProjectId, int VendorId)
        {
            InvoiceRaiseToProject ObjInvoiceRaiseToProject = new InvoiceRaiseToProject();
            List<InvoiceRaiseToProject> ObjInvoiceRaiseToProjectList;

            List<InvoiceRaiseToCompanyModal> ObjInvoiceRaiseToCompanyList;
            List<InvoiceToPhaseModal> ObjInvoiceToPhaseList;
            List<InvoiceForResourceModal> ObjInvoiceForResourceList;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = string.Empty;

                if (ProjectId == 0)
                {
                    query = "Usp_GetProjectForInvoiceRaise";
                    var Result = con.QueryMultiple(query, new { ProjectId = ProjectId, VendorId = VendorId }, commandType: CommandType.StoredProcedure);
                    ObjInvoiceRaiseToCompanyList = Result.Read<InvoiceRaiseToCompanyModal>().ToList();


                }
                else
                {
                    query = "Usp_GetProjectForInvoiceRaise";
                    var Result = con.QueryMultiple(query, new { ProjectId = ProjectId, VendorId = VendorId }, commandType: CommandType.StoredProcedure);
                    ObjInvoiceRaiseToProjectList = Result.Read<InvoiceRaiseToProject>().ToList();
                    ObjInvoiceRaiseToCompanyList = Result.Read<InvoiceRaiseToCompanyModal>().ToList();
                    ObjInvoiceToPhaseList = Result.Read<InvoiceToPhaseModal>().ToList();
                    ObjInvoiceForResourceList = Result.Read<InvoiceForResourceModal>().ToList();
                    ObjInvoiceRaiseToProject.InvoiceRaiseToProjectList = ObjInvoiceRaiseToProjectList;
                    ObjInvoiceRaiseToProject.InvoiceToPhaseList = ObjInvoiceToPhaseList;
                    ObjInvoiceRaiseToProject.InvoiceRaiseToCompanyList = ObjInvoiceRaiseToCompanyList;
                    ObjInvoiceRaiseToProject.InvoiceForResourceList = ObjInvoiceForResourceList;

                }

            }
            ObjInvoiceRaiseToProject.InvoiceRaiseToCompanyList = ObjInvoiceRaiseToCompanyList;



            return ObjInvoiceRaiseToProject;

        }
        public List<InvoiceToMilestoneModal> GetMilestoneforProject(int Id)
        {
            List<InvoiceToMilestoneModal> ObjInvoiceToMilestoneList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select Pms.Id Id,Pms.Name Name from  ProjectMileStones Pms where Pms.PhaseId = @Id";
                var Result = con.QueryMultiple(query, new { Id = Id });
                ObjInvoiceToMilestoneList = Result.Read<InvoiceToMilestoneModal>().ToList();
            }
            return ObjInvoiceToMilestoneList;

        }

        public List<InvoiceRaiseToCompanyModal> ProjectForInvoiceDrop(int CompanyId)
        {

            List<InvoiceRaiseToCompanyModal> ObjInvoiceToMilestoneList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"Select P.Id,P.Name from Projects P where P.CompanyId = @CompanyId";
                var Result = con.QueryMultiple(query, new { Id = CompanyId });
                ObjInvoiceToMilestoneList = Result.Read<InvoiceRaiseToCompanyModal>().ToList();
            }
            return ObjInvoiceToMilestoneList;

        }


        public List<VendorFilterModal> GetCompanyVendorDropList(int CompanyId)
        {

            List<VendorFilterModal> ObjInvoiceToMilestoneList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select distinct C.Name,C.Id VendorCompanyId from  VendorInvitations vI
                                  inner join UserDetails ud on vI.UserId = ud.UserId
                                  inner join Companies C on C.Id = ud.CompanyId
                                  where vI.CompanyId = @CompanyId and vI.ProjectId = 0 and  InvitationType = 0";
                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId });
                ObjInvoiceToMilestoneList = Result.Read<VendorFilterModal>().ToList();
            }
            return ObjInvoiceToMilestoneList;

        }


        public List<CompanyModal> GetCompanyForDrop(int CompanyId)
        {

            List<CompanyModal> ObjCompanyModalList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select C.Name Name,C.Id CompanyId from CompanyVendor CV inner join Companies C on CV.CompanyId = C.Id
                        where CV.VendorId = @CompanyId";
                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId });
                ObjCompanyModalList = Result.Read<CompanyModal>().ToList();
            }
            return ObjCompanyModalList;

        }


        public List<VendorFilterModal> GetVendorName(VendorFilterModal Modal)
        {

            List<VendorFilterModal> ObjVendorModalList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select CONCAT(ud.FirstName +' ' ,ud.LastName) Name,ud.Id VendorId  from  VendorInvitations vI
                                  inner join UserDetails ud on vI.UserId = ud.UserId
                                  inner join Companies C on C.Id = ud.CompanyId    where vI.CompanyId = @CompanyId
                                  and vI.ProjectId = 0 and  InvitationType = 0 and ud.FirstName Like @Name  Or ud.LastName like @Name";
                var Result = con.QueryMultiple(query, new { CompanyId = Modal.CompanyId, Name = "%" + Modal.Name + "%" });
                ObjVendorModalList = Result.Read<VendorFilterModal>().ToList();
            }
            return ObjVendorModalList;

        }
        public List<VendorFilterModal> GetVendorCompanyName(VendorFilterModal Modal)
        {

            List<VendorFilterModal> ObjVendorModalList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select (select Name from companies where id=c.vendorid) Name,c.Id VendorId  from  CompanyVendor c
                                  inner join Companies c1 on c1.Id = c.CompanyId    where c.CompanyId = @CompanyId
                                  and (select Name from companies where id=c.vendorid) Like @Name and c.IsDeleted <> 1";
                var Result = con.QueryMultiple(query, new { CompanyId = Modal.CompanyId, Name = "%" + Modal.Name + "%" });
                ObjVendorModalList = Result.Read<VendorFilterModal>().ToList();
            }
            return ObjVendorModalList;

        }


        public List<VendorDetailModal> GetVendorDetail(int CompanyId, int VendorId)
        {
            List<VendorDetailModal> ObjVendorDetailList = new List<VendorDetailModal>();
            List<VendorFeedbackModal> ObjVendorFeedbackList;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select P.Name ProjectName,CONCAT(ud.FirstName+ ' ',ud.LastName) VendorName, PVQ.Hours BudgetedHours
                                ,(select sum(ISNULL( convert (Int ,I.TotalHours),0)) from Invoices I  where I.ProjectId = PVQ.ProjectId and I.IsRejected = 0 and I.InvoiceNumber LIKE 'INV-V%' ) HourSpent
                                ,(select sum(ISNULL( convert (Int ,I.TotalAmount),0)) from Invoices I  where I.ProjectId = PVQ.ProjectId and I.IsRejected = 0 and I.InvoiceNumber LIKE 'INV-V%' ) Amount
                                ,ISNULL('Uploads/Profile/Thumbnail/' + ud.ProfilePhoto, 'Uploads/Default/profile.png') ProfilePhoto 
                                ,P.IsComplete
                                 from  Projects P inner join ProjectVendorQuote PVQ on P.Id  = PVQ.ProjectId 
                                 inner join UserDetails ud on ud.Id = @VendorId
                               	 where VendorId  = @VendorId and P.CompanyId = @CompanyId and P.IsComplete = 3

                               	  select  Pv.CompanyFeedback ,P.Name    from  ProjectVendor Pv inner join  CompanyVendor Cv
									  on  Pv.VendorId = Cv.VendorId  Inner join Projects P on P.Id = Pv.ProjectId
									   where Cv.CompanyId = @CompanyId and  Cv.VendorId = @VendorId
									   and Pv.VendorId = @VendorId and Pv.IsHired = 1 and Pv.IsActive = 1
									   and P.IsComplete = 3"
;

                var Result = con.QueryMultiple(query, new { VendorId = VendorId, CompanyId = CompanyId });
                ObjVendorDetailList = Result.Read<VendorDetailModal>().ToList();

                if (ObjVendorDetailList.Count == 0)
                {
                    return ObjVendorDetailList;
                }

                ObjVendorFeedbackList = Result.Read<VendorFeedbackModal>().ToList();

                ObjVendorDetailList[0].VendorFeedbackList = ObjVendorFeedbackList;



            }
            return ObjVendorDetailList;


        }


        public List<VendorCompanyPocModal> GetVendorCompanyPoc(int VendorCompanyId)
        {
            List<VendorCompanyPocModal> ObjVendorCompPoc;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select CONCAT(ud.FirstName + ' ', Ud.LastName )  VendorPocName
                                , (STUFF((select Distinct ', ' + P.Name  from Projects P
                                   Inner
                                  join ProjectVendor PV on P.Id = PV.ProjectId where IsHired = 1  and VendorId = Ud.Id
                                    FOR XML PATH('')), 1, 2, '')) Project
                                ,ISNULL('Uploads/Profile/Thumbnail/' + ud.ProfilePhoto, 'Uploads/Default/profile.png') ProfilePhoto
                                ,Ud.Email Email
                                , (select CASE WHEN(Avg(pv.Rating) - CAST(Avg(pv.Rating) as int)) > .50
                                    THEN ROUND(AVG(Pv.Rating), 0)
                                     WHEN(Avg(pv.Rating) - CAST(Avg(pv.Rating) as int)) < .50
                                    THEN ROUND(AVG(Pv.Rating), 0) ELSE  Avg(pv.Rating)  END
                                    from ProjectVendor Pv where Pv.VendorId = Ud.Id) Rating
								,(select Avg(pv.Rating)  from ProjectVendor Pv where Pv.VendorId = Ud.Id) Rating1
                                    from Companies c
                                inner join UserDetails Ud  on c.Id = Ud.CompanyId
                                where Ud.CompanyId = @VendorCompanyId and ud.RoleId <> 1";

                var Result = con.QueryMultiple(query, new { VendorCompanyId = VendorCompanyId });
                ObjVendorCompPoc = Result.Read<VendorCompanyPocModal>().ToList();

            }
            return ObjVendorCompPoc;

        }


        public CompanyModel GetSelectedVendorDetail(int VendorCompanyId)
        {
            CompanyModel ObjVendorCompPoc;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select Id CompanyId,Name CompanyName,Email CompanyEmail,PhoneNo CompanyPhoneNo,
                                Website CompanyWebsite,IsVendorFirmRegistered CompanyIsVendorFirmRegistered
                                    from Companies c
                                where c.Id = @VendorCompanyId and IsActive= 1 ";

                var Result = con.QueryMultiple(query, new { VendorCompanyId = VendorCompanyId });
                ObjVendorCompPoc = Result.Read<CompanyModel>().FirstOrDefault();

            }
            return ObjVendorCompPoc;
        }


        public string deleteVendorDetail(int Id)
        {
            string msg = string.Empty;
            int ResponseId;

            using (var con = new SqlConnection(ConnectionString))

            {

                ResponseId = con.Query<int>("Usp_deleteVendorDetail", new
                {

                    Id = Id,


                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }

            if (ResponseId == -1)
            {
                msg = "ProjectPresentInVendorDetail";
            }
            if (ResponseId == 1)
            {
                msg = "VendorDetailDeletedSuccessfully";
            }
            //dbcontext = new AppContext();
            //string msg = string.Empty;
            //var AllProjects = (from p in dbcontext.Projects
            //                   where p.TeamId == TeamId && p.IsDeleted == false
            //                   select p.Id).ToList();

            //if (AllProjects.Count() > 0)
            //{
            //    msg = "ProjectPresentInTeam";
            //}
            //else
            //{
            //    TIGHTEN.ENTITY.Team teamEntity = (from m in dbcontext.Teams where m.Id == TeamId && m.IsDeleted == false select m).SingleOrDefault();

            //    teamEntity.IsDeleted = true;
            //    dbcontext.SaveChanges();
            //    msg = "TeamDeletedSuccessfully";
            //}

            return msg;
        }

        public CompanyModel GetSelectedVendorDetailById(int Id)
        {
            CompanyModel ObjVendorCompPoc;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select c.Id Id, c.CompanyId,c.VendorId, (select top 1 Name from Companies where id=c.vendorId) CompanyName,c.ContactEmail CompanyEmail,c.ContactPhoneNo CompanyPhoneNo,
                                c.ContactWebsite CompanyWebsite,c1.IsVendorFirmRegistered CompanyIsVendorFirmRegistered,c.ContactFirstName,
(select IsVendorFirmRegistered from companies where id=c.VendorId ) as VendorCompanyIsVendorFirmRegistered,
                                c.ContactLastName
                                    from CompanyVendor c inner join Companies c1
                                    on c1.Id=c.CompanyId 
                                where c.Id = @Id and c.IsActive= 1 and c.IsDeleted <> 1 ";

                var Result = con.QueryMultiple(query, new { Id = Id });
                ObjVendorCompPoc = Result.Read<CompanyModel>().FirstOrDefault();
                if (ObjVendorCompPoc != null)
                {
                    RoleModel objRole = GetPOCRole();

                    int roleId = 0;
                    if (objRole != null)
                    {
                        roleId = objRole.RoleId;
                    }
                    query = @"select c.Id Id,c.UserId,c.RoleId, c.CompanyId,u.FirstName as pocFirstName,u.LastName as pocLastName,u.Email as pocEmail
                                    from UserCompanyRoles c inner join userdetails u on c.UserId=u.UserId
                                where c.CompanyId = @CompanyId and c.RoleId= @RoleId and c.IsDeleted <> 1 and c.userid in (select userid from userdetails where companyid=@VendorId and IsActive=1 and IsDeleted<>1 )";

                    var POCResult = con.QueryMultiple(query, new { CompanyId = ObjVendorCompPoc.CompanyId, RoleId = roleId,VendorId= ObjVendorCompPoc.VendorId });
                   List<VendorPOCModel> ObjUsrCompRole = POCResult.Read<VendorPOCModel>().ToList();
                    ObjVendorCompPoc.VendorPOCList = ObjUsrCompRole;
                }
            }
            return ObjVendorCompPoc;
        }
        public RoleModel GetPOCRole()
        {
            RoleModel objRole = new RoleModel();
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"SELECT RoleId FROM Roles  where [Name] = 'POC'";
                objRole = con.Query<RoleModel>(query, null).FirstOrDefault();
            }
            return objRole;
        }
        public VendorCompanyMail UpdateVendor(VendorInvitationModal modal, string url)
        {
            int Id = 0;
            string msg;

            string VendorUserId = Guid.NewGuid().ToString();
            UserDetail obj = new UserDetail();

            VendorCompanyMail objCompanysend;
            string keyy = UTILITIES.EmailUtility.GetUniqueKey(16);
            using (var con = new SqlConnection(ConnectionString))
            {
                //string pocEmails = "";
                //foreach(var pocObj in modal.VendorPOCList)
                //{
                //    pocEmails += pocObj.pocEmail+",";
                //}
                //if (pocEmails.Length > 0)
                //{
                //    pocEmails = pocEmails.Substring(0, pocEmails.Length - 1);
                //}
                string delpocEmails = "";
                foreach (var pocObj in modal.DeletedVendorPOCList)
                {
                    delpocEmails += pocObj.pocEmail + ",";
                }
                if (delpocEmails.Length > 0)
                {
                    delpocEmails = delpocEmails.Substring(0, delpocEmails.Length - 1);
                }


                var Result = con.QueryMultiple("Usp_UpdateVendor",
                    new
                    {
                        FirstName = modal.FirstName,
                        LastName = modal.LastName,
                        EmaiAddress = modal.EmaiAddress,
                        CompanyId = modal.CompanyId,
                        Website = modal.Website,
                        PhoneNo = modal.PhoneNo,
                        CId = modal.CId
                        //DelPOCEmails= delpocEmails,
                        //POCEmails =pocEmails
                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<VendorCompanyMail>().FirstOrDefault();


                if (objCompanysend.Id != -1)
                {
                    //HttpContext ctx = HttpContext.Current;
                    //Thread childref = new Thread(new ThreadStart(() =>
                    //{
                    //    HttpContext.Current = ctx;
                    //    objCompanysend.UserId = modal.UserId;
                    //    objCompanysend.EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
                    //    ThreadForInvitationVendor(objCompanysend, url);
                    //}
                    //));
                    //childref.Start();
                    msg = "Message Saved";
                    //string query = @"SELECT RoleId FROM Roles  where [Name] = 'POC'";
                    RoleModel objRole = GetPOCRole();// con.Query<RoleModel>(query, null).FirstOrDefault();
                    
                    int roleId = 0;
                    if (objRole != null)
                    {
                        roleId=objRole.RoleId;
                    }
                 
                    var DeleteResult = con.QueryMultiple("Usp_DeleteVendorPOCOfCompany",
                        new
                        {
                            CompanyId = modal.CompanyId,
                            RoleId = roleId,
                            POCEmails = delpocEmails
                        }, commandType: CommandType.StoredProcedure);
                    
                    foreach (var pocObj in modal.VendorPOCList)
                    {
                        string NewUserUserId = Guid.NewGuid().ToString();
                        keyy = UTILITIES.EmailUtility.GetUniqueKey(16);
                        var POCResult = con.QueryMultiple("Usp_SavePOCUser",
                    new
                    {
                        newUserUserId= NewUserUserId,
                        FirstName = pocObj.pocFirstName,
                        LastName = pocObj.pocLastName,
                        Email = pocObj.pocEmail,
                        RoleId=roleId,
                        CompanyId = modal.CompanyId,
                        VendorCompanyId = objCompanysend.VendorCompanyId,
                        EmailConfirmationCode = EmailUtility.GetUniqueKey(8)
                    }, commandType: CommandType.StoredProcedure);

                        VendorCompanyMail objPOCCompanysend = POCResult.Read<VendorCompanyMail>().FirstOrDefault();
                        if(objPOCCompanysend.Id != -1)
                        {
                            string query = @"select * from UserDetails 
                                where UserId = @userid ";
                            var POCVendorResult = con.QueryMultiple(query, new { userid = NewUserUserId });
                            UserModel ObjVendorCompPoc = POCVendorResult.Read<UserModel>().FirstOrDefault();
                            //if (objPOCCompanysend.Id == 1)
                            //{
                            HttpContext ctx1 = HttpContext.Current;
                                Thread childref1 = new Thread(new ThreadStart(() =>
                                {
                                    HttpContext.Current = ctx1;
                                    
                                   // using (var poccon = new SqlConnection(ConnectionString))
                                    //{
                                       
                                        if (ObjVendorCompPoc != null)
                                        {
                                            objPOCCompanysend.UserId = ObjVendorCompPoc.userId;
                                            objPOCCompanysend.EmailConfirmationCode = ObjVendorCompPoc.EmailConfirmationCode;
                                        objPOCCompanysend.Email = ObjVendorCompPoc.email;
                                            ThreadForAddPOC(objPOCCompanysend, url, objPOCCompanysend.Id);
                                        }
                                    //}
                                }
                                ));
                                childref1.Start();
                            //}
                        }
                    }
                }


            }

            return objCompanysend;
        }
        public bool ValidationEmailForPOC(string pocEmail, int CompanyId)
        {
            bool IsValidPOC=true;
            UserModel ObjVendorCompPoc = new UserModel();
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select * from UserDetails 
                                where Email = @pocEmail ";

                var Result = con.QueryMultiple(query, new { pocEmail = pocEmail });
                ObjVendorCompPoc = Result.Read<UserModel>().FirstOrDefault();
                if (ObjVendorCompPoc != null)
                {
                    query = @"select * from usercompanyroles 
                                where CompanyId = @companyId and UserId=@UserId and RoleId=(select roleid from Roles where Name='POC') and IsDeleted <> 1 ";

                    var Result_Role = con.QueryMultiple(query, new { companyId = CompanyId,UserId= ObjVendorCompPoc.userId});
                    if (Result_Role.Read<UserCompanyRole>().FirstOrDefault() != null)
                    {
                        IsValidPOC = false;
                    }
                }

            }
            return IsValidPOC;
        }


        public List<CompanyModel> GetGeneralCompanyListOfVendor(int CompanyId)
        {

            vendorModel obj = new vendorModel();
            List<CompanyModel> objVM;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT (select top 1 Id from CompanyVendor where companyid=CompanyVendor.companyid and vendorid=CompanyVendor.vendorid order by createddate desc) Id,
                               CompanyId,VendorId,(select top 1 [name] from companies where id=CompanyVendor.CompanyId) CompanyName,
                               (select top 1 CreatedDate from CompanyVendor where companyid=CompanyVendor.companyid and vendorid=CompanyVendor.vendorid order by createddate desc) CreatedDate,
                               (select Count(*) from companyvendoraccount where companyid=CompanyVendor.companyid and vendorid=CompanyVendor.vendorid) AccountCount,
                               IsActive, '' ProjectCount FROM CompanyVendor where VendorId = @CompanyId and isactive=1
                                group by  CompanyId,VendorId,isactive";

                objVM = con.Query<CompanyModel>(query, new
                {
                    CompanyId = CompanyId,

                }).ToList();
                foreach (var objec in objVM)
                {
                    if(string.IsNullOrEmpty(objec.CreatedDate)==false)
                    objec.CreatedDate = Convert.ToDateTime(objec.CreatedDate).ToShortDateString();
                }

            }

            return objVM;



        }




        #region InviteNewUserFunctionality

        public List<UserRoleModel> GetNewUserRole(int CompanyId)
        {

            List<UserRoleModel> UserRoleList;


            using (var con = new SqlConnection(ConnectionString))
            {
                string query = "Select RoleId ,Name,Description from Roles where  Type in (1,3) and IsActive=1 and IsDeleted=0 and (CompanyId=@CompanyId or CompanyId=0)";
                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId });
                UserRoleList = Result.Read<UserRoleModel>().ToList();
            }

            return UserRoleList;
        }

        public NewUserMail InviteUser(UserInvitationModal modal, string url)
        {
            string msg = String.Empty;

            StringBuilder UserRoleIdList = new StringBuilder();
            StringBuilder UserRoleNameList = new StringBuilder();
            for (int i = 0; i < modal.UserRoleList.Count; i++)
            {
                UserRoleIdList.Append(modal.UserRoleList[i].RoleId).Append(",");
                UserRoleNameList.Append(modal.UserRoleList[i].Name).Append(",");

            }
            UserRoleNameList.Length--; //to remove last commma from rolename list

            UserDetail obj = new UserDetail();
            NewUserMail objCompanysend;
            string NewUserUserId = Guid.NewGuid().ToString();
            string keyy = UTILITIES.EmailUtility.GetUniqueKey(16);
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_RegisterNewUser",
                    new
                    {
                        NewUserUserId = NewUserUserId,
                        UserId = modal.UserId,
                        FirstName = modal.FirstName,
                        LastName = modal.LastName,
                        CompanyName = modal.CompanyName,
                        EmailAddress = modal.Email,
                        CompanyId = modal.CompanyId,
                        MessageTextBox = modal.MessageTextBox,
                        UserRoleIdList = Convert.ToString(UserRoleIdList),
                        JobProfileId=modal.JobProfileId,
                        EmailConfirmationCode = EmailUtility.GetUniqueKey(8)

                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<NewUserMail>().FirstOrDefault();


                if (objCompanysend.Id != -1)
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForInvitationNewUser(objCompanysend, url, UserRoleNameList,modal.JobProfileName);
                    }
                    ));
                    childref.Start();
                    msg = "Message Saved";

                }


            }

            return objCompanysend;
        }

        // sending mail for registration of new user
        private void ThreadForInvitationNewUser(NewUserMail objCompanysend, string url,StringBuilder UserRoleNameList,String JobProfileName)
        {

            var callbackUrl = url + "#/confirmemail/" + objCompanysend.UserId + "/" + objCompanysend.EmailConfirmationCode + "/" + 1;
            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendToNewUserInvitation")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", objCompanysend.Name).Replace("@@SenderCompanyName", objCompanysend.SenderCompanyName)
                .Replace("@@VendorCompanyName", objCompanysend.VendorCompany).Replace("@@message", objCompanysend.Message)
                .Replace("@@callbackUrl", callbackUrl).Replace("@@RoleName",Convert.ToString(UserRoleNameList)).Replace("@@JobProfileName",String.IsNullOrEmpty(JobProfileName)?".":"and JobProfile is " + JobProfileName+".");

            EmailUtility.SendMailInThread(objCompanysend.Email, Subject, emailbody);


        }


        


        public List<UserRoleModel> GetAllNewUser(int CompanyId)
        {
            List<UserRoleModel> ObjNewUserList = new List<UserRoleModel>();

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select UD.FirstName+' '+UD.LastName AS Name, UD.RoleId,UD.UserId,UD.Id from [dbo].[UserDetails]  UD WHERE UD.IsActive=1 and UD.IsDeleted=0 
                AND( UD.RoleId IS NULL OR UD.RoleId ='') and UD.CompanyId=@CompanyId AND UD.EmailConfirmed=1 AND (UD.Password is not null and UD.Password <> '') AND  (UD.Token is not null and UD.Token <> '') 
";
                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId });
                ObjNewUserList = Result.Read<UserRoleModel>().ToList();

            }
            return ObjNewUserList;


        }

        public string GetCompanyEmailForInviteNewUser(string CompanyName)
        {
            string email = string.Empty;


            using (var con = new SqlConnection(ConnectionString))
            {
                string query = "Select Email from Companies WHERE Name=@CompanyName";
                var Result = con.QueryMultiple(query, new { CompanyName = CompanyName });
                email = Result.Read<String>().FirstOrDefault();
            }

            return email;
        }

        public NewUserMail UpdateExistingInternalUsers(UserInvitationModal modal, string url)
        {
            string msg = String.Empty;
            StringBuilder UserRoleIdList = new StringBuilder();
            StringBuilder UserRoleNameList = new StringBuilder();
            for (int i = 0; i < modal.UserRoleList.Count; i++)
            {
                UserRoleIdList.Append(modal.UserRoleList[i].RoleId).Append(",");
                UserRoleNameList.Append(modal.UserRoleList[i].Name).Append(",");

            }
            UserRoleNameList.Length--; //to remove last commma from rolename list

            UserDetail obj = new UserDetail();
            NewUserMail objCompanysend;
            if(Convert.ToString(modal.ValidUntilDate) == "1/1/0001 12:00:00 AM")
            {
                modal.ValidUntilDate = DateTime.Now;
            }
           using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_UpdateNewUser",
                    new
                    {
                        Id = modal.Id,
                        UserId = modal.UserId,
                        FirstName = modal.FirstName,
                        LastName = modal.LastName,
                        Rating = modal.Rating,
                        CompanyId = modal.CompanyId,
                        UserRoleIdList = Convert.ToString(UserRoleIdList),
                        JobProfileId = modal.JobProfileId,
                        Password = modal.Password,
                        IsActive = modal.IsActive,
                        Indefinitely = modal.Indefinitely,
                        ValidUntil = modal.ValidUntil,
                        ValidUntilDate =modal.ValidUntilDate

                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<NewUserMail>().FirstOrDefault();


                if (objCompanysend.Id != -1)
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForNewUserInformationUpdate(objCompanysend, url, UserRoleNameList, modal.JobProfileName,modal.Rating);
                    }
                    ));
                    childref.Start();
                    msg = "Message Saved";

                }


            }

            return objCompanysend;
        }

        // sending mail for updating new user regarding information updation
        private void ThreadForNewUserInformationUpdate(NewUserMail objCompanysend, string url, StringBuilder UserRoleNameList, String JobProfileName,decimal Rating)
        {

            var callbackUrl = url + "#/confirmemail/" + objCompanysend.UserId + "/" + objCompanysend.EmailConfirmationCode + "/" + 1;
            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendToNewUserInformationUpdate")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", objCompanysend.Name).Replace("@@SenderCompanyName", objCompanysend.SenderCompanyName)
                .Replace("@@VendorCompanyName", objCompanysend.VendorCompany).Replace("@@message", objCompanysend.Message)
                .Replace("@@callbackUrl", callbackUrl).Replace("@@RoleName", Convert.ToString(UserRoleNameList)).Replace("@@JobProfileName", JobProfileName)
                .Replace("@@FirstName", objCompanysend.FirstName).Replace("@@LastName", objCompanysend.LastName).Replace("@@Rating", Convert.ToString(Rating))
                .Replace("@@Password", objCompanysend.Password).Replace("@@Status", (objCompanysend.IsActive) ?"Active":"Inactive")
                .Replace("@@ValidUntilDate", Convert.ToString(objCompanysend.ValidUntilDate));

            EmailUtility.SendMailInThread(objCompanysend.Email, Subject, emailbody);


        }

        public List<CompanyModel> GetCompaniesForVenderCompanyName(string Name)
        {
            List<CompanyModel> venderCompanyList;
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("usp_getCompaniesForVenderCompany",
                    new
                    {
                        CompanyName = Name

                    }, commandType: CommandType.StoredProcedure);

                venderCompanyList = Result.Read<CompanyModel>().ToList();


                


            }

            return venderCompanyList;
        }
        #endregion
    }
}
