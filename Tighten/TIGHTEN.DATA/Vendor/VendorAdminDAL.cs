﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL.VendorAdminModal;
using Dapper;
using System.Data;
using TIGHTEN.MODEL.Vendor.VendorAdminModel;

namespace TIGHTEN.DATA.Vendor
{
  public  class VendorAdminDAL:BaseClass
    {

        public List<AdminProfileVendorInvitationModel> GetInvitationToVendor(FilterVendorInvitationModel Modal)
        {
            List<AdminProfileVendorInvitationModel> ObjVendorInvitation ;
            using (var con = new SqlConnection(ConnectionString))
            {
                

                ObjVendorInvitation = con.Query<AdminProfileVendorInvitationModel>("Usp_GetVendorInvitaionToVendorAdminProfile", 
                    new {
                        VendorCompanyId = Modal.VendorCompanyId,
                        CompanyId = Modal.CompanyId,
                        VendorId =  Modal.VendorId,
                        IsApprovedId = Modal.IsApprovedId,
                        IsApproved = Modal.IsApproved,
                        PageIndex = Modal.PageIndex,
                        PageSize = Modal.PageSize


                    }, commandType: CommandType.StoredProcedure).ToList();

            }

            return ObjVendorInvitation;



        }

        public List<CompanyModel> GetCompanyForInvitaionDropDown(int VendorCompanyId)
        {
            List<CompanyModel> ObjVendorInvitation;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select Distinct
	                                C.Id CompanyId,
	                                C.Name
                                 from VendorInvitations VI
                                inner join UserDetails UD on VI.UserId = UD.UserId
                                inner join Companies C on C.Id = VI.CompanyId
                                where UD.CompanyId = @VendorCompanyId";
                ObjVendorInvitation = con.Query<CompanyModel>(query, new { VendorCompanyId = VendorCompanyId }).ToList();

            }

            return ObjVendorInvitation;



        }


        public List<VendorModel> GetPocsforDrops(int VendorCompanyId)
        {
            List<VendorModel> ObjVendorInvitation;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select CONCAT(UD.FirstName + ' ',UD.LastName) Name,UD.Id VendorId from UserDetails UD	
                                where UD.CompanyId = @VendorCompanyId and UD.RoleId <> 1";
                ObjVendorInvitation = con.Query<VendorModel>(query, new { VendorCompanyId = VendorCompanyId }).ToList();

            }

            return ObjVendorInvitation;



        }

        public List<CompanyModel> GetCompanyForFilterVendorAdminProfile(int VendorCompanyId)
        {

            List<CompanyModel> ObjCompanyModalList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select DISTINCT C.Name Name,C.Id CompanyId from CompanyVendor CV
                            inner join UserDetails UD on UD.Id = CV.VendorId inner join Companies C on CV.CompanyId = C.Id
                             where UD.CompanyId = @VendorCompanyId";
                var Result = con.QueryMultiple(query, new { VendorCompanyId = VendorCompanyId });
                ObjCompanyModalList = Result.Read<CompanyModel>().ToList();
            }
            return ObjCompanyModalList;

        }


        public List<ProjectToQuotevendorModel> GetProjectToQuoteListVendorAdminProfile(ProjectToQuoteFilterModel Model)
        {

            List<ProjectToQuotevendorModel> ObjCompanyModalList;
            using (var con = new SqlConnection(ConnectionString))
            {
         
                var Result = con.QueryMultiple("Usp_GetProjectTOQuoteVendorAdminProfile", 
                    new {
                        VendorCompanyId = Model.VendorCompanyId,
                          CompanyId =   Model.CompanyId,
                          ProjectId  = Model.ProjectId,
                        VendorId = Model.VendorId,
                        StatusId =  Model.StatusId,
                        PageIndex = Model.PageIndex,
                        PageSize = Model.PageSize

                    },commandType:CommandType.StoredProcedure);
                ObjCompanyModalList = Result.Read<ProjectToQuotevendorModel>().ToList();
            }
            return ObjCompanyModalList;

        }


        public List<AutoCompleteProjectToQuoteModel> GetProjectByNameVendorAdminProfile(int VendorCompanyId, string ProjectName)
        {

            List<AutoCompleteProjectToQuoteModel> ObjCompanyModalList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select DISTINCT Prj.Name ProjectName,Prj.Id ProjectId
			   from ProjectVendor Pv Inner Join Projects Prj  on Prj.Id = Pv.ProjectId
                Inner Join Companies C on C.Id = Prj.CompanyId
				inner join UserDetails UD on UD.Id = Pv.VendorId
				inner join VendorInvitations VI on VI.ProjectId = Pv.ProjectId  and VI.UserId = UD.UserId
                left join ProjectVendorQuote PrjVQ on PrjVQ.ProjectId = Prj.Id and PrjVQ.VendorId = Pv.VendorId   
                where  UD.CompanyId = @VendorCompanyId  and Prj.Name LIKE  @ProjectName";
                var Result = con.QueryMultiple(query, new { VendorCompanyId = VendorCompanyId, ProjectName = "%"+ ProjectName + "%" });
                ObjCompanyModalList = Result.Read<AutoCompleteProjectToQuoteModel>().ToList();
            }
            return ObjCompanyModalList;

        }


        public VendorQuoteViewDetailForAdminProfileModal GetProjectToQuoteViewDetailAdminProfile(ProjectToQuoteViewDetailRequestModel Model)
        {
            VendorQuoteViewDetailForAdminProfileModal ObjVendorQuoteModal = new VendorQuoteViewDetailForAdminProfileModal();
            List<ProjectDocument> ProjectDocList;
            List<vendorPhaseModal> ProjPhaseList;
            List<PhaseDocument> PhaseDocList;
            List<vendorMilestone> ProjMilestoneList;
            List<ResourceModal> ObjResourceList;
            List<VendorAdminProfileCustomControlUIModel> ObjCustomControlUIList;
            VendorAdminCustomControlValueModel ObjCustomControlValueList;

            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetProjectDetailForVendorAdmin",
                new { VendorId = Model.VendorId, ProjectId = Model.ProjectId }, commandType: CommandType.StoredProcedure);

                ObjVendorQuoteModal = Result.Read<VendorQuoteViewDetailForAdminProfileModal>().FirstOrDefault();
                ProjectDocList = Result.Read<ProjectDocument>().ToList();
                ProjPhaseList = Result.Read<vendorPhaseModal>().ToList();
                PhaseDocList = Result.Read<PhaseDocument>().ToList();
                ProjMilestoneList = Result.Read<vendorMilestone>().ToList();
                ObjResourceList = Result.Read<ResourceModal>().ToList();
                ObjCustomControlUIList = Result.Read<VendorAdminProfileCustomControlUIModel>().ToList();
                ObjCustomControlValueList = Result.Read<VendorAdminCustomControlValueModel>().FirstOrDefault();





                foreach (vendorPhaseModal item in ProjPhaseList)
                {

                    List<PhaseDocument> PdocList = PhaseDocList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.PhaseDocumentList = new List<PhaseDocument>();
                    foreach (PhaseDocument item1 in PdocList)
                    {
                        item.PhaseDocumentList.Add(item1);
                    }
                    List<vendorMilestone> PMilestoneList = ProjMilestoneList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.MileStone = new List<vendorMilestone>();
                    foreach (vendorMilestone item2 in PMilestoneList)
                    {
                        item.MileStone.Add(item2);
                    }
                    int i = 0;
                    foreach (VendorAdminProfileCustomControlUIModel item3 in ObjCustomControlUIList)
                    {
                        List<CustomUIDropDownModel> ObjCustDrop = new List<CustomUIDropDownModel>();
                        List<CustomUIRadioButtonModel> ObjRadioList = new List<CustomUIRadioButtonModel>();
                        if (item3.Values != null && item3.ControlType == "Dropdown")
                        {
                            string[] drop = item3.Values.Split(',');
                            int j = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIDropDownModel obj = new CustomUIDropDownModel();
                                obj.Name = item1;
                                obj.Id = j;
                                ObjCustDrop.Add(obj);
                                j++;
                            }
                        }
                        if (item3.Values != null && item3.ControlType == "Radio buttons")
                        {
                            string[] drop = item3.Values.Split(',');
                            int k = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIRadioButtonModel obj = new CustomUIRadioButtonModel();
                                obj.Name = item1;
                                obj.Id = k;
                                ObjRadioList.Add(obj);
                                k++;
                            }
                        }

                        ObjCustomControlUIList[i].FieldModel = ObjCustomControlUIList[i].FieldName.Replace(' ', '_');
                        ObjCustomControlUIList[i].CustomDropDownList = ObjCustDrop;
                        ObjCustomControlUIList[i].CustomRadioButtonList = ObjRadioList;
                        i++;

                    }

                }
            }

            ObjVendorQuoteModal.ProjectDocumentList = ProjectDocList;
            ObjVendorQuoteModal.PhaseList = ProjPhaseList;
            ObjVendorQuoteModal.ResourcesList = ObjResourceList;
            ObjVendorQuoteModal.CustomControlUIList = ObjCustomControlUIList;
            ObjVendorQuoteModal.CustomControlValueModel = ObjCustomControlValueList;

            return ObjVendorQuoteModal;


        }
        public List<AWardedProjectAdminProfileModel> GetAwardedProjectListAdminProfile(AwardedProjectListRequestModel Model)
        {

            List<AWardedProjectAdminProfileModel> ObjAwardedProjectList = new List<AWardedProjectAdminProfileModel>();

            using (var con = new SqlConnection(ConnectionString))
            {
                ObjAwardedProjectList = con.Query<AWardedProjectAdminProfileModel>("Usp_GetProjectAwardedforVendorAdminProfile", new
                {
                    StatusId = Model.StatusId,
                    CompanyId = Model.CompanyId,
                    VendorCompanyId = Model.VendorCompanyId,
                    VendorId = Model.VendorId,
                    UnpaidInvoice = Model.UnpaidInvoice,
                    ProjectId = Model.ProjectId,
                    PageIndex = Model.PageIndex,
                    PageSize = Model.Pagesize


                }, commandType: CommandType.StoredProcedure).ToList();
            }

            return ObjAwardedProjectList;

        }


        public ProjectAwardedViewDetailModel GetAwardedProjectViewDetailVendorAdminProfile(int ProjectId, int VendorId)
        {
            ProjectAwardedViewDetailModel ObjProjectAwardedViewDetail = new ProjectAwardedViewDetailModel();
            List<ProjectDocument> ObjProjectDocList;
            List<vendorPhaseModal> ObjvendorPhaseList;
            List<PhaseDocument> ObjPhaseDocumentList;
            List<vendorMilestone> ObjvendorMileList;
            List<ResourceModal> ObjResourceList;
            List<VendorProjectProgress> ObjProjectProgress;
            List<VendorAdminProfileCustomControlUIModel> ObjCustomControlUIList;
            VendorAdminCustomControlValueModel ObjCustomControlValueList;

            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetAwardedProjectViewDetailVendorAminProfile", new { ProjectId = ProjectId, VendorId = VendorId }, commandType: CommandType.StoredProcedure);
                ObjProjectAwardedViewDetail = Result.Read<ProjectAwardedViewDetailModel>().FirstOrDefault();
                ObjProjectDocList = Result.Read<ProjectDocument>().ToList();
                ObjvendorPhaseList = Result.Read<vendorPhaseModal>().ToList();
                ObjPhaseDocumentList = Result.Read<PhaseDocument>().ToList();
                ObjvendorMileList = Result.Read<vendorMilestone>().ToList();
                ObjResourceList = Result.Read<ResourceModal>().ToList();
                ObjProjectProgress = Result.Read<VendorProjectProgress>().ToList();
                ObjCustomControlUIList = Result.Read<VendorAdminProfileCustomControlUIModel>().ToList();
                ObjCustomControlValueList = Result.Read<VendorAdminCustomControlValueModel>().FirstOrDefault();



                foreach (vendorPhaseModal item in ObjvendorPhaseList)
                {

                    List<PhaseDocument> ObjPhasedocList = ObjPhaseDocumentList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.PhaseDocumentList = new List<PhaseDocument>();
                    foreach (PhaseDocument item1 in ObjPhasedocList)
                    {
                        item.PhaseDocumentList.Add(item1);
                    }
                    List<vendorMilestone> PMilestoneList = ObjvendorMileList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.MileStone = new List<vendorMilestone>();
                    foreach (vendorMilestone item2 in PMilestoneList)
                    {
                        item.MileStone.Add(item2);
                    }
                    int i = 0;
                    foreach (VendorAdminProfileCustomControlUIModel item3 in ObjCustomControlUIList)
                    {
                        List<CustomUIDropDownModel> ObjCustDrop = new List<CustomUIDropDownModel>();
                        List<CustomUIRadioButtonModel> ObjRadioList = new List<CustomUIRadioButtonModel>();
                        if (item3.Values != null && item3.ControlType == "Dropdown")
                        {
                            string[] drop = item3.Values.Split(',');
                            int j = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIDropDownModel obj = new CustomUIDropDownModel();
                                obj.Name = item1;
                                obj.Id = j;
                                ObjCustDrop.Add(obj);
                                j++;
                            }
                        }
                        if (item3.Values != null && item3.ControlType == "Radio buttons")
                        {
                            string[] drop = item3.Values.Split(',');
                            int k = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIRadioButtonModel obj = new CustomUIRadioButtonModel();
                                obj.Name = item1;
                                obj.Id = k;
                                ObjRadioList.Add(obj);
                                k++;
                            }
                        }

                        ObjCustomControlUIList[i].FieldModel = ObjCustomControlUIList[i].FieldName.Replace(' ', '_');
                        ObjCustomControlUIList[i].CustomDropDownList = ObjCustDrop;
                        ObjCustomControlUIList[i].CustomRadioButtonList = ObjRadioList;
                        i++;

                    }


                }

            }
            ObjProjectAwardedViewDetail.PhaseList = ObjvendorPhaseList;
            ObjProjectAwardedViewDetail.ProjectDocumentList = ObjProjectDocList;
            ObjProjectAwardedViewDetail.ResourcesList = ObjResourceList;
            ObjProjectAwardedViewDetail.VendorProjectProgressList = ObjProjectProgress;
            ObjProjectAwardedViewDetail.CustomControlUIList = ObjCustomControlUIList;
            ObjProjectAwardedViewDetail.CustomControlValueModel = ObjCustomControlValueList;
            return ObjProjectAwardedViewDetail;

        }

        public ProjectCompleteFeedBackModal GetProjectCompletionFeedBackVendorAdminProfile(int ProjectId,  int VendorId)
        {
            ProjectCompleteFeedBackModal ObjCreatedProjectModal = new ProjectCompleteFeedBackModal();
            List<VendorResourceForFeedBackModel> ObjVendorResourceList;

            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetProjectCompletionFeedBackVendorAdminProfile", new
                { ProjectId = ProjectId,  VendorId = VendorId }, commandType: CommandType.StoredProcedure);

                ObjCreatedProjectModal = Result.Read<ProjectCompleteFeedBackModal>().FirstOrDefault();
                ObjVendorResourceList = Result.Read<VendorResourceForFeedBackModel>().ToList();
                ObjCreatedProjectModal.VendorResourceList = ObjVendorResourceList;

            }

            return ObjCreatedProjectModal;
        }

        public List<InvoiceVendorAdminProfileModel> GetAllInvoiceListVendorAdminProfile(InvoiceFilterAdminProfileModel Model)
        {
            List<InvoiceVendorAdminProfileModel> objInvoiceModal;
            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceVendorAdminProfileModel>("Usp_GetAllInvoiceListVendorAdminProfile", new
                {
                    VendorCompanyId = Model.VendorCompanyId,
                    CompanyId = Model.CompanyId,
                    FromDate = Model.FromDate,
                    ToDate = Model.ToDate,
                    VendorId = Model.VendorId,
                    PageIndex=Model.PageIndex,
                    PageSizeSelected=Model.PageSizeSelected


                },commandType:CommandType.StoredProcedure).ToList();

            }
            return objInvoiceModal;

        }
        public InvoicePreviewVendorAdminProfileModel InvoicePreviewVendorAdminProfile(int InvoiceId)
        {
            InvoicePreviewVendorAdminProfileModel ObjInvoiceModal;
            List<InvoiceForResourceModal> ObjInvoiceForResourceList;
            List<InvoiceToMilestoneModal> ObjInvoiceToMilestoneList;
            List<InvoiceExtraResourceModal> ObjInvoiceExtraResourceList;

            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("usp_GetInvoicePreviewVendorAndCompanyProfile", new { InvoiceId = InvoiceId }, commandType: CommandType.StoredProcedure);
                ObjInvoiceModal = Result.Read<InvoicePreviewVendorAdminProfileModel>().FirstOrDefault();
                ObjInvoiceForResourceList = Result.Read<InvoiceForResourceModal>().ToList();
                ObjInvoiceToMilestoneList = Result.Read<InvoiceToMilestoneModal>().ToList();
                ObjInvoiceExtraResourceList = Result.Read<InvoiceExtraResourceModal>().ToList();
                ObjInvoiceModal.InvoiceForResourceList = ObjInvoiceForResourceList;
                ObjInvoiceModal.InvoiceToMilestoneList = ObjInvoiceToMilestoneList;
                ObjInvoiceModal.InvoiceToExtraResourceList = ObjInvoiceExtraResourceList;

            }
            return ObjInvoiceModal;

        }


        #region Paging

        public List<InvoiceVendorAdminProfileModel> GetAllInvoiceListVendorAdminProfileOutstanding(InvoiceFilterAdminProfileModel Model)
        {
            List<InvoiceVendorAdminProfileModel> objInvoiceModal;
            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceVendorAdminProfileModel>("Usp_GetAllInvoiceListVendorAdminProfileOutstanding", new
                {
                    VendorCompanyId = Model.VendorCompanyId,
                    CompanyId = Model.CompanyId,
                    FromDate = Model.FromDate,
                    ToDate = Model.ToDate,
                    VendorId = Model.VendorId,
                    PageIndex = Model.PageIndex,
                    PageSizeSelected = Model.PageSizeSelected


                }, commandType: CommandType.StoredProcedure).ToList();

            }
            return objInvoiceModal;

        }

        public List<InvoiceVendorAdminProfileModel> GetAllInvoiceListVendorAdminProfilePaid(InvoiceFilterAdminProfileModel Model)
        {
            List<InvoiceVendorAdminProfileModel> objInvoiceModal;
            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceVendorAdminProfileModel>("Usp_GetAllInvoiceListVendorAdminProfilePaid", new
                {
                    VendorCompanyId = Model.VendorCompanyId,
                    CompanyId = Model.CompanyId,
                    FromDate = Model.FromDate,
                    ToDate = Model.ToDate,
                    VendorId = Model.VendorId,
                    PageIndex = Model.PageIndex,
                    PageSizeSelected = Model.PageSizeSelected


                }, commandType: CommandType.StoredProcedure).ToList();

            }
            return objInvoiceModal;

        }

        public List<InvoiceVendorAdminProfileModel> GetAllInvoiceListVendorAdminProfileRejected(InvoiceFilterAdminProfileModel Model)
        {
            List<InvoiceVendorAdminProfileModel> objInvoiceModal;
            using (var con = new SqlConnection(ConnectionString))
            {
                objInvoiceModal = con.Query<InvoiceVendorAdminProfileModel>("Usp_GetAllInvoiceListVendorAdminProfileRejected", new
                {
                    VendorCompanyId = Model.VendorCompanyId,
                    CompanyId = Model.CompanyId,
                    FromDate = Model.FromDate,
                    ToDate = Model.ToDate,
                    VendorId = Model.VendorId,
                    PageIndex = Model.PageIndex,
                    PageSizeSelected = Model.PageSizeSelected


                }, commandType: CommandType.StoredProcedure).ToList();

            }
            return objInvoiceModal;

        }
        #endregion

    }
}
