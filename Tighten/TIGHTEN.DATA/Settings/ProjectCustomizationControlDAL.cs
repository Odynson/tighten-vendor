﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL.Settings;
using Dapper;

namespace TIGHTEN.DATA
{
    public class ProjectCustomizationControlDAL : BaseClass
    {

        public int InsertCustomControl(CustomControlModel Model)
        {
            int CustomControlId = 0;
            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"insert into  ProjectCustomizationSettings(CompanyId, FieldName, Watermark, Description, ControlType, [Values],IsActive,IsDeleted,Priority)
                                 values(@CompanyId,@FieldName,@Watermark,@Description,@ControlType,@ControlValue,1,0,@Priority)";

                CustomControlId = con.Query<int>(query, new
                {
                    CompanyId = Model.CompanyId,
                    FieldName = Model.FieldName,
                    Watermark = Model.Watermark,
                    Description = Model.Description,
                    ControlType = Model.ControlType,
                    ControlValue = Model.ControlValue == "" ? null : Model.ControlValue,
                    Priority = Model.PriorityControlValue == 0 ? null : Model.PriorityControlValue,
                }).FirstOrDefault();

            }

            CustomControlId = 1;
            return CustomControlId;

        }

        public List<CustomControlModel> GetCustomControl(int CompanyId)
        {

            List<CustomControlModel> ObjCusControl;
            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"select Id CustomControlId, CompanyId, FieldName, Watermark, Description, ControlType, [Values] ControlValue ,IsActive IsActive,Priority PriorityControlValue 
                                from ProjectCustomizationSettings where CompanyId  = @CompanyId and IsDeleted = 0 order by PriorityControlValue";
                ObjCusControl = ObjCusControl = con.Query<CustomControlModel>(query, new { CompanyId = CompanyId }).ToList();
            }

            return ObjCusControl;

        }


        public int DeleteCustomControlOrActive(CustomControlDelete Model)
        {
            int Id;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = "";
                if (Model.IsInActiveOrDelete == 1)
                {
                    query = @"update ProjectCustomizationSettings set  IsDeleted = 1 where Id = @CustomControlId";
                }
                if (Model.IsInActiveOrDelete == 2)
                {
                    query = @"update ProjectCustomizationSettings set  IsActive = @IsActive where Id = @CustomControlId";
                }
                con.Query(query, new { CustomControlId = Model.CustomControlId, IsActive = Model.IsActive });
                Id = 1;

            }
            return Id;

        }


        public int UpdatePriority(MODEL.updatePriorityModel model)
        {

            int Id = 0;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"update ProjectCustomizationSettings set Priority = @SecondPriorityId where Id = @FirstCustomControlId
                                update ProjectCustomizationSettings set Priority = @FirstPriorityId  where Id = @SecondCustomControlId";

                con.Query(query, new
                {
                    FirstCustomControlId = model.FirstCustomControlId,
                    SecondCustomControlId = model.SecondCustomControlId,
                    FirstPriorityId = model.FirstPriorityId,
                    SecondPriorityId = model.SecondPriorityId
                });
            }
            return 0;

        }



    }
}
