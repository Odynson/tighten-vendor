﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using TIGHTEN.MODEL;
using System.Xml.Linq;
using System.Web;
using TIGHTEN.UTILITIES;
using System.Threading;

namespace TIGHTEN.DATA
{
    public class AccountSettings : BaseClass
    {
        AppContext dbcontext;

        public MODEL.AccountSettingsModel GetAccountSettings(string UserId)
        {


            MODEL.AccountSettingsModel ReturnModel;
            int Id;
            using (var con = new SqlConnection(ConnectionString))
            {

                ReturnModel = con.Query<MODEL.AccountSettingsModel>("Usp_GetAccountSettingsForFreelancer", new
                {
                    UserId = UserId,

                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }



            if (ReturnModel != null)
            {
                DateTime date = new DateTime(Convert.ToInt32(ReturnModel.BirthYear), Convert.ToInt32(ReturnModel.BirthMonth), Convert.ToInt32(ReturnModel.BirthDay));

                ReturnModel.DOB = date;
            }



            return ReturnModel;
        }


        public string UpdateAccountSettings(MODEL.AccountSettingsModel model)
        {
            string msg = string.Empty;
            int checksuccess;

            using (var con = new SqlConnection(ConnectionString))
            {

                checksuccess = con.Query<int>("Usp_UpdateUserRoutingNumberForFreelancer",

                new
                {
                    UserId = model.UserId,
                    TosAcceptanceDate = DateTime.UtcNow,
                    TosAcceptanceIp = "124.253.176.204",
                    LegalEntityType = "individual",
                    LegalEntityAddressLine1 = model.Address,
                    LegalEntityAddressCity = model.AddressCity,
                    LegalEntityAddressState = model.AddressState,
                    LegalEntityAddressPostalCode = model.AddressPostalCode,
                    LegalEntityBirthDay = model.DOB.Day.ToString(),
                    LegalEntityBirthMonth = model.DOB.Month.ToString(),
                    LegalEntityBirthYear = model.DOB.Year.ToString(),
                    LegalEntitySSNLast4 = model.SocialSecurityNumber,
                    ExternalBankAccountNumber = model.AccountNumber,
                    ExternalBankCountry = model.Country,
                    ExternalBankRoutingNumber = model.RoutingNumber,
                    response = 0

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }




            //UserDetail user = (from usr in dbcontext.UserDetails
            //                   where usr.UserId == model.UserId
            //                   && usr.IsDeleted == false
            //                   select usr).FirstOrDefault();

            UserDetail user;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from UserDetails usr where usr.UserId = @UserId  and usr.IsDeleted = 0 ";
                user = con.Query<UserDetail>(sqlquery, new { UserId = model.UserId }).FirstOrDefault();
            }


            if (user.StripeUserId != null && user.StripeUserId != string.Empty)
            {

                #region  Save freelancer Account info to StripeAccountDetail Table

                //StripeAccountDetail stripedetail = (from str in dbcontext.StripeAccountDetails
                //                                    where str.UserId == model.UserId
                //                                    select str).FirstOrDefault();

                //if (stripedetail.UserId != string.Empty && stripedetail.UserId != null)
                //{
                //    stripedetail.TosAcceptanceDate = DateTime.UtcNow.Date;
                //    stripedetail.TosAcceptanceIp = "124.253.176.204";
                //    stripedetail.LegalEntityType = "individual";
                //    stripedetail.LegalEntityAddressLine1 = model.Address;
                //    stripedetail.LegalEntityAddressCity = model.AddressCity;
                //    stripedetail.LegalEntityAddressState = model.AddressState;
                //    stripedetail.LegalEntityAddressPostalCode = model.AddressPostalCode;
                //    stripedetail.LegalEntityFirstName = user.FirstName;
                //    stripedetail.LegalEntityLastName = user.LastName;
                //    stripedetail.LegalEntityBirthDay = model.DOB.Day.ToString();
                //    stripedetail.LegalEntityBirthMonth = model.DOB.Month.ToString();
                //    stripedetail.LegalEntityBirthYear = model.DOB.Year.ToString();
                //    stripedetail.LegalEntitySSNLast4 = model.SocialSecurityNumber;
                //    stripedetail.ExternalBankAccountNumber = model.AccountNumber;
                //    stripedetail.ExternalBankCountry = model.Country;
                //    stripedetail.ExternalBankRoutingNumber = model.RoutingNumber;

                //    dbcontext.SaveChanges();
                //}

                #endregion

                #region  Send info to Stripe
                var myAccount = new StripeAccountUpdateOptions();

                myAccount.TosAcceptanceDate = DateTime.UtcNow.Date;
                myAccount.TosAcceptanceIp = "124.253.176.204";

                StripeAccountLegalEntityOptions legalEntity = new StripeAccountLegalEntityOptions();

                legalEntity.Type = "individual";
                legalEntity.AddressLine1 = model.Address;
                legalEntity.AddressCity = model.AddressCity;
                legalEntity.AddressState = model.AddressState;
                legalEntity.AddressPostalCode = model.AddressPostalCode;
                legalEntity.FirstName = user.FirstName;
                legalEntity.LastName = user.LastName;

                if (user.IsStripeAccountVerified == false)
                {
                    legalEntity.BirthDay = Convert.ToInt32(model.DOB.Day);
                    legalEntity.BirthMonth = Convert.ToInt32(model.DOB.Month);
                    legalEntity.BirthYear = Convert.ToInt32(model.DOB.Year);
                }
                legalEntity.SSNLast4 = model.SocialSecurityNumber;

                myAccount.LegalEntity = legalEntity;

                StripeAccountBankAccountOptions externalBankAccount = new StripeAccountBankAccountOptions();
                externalBankAccount.AccountNumber = model.AccountNumber;
                externalBankAccount.Country = model.Country;
                externalBankAccount.RoutingNumber = model.RoutingNumber;

                myAccount.ExternalBankAccount = externalBankAccount;


                var accountService = new StripeAccountService();
                StripeAccount response = accountService.Update(user.StripeUserId, myAccount);

                if (response.AccountVerification.DisabledReason == null && response.PayoutsEnabled == true)
                {


                    using (var con = new SqlConnection(ConnectionString))
                    {

                        checksuccess = con.Query<int>("Usp_UpdateUserRoutingNumberForFreelancer",

                        new
                        {
                            UserId = model.UserId,
                            TosAcceptanceDate = DateTime.UtcNow,
                            TosAcceptanceIp = "124.253.176.204",
                            LegalEntityType = "individual",
                            LegalEntityAddressLine1 = model.Address,
                            LegalEntityAddressCity = model.AddressCity,
                            LegalEntityAddressState = model.AddressState,
                            LegalEntityAddressPostalCode = model.AddressPostalCode,
                            LegalEntityBirthDay = model.DOB.Day.ToString(),
                            LegalEntityBirthMonth = model.DOB.Month.ToString(),
                            LegalEntityBirthYear = model.DOB.Year.ToString(),
                            LegalEntitySSNLast4 = model.SocialSecurityNumber,
                            ExternalBankAccountNumber = model.AccountNumber,
                            ExternalBankCountry = model.Country,
                            ExternalBankRoutingNumber = model.RoutingNumber,
                            response = 1

                        }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    }


                    //user.IsStripeAccountVerified = true;
                    //dbcontext.SaveChanges();

                    msg = "Verified";
                }
                #endregion

            }


            return msg;
        }

        public string UpdateAccountSettingsVendorCompany(MODEL.AccountSettingsModel model)
        {
            string msg = string.Empty;
            int checksuccess;

            using (var con = new SqlConnection(ConnectionString))
            {
                // This Procedure Use Both Pocs and Vendor
                checksuccess = con.Query<int>("Usp_UpdateUserRoutingNumberForFreelancer",

                new
                {
                    UserId = model.UserId,
                    TosAcceptanceDate = DateTime.UtcNow,
                    TosAcceptanceIp = "124.253.176.204",
                    LegalEntityType = "individual",
                    LegalEntityAddressLine1 = model.Address,
                    LegalEntityAddressCity = model.AddressCity,
                    LegalEntityAddressState = model.AddressState,
                    LegalEntityAddressPostalCode = model.AddressPostalCode,
                    LegalEntityBirthDay = model.DOB.Day.ToString(),
                    LegalEntityBirthMonth = model.DOB.Month.ToString(),
                    LegalEntityBirthYear = model.DOB.Year.ToString(),
                    LegalEntitySSNLast4 = model.SocialSecurityNumber,
                    ExternalBankAccountNumber = model.AccountNumber,
                    ExternalBankCountry = model.Country,
                    ExternalBankRoutingNumber = model.RoutingNumber,
                    response = 0

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }

            msg = "Verified";

            return msg;
        }



        public string UpdateUserRoutingNumberVendorPOCsProfile(MODEL.AccountSettingsModel model)
        {
            string msg = string.Empty;
            int checksuccess;

            using (var con = new SqlConnection(ConnectionString))
            {
                // This Procedure Use Both Pocs and Vendor
                checksuccess = con.Query<int>("Usp_UpdateUserRoutingNumberForFreelancer",

                new
                {
                    UserId = model.UserId,
                    TosAcceptanceDate = DateTime.UtcNow,
                    TosAcceptanceIp = "124.253.176.204",
                    LegalEntityType = "individual",
                    LegalEntityAddressLine1 = model.Address,
                    LegalEntityAddressCity = model.AddressCity,
                    LegalEntityAddressState = model.AddressState,
                    LegalEntityAddressPostalCode = model.AddressPostalCode,
                    LegalEntityBirthDay = model.DOB.Day.ToString(),
                    LegalEntityBirthMonth = model.DOB.Month.ToString(),
                    LegalEntityBirthYear = model.DOB.Year.ToString(),
                    LegalEntitySSNLast4 = model.SocialSecurityNumber,
                    ExternalBankAccountNumber = model.AccountNumber,
                    ExternalBankCountry = model.Country,
                    ExternalBankRoutingNumber = model.RoutingNumber,
                    response = 0

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }

            msg = "Verified";

            return msg;
        }

        public string UpdateUserRoutingNumberVendorAdminProfile(MODEL.AccountSettingsModel model)
        {
             string msg = string.Empty;
           

            List <MailSendVendorAdminProfileAccountSendingModel> ObjMailSendingVendorAdminProfile;

            using (var con = new SqlConnection(ConnectionString))
            {
                // This Procedure Use Both Pocs and Vendor
                ObjMailSendingVendorAdminProfile = con.Query<MailSendVendorAdminProfileAccountSendingModel>("Usp_UpdateUserRoutingNumberForVendorAdminProfile",

                new
                {
                    UserId = model.UserId,
                    TosAcceptanceDate = DateTime.UtcNow,
                    TosAcceptanceIp = "124.253.176.204",
                    LegalEntityType = "individual",
                    LegalEntityAddressLine1 = model.Address,
                    LegalEntityAddressCity = model.AddressCity,
                    LegalEntityAddressState = model.AddressState,
                    LegalEntityAddressPostalCode = model.AddressPostalCode,
                    LegalEntityBirthDay = model.DOB.Day.ToString(),
                    LegalEntityBirthMonth = model.DOB.Month.ToString(),
                    LegalEntityBirthYear = model.DOB.Year.ToString(),
                    LegalEntitySSNLast4 = model.SocialSecurityNumber,
                    ExternalBankAccountNumber = model.AccountNumber,
                    ExternalBankCountry = model.Country,
                    ExternalBankRoutingNumber = model.RoutingNumber,


                }, commandType: CommandType.StoredProcedure).ToList();

            }


            if (ObjMailSendingVendorAdminProfile != null)
            {
                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForsentMailVendorAdminProfileAccountSetting(ObjMailSendingVendorAdminProfile);
                }));
                childref.Start();
              

            }
            msg = "User Routing Number updated successfully !!";
            return msg;
        }



        private void ThreadForsentMailVendorAdminProfileAccountSetting(List<MailSendVendorAdminProfileAccountSendingModel> MailModel)
        {

            String emailbody = null;
            string Subject = string.Empty;
            string subjectBysender = string.Empty;
            subjectBysender = "Payment";



            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing

            var MailModule = from r in xdoc.Descendants("MailModule")
                             .Where(r => (string)r.Attribute("id") == "VendorAdminProfileAccountSetting")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 Subject = r.Element("Subject").Value
                             };


            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.Subject;

            };

            foreach (var item in MailModel)
            {

                emailbody = emailbody.Replace("@@ReceiverName", item.ReceiverName).Replace("@@SenderCompanyName", item.SenderCompanyName);
                EmailUtility.SendMailInThread(item.Email, subjectBysender, emailbody);
            }

        }

        public void UpdateUserRoutingNumber(MODEL.AccountSettingsModel model)
        {



            UserDetail user = (from usr in dbcontext.UserDetails
                               where usr.UserId == model.UserId
                               && usr.IsDeleted == false
                               select usr).FirstOrDefault();

            if (user.StripeUserId != null && user.StripeUserId != string.Empty)
            {



                #region  Save freelancer Account info to StripeAccountDetail Table

                StripeAccountDetail stripedetail = (from str in dbcontext.StripeAccountDetails
                                                    where str.UserId == model.UserId
                                                    select str).FirstOrDefault();

                if (stripedetail.UserId != string.Empty && stripedetail.UserId != null)
                {
                    // Following fields are also required for stripe transfer
                    stripedetail.TosAcceptanceDate = DateTime.UtcNow.Date;
                    stripedetail.TosAcceptanceIp = "124.253.176.204";
                    stripedetail.LegalEntityType = "individual";
                    stripedetail.LegalEntityFirstName = user.FirstName;
                    stripedetail.LegalEntityLastName = user.LastName;
                    // here we are saving Routing Number
                    stripedetail.ExternalBankRoutingNumber = model.RoutingNumber;
                    dbcontext.SaveChanges();
                }

                #endregion

                #region  Send info to Stripe
                var myAccount = new StripeAccountUpdateOptions();

                myAccount.TosAcceptanceDate = DateTime.UtcNow.Date;
                myAccount.TosAcceptanceIp = "124.253.176.204";

                StripeAccountLegalEntityOptions legalEntity = new StripeAccountLegalEntityOptions();
                legalEntity.Type = "individual";
                legalEntity.FirstName = user.FirstName;
                legalEntity.LastName = user.LastName;

                myAccount.LegalEntity = legalEntity;

                StripeAccountBankAccountOptions externalBankAccount = new StripeAccountBankAccountOptions();
                externalBankAccount.AccountNumber = model.AccountNumber;
                externalBankAccount.RoutingNumber = model.RoutingNumber;
                externalBankAccount.Country = model.Country;

                myAccount.ExternalBankAccount = externalBankAccount;


                var accountService = new StripeAccountService();
                StripeAccount response = accountService.Update(user.StripeUserId, myAccount);

                if (response.AccountVerification.DisabledReason == null && response.PayoutsEnabled == true)
                {

                    using (var con = new SqlConnection(ConnectionString))
                    {

                        var AllTeams = con.QueryMultiple("Usp_UpdateUserRoutingNumberForFreelancer",

                        new
                        {
                            UserId = model.UserId,
                            TosAcceptanceDate = DateTime.UtcNow,
                            TosAcceptanceIp = "124.253.176.204",
                            LegalEntityType = "individual",
                            response = 0

                        }, commandType: CommandType.StoredProcedure);

                    }


                    //user.IsStripeAccountVerified = true;
                    //    dbcontext.SaveChanges();
                }
                #endregion

            }
        }



    }
}
