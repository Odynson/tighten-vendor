﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class Settings : BaseClass
    {
        AppContext dbcontext;

        public List<MODEL.GetUserNotificationConfigModel> GetAllEmailSettingsOption(string UserId, int CompanyId, int ProjectId, int RoleId)
        {

            //List<MODEL.GetUserNotificationConfigModel> AllEmailOption = ((from emailopt in dbcontext.SettingConfigs
            //                                                              join setsec in dbcontext.SettingSections on emailopt.SettingSectionId equals setsec.Id
            //                                                              join usrset in dbcontext.UserSettingConfigs on emailopt.Id equals usrset.NotificationId
            //                                                              where emailopt.IsActive == true
            //                                                              && usrset.UserId == UserId
            //                                                              && usrset.CompanyId == CompanyId
            //                                                              select new MODEL.GetUserNotificationConfigModel
            //                                                              {
            //                                                                  NotificationId = emailopt.Id,
            //                                                                  SettingSectionId = emailopt.SettingSectionId,
            //                                                                  SettingSectionName = setsec.SectionName,
            //                                                                  NotificationName = emailopt.NotificationName,
            //                                                                  NotificationText = emailopt.NotificationText,
            //                                                                  NotificationDescription = emailopt.NotificationDescription,
            //                                                                  IsActive = emailopt.IsActive.HasValue ? emailopt.IsActive.Value : false,
            //                                                                  IsUserNotificationActive = usrset.IsActive

            //                                                              })

            //                                                               )
            //                                                              .ToList();


            List<MODEL.GetUserNotificationConfigModel> AllEmailOption;
            List<MODEL.SettingSectioncheck> objList;
            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("usp_GetAllEmailSettingsOption", new { UserId = UserId, CompanyId = CompanyId }, commandType: System.Data.CommandType.StoredProcedure);

                AllEmailOption = Result.Read<MODEL.GetUserNotificationConfigModel>().ToList();
               // objList = Result.Read<MODEL.SettingSectioncheck>().ToList();



                //AllEmailOption = con.Query<MODEL.GetUserNotificationConfigModel>("usp_GetAllEmailSettingsOption", new { UserId = UserId, CompanyId = CompanyId }, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }

            return AllEmailOption;
        }



        public string ChangeEmailSettingsOption(MODEL.GetUserNotificationConfigModel model)
        {
            string msg = string.Empty;

            //UserSettingConfig emailoption = (from opt in dbcontext.UserSettingConfigs
            //                                   where opt.UserId == model.UserId
            //                                   && opt.NotificationId == model.NotificationId
            //                                   select opt).FirstOrDefault();

            UserSettingConfig emailoption;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from UserSettingConfigs opt where opt.UserId = @UserId and opt.NotificationId = @NotificationId";

                emailoption = con.Query<UserSettingConfig>(sqlquery, new { UserId = model.UserId, NotificationId = model.NotificationId }).FirstOrDefault();


                if (emailoption != null)
                {
                    //emailoption.IsActive = model.IsUserNotificationActive;

                    string sqlquery_updateActive = @"update UserSettingConfigs set IsActive = @IsUserNotificationActive  where UserId = @UserId and NotificationId = @NotificationId";
                    con.Execute(sqlquery_updateActive, new { IsUserNotificationActive = model.IsUserNotificationActive, UserId = model.UserId, NotificationId = model.NotificationId });
                }
                else
                {
                    //UserSettingConfig obj = new UserSettingConfig();
                    //obj.UserId = model.UserId;
                    //obj.NotificationId = model.NotificationId;
                    //obj.IsActive = model.IsUserNotificationActive;

                    //dbcontext.UserSettingConfigs.Add(obj);
                    //dbcontext.SaveChanges();

                    string sqlquery_Insert = @"insert into UserSettingConfigs (UserId,NotificationId,IsActive ) values (@UserId, @NotificationId,@IsUserNotificationActive)";
                    con.Execute(sqlquery_Insert, new { UserId = model.UserId, NotificationId = model.NotificationId, IsUserNotificationActive = model.IsUserNotificationActive });

                }

            }
            msg = "Notification for " + model.NotificationText + " is updated Successfully !!";
            return msg;
        }




        public List<MODEL.GetProjectsForCurrentCompanyModel> GetProjectsForCurrentCompany(string UserId, int CompanyId)
        {

            //List<MODEL.GetProjectsForCurrentCompanyModel> UserProjects = (from prjct in dbcontext.Projects
            //                                                               join prjctUsr in dbcontext.ProjectUsers on prjct.Id equals prjctUsr.ProjectId
            //                                                               where prjctUsr.UserId == UserId && prjct.CompanyId == CompanyId
            //                                                               && prjct.IsDeleted == false
            //                                                              select new MODEL.GetProjectsForCurrentCompanyModel
            //                                                               {
            //                                                                ProjectId =  prjct.Id ,
            //                                                                ProjectName =    prjct.Name
            //                                                               }).ToList();

            List<MODEL.GetProjectsForCurrentCompanyModel> UserProjects;
            using (var con = new SqlConnection(ConnectionString))
            {
                UserProjects = con.Query<MODEL.GetProjectsForCurrentCompanyModel>("usp_GetProjectsForCurrentCompany", new { UserId = UserId, CompanyId = CompanyId }, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }


            return UserProjects;
        }



        public MODEL.EmailSettingsModelForProjectLevel GetEmailSettingsForProjectLevel(string UserId, int CompanyId)
        {
            MODEL.EmailSettingsModelForProjectLevel ProjectSettingObject = new MODEL.EmailSettingsModelForProjectLevel();

            //List<MODEL.ProjectSettingsModel> allProjects = (from prjct in dbcontext.Projects
            //                                                join prjctUsr in dbcontext.ProjectUsers on prjct.Id equals prjctUsr.ProjectId
            //                                                where prjctUsr.UserId == UserId && prjct.CompanyId == CompanyId
            //                                                && prjct.IsDeleted == false
            //                                                select new MODEL.ProjectSettingsModel
            //                                                {
            //                                                    ProjectId = prjct.Id,
            //                                                    ProjectName = prjct.Name,
            //                                                    ProjectDescription = prjct.Description
            //                                                }).ToList();


            //UserSettingConfig usrsetobj        =     (from usrsetconfig in dbcontext.UserSettingConfigs
            //                                           join setconfig in dbcontext.SettingConfigs on usrsetconfig.NotificationId equals setconfig.Id
            //                                           where usrsetconfig.UserId == UserId
            //                                           && usrsetconfig.CompanyId == CompanyId
            //                                           && setconfig.NotificationName == "Project Update"
            //                                           select usrsetconfig).FirstOrDefault();


            List<MODEL.ProjectSettingsModel> allProjects;
            UserSettingConfig usrsetobj;

            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetUsersAllSettingsAndProjects", new { UserId = UserId, CompanyId = CompanyId }, commandType: System.Data.CommandType.StoredProcedure);
                allProjects = result.Read<MODEL.ProjectSettingsModel>().ToList();
                usrsetobj = result.Read<UserSettingConfig>().FirstOrDefault();

            }


            int[] ProjectIds = null;
            List<MODEL.ProjectSettingsModel> activeProjects = new List<MODEL.ProjectSettingsModel>();

            if (usrsetobj != null)
            {
                if (usrsetobj.ProjectId != null && usrsetobj.ProjectId != string.Empty)
                {
                    //ProjectIds = usrsetobj.ProjectId.Split(',').Select(str => int.Parse(str)).ToArray();

                    //activeProjects = (from prjct in dbcontext.Projects
                    //                  join prjctUsr in dbcontext.ProjectUsers on prjct.Id equals prjctUsr.ProjectId
                    //                  where prjctUsr.UserId == UserId && prjct.CompanyId == CompanyId
                    //                  && ProjectIds.Contains(prjct.Id) && prjct.IsDeleted == false
                    //                  select new MODEL.ProjectSettingsModel
                    //                  {
                    //                      ProjectId = prjct.Id,
                    //                      ProjectName = prjct.Name,
                    //                      ProjectDescription = prjct.Description
                    //                  }).ToList();

                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlquery = @"select prjct.Id as ProjectId,prjct.Name as ProjectName,prjct.Description as ProjectDescription from Projects prjct inner join ProjectUsers prjctUsr on prjct.Id = prjctUsr.ProjectId 
                                            where prjctUsr.UserId =  @UserId and prjct.CompanyId = @CompanyId and prjct.Id in (" + usrsetobj.ProjectId + ") and prjct.IsDeleted = 0";

                        activeProjects = con.Query<MODEL.ProjectSettingsModel>(sqlquery, new { UserId = UserId, CompanyId = CompanyId }).ToList();
                    }


                }

                ProjectSettingObject.IsAllProjectsSelected = usrsetobj.IsActive;
            }


            ProjectSettingObject.AllProjects = allProjects;
            ProjectSettingObject.ActiveProjects = activeProjects;

            return ProjectSettingObject;
        }



        public MODEL.EmailSettingsModelForProjectLevel ChangeAllProjectSetting(MODEL.ChangeSettingsModel model)
        {
            MODEL.EmailSettingsModelForProjectLevel ProjectSettingObject = new MODEL.EmailSettingsModelForProjectLevel();

            //UserSettingConfig emailoption  =  (from opt in dbcontext.UserSettingConfigs
            //                                   join usr in dbcontext.SettingConfigs on opt.NotificationId equals usr.Id
            //                                   where opt.UserId == model.UserId
            //                                   && opt.CompanyId == model.CompanyId
            //                                   && usr.NotificationName == "Project Update"
            //                                   select opt).FirstOrDefault();


            //List< MODEL.ProjectSettingsModel> allProject  =  (from prjct in dbcontext.Projects
            //                                                  join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId
            //                                                  where prjct.CompanyId == model.CompanyId && prjct.IsDeleted == false
            //                                                  && prjctusr.UserId == model.UserId
            //                                                  select new MODEL.ProjectSettingsModel
            //                                                  {
            //                                                      ProjectId = prjct.Id,
            //                                                      ProjectName = prjct.Name,
            //                                                      ProjectDescription = prjct.Description
            //                                                  }).ToList();


            UserSettingConfig emailoption;
            List<MODEL.ProjectSettingsModel> allProject;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetOptionsForChangeAllProjectSetting", new { UserId = model.UserId, CompanyId = model.CompanyId }, commandType: System.Data.CommandType.StoredProcedure);

                emailoption = result.Read<UserSettingConfig>().FirstOrDefault();
                allProject = result.Read<MODEL.ProjectSettingsModel>().ToList();


                string projectids = string.Empty;

                foreach (var item in allProject)
                {
                    if (projectids == string.Empty)
                    {
                        projectids = item.ProjectId.ToString();
                    }
                    else
                    {
                        projectids += "," + item.ProjectId.ToString();
                    }
                }

                if (emailoption != null)
                {
                    //emailoption.IsActive = model.IsUserNotificationActive;
                    bool IsActive = model.IsUserNotificationActive;

                    if (model.IsUserNotificationActive == false)
                    {
                        projectids = null;
                        allProject = null;
                    }

                    //if (model.IsUserNotificationActive == true)
                    //{
                    //    emailoption.ProjectId = projectids;
                    //}
                    //else
                    //{
                    //    emailoption.ProjectId = null;
                    //    allProject = null;
                    //}


                    string sqlquery = @"update UserSettingConfigs set IsActive = @IsActive,ProjectId = @projectids where UserId = @UserId and CompanyId = @CompanyId and NotificationId = 5 ";
                    con.Execute(sqlquery, new { IsActive = IsActive, projectids = projectids, UserId = model.UserId, CompanyId = model.CompanyId });

                }
            }

            ProjectSettingObject.ActiveProjects = allProject;

            return ProjectSettingObject;
        }



        public MODEL.EmailSettingsModelForTodoLevel GetEmailSettingsForTodoLevel(string UserId, int CompanyId)
        {
            MODEL.EmailSettingsModelForTodoLevel TodoSettingObject = new MODEL.EmailSettingsModelForTodoLevel();

            //List<MODEL.TodoSettingsModel>   allTodos = (from fol in dbcontext.Followers
            //                                            join tod in dbcontext.ToDos on fol.TodoId equals tod.Id
            //                                            join Sec in dbcontext.Sections on tod.SectionId equals Sec.Id
            //                                            join prjct in dbcontext.Projects on Sec.ProjectId equals prjct.Id
            //                                            where fol.UserId == UserId && prjct.IsDeleted == false
            //                                            && prjct.CompanyId == CompanyId && tod.IsDeleted == false
            //                                            select new MODEL.TodoSettingsModel
            //                                            {
            //                                                TodoId = tod.Id,
            //                                                TodoName = tod.Name,
            //                                                TodoDescription = tod.Description
            //                                            }).ToList();


            //UserSettingConfig usrsetobj = (from usrsetconfig in dbcontext.UserSettingConfigs
            //                               join setconfig in dbcontext.SettingConfigs on usrsetconfig.NotificationId equals setconfig.Id
            //                               where usrsetconfig.UserId == UserId
            //                               && usrsetconfig.CompanyId == CompanyId
            //                               && setconfig.NotificationName == "Todo Update"
            //                               select usrsetconfig).FirstOrDefault();


            List<MODEL.TodoSettingsModel> allTodos;
            UserSettingConfig usrsetobj;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetEmailSettingsForTodoLevel", new { UserId = UserId, CompanyId = CompanyId }, commandType: System.Data.CommandType.StoredProcedure);
                allTodos = result.Read<MODEL.TodoSettingsModel>().ToList();
                usrsetobj = result.Read<UserSettingConfig>().FirstOrDefault();
            }



            int[] TodoIds = null;
            List<MODEL.TodoSettingsModel> activeTodos = new List<MODEL.TodoSettingsModel>();

            if (usrsetobj != null)
            {
                if (usrsetobj.TodoId != null && usrsetobj.TodoId != string.Empty)
                {
                    //TodoIds = usrsetobj.TodoId.Split(',').Select(str => int.Parse(str)).ToArray();
                    //activeTodos = (from fol in dbcontext.Followers
                    //               join tod in dbcontext.ToDos on fol.TodoId equals tod.Id
                    //               join Sec in dbcontext.Sections on tod.SectionId equals Sec.Id
                    //               join prjct in dbcontext.Projects on Sec.ProjectId equals prjct.Id
                    //               where fol.UserId == UserId && prjct.IsDeleted == false
                    //               && prjct.CompanyId == CompanyId && tod.IsDeleted == false
                    //               && TodoIds.Contains(tod.Id)
                    //               select new MODEL.TodoSettingsModel
                    //               {
                    //                   TodoId = tod.Id,
                    //                   TodoName = tod.Name,
                    //                   TodoDescription = tod.Description
                    //               }).ToList();


                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlquery = @"select tod.Id as TodoId,tod.Name as TodoName, tod.Description as TodoDescription
                                            from Followers fol inner join ToDoes tod on fol.TodoId = tod.Id inner join Sections Sec on tod.SectionId = Sec.Id 
                                            inner join Projects prjct on Sec.ProjectId = prjct.Id where fol.UserId = @UserId and prjct.IsDeleted = 0 
                                            and prjct.CompanyId = @CompanyId and tod.IsDeleted = 0 and tod.Id in (" + usrsetobj.TodoId + ")"
                                            ;

                        activeTodos = con.Query<MODEL.TodoSettingsModel>(sqlquery, new { UserId = UserId, CompanyId = CompanyId }).ToList();
                    }


                }

                TodoSettingObject.IsAllTodosSelected = usrsetobj.IsActive;
            }

            TodoSettingObject.AllTodos = allTodos;
            TodoSettingObject.ActiveTodos = activeTodos;


            return TodoSettingObject;
        }



        public MODEL.EmailSettingsModelForTodoLevel ChangeAllTodoSetting(MODEL.ChangeSettingsModel model)
        {
            MODEL.EmailSettingsModelForTodoLevel TodoSettingObject = new MODEL.EmailSettingsModelForTodoLevel();

            //UserSettingConfig emailoption  =  (from opt in dbcontext.UserSettingConfigs
            //                                   join usr in dbcontext.SettingConfigs on opt.NotificationId equals usr.Id
            //                                   where opt.UserId == model.UserId
            //                                   && opt.CompanyId == model.CompanyId
            //                                   && usr.NotificationName == "Todo Update"
            //                                   select opt).FirstOrDefault();


            //List<MODEL.TodoSettingsModel> allfollower = (from fol in dbcontext.Followers
            //                                             join tod in dbcontext.ToDos on fol.TodoId equals tod.Id
            //                                             join sec in dbcontext.Sections on tod.SectionId equals sec.Id
            //                                             join prjct in dbcontext.Projects on sec.ProjectId equals prjct.Id
            //                                             where fol.UserId == model.UserId && prjct.IsDeleted == false
            //                                             && prjct.CompanyId == model.CompanyId && tod.IsDeleted == false
            //                                           select new MODEL.TodoSettingsModel
            //                                               {
            //                                                   TodoId = tod.Id,
            //                                                   TodoName = tod.Name,
            //                                                   TodoDescription = tod.Description
            //                                               }).ToList();

            UserSettingConfig emailoption;
            List<MODEL.TodoSettingsModel> allfollower;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetOptionsForChangeAllTodoSetting", new { UserId = model.UserId, CompanyId = model.CompanyId }, commandType: System.Data.CommandType.StoredProcedure);
                emailoption = result.Read<UserSettingConfig>().FirstOrDefault();
                allfollower = result.Read<MODEL.TodoSettingsModel>().ToList();


                string TodoIds = string.Empty;

                foreach (var item in allfollower)
                {
                    if (TodoIds == string.Empty)
                    {
                        TodoIds = item.TodoId.ToString();
                    }
                    else
                    {
                        TodoIds += "," + item.TodoId.ToString();
                    }
                }


                if (emailoption != null)
                {
                    //emailoption.IsActive = model.IsUserNotificationActive;
                    bool IsActive = model.IsUserNotificationActive;

                    if (model.IsUserNotificationActive == false)
                    {
                        TodoIds = null;
                        allfollower = null;
                    }


                    //if (model.IsUserNotificationActive == true)
                    //{
                    //    emailoption.TodoId = TodoIds;
                    //}
                    //else
                    //{
                    //    emailoption.TodoId = null;
                    //    allfollower = null;
                    //}


                    string sqlquery_UpdateTodo = @"update UserSettingConfigs set IsActive = @IsActive,TodoId = @TodoIds where UserId = @UserId and opt.CompanyId = @CompanyId and usr.NotificationId = 7 ";
                    con.Execute(sqlquery_UpdateTodo, new { IsActive = IsActive, TodoIds = TodoIds, UserId = model.UserId, CompanyId = model.CompanyId });

                }

            }


            TodoSettingObject.ActiveTodos = allfollower;

            return TodoSettingObject;
        }




        public string ChangeProjectSetting(MODEL.EmailSettingsModelForProjectLevel model)
        {
            string msg = string.Empty;

            #region ChangeProjectSetting
            /* getting all Settings for the user  */
            //UserSettingConfig usrsetting = (from usrset in dbcontext.UserSettingConfigs
            //                                join set in dbcontext.SettingConfigs on usrset.NotificationId equals set.Id
            //                                where usrset.UserId == model.UserId
            //                                && usrset.CompanyId == model.CompanyId
            //                                && set.NotificationName == "Project Update"
            //                                select usrset).FirstOrDefault();

            UserSettingConfig usrsetting;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select usrset.* from UserSettingConfigs usrset inner join SettingConfigs set on usrset.NotificationId = set.Id where usrset.UserId = @UserId 
                                    and usrset.CompanyId = @CompanyId and set.NotificationName = 'Project Update'  ";

                usrsetting = con.Query<UserSettingConfig>(sqlquery, new { UserId = model.UserId, CompanyId = model.CompanyId }).FirstOrDefault();


                if (usrsetting != null)
                {
                    string activeProjects = string.Empty;
                    bool IsActive = false;

                    foreach (var item in model.UserSelectedProjects)
                    {
                        if (activeProjects == string.Empty)
                        {
                            activeProjects = item;
                        }
                        else
                        {
                            activeProjects += "," + item;
                        }
                    }

                    if (activeProjects == "")
                    {
                        IsActive = false;
                    }
                    else
                    {
                        IsActive = true;
                    }


                    string sqlquery_UpdateProjectId = @"update UserSettingConfigs set IsActive = @IsActive,ProjectId = @activeProjects  where UserId = @UserId 
                                                        and CompanyId = @CompanyId and NotificationId = 5  ";

                    con.Execute(sqlquery_UpdateProjectId, new { UserId = model.UserId, CompanyId = model.CompanyId, IsActive = IsActive, activeProjects = activeProjects });

                }
            }

            #endregion

            msg = "Settings for project updation is updated Successfully !!";
            return msg;
        }




        public string changeTodoSetting(MODEL.EmailSettingsModelForTodoLevel model)
        {
            string msg = string.Empty;

            //UserSettingConfig emailoption = (from opt in dbcontext.UserSettingConfigs
            //                                  join usr in dbcontext.SettingConfigs on opt.NotificationId equals usr.Id
            //                                  where opt.UserId == model.UserId
            //                                  && opt.CompanyId == model.CompanyId
            //                                  && usr.NotificationName == "Todo Update"
            //                                  select opt).FirstOrDefault();



            UserSettingConfig emailoption;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from UserSettingConfigs opt inner join SettingConfigs usr on opt.NotificationId = usr.Id where opt.UserId = @UserId
                                 and opt.CompanyId = @CompanyId and usr.NotificationName = 'Todo Update'";

                emailoption = con.Query<UserSettingConfig>(sqlquery, new { UserId = model.UserId, CompanyId = model.CompanyId }).FirstOrDefault();



                if (emailoption != null)
                {
                    string activeProjects = string.Empty;
                    bool IsActive = false;

                    foreach (var item in model.UserSelectedTodos)
                    {
                        if (activeProjects == string.Empty)
                        {
                            activeProjects = item;
                        }
                        else
                        {
                            activeProjects += "," + item;
                        }
                    }

                    if (activeProjects == "")
                    {
                        IsActive = false;
                    }
                    else
                    {
                        IsActive = true;
                    }


                    string sqlquery_UpdateTodoId = @"update UserSettingConfigs set IsActive = @IsActive,TodoId = @activeProjects  where UserId = @UserId 
                                                        and CompanyId = @CompanyId and NotificationId = 7  ";

                    con.Execute(sqlquery_UpdateTodoId, new { UserId = model.UserId, CompanyId = model.CompanyId, IsActive = IsActive, activeProjects = activeProjects });


                }

            }


            msg = "Settings for todo updation is updated Successfully !!";
            return msg;
        }

        public MODEL.VendorMainSettingsModel ChangeVendorSetting(int CompanyId, bool IsVendor)
        {
            MODEL.VendorMainSettingsModel objVendorMainsetting = new MODEL.VendorMainSettingsModel();
            List<MODEL.VendorAreaSettingsModel> objAreasetting = new List<MODEL.VendorAreaSettingsModel>();

            if (CompanyId != 0)
            {

                using (var con = new SqlConnection(ConnectionString))
                {
                    var result = con.QueryMultiple("Usp_ChangeVendorSetting", new { IsVendor = IsVendor, CompanyId = CompanyId }, commandType: System.Data.CommandType.StoredProcedure);
                    objVendorMainsetting = result.Read<MODEL.VendorMainSettingsModel>().FirstOrDefault();
                    objAreasetting = result.Read<MODEL.VendorAreaSettingsModel>().ToList(); 

                }

                if (objVendorMainsetting != null)
                {

                    objVendorMainsetting.VendorAreaList = objAreasetting;
                }

            }
            return objVendorMainsetting;

        }


        public MODEL.VendorMainSettingsModel GetVendorSetting(string UserId, int CompanyId)
        {
            MODEL.VendorMainSettingsModel obj = new MODEL.VendorMainSettingsModel();
            List<MODEL.VendorAreaSettingsModel> objVendorArea = new List<MODEL.VendorAreaSettingsModel>();

            if (CompanyId != 0)
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    string query = @"select Companies.Id CompanyId ,Companies.IsVendor  IsVendor from Companies  where Companies.Id = @CompanyId";
                    string query1 = @" SELECT [ServiceId] as Id ,[ServiceName],[Description],[IsActive] FROM [Tighten].[dbo].[VendorServices]";
                    obj = con.Query<MODEL.VendorMainSettingsModel>(query, new { CompanyId = CompanyId }).SingleOrDefault();
                    objVendorArea = con.Query<MODEL.VendorAreaSettingsModel>(query1).ToList();





                }
                obj.VendorAreaList = objVendorArea;

            }
            return obj;
        }


        public MODEL.VendorMainSettingsModel SaveUpdateVendorWorkingArea(MODEL.VendorMainSettingsModel model)
        {

            StringBuilder objIdList = new StringBuilder();
            StringBuilder objVendorServiceIdList = new StringBuilder();


            for (int i =0; i < model.VendorAreaList.Count;i++)
            {
                objIdList.Append(model.VendorAreaList[i].Id).Append(",");
                objVendorServiceIdList.Append(model.VendorAreaList[i].ServiceName).Append(",");

            }
            MODEL.VendorMainSettingsModel obj = new MODEL.VendorMainSettingsModel();
            List<MODEL.VendorAreaSettingsModel> objVendorArea = new List<MODEL.VendorAreaSettingsModel>();

            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("Usp_SaveUpdateVendorWorkingArea", new { CompanyId = model.CompanyId,
                    IdList = Convert.ToString(objIdList), ServiceIdList = Convert.ToString(objVendorServiceIdList) }, commandType: System.Data.CommandType.StoredProcedure);
                  objVendorArea = Result.Read<MODEL.VendorAreaSettingsModel>().ToList();
            }
             obj.VendorAreaList = objVendorArea;

            return obj;

        }
        public MODEL.VendorMainSettingsModel GetVendorSelectedAreas(int CompanyId)
        {
            MODEL.VendorMainSettingsModel obj = new MODEL.VendorMainSettingsModel();
            List<MODEL.VendorAreaSettingsModel> objVendorArea = new List<MODEL.VendorAreaSettingsModel>();

            if (CompanyId != 0)
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    string query1 = @"SELECT  [Id],[ServiceName],[CompanyId],[VendorServiceId],[CreatedDate],[IsActive] FROM [VendorSelectedServices] where CompanyId = @CompanyId and [IsActive] = 1";
                    objVendorArea = con.Query<MODEL.VendorAreaSettingsModel>(query1,new { CompanyId = CompanyId }).ToList();
                }
                obj.VendorAreaList = objVendorArea;

            }
            return obj;
        }


    }
}
