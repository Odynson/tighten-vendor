﻿using TIGHTEN.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using System.Web.Mvc;
using System.Xml.Linq;
using Dapper;
using System.Data.SqlClient;
using System.Data;


namespace TIGHTEN.DATA
{
    public class UsersAPI : BaseClass
    {
        public AppContext dbcontext = DependencyResolver.Current.GetService<AppContext>();//new AppContext();
        /// <summary>
        /// It fetches all the Users of particular Company
        /// </summary>
        /// <param name="UserId">It is the unique Id of logged in user</param>
        /// <returns>It returns the list of Users of a Company</returns>
        public List<UsersDetailsModel> getUsers(String UserId, bool checkEmailConfirmed, int CompanyId)
        {
            // Below line is commented because now we have CompanyId as parameter 
            //UserDetail userEntity = (from m in dbcontext.UserDetails where m.UserId == UserId select m).SingleOrDefault();

            //List<string> frEmail = (from ud in dbcontext.UserDetails
            //                        where ud.IsDeleted == false && ud.IsDeleted == false
            //                        select ud.Email).ToList();

            //List<UsersDetailsModel> result = (
            //        /* By RK 
            //        from roles in dbcontext.Roles
            //        from users in roles.Users
            //        join user in dbcontext.Users on users.UserId equals user.Id
            //        join userdetails in dbcontext.UserDetails on users.UserId equals userdetails.UserId
            //        join role in dbcontext.Roles on users.RoleId equals role.Id
            //        */
            //        from userdetails in dbcontext.UserDetails
            //        join userrole in dbcontext.UserCompanyRoles on userdetails.UserId equals userrole.UserId
            //        join role in dbcontext.Roles on userrole.RoleId equals role.RoleId
            //        where userrole.CompanyId == CompanyId && role.IsDeleted == false
            //        && userdetails.IsDeleted == false
            //        //where userdetails.CompanyId == CompanyId
            //        //&& userdetails.IsFreelancer == false
            //        && (checkEmailConfirmed == true ? userdetails.EmailConfirmed == true : userdetails.EmailConfirmed == userdetails.EmailConfirmed)
            //        select new UsersDetailsModel
            //        {
            //            UserId = userdetails.UserId,
            //            FirstName = userdetails.FirstName,
            //            LastName = string.IsNullOrEmpty(userdetails.LastName) ? "" : userdetails.LastName,
            //            Gender = (userdetails.Gender.HasValue) ? userdetails.Gender.Value : 0,
            //            Email = userdetails.Email,
            //            ProfilePhoto = (userdetails.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + userdetails.ProfilePhoto),
            //            PhoneNo = userdetails.PhoneNo,
            //            Address = userdetails.Address,
            //            DateOfBirth = userdetails.DateOfBirth,
            //            IsEmailConfirmed = userdetails.EmailConfirmed.Value,
            //            Value = userdetails.UserId,
            //            //Text = (userdetails.IsFreelancer == true ?    :   ) + userdetails.FirstName + " " + (string.IsNullOrEmpty(userdetails.LastName) ? "" : userdetails.LastName) + " ( " + userdetails.Email + " )",
            //            Text = userdetails.FirstName + " " + (string.IsNullOrEmpty(userdetails.LastName) ? "" : userdetails.LastName) + " ( " + userdetails.Email + " )",
            //            UserRole = role.Name,
            //            RoleId = role.RoleId,
            //            IsFreelancer = userdetails.IsFreelancer,
            //            IsActiveFreelancer = true
            //        })
            //        .Union
            //        (

            //        from frlncrInv in dbcontext.FreelancerInvitations
            //        join role in dbcontext.Roles on frlncrInv.RoleId equals role.RoleId
            //        where frlncrInv.CompanyId == CompanyId && role.IsDeleted == false
            //        //&& !frEmail.Contains(frlncrInv.FreelancerEmail)
            //        && frlncrInv.ProjectId == 0
            //        && frlncrInv.IskeyActive == true
            //        && frlncrInv.IsAccepted == false
            //        select new UsersDetailsModel
            //        {
            //            UserId = "",
            //            FirstName = frlncrInv.FreelancerEmail,
            //            LastName = "",
            //            Gender = 3,
            //            Email = frlncrInv.FreelancerEmail,
            //            ProfilePhoto = "Uploads/Default/profile.png",
            //            PhoneNo = "",
            //            Address = "",
            //            DateOfBirth = DateTime.Now,
            //            IsEmailConfirmed = false,
            //            Value = "",
            //            //Text = (userdetails.IsFreelancer == true ?    :   ) + userdetails.FirstName + " " + (string.IsNullOrEmpty(userdetails.LastName) ? "" : userdetails.LastName) + " ( " + userdetails.Email + " )",
            //            Text = frlncrInv.FreelancerEmail,
            //            UserRole = role.Name,
            //            RoleId = role.RoleId,
            //            IsFreelancer = true,
            //            IsActiveFreelancer = false
            //        })

            //        .ToList();

            List<UsersDetailsModel> result;
            using (var con = new SqlConnection(ConnectionString))
            {
                var Queryresult = con.QueryMultiple("usp_GetUsers", new { UserId = UserId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                //var Queryresult = con.QueryMultiple("usp_GetUsers", new { UserId = UserId, CompanyId = CompanyId, checkEmailConfirmed = checkEmailConfirmed }, commandType: CommandType.StoredProcedure);

                result = Queryresult.Read<UsersDetailsModel>().ToList();

            }
            result = result.OrderBy(x => x.LastName).ToList();

            return result;
        }

        /// <summary>
        /// It fetches all the Freelancers of particular Company
        /// </summary>
        /// <param name="UserId">It is the unique Id of logged in user</param>
        /// <returns>It returns the list of Freelancers of a Company</returns>
        public List<UsersDetailsModel> GetFreelancers(String UserId, bool checkEmailConfirmed, int CompanyId)
        {
            List<UsersDetailsModel> result = (from userdetails in dbcontext.UserDetails
                                              join userrole in dbcontext.UserCompanyRoles on userdetails.UserId equals userrole.UserId
                                              join role in dbcontext.Roles on userrole.RoleId equals role.RoleId
                                              where userrole.CompanyId == CompanyId && role.IsDeleted == false
                                              && userdetails.IsFreelancer == true
                                              && userdetails.IsDeleted == false
                                              && (checkEmailConfirmed == true ? userdetails.EmailConfirmed == true : userdetails.EmailConfirmed == userdetails.EmailConfirmed)
                                              select new UsersDetailsModel
                                              {
                                                  UserId = userdetails.UserId,
                                                  FirstName = userdetails.FirstName,
                                                  LastName = string.IsNullOrEmpty(userdetails.LastName) ? "" : userdetails.LastName,
                                                  Gender = (userdetails.Gender.HasValue) ? userdetails.Gender.Value : 0,
                                                  Email = userdetails.Email,
                                                  ProfilePhoto = (userdetails.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + userdetails.ProfilePhoto),
                                                  PhoneNo = userdetails.PhoneNo,
                                                  Address = userdetails.Address,
                                                  DateOfBirth = userdetails.DateOfBirth,
                                                  IsEmailConfirmed = userdetails.EmailConfirmed.Value,
                                                  Value = userdetails.UserId,
                                                  Text = userdetails.FirstName + " " + (string.IsNullOrEmpty(userdetails.LastName) ? "" : userdetails.LastName) + " ( " + userdetails.Email + " )",
                                                  UserRole = role.Name,
                                                  RoleId = role.RoleId
                                              }).ToList();
            return result;
        }



        public int getActiveUsersCount(int CompanyId)
        {
            List<string> frEmail = (from ud in dbcontext.UserDetails
                                    where ud.IsDeleted == false
                                    select ud.Email).ToList();

            var now = DateTime.Now;
            var first = new DateTime(now.Year, now.Month, 1);
            var last = first.AddMonths(1).AddDays(-1);


            List<UsersDetailsModel> result;
            using (var con = new SqlConnection(ConnectionString))
            {
                var Queryresult = con.QueryMultiple("Usp_GetActiveUsersCount",
                    new { CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);

                result = Queryresult.Read<UsersDetailsModel>().ToList();

            }

            return result.Union(result).Count();

        }
        public int getActiveUsersCount1(int CompanyId)
        {
            List<string> frEmail = (from ud in dbcontext.UserDetails
                                    where ud.IsDeleted == false
                                    select ud.Email).ToList();

            var now = DateTime.Now;
            var first = new DateTime(now.Year, now.Month, 1);
            var last = first.AddMonths(1).AddDays(-1);


            var result = (
                        from userdetails in dbcontext.UserDetails
                        join userrole in dbcontext.UserCompanyRoles on userdetails.UserId equals userrole.UserId
                        where userdetails.IsFreelancer == false && userrole.CompanyId == CompanyId
                        && userdetails.EmailConfirmed == true
                        && userdetails.IsDeleted == false
                        select new UsersDetailsModel
                        {
                            UserId = userdetails.UserId,
                            FirstName = userdetails.FirstName,
                        }
                   );

            var result2 = (
             from userdetails in dbcontext.UserDetails
             join userrole in dbcontext.UserCompanyRoles on userdetails.UserId equals userrole.UserId
             join todo in dbcontext.ToDos on userdetails.UserId equals todo.AssigneeId
             join todotimelog in dbcontext.TodoTimeLogs on todo.Id equals todotimelog.TodoId
             where userdetails.IsFreelancer == true && userrole.CompanyId == CompanyId
             && userdetails.EmailConfirmed == true && todo.IsDeleted == false
             && userdetails.IsDeleted == false && todotimelog.Duration > 0 &&
             ((first <= todotimelog.StartDateTime && todotimelog.StartDateTime <= last) || (first <= todotimelog.StopDateTime && todotimelog.StopDateTime <= last))
             select new UsersDetailsModel
             {
                 UserId = userdetails.UserId,
                 FirstName = userdetails.FirstName,
             }
        );

            return result.Union(result2).Count();



        }

        public UserModel getUser(string UserId, int CompanyId)
        {
            try
            {

                //UserModel result = (from ud in dbcontext.UserDetails
                //                        join ucr in dbcontext.UserCompanyRoles on ud.UserId equals ucr.UserId //into userCompanyRole
                //                                                                                              //from ucrs in userCompanyRole.DefaultIfEmpty()
                //                        join r in dbcontext.Roles on ucr.RoleId equals r.RoleId
                //                        join c in dbcontext.Companies on ucr.CompanyId equals c.Id
                //                        join un in dbcontext.Notifications on ud.UserId equals un.NotifyingUserId into uns
                //                        where ud.UserId == UserId && r.IsDeleted == false  //&& r.RoleId == 1
                //                        && ud.IsDeleted == false && ucr.CompanyId == CompanyId
                //                        let FirmName = (c.Name)
                //                        let NotificationCount = uns.Count(x => x.IsRead == false)
                //                        let IsAdminLoggedIn = (ud.ParentUserId == null ? "True" : "False")
                //                        let profilePhoto = (ud.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + ud.ProfilePhoto)
                //                        select new UserModel
                //                        {
                //                            userId = ud.UserId,
                //                            firstName = ud.FirstName,
                //                            lastName = ud.LastName,
                //                            gender = (ud.Gender.HasValue) ? ud.Gender.Value : 0,
                //                            email = ud.Email,
                //                            profilePhoto = profilePhoto,
                //                            CompanyId = c.Id,
                //                            CompanyName = FirmName,
                //                            SendNotificationEmail = (ud.SendNotificationEmail.HasValue) ? ud.SendNotificationEmail.Value : false,
                //                            Address = ud.Address,
                //                            Department = ud.Department,
                //                            Designation = ud.Designation,
                //                            Experience = ud.Experience,
                //                            Expertise = ud.Expertise,
                //                            PhoneNo = ud.PhoneNo,
                //                            NotificationCount = NotificationCount,
                //                            IsAdminLoggedIn = IsAdminLoggedIn,
                //                            originalProfilePhoto = ud.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Original/" + ud.ProfilePhoto,
                //                            DateOfBirth = ud.DateOfBirth,
                //                            AboutMe = ud.AboutMe,
                //                            TeamId = (ud.SelectedTeamId.HasValue) ? ud.SelectedTeamId.Value : 0,
                //                            Rate = ud.Rate,
                //                            Availabilty = ud.Availabilty,
                //                            isProfilePublic = ud.IsProfilePublic,
                //                            isFreelancer = ud.IsFreelancer,
                //                            Password = ud.Password

                //                        }).SingleOrDefault();



                UserModel result;
                using (var con = new SqlConnection(ConnectionString))
                {


                    result = con.Query<UserModel>("usp_GetUsers", new { CompanyId = CompanyId, UserId = UserId },
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }

                if (result == null && CompanyId == 0)
                {
                    //var usrdetail = (from usr in dbcontext.UserDetails
                    //                 where usr.UserId == UserId
                    //                 && usr.IsDeleted == false
                    //                 select usr).SingleOrDefault();

                    UserDetail usrdetail;
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlquery = @"select * from UserDetails usr where usr.UserId = @UserId and usr.IsDeleted = @false";
                        usrdetail = con.Query<UserDetail>(sqlquery).FirstOrDefault();
                    }

                    return new UserModel
                    {
                        userId = UserId,
                        firstName = usrdetail.FirstName,
                        gender = 0,
                        CompanyId = 0,
                        CompanyName = string.Empty,
                        NotificationCount = 0,
                        IsAdminLoggedIn = "false",
                        profilePhoto = usrdetail.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Original/" + usrdetail.ProfilePhoto,
                        originalProfilePhoto = usrdetail.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Original/" + usrdetail.ProfilePhoto,
                        TeamId = 0,
                        isFreelancer = usrdetail.IsFreelancer,
                        Password = usrdetail.Password
                    };

                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getUserName(string UserId)
        {
            try
            {
                string result = (from ud in dbcontext.UserDetails
                                 where ud.UserId == UserId
                                 && ud.IsDeleted == false
                                 select ud).SingleOrDefault().FirstName;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getUserEmail(string UserId)
        {
            try
            {
                string result = (from ud in dbcontext.UserDetails
                                 where ud.UserId == UserId
                                 && ud.IsDeleted == false
                                 select ud).SingleOrDefault().Email;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CardModel getUserCardInfo(string UserId, int CompanyId)
        {
            try
            {
                CardModel result = (from ud in dbcontext.UserDetails
                                    join ucr in dbcontext.UserCompanyRoles on ud.UserId equals ucr.UserId
                                    where ud.UserId == UserId
                                    && ud.IsDeleted == false
                                    && ucr.CompanyId == CompanyId
                                    select new CardModel
                                    {
                                        CreditCard = ud.CreditCard,
                                        CardHolderName = ud.CardHolderName,
                                        CvvNumber = ud.CvvNumber,
                                        ExpiryMonth = ud.ExpiryMonth,
                                        ExpiryYear = ud.ExpiryYear,
                                        AllowReckringPayment = ud.AllowReckringPayment
                                    }).SingleOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsUserCardInfoExist(string UserId, int CompanyId)
        {
            try
            {
                bool exist = false;
                int result = (from ud in dbcontext.PaymentDetails
                              where ud.CompanyId == CompanyId
                              && ud.IsDeleted == false
                              select ud
                              ).Count();

                if (result > 0)
                {
                    exist = true;
                }
                return exist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserModel saveUser(String UserId, String ParentUserId, AddNewUserModel model)
        {
            //var ParentCompany = (from e in dbcontext.UserDetails
            //                     join ec in dbcontext.Companies on e.CompanyId equals ec.Id
            //                     where e.UserId == ParentUserId
            //                     && e.IsDeleted == false
            //                     select new
            //                     {
            //                         CompanyId = e.CompanyId,
            //                         UserName = e.FirstName,
            //                         CompanyName = ec.Name
            //                     }
            //                     ).SingleOrDefault();

            //Int32 CompanyID = Convert.ToInt32(ParentCompany.CompanyId);
            //string UserName = ParentCompany.UserName;
            //string CompanyName = ParentCompany.CompanyName;

            //UserDetail oldUser = (from user in dbcontext.UserDetails
            //                      where user.Email == model.Email && user.CompanyId == CompanyID
            //                      && user.IsDeleted == false
            //                      select user).FirstOrDefault();
            //if (oldUser == null)
            //{
            //    TIGHTEN.ENTITY.Team team = new TIGHTEN.ENTITY.Team
            //    {
            //        TeamName = "My Personal",
            //        Description = "This is my personal team",
            //        CreatedBy = UserId,
            //        CreatedDate = DateTime.Now.ToUniversalTime(),
            //        CompanyId = CompanyID,
            //        ModifiedBy = UserId,
            //        ModifiedDate = DateTime.Now.ToUniversalTime(),
            //        IsDefault = true
            //    };
            //    // Save Default WOrkplace  */
            //    dbcontext.Teams.Add(team);
            //    dbcontext.SaveChanges();

            //    TeamUser teamusers = new TeamUser
            //    {
            //        UserId = UserId,
            //        CreatedBy = UserId,
            //        CreatedDate = DateTime.Now.ToUniversalTime(),
            //        TeamId = team.Id
            //    };
            //    dbcontext.TeamUsers.Add(teamusers);
            //    dbcontext.SaveChanges();


            //    // userCompanyRole
            //    UserCompanyRole objUserRole = new UserCompanyRole();
            //    objUserRole.CompanyId = CompanyID;
            //    // Temporarily giving static RoleId For Multiple roles of user
            //    //objUserRole.RoleId = 3;
            //    objUserRole.RoleId = model.RoleId;
            //    objUserRole.UserId = UserId;
            //    objUserRole.CreatedDate = DateTime.Now;
            //    dbcontext.UserCompanyRoles.Add(objUserRole);

            //    UserDetail userdetail = new UserDetail
            //    {
            //        Email = model.Email,
            //        UserId = UserId,
            //        CompanyId = CompanyID,
            //        ParentUserId = ParentUserId,
            //        SendNotificationEmail = true,
            //        SelectedTeamId = team.Id,
            //        FirstName = model.FirstName,
            //        LastName = model.LastName,
            //        EmailConfirmationCode = EmailUtility.GetUniqueKey(8),
            //        EmailConfirmed = false,
            //        Token = string.Empty,
            //        Rate = model.Rate == 0 ? 10 : model.Rate
            //    };
            //    /* Save User Details */
            //    dbcontext.UserDetails.Add(userdetail);

            //    #region Inserting default values for UserSettingConfig table
            //    /* getting all Settings for the user  */
            //    List<SettingConfig> setting = (from set in dbcontext.SettingConfigs
            //                                   where set.IsActive == true
            //                                   && set.NotificationName != "Subscription Expiry"
            //                                   && set.NotificationName != "Invoice Raise"
            //                                   select set).ToList();

            //    foreach (SettingConfig item in setting)
            //    {
            //        /* Inserting default values for UserSettingConfig table   */
            //            UserSettingConfig obj = new UserSettingConfig
            //            {
            //                CompanyId = CompanyID,
            //                UserId = UserId,
            //                NotificationId = item.Id,
            //                IsActive = true
            //            };
            //            dbcontext.UserSettingConfigs.Add(obj);
            //        //dbcontext.SaveChanges();
            //    }

            //    #endregion

            //    dbcontext.SaveChanges();
            //    return new UserModel() { userId = userdetail.UserId, email = userdetail.Email, EmailConfirmationCode = userdetail.EmailConfirmationCode, firstName = UserName, CompanyName = CompanyName };
            //}
            //else {
            //    return new UserModel() { DuplicateUserId = oldUser.UserId, firstName = oldUser.FirstName, lastName = oldUser.LastName, email = oldUser.Email, CompanyName = CompanyName };
            //}

            string EmailConfirmationCode = EmailUtility.GetUniqueKey(8);

            UserModel UserModel;
            using (var con = new SqlConnection(ConnectionString))
            {
                UserModel = con.Query<UserModel>("usp_InsertUser", new { ParentUserId, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName, UserId = UserId, RoleId = model.RoleId, EmailConfirmationCode = EmailConfirmationCode, Rate = model.Rate == 0 ? 10 : model.Rate }, commandType: CommandType.StoredProcedure).Single();
            }

            return UserModel;

        }

        public void UpdateUserRole(string ParentUserId, UserRoleModel model)
        {
            //Int32 CompanyID = Convert.ToInt32((from e in dbcontext.UserDetails
            //                                   where e.UserId == ParentUserId
            //                                   && e.IsDeleted == false
            //                                   select e.CompanyId).SingleOrDefault());

            //UserCompanyRole oldUser = (from user in dbcontext.UserCompanyRoles
            //                           where user.UserId == model.UserId && user.CompanyId == CompanyID
            //                           select user).FirstOrDefault();
            //oldUser.RoleId = model.RoleId;
            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("usp_UpdateUserRole", new { ParentUserId = ParentUserId, UserId = model.UserId, RoleId = model.RoleId }, commandType: CommandType.StoredProcedure);
            }
        }

        public void updateUser(UserModel model)
        {

            UserDetail userdetailentity = (from e in dbcontext.UserDetails
                                           where e.UserId == model.userId
                                           && e.IsDeleted == false
                                           select e).SingleOrDefault();

            userdetailentity.FirstName = model.firstName;
            userdetailentity.LastName = model.lastName;
            userdetailentity.IsSubscribed = model.isSubscribed;
            userdetailentity.Gender = model.gender;


            dbcontext.SaveChanges();

        }

        public void updateMyProfile(string userid, UserModel model)
        {

            UserDetail userdetailentity = (from e in dbcontext.UserDetails
                                           where e.UserId == userid
                                           && e.IsDeleted == false
                                           select e).SingleOrDefault();

            userdetailentity.FirstName = model.firstName;
            userdetailentity.LastName = model.lastName;
            userdetailentity.Gender = model.gender;
            userdetailentity.Address = model.Address;
            userdetailentity.Department = model.Department;
            userdetailentity.Designation = model.Designation;
            userdetailentity.Experience = model.Experience;
            userdetailentity.Expertise = model.Expertise;
            userdetailentity.PhoneNo = model.PhoneNo;
            userdetailentity.AboutMe = model.AboutMe;
            userdetailentity.DateOfBirth = model.DateOfBirth;

            dbcontext.SaveChanges();


        }

        //public string updateUserProfilePhoto(string FileName, string UserId)
        //{

        //    UserDetail userdetailentity = (from e in dbcontext.UserDetails
        //                                   where e.UserId == UserId
        //                                   && e.IsDeleted == false
        //                                   select e).SingleOrDefault();
        //    string PreviousPhoto = userdetailentity.ProfilePhoto;
        //    userdetailentity.ProfilePhoto = FileName;
        //    dbcontext.SaveChanges();
        //    return PreviousPhoto;

        //}

        public string updateUserProfilePhoto(string FileName, string UserId)
        {
            string PreviousPhoto;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select ProfilePhoto from UserDetails e where e.UserId = @UserId
                                    update UserDetails set ProfilePhoto  = @FileName where UserId  = @UserId";
                PreviousPhoto = con.Query<string>(sqlquery, new { FileName = FileName, UserId = UserId }).FirstOrDefault();
            }



            //UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                               where e.UserId == UserId
            //                               && e.IsDeleted == false
            //                               select e).SingleOrDefault();
            //string PreviousPhoto = userdetailentity.ProfilePhoto;
            //userdetailentity.ProfilePhoto = FileName;
            //dbcontext.SaveChanges();
            return PreviousPhoto;

        }

        public string clearProfilePhoto(string UserId)
        {

            UserDetail userdetailentity = (from e in dbcontext.UserDetails
                                           where e.UserId == UserId
                                           && e.IsDeleted == false
                                           select e).SingleOrDefault();
            string PreviousPhoto = userdetailentity.ProfilePhoto;
            userdetailentity.ProfilePhoto = null;
            dbcontext.SaveChanges();
            return PreviousPhoto;

        }

        public void DeleteUser(String UserId)
        {

            ////FLOW :-
            ////Discussion : Tasks, project, Teams needs to be deleted or not
            //int teamid = 0;
            //var  team = (from tm in dbcontext.Teams
            //                     where tm.CreatedBy == UserId && tm.TeamName == "My Personal"
            //                     select tm).SingleOrDefault();

            //if (team != null)
            //{
            //    teamid = team.Id;
            //    dbcontext.Teams.Remove(team);
            //    dbcontext.SaveChanges();
            //}

            //var teamUser = (from tmusr in dbcontext.TeamUsers
            //                            where tmusr.UserId == UserId && tmusr.Id == teamid
            //                select tmusr).SingleOrDefault();

            //if (teamUser != null)
            //{
            //    dbcontext.TeamUsers.Remove(teamUser);
            //    dbcontext.SaveChanges();
            //}

            //ENTITY.UserCompanyRole UserCompanyRole = (from usrCmpnyRole in dbcontext.UserCompanyRoles
            //                                          where usrCmpnyRole.UserId == UserId  
            //                                         select usrCmpnyRole).SingleOrDefault();

            //if (UserCompanyRole != null)
            //{
            //    dbcontext.UserCompanyRoles.Remove(UserCompanyRole);
            //    dbcontext.SaveChanges();
            //}

            //ENTITY.UserDetail UserDetail = (from usrDetl in dbcontext.UserDetails
            //                                where usrDetl.UserId == UserId
            //                                && usrDetl.IsDeleted == false
            //                                select usrDetl).SingleOrDefault();

            //if (UserDetail != null)
            //{
            //    if (UserDetail.IsFreelancer == false)
            //    {
            //        UserDetail.IsDeleted = true;
            //        dbcontext.SaveChanges();
            //    }
            //    else
            //    {
            //    }
            //}


            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update UserDetails set IsDeleted = 1 where UserId = @UserId and IsDeleted =0 and IsFreelancer = 0";
                con.Execute(sqlquery, new { UserId = UserId });
            }


        }

        public void updateSelectedTeam(String UserId, UserModel model)
        {
            //UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                               where e.UserId == UserId
            //                               && e.IsDeleted == false
            //                               select e).SingleOrDefault();

            //userdetailentity.SelectedTeamId = model.TeamId;
            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update UserDetails set SelectedTeamId = @TeamId where UserId = @UserId and IsDeleted = 0";
                con.Execute(sqlquery, new { UserId = UserId, TeamId = model.TeamId });
            }
        }

        public void updateSendEmailNotification(String UserId, UserModel model)
        {
            UserDetail userdetailentity = (from e in dbcontext.UserDetails
                                           where e.UserId == UserId
                                           && e.IsDeleted == false
                                           select e).SingleOrDefault();

            userdetailentity.SendNotificationEmail = model.SendNotificationEmail;
            dbcontext.SaveChanges();
        }

        public async Task<string> sendMainAccountRegistrationCreationConfirmationEmail(string destinationEmail, string userId, string confirmEmailtoken)
        {
            var url = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);

            var callbackUrl = url.Action("AccountVerification", "Account",
                new { userId = userId, code = confirmEmailtoken },
                protocol: HttpContext.Current.Request.Url.Scheme
                );


            String emailbody = "<p>You Have been registered as a new User</p>";
            emailbody += "<p>Please confirm your account by clicking this link </p>";
            emailbody += "<a href=\"" + callbackUrl + "\">link</a>";

            /*Send Email to user and inform about new account created*/
            await EmailUtility.Send(destinationEmail, "New Account Created", emailbody);

            return "true";

        }

        public async Task<string> sendAccountCreationConfirmationEmail(string destinationEmail, string userId, string token)
        {
            var url = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);

            var callbackUrl = url.Action("ConfirmEmail", "Account",
                new { userId = userId, code = token },
                protocol: HttpContext.Current.Request.Url.Scheme
                );


            String emailbody = "<p>A new account has been created for you</p>";
            emailbody += "<p>Please confirm your account by clicking this link and set your new password</p>";
            emailbody += "<a href=\"" + callbackUrl + "\">link</a>";

            /*Send Email to user and inform about new account created*/
            await EmailUtility.Send(destinationEmail, "New Account Created", emailbody);

            return "true";

        }

        public async Task<string> sendForgetPasswordEmail(string destinationEmail, string userId, string token)
        {
            var url = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);

            var callbackUrl = url.Action("ConfirmEmail", "Account",
                new { userId = userId, code = token },
                protocol: HttpContext.Current.Request.Url.Scheme
                );


            String emailbody = "<p>Forgot Password Email</p>";
            emailbody += "<p>Please reset your password and create a new password by clicking this link </p>";
            emailbody += "<a href=\"" + callbackUrl + "\">link</a>";

            /*Send Email to user and inform about new account created*/
            await EmailUtility.Send(destinationEmail, "Forgot Password", emailbody);
            return "True";



        }

        public async Task<string> sendEmailChangeConfirmationEmail(string destinationEmail, string userId, string token)
        {
            var url = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);

            var callbackUrl = url.Action("ConfirmEmailChange", "Account",
                new { userId = userId, code = token },
                protocol: HttpContext.Current.Request.Url.Scheme
                );


            String emailbody = "<p>Your account has been linked to your new email address.</p>";
            emailbody += "<p>Please confirm your account by clicking this link.</p>";
            emailbody += "<a href=\"" + callbackUrl + "\">link</a>";
            emailbody += "<p>Use this Email ID to login to your Tighten account.</p>";

            /*Send Email to user and inform about Email/UserName change */
            await EmailUtility.Send(destinationEmail, "Email Changed", emailbody);

            return "true";

        }

        /// <summary>
        /// Getting All Projects of User's company
        /// </summary>
        /// <param Name="CompanyId"> It is the unique id of the company whose projects will be fetched
        /// </param>
        public List<MODEL.ProjectModel.Project> GetAllProjectsOfCompany(MODEL.FreelancerInfoToSendInvitationModel model)
        {
            List<MODEL.ProjectModel.Project> list = (from prjct in dbcontext.Projects
                                                     where prjct.CompanyId == model.CompanyId && prjct.IsDeleted == false
                                                     select new MODEL.ProjectModel.Project
                                                     {
                                                         ProjectId = prjct.Id,
                                                         ProjectName = prjct.Name,
                                                         ProjectDescription = prjct.Description,
                                                         TeamId = prjct.TeamId,
                                                         isTicked = model.chkInvitationType == "" ? 0 :
                                                                           ((from usr in dbcontext.UserDetails
                                                                             join freelncr in dbcontext.FreelancerInvitations on usr.Email equals freelncr.FreelancerEmail
                                                                             where freelncr.CompanyId == model.CompanyId
                                                                             && usr.IsDeleted == false
                                                                             && freelncr.ProjectId == prjct.Id
                                                                             && freelncr.SentBy == model.UserId
                                                                             && (model.chkInvitationType == "email" ? usr.Email == model.EmailOrHandle : usr.UserName == model.EmailOrHandle)
                                                                             select freelncr.Id).FirstOrDefault()
                                                                             )
                                                     }).ToList();

            return list;
        }

        public List<MODEL.MailSendToFreelancerOnInvitationSent> insertFreelancer(MODEL.FreelancerInvitationModel model, string url)
        {
            MODEL.ParentCompanyInfo ParentCompany;
            string RoleNameForFreelancer;

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_ParentCompany = @"select e.FirstName as UserName,ec.Name as CompanyName from UserDetails e
                                inner join Companies ec on e.CompanyId = ec.Id where e.UserId = @SentBy and e.IsDeleted = 0 ";
                ParentCompany = con.Query<MODEL.ParentCompanyInfo>(sqlquery_ParentCompany, new { SentBy = model.SentBy }).SingleOrDefault();

                string sqlquery_RoleName = @"select rol.Name from Roles rol where rol.RoleId = @RoleId and rol.IsDeleted = 0 ";
                RoleNameForFreelancer = con.Query<string>(sqlquery_RoleName, new { RoleId = model.RoleId }).FirstOrDefault();
            }

            string ParentUserName = ParentCompany.UserName;
            string ParentCompanyName = ParentCompany.CompanyName;


            List<MODEL.MailSendToFreelancerOnInvitationSent> ReturnList = new List<MailSendToFreelancerOnInvitationSent>();
            string email = string.Empty;
            string UserId = string.Empty;
            string keyy = UTILITIES.EmailUtility.GetUniqueKey(16);

            string SendInvitationTo = "SendInvitationToFreelancer";

            if (model.invitationType != "email")
            {
                string[] EmailOrHandleArray = model.EmailOrHandle.Split('@');
                model.EmailOrHandle = EmailOrHandleArray[EmailOrHandleArray.Length - 1];

                SendInvitationTo = "SendInvitationToRegisteredFreelancer";
            }
            else
            {
                email = model.EmailOrHandle;
                int FreelancerEsixtCount = 0;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlqeuery_FreelancerEsixtCount = @"select COUNT(usr.UserId) from UserDetails usr inner join UserCompanyRoles usrcom on usr.UserId = usrcom.UserId where usrcom.CompanyId = @CompanyId and usr.Email = @EmailOrHandle and usr.IsDeleted = 0";
                    FreelancerEsixtCount = con.Query<int>(sqlqeuery_FreelancerEsixtCount, new { CompanyId = model.CompanyId, EmailOrHandle = model.EmailOrHandle, }).FirstOrDefault();
                }

                if (FreelancerEsixtCount > 0)
                {
                    SendInvitationTo = "SendInvitationToRegisteredFreelancer";
                }

            }

            UserDetail GetUsr;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_GetUsr = @"select * from UserDetails usr where usr.Email = case when @invitationType = 'email' then @EmailOrHandle else usr.Email end 
                                           and usr.UserName = case when @invitationType = 'email'	then usr.UserName	else @EmailOrHandle  end 
                                           and usr.IsDeleted = 0       
                                            ";
                GetUsr = con.Query<UserDetail>(sqlquery_GetUsr, new { invitationType = model.invitationType, EmailOrHandle = model.EmailOrHandle }).FirstOrDefault();
                if (GetUsr != null)
                {
                    email = GetUsr.Email;
                    UserId = GetUsr.UserId;
                }

                string sqlquery_InsertFreelancer = @"insert into FreelancerInvitations(FreelancerEmail,CompanyId,SentBy,RoleId,IskeyActive,SentDate,IsAccepted,IsJobInvitation,ProjectId,Activationkey)  
                                                     values(@email,@CompanyId,@SentBy,@RoleId,1,GETDATE(),0,0,0,@keyy )  ";

                if (model.IsHireMe == true)
                {
                    sqlquery_InsertFreelancer = @"insert into FreelancerInvitations(FreelancerEmail,CompanyId,SentBy,RoleId,IskeyActive,SentDate,IsAccepted,IsJobInvitation,UserRate,WeeklyLimit,JobTitle,JobDescription,ProjectId,Activationkey )  
                                                  values(@email,@CompanyId,@SentBy,@RoleId,1,GETDATE(),0,1,@UserRate,@WeeklyLimit,@JobTitle,@JobDescription,0,@keyy ) ";
                }


                con.Execute(sqlquery_InsertFreelancer, new { email = email, CompanyId = model.CompanyId, SentBy = model.SentBy, RoleId = model.RoleId, UserRate = model.UserRate, WeeklyLimit = model.WeeklyLimit, JobTitle = model.JobTitle, JobDescription = model.JobDescription, keyy = keyy });

                string sqlquery_InsertNotification = @"insert into Notifications(Name,ProjectId,CreatedBy,CreatedDate,IsRead,Type,NotifyingUserId ) values('has Invited you for project :',0, @SentBy,GETDATE(),0,10,@UserId )";

                if (!string.IsNullOrEmpty(UserId))
                {
                    con.Execute(sqlquery_InsertNotification, new { SentBy = model.SentBy, UserId = UserId });
                }


            }


            #region Mail-Body

            var callbackUrl = url + "#/confirmfreelancer/" + keyy;

            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == SendInvitationTo)
                                 //select r;
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ParentUserName", ParentUserName).Replace("@@ParentCompanyName", ParentCompanyName).Replace("@@callbackUrl", callbackUrl).Replace("@@RoleName", RoleNameForFreelancer);



            #endregion

            MODEL.MailSendToFreelancerOnInvitationSent mdl = new MailSendToFreelancerOnInvitationSent();
            mdl.emailbody = emailbody;
            ReturnList.Add(mdl);


            return ReturnList;
        }


        public void ThreadForSendMailToSendInvitationToFreelancer(string EmailOrHandle, List<MODEL.MailSendToFreelancerOnInvitationSent> mail)
        {
            foreach (var item in mail)
            {
                EmailUtility.SendMailInThread(EmailOrHandle, "New Invitation ", item.emailbody);
            }
        }


        public MODEL.CompanyAndFreelancerModel GetCompanyAndFreelancerName(string invitationType, string EmailOrHandle, int CompanyId)
        {
            try
            {
                MODEL.CompanyAndFreelancerModel obj = new CompanyAndFreelancerModel();

                //var CompanyName = (from com in dbcontext.Companies
                //                   where com.Id == CompanyId
                //                   select com.Name).FirstOrDefault();
                string CompanyName;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"select Name from Companies where Id = @CompanyId";
                    CompanyName = con.Query(sqlquery, new { CompanyId = CompanyId }).FirstOrDefault();
                }

                string FreelancerName = string.Empty;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"select FirstName from UserDetails where Email = case when invitationType = 'email' then @EmailOrHandle else Email end and UserName = case when invitationType <> 'email' then @EmailOrHandle else UserName end and IsDeleted = 0 ";
                    FreelancerName = con.Query(sqlquery, new { CompanyId = CompanyId }).FirstOrDefault();
                }


                //if (invitationType == "email")
                //{
                //     FreelancerName = (from fr in dbcontext.UserDetails
                //                          where fr.Email == EmailOrHandle
                //                          && fr.IsDeleted == false
                //                       select fr.FirstName).FirstOrDefault();

                //}
                //else
                //{
                //     FreelancerName = (from fr in dbcontext.UserDetails
                //                          where fr.UserName == EmailOrHandle
                //                          && fr.IsDeleted == false
                //                       select fr.FirstName).FirstOrDefault();

                //}

                obj.CompanyName = CompanyName;
                obj.FreelancerName = FreelancerName;

                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public void UpdateUserFirstName(UserModel model)
        {

            string query = @"update UserDetails set  FirstName = @firstName where UserDetails.UserId = @userId ";


            using (var con = new SqlConnection(ConnectionString))
            {

                con.Query(query, new { firstName = model.firstName, UserId = model.userId });


            }


            //    UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                                   where e.UserId == model.userId
            //                                   && e.IsDeleted == false
            //                                   select e).SingleOrDefault();

            //userdetailentity.FirstName = model.firstName;
            //dbcontext.SaveChanges();
        }


        public void UpdateUserLastName(UserModel model)
        {

            string query = @"update UserDetails set  LastName = @lastName where UserDetails.UserId = @userId ";


            using (var con = new SqlConnection(ConnectionString))
            {

                con.Query(query, new { lastName = model.lastName, UserId = model.userId });


            }

            //UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                               where e.UserId == model.userId
            //                               && e.IsDeleted == false
            //                               select e).SingleOrDefault();

            //userdetailentity.LastName = model.lastName;
            //dbcontext.SaveChanges();
        }

        public void UpdateUserAddress(UserModel model)
        {

            string query = @"update UserDetails set  Address = @Address where UserDetails.UserId = @userId ";


            using (var con = new SqlConnection(ConnectionString))
            {

                con.Query(query, new { Address = model.Address, UserId = model.userId });


            }



            //UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                               where e.UserId == model.userId
            //                               && e.IsDeleted == false
            //                               select e).SingleOrDefault();

            //userdetailentity.Address = model.Address;
            //dbcontext.SaveChanges();
        }

        public void UpdateUserPhoneNo(UserModel model)
        {

            string query = @"update UserDetails set  PhoneNo = @PhoneNo where UserDetails.UserId = @userId ";


            using (var con = new SqlConnection(ConnectionString))
            {
                con.Query(query, new { PhoneNo = model.PhoneNo, UserId = model.userId });

            }




            //UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                               where e.UserId == model.userId
            //                               && e.IsDeleted == false
            //                               select e).SingleOrDefault();

            //userdetailentity.PhoneNo = model.PhoneNo;
            //dbcontext.SaveChanges();
        }

        public void UpdateUserDesignation(UserModel model)
        {

            string query = @"update UserDetails set  Designation = @Designation where UserDetails.UserId = @userId ";


            using (var con = new SqlConnection(ConnectionString))
            {
                con.Query(query, new { Designation = model.Designation, UserId = model.userId });

            }



            //UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                               where e.UserId == model.userId
            //                               && e.IsDeleted == false
            //                               select e).SingleOrDefault();

            //userdetailentity.Designation = model.Designation;
            //dbcontext.SaveChanges();
        }

        public void UpdateUserAboutMe(UserModel model)
        {
            string query = @"update UserDetails set  AboutMe = @AboutMe where UserDetails.UserId = @userId ";


            using (var con = new SqlConnection(ConnectionString))
            {
                con.Query(query, new { AboutMe = model.AboutMe, UserId = model.userId });

            }


            //UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                               where e.UserId == model.userId
            //                               && e.IsDeleted == false
            //                               select e).SingleOrDefault();

            //userdetailentity.AboutMe = model.AboutMe;
            //dbcontext.SaveChanges();
        }

        public void UpdateUserGender(UserModel model)
        {


            string query = @"update UserDetails set  Gender = @gender where UserDetails.UserId = @userId ";


            using (var con = new SqlConnection(ConnectionString))
            {
                con.Query(query, new { gender = model.gender, UserId = model.userId });

            }

            //UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                               where e.UserId == model.userId
            //                               && e.IsDeleted == false
            //                               select e).SingleOrDefault();

            //userdetailentity.Gender = model.gender;
            //dbcontext.SaveChanges();
        }

        public void UpdateUserDateOfBirth(UserModel model)
        {

            string query = @"update UserDetails set  DateOfBirth = @DateOfBirth where UserDetails.UserId = @userId ";


            using (var con = new SqlConnection(ConnectionString))
            {
                con.Query(query, new { DateOfBirth = model.DateOfBirth, UserId = model.userId });

            }


            //UserDetail userdetailentity = (from e in dbcontext.UserDetails
            //                               where e.UserId == model.userId
            //                               && e.IsDeleted == false
            //                               select e).SingleOrDefault();

            //userdetailentity.DateOfBirth = model.DateOfBirth;
            //dbcontext.SaveChanges();
        }

        public bool CheckIfCompanyIsVendor(int CompanyId)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var Queryresult = con.ExecuteScalar("usp_CheckIfCompanyIsVendor",
                    new { CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                return Convert.ToBoolean(Queryresult.ToString());
            }

        }


        public bool UpdateGeneralCompanyToVendorCompany(string UserId, int CompanyId)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                var Queryresult = con.ExecuteScalar("usp_EnableGeneralCompanyAsVendorCompany",
                    new { UserId = UserId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                return Convert.ToBoolean(Queryresult.ToString());
            }

        }


        public List<UserRoleModel> GetUsersForTeam(int CompanyId)
        {
            List<UserRoleModel> ObjNewUserList = new List<UserRoleModel>();

            using (var con = new SqlConnection(ConnectionString))
            {
                //string query = @"select UD.FirstName+' '+UD.LastName AS Name, UD.RoleId,UD.UserId from [dbo].[UserDetails]  UD WHERE UD.IsActive=1 and UD.IsDeleted=0 
                // and UD.CompanyId=@CompanyId AND UD.EmailConfirmed=1 AND (UD.Password is not null and UD.Password <> '') 
                // and RoleId Is NOT NULL";
                string query = @"select UD.FirstName+' '+UD.LastName AS Name, UD.RoleId,UD.UserId from [dbo].[UserDetails]  UD WHERE UD.IsActive=1 and UD.IsDeleted=0 
AND( UD.RoleId IS NULL OR UD.RoleId ='') and UD.CompanyId=@CompanyId AND UD.EmailConfirmed=1 AND (UD.Password is not null and UD.Password <> '')
 AND  (UD.Token is not null and UD.Token <> '') ";
                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId });
                ObjNewUserList = Result.Read<UserRoleModel>().ToList();

            }
            return ObjNewUserList;

        }

    }
}
