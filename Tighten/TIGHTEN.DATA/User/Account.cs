﻿using TIGHTEN.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using System.Threading.Tasks;
using System.Globalization;
using System.Web.Security;
using System.Drawing;
using System.Threading;
using System.Xml.Linq;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using Stripe;
using TIGHTEN.UTILITIES.GeneralUtilities;
using System.Configuration;

namespace TIGHTEN.DATA
{
    public class Account : BaseClass
    {
        AppContext dbcontext;

        public UserModel Register(RegisterBindingModel model)
        {
            dbcontext = new AppContext();

            //UserDetail oldUser = (from user in dbcontext.UserDetails
            //                      where user.Email == model.Email && user.IsDeleted == false
            //                      select user).FirstOrDefault();

            UserDetail oldUser;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = "select * from UserDetails where Email = @Email and IsDeleted = 0";
                oldUser = con.Query<UserDetail>(sqlquery, new { Email = model.Email }).FirstOrDefault();
            }

            if (oldUser != null)
            {
                return new UserModel() { userId = string.Empty, firstName = string.Empty, email = model.Email, EmailConfirmationCode = string.Empty, CompanyId = 0, TeamId = 0 };
            }


            #region Old code using EntityFramework
            //UserDetail userdetail = new UserDetail
            //{
            //    Email = model.Email,
            //    Password = model.Password,
            //    FirstName = model.FirstName,
            //    LastName = model.LastName,
            //    UserId = Guid.NewGuid().ToString(),
            //    EmailConfirmed = false,
            //    IsSubscribed = false,
            //    //LastName = model.LastName,
            //    Token = string.Empty,
            //    EmailConfirmationCode = EmailUtility.GetUniqueKey(8),
            //    Rate = model.Rate == 0 ? 10 : model.Rate,
            //    IsFreelancer = model.isFreelancer,
            //    IsProfilePublic = true,
            //    //CreditCard = model.CreditCard,
            //    //CvvNumber = model.CvvNumber
            //};

            ////string Company_Name = "FreelancerCompany";    //  to use this in team table instead of objcompany.Id
            ////string UserName = model.UserName;

            /////* If User Is Company Not Freelancer */
            ////if (model.isFreelancer == false)
            ////{
            ////    Company_Name = model.CompanyName;
            ////    UserName = string.Empty;
            ////}

            ///* Save Company Details */
            //Company objcompany = new Company();
            //objcompany.Name = model.CompanyName;
            //objcompany.CreatedDate = DateTime.Now;
            //objcompany.CreatedBy = userdetail.UserId;
            //dbcontext.Companies.Add(objcompany);
            //var companyId = dbcontext.SaveChanges();

            //// userCompanyRole
            //UserCompanyRole objUserRole = new UserCompanyRole();
            //objUserRole.CompanyId = objcompany.Id;
            //objUserRole.RoleId = model.RoleId != 0 ? model.RoleId : 1;   // 1 is the super admin   4 is the freelancer
            //objUserRole.UserId = userdetail.UserId;
            //objUserRole.CreatedDate = DateTime.Now;
            //dbcontext.UserCompanyRoles.Add(objUserRole);

            //Package pack = (from p in dbcontext.Packages
            //                    //where p.Id == 1
            //                select p).FirstOrDefault();

            //if (pack != null)
            //{
            //    // Save Default Subscription  */
            //    TIGHTEN.ENTITY.Subscription Subscription = new TIGHTEN.ENTITY.Subscription
            //    {
            //        PackageID = pack.Id,
            //        UserId = userdetail.UserId,
            //        CompanyId = objcompany.Id,
            //        StartDate = DateTime.Now,
            //        EndDate = DateTime.Now.AddDays(pack.DurationInDays),
            //        IsActive = true
            //    };
            //    dbcontext.Subscriptions.Add(Subscription);
            //    var SubscriptionId = dbcontext.SaveChanges();
            //}

            //#region Inserting default values for UserSettingConfig table
            ///* getting all Settings for the user  */
            //List<SettingConfig> setting = (from set in dbcontext.SettingConfigs
            //                               where set.IsActive == true
            //                               select set).ToList();

            //foreach (SettingConfig item in setting)
            //{
            //    /* Inserting default values for UserSettingConfig table   */
            //    UserSettingConfig obj = new UserSettingConfig
            //    {
            //        CompanyId = objcompany.Id,
            //        UserId = userdetail.UserId,
            //        NotificationId = item.Id,
            //        IsActive = true
            //    };
            //    dbcontext.UserSettingConfigs.Add(obj);
            //    //dbcontext.SaveChanges();
            //}

            //#endregion


            //#region Inserting default permissions for all roles in Permission table

            //List<Permission> permissions = (from per in dbcontext.Permissions
            //                                where per.CompanyId == 0
            //                                && per.isDeleted == false
            //                                select per).ToList();

            //foreach (var PermissionItem in permissions)
            //{
            //    Permission PermObj = new Permission();
            //    PermObj.UserRole = PermissionItem.UserRole;
            //    PermObj.Link = PermissionItem.Link;
            //    PermObj.MenuId = PermissionItem.MenuId;
            //    PermObj.CompanyId = objcompany.Id;
            //    PermObj.isActive = true;
            //    PermObj.isDeleted = false;

            //    dbcontext.Permissions.Add(PermObj);
            //}
            ////dbcontext.SaveChanges();

            //#endregion


            //#region Inserting default Internal permissions for all roles in InternalPermission table

            //List<InternalPermission> InternalPermissions = (from IntPer in dbcontext.InternalPermissions
            //                                                where IntPer.CompanyId == 0
            //                                                && IntPer.isDeleted == false
            //                                                select IntPer).ToList();

            //foreach (var InternalPermissionItem in InternalPermissions)
            //{
            //    InternalPermission PermObj = new InternalPermission();
            //    PermObj.PermissionName = InternalPermissionItem.PermissionName;
            //    PermObj.MenuId = InternalPermissionItem.MenuId;
            //    PermObj.RoleId = 1;
            //    PermObj.CompanyId = objcompany.Id;
            //    PermObj.isActive = true;
            //    PermObj.isDeleted = false;

            //    dbcontext.InternalPermissions.Add(PermObj);
            //}
            ////dbcontext.SaveChanges();

            //#endregion



            //// Save Default Team  */
            //TIGHTEN.ENTITY.Team team = new TIGHTEN.ENTITY.Team
            //{
            //    TeamName = "My Personal",
            //    Description = "This is my personal team",
            //    CreatedBy = userdetail.UserId,
            //    CreatedDate = DateTime.Now,
            //    CompanyId = objcompany.Id,
            //    IsDefault = true
            //};
            //dbcontext.Teams.Add(team);
            //var teamId = dbcontext.SaveChanges();

            ////Adding the default user in the team
            //TeamUser teamusers = new TeamUser
            //{
            //    UserId = userdetail.UserId,
            //    CreatedBy = userdetail.UserId,
            //    CreatedDate = DateTime.Now,
            //    TeamId = team.Id
            //};
            //dbcontext.TeamUsers.Add(teamusers);
            //var teamUserId = dbcontext.SaveChanges();

            //userdetail.SelectedTeamId = team.Id;
            //userdetail.CompanyId = objcompany.Id;
            ////userdetail.UserName = UserName;
            ///* Save User Details */
            //dbcontext.UserDetails.Add(userdetail);
            //var Id = dbcontext.SaveChanges();
            #endregion

            string UserId = Guid.NewGuid().ToString();
            string EmailConfirmationCode = EmailUtility.GetUniqueKey(8);

            UserModel newUser;
            using (var con = new SqlConnection(ConnectionString))
            {
                newUser = con.Query<UserModel>("usp_RegisterInternalUser",
                    new
                    {
                        Email = model.Email,
                        Password = model.Password,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        UserId = UserId,
                        EmailConfirmationCode = EmailConfirmationCode,
                        Rate = model.Rate == 0 ? 10 : model.Rate,
                        CompanyName = model.CompanyName,
                        CurrentDate = DateTime.Now
                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }


            //return new UserModel() { userId = userdetail.UserId, firstName = userdetail.FirstName, email = userdetail.Email, EmailConfirmationCode = userdetail.EmailConfirmationCode, CompanyId = objcompany.Id, TeamId = team.Id, RoleId = objUserRole.RoleId.Value };

            return newUser;
        }

        public bool chkUserNameExist(FreelancerInfoForRegistration model)
        {
            dbcontext = new AppContext();

            //var IsUserNameExist = (from usrname in dbcontext.UserDetails
            //                       where usrname.UserName == model.UserName && usrname.IsDeleted == false
            //                       select usrname).SingleOrDefault();

            UserDetail IsUserNameExist;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = "select * from UserDetails usrname where usrname.UserName = @UserName and usrname.IsDeleted = 0";
                IsUserNameExist = con.Query<UserDetail>(sqlquery, new { UserName = model.UserName }).SingleOrDefault();
            }

            if (IsUserNameExist != null)
                return true;
            else
                return false;
        }

        public UserModel ConfirmRegistration(string userId, string code)
        {
            dbcontext = new AppContext();

            //UserDetail oldUser = (from user in dbcontext.UserDetails
            //                      where user.UserId == userId && user.EmailConfirmationCode == code && user.IsDeleted == false
            //                      select user).FirstOrDefault();

            UserDetail oldUser;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = "select * from UserDetails where UserId = @userId and EmailConfirmationCode = @code and IsDeleted = 0";
                oldUser = con.Query<UserDetail>(sqlquery, new { userId = userId, code = code }).FirstOrDefault();
            }

            if (oldUser != null)
            {
                //oldUser.EmailConfirmed = true;
                //oldUser.EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
                //dbcontext.SaveChanges();

                string EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"update UserDetails set EmailConfirmed = 1,EmailConfirmationCode = @EmailConfirmationCode where
                        UserId = @userId and EmailConfirmationCode = @code and IsDeleted = 0";
                    con.Query<int>(sqlquery, new { EmailConfirmationCode = EmailConfirmationCode, userId = userId, code = code }).FirstOrDefault();
                }

                return new UserModel() { userId = oldUser.UserId, EmailConfirmationCode = EmailConfirmationCode };
            }
            else
            {
                return new UserModel() { userId = string.Empty, EmailConfirmationCode = string.Empty, TeamId = -1 };
            }
        }

        public UserModel ForgotPassword(string Email)
        {
            try
            {
                dbcontext = new AppContext();
                //UserDetail aspnetuser = (from m in dbcontext.UserDetails
                //                         where m.Email == Email && m.IsDeleted == false
                //                         select m).SingleOrDefault();

                UserDetail aspnetuser;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlQuery = "Select * from UserDetails where Email=@Email and IsDeleted=0";
                    aspnetuser = con.Query<UserDetail>(sqlQuery, new { Email = Email }).FirstOrDefault();
                }


                if (aspnetuser == null)
                {
                    return new UserModel() { userId = string.Empty, EmailConfirmationCode = string.Empty, firstName = string.Empty };
                }
                else
                {
                    //aspnetuser.EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
                    //aspnetuser.EmailConfirmationCodeCreatedDate = DateTime.Now;
                    //dbcontext.SaveChanges();
                    string EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
                    DateTime EmailConfirmationCodeCreatedDate = DateTime.Now;
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlQuery = "update UserDetails set EmailConfirmationCode = @EmailConfirmationCode,EmailConfirmationCodeCreatedDate = @EmailConfirmationCodeCreatedDate where Email=@Email and IsDeleted=0";
                        con.Query<int>(sqlQuery, new { Email = Email, EmailConfirmationCode = EmailConfirmationCode, EmailConfirmationCodeCreatedDate = EmailConfirmationCodeCreatedDate }).FirstOrDefault();
                    }
                    return new UserModel() { userId = aspnetuser.UserId, EmailConfirmationCode = EmailConfirmationCode, firstName = aspnetuser.FirstName };
                }
            }
            catch (Exception ex)
            {
                return new UserModel() { userId = string.Empty, EmailConfirmationCode = string.Empty, TeamId = -1 };
            }
        }

        public bool setPassword(SetPasswordBindingModel model)
        {
            dbcontext = new AppContext();
            string EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
            int count = 0;
            using (var con = new SqlConnection(ConnectionString))
            {
                SetPasswordBindingModel objSetPassModal = new SetPasswordBindingModel();
                UserDetail objModalUserDetail = new UserDetail();
                UserDetail objmodalUser = new UserDetail();

                string sqlquery1 = @"select * from UserDetails where UserId = @UserId";
                objmodalUser = con.Query<UserDetail>(sqlquery1, new { UserId = model.UserId, }).FirstOrDefault();

                #region Stripe
                // Saving StripeAccount Settings and info in StripeAccountDetail table
                StripeAccountDetail accdetail = new StripeAccountDetail
                {
                    UserId = objmodalUser.UserId,
                    Email = objmodalUser.Email,
                    FirstName = objmodalUser.FirstName,
                    LastName = objmodalUser.LastName,
                    Managed = true
                };

                // setting the stripe api key
                StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["stripeapikey"].ToString());
                // creating the object to create new account
                var account = new StripeAccountCreateOptions();
                account.Email = objmodalUser.Email;
                account.Country = "US";
                account.BusinessName = objmodalUser.FirstName + " " + objmodalUser.LastName;
                account.Type = StripeAccountType.Custom;
                // Calling the stripe account creation api
                var accountService = new StripeAccountService();
                StripeAccount response = accountService.Create(account);

                objmodalUser.StripeUserId = response.Id;
                objmodalUser.IsStripeAccountVerified = false;


                #endregion
                if (objmodalUser.StripeUserId != null)
                {
                    string sqlquery = @"update UserDetails set Password = @NewPassword, EmailConfirmationCode = @EmailConfirmationCode,StripeUserId = @StripeUserId 
                                    ,IsStripeAccountVerified = 1 where UserId =@UserId 
                                     and EmailConfirmationCode = @Code and IsDeleted = 0 ";

                    count = con.Query<int>(sqlquery, new
                    {
                        NewPassword = model.NewPassword,
                        EmailConfirmationCode = EmailConfirmationCode,
                        UserId = model.UserId,
                        Code = model.Code,
                        StripeUserId = objmodalUser.StripeUserId,
                        Email = objmodalUser.Email
                    }).FirstOrDefault();
                }

            }

            if (count >= 0)
                return true;
            else
                return false;

        }


        public bool SetPasswordForLinkedInUser(SetPasswordBindingModel model)
        {
            dbcontext = new AppContext();

            //UserDetail oldUser = (from user in dbcontext.UserDetails
            //                      where user.UserId == model.UserId && user.Email == model.EmailId && user.IsDeleted == false
            //                      select user).FirstOrDefault();
            //if (oldUser != null)
            //{
            //    oldUser.Password = model.NewPassword;
            //    oldUser.EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
            //    dbcontext.SaveChanges();
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}


            string EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
            int count = 0;

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = "update UserDetails set Password = @NewPassword, EmailConfirmationCode = @EmailConfirmationCode where UserId =@UserId " +
                                  "and Email = @Email and IsDeleted = 0";
                count = con.Query<int>(sqlquery, new { NewPassword = model.NewPassword, EmailConfirmationCode = EmailConfirmationCode, UserId = model.UserId, Email = model.EmailId }).Single();
            }

            if (count > 0)
                return true;
            else
                return false;

        }


        public bool chkPasswordLinkExpiry(SetPasswordBindingModel model)
        {
            dbcontext = new AppContext();

            //UserDetail oldUser = (from user in dbcontext.UserDetails
            //                      where user.UserId == model.UserId && user.EmailConfirmationCode == model.Code && user.IsDeleted == false
            //                      select user).FirstOrDefault();

            UserDetail oldUser;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = "select * from UserDetails as u where u.UserId = @UserId and u.EmailConfirmationCode = @Code and u.IsDeleted = 0";
                oldUser = con.Query<UserDetail>(sqlquery, new { UserId = model.UserId, Code = model.Code }).FirstOrDefault();
            }

            if (model.formType == "reset")
            {
                DateTime ExpiryDate = oldUser.EmailConfirmationCodeCreatedDate.Value.AddHours(24);
                if (ExpiryDate < DateTime.Now)
                {
                    oldUser = null;
                }
            }


            if (oldUser != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public UserModel Login(Login model, string ipAddress, string userAgent)
        {
            dbcontext = new AppContext();
            UserModel returnModel;
            UserModel objUsercheck = new UserModel();
            bool IsVendor = false;

            //UserDetail oldUser = (from user in dbcontext.UserDetails
            //                      where user.Email == model.Email && user.IsDeleted == false  //&& user.Password==model.Password
            //                      select user).FirstOrDefault();

            UserDetail oldUser;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlQuery = @"select Id,UserId,Email,FirstName,LastName,Gender,IsSubscribed,CompanyId,RoleId,ParentUserId,ProfilePhoto,SelectedTeamId,
                    SendNotificationEmail,Address,PhoneNo,Department,Designation,Experience,Expertise,DateOfBirth,AboutMe,EmailConfirmed,
                    Password,Token,EmailConfirmationCode,Rate,IsFreelancer,UserName,Availabilty,Tags,FavProjects,IsProfilePublic
                    ,CreditCard,ExpiryMonth,ExpiryYear,CvvNumber,CardHolderName,AllowReckringPayment,StripeUserId,IsStripeAccountVerified
                    ,EmailConfirmationCodeCreatedDate,IsDeleted,IsActive,IsNull(UD.Indefinitely, 1) Indefinitely ,IsNull(UD.ValidUntil, 0) ValidUntil,ValidUntilDate,Ud.Id VendorId
                    from UserDetails Ud
                     where Ud.Email=@Email and Ud.IsDeleted=0  and Ud.IsActive = 1";
                oldUser = con.Query<UserDetail>(sqlQuery, new { Email = model.Email }).FirstOrDefault();
                //Vendor code
                if (oldUser != null)
                {
                    string sqlQuery1 = @"Select C.Id as CompanyId ,IsVendor,Ud.IsActive,C.Name as CompanyName from Companies C inner join UserDetails Ud on C.Id = Ud.CompanyId
                                        where Ud.UserId = @UserId and Ud.IsActive = 1";
                    objUsercheck = con.Query<UserModel>(sqlQuery1, new { UserId = oldUser.UserId }).SingleOrDefault();
                }


                //Vendor code
            }


            if (oldUser == null)
            {
                returnModel = new UserModel()
                {
                    userId = string.Empty,
                    firstName = string.Empty,
                    lastName = string.Empty,
                    email = model.Email,
                    EmailConfirmationCode = string.Empty,
                    CompanyId = 0,
                    TeamId = 0,
                    AboutMe = "2",
                    IsSubscriptionsEnds = false,
                    SubscriptionsEndDate = null,
                    IsActive = false,
                    Indefinitely = false,
                    ValidUntil=false,



                };
                return returnModel;
            }
            else
            {
                /* Update login token User Details */
                if (oldUser.Password == model.Password || model.IsLoginWithLinedIn == true)
                {
                    string token = SecurityManager.GenerateToken(model.Email, model.Password, ipAddress, userAgent, DateTime.Now.Ticks);

                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlQuery = "update UserDetails set Token = @token where UserId = @UserId";
                        con.Execute(sqlQuery, new { token = token, UserId = oldUser.UserId });
                    }

                    //oldUser.Token = token;
                    //dbcontext.SaveChanges();

                    int UserRoleId = 4;
                    int CompanyId = 0;
                    bool subscriptionsEnd = false;
                    DateTime? subscriptionsEndDate = null;
                    bool IsExpiryMailReceived = false;
                    bool IsExpiryWarningMailReceived = false;

                    //string profilePic = !string.IsNullOrEmpty(oldUser.ProfilePhoto) ? oldUser.ProfilePhoto : "profile.png";
                    string profilePic = !string.IsNullOrEmpty(oldUser.ProfilePhoto) ? "Uploads/Profile/Thumbnail/" + oldUser.ProfilePhoto : "Uploads/Default/profile.png";
                    if (!oldUser.IsFreelancer)
                    {
                        UserCompanyRole obj;
                        using (var con = new SqlConnection(ConnectionString))
                        {
                            string sqlQuery = "select * from UserCompanyRoles where UserId = @UserId";
                            obj = con.Query<UserCompanyRole>(sqlQuery, new { UserId = oldUser.UserId }).FirstOrDefault();
                        }

                        UserRoleId = obj.RoleId.Value;
                        CompanyId = obj.CompanyId.Value;
                        ////Vendor code
                        if (objUsercheck != null && objUsercheck.IsVendor == true)
                        {
                            returnModel = new UserModel()
                            {
                                userId = oldUser.UserId,
                                firstName = oldUser.FirstName,
                                lastName = oldUser.LastName,               ////Vendor code
                                email = oldUser.Email,
                                isAccountActive = oldUser.EmailConfirmed.Value,
                                RoleId = Convert.ToInt32(oldUser.RoleId),
                                isFreelancer = oldUser.IsFreelancer,
                                Token = oldUser.Token,
                                AboutMe = "",
                                originalProfilePhoto = profilePic,
                                CompanyId = objUsercheck.CompanyId,
                                CompanyName=objUsercheck.CompanyName,
                                VendorId = oldUser.VendorId,
                                IsActive = oldUser.IsActive,
                                Indefinitely = oldUser.Indefinitely,
                                ValidUntil = oldUser.ValidUntil,
                                ValidUntilDate = oldUser.ValidUntilDate,

                                
                            };
                            return returnModel;

                        }
                        ////Vendor code


                        Subscription sub;
                        using (var con = new SqlConnection(ConnectionString))
                        {
                            string sqlQuery = "select * from Subscriptions where IsActive = 1 and CompanyId = @CompanyId";
                            sub = con.Query<Subscription>(sqlQuery, new { CompanyId = CompanyId }).FirstOrDefault();
                        }

                        if (sub == null)
                        {
                            subscriptionsEnd = true;
                        }
                        else
                        {
                            // subscriptionsEnd = DateTime.Now <= sub.EndDate ? false : true;   Pop-up remove
                            subscriptionsEnd = false;
                            subscriptionsEndDate = sub.EndDate;
                            IsExpiryMailReceived = sub.IsExpiryMailReceived.HasValue ? sub.IsExpiryMailReceived.Value : false;
                            IsExpiryWarningMailReceived = sub.IsExpiryWarningMailReceived.HasValue ? sub.IsExpiryWarningMailReceived.Value : false;
                        }
                        //}
                    }

                    returnModel = new UserModel()
                    {
                        userId = oldUser.UserId,
                        firstName = oldUser.FirstName,
                        lastName = oldUser.LastName,
                        email = oldUser.Email,
                        isAccountActive = oldUser.EmailConfirmed.Value,
                        TeamId = oldUser.SelectedTeamId.HasValue ? oldUser.SelectedTeamId.Value : 0,
                        CompanyId = CompanyId,
                        RoleId = UserRoleId,
                        isFreelancer = oldUser.IsFreelancer,
                        Token = oldUser.Token,
                        AboutMe = "",
                        originalProfilePhoto = profilePic,
                        IsSubscriptionsEnds = subscriptionsEnd,
                        SubscriptionsEndDate = subscriptionsEndDate,
                        Indefinitely = oldUser.Indefinitely,
                        ValidUntil = oldUser.ValidUntil,
                        IsActive= oldUser.IsActive,
                        ValidUntilDate=oldUser.ValidUntilDate,
                   


                    };

                    /* START  Checking wheather current Account is Active for further checking or sending mail on Account Subscription ends */


                    if (returnModel.isAccountActive)
                    {
                        if (returnModel.SubscriptionsEndDate != null)
                        {
                            var count = (Convert.ToDateTime(returnModel.SubscriptionsEndDate) - DateTime.Now).Days;
                            // Check for user subscription ends or not and if yes sending mail via new thread
                            if (returnModel.IsSubscriptionsEnds)
                            {
                                if (!IsExpiryMailReceived)
                                {
                                    HttpContext ctx = HttpContext.Current;
                                    Thread childref = new Thread(new ThreadStart(() =>
                                    {
                                        HttpContext.Current = ctx;
                                        sendExpirationMembershipEmail(returnModel, 0);
                                    }
                                    ));
                                    childref.Start();

                                    //Subscription sub = (from comp in dbcontext.Subscriptions
                                    //                    where comp.IsActive == true && comp.CompanyId == CompanyId
                                    //                    select comp
                                    //          ).SingleOrDefault();

                                    //sub.IsExpiryMailReceived = true;
                                    //dbcontext.SaveChanges();

                                    using (var con = new SqlConnection(ConnectionString))
                                    {
                                        string sqlQuery = "update Subscriptions set IsExpiryMailReceived = 1 where IsActive = 1 and CompanyId = @CompanyId";
                                        con.Query<int>(sqlQuery, new { CompanyId = CompanyId });
                                    }

                                }
                            }
                            // Check for user subscription ends in 7 days or not and if yes sending mail via new thread
                            else if (count > 0 && count <= 7)
                            {
                                if (!IsExpiryWarningMailReceived)
                                {
                                    HttpContext ctx = HttpContext.Current;
                                    Thread childref = new Thread(new ThreadStart(() =>
                                    {
                                        HttpContext.Current = ctx;
                                        sendExpirationMembershipEmail(returnModel, count);
                                    }
                                    ));
                                    childref.Start();

                                    //Subscription sub = (from comp in dbcontext.Subscriptions
                                    //                    where comp.IsActive == true && comp.CompanyId == CompanyId
                                    //                    select comp
                                    //          ).SingleOrDefault();

                                    //sub.IsExpiryWarningMailReceived = true;
                                    //dbcontext.SaveChanges();

                                    using (var con = new SqlConnection(ConnectionString))
                                    {
                                        string sqlQuery = "update Subscriptions set IsExpiryWarningMailReceived = 1 where IsActive = 1 and CompanyId = @CompanyId";
                                        con.Query<int>(sqlQuery, new { CompanyId = CompanyId });
                                    }

                                }
                            }
                        }
                    }
                    /* END  Checking wheather current Account is Active for further checking or sending mail on Account Subscription ends */

                    return returnModel;
                }
                else
                {
                    returnModel = new UserModel() { userId = string.Empty, firstName = string.Empty, lastName = string.Empty, email = model.Email, EmailConfirmationCode = string.Empty, CompanyId = 0, TeamId = 0, AboutMe = "1", originalProfilePhoto = "Uploads/Default/profile.png", IsSubscriptionsEnds = false, SubscriptionsEndDate = null };
                    return returnModel;
                }
            }

        }


        private void sendExpirationMembershipEmail(UserModel returnModel, int days)
        {
            bool IsSettingActivated = false;

            if (!string.IsNullOrEmpty(returnModel.userId))
            {

                int SettingActivated;
                using (var con = new SqlConnection(ConnectionString))
                {
                    SettingActivated = con.Query<int>("usp_IsUserEmailSettingActivated", new { CompanyId = returnModel.CompanyId, UserId = returnModel.userId, NotificationName = "Subscription Expiry" }, commandType: CommandType.StoredProcedure).Single();
                }

                if (SettingActivated == 0)
                    IsSettingActivated = false;
                else
                    IsSettingActivated = true;

            }

            if (IsSettingActivated)
            {
                String emailbody = null;
                string Subject = string.Empty;
                if (days == 0)
                {
                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                    var MailModule = from r in xdoc.Descendants("MailModule").
                                     Where(r => (string)r.Attribute("id") == "ExpirationMembershipEmail")
                                         //select r;
                                     select new
                                     {
                                         mailbody = r.Element("Body").Value,
                                         subject = r.Element("Subject").Value
                                     };

                    foreach (var item in MailModule)
                    {
                        emailbody = item.mailbody;
                        Subject = item.subject;
                    }


                    emailbody = emailbody.Replace("@@Receiver_firstName", returnModel.firstName);

                }
                else
                {
                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                    var MailModule = from r in xdoc.Descendants("MailModule").
                                     Where(r => (string)r.Attribute("id") == "ExpirationMembershipEmailAboutToExpire")
                                         //select r;
                                     select new
                                     {
                                         mailbody = r.Element("Body").Value,
                                         subject = r.Element("Subject").Value
                                     };

                    foreach (var item in MailModule)
                    {
                        emailbody = item.mailbody;
                        Subject = item.subject;
                    }

                    emailbody = emailbody.Replace("@@Receiver_firstName", returnModel.firstName).Replace("@@days", days.ToString());

                }


                EmailUtility.SendMailInThread(returnModel.email, "Membership expire", emailbody);
            }

        }

        public bool ChangePassword(string UserId, ChangePasswordModel model)
        {
            dbcontext = new AppContext();

            UserDetail oldUser = (from user in dbcontext.UserDetails
                                  where user.UserId == UserId && user.IsDeleted == false
                                  select user).FirstOrDefault();
            if (oldUser != null)
            {
                if (oldUser.Password == model.CurrentPassword)
                {
                    oldUser.Password = model.NewPassword;
                    dbcontext.SaveChanges();
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }
        }



        public RegistrationMessageModa RegisterInvitationVendorCompany(VendorCompanyInvitationModal Modal, string url)
        {

            string[] EmailDomain = Modal.EmailId.Split('@');
            int ExistIndataBase = 0;
            string VendorCompnayUserId = Guid.NewGuid().ToString();
            RegistrationMessageModa ObjRegistrationModal;

            string keyy = UTILITIES.EmailUtility.GetUniqueKey(16);

            using (var con = new SqlConnection(ConnectionString))
            {
                ObjRegistrationModal = con.Query<RegistrationMessageModa>("Usp_InvitationVendorCompany", new
                {
                    EmailDomain = EmailDomain[1],
                    Email = Modal.EmailId

                }, commandType: CommandType.StoredProcedure).FirstOrDefault();




                if (ObjRegistrationModal != null)
                {
                    if (ObjRegistrationModal.Id == 1)
                    {
                        HttpContext ctx = HttpContext.Current;
                        Thread childref = new Thread(new ThreadStart(() =>
                        {
                            HttpContext.Current = ctx;
                            ThreadForVendorCompanyInvitations(Modal.EmailId, url);
                        }
                        ));
                        childref.Start();
                    }

                }

            }

            return ObjRegistrationModal;

        }


        private void ThreadForVendorCompanyInvitations(string EmailId, string url)
        {
            string encriptMailId = "";
            string mailnew = "dasf";
            string Key = "k2k3aij3h";

            string EmailConfirmationCode = EmailUtility.GetUniqueKey(8);


            encriptMailId = UTILITIES.GeneralUtilities.CryptoEngine.Encrypt(EmailId, Key);
            mailnew = UTILITIES.GeneralUtilities.CryptoEngine.Decrypt(encriptMailId, Key);

            var callbackUrl = url + "#/registration/vendorcompany/" + encriptMailId.Replace("/", "-") + "/" + EmailConfirmationCode;

            String emailbody = null;
            string Subject = string.Empty;


            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));
            var MailModule = from r in xdoc.Descendants("MailModule").
                                      Where(r => (string)r.Attribute("id") == "SendToCompanyVendorInvitation")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };


            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@callbackUrl", callbackUrl);
            EmailUtility.SendMailInThread(EmailId, Subject, emailbody);

        }


        public VendorCompanyInvitationModal GetVendorCompanyRecordForRegistration(VendorCompanyInvitationModal Modal, string url)
        {
            VendorCompanyInvitationModal ObjVendorCompanyRegistrationModal = new VendorCompanyInvitationModal();

            string mailnew = "dasf";
            string Key = "k2k3aij3h";
            string encriptMailId = "";
            string test1 = "";

            ObjVendorCompanyRegistrationModal.EmailId = UTILITIES.GeneralUtilities.CryptoEngine.Decrypt(Modal.userCode.Replace("-", "/"), Key);
            encriptMailId = UTILITIES.GeneralUtilities.CryptoEngine.Encrypt(ObjVendorCompanyRegistrationModal.EmailId, Key);



            //test1 = UTILITIES.GeneralUtilities.CryptoEngine.Encrypt("MytestVendor500@Mohindra.com", Key);
            //ObjVendorCompanyRegistrationModal.EmailId = UTILITIES.GeneralUtilities.CryptoEngine.Decrypt(test1.Replace("-", "/"), Key);


            return ObjVendorCompanyRegistrationModal;

        }

        public VendorCompanyRegistrationMailModal RegistrationsVendorCompany(VendorCompanyRegistrationModal Modal)
        {


            string UserId = Guid.NewGuid().ToString(); // user Id unique for stripe account

            SetPasswordBindingModel objSetPassModal = new SetPasswordBindingModel();
            UserDetail objModalUserDetail = new UserDetail();
            UserDetail objmodalUser = new UserDetail();







            VendorCompanyRegistrationMailModal ObjVendorCompanyRegistration;
            string[] EmailDomain = Modal.Email.Split('@');


            string RegistrationConfirmationCode = EmailUtility.GetUniqueKey(8);

            using (var con = new SqlConnection(ConnectionString))
            {


                ObjVendorCompanyRegistration = con.Query<VendorCompanyRegistrationMailModal>("Usp_RegistrationVendorCompany", new
                {

                    Email = Modal.Email,
                    FirstName = Modal.FirstName,
                    LastName = Modal.LastName,
                    CompanyName = Modal.CompanyName,
                    Password = Modal.Password,
                    Phone = Modal.Phone,
                    Fax = Modal.Fax,
                    Website = Modal.Website,
                    Address = Modal.Address,
                    Description = Modal.Description,
                    UserId = UserId,
                    EmailDomain = EmailDomain[1],
                    RegistrationConfirmationCode = RegistrationConfirmationCode



                }, commandType: CommandType.StoredProcedure).FirstOrDefault();



                #region Stripe
                // Saving StripeAccount Settings and info in StripeAccountDetail table
                StripeAccountDetail accdetail = new StripeAccountDetail
                {
                    UserId = UserId,
                    Email = ObjVendorCompanyRegistration.Email,
                    FirstName = ObjVendorCompanyRegistration.FirstName,
                    LastName = ObjVendorCompanyRegistration.LastName,
                    Managed = true
                };

                // setting the stripe api key
                StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["stripeapikey"].ToString());
                // creating the object to create new account
                var account = new StripeAccountCreateOptions();
                account.Email = ObjVendorCompanyRegistration.Email;
                account.Country = "US";
                //account.BusinessName = objmodalUser.FirstName + " " + objmodalUser.LastName;
                account.Type = StripeAccountType.Custom;
                // Calling the stripe account creation api
                var accountService = new StripeAccountService();
                StripeAccount response = accountService.Create(account);

                objmodalUser.StripeUserId = response.Id;
                objmodalUser.IsStripeAccountVerified = false;

                string Query = "update UserDetails set StripeUserId = @StripeUserId ,IsStripeAccountVerified = 1 where UserId = @UserId";
                con.Query(Query, new { StripeUserId = objmodalUser.StripeUserId, UserId = ObjVendorCompanyRegistration.UserId });

                #endregion




            }
            return ObjVendorCompanyRegistration;

        }


    }
}
