﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL.WorkLog;
using TIGHTEN.UTILITIES;
using TIGHTEN.DATA.WorkLog;
using System.Data;
using TIGHTEN.MODEL;
using System.Net.Http;

namespace TIGHTEN.DATA.WorkLog
{
    public class WorkLogDAL:BaseClass
    {

        public List<WorkLogDetailsModel> GetWorkLogList(string UserId)
        {
            
            List<WorkLogDetailsModel> objWL;
            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT WL.Id,WL.WorkLogTitle,WL.WorkLogDescription,WL.CreatedOn,WL.BillableStatus,WL.ProjectId,P.Name as ProjectName,WL.PhaseId,PP.Name as PhaseName,WL.PhaseMilestoneId,PM.Name as MilestoneName,PP.Name+'/'+PM.Name as PhaseMilestoneName FROM WorkLogDetail WL inner join
                                 projects P on WL.ProjectId=P.Id left outer join
                                 projectphases PP on WL.PhaseId=PP.Id left outer join
                                 projectmilestones PM on WL.PhaseMilestoneId=PM.Id
                                 where WL.CreatedUserId = @userId and WL.IsDeleted<>1";
                objWL = con.Query<WorkLogDetailsModel>(query, new
                {
                    userId = UserId,

                }).ToList();

            }
            foreach(var WorkLogObj in objWL)
            {
                WorkLogObj.CreatedOn = Convert.ToDateTime(WorkLogObj.CreatedOn).ToShortDateString();
            }
            return objWL;



        }

        public string saveWorkLog(WorkLogDetailsModel modal)
        {
            string msg="";
            WorkLogDetailsModel objCompanysend = new WorkLogDetailsModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("usp_SaveWorkLog",
                    new
                    {
                        WorkLogTitle = modal.WorkLogTitle,
                        WorkLogDescription = modal.WorkLogDescription,
                        CreatedOn =Convert.ToDateTime( modal.CreatedOn).Date,
                        UserId = modal.CreatedUserId,
                        BillableStatus = modal.BillableStatus,
                        ProjectId = modal.ProjectId,
                        PhaseId=modal.PhaseId,
                        PhaseMilestoneId=modal.PhaseMilestoneId
                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<WorkLogDetailsModel>().FirstOrDefault();
                if (objCompanysend.Id != 0)
                {
                    msg = "Worklog is successfully saved";
                }


            }

            return msg;
        }

        public string editWorkLog(WorkLogDetailsModel modal)
        {
            string msg = "";
            WorkLogDetailsModel objCompanysend = new WorkLogDetailsModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("usp_EditWorkLog",
                    new
                    {
                        Id=modal.Id,
                        WorkLogTitle = modal.WorkLogTitle,
                        WorkLogDescription = modal.WorkLogDescription,
                        CreatedOn = Convert.ToDateTime(modal.CreatedOn).Date,
                        UserId = modal.CreatedUserId,
                        BillableStatus = modal.BillableStatus,
                        ProjectId = modal.ProjectId,
                        PhaseId = modal.PhaseId,
                        PhaseMilestoneId = modal.PhaseMilestoneId
                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<WorkLogDetailsModel>().FirstOrDefault();
               
                    msg = "Worklog is successfully updated";
               

            }

            return msg;
        }

        public WorkLogDetailsModel GetWorkLogById(int Id)
        {

            WorkLogDetailsModel objWL;
            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT WL.Id,WL.WorkLogTitle,WL.WorkLogDescription,WL.CreatedOn,WL.BillableStatus,WL.ProjectId,P.Name as ProjectName,WL.PhaseId,PP.Name as PhaseName,WL.PhaseMilestoneId,PM.Name as MilestoneName,PP.Name+'/'+PM.Name as PhaseMilestoneName FROM WorkLogDetail WL inner join
                                 projects P on WL.ProjectId=P.Id left outer join
                                 projectphases PP on WL.PhaseId=PP.Id left outer join
                                 projectmilestones PM on WL.PhaseMilestoneId=PM.Id  where WL.Id = @Id";

              
                objWL = con.Query<WorkLogDetailsModel>(query, new
                {
                    Id = Id,

                }).FirstOrDefault();

            }

            return objWL;



        }

        public string deleteWorkLogById(int Id)
        {
            string msg = "";
            WorkLogDetailsModel objCompanysend = new WorkLogDetailsModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("usp_deleteWorkLog",
                    new
                    {
                        Id = Id,
                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<WorkLogDetailsModel>().FirstOrDefault();
                if (objCompanysend.Id != 0)
                {
                    msg = "Worklog is successfully deleted";
                }


            }

            return msg;
        }


        public WorkLogDetailsModel GetWorkLogEnteryList(int WorkLogId)
        {

            WorkLogDetailsModel objWL;
            objWL = GetWorkLogById(WorkLogId);
            if (objWL != null)
            {
                using (var con = new SqlConnection(ConnectionString))
                {

                    string query = @"SELECT WL.Id,WL.Description,WL.TimeSpent,WL.Date CreatedDate,Wl.WorkLogId FROM WorkLogEntries WL  where WL.WorkLogId = @workLogId and WL.IsDeleted<>1";


                    objWL.WorkLogEnteries = con.Query<WorkLogEnteriesModel>(query, new
                    {
                        workLogId = WorkLogId,

                    }).ToList();

                }
                foreach (var WorkLogEnteryObj in objWL.WorkLogEnteries)
                {
                    if (string.IsNullOrEmpty(WorkLogEnteryObj.CreatedDate) == false)
                    {
                        WorkLogEnteryObj.CreatedDate = Convert.ToDateTime(WorkLogEnteryObj.CreatedDate).ToShortDateString();
                    }
                }
            }
            return objWL;



        }


        public string SaveWorkLogEntry(WorkLogEnteriesModel modal)
        {
            string msg = "";
            WorkLogEnteriesModel objCompanysend = new WorkLogEnteriesModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("usp_SaveWorkLogEntry",
                    new
                    {
                        Description = modal.Description,
                        TimeSpent=modal.TimeSpent,
                        CreatedDate = Convert.ToDateTime(modal.CreatedDate).Date,
                        UserId = modal.CreatedUserId,
                        WorkLogId=modal.WorkLogId
                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<WorkLogEnteriesModel>().FirstOrDefault();
                if (objCompanysend.Id != 0)
                {
                    msg = "Worklog Entry is successfully saved";
                }


            }

            return msg;
        }

        public WorkLogEnteriesModel GetWorkLogEntryById(int Id)
        {

            WorkLogEnteriesModel objWL;
           
                using (var con = new SqlConnection(ConnectionString))
                {

                    string query = @"SELECT WL.Id,WL.Description,WL.TimeSpent,WL.Date CreatedDate,Wl.WorkLogId FROM WorkLogEntries WL  where WL.Id = @Id and WL.IsDeleted<>1";


                    objWL = con.Query<WorkLogEnteriesModel>(query, new
                    {
                        Id = Id,

                    }).FirstOrDefault();
                 
                
                }
            if (objWL != null && string.IsNullOrEmpty(objWL.CreatedDate) == false)
            {
                objWL.CreatedDate = Convert.ToDateTime(objWL.CreatedDate).ToShortDateString();
            }
            return objWL;



        }
        public string editWorkLogEntry(WorkLogEnteriesModel modal)
        {
            string msg = "";
            WorkLogEnteriesModel objCompanysend = new WorkLogEnteriesModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("usp_EditWorkLogEntry",
                    new
                    {
                        Id = modal.Id,
                        
                        Description = modal.Description,
                        TimeSpent = modal.TimeSpent,
                        CreatedDate = Convert.ToDateTime(modal.CreatedDate).Date,
                        UserId = modal.CreatedUserId,
                        WorkLogId = modal.WorkLogId
                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<WorkLogEnteriesModel>().FirstOrDefault();
                if (objCompanysend.Id != 0)
                {
                    msg = "Worklog Entry is successfully updated";
                }


            }

            return msg;
        }

        public string deleteWorkLogEntryById(int Id)
        {
            string msg = "";
            WorkLogDetailsModel objCompanysend = new WorkLogDetailsModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("usp_deleteWorkLogEntry",
                    new
                    {
                        Id = Id,
                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<WorkLogDetailsModel>().FirstOrDefault();
                if (objCompanysend.Id != 0)
                {
                    msg = "Worklog Entry is successfully deleted";
                }


            }

            return msg;
        }

        public List<WorkLogDetailsModel> GetWorkLogListByProjectFromTo(string UserId, int ProjectId, string FromDate, string ToDate)
        {

            List<WorkLogDetailsModel> objWL;
            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT WL.Id,WL.WorkLogTitle,WL.WorkLogDescription,WL.CreatedOn,WL.BillableStatus,WL.ProjectId,P.Name as ProjectName,WL.PhaseId,PP.Name as PhaseName,WL.PhaseMilestoneId,PM.Name as MilestoneName,PP.Name+'/'+PM.Name as PhaseMilestoneName
                                 ,(select sum(TimeSpent) from WorkLogEntries where WorkLogId=WL.Id and IsDeleted<>1) TotalHrs
                                 FROM WorkLogDetail WL inner join
                                 projects P on WL.ProjectId=P.Id left outer join
                                 projectphases PP on WL.PhaseId=PP.Id left outer join
                                 projectmilestones PM on WL.PhaseMilestoneId=PM.Id 
                                 where WL.CreatedUserId = @userId and WL.IsDeleted<>1 and WL.ProjectId=@projectId and wl.LastEntryUpdateDate>=Convert(datetime,@fromDate,103) and wl.LastEntryUpdateDate<=Convert(datetime,@toDate,103)";
                objWL = con.Query<WorkLogDetailsModel>(query, new
                {
                    userId = UserId,
                    projectId=ProjectId,
                    fromDate=FromDate,
                    toDate=ToDate
                }).ToList();

            }
            foreach (var WorkLogObj in objWL)
            {
                WorkLogObj.CreatedOn = Convert.ToDateTime(WorkLogObj.CreatedOn).ToShortDateString();
            }
            return objWL;



        }
    }
}
