﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;

namespace TIGHTEN.DATA
{
    public class SubscriptionRecurring : Common
    {

        public string RecurringPayment()
        {
           List<UserDetail> RecurringUsers = (from usr in dbcontext.UserDetails
                                              join subsc in dbcontext.Subscriptions on usr.UserId equals subsc.UserId
                                              where usr.AllowReckringPayment == true && usr.IsDeleted == false
                                               && subsc.EndDate < DateTime.Now
                                              select usr).ToList();

            foreach (var item in RecurringUsers)
            {
                    var subs    =   (from sub in dbcontext.Subscriptions
                                     join pac in dbcontext.Packages on sub.PackageID equals pac.Id
                                     where sub.UserId == item.UserId && sub.CompanyId == item.CompanyId
                                     && sub.IsActive == true 
                                     && pac.IsActive == true
                                     select new
                                     {
                                         Price = pac.Price,
                                         Duration = pac.DurationInDays,
                                         Discount = pac.Discount,
                                         PackageId = sub.PackageID,
                                         SubscriptionId = sub.Id ,
                                         SubscriptionExpiryDate = sub.EndDate
                                     }
                                     ).FirstOrDefault();

                if (subs != null)
                {
                    // Get All eligible users for Calculating pricing of stripe for subscription

                    int NumberOfUsers = (
                                          (from usr in dbcontext.UserDetails
                                           where usr.CompanyId == item.CompanyId
                                           && usr.IsFreelancer == false && usr.IsDeleted == false
                                           select usr
                                           )
                                           .Union
                                           (
                                            from usr in dbcontext.UserDetails
                                            join usrcomrol in dbcontext.UserCompanyRoles on usr.UserId equals usrcomrol.UserId
                                            join prjctusr in dbcontext.ProjectUsers on usr.UserId equals prjctusr.UserId
                                            join prjct in dbcontext.Projects on prjctusr.ProjectId equals prjct.Id
                                            where usrcomrol.CompanyId == item.CompanyId && prjct.IsDeleted == false
                                            && prjct.IsComplete == false && usr.IsDeleted == false
                                            && usr.IsFreelancer == true
                                            select usr
                                           )
                                         ).Count();


                    decimal CalculatedStripeAmountForCharge = subs.Price.HasValue ? 0 * NumberOfUsers : subs.Price.Value  * NumberOfUsers;


                    // Creating Stripe Token to continue with the Subscription Payment

                    var myToken = new StripeTokenCreateOptions();

                    myToken.Card = new StripeCreditCardOptions()
                    {
                        Number = item.CreditCard,
                        ExpirationYear =  Convert.ToInt32(item.ExpiryYear),
                        ExpirationMonth = Convert.ToInt32(item.ExpiryMonth),
                        Cvc = item.CvvNumber
                    };

                    var tokenService = new StripeTokenService();
                    StripeToken stripeToken = tokenService.Create(myToken);



                    #region Stripe Payment      
                        var myCharge = new StripeChargeCreateOptions();

                        CalculatedStripeAmountForCharge = CalculatedStripeAmountForCharge * 100;

                        // always set these properties
                        myCharge.Amount = Convert.ToInt32(CalculatedStripeAmountForCharge);
                        
                        myCharge.Currency = "usd";

                        // set this if you want to
                        myCharge.Description = "Tighten Subscription(Recurring)";

                        myCharge.SourceTokenOrExistingSourceId = stripeToken.Id;

                        // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
                        myCharge.Capture = true;

                        var chargeService = new StripeChargeService();
                        StripeCharge stripeCharge = chargeService.Create(myCharge);

                    #endregion

                    //Saving data in Subscription Table on Stripe Payment success
                    if (stripeCharge.Status == "succeeded")
                    {
                        var subscription = (from p in dbcontext.Subscriptions
                                            where p.CompanyId == item.CompanyId && p.UserId == item.UserId
                                            select p).ToList();

                        foreach (var SubsItem in subscription)
                        {
                            SubsItem.IsActive = false;
                        }

                        TIGHTEN.ENTITY.Subscription Subscription = new TIGHTEN.ENTITY.Subscription
                        {
                            PackageID = subs.PackageId,
                            UserId = item.UserId,
                            CompanyId = item.CompanyId.Value,
                            StartDate = DateTime.Now,
                            EndDate = DateTime.Now.AddDays(subs.Duration),
                            IsActive = true
                        };
                        dbcontext.Subscriptions.Add(Subscription);
                        dbcontext.SaveChanges();


                        //Saving data in Transaction Table on Stripe Payment success

                        Guid obj = Guid.NewGuid();

                        dbcontext = new AppContext();
                        TIGHTEN.ENTITY.Transaction transaction = new TIGHTEN.ENTITY.Transaction
                        {
                            EventID = Subscription.Id,
                            AmountPaid = Convert.ToDecimal(myCharge.Amount),
                            TransactionDate = DateTime.Now,
                            TransactionID = obj.ToString(),
                            TransactionBy = item.UserId,
                            TransactionType = "Tighten Subscription(Recurring)",
                            TransactionFor = item.UserId,
                            StirpeTransactionID = stripeCharge.Id
                        };
                        dbcontext.Transactions.Add(transaction);
                        dbcontext.SaveChanges();

                    }

                }

            }

            return "";
        }


    }
}
