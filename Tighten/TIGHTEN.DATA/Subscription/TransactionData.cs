﻿using TIGHTEN.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using System.Threading.Tasks;
using System.Globalization;

using System.Web.Security;
using System.Drawing;

namespace TIGHTEN.DATA
{
    public class TransactionData
    {
        AppContext dbcontext;

        public List<TransactionModel> GetTransactions(int SubscriptionID)
        {
            dbcontext = new AppContext();
            var transactions = (from p in dbcontext.Transactions
                               where p.EventID == SubscriptionID
                               select p).ToList();

            List<TransactionModel> obj = new List<TransactionModel>();
            foreach (var item in transactions)
            {
                TIGHTEN.MODEL.TransactionModel Transaction = new TIGHTEN.MODEL.TransactionModel
                {
                    EventID = item.EventID,
                    AmountPaid = item.AmountPaid,
                    TransactionDate = item.TransactionDate,
                    TransactionID = item.TransactionID,
                    TransactionBy = item.TransactionBy,
                    TransactionFor = item.TransactionFor,
                    TransactionType = item.TransactionType,
                    StirpeTransactionID = item.StirpeTransactionID
                };
                obj.Add(Transaction);
            }

            return obj;
        }

        public TransactionModel GetTransactionSingleOrDefault(int SubscriptionID)
        {
            dbcontext = new AppContext();
            var transactions = (from p in dbcontext.Transactions
                               where p.EventID == SubscriptionID
                               select p).SingleOrDefault();

            TIGHTEN.MODEL.TransactionModel Transaction = new TIGHTEN.MODEL.TransactionModel
            {
                EventID = transactions.EventID,
                AmountPaid = transactions.AmountPaid,
                TransactionDate = transactions.TransactionDate,
                TransactionID = transactions.TransactionID
            };
            return Transaction;
        }

        public string AddTransaction(TransactionModel model)
        {
            Guid obj = Guid.NewGuid();
            model.TransactionID = obj.ToString();

            dbcontext = new AppContext();
            TIGHTEN.ENTITY.Transaction transaction = new TIGHTEN.ENTITY.Transaction
            {
                EventID = model.EventID,
                AmountPaid = model.AmountPaid,
                TransactionDate = model.TransactionDate,
                TransactionID = model.TransactionID,
                TransactionBy = model.TransactionBy,
                TransactionType= model.TransactionType,
                TransactionFor = model.TransactionFor,
                StirpeTransactionID = model.StirpeTransactionID
            };
            dbcontext.Transactions.Add(transaction);
            dbcontext.SaveChanges();
            return transaction.TransactionID;
        }

        public string UpdateTransaction(TransactionModel model)
        {
            dbcontext = new AppContext();
            var transaction = (from p in dbcontext.Transactions
                               where p.EventID == model.EventID
                               select p).ToList();

            foreach (var item in transaction)
            {
                item.EventID = model.EventID;
                item.AmountPaid = model.AmountPaid;
                item.TransactionDate = model.TransactionDate;
                item.TransactionID = model.TransactionID;
                item.TransactionBy = model.TransactionBy;
                item.TransactionType = model.TransactionType;
                item.TransactionFor = model.TransactionFor;
                item.StirpeTransactionID = model.StirpeTransactionID;
                dbcontext.SaveChanges();
            }
            return model.TransactionID;
        }
    }
}
