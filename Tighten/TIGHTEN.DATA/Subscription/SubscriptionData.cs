﻿using TIGHTEN.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using System.Threading.Tasks;
using System.Globalization;

using System.Web.Security;
using System.Drawing;
using Stripe;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class SubscriptionData : BaseClass
    {
        AppContext dbcontext;

        public List<SubscriptionModel> GetUserSubscriptions(string UserId, int CompanyId)
        {
            //dbcontext = new AppContext();
            //var subscription = (from p in dbcontext.Subscriptions
            //                    where p.CompanyId == CompanyId && p.UserId == UserId
            //                    select p).ToList();

            //List<SubscriptionModel> subscriptionObj = new List<SubscriptionModel>();
            //foreach (var item in subscription)
            //{
            //    TIGHTEN.MODEL.SubscriptionModel Subscription = new TIGHTEN.MODEL.SubscriptionModel
            //    {
            //        PackageId = item.PackageID,
            //        UserId = item.UserId,
            //        CompanyId = item.CompanyId,
            //        StartDate = item.StartDate,
            //        EndDate = item.EndDate,
            //        IsActive = item.IsActive
            //    };
            //    subscriptionObj.Add(Subscription);
            //}

            List<SubscriptionModel> subscriptionObj;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select p.PackageID,p.UserId,p.CompanyId,p.StartDate,p.EndDate,p.IsActive from Subscriptions p where p.CompanyId = @CompanyId and p.UserId = @UserId ";
                subscriptionObj = con.Query<SubscriptionModel>(sqlquery, new { CompanyId= CompanyId , UserId = UserId }).ToList();
            }

            return subscriptionObj;
        }

        public SubscriptionModel GetUserActiveSubscriptions(string UserId, int CompanyId)
        {

            TIGHTEN.MODEL.SubscriptionModel Subscription;
            string query = "select * from Subscriptions p where p.IsActive = 1 and CompanyId = @CompanyId and p.UserId =  @UserId";

            using (var con = new SqlConnection(ConnectionString))
            {
                Subscription = con.Query<TIGHTEN.MODEL.SubscriptionModel>(query, new { CompanyId = CompanyId, UserId = UserId }).FirstOrDefault();
            }


            //dbcontext = new AppContext();
            //var subscription = (from p in dbcontext.Subscriptions
            //                    where p.IsActive == true && p.CompanyId == CompanyId && p.UserId == UserId
            //                    select p).SingleOrDefault();

            //TIGHTEN.MODEL.SubscriptionModel Subscription = new TIGHTEN.MODEL.SubscriptionModel
            //{
            //    PackageId = subscription.PackageID,
            //    UserId = subscription.UserId,
            //    CompanyId = subscription.CompanyId,
            //    StartDate = subscription.StartDate,
            //    EndDate = subscription.EndDate,
            //    IsActive = subscription.IsActive
            //};


            return Subscription;
        }

        public int AddSubscription1(PaymentModel userdetail)
        {


            

            int Id = 0;

            using (var con = new SqlConnection(ConnectionString))
            {



                string query = "select* from PaymentDetails where CompanyId = @CompanyId and IsDefault = 1 and IsDeleted = 0";

                if (userdetail.CreditCard == null)
                {
                    PaymentModel obj;
                    con.Query<PaymentModel>(query, new
                    { companyId = userdetail.CompanyId, }).FirstOrDefault();




                }
                else
                {




                }

                con.Query<PaymentModel>(query);
                 
                con.Execute("Usp_GetUserActiveSubscriptions", new
                {
                    paymentType = userdetail.PaymentType,
                    companyId = userdetail.CompanyId,
                    CardName = userdetail.CardName,
                    CreditCardNumber = userdetail.CreditCard,
                    CvvNumber = userdetail.CvvNumber,
                    ExpiryMonth = userdetail.ExpiryMonth,
                    ExpiryYear = userdetail.ExpiryYear,
                    AllowReckringPayment = Convert.ToBoolean(userdetail.AllowReckringPayment),
                    CreatedBy = userdetail.UserId,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = userdetail.UserId,
                    ModifiedDate = DateTime.Now,
                    IsDefault = 1,
                    PackageId = userdetail.PackageId,
                    UserId = userdetail.UserId
                }, commandType: System.Data.CommandType.StoredProcedure);
            }

            return Id;
        }


        public int AddSubscription(PaymentModel userdetail)
        {
            //dbcontext = new AppContext();
            //if (userdetail.PaymentType == 1)
            //{
            //    TIGHTEN.ENTITY.PaymentDetail paymnt = new ENTITY.PaymentDetail();
            //    paymnt.CompanyId = userdetail.CompanyId;
            //    paymnt.CardName = userdetail.CardName;
            //    //paymnt.CardDescription = userdetail.CompanyId;
            //    paymnt.CreditCardNumber = userdetail.CreditCard;
            //    paymnt.CvvNumber = userdetail.CvvNumber;
            //    paymnt.ExpiryMonth = userdetail.ExpiryMonth;
            //    paymnt.ExpiryYear = userdetail.ExpiryYear;
            //    paymnt.AllowReckringPayment = Convert.ToBoolean(userdetail.AllowReckringPayment);
            //    paymnt.CreatedBy = userdetail.UserId;
            //    paymnt.CreatedDate = DateTime.Now;
            //    paymnt.ModifiedBy = userdetail.UserId;
            //    paymnt.ModifiedDate = DateTime.Now;
            //    //paymnt.IsActive = true;
            //    paymnt.IsDefault = true;

            //    dbcontext.PaymentDetails.Add(paymnt);
            //}


            //var subscription = (from p in dbcontext.Subscriptions
            //                    where p.CompanyId == userdetail.CompanyId && p.UserId == userdetail.UserId
            //                    select p).ToList();

            //foreach (var item in subscription)
            //{
            //    item.IsActive = false;
            //}
            //Package pack = (from p in dbcontext.Packages
            //                where p.Id == userdetail.PackageId
            //                select p).SingleOrDefault();

            //TIGHTEN.ENTITY.Subscription Subscription = new TIGHTEN.ENTITY.Subscription
            //{
            //    PackageID = userdetail.PackageId,
            //    UserId = userdetail.UserId,
            //    CompanyId = userdetail.CompanyId,
            //    StartDate = DateTime.Now,
            //    EndDate = DateTime.Now.AddDays(pack.DurationInDays),
            //    IsActive = true
            //};
            //dbcontext.Subscriptions.Add(Subscription);
            //dbcontext.SaveChanges();

            int Id = 0;

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("Usp_GetUserActiveSubscriptions", new
                { paymentType = userdetail.PaymentType,
                    companyId = userdetail.CompanyId,
                    CardName = userdetail.CardName,
                    CreditCardNumber = userdetail.CreditCard,
                    CvvNumber = userdetail.CvvNumber,
                    ExpiryMonth = userdetail.ExpiryMonth,
                    ExpiryYear = userdetail.ExpiryYear,
                    Discount = 0,
                    AllowReckringPayment = Convert.ToBoolean(userdetail.AllowReckringPayment), CreatedBy = userdetail.UserId, CreatedDate = DateTime.Now, ModifiedBy = userdetail.UserId, ModifiedDate = DateTime.Now, IsDefault = 1 , PackageId = userdetail.PackageId, UserId = userdetail.UserId }, commandType: System.Data.CommandType.StoredProcedure);
            }

            return Id;
        }



        public string SubsctriptionPayment(SubsctriptionPaymentModel model)
        {
            //dbcontext = new AppContext();

            Company com;
            string query = @"select Top 1  [Id],[Name],[Email],[PhoneNo],[Fax],[Website],[Address],[Description],[Logo],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate]
                            from Companies co where co.Id = @Id";

            using (var con = new SqlConnection(ConnectionString))
            {
                 com = con.Query<Company>(query, new { Id = model.CompanyId }).FirstOrDefault(); ;
            }

                 //com = (from co in dbcontext.Companies
                 //          where co.Id == model.CompanyId
                 //          select co).FirstOrDefault();

            var myCharge = new StripeChargeCreateOptions();

            // always set these properties
            myCharge.Amount = model.Amount;
            if (myCharge.Amount > 0)
            {
                myCharge.Amount = myCharge.Amount * 100;
            }
            myCharge.Currency = "usd";

            // set this if you want to
            myCharge.Description = "Subscription : Payment of $ " + (myCharge.Amount / 100) + " from " + com.Name + " to Tighten";

            myCharge.SourceTokenOrExistingSourceId = model.Token;


            // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
            myCharge.Capture = true;


            var chargeService = new StripeChargeService();
            StripeCharge stripeCharge = chargeService.Create(myCharge);

            return stripeCharge.Id;
        }



        public int UpdateSubscription(SubscriptionModel userdetail)
        {


            string query = @"update Subscriptions set PackageID = @PackageId, 
                            UserId = @UserId,CompanyId =@CompanyId,StartDate = @StartDate,EndDate =@EndDate,IsActive= @IsActive
                            where CompanyId = @CompanyId and p.UserId =  @UserId";

            using (var con = new SqlConnection(ConnectionString))
            {
                     con.Execute(query,
                    new
                    {
                        PackageID = userdetail.PackageId,
                        UserId = userdetail.UserId,
                        CompanyId = userdetail.CompanyId,
                        StartDate = userdetail.StartDate,
                        EndDate = userdetail.StartDate.AddDays(30),
                        IsActive = userdetail.IsActive

                    });
            }

            //dbcontext = new AppContext();
            //var subscription = (from p in dbcontext.Subscriptions
            //                    where p.CompanyId == userdetail.CompanyId && p.UserId == userdetail.UserId
            //                    select p).ToList();

            //foreach (var item in subscription)
            //{
            //    item.PackageID = userdetail.PackageId;
            //    item.UserId = userdetail.UserId;
            //    item.CompanyId = userdetail.CompanyId;
            //    item.StartDate = userdetail.StartDate;
            //    item.EndDate = userdetail.StartDate.AddDays(30);
            //    item.IsActive = userdetail.IsActive;
            //}
            //dbcontext.SaveChanges();
            var SubscriptionId = userdetail.Id;
            return SubscriptionId;
        }
    }
}
