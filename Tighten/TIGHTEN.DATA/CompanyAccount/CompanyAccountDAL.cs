﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL.CompanyAccount;
using TIGHTEN.UTILITIES;
using TIGHTEN.DATA;
using System.Data;
using TIGHTEN.MODEL;
using static TIGHTEN.MODEL.CompanyAccount.AccountDocumentsModel;
using System.Net.Http;
namespace TIGHTEN.DATA.CompanyAccount
{
    public class CompanyAccountDAL : BaseClass
    {
        public List<AccountModel> GetCompanyAccountList(int CompanyId)
        {

            List<AccountModel> objAcc;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT Id,[Name],CompanyId,(select top 1 name from companies where id=CompanyVendorAccount.CompanyId) CompanyName,VendorId,(select top 1 name from companies where id=CompanyVendorAccount.VendorId) VendorCompanyName,TeamId,(select top 1 TeamName from Teams where id=CompanyVendorAccount.TeamId) TeamName, CreatedDate,IsActive FROM CompanyVendorAccount  where CompanyId = @CompanyId and IsDeleted<>1";
                objAcc = con.Query<AccountModel>(query, new
                {
                    CompanyId = CompanyId,

                }).ToList();

            }

            return objAcc;



        }
        public List<CompanyModel> GetVendorList(int CompanyId)
        {

            List<CompanyModel> objVC;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT c.Id,c1.Name CompanyName,c.VendorId,c1.IsVendorFirmRegistered VendorCompanyIsVendorFirmRegistered FROM companyVendor c
                                 inner join  Companies c1 on c.VendorId=c1.Id   
                                where c.CompanyId = @CompanyId and c.IsActive=1 and c.IsDeleted<>1 and c1.IsActive=1 ";
                objVC = con.Query<CompanyModel>(query, new
                {
                    CompanyId = CompanyId,

                }).ToList();

            }

            return objVC;



        }
        public List<TIGHTEN.MODEL.Vendor.VendorPOCModel> GetVendorPOCList(int CompanyId, int VendorId)
        {

            List<TIGHTEN.MODEL.Vendor.VendorPOCModel> objVC;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT c.Id,c.FirstName as pocFirstName,c.LastName pocLastName,(c.FirstName+' '+c.LastName+' - '+Convert(varchar(10),c.Id)) as pocFullName,c.UserId,c.Email pocEmail FROM UserDetails c
                                 inner join  UserCompanyRoles c1 on c.UserId=c1.UserId   
                                where c.CompanyId = @VendorId and c.IsActive=1 and c.IsDeleted<>1 and c1.IsDeleted<>1 and c.UserId in (select userid from UserCompanyRoles where CompanyId=@CompanyId and RoleId=(Select RoleId from Roles where [Name]='POC'))
group by c.Id,c.FirstName,c.LastName ,c.UserId,c.Email
";
                objVC = con.Query<TIGHTEN.MODEL.Vendor.VendorPOCModel>(query, new
                {
                    CompanyId = CompanyId,
                    VendorId = VendorId

                }).ToList();

            }

            return objVC;



        }

        // sending mail for account craation
        private void ThreadForInvitationVendor(AccountMail objCompanysend, string url)
        {
          //  string abc = @"Hello vendor name , 
//Google have just created an account - account name  with you to manage his team x. You need to assign the pocs to that account . Please do the needful . ";
            var callbackUrl = url + "#/confirmemail/" + objCompanysend.UserId + "/" + objCompanysend.EmailConfirmationCode + "/" + 1;
            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendToVendorOnAccountCreation")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@VendorCompanyName", objCompanysend.VendorCompanyName).Replace("@@CompanyName", objCompanysend.SenderCompanyName)
                .Replace("@@AccountName", objCompanysend.Name).Replace("@@TeamName", objCompanysend.TeamName)
                .Replace("@@callbackUrl", callbackUrl);
            //     Subject = Subject.Replace("@@SenderName", objCompanysend.Name);

            EmailUtility.SendMailInThread(objCompanysend.Email, Subject, emailbody);


        }

        // sending mail for account craation
        private void ThreadForPOCAssign(List<AccountMail> objCompanysendList, string url)
        {

            foreach (var objCompanysend in objCompanysendList)
            {
                var callbackUrl = url + "#/confirmemail/" + objCompanysend.UserId + "/" + objCompanysend.EmailConfirmationCode + "/" + 1;
                String emailbody = null;
                string Subject = string.Empty;

                XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                var MailModule = from r in xdoc.Descendants("MailModule").
                                 Where(r => (string)r.Attribute("id") == "SendToCompanyOnAssignPOC")
                                 select new
                                 {
                                     mailbody = r.Element("Body").Value,
                                     subject = r.Element("Subject").Value
                                 };

                foreach (var Mailitem in MailModule)
                {
                    emailbody = Mailitem.mailbody;
                    Subject = Mailitem.subject;
                }

                emailbody = emailbody.Replace("@@VendorCompanyName", objCompanysend.VendorCompanyName).Replace("@@CompanyPersonName", objCompanysend.CompanyPersonName)
                    .Replace("@@AccountName", objCompanysend.Name).Replace("@@TeamName", objCompanysend.TeamName)
                    .Replace("@@callbackUrl", callbackUrl);
                //     Subject = Subject.Replace("@@SenderName", objCompanysend.Name);

                EmailUtility.SendMailInThread(objCompanysend.Email, Subject, emailbody);
            }

        }

        public string SaveAccountDetail(AccountModel model,string url)
        {
            string returnObj;
            string msg = string.Empty; 
            AccountMail objCompanysend = new AccountMail();
            using (var con = new SqlConnection(ConnectionString))
            {

                msg = con.Query<string>("Usp_SaveNewAccount", new
                {
                    Name = model.Name,
                    TeamId = model.TeamId,
                    CompanyId = model.CompanyId,
                    VendorId = model.VendorId,
                    Description = model.Description
                }, commandType: CommandType.StoredProcedure

                 ).FirstOrDefault();

                returnObj = msg;
                //objCompanysend.Name = model.Name;
                objCompanysend = con.Query<AccountMail>("Usp_GetAccountDetailById", new
                {
                    Id = Convert.ToInt32(returnObj)
                }, commandType: CommandType.StoredProcedure

                 ).FirstOrDefault();
                HttpContext ctx = HttpContext.Current;
                if (objCompanysend != null)
                {
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        objCompanysend.EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
                        objCompanysend.UserId = model.UserId;
                        ThreadForInvitationVendor(objCompanysend, url);
                    }
                    ));
                    childref.Start();
                }
            }

            return returnObj;
        }

        public string saveAccountChildDetail(AccountModel model, string msg)
        {

            string returnObj;
            using (var con = new SqlConnection(ConnectionString))
            {

                //msg = con.Query<string>("Usp_SaveNewAccount", new
                //{
                //    Name = model.Name,
                //    TeamId = model.TeamId,
                //    CompanyId = model.CompanyId,
                //    VendorId =model.VendorId,
                //    Description = model.Description
                //}, commandType: CommandType.StoredProcedure

                // ).FirstOrDefault();

                if (msg != "-1")
                {
                    int AccId = Convert.ToInt32(msg);
                    string userIds = "";
                    string docsName = "";
                    string docsPath = "";
                    string docsAccFileNAme = "";
                    foreach (var AccPOCObj in model.AccountPOCList)
                    {
                        userIds += AccPOCObj.UserId + ",";
                    }
                    if (userIds.Length > 0)
                    {
                        userIds = userIds.Substring(0, userIds.Length - 1);
                    }

                    foreach (var AccDocsObj in model.AccountDocumentList)
                    {
                        docsName += AccDocsObj.Name + ",";
                        docsPath += AccDocsObj.Path + ",";
                        docsAccFileNAme += AccDocsObj.ActualFileName + ",";
                    }
                    if (docsName.Length > 0)
                    {
                        docsName = docsName.Substring(0, docsName.Length - 1);
                        docsPath = docsPath.Substring(0, docsPath.Length - 1);
                        docsAccFileNAme = docsAccFileNAme.Substring(0, docsAccFileNAme.Length - 1);
                    }

                    msg = con.Query<string>("Usp_SaveNewAccountChildDetail", new
                    {
                        AccId = AccId,
                        UserIds = userIds,
                        DocsName = docsName,
                        DocsPath = docsPath,
                        DocsActualFileName = docsAccFileNAme
                    }, commandType: CommandType.StoredProcedure

                ).FirstOrDefault();
                    returnObj = msg;
                }
                else
                {
                    returnObj = msg;
                }

            }

            return returnObj;



        }

        public int CheckAccountExist(int CompanyId, int TeamId, int VendorId)
        {
            int ResultId = 0;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query;
                // query new Project creation
                query = @"If Not Exists (select c.Id  from CompanyVendorAccount c where c.TeamId = @TeamId and  c.CompanyId = @CompanyId and c.VendorId=@VendorId and c.IsDeleted<>1)
                                Begin select ResultId = 1 end else begin select -1 end";

                ResultId = con.Query<int>(query, new { CompanyId = CompanyId, TeamId = TeamId, VendorId = VendorId }).SingleOrDefault();

            }
            return ResultId;

        }

        public AccountModel GetCompanyAccount(int Id)
        {

            AccountModel objAcc;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"SELECT Id,[Name],CompanyId,(select top 1 name from companies where id=CompanyVendorAccount.CompanyId) CompanyName,VendorId,(select top 1 name from companies where id=CompanyVendorAccount.VendorId) VendorCompanyName,TeamId,(select top 1 TeamName from Teams where id=CompanyVendorAccount.TeamId) TeamName, CreatedDate,IsActive,
                (select top 1 IsVendorFirmRegistered from Companies where id=CompanyVendorAccount.VendorId) IsRegisteredVendor,Description

                FROM CompanyVendorAccount  where Id = @id";
                objAcc = con.Query<AccountModel>(query, new
                {
                    id = Id,

                }).FirstOrDefault();

                query = @"SELECT CompanyVendorPOCAccount.Id,CompanyVendorPOCAccount.AccId,CompanyVendorPOCAccount.UserId,UserDetails.FirstName,UserDetails.LastName,UserDetails.Email,(UserDetails.FirstName+' '+UserDetails.LastName+' - '+Convert(varchar(50),UserDetails.Id)) as pocFullName FROM CompanyVendorPOCAccount inner join  
                                UserDetails on UserDetails.UserId=CompanyVendorPOCAccount.UserId and UserDetails.IsActive=1 and UserDetails.IsDeleted<>1
                                where CompanyVendorPOCAccount.AccId = @id and CompanyVendorPOCAccount.IsDeleted<>1";
                objAcc.AccountPOCList = con.Query<AccountPOCModel>(query, new
                {
                    id = Id,

                }).ToList();

                query = @"SELECT Id,AccId,[Name],Path,ActualFileName FROM CompanyVendorDocsAccount  
                                
                                where AccId = @id and IsDeleted<>1";
                objAcc.AccountDocumentList = con.Query<AccountDocumentsModel>(query, new
                {
                    id = Id,

                }).ToList();

            }

            return objAcc;



        }

        public string deleteCompanyAccountDetail(int Id)
        {
            string msg = string.Empty;
            int ResponseId;

            using (var con = new SqlConnection(ConnectionString))

            {

                ResponseId = con.Query<int>("Usp_deleteCompanyAccountDetail", new
                {

                    Id = Id,


                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }
            if (ResponseId == 1)
            {
                msg = "CompanyAccDetailDeletedSuccessfully";
            }
            return msg;
        }
        public string updateCompanyAccountDetail(AccountModel saveObj,string url)
        {
            string msg = string.Empty;
            int ResponseId;
            string userIds = "";
            List<AccountMail> objCompanysend = new List<AccountMail>();
            foreach (var POCObj in saveObj.AccountPOCList)
            {
                userIds += POCObj.UserId + ",";
            }
            if (userIds.Length > 0)
            {
                userIds = userIds.Substring(0, userIds.Length - 1);
            }
            using (var con = new SqlConnection(ConnectionString))

            {

                ResponseId = con.Query<int>("Usp_UpdateCompanyAccountDetail", new
                {

                    AccId = saveObj.Id,
                    UserIds = userIds,
                    Description = saveObj.Description

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                objCompanysend = con.Query<AccountMail>("Usp_GetAccountDetailForMailOnAssignPOC", new
                {
                    Id = saveObj.Id
                }, commandType: CommandType.StoredProcedure

                 ).ToList();
            }
            if (ResponseId >= 1)
            {
                msg = "CompanyAccDetailUpdatedSuccessfully";
                if (saveObj.AccountPOCList.Count > 0)
                {
                    HttpContext ctx = HttpContext.Current;
                    if (objCompanysend.Count>0)
                    {
                        Thread childref = new Thread(new ThreadStart(() =>
                        {
                            HttpContext.Current = ctx;
                            foreach (var objCompanySendObj in objCompanysend)
                            {
                                
                                objCompanySendObj.EmailConfirmationCode = EmailUtility.GetUniqueKey(8);
                                objCompanySendObj.UserId = saveObj.UserId;
                            }
                            ThreadForPOCAssign(objCompanysend, url);
                        }
                        ));
                        childref.Start();
                    }

                }


            }
            else if (ResponseId == -1)
            {
                msg = "CompanyAccountNotExists";
            }
            return msg;
        }


        public AccountDocumentsModel getAccountDocDetail(int attachementId)
        {
            using (var con = new SqlConnection(ConnectionString))

            {
                string query = @"SELECT Id,AccId,[Name],Path,ActualFileName FROM CompanyVendorDocsAccount  
                                
                                where Id = @Attachementid and IsDeleted<>1";
                return con.Query<AccountDocumentsModel>(query, new
                {
                    Attachementid = attachementId,

                }).FirstOrDefault();
            }
            return null;
        }
        public List<AccountModel> getAccountDetailByVendorId(int VendorId)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select c.Name as CompanyName,CompanyVendorAccount.id ,CompanyVendorAccount.name, CompanyVendorAccount.CreatedDate,CompanyVendorAccount.IsActive,

            STUFF((SELECT CAST(',' AS nvarchar(max)) + (select FirstName+' '+LastName from userdetails where UserId= b.UserId) 
            FROM CompanyVendorPOCAccount  as b 
            WHERE b.accid = a.id and b.Isdeleted <>1
            FOR XML PATH(''), TYPE 
            ).value('.', 'nvarchar(max)' 
         ),1, 1,'') as POCNames from 
		 (SELECT DISTINCT id FROM CompanyVendorAccount ) AS a left outer join 
		 CompanyVendorAccount on a.id=CompanyVendorAccount.id inner join
         Companies c on c.Id=CompanyVendorAccount.CompanyId
		 where CompanyVendorAccount.VendorId = @vendorId and CompanyVendorAccount.IsDeleted<>1";
                List<AccountModel> returnList = con.Query<AccountModel>(query, new
                {
                    vendorId = VendorId,

                }).ToList();
                foreach (var AccObj in returnList)
                {
                    if (string.IsNullOrEmpty(AccObj.CreatedDate) == false)
                    {
                        AccObj.CreatedDate = Convert.ToDateTime(AccObj.CreatedDate).ToShortDateString();
                    }
                    
                }
                return returnList;
                
            }
            //return null;
        }
        public List<AccountModel> getAccountDetailByCompanyId(int CompanyId,int VendorId)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select c.Name as CompanyName,CompanyVendorAccount.id ,CompanyVendorAccount.name, CompanyVendorAccount.CreatedDate,CompanyVendorAccount.IsActive,

            STUFF((SELECT CAST(',' AS nvarchar(max)) + (select FirstName+' '+LastName from userdetails where UserId= b.UserId) 
            FROM CompanyVendorPOCAccount  as b 
            WHERE b.accid = a.id and b.Isdeleted <>1
            FOR XML PATH(''), TYPE 
            ).value('.', 'nvarchar(max)' 
         ),1, 1,'') as POCNames from 
		 (SELECT DISTINCT id FROM CompanyVendorAccount ) AS a left outer join 
		 CompanyVendorAccount on a.id=CompanyVendorAccount.id inner join
         Companies c on c.Id=CompanyVendorAccount.CompanyId
		 where CompanyVendorAccount.CompanyId = @companyId and CompanyVendorAccount.IsDeleted<>1 and CompanyVendorAccount.VendorId = @vendorId";
                List<AccountModel> returnList = con.Query<AccountModel>(query, new
                {
                    companyId = CompanyId,
                    vendorId=VendorId

                }).ToList();
                foreach (var AccObj in returnList)
                {
                    if (string.IsNullOrEmpty(AccObj.CreatedDate) == false)
                    {
                        AccObj.CreatedDate = Convert.ToDateTime(AccObj.CreatedDate).ToShortDateString();
                    }

                }
                return returnList;

            }
            //return null;
        }
        public List<AccountModel> getAccountDetailById(int Id)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select c.Name as CompanyName,CompanyVendorAccount.id ,CompanyVendorAccount.name, CompanyVendorAccount.CreatedDate,

            STUFF((SELECT CAST(',' AS nvarchar(max)) + (select FirstName+' '+LastName from userdetails where UserId= b.UserId) 
            FROM CompanyVendorPOCAccount  as b 
            WHERE b.accid = a.id and b.Isdeleted <>1
            FOR XML PATH(''), TYPE 
            ).value('.', 'nvarchar(max)' 
         ),1, 1,'') as POCNames from 
		 (SELECT DISTINCT id FROM CompanyVendorAccount ) AS a left outer join 
		 CompanyVendorAccount on a.id=CompanyVendorAccount.id inner join
         Companies c on c.Id=CompanyVendorAccount.CompanyId
		 where CompanyVendorAccount.Id = @id and CompanyVendorAccount.IsDeleted<>1";
                List<AccountModel> returnList = con.Query<AccountModel>(query, new
                {
                    id = Id

                }).ToList();
                foreach (var AccObj in returnList)
                {
                    if (string.IsNullOrEmpty(AccObj.CreatedDate) == false)
                    {
                        AccObj.CreatedDate = Convert.ToDateTime(AccObj.CreatedDate).ToShortDateString();
                    }

                }
                return returnList;

            }
            //return null;
        }

        public List<AccountModel> GetMyEnterpriseByCompanyName(AccountModel model)
        {
            List<AccountModel> returnList=new List<AccountModel>();
            using (var con = new SqlConnection(ConnectionString))

            {

                returnList = con.Query<AccountModel>("usp_GetMyEnterpriseCompaniesByCompanyName", new
                {

                    VendorId = model.VendorId,
                    CompanyId=model.CompanyId,
                    CompanyName = model.CompanyName
                    

                }, commandType: CommandType.StoredProcedure).ToList();

            }
            return returnList;
        }

        public List<AccountModel> GetCompanyAccByCompanyId(int companyId)
        {
            List<AccountModel> returnList = new List<AccountModel>();
            using (var con = new SqlConnection(ConnectionString))

            {

                returnList = con.Query<AccountModel>("Usp_GetCompanyAccountsOfCompany", new
                {
                    CompanyId = companyId


                }, commandType: CommandType.StoredProcedure).ToList();

            }
            return returnList;
        }
    }
}
