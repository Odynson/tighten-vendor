﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL.Initiative;
using TIGHTEN.UTILITIES;
using TIGHTEN.DATA;
using System.Data;
using TIGHTEN.MODEL;
//using static TIGHTEN.MODEL.Initiative;
using System.Net.Http;
namespace TIGHTEN.DATA.Initiative
{
    public class InitiativeDAL : BaseClass
    {
        public List<InitiativeModel> GetInitiativeList(int CompanyId)
        {

            List<InitiativeModel> objInit;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"select id,initiative_title,description,(select count( distinct vendorId) from initiativeVendors where initiativeid=Init.Id and initiativeVendors.IsDeleted<>1) noOfVendors,Convert(decimal(18,2),0) totalAmountSpent,0 totalHrs,status from Initiative Init   where enterpriseCompanyId = @CompanyId and IsDeleted<>1";
                objInit = con.Query<InitiativeModel>(query, new
                {
                    CompanyId = CompanyId,

                }).ToList();

            }

            return objInit;



        }

        public List<initiativeVendorsModel> GetVendorProjectInitiativeList(int InitiativeId)
        {

            List<initiativeVendorsModel> objInVendorProjectList;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"select Id,vendorId,projectid,(select name from Companies where Id=InitVP.vendorId) vendorName,
                                (select name from projects where Id=InitVP.projectid) projectName,
                                Convert(decimal(18,2),0) AmtSpent,0 hrsBilled,contactperson from initiativevendors InitVP   where initiativeId = @initiativeId and IsDeleted<>1";
                objInVendorProjectList = con.Query<initiativeVendorsModel>(query, new
                {
                    initiativeId = InitiativeId,

                }).ToList();

            }

            return objInVendorProjectList;



        }
        public int CheckInitiativeExist(int CompanyId, string InitiativeTitle, int InitiativeId)
        {
            int ResultId = 0;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query;
                if (InitiativeId == 0)
                {
                    // query new Project creation
                    query = @"If Not Exists (select prj.initiative_title  from initiative prj where initiative_title = @InitiativeTitle and  prj.enterpriseCompanyID = @CompanyId and prj.IsDeleted<>1)
                                Begin select ResultId = 1 end else begin select -1 end";
                }
                else
                {
                    // query edit case of project
                    query = @"If Not Exists (select prj.initiative_title  from initiative prj where initiative_title = @InitiativeTitle and  prj.enterpriseCompanyID = @CompanyId and prj.Id <> @InitiativeId and prj.IsDeleted<>1)
                                Begin select ResultId = 1 end else begin select -1 end";
                }
                ResultId = con.Query<int>(query, new { CompanyId = CompanyId, InitiativeTitle = InitiativeTitle, InitiativeId = InitiativeId }).SingleOrDefault();

            }
            return ResultId;

        }
        public int SaveNewInitiative(InitiativeModel model)
        {
            string msg = string.Empty;
            string ProjectName;
            
            int datasave = 0;
            
            
            StringBuilder VendorList = new StringBuilder();
            StringBuilder ProjectList = new StringBuilder();
            StringBuilder ContactPErsonList = new StringBuilder();
            StringBuilder PhNoList = new StringBuilder();
            foreach (var Item in model.initiativeVendors)
            {
                    VendorList.Append(Item.vendorId).Append(',');
                ProjectList.Append(Item.projectid).Append(',');
                ContactPErsonList.Append(Item.contactperson).Append(',');
                PhNoList.Append(Item.contactpersonno).Append(',');

            }

            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("Usp_SaveNewInitiative", new
                {
                    UserId = model.createdby,
                    CompanyId = model.enterpriseCompanyID,
                    InititiativeTitle = model.initiative_title,
                    InititiativeDescription = model.description,
                    contactperson = model.contactperson,
                    contactpersonphno = model.contactpersonno,
                    status=model.status,
                    FileNameList = model.FileName,
                    ActualFileNameList = model.ActualFileName,
                    FilePathList = model.FilePath,
                    VendorList = VendorList.ToString(),
                    ProjectList = ProjectList.ToString(),
                    VendorContactList = ContactPErsonList.ToString(),
                    VendorPhNoList = PhNoList.ToString()
                }, commandType: CommandType.StoredProcedure);


                InitiativeModel ResultObj = Result.Read<InitiativeModel>().FirstOrDefault();

                
                datasave = ResultObj == null ? 0 : 1;

            

            }


            return datasave;
        }
        public int DeleteVendorProjectInitiativeFromList(int Id)
        {

           int objInVendorProjectList=0;
            InitiativeModel ResultObj = new InitiativeModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("usp_deleteVendorProjectInitiative", new
                {
                    Id = Id
                }, commandType: CommandType.StoredProcedure);


                ResultObj = Result.Read<InitiativeModel>().FirstOrDefault();

            }

            return objInVendorProjectList=ResultObj==null?0: ResultObj.Id;



        }

        public InitiativeModel GetInitiativeById(int InitiativeId)
        {
            
            InitiativeModel ResultObj = new InitiativeModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetVendorProjectInitiativeById", new
                {
                    Id = InitiativeId
                }, commandType: CommandType.StoredProcedure);


                ResultObj = Result.Read<InitiativeModel>().FirstOrDefault();
                ResultObj.initiativeVendors= Result.Read<initiativeVendorsModel>().ToList();
                ResultObj.vendorProjectDocumentList= Result.Read<newVendorProjectDocument>().ToList();

            }

            return ResultObj;



        }
        public int DeleteInitiative(int Id)
        {

            int objInVendorProjectList = 0;
            InitiativeModel ResultObj = new InitiativeModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("usp_deleteInitiative", new
                {
                    Id = Id
                }, commandType: CommandType.StoredProcedure);


                ResultObj = Result.Read<InitiativeModel>().FirstOrDefault();

            }

            return objInVendorProjectList = ResultObj == null ? 0 : ResultObj.Id;



        }

        public int EditInitiative(InitiativeModel model)
        {
            string msg = string.Empty;
            string ProjectName;

            int datasave = 0;

            StringBuilder VendorProjectIdList = new StringBuilder();
            StringBuilder VendorList = new StringBuilder();
            StringBuilder ProjectList = new StringBuilder();
            StringBuilder ContactPErsonList = new StringBuilder();
            StringBuilder PhNoList = new StringBuilder();
            StringBuilder VPIsDeletedList = new StringBuilder();
            StringBuilder VPListIsAdded = new StringBuilder();
            foreach (var Item in model.initiativeVendors)
            {
                VendorProjectIdList.Append(Item.Id).Append(',');
                VendorList.Append(Item.vendorId).Append(',');
                ProjectList.Append(Item.projectid).Append(',');
                ContactPErsonList.Append(Item.contactperson).Append(',');
                PhNoList.Append(Item.contactpersonno).Append(',');
                VPIsDeletedList.Append(Item.isDeleted).Append(',');
                VPListIsAdded.Append(Item.IsAdded).Append(',');

            }

            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("Usp_EditSaveNewInitiative", new
                {
                    InitiativeId=model.Id,
                    UserId = model.createdby,
                    CompanyId = model.enterpriseCompanyID,
                    InititiativeTitle = model.initiative_title,
                    InititiativeDescription = model.description,
                    contactperson = model.contactperson,
                    contactpersonphno = model.contactpersonno,
                    status=model.status,
                    DocIdList =model.DocIdList,
                    FileNameList = model.FileName,
                    ActualFileNameList = model.ActualFileName,
                    FilePathList = model.FilePath,
                    DocIsDeletedList=model.DocIsDeletedList,
                    DocListIsAdded=model.DocListIsAdded,
                    VendorProjectIdList= VendorProjectIdList.ToString(),
                    VendorList = VendorList.ToString(),
                    ProjectList = ProjectList.ToString(),
                    VendorContactList = ContactPErsonList.ToString(),
                    VendorPhNoList = PhNoList.ToString(),
                    VPIsDeletedList = VPIsDeletedList.ToString(),
                    VPListIsAdded = VPListIsAdded.ToString()
                }, commandType: CommandType.StoredProcedure);


                InitiativeModel ResultObj = Result.Read<InitiativeModel>().FirstOrDefault();


                datasave = ResultObj == null ? 0 :1;



            }


            return datasave;
        }
    }
}
