﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.UTILITIES;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class Organization : BaseClass
    {
        //AppContext dbcontext;

        public List<MODEL.OrganizationModel> GetAllOrganizations(string UserId)
        {
            //var SentBy = (from freelancrInv in dbcontext.FreelancerInvitations
            //              join usde in dbcontext.UserDetails on freelancrInv.FreelancerEmail equals usde.Email
            //              where usde.UserId == UserId && usde.IsDeleted == false
            //              select freelancrInv.SentBy).FirstOrDefault();

            //var Organization = (from usrdet in dbcontext.UserDetails
            //                    join usrcmpnyrole in dbcontext.UserCompanyRoles on usrdet.CompanyId equals usrcmpnyrole.CompanyId
            //                    join cmp in dbcontext.Companies on usrcmpnyrole.CompanyId equals cmp.Id
            //                    join role in dbcontext.Roles on usrcmpnyrole.RoleId equals role.RoleId
            //                    where usrcmpnyrole.UserId == UserId && usrdet.IsDeleted == false
            //                    && usrdet.ParentUserId == null && role.IsDeleted == false
            //                    && usrdet.IsFreelancer == false
            //                    select new
            //                    {
            //                        CompanyId = usrcmpnyrole.CompanyId,
            //                        CompanyUserId = usrdet.UserId,
            //                        CompanyName = cmp.Name,
            //                        CompanyUserName = usrdet.FirstName,
            //                        Role=role.Name,

            //                        //CompanyProfilePic = usrdet.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + usrdet.ProfilePhoto,
            //                        //CompanyAssignedOnDate = GroupedData.Key.CompanyAssignedOnDate,
            //                        CompanyUserProfilePic = usrdet.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + usrdet.ProfilePhoto,
            //                        CompanyProfilePic = cmp.Logo == null ? "Uploads/Default/nologo.png" : "Uploads/CompanyLogos/" + cmp.Logo,
            //                        Rate = usrdet.Rate,
            //                        //for temprary demo I use DateTime.Now
            //                        CompanyAssignedOnDate = DateTime.Now,
                                     
            //                        loggedInHours = (
            //                                          from todo in dbcontext.ToDos
            //                                          join sec in dbcontext.Sections on todo.SectionId equals sec.Id
            //                                          join prjct in dbcontext.Projects on sec.ProjectId equals prjct.Id
            //                                          where prjct.CompanyId == usrcmpnyrole.CompanyId
            //                                          && todo.AssigneeId == UserId && prjct.IsDeleted == false
            //                                          && todo.IsDone == true && todo.IsApproved == true
            //                                          && todo.IsDeleted == false
            //                                          select new MODEL.LoggedHour
            //                                          {
            //                                              LoggedInHour = todo.LoggedTime
            //                                          }
            //                                         )

            //                    });




            //List<MODEL.OrganizationModel> OrganizationList1 = Organization.AsEnumerable().Select(item => new MODEL.OrganizationModel
            //{
            //    CompanyId = item.CompanyId.Value,
            //    CompanyUserId = item.CompanyUserId,
            //    Role=item.Role,
            //    CompanyName = item.CompanyName,
            //    CompanyProfilePic = item.CompanyProfilePic,
            //    CompanyUserName = item.CompanyUserName,
            //    CompanyUserProfilePic = item.CompanyUserProfilePic,
            //    CompanyAssignedOnDate = item.CompanyAssignedOnDate,
            //    Rate = item.Rate,
            //    loggedInHours = item.loggedInHours.ToList(),

            //}
            //).ToList();



            List<MODEL.OrganizationModel> OrganizationList;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetAllOrganizations", new { UserId = UserId },commandType:System.Data.CommandType.StoredProcedure);
                OrganizationList = result.Read<MODEL.OrganizationModel>().ToList();

                if (OrganizationList != null)
                {
                    var loggedInHours = result.Read<MODEL.LoggedHour>().ToList();
                    foreach (var item in OrganizationList)
                    {
                        item.loggedInHours = loggedInHours.Where(x => x.CompanyId == item.CompanyId).ToList();
                    }
                }
            }

            // This need to be changed after some time ,  this is a temporary fix
            // Here we have to find some other method to find Total Hours 

            #region Changable Data


            decimal rate = 0;

            foreach (var item in OrganizationList)
            {
                int TotalMinutesForTodos = 0;
                foreach (var LoggedTime in item.loggedInHours)
                {
                    int LoggedTimeInMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(LoggedTime.LoggedInHour);

                    TotalMinutesForTodos += LoggedTimeInMinutes;
                }


                rate = item.Rate;
                decimal totalamt = 0;
                int TotalHours = (TotalMinutesForTodos / 60);
                totalamt = (TotalHours * rate);

                int TotalMinutes = (TotalMinutesForTodos % 60);
                if (TotalMinutes > 0)
                {
                    decimal totalAmtForMinutes = (Convert.ToDecimal(TotalMinutes / 60.00) * rate);
                    totalamt = totalamt + totalAmtForMinutes;
                }


                item.TotalTime = TotalHours + "h " + TotalMinutes + "m";

                item.TotalAmount = Math.Round(totalamt, 2);


            }




            #endregion


            return OrganizationList;
        }


        public List<MODEL.OrganizationTeamMembersModel> GetOrganization(string UserId, int CompanyId)
        {

            //var Organization = (from team in dbcontext.Teams
            //                    join teamusr in dbcontext.TeamUsers on team.Id equals teamusr.TeamId
            //                    join usrdetail in dbcontext.UserDetails on teamusr.UserId equals usrdetail.UserId
            //                    where team.CompanyId == CompanyId && usrdetail.IsDeleted == false
            //                    && teamusr.UserId == UserId && team.IsDeleted == false
            //                    select new
            //                    {
            //                        TeamName = team.TeamName,
            //                        TeamDescription = team.Description,
            //                        CreatedDate = team.CreatedDate,
            //                        TeamMembers = (
            //                                        from tu in dbcontext.TeamUsers
            //                                        join udd in dbcontext.UserDetails on tu.UserId equals udd.UserId
            //                                        where tu.TeamId == team.Id && udd.IsDeleted == false
            //                                        select new MODEL.OrganizationTeamProjectMembersModel
            //                                        {
            //                                            MemberName = udd.FirstName,
            //                                            MemberEmail = udd.Email,
            //                                            MemberUserId = udd.UserId,
            //                                            MemberProfilePhoto = udd.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + udd.ProfilePhoto
            //                                        }
            //                                      ),



            //                        Projects = (
            //                                              from prjct in dbcontext.Projects
            //                                              join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId

            //                                              where prjct.CompanyId == CompanyId && prjct.IsDeleted == false
            //                                              && prjct.TeamId == team.Id
            //                                              && prjctusr.UserId == UserId
            //                                              select new
            //                                              {
            //                                                  ProjectName = prjct.Name,
            //                                                  ProjectDescription = prjct.Description,
            //                                                  CreatedDate = prjct.CreatedDate,


            //                                                  ProjectMembers = (
            //                                                                      from pu in dbcontext.ProjectUsers
            //                                                                      join ud in dbcontext.UserDetails on pu.UserId equals ud.UserId
            //                                                                      where pu.ProjectId == prjct.Id && ud.IsDeleted == false
            //                                                                      select new MODEL.OrganizationTeamProjectMembersModel
            //                                                                      {
            //                                                                          MemberName = ud.FirstName,
            //                                                                          MemberEmail = ud.Email,
            //                                                                          MemberUserId = ud.UserId,
            //                                                                          MemberProfilePhoto = ud.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + ud.ProfilePhoto
            //                                                                      }
            //                                                                    ),


            //                                                  loggedInHours = (
            //                                                    from usrcmrl in dbcontext.UserCompanyRoles
            //                                                    join projct in dbcontext.Projects on usrcmrl.CompanyId equals projct.CompanyId
            //                                                    join sec in dbcontext.Sections on projct.Id equals sec.ProjectId
            //                                                    join todo in dbcontext.ToDos on sec.Id equals todo.SectionId
            //                                                    where usrcmrl.CompanyId == CompanyId && projct.IsDeleted == false
            //                                                    && usrcmrl.UserId == UserId && todo.IsDeleted == false
            //                                                    && todo.IsDone == true && todo.IsApproved == true
            //                                                    && projct.Id == prjct.Id
            //                                                    select new MODEL.LoggedHour
            //                                                    {
            //                                                        LoggedInHour = todo.LoggedTime
            //                                                    }
            //                                                   )

                                                              
            //                                              }
            //                                             )

            //                    });






            //List<MODEL.OrganizationTeamMembersModel> OrganizationList1 = Organization.AsEnumerable().Select(item => new MODEL.OrganizationTeamMembersModel
            //{
            //    TeamMembers = item.TeamMembers.ToList(),
            //    TeamName = item.TeamName,
            //    TeamDescription = item.TeamDescription,
            //    CreatedDate = item.CreatedDate,

            //    Projects = item.Projects.AsEnumerable().Select(projectitem => new MODEL.OrganizationProjectModel
            //    {
            //        ProjectName = projectitem.ProjectName,
            //        ProjectDescription = projectitem.ProjectDescription,
            //        ProjectMembers = projectitem.ProjectMembers.ToList(),
            //        CreatedDate = projectitem.CreatedDate,
            //        loggedInHours = projectitem.loggedInHours.ToList()
            //    }
            //    ).ToList()
            //}
            //).ToList();


            List<MODEL.OrganizationTeamMembersModel> OrganizationList;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetOrganization", new { CompanyId = CompanyId, UserId = UserId }, commandType: System.Data.CommandType.StoredProcedure);

                OrganizationList = result.Read<MODEL.OrganizationTeamMembersModel>().ToList();

                if (OrganizationList != null)
                {
                    var TeamMembers = result.Read<MODEL.OrganizationTeamProjectMembersModel>().ToList();
                    var Projects = result.Read<MODEL.OrganizationProjectModel>().ToList();
                    var ProjectMembers = result.Read<MODEL.OrganizationTeamProjectMembersModel>().ToList();
                    var loggedInHours = result.Read<MODEL.LoggedHour>().ToList();

                    foreach (var item in OrganizationList)
                    {
                        item.TeamMembers = TeamMembers.Where(x => x.TeamId == item.TeamId).ToList();
                        item.Projects = Projects.Where(x => x.TeamId == item.TeamId).ToList();

                        foreach (var prjctItem in item.Projects)
                        {
                            prjctItem.ProjectMembers = ProjectMembers.Where(x => x.ProjectId == prjctItem.ProjectId).ToList();
                            prjctItem.loggedInHours = loggedInHours.Where(x=>x.ProjectId == prjctItem.ProjectId).ToList();
                        }
                    }
                }

            }

                // This need to be changed after some time ,  this is a temporary fix
                // Here we have to find some other method to find Total Hours 

                #region Changable Data

                //int TotalMinutesForTodos = 0;
                decimal rate = 0;

            foreach (var item in OrganizationList)
            {
                foreach (var project in item.Projects)
                {
                    int TotalMinutesForTodos = 0;
                    foreach (var LoggedTime in project.loggedInHours)
                    {
                        int LoggedTimeInMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(LoggedTime.LoggedInHour);

                        TotalMinutesForTodos += LoggedTimeInMinutes;
                    }

                    // added fixed rate for demo
                    // need to change in the real scenario
                    rate = 10;
                    decimal totalamt = 0;
                    int TotalHours = (TotalMinutesForTodos / 60);
                    totalamt = (TotalHours * rate);

                    int TotalMinutes = (TotalMinutesForTodos % 60);
                    if (TotalMinutes > 0)
                    {
                        decimal totalAmtForMinutes = (Convert.ToDecimal(TotalMinutes / 60.00) * rate);
                        totalamt = totalamt + totalAmtForMinutes;
                    }


                    project.Hours = TotalHours + "h " + TotalMinutes + "m";

                    project.Amount = Math.Round(totalamt, 2);
                }


            }
            #endregion

            return OrganizationList;
        }


    }
}
