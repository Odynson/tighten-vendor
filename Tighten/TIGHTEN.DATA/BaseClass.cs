﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.DATA
{
    public class BaseClass 
    {
        public BaseClass()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["Tighten"].ToString();
        }
        public string ConnectionString { get; set; }
    }
}
