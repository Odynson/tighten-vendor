﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using Dapper;

namespace TIGHTEN.DATA
{
    public class ComingtodosCountDirective : Common
    {

        /// <summary>
        /// It gets All the Packages
        /// </summary>
        /// <returns>It returns All the Packages </returns>
        public List<TodoModel.TodosData> GetUserComingtodos(string UserId, int CompanyId)
        {
            dbcontext = new AppContext();

            List<TodoModel.TodosData> TodoList;

            string query = @"select * from ToDoes tod where tod.AssigneeId = @UserId and tod.IsDeleted = 0 and tod.DeadlineDate > getdate()
                            and tod.DeadlineDate < dateadd(dd,7,getdate())";

            using (var con = new SqlConnection(ConnectionString))
            {
                TodoList = con.Query<TodoModel.TodosData>(query,new { UserId = UserId }).ToList();
      
            }
            // DateTime DeadlineDate = DateTime.Now.AddDays(7);
            //TodoList = (from tod in dbcontext.ToDos
            //                                      where tod.AssigneeId == UserId && tod.IsDeleted == false
            //                                      && tod.DeadlineDate > DateTime.Now
            //                                      && tod.DeadlineDate < DeadlineDate
            //                                      select new TodoModel.TodosData
            //                                      {
            //                                          Name = tod.Name,
            //                                          Description = tod.Description
            //                                      }
            //                                  ).ToList();

            if(TodoList == null)
            {
               
            }
            return TodoList;
        }




    }
}
