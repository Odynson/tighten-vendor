﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using System.Data;

namespace TIGHTEN.DATA
{
    public class FreelancersHiredDirective : Common
    {

        public List<MODEL.FreelancersHiredDirective> GetFreelancersHired1(string UserId, int CompanyId)
        {
            var Freelancerlist = (from usr in dbcontext.UserDetails
                                  join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
                                  where usrcom.CompanyId == CompanyId
                                  && usr.IsDeleted == false
                                  && usr.IsFreelancer == true
                                  select new
                                  {
                                      FreelancerUserId = usr.UserId,
                                      FreelancerName = usr.FirstName ,
                                      HourlyRate = usr.Rate,
                                      ProjectList = (from p in dbcontext.Projects
                                                     join pu in dbcontext.ProjectUsers on p.Id equals pu.ProjectId
                                                     where pu.UserId == usr.UserId
                                                     && p.IsDeleted == false
                                                     select new MODEL.FreelancersProject
                                                     {
                                                         ProjectId = p.Id,
                                                         ProjectName = p.Name,
                                                         ProjectDescription = p.Description
                                                     }
                                                    )

                                  }
                                  );

            List<MODEL.FreelancersHiredDirective> Freelancerslist = Freelancerlist.AsEnumerable().Select(x =>
             new MODEL.FreelancersHiredDirective
             {
                 FreelancerName = x.FreelancerName,
                 FreelancerUserId = x.FreelancerUserId,
                 HourlyRate = x.HourlyRate,
                 ProjectList = x.ProjectList.ToList()
             }).ToList();


            return Freelancerslist;
        }


        public List<MODEL.FreelancersHiredDirective> GetFreelancersHired(string UserId, int CompanyId)
        {

            List<MODEL.FreelancersHiredDirective> Freelancerslist;
            List<MODEL.FreelancersProject> ProjectList;
            using (var con = new SqlConnection(ConnectionString) )
            {

                var Result = con.QueryMultiple("Usp_GetFreelancersHired", new { UserId = UserId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);

                Freelancerslist = Result.Read<MODEL.FreelancersHiredDirective>().ToList();
                ProjectList = Result.Read<MODEL.FreelancersProject>().ToList();

                if (Freelancerslist != null && ProjectList != null)
                {
                    foreach (var item in Freelancerslist)
                    {

                        item.ProjectList = ProjectList.Where(x=> x.UserId == item.FreelancerUserId).ToList();
                    }

                 }

            }
                return Freelancerslist;
        }


    }
}
