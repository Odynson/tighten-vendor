﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using Dapper;

namespace TIGHTEN.DATA
{
    public class OverBudgetedProjectsCountDirective : Common
    {

        /// <summary>
        /// It gets All the Packages
        /// </summary>
        /// <returns>It returns All the Packages </returns>
        public List<MODEL.OverBudgetedProjectsCountDirective> GetUserOverBudgetedProjects1(string UserId, int CompanyId)
        {


            string query = @" select * from Projects p inner join ProjectUsers on pu.ProjectId = p.Id";


           var projectList =   (from p in dbcontext.Projects
                                  join pu in dbcontext.ProjectUsers on p.Id equals pu.ProjectId
                                  where pu.UserId == UserId && p.IsDeleted == false
                                  && p.CompanyId == CompanyId
                                  select new 
                                  {
                                      ProjectBudget = p.EstimatedBudget,
                                      ProjectId = p.Id,
                                      ProjectName = p.Name,
                                      ProjectDescription = p.Description,
                                      ProjectMembers = (from m3 in dbcontext.ProjectUsers
                                                        join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
                                                        where m3.ProjectId == p.Id
                                                        && m2.IsDeleted == false
                                                        select new ProjectModel.ProjectMembers
                                                        {
                                                            MemberEmail = m2.Email,
                                                            MemberName = m2.FirstName,
                                                            MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
                                                            MemberUserId = m2.UserId,
                                                            Rate = m3.UserRate.HasValue ? m3.UserRate.Value : 0
                                                        }),
                                      //TodoBudget = (from t in dbcontext.ToDos
                                      //                                    join sec in dbcontext.Sections on t.SectionId equals sec.Id
                                      //                                    join prjct in dbcontext.Projects on sec.ProjectId equals prjct.Id
                                      //                                    join PrjctUsr in dbcontext.ProjectUsers on prjct.Id equals PrjctUsr.ProjectId
                                      //                                    join invdet in dbcontext.InvoiceDetails on t.Id equals invdet.TodoId
                                      //                                    where PrjctUsr.UserId == UserId
                                      //                                    && (CompanyId == 0 ? prjct.CompanyId == prjct.CompanyId : prjct.CompanyId == CompanyId)
                                      //                                    select invdet.Amount
                                      //                                    ).Sum() ,

                                      IsTodoBudgetExceedsProjectBudget = (from t in dbcontext.ToDos
                                                                           join sec in dbcontext.Sections on t.SectionId equals sec.Id
                                                                           join prjct in dbcontext.Projects on sec.ProjectId equals prjct.Id
                                                                           join PrjctUsr in dbcontext.ProjectUsers on prjct.Id equals PrjctUsr.ProjectId
                                                                           join invdet in dbcontext.InvoiceDetails on t.Id equals invdet.TodoId
                                                                           where prjct.Id == p.Id && prjct.IsDeleted == false && t.IsDeleted == false
                                                                           && (CompanyId == 0 ? prjct.CompanyId == prjct.CompanyId : prjct.CompanyId == CompanyId)
                                                                           select invdet.Amount
                                                                          ).Sum() > p.EstimatedBudget ? true : false
                                                                           

                                  });


            List<MODEL.OverBudgetedProjectsCountDirective> projectModList = projectList.AsEnumerable().Select(item =>
                                                                              new MODEL.OverBudgetedProjectsCountDirective()
                                                                              {
                                                                                  ProjectId = item.ProjectId,
                                                                                  ProjectName = item.ProjectName,
                                                                                  ProjectDescription = item.ProjectDescription,
                                                                                  ProjectMembers = item.ProjectMembers.ToList(),
                                                                                  ProjectBudget = item.ProjectBudget,
                                                                                  IsTodoBudgetExceedsProjectBudget = item.IsTodoBudgetExceedsProjectBudget
                                                                              }).ToList();



            //var TodoBudget = (from t in dbcontext.ToDos
            //                  join sec in dbcontext.Sections on t.SectionId equals sec.Id
            //                  join prjct in dbcontext.Projects on sec.ProjectId equals prjct.Id
            //                  join PrjctUsr in dbcontext.ProjectUsers on prjct.Id equals PrjctUsr.ProjectId
            //                  join invdet in dbcontext.InvoiceDetails on t.Id equals invdet.TodoId
            //                  where PrjctUsr.UserId == UserId
            //                  && (CompanyId == 0 ? prjct.CompanyId == prjct.CompanyId : prjct.CompanyId == CompanyId)
            //                  select invdet.Amount
            //                  ).Sum();


            return projectModList;

        }


        public List<MODEL.OverBudgetedProjectsCountDirective> GetUserOverBudgetedProjects(string UserId, int CompanyId)
        {


            List<MODEL.OverBudgetedProjectsCountDirective> projectModList;
            List<ProjectModel.ProjectMembers> ProjectMembers;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("Usp_GetUserOverBudgetedProjects",
                    new { UserId = UserId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                projectModList = result.Read<MODEL.OverBudgetedProjectsCountDirective>().ToList();
                ProjectMembers = result.Read<ProjectModel.ProjectMembers>().ToList();

                if (projectModList != null && ProjectMembers != null)
                {
                    foreach (var item in projectModList)
                    {
                        item.ProjectMembers = ProjectMembers.Where(x => x.ProjectId == item.ProjectId).ToList();

                    }

                }
            }
            return projectModList;



        }


    }
}
