﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using Dapper;
using System.Data;

namespace TIGHTEN.DATA
{
    public class ProjectCountDirective : Common
    {

        /// <summary>
        /// It gets All the Packages
        /// </summary>
        /// <returns>It returns All the Packages </returns>
        public List<ProjectModel.ProjectWithMembers> GetUserProjects1(string UserId, int CompanyId)
        {
            dbcontext = new AppContext();

            var projectList = (from m1 in dbcontext.Projects
                               join PrjctUsr in dbcontext.ProjectUsers on m1.Id equals PrjctUsr.ProjectId
                               where PrjctUsr.UserId == UserId && m1.IsDeleted == false
                               && m1.CompanyId == CompanyId
                               orderby m1.CreatedDate descending
                               select new
                               {
                                   ProjectId = m1.Id,
                                   ProjectName = m1.Name,
                                   ProjectDescription = m1.Description,
                                   Accesslevel = m1.AccessLevel.HasValue ? m1.AccessLevel.Value : 0,
                                   //projectHours = m1.EstimatedHours,
                                   //projectBudget = m1.EstimatedBudget,
                                   projectHours = m1.EstimatedHours.HasValue ? m1.EstimatedHours.Value : 0,
                                   projectBudget = m1.EstimatedBudget.HasValue ? m1.EstimatedBudget.Value : 0,
                                   IsComplete = m1.IsComplete,
                                   ProjectMembers = (from m3 in dbcontext.ProjectUsers
                                                     join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
                                                     where m3.ProjectId == m1.Id
                                                     && m2.IsDeleted == false
                                                     select new ProjectModel.ProjectMembers
                                                     {
                                                         MemberEmail = m2.Email,
                                                         MemberName = m2.FirstName,
                                                         MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
                                                         MemberUserId = m2.UserId,
                                                         Rate = m3.UserRate.HasValue ? m3.UserRate.Value : 0
                                                     })
                               });


            List<ProjectModel.ProjectWithMembers> projectModList = projectList.AsEnumerable().Select(item =>
                                                                  new ProjectModel.ProjectWithMembers()
                                                                  {
                                                                      ProjectId = item.ProjectId,
                                                                      ProjectName = item.ProjectName,
                                                                      Accesslevel = item.Accesslevel,
                                                                      ProjectDescription = item.ProjectDescription,
                                                                      projectHours = item.projectHours,
                                                                      projectBudget = item.projectBudget,
                                                                      IsComplete = item.IsComplete,
                                                                      ProjectMembers = item.ProjectMembers.ToList()
                                                                  }).ToList();


            return projectModList;
        }


        public List<ProjectModel.ProjectWithMembers> GetUserProjects(string UserId, int CompanyId)
        {
            List<ProjectModel.ProjectWithMembers> projectModList;
            List<ProjectModel.ProjectMembers> ProjectMembers;

            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("Usp_GetUserProjects",
                    new { UserId = UserId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                projectModList = result.Read<ProjectModel.ProjectWithMembers>().ToList();
                ProjectMembers = result.Read<ProjectModel.ProjectMembers>().ToList();
                if (projectModList != null && ProjectMembers != null)
                {
                   foreach(var item in projectModList)
                    {
                        item.ProjectMembers = ProjectMembers.Where(x => x.ProjectId == item.ProjectId).ToList();


                    }
              
                }
            }

            return projectModList;
        }




    }
}
