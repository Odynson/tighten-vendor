﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using System.Data;
using Dapper;
namespace TIGHTEN.DATA
{
    public class TeamCountDirective : Common
    {

        /// <summary>
        /// This function fetches the Teams with team members for a particular Company
        /// </summary>
        /// <param name="companyId">It is the unique Id of Company</param>
        /// <returns>It returns Teams of a particular company with team members</returns>
        public List<TeamModel.TeamWithMembers> getCompanyTeams1(int CompanyId, string UserId)
        {
            dbcontext = new AppContext();
            var UserRole = (from user in dbcontext.UserDetails
                            join usrcom in dbcontext.UserCompanyRoles on user.UserId equals usrcom.UserId
                            where user.UserId == UserId
                            && user.IsDeleted == false
                            && usrcom.CompanyId == CompanyId
                            select usrcom.RoleId).FirstOrDefault();

            var compModList = (from data in
                               (from m in dbcontext.Teams
                                join tu in dbcontext.TeamUsers on m.Id equals tu.TeamId
                                where m.IsDefault == false && m.IsDeleted == false
                                && (CompanyId == 0 ? m.CompanyId == m.CompanyId : m.CompanyId == CompanyId)
                                && (UserRole == 1 || UserRole == 2 ? tu.UserId == tu.UserId : tu.UserId == UserId)
                                select new { m, tu }
                                )
                               group data by new { data.m.Id, data.m.TeamName, data.m.TeamBudget, data.m.Description }
                               into GroupedData
                               select new
                               {
                                   TeamId = GroupedData.Key.Id,
                                   TeamName = GroupedData.Key.TeamName,
                                   TeamBudget = GroupedData.Key.TeamBudget,
                                   TeamDescription = GroupedData.Key.Description,
                                   TeamMembers = (from m1 in dbcontext.TeamUsers
                                                  join m2 in dbcontext.UserDetails on m1.UserId equals m2.UserId
                                                  where m1.TeamId == GroupedData.Key.Id
                                                  && m2.IsDeleted == false
                                                  orderby m1.CreatedDate
                                                  select new TeamModel.TeamMembers
                                                  {
                                                      MemberEmail = m2.Email,
                                                      MemberName = m2.FirstName,
                                                      MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
                                                      MemberUserId = m2.UserId
                                                  })

                               });
            List<TeamModel.TeamWithMembers> compMod = compModList.AsEnumerable().Select(item =>
                                                          new TeamModel.TeamWithMembers()
                                                          {
                                                              TeamId = item.TeamId,
                                                              TeamName = item.TeamName,
                                                              TeamBudget = item.TeamBudget,
                                                              TeamDescription = item.TeamDescription,
                                                              TeamMembers = item.TeamMembers.ToList()
                                                          }).ToList();
            return compMod;
        }


        public List<TeamModel.TeamWithMembers> getCompanyTeams(int CompanyId, string UserId)
        {
            List<TeamModel.TeamWithMembers> compMod;
            List<TeamModel.TeamMembers> TeamMembers;
            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("Usp_GetCompanyTeamsDirective", new { UserId = UserId, CompanyId = CompanyId }, commandType
                    : CommandType.StoredProcedure);

                compMod = Result.Read<TeamModel.TeamWithMembers>().ToList();
                TeamMembers = Result.Read<TeamModel.TeamMembers>().ToList();

                foreach(var item in compMod)
                {

                    item.TeamMembers = TeamMembers.Where(x => x.TeamId == item.TeamId).ToList();
                }


            }

            return compMod;

        }


    }
}
