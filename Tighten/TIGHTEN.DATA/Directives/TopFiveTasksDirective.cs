﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using Dapper;

namespace TIGHTEN.DATA
{
    public class TopFiveTasksDirective : Common
    {
        public List<MODEL.TopFiveTasksDirective> GetTopFiveTasks(string UserId, int CompanyId)
        {



            List<MODEL.TopFiveTasksDirective> TaskList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select top 5 tod.Id TaskId,tod.Name TaskName, tod.Description TaskDescription from ToDoes tod where 
                                tod.AssigneeId = @UserId and tod.IsDeleted = 0 order by tod.CreatedDate desc";
                 TaskList = con.Query<MODEL.TopFiveTasksDirective>(query, new { UserId = UserId }).ToList();
                

            }



                      //TaskList = (from t in dbcontext.ToDos
                      //                                    where t.AssigneeId == UserId
                      //                                    && t.IsDeleted == false
                      //                                    orderby t.CreatedDate descending
                      //                                    select new MODEL.TopFiveTasksDirective
                      //                                    {
                      //                                        TaskId = t.Id,
                      //                                        TaskName = t.Name,
                      //                                        TaskDescription = t.Description
                      //                                    }
                      //                                    ).Take(5).ToList();
                    
            return TaskList;
        }


    }
}
