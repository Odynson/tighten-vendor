﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using Dapper;

namespace TIGHTEN.DATA
{
    public class TodoCountDirective : Common
    {

        /// <summary>
        /// It gets All the Packages
        /// </summary>
        /// <returns>It returns All the Packages </returns>
        public List<TodoModel.TodosData> GetUserCompletedTodos(string UserId, int CompanyId)
        {
            dbcontext = new AppContext();


            List<TodoModel.TodosData> TodoList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select tod.Name Name,tod.Description from ToDoes tod 
                            where  tod.AssigneeId = @UserId and tod.IsDeleted = 0";
                var result = con.QueryMultiple(query, new { UserId = UserId });
                TodoList = result.Read<TodoModel.TodosData>().ToList();

            }
                             //TodoList = (from tod in dbcontext.ToDos
                             //                     where tod.AssigneeId == UserId
                             //                     && tod.IsDeleted == false
                             //                     select new TodoModel.TodosData
                             //                     {
                             //                         Name = tod.Name,
                             //                         Description = tod.Description
                             //                     }
                             //                     ).ToList();


            return TodoList;
        }



    }
}
