﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL.RoleManageVendorAdminProfile;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class RoleManageVendorAdminProfileDLL : BaseClass
    {


        public List<RoleManageVendorAdminProfileModel> GetAllRolesForVendorAdminProfile(int VendorCompanyId)
        {
            List<RoleManageVendorAdminProfileModel> ObjRoleManageVendorAdminProfileList;
            using (var con = new SqlConnection(ConnectionString))
            {

             ObjRoleManageVendorAdminProfileList = con.Query<RoleManageVendorAdminProfileModel>("Ups_GetAllRolesForVendorCompanyProfile",new { VendorCompanyId = VendorCompanyId },commandType: CommandType.StoredProcedure).ToList();


            }
            return ObjRoleManageVendorAdminProfileList;
        }


        public string saveRole(MODEL.RoleManageVendorAdminProfile.RoleAddUpdateModel obj)
        {
            

            int IsRoleInsert;
            string msg = string.Empty;
            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"if not exists(select * from Roles  where name = @Name  and(CompanyId = @CompanyId or CompanyId = 0) and[Priority] = @Priority and IsDeleted = 0)
                                    begin

                                   if  exists(select * from Roles  where name = @Name and (CompanyId = @CompanyId or CompanyId = 0)  and IsDeleted = 0)
			                         begin

                                     select - 1

                                         end  else begin

                                         insert into Roles(Name, isActive,[Description], CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
                                          , CompanyId,[Priority], IsDeleted,[RoleVisibleToVendor])
			                              values(@Name, 1, @Description, @CreatedBy, GetDate(), @ModifiedBy, GETDATE(), @CompanyId, @Priority, 0, @RoleVisibleToVendor
                                          )

                                          select 1   end  end else begin 
                                           select - 2
                                           end";



                IsRoleInsert = con.Query<int>(query,
                    new
                    {
                        Name = obj.RoleModel.RoleName,
                        Description = obj.RoleModel.RoleDescription,
                        CreatedBy = obj.UserId,
                        ModifiedBy = obj.UserId,
                        CompanyId = obj.VendorCompanyId,
                        Priority = obj.RoleModel.RolePriority,
                        RoleVisibleToVendor = obj.RoleModel.RoleVisibleToVendor

                    }).FirstOrDefault();


            }
            if (IsRoleInsert == 1){
                msg = "RoleAddedSuccess";
            }
            else if (IsRoleInsert == -1){

                msg = "DuplicatePriority";
            }
            else if (IsRoleInsert == -2){

                msg = "DuplicateRole";
            }

            return msg;
        }



        public string updateRole(MODEL.RoleManageVendorAdminProfile.RoleAddUpdateModel obj)
        {
            string msg = string.Empty;
            int Id;
            string query = string.Empty;
            using (var con = new SqlConnection(ConnectionString))
            {

                query = @"update  Roles set  
			                           Name					=     @Name 
			                          ,[Description]		=     @Description
			                          ,ModifiedBy           =     @ModifiedBy
			                          ,ModifiedDate			=     getdate()
			                          ,CompanyId			=     @CompanyId
			                          ,[Priority]			=     @Priority
			                          
			                         where RoleId = @RoleId  select 1 ";
                Id = con.Query<int>(query, new
                {

                    Name = obj.RoleModel.RoleName,
                    Description = obj.RoleModel.RoleDescription,
                    ModifiedBy = obj.UserId,
                    ModifiedDate = DateTime.Now,
                    CompanyId = obj.VendorCompanyId,
                    Priority = obj.RoleModel.RolePriority,
                    RoleId = obj.RoleModel.RoleId
                }).SingleOrDefault();

            }

            if (Id == 1){
                msg = "RoleUpdatedSuccess";
            }
            return msg;

        }

        public string DeleteRole(int RoleId)
        {
            string msg = string.Empty;
            int Id;
            string query = string.Empty;
            using (var con = new SqlConnection(ConnectionString))
            {

                query = @"update  Roles set  
			                           IsDeleted           =     1
			                         where RoleId = @RoleId select 1";
                Id = con.Query<int>(query, new
                {
                    RoleId = RoleId
                }).SingleOrDefault();

            }

            if (Id == 1)
            {
                msg = "RoleDeletedSuccessfully";
            }
            return msg;

        }

    }   
}
