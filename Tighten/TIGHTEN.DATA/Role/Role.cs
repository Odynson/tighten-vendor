﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class Role : BaseClass
    {
        AppContext dbcontext;

        public List<MODEL.RoleModel> getRoles(int CompanyId, int RoleId)
        {


            List<MODEL.RoleModel> result;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select role.RoleId, role.Name,role.Description from Roles role where role.isActive = 1 and role.IsDeleted = 0 and (role.CompanyId = @CompanyId or role.CompanyId = 0) 
                                   and role.RoleId <> 4 and role.RoleId >= @RoleId ";
                result = con.Query<MODEL.RoleModel>(sqlquery, new { CompanyId = CompanyId, RoleId = RoleId }).ToList();
            }

            return result;
        }

        public MODEL.SideMenuOptionRolewiseModel showSideMenuOptionRolewise(int RoleId, string UserId, int CompanyId)
        {



            MODEL.SideMenuOptionRolewiseModel model = new SideMenuOptionRolewiseModel();
            int RoleIdForFreelancer = 0;
            UserDetail user;
            bool IsVendor = false;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from UserDetails usr where usr.UserId = @UserId and usr.IsDeleted = 0";
                user = con.Query<UserDetail>(sqlquery, new { UserId = UserId }).FirstOrDefault();

                // vendor code


                string sqlquery1 = @"Select IsVendor from Companies C inner join UserDetails Ud on C.Id = Ud.CompanyId
                                     where Ud.UserId = @UserId ";
                IsVendor = con.Query<bool>(sqlquery1, new { UserId = UserId }).FirstOrDefault();
                //string sqlquery2 = @"Select c.RoleId from usercompanyroles C inner join UserDetails Ud on C.UserId = Ud.UserId
                //                     where Ud.UserId = @UserId and c.CompanyId=@companyId";
                //RoleId = con.Query<int>(sqlquery2, new { UserId = UserId,companyId=CompanyId }).FirstOrDefault();
                // vendor code

            }
            if (user.IsFreelancer == true)
            {
                RoleIdForFreelancer = RoleId;
                RoleId = 4;
            }

            IEnumerable<MODEL.SideMenuOption> MenuWithSubmenus = null;

            using (var con = new SqlConnection(ConnectionString))
            {

                if (IsVendor)
                {

                    model = Vendor(RoleId, UserId, CompanyId);

                    return model;
                }
                else
                {
                    string sqlquery = @"select m.Name as MenuName,m.Id as MenuId from Permissions P inner join Menus m on P.MenuId = m.Id where P.UserRole = @RoleId 
                                        and P.CompanyId = @CompanyId and P.isActive = 1 and m.ParentId = 0    

                                  select v.Name as MenuName, v.Name as SubMenuName, v.URL as URL,v.ParentId as ParentId from Permissions p inner join Menus v on p.MenuId = v.Id 
                                  where p.isActive = 1 and p.UserRole = @RoleId and p.CompanyId = @CompanyId ";

                    //and m.Name <> 'UserProfile'   --- this was in where condition of 1st select statement  in sqlquery
                    var AllMenuOptions = con.QueryMultiple(sqlquery, new { RoleId = RoleId, CompanyId = CompanyId });

                    MenuWithSubmenus = AllMenuOptions.Read<MODEL.SideMenuOption>().ToList();
                    if (MenuWithSubmenus != null)
                    {
                        var SideMenuOption_Child = AllMenuOptions.Read<MODEL.SideMenuOptionRolewiseModel_Child>().ToList();

                        foreach (var options in MenuWithSubmenus)
                        {
                            options.SubMenuList = SideMenuOption_Child.Where(x => x.ParentId == options.MenuId).ToList();
                        }
                    }


                }


            }


            if (user.IsFreelancer)
            {
                model.Role = RoleIdForFreelancer;
            }
            else
            {
                model.Role = RoleId;
            }


            foreach (var item in MenuWithSubmenus)
            {
                if (item.MenuName == "Company")
                {
                    model.IsCompany = true;
                }
                else if (item.MenuName == "UserProfile")
                {
                    model.IsUserProfile = true;
                }
                else if (item.MenuName == "Team")
                {
                    model.IsTeam = true;
                    model.TeamOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "Team";
                    model.TeamOptions.Insert(0, menu);
                }
                else if (item.MenuName == "Projects")
                {
                    model.IsProject = true;
                    model.ProjectOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "Projects";
                    model.ProjectOptions.Insert(0, menu);
                }
                else if (item.MenuName == "Financial")
                {
                    model.IsFinancialTeam = true;
                    model.FinancialTeamOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "Financial";
                    model.FinancialTeamOptions.Insert(0, menu);
                }
                else if (item.MenuName == "TaskApprovalForUser")
                {
                    model.IsTaskApprovalUser = true;
                    model.TaskApprovalUserOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    //menu.MenuName = "Task Approval For User";
                    menu.MenuName = "Task Approval";
                    model.TaskApprovalUserOptions.Insert(0, menu);
                }
                else if (item.MenuName == "TaskApprovalForAdmin")
                {
                    model.IsTaskApprovalAdmin = true;
                    model.TaskApprovalAdminOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    //menu.MenuName = "Task Approval For Admin";
                    menu.MenuName = "Task Approval";
                    model.TaskApprovalAdminOptions.Insert(0, menu);
                }

                else if (item.MenuName == "Account")
                {
                    model.IsAccount = true;
                    model.AccountOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "Account";
                    model.AccountOptions.Insert(0, menu);
                }

                //else if (item.MenuName == "Role")
                //{
                //    model.IsRole = true;
                //    model.RoleOptions = item.SubMenuList;
                //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                //    menu.MenuName = "Role";
                //    model.RoleOptions.Insert(0, menu);
                //}
                else if (item.MenuName == "User Management")
                {
                    model.IsRole = true;
                    model.RoleOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "User Management";
                    model.RoleOptions.Insert(0, menu);
                }

                else if (item.MenuName == "Payment")
                {
                    model.IsPayment = true;
                    model.PaymentOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "Payment";
                    model.PaymentOptions.Insert(0, menu);
                }

                else if (item.MenuName == "Feedback")
                {
                    model.IsFeedback = true;
                    model.FeedbackOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "Feedback";
                    model.FeedbackOptions.Insert(0, menu);
                }

                // vendor link  in companany
                else if (item.MenuName == "vendors")
                {
                    model.IsVendor1 = true;
                    model.VendorOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "vendors";
                    model.VendorOptions.Insert(0, menu);
                }


                else if (item.MenuName == "Settings")
                {
                    model.Settings = true;
                    model.SettingOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "Settings";
                    model.SettingOptions.Insert(0, menu);
                }
                else if (item.MenuName == "Account Manager")
                {
                    model.Settings = true;
                    model.SettingOptions = item.SubMenuList;
                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                    menu.MenuName = "Account Manager";
                    model.SettingOptions.Insert(0, menu);
                }
                //else if (item.MenuName == "Account Manager")
                //{
                //    model.Settings = true;
                //    model.SettingVendorAdminProfileOptions = item.SubMenuList;
                //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                //    menu.MenuName = "Account Manager";
                //    model.SettingVendorAdminProfileOptions.Insert(0, menu);
                //}
                //else if (item.MenuName == "My Enterprise Companies")
                //{
                //    model.IsAccount = true;
                //    model.AccountOptions = item.SubMenuList;
                //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                //    menu.MenuName = "My Enterprise Companies";
                //    model.AccountOptions.Insert(0, menu);
                //}

                // link in venodor profile
            }

            return model;
        }

        /// <summary>
        /// This method and parameter for vendor Profile
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="UserId"></param>
        /// <param name="CompanyId"></param>
        /// <returns></returns>

        public MODEL.SideMenuOptionRolewiseModel Vendor(int RoleId, string UserId, int CompanyId)
        {

            MODEL.SideMenuOptionRolewiseModel model = new SideMenuOptionRolewiseModel();
            List<MODEL.SideMenuOption> MenuWithSubmenus = new List<MODEL.SideMenuOption>();
            double Rating = 0;
            string sqlquery = string.Empty;
            // if Role is greater than 3 than we consider it general role for Vendor Admin Profile
            if (RoleId == 0|| RoleId == 6||RoleId==32)
            {
                RoleId = 1;
            }
            RoleId = RoleId > 3 ? 3 : RoleId;
            using (var con = new SqlConnection(ConnectionString))
            {

               // if (RoleId == 1)
               // {
                    ////Query to fetch menu both for vendor and general company(P.CompanyId = 1) combined
                    //sqlquery = @"Declare @VendorCompanyId int = 0
                    //            select @VendorCompanyId = CompanyId from UserDetails ud  where ud.UserId = @UserId

                    //            select distinct(MenuId),MenuName,URL   ,UserRole from(select m.Name as MenuName,m.Id as MenuId,m.URL as URL
                    //            ,p.UserRole from Permissions P inner join Menus m on P.MenuId = m.Id where P.UserRole = @RoleId
                    //            and P.CompanyId = 1 and P.isActive = 1 and m.ParentId = 0
                    //            UNION ALL
                    //            select m.Name as MenuName,m.Id as MenuId ,m.URL as URL,p.UserRole from Permissions p inner join Menus
                    //            m on p.MenuId = m.Id where p.UserRole = @RoleId
                    //            and p.CompanyId = @VendorCompanyId and p.isActive = 1  )  MainMenu


                    //            select distinct(URL),MenuName,SubMenuName,ParentId from(select v.Name as MenuName, v.Name as SubMenuName, v.URL as URL,
                    //            v.ParentId as ParentId from Permissions p inner join Menus v on p.MenuId = v.Id
                    //            where p.isActive = 1 and p.UserRole = @RoleId and p.CompanyId = 1
                    //            UNION ALL
                    //            select M.Name as MenuName, M.Name as SubMenuName, M.URL as URL,M.ParentId as ParentId from
                    //            Permissions p inner join Menus M on p.MenuId = M.Id where p.isActive = 1 and p.UserRole = @RoleId
                    //            and p.CompanyId = @VendorCompanyId )  SubMenu

                    //            select ISNULL(AVG(PPRV.Rating),0)  Rating from ProjectProposedResourcesByVendor PPRV inner join UserDetails Ud on Ud.Email = PPRV.Email	
                    //            where Ud.UserId = @UserId";
                //}
               // else {
                    //Query to fetch menu for vendor company

                    sqlquery = @"Declare @VendorCompanyId int = 0
                                   	select @VendorCompanyId = CompanyId from UserDetails ud  where ud.UserId = @UserId
                                    select m.Name as MenuName,m.Id as MenuId ,m.URL as URL ,p.UserRole
        	                        from Permissions p inner join Menus m on p.MenuId = m.Id where p.UserRole = @RoleId
                                    and p.CompanyId = @VendorCompanyId and p.isActive = 1   

                                    select M.Name as MenuName, M.Name as SubMenuName, M.URL as URL,M.ParentId as ParentId from 
                                    Permissions p inner join Menus M on p.MenuId = M.Id where p.isActive = 1 and p.UserRole = @RoleId 
                                    and p.CompanyId = @VendorCompanyId 

                                    select ISNULL(AVG(PPRV.Rating),0)  Rating from ProjectProposedResourcesByVendor PPRV inner join UserDetails Ud on Ud.Email = PPRV.Email	
                                    where Ud.UserId = @UserId";
               // }

                //and m.Name <> 'UserProfile'   --- this was in where condition of 1st select statement  in sqlquery
                var AllMenuOptions = con.QueryMultiple(sqlquery, new { RoleId = RoleId, CompanyId = CompanyId, UserId = UserId });

                MenuWithSubmenus = AllMenuOptions.Read<MODEL.SideMenuOption>().ToList();
                if (MenuWithSubmenus.Count > 0)
                {
                    var SideMenuOption_Child = AllMenuOptions.Read<MODEL.SideMenuOptionRolewiseModel_Child>().ToList();
                    Rating = AllMenuOptions.Read<double>().FirstOrDefault();
                    foreach (var options in MenuWithSubmenus)
                    {
                        options.SubMenuList = SideMenuOption_Child.Where(x => x.ParentId == options.MenuId).ToList();
                    }
                }
                //////This is temporary code for showing all menus////////
                else
                {
                    sqlquery = @"select m.Name as MenuName,m.Id as MenuId,m.URL,p.UserRole from Permissions P inner join Menus m on P.MenuId = m.Id where P.UserRole = @RoleId 
                                        and P.CompanyId = @CompanyId and P.isActive = 1 and m.ParentId = 0    

                                  select v.Name as MenuName, v.Name as SubMenuName, v.URL as URL,v.ParentId as ParentId from Permissions p inner join Menus v on p.MenuId = v.Id 
                                  where p.isActive = 1 and p.UserRole = @RoleId and p.CompanyId = @CompanyId 

                                  select ISNULL(AVG(PPRV.Rating),0)  Rating from ProjectProposedResourcesByVendor PPRV inner join UserDetails Ud on Ud.Email = PPRV.Email	
                                  where Ud.UserId = @UserId";

                    //and m.Name <> 'UserProfile'   --- this was in where condition of 1st select statement  in sqlquery
                    AllMenuOptions = con.QueryMultiple(sqlquery, new { RoleId = 1, CompanyId = 0, UserId = UserId });

                    MenuWithSubmenus = AllMenuOptions.Read<MODEL.SideMenuOption>().ToList();
                    if (MenuWithSubmenus != null)
                    {
                        var SideMenuOption_Child = AllMenuOptions.Read<MODEL.SideMenuOptionRolewiseModel_Child>().ToList();

                        foreach (var options in MenuWithSubmenus)
                        {
                            options.SubMenuList = SideMenuOption_Child.Where(x => x.ParentId == options.MenuId).ToList();
                        }
                    }
                }
                if ( MenuWithSubmenus[0].UserRole == 3)
                {

                    model.IsCompany = true;  //  use for default profile > 2
                    model.IsUserProfile = true;
                    foreach (var item in MenuWithSubmenus)
                    {
                        if (item.MenuName == "UserProfile")
                        {
                            model.IsUserProfile = true;
                            model.IsVendorGeneralUser = true;
                            model.RatingVendorGeneralUser = Rating;
                        }

                        else if (item.MenuName == "My Projects")
                        {
                            model.IsProjectToQuote = true;
                            model.ProjectstoQuoteVendorOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = item.MenuName;
                            model.ProjectstoQuoteVendorOptions.Insert(0, menu);
                        }

                    }
                }
                //else if (MenuWithSubmenus[0].UserRole == 32)
                //{

                //    model.IsCompany = true;  //  use for default profile > 2
                //    model.IsUserProfile = true;
                //    foreach (var item in MenuWithSubmenus)
                //    {
                //        //if (item.MenuName == "UserProfile")
                //        //{
                //        //    model.IsUserProfile = true;
                //        //    model.IsVendorGeneralUser = true;
                //        //    model.RatingVendorGeneralUser = Rating;
                //        //}

                //       // else
                //        if (item.MenuName == "Projects to Quote")
                //        {
                //            model.IsProjectToQuote = true;
                //            model.ProjectstoQuoteVendorOptions = item.SubMenuList;
                //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                //            menu.MenuName = item.MenuName;
                //            model.ProjectstoQuoteVendorOptions.Insert(0, menu);
                //        }
                //        else if (item.MenuName == "Projects Awarded")
                //        {
                //            model.IsProjectAwarded = true;
                //            model.ProjectAwardedVendorOptions = item.SubMenuList;
                //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                //            menu.MenuName = item.MenuName;
                //            model.ProjectAwardedVendorOptions.Insert(0, menu);

                //        }
                //        else if (item.MenuName == "Invoices")
                //        {
                //            model.IsVendorInvoice = true;
                //            model.InvoiceVendorOptions = item.SubMenuList;
                //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                //            menu.MenuName = item.MenuName;
                //            model.InvoiceVendorOptions.Insert(0, menu);

                //        }
                //        else if (item.MenuName == "Account Manager")
                //        {
                //            model.Settings = true;
                //            model.SettingVendorAdminProfileOptions = item.SubMenuList;
                //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                //            menu.MenuName = "Account Manager";
                //            model.SettingVendorAdminProfileOptions.Insert(0, menu);
                //        }
                //    }
                //}
                else
                {
                    model.IsCompany = true;  //  use for default profile > 2
                    model.IsUserProfile = true;
                    foreach (var item in MenuWithSubmenus)
                    {

                        #region VendorCompanyMenu
                        if (item.MenuName == "Company")
                        {
                            model.IsCompany = RoleId == 1 ? true : false;
                        }
                        else if (item.MenuName == "Company Name")
                        {
                            model.IsCompanyName = false;
                            model.CompanyNameOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = "Company Name";
                            model.CompanyNameOptions.Insert(0, menu);
                        }
                        else if (item.MenuName == "My Invitations")
                        {
                            model.IsMyInvitaiton = true;
                            model.MyInvitationsVendorOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = "My Invitations";
                            model.MyInvitationsVendorOptions.Insert(0, menu);
                        }
                        else if (item.MenuName == "Projects to Quote")
                        {
                            model.IsProjectToQuote = true;
                            model.ProjectstoQuoteVendorOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = item.MenuName;
                            model.ProjectstoQuoteVendorOptions.Insert(0, menu);
                        }
                        else if (item.MenuName == "Projects Awarded")
                        {
                            model.IsProjectAwarded = true;
                            model.ProjectAwardedVendorOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = item.MenuName;
                            model.ProjectAwardedVendorOptions.Insert(0, menu);

                        }
                        else if (item.MenuName == "Invoices")
                        {
                            model.IsVendorInvoice = true;
                            model.InvoiceVendorOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = item.MenuName;
                            model.InvoiceVendorOptions.Insert(0, menu);

                        }
                        else if (item.MenuName == "Account Settings")
                        {
                            model.IsVendorInvoiceSetting = true;
                            model.VendorAccountSettingOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = item.MenuName;
                            model.VendorAccountSettingOptions.Insert(0, menu);

                        }
                        else if (item.MenuName == "Invitations")
                        {
                            model.IsMyInvitaiton = true;
                            model.MyInvitationsVendorOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = "Invitations";
                            model.MyInvitationsVendorOptions.Insert(0, menu);
                        }
                        else if (item.MenuName == "Settings")
                        {
                            model.Settings = true;
                            model.SettingVendorAdminProfileOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = "Settings";
                            model.SettingVendorAdminProfileOptions.Insert(0, menu);
                        }
                        else if (item.MenuName == "Account Manager")
                        {
                            model.Settings = true;
                            model.SettingVendorAdminProfileOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = "Account Manager";
                            model.SettingVendorAdminProfileOptions.Insert(0, menu);
                        }

                        else if (item.MenuName == "Account Manager")
                        {
                            model.IsRole = true;
                            model.RoleOptions = item.SubMenuList;
                            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                            menu.MenuName = "Account Manager";
                            model.RoleOptions.Insert(0, menu);
                        }
                        //else if (item.MenuName == "My Enterprise Companies")
                        //{
                        //    model.IsAccount = true;
                        //    model.AccountOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "My Enterprise Companies";
                        //    model.AccountOptions.Insert(0, menu);
                        //}
                        #endregion


                        //#region GeneralCompanyMenu
                        //if (item.MenuName == "Company")
                        //{
                        //    model.IsCompany = true;
                        //}
                        //else if (item.MenuName == "UserProfile")
                        //{
                        //    model.IsUserProfile = true;
                        //}
                        //else if (item.MenuName == "Team")
                        //{
                        //    model.IsTeam = true;
                        //    model.TeamOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "Team";
                        //    model.TeamOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "Projects")
                        //{
                        //    model.IsProject = true;
                        //    model.ProjectOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "Projects";
                        //    model.ProjectOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "Financial")
                        //{
                        //    model.IsFinancialTeam = true;
                        //    model.FinancialTeamOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "Financial";
                        //    model.FinancialTeamOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "TaskApprovalForUser")
                        //{
                        //    model.IsTaskApprovalUser = true;
                        //    model.TaskApprovalUserOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    //menu.MenuName = "Task Approval For User";
                        //    menu.MenuName = "Task Approval";
                        //    model.TaskApprovalUserOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "TaskApprovalForAdmin")
                        //{
                        //    model.IsTaskApprovalAdmin = true;
                        //    model.TaskApprovalAdminOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    //menu.MenuName = "Task Approval For Admin";
                        //    menu.MenuName = "Task Approval";
                        //    model.TaskApprovalAdminOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "Account")
                        //{
                        //    model.IsAccount = true;
                        //    model.AccountOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "Account";
                        //    model.AccountOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "Role")
                        //{
                        //    model.IsRole = true;
                        //    model.RoleOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "Role";
                        //    model.RoleOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "Payment")
                        //{
                        //    model.IsPayment = true;
                        //    model.PaymentOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "Payment";
                        //    model.PaymentOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "Feedback")
                        //{
                        //    model.IsFeedback = true;
                        //    model.FeedbackOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "Feedback";
                        //    model.FeedbackOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "vendors")
                        //{
                        //    model.IsVendor1 = true;
                        //    model.VendorOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "vendors";
                        //    model.VendorOptions.Insert(0, menu);
                        //}
                        //else if (item.MenuName == "Settings")
                        //{
                        //    model.Settings = true;
                        //    model.SettingOptions = item.SubMenuList;
                        //    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
                        //    menu.MenuName = "Settings";
                        //    model.SettingOptions.Insert(0, menu);
                        //}
                        //#endregion


                    }

                }

                return model;


            }
        }

        // show side menu for general company

        //public MODEL.SideMenuOptionRolewiseModel showSideMenuOptionRolewise(int RoleId, string UserId, int CompanyId)
        //{


        //    MODEL.SideMenuOptionRolewiseModel model = new SideMenuOptionRolewiseModel();
        //    int RoleIdForFreelancer = 0;
        //    UserDetail user;
        //    bool IsVendor = false;
        //    using (var con = new SqlConnection(ConnectionString))
        //    {
        //        string sqlquery = @"select * from UserDetails usr where usr.UserId = @UserId and usr.IsDeleted = 0";
        //        user = con.Query<UserDetail>(sqlquery, new { UserId = UserId }).FirstOrDefault();

        //        // vendor code


        //        string sqlquery1 = @"Select IsVendor from Companies C inner join UserDetails Ud on C.Id = Ud.CompanyId
        //                             where Ud.UserId = @UserId ";
        //        IsVendor = con.Query<bool>(sqlquery1, new { UserId = UserId }).FirstOrDefault();
        //        // vendor code

        //    }
        //    if (user.IsFreelancer == true)
        //    {
        //        RoleIdForFreelancer = RoleId;
        //        RoleId = 4;
        //    }

        //    IEnumerable<MODEL.SideMenuOption> MenuWithSubmenus = null;

        //    using (var con = new SqlConnection(ConnectionString))
        //    {

        //        //if (IsVendor)
        //        //{

        //        //    model = Vendor(RoleId, UserId, CompanyId);

        //        //    return model;
        //        //}
        //        //else
        //        //{
        //        string sqlquery = @"select m.Name as MenuName,m.Id as MenuId from Permissions P inner join Menus m on P.MenuId = m.Id where P.UserRole = 1 
        //                                and P.CompanyId = 1 and P.isActive = 1 and m.ParentId = 0    

        //                          select v.Name as MenuName, v.Name as SubMenuName, v.URL as URL,v.ParentId as ParentId from Permissions p inner join Menus v on p.MenuId = v.Id 
        //                          where p.isActive = 1 and p.UserRole = 1 and p.CompanyId = 1 ";

        //        //and m.Name <> 'UserProfile'   --- this was in where condition of 1st select statement  in sqlquery
        //        var AllMenuOptions = con.QueryMultiple(sqlquery, new { RoleId = 1, CompanyId = 1 });

        //        MenuWithSubmenus = AllMenuOptions.Read<MODEL.SideMenuOption>().ToList();
        //        if (MenuWithSubmenus != null)
        //        {
        //            var SideMenuOption_Child = AllMenuOptions.Read<MODEL.SideMenuOptionRolewiseModel_Child>().ToList();

        //            foreach (var options in MenuWithSubmenus)
        //            {
        //                options.SubMenuList = SideMenuOption_Child.Where(x => x.ParentId == options.MenuId).ToList();
        //            }
        //        }


        //        // }


        //    }


        //    if (user.IsFreelancer)
        //    {
        //        model.Role = RoleIdForFreelancer;
        //    }
        //    else
        //    {
        //        model.Role = RoleId;
        //    }


        //    foreach (var item in MenuWithSubmenus)
        //    {
        //        if (item.MenuName == "Company")
        //        {
        //            model.IsCompany = true;
        //        }
        //        else if (item.MenuName == "UserProfile")
        //        {
        //            model.IsUserProfile = true;
        //        }
        //        else if (item.MenuName == "Team")
        //        {
        //            model.IsTeam = true;
        //            model.TeamOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            menu.MenuName = "Team";
        //            model.TeamOptions.Insert(0, menu);
        //        }
        //        else if (item.MenuName == "Projects")
        //        {
        //            model.IsProject = true;
        //            model.ProjectOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            menu.MenuName = "Projects";
        //            model.ProjectOptions.Insert(0, menu);
        //        }
        //        else if (item.MenuName == "Financial")
        //        {
        //            model.IsFinancialTeam = true;
        //            model.FinancialTeamOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            menu.MenuName = "Financial";
        //            model.FinancialTeamOptions.Insert(0, menu);
        //        }
        //        else if (item.MenuName == "TaskApprovalForUser")
        //        {
        //            model.IsTaskApprovalUser = true;
        //            model.TaskApprovalUserOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            //menu.MenuName = "Task Approval For User";
        //            menu.MenuName = "Task Approval";
        //            model.TaskApprovalUserOptions.Insert(0, menu);
        //        }
        //        else if (item.MenuName == "TaskApprovalForAdmin")
        //        {
        //            model.IsTaskApprovalAdmin = true;
        //            model.TaskApprovalAdminOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            //menu.MenuName = "Task Approval For Admin";
        //            menu.MenuName = "Task Approval";
        //            model.TaskApprovalAdminOptions.Insert(0, menu);
        //        }

        //        else if (item.MenuName == "Account")
        //        {
        //            model.IsAccount = true;
        //            model.AccountOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            menu.MenuName = "Account";
        //            model.AccountOptions.Insert(0, menu);
        //        }

        //        else if (item.MenuName == "Role")
        //        {
        //            model.IsRole = true;
        //            model.RoleOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            menu.MenuName = "Role";
        //            model.RoleOptions.Insert(0, menu);
        //        }

        //        else if (item.MenuName == "Payment")
        //        {
        //            model.IsPayment = true;
        //            model.PaymentOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            menu.MenuName = "Payment";
        //            model.PaymentOptions.Insert(0, menu);
        //        }

        //        else if (item.MenuName == "Feedback")
        //        {
        //            model.IsFeedback = true;
        //            model.FeedbackOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            menu.MenuName = "Feedback";
        //            model.FeedbackOptions.Insert(0, menu);
        //        }

        //        // vendor link  in companany
        //        else if (item.MenuName == "vendors")
        //        {
        //            model.IsVendor1 = true;
        //            model.VendorOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            menu.MenuName = "vendors";
        //            model.VendorOptions.Insert(0, menu);
        //        }


        //        else if (item.MenuName == "Settings")
        //        {
        //            model.Settings = true;
        //            model.SettingOptions = item.SubMenuList;
        //            SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //            menu.MenuName = "Settings";
        //            model.SettingOptions.Insert(0, menu);
        //        }



        //        // link in venodor profile
        //    }

        //    return model;
        //}

        ///// <summary>
        ///// This method and parameter for vendor Profile
        ///// </summary>
        ///// <param name="RoleId"></param>
        ///// <param name="UserId"></param>
        ///// <param name="CompanyId"></param>
        ///// <returns></returns>
        //// show side menu for vendor company
        //public MODEL.SideMenuOptionRolewiseModel showSideMenuForVendorCompany(int RoleId, string UserId, int CompanyId)
        //{

        //    MODEL.SideMenuOptionRolewiseModel model = new SideMenuOptionRolewiseModel();
        //    List<MODEL.SideMenuOption> MenuWithSubmenus = new List<MODEL.SideMenuOption>();
        //    double Rating = 0;

        //    // if Role is greater than 3 than we consider it general role for Vendor Admin Profile
        //    RoleId = RoleId > 3 ? 3 : RoleId;
        //    using (var con = new SqlConnection(ConnectionString))
        //    {
        //        string sqlquery = @"Declare @VendorCompanyId int = 0
        //                           	 select @VendorCompanyId = CompanyId from UserDetails ud  where ud.UserId = @UserId
        //                            select distinct m.Id as MenuId,m.Name as MenuName ,m.URL as URL  
        //	,p.UserRole
        //	from Permissions p inner join Menus
        //                            m on p.MenuId = m.Id where p.UserRole = @RoleId
        //                            and p.CompanyId >1 and p.isActive = 1   

        //                        	select  distinct m.Id as MenuId,M.Name as MenuName, M.Name as SubMenuName, M.URL as URL,M.ParentId as ParentId from 
        //                           Permissions p inner join Menus M on p.MenuId = M.Id where p.isActive = 1 and p.UserRole = 1 
        //                           and p.CompanyId >1 

        //                        select ISNULL(AVG(PPRV.Rating),0)  Rating from ProjectProposedResourcesByVendor PPRV inner join UserDetails Ud on Ud.Email = PPRV.Email	
        //                        where Ud.UserId = @UserId";

        //        //and m.Name <> 'UserProfile'   --- this was in where condition of 1st select statement  in sqlquery
        //        var AllMenuOptions = con.QueryMultiple(sqlquery, new { RoleId = RoleId, CompanyId = CompanyId, UserId = UserId });

        //        MenuWithSubmenus = AllMenuOptions.Read<MODEL.SideMenuOption>().ToList();
        //        if (MenuWithSubmenus != null)
        //        {
        //            var SideMenuOption_Child = AllMenuOptions.Read<MODEL.SideMenuOptionRolewiseModel_Child>().ToList();
        //            Rating = AllMenuOptions.Read<double>().FirstOrDefault();
        //            foreach (var options in MenuWithSubmenus)
        //            {
        //                options.SubMenuList = SideMenuOption_Child.Where(x => x.ParentId == options.MenuId).ToList();
        //            }
        //        }

        //        if (MenuWithSubmenus[0].UserRole == 3)
        //        {

        //            model.IsCompany = true;  //  use for default profile > 2
        //            model.IsUserProfile = true;
        //            foreach (var item in MenuWithSubmenus)
        //            {
        //                if (item.MenuName == "UserProfile")
        //                {
        //                    model.IsUserProfile = true;
        //                    model.IsVendorGeneralUser = true;
        //                    model.RatingVendorGeneralUser = Rating;
        //                }

        //                else if (item.MenuName == "My Projects")
        //                {
        //                    model.IsProjectToQuote = true;
        //                    model.ProjectstoQuoteVendorOptions = item.SubMenuList;
        //                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //                    menu.MenuName = item.MenuName;
        //                    model.ProjectstoQuoteVendorOptions.Insert(0, menu);
        //                }

        //            }
        //        }
        //        else
        //        {
        //            model.IsCompany = true;  //  use for default profile > 2
        //            model.IsUserProfile = true;
        //            foreach (var item in MenuWithSubmenus)
        //            {


        //                if (item.MenuName == "Company")
        //                {
        //                    model.IsCompany = RoleId == 1 ? true : false;
        //                }


        //                else if (item.MenuName == "Company Name")
        //                {
        //                    model.IsCompanyName = false;
        //                    model.CompanyNameOptions = item.SubMenuList;
        //                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //                    menu.MenuName = "Company Name";
        //                    model.CompanyNameOptions.Insert(0, menu);
        //                }


        //                else if (item.MenuName == "My Invitations")
        //                {
        //                    model.IsMyInvitaiton = true;
        //                    model.MyInvitationsVendorOptions = item.SubMenuList;
        //                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //                    menu.MenuName = "My Invitations";
        //                    model.MyInvitationsVendorOptions.Insert(0, menu);
        //                }


        //                else if (item.MenuName == "Projects to Quote")
        //                {
        //                    model.IsProjectToQuote = true;
        //                    model.ProjectstoQuoteVendorOptions = item.SubMenuList;
        //                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //                    menu.MenuName = item.MenuName;
        //                    model.ProjectstoQuoteVendorOptions.Insert(0, menu);
        //                }


        //                else if (item.MenuName == "Projects Awarded")
        //                {
        //                    model.IsProjectAwarded = true;
        //                    model.ProjectAwardedVendorOptions = item.SubMenuList;
        //                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //                    menu.MenuName = item.MenuName;
        //                    model.ProjectAwardedVendorOptions.Insert(0, menu);

        //                }

        //                else if (item.MenuName == "Invoices")
        //                {
        //                    model.IsVendorInvoice = true;
        //                    model.InvoiceVendorOptions = item.SubMenuList;
        //                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //                    menu.MenuName = item.MenuName;
        //                    model.InvoiceVendorOptions.Insert(0, menu);

        //                }
        //                else if (item.MenuName == "Account Settings")
        //                {
        //                    model.IsVendorInvoiceSetting = true;
        //                    model.VendorAccountSettingOptions = item.SubMenuList;
        //                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //                    menu.MenuName = item.MenuName;
        //                    model.VendorAccountSettingOptions.Insert(0, menu);

        //                }


        //                else if (item.MenuName == "Invitations")
        //                {
        //                    model.IsMyInvitaiton = true;
        //                    model.MyInvitationsVendorOptions = item.SubMenuList;
        //                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //                    menu.MenuName = "Invitations";
        //                    model.MyInvitationsVendorOptions.Insert(0, menu);
        //                }

        //                else if (item.MenuName == "Settings")
        //                {
        //                    model.Settings = true;
        //                    model.SettingVendorAdminProfileOptions = item.SubMenuList;
        //                    SideMenuOptionRolewiseModel_Child menu = new SideMenuOptionRolewiseModel_Child();
        //                    menu.MenuName = "Settings";
        //                    model.SettingVendorAdminProfileOptions.Insert(0, menu);
        //                }



        //            }

        //        }

        //        return model;


        //    }
        //}

    }
}


