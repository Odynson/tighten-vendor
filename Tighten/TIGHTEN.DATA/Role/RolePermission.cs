﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;

namespace TIGHTEN.DATA
{
    public class RolePermission : BaseClass
    {
        AppContext dbcontext;

        public MODEL.RolePermissionModel.InternalPermissionsForCurrentUserModel GetInternalPermissionsForCurrentUser(int RoleId, int CompanyId)
        {

            List<MODEL.RolePermissionModel.PermissionForSelectedRole> AllPermissions;

            using (var con = new SqlConnection(ConnectionString))
            {

                AllPermissions = con.Query<MODEL.RolePermissionModel.PermissionForSelectedRole>("Usp_GetInternalPermissionsForCurrentUser",
                 new { RoleId = RoleId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure).ToList();

            }

            //AllPermissions = (from perm in dbcontext.InternalPermissions
            //                  where perm.CompanyId == 0
            //                  && perm.RoleId == 0
            //                  && perm.isActive == true
            //                  && perm.isDeleted == false
            //                  select new MODEL.RolePermissionModel.PermissionForSelectedRole
            //                  {
            //                      MenuId = perm.MenuId,
            //                      MenuName = perm.PermissionName,
            //                      IsActive = (from intPer in dbcontext.InternalPermissions
            //                                  where intPer.CompanyId == CompanyId
            //                                  && intPer.RoleId == RoleId
            //                                  //&& intPer.MenuId == perm.MenuId
            //                                  && intPer.PermissionName == perm.PermissionName
            //                                  && intPer.isActive == true
            //                                  && intPer.isDeleted == false
            //                                  select intPer.isActive).FirstOrDefault()
            //                  }
            //                       ).ToList();

            MODEL.RolePermissionModel.InternalPermissionsForCurrentUserModel InternalPermissions = new MODEL.RolePermissionModel.InternalPermissionsForCurrentUserModel();

            foreach (var item in AllPermissions)
            {
                if (item.MenuName == "Edit Company Profile")
                {
                    if (item.IsActive == true)
                    {
                        InternalPermissions.CanEditCompanyProfile = true;
                    }
                    else
                    {
                        InternalPermissions.CanEditCompanyProfile = false;
                    }
                }
                else if (item.MenuName == "Add New Team")
                {
                    if (item.IsActive == true)
                    {
                        InternalPermissions.CanAddNewTeam = true;
                    }
                    //else
                    //{
                    //    InternalPermissions.CanAddNewTeam = false;
                    //}
                }
                else if (item.MenuName == "Update Team")
                {
                    if (item.IsActive == true)
                    {
                        InternalPermissions.CanUpdateTeam = true;
                    }
                }
                else if (item.MenuName == "Add New Project")
                {
                    if (item.IsActive == true)
                    {
                        InternalPermissions.CanAddNewProject = true;
                    }
                    else
                    {
                        InternalPermissions.CanAddNewProject = false;
                    }
                }
                else if (item.MenuName == "Update Project")
                {
                    if (item.IsActive == true)
                    {
                        InternalPermissions.CanUpdateProject = true;
                    }
                }
                else if (item.MenuName == "Approve Invoice")
                {
                    if (item.IsActive == true)
                    {
                        InternalPermissions.CanApproveInvoice = true;
                    }
                }

            }


            return InternalPermissions;
        }



        public List<MODEL.RolePermissionModel.AllRolesForCompanyModel> GetAllRolesForCompany(int CompanyId)
        {

            List<MODEL.RolePermissionModel.AllRolesForCompanyModel> AllRoles;
            using (var con = new SqlConnection(ConnectionString))
            {
                var AllRoles1 = con.QueryMultiple("Usp_GetAllRolesForCompany", new
                { CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);

             AllRoles = AllRoles1.Read<MODEL.RolePermissionModel.AllRolesForCompanyModel>().ToList();

            }


            //List<MODEL.RolePermissionModel.AllRolesForCompanyModel> AllRoles = (from rol in dbcontext.Roles
            //                                                                    where rol.isActive == true && rol.IsDeleted == false
            //                                                                    && rol.CompanyId == CompanyId
            //                                                                    select new MODEL.RolePermissionModel.AllRolesForCompanyModel
            //                                                                    {
            //                                                                        RoleId = rol.RoleId,
            //                                                                        RoleName = rol.Name
            //                                                                    }
            //                                                       ).ToList();





            //List<MODEL.RolePermissionModel.AllRolesForCompanyModel> AllRoles = (from rol in dbcontext.Roles
            //                                                                    where rol.isActive == true && rol.IsDeleted == false
            //                                                                    && rol.CompanyId == CompanyId
            //                                                                    select new MODEL.RolePermissionModel.AllRolesForCompanyModel
            //                                                                    {
            //                                                                        RoleId = rol.RoleId,
            //                                                                        RoleName = rol.Name
            //                                                                    }
            //                                                                   ).ToList();



            return AllRoles;
        }


        public List<MODEL.RolePermissionModel.PermissionForSelectedRole> GetPermissionForSelectedRole(int RoleId, int CompanyId)
        {



            List<MODEL.RolePermissionModel.PermissionForSelectedRole> AllPermissions;
            List<MODEL.RolePermissionModel.AllInternalPermissionsModel> InternalPermission;
            List<MODEL.RolePermissionModel.InternalPermissionsMOdalSpecific> intePermSpecific;

            using (var con = new SqlConnection(ConnectionString))
            {

                var AllPermissions1 = con.QueryMultiple("Usp_GetPermissionForSelectedRole",
                new { RoleId = RoleId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                InternalPermission = AllPermissions1.Read<RolePermissionModel.AllInternalPermissionsModel>().ToList();
                AllPermissions = AllPermissions1.Read<RolePermissionModel.PermissionForSelectedRole>().ToList();
                intePermSpecific = AllPermissions1.Read<RolePermissionModel.InternalPermissionsMOdalSpecific>().ToList();

                int i = 0;
                foreach (var option in AllPermissions)
                {

                    AllPermissions[i].InternalPermissions = InternalPermission.Where(x => x.CompanyId == CompanyId && x.RoleId == RoleId && x.MenuId == option.MenuId).ToList();
                    i++;

                }


            }

            //var Permission = (from menu in dbcontext.Menus
            //                  where menu.IsActive == true
            //                  && menu.Name != "UserProfile"
            //                  orderby menu.Id
            //                  select new
            //                  {
            //                      CompanyId = CompanyId,
            //                      RoleId = RoleId,
            //                      MenuId = menu.Id,
            //                      MenuName = menu.Name,
            //                      MenuDescription = menu.Description,
            //                      Url = menu.URL,
            //                      ParentId = menu.ParentId,
            //                      IsActive = (from per in dbcontext.Permissions
            //                                  where per.CompanyId == CompanyId
            //                                  && per.UserRole == RoleId
            //                                  && per.MenuId == menu.Id
            //                                  select per.isActive).FirstOrDefault(),
            //                      IsInternalPermissionPresent = false,
            //                      InternalPermissions = (from perm in dbcontext.InternalPermissions
            //                                             where perm.isActive == true && perm.isDeleted == false
            //                                             && perm.RoleId == 0
            //                                             && perm.MenuId == menu.Id
            //                                             && perm.CompanyId == 0
            //                                             select new MODEL.RolePermissionModel.AllInternalPermissionsModel
            //                                             {
            //                                                 PermissionId = (from intPer in dbcontext.InternalPermissions
            //                                                                 where intPer.CompanyId == CompanyId
            //                                                                 && intPer.RoleId == RoleId
            //                                                                 //&& intPer.MenuId == menu.Id
            //                                                                 && intPer.PermissionName == perm.PermissionName
            //                                                                 && intPer.isActive == true
            //                                                                 && intPer.isDeleted == false
            //                                                                 select intPer.Id).FirstOrDefault(),
            //                                                 PermissionName = perm.PermissionName,
            //                                                 CompanyId = CompanyId,
            //                                                 RoleId = RoleId,
            //                                                 MenuId = menu.Id,
            //                                                 IsAdminLevelPermission = perm.isAdminLevelPermission,
            //                                                 IsPermissionActive = (from intPer in dbcontext.InternalPermissions
            //                                                                       where intPer.CompanyId == CompanyId
            //                                                                       && intPer.RoleId == RoleId
            //                                                                       //&& intPer.MenuId == menu.Id
            //                                                                       && intPer.PermissionName == perm.PermissionName
            //                                                                       && intPer.isActive == true
            //                                                                       && intPer.isDeleted == false
            //                                                                       select intPer.isActive).FirstOrDefault()
            //                                             })

            //                  }
            //                    );



            //List<MODEL.RolePermissionModel.PermissionForSelectedRole> AllPermission = Permission.AsEnumerable().Select(x =>
            //new MODEL.RolePermissionModel.PermissionForSelectedRole
            //{
            //    CompanyId = x.CompanyId,
            //    RoleId = x.RoleId,
            //    MenuId = x.MenuId,
            //    MenuName = x.MenuName,
            //    MenuDescription = x.MenuDescription,
            //    Url = x.Url,
            //    ParentId = x.ParentId,
            //    IsActive = x.IsActive,
            //    IsInternalPermissionPresent = x.IsInternalPermissionPresent,
            //    InternalPermissions = x.InternalPermissions.ToList()

            //}).ToList();






            //foreach (var item in AllPermissions)
            //{

            //    int intperm = ((from intPer in dbcontext.InternalPermissions
            //                    join men in dbcontext.Menus on intPer.MenuId equals men.Id
            //                    where intPer.CompanyId == 0
            //                    //&& intPer.RoleId == RoleId
            //                    && men.ParentId == item.MenuId
            //                    && intPer.isActive == true
            //                    && intPer.isDeleted == false
            //                    select intPer.isActive)
            //                   .Union
            //                   (from intPer in dbcontext.InternalPermissions
            //                    join men in dbcontext.Menus on intPer.MenuId equals men.Id
            //                    where intPer.CompanyId == 0
            //                    //&& intPer.RoleId == RoleId
            //                    && intPer.MenuId == item.MenuId
            //                    && intPer.isActive == true
            //                    && intPer.isDeleted == false
            //                    select intPer.isActive)
            //                    ).ToList().Count();




            //        if (intperm > 0)
            //    {
            //        item.IsInternalPermissionPresent = true;
            //    }
            //    else
            //    {
            //        item.IsInternalPermissionPresent = false;
            //    }

            //}



            int j = 0;

            foreach (var item in AllPermissions)
            {



                if (intePermSpecific.Count <= j)
                {
                    j = 0;
                    continue;

                }

                if (item.MenuId == intePermSpecific[j].MenuId)
                {
                    item.IsInternalPermissionPresent = true;
                }
                else
                {
                    item.IsInternalPermissionPresent = false;
                }
                j++;
            }


            return AllPermissions;
        }

        public string savePermissionsForSelectedRole(MODEL.RolePermissionModel.SavePermissionForSelectedRole model)
        {

            #region permission   inserting ----------------------- updating and deleting 

            foreach (var item in model.PermissionsList)
            {
                if (item.IsActive != null)
                {
                    if (item.IsActive.Value == true)
                    {
                        int IdSuccess;

                        using (var con = new SqlConnection(ConnectionString))
                        {
                            IdSuccess = con.Query<int>("Usp_InsertUPdatePermission", new
                            {
                                UserRole = item.RoleId,
                                Link = item.Url,
                                MenuId = item.MenuId,
                                CompanyId = item.CompanyId,
                                isActive = item.IsActive,


                            }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        }


                    }
                    else
                    {

                        //  deleting the permission if item.IsActive.value == null  
                        int IdSuccess;
                        using (var con = new SqlConnection(ConnectionString))
                        {
                            IdSuccess = con.Query<int>("Usp_RemovePermission", new
                            {
                                UserRole = item.RoleId,
                                Link = item.Url,
                                MenuId = item.MenuId,
                                CompanyId = item.CompanyId,
                                isActive = item.IsActive,
                                response = 0

                            }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        }

                    }
                }
                else
                {
                    //  deleting the permission if item.IsActive == null  
                    int IdSuccess;
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        IdSuccess = con.Query<int>("Usp_RemovePermission", new
                        {
                            UserRole = item.RoleId,
                            Link = item.Url,
                            MenuId = item.MenuId,
                            CompanyId = item.CompanyId,
                            isActive = item.IsActive,
                            response = 0

                        }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    }

                }

                #endregion

                #region  InternalPermission -----------------------  inserting  updating and deleting 

                // Saving Internal Permissions


                foreach (var Internalitem in item.InternalPermissions)
                {
                    int SuccessIntPermissions;
                    if (Internalitem.IsPermissionActive != null)
                    {
                        if (Internalitem.IsPermissionActive.Value == true)
                        {
                            using (var con = new SqlConnection(ConnectionString))
                            {
                                SuccessIntPermissions = con.Query<int>("Usp_InsertUpdateInternalPermission", new
                                {
                                    PermissionName = Internalitem.PermissionName,
                                    MenuId = Internalitem.MenuId,
                                    RoleId = Internalitem.RoleId,
                                    CompanyId = Internalitem.CompanyId,
                                    isActive = Internalitem.IsPermissionActive,
                                }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                            }
                        }
                        else
                        {
                            using (var con = new SqlConnection(ConnectionString))
                            {
                                var SuccessIntPermissions1 = con.Query<int>("Usp_RemoveInternalPermission", new
                                {
                                    PermissionName = Internalitem.PermissionName,
                                    MenuId = Internalitem.MenuId,
                                    RoleId = Internalitem.RoleId,
                                    CompanyId = Internalitem.CompanyId,
                                    isActive = Internalitem.IsPermissionActive
                                }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                            }
                        }
                    }
                    else
                    {
                        using (var con = new SqlConnection(ConnectionString))
                        {
                            SuccessIntPermissions = con.Query<int>("Usp_RemoveInternalPermission", new
                            {
                                PermissionName = Internalitem.PermissionName,
                                MenuId = Internalitem.MenuId,
                                RoleId = Internalitem.RoleId,
                                CompanyId = Internalitem.CompanyId,
                                isActive = Internalitem.IsPermissionActive,
                            }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        }
                    }

                }

                #endregion

            }
            return "Permissions are updated";

        }







        public string savePermissionsForSelectedRole1(MODEL.RolePermissionModel.SavePermissionForSelectedRole model)
        {

            foreach (var item in model.PermissionsList)
            {
                if (item.IsActive != null)
                {
                    if (item.IsActive.Value == true)
                    {
                        Permission Per = (from perm in dbcontext.Permissions
                                          where perm.CompanyId == item.CompanyId
                                          && perm.UserRole == item.RoleId
                                          && perm.MenuId == item.MenuId
                                          select perm).FirstOrDefault();

                        if (Per == null)
                        {
                            Permission PerObj = new Permission();
                            PerObj.UserRole = item.RoleId;
                            PerObj.Link = item.Url;
                            PerObj.MenuId = item.MenuId;
                            PerObj.CompanyId = item.CompanyId;
                            PerObj.isActive = item.IsActive;
                            PerObj.isDeleted = false;

                            dbcontext.Permissions.Add(PerObj);

                        }
                        else
                        {
                            Per.isActive = item.IsActive;
                            dbcontext.Entry<Permission>(Per).State = EntityState.Modified;
                        }
                    }
                    else
                    {
                        Permission Per = (from perm in dbcontext.Permissions
                                          where perm.CompanyId == item.CompanyId
                                          && perm.UserRole == item.RoleId
                                          && perm.MenuId == item.MenuId
                                          select perm).FirstOrDefault();

                        if (Per != null)
                        {
                            dbcontext.Permissions.Remove(Per);
                        }

                    }
                }
                else
                {
                    Permission Per = (from perm in dbcontext.Permissions
                                      where perm.CompanyId == item.CompanyId
                                      && perm.UserRole == item.RoleId
                                      && perm.MenuId == item.MenuId
                                      select perm).FirstOrDefault();

                    if (Per != null)
                    {
                        dbcontext.Permissions.Remove(Per);
                    }

                }

                dbcontext.SaveChanges();

                // Saving Internal Permissions
                foreach (var Internalitem in item.InternalPermissions)
                {
                    if (Internalitem.IsPermissionActive != null)
                    {
                        if (Internalitem.IsPermissionActive.Value == true)
                        {
                            InternalPermission Per = (from perm in dbcontext.InternalPermissions
                                                      where perm.CompanyId == Internalitem.CompanyId
                                                      && perm.RoleId == Internalitem.RoleId
                                                      //&& perm.MenuId == Internalitem.MenuId
                                                      && perm.PermissionName == Internalitem.PermissionName
                                                      //&& perm.Id == Internalitem.PermissionId
                                                      select perm).FirstOrDefault();

                            if (Per == null)
                            {
                                InternalPermission PerObj = new InternalPermission();
                                PerObj.PermissionName = Internalitem.PermissionName;
                                PerObj.MenuId = Internalitem.MenuId;
                                PerObj.RoleId = Internalitem.RoleId;
                                PerObj.CompanyId = Internalitem.CompanyId;
                                PerObj.isActive = Internalitem.IsPermissionActive;
                                PerObj.isDeleted = false;

                                dbcontext.InternalPermissions.Add(PerObj);

                            }
                            else
                            {
                                Per.isActive = Internalitem.IsPermissionActive;
                                dbcontext.Entry<InternalPermission>(Per).State = EntityState.Modified;
                            }
                        }
                        else
                        {
                            InternalPermission Per = (from perm in dbcontext.InternalPermissions
                                                      where perm.CompanyId == Internalitem.CompanyId
                                                      && perm.RoleId == Internalitem.RoleId
                                                      //&& perm.MenuId == Internalitem.MenuId
                                                      && perm.PermissionName == Internalitem.PermissionName
                                                      //&& perm.Id == Internalitem.PermissionId
                                                      select perm).FirstOrDefault();

                            if (Per != null)
                            {
                                dbcontext.InternalPermissions.Remove(Per);
                            }

                        }
                    }
                    else
                    {
                        InternalPermission Per = (from perm in dbcontext.InternalPermissions
                                                  where perm.CompanyId == Internalitem.CompanyId
                                                  && perm.RoleId == Internalitem.RoleId
                                                  //&& perm.MenuId == Internalitem.MenuId
                                                  && perm.PermissionName == Internalitem.PermissionName
                                                  //&& perm.Id == Internalitem.PermissionId
                                                  select perm).FirstOrDefault();

                        if (Per != null)
                        {
                            dbcontext.InternalPermissions.Remove(Per);
                        }

                    }
                    dbcontext.SaveChanges();

                }

            }


            return "Permissions are updated";
        }



        public List<MODEL.RolePermissionModel.AllInternalPermissionsModel> GetAllInternalPermissionsForPermission(int MenuId, int RoleId, int CompanyId)
        {
            List<MODEL.RolePermissionModel.AllInternalPermissionsModel> AllPermissions = (from perm in dbcontext.InternalPermissions
                                                                                          where perm.isActive == true && perm.isDeleted == false
                                                                                          && perm.RoleId == 0
                                                                                          && perm.MenuId == MenuId
                                                                                          && perm.CompanyId == 0
                                                                                          select new MODEL.RolePermissionModel.AllInternalPermissionsModel
                                                                                          {
                                                                                              PermissionId = (from intPer in dbcontext.InternalPermissions
                                                                                                              where intPer.CompanyId == CompanyId
                                                                                                              && intPer.RoleId == RoleId
                                                                                                              && intPer.MenuId == MenuId
                                                                                                              && intPer.isActive == true
                                                                                                              && intPer.isDeleted == false
                                                                                                              select intPer.Id).FirstOrDefault(),
                                                                                              PermissionName = perm.PermissionName,
                                                                                              CompanyId = CompanyId,
                                                                                              RoleId = RoleId,
                                                                                              MenuId = MenuId,
                                                                                              IsPermissionActive = (from intPer in dbcontext.InternalPermissions
                                                                                                                    where intPer.CompanyId == CompanyId
                                                                                                                    && intPer.RoleId == RoleId
                                                                                                                    && intPer.MenuId == MenuId
                                                                                                                    && intPer.isActive == true
                                                                                                                    && intPer.isDeleted == false
                                                                                                                    select intPer.isActive).FirstOrDefault()
                                                                                          }
                                                                                           ).ToList();







            return AllPermissions;
        }




        public string SaveInternalPermissions(MODEL.RolePermissionModel.SaveInternalPermissionsModel model)
        {

            foreach (var item in model.PermissionsList)
            {
                if (item.IsPermissionActive != null)
                {
                    if (item.IsPermissionActive.Value == true)
                    {
                        InternalPermission Per = (from perm in dbcontext.InternalPermissions
                                                  where perm.CompanyId == item.CompanyId
                                                  && perm.RoleId == item.RoleId
                                                  && perm.MenuId == item.MenuId
                                                  && perm.Id == item.PermissionId
                                                  select perm).FirstOrDefault();

                        if (Per == null)
                        {
                            InternalPermission PerObj = new InternalPermission();
                            PerObj.PermissionName = item.PermissionName;
                            PerObj.MenuId = item.MenuId;
                            PerObj.RoleId = item.RoleId;
                            PerObj.CompanyId = item.CompanyId;
                            PerObj.isActive = item.IsPermissionActive;
                            PerObj.isDeleted = false;

                            dbcontext.InternalPermissions.Add(PerObj);

                        }
                        else
                        {
                            Per.isActive = item.IsPermissionActive;
                            dbcontext.Entry<InternalPermission>(Per).State = EntityState.Modified;
                        }
                    }
                    else
                    {
                        InternalPermission Per = (from perm in dbcontext.InternalPermissions
                                                  where perm.CompanyId == item.CompanyId
                                                  && perm.RoleId == item.RoleId
                                                  && perm.MenuId == item.MenuId
                                                  && perm.Id == item.PermissionId
                                                  select perm).FirstOrDefault();

                        if (Per != null)
                        {
                            dbcontext.InternalPermissions.Remove(Per);
                        }

                    }
                }
                else
                {
                    InternalPermission Per = (from perm in dbcontext.InternalPermissions
                                              where perm.CompanyId == item.CompanyId
                                              && perm.RoleId == item.RoleId
                                              && perm.MenuId == item.MenuId
                                              && perm.Id == item.PermissionId
                                              select perm).FirstOrDefault();

                    if (Per != null)
                    {
                        dbcontext.InternalPermissions.Remove(Per);
                    }

                }
                dbcontext.SaveChanges();

            }


            return "Internal Permissions are updated";
        }






    }
}
