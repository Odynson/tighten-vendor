﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using Dapper;

namespace TIGHTEN.DATA
{
    public class RoleManage : BaseClass
    {
        AppContext dbcontext;

        public List<MODEL.RoleManageModel.AllRolesForCompany> GetAllRolesForCompany(int CompanyId)
        {
            List<MODEL.RoleManageModel.AllRolesForCompany> AllRoles;
            int Id;
            using (var con = new SqlConnection(ConnectionString))
            {

                AllRoles = con.Query<MODEL.RoleManageModel.AllRolesForCompany>("Ups_GetAllRolesForCompany", new
                {
                    CompanyId = CompanyId,

                }, commandType: CommandType.StoredProcedure).ToList();
            }




            //AllRoles = (from rol in dbcontext.Roles
            //                                                           where rol.isActive == true && rol.IsDeleted == false
            //                                                           && (rol.CompanyId == 0 || rol.CompanyId == CompanyId)
            //                                                           select new MODEL.RoleManageModel.AllRolesForCompany
            //                                                           {
            //                                                               RoleId = rol.RoleId,
            //                                                               RoleName = rol.Name,
            //                                                               RoleDescription = rol.Description,
            //                                                               CompanyId  = rol.CompanyId,
            //                                                               RolePriority = rol.Priority
            //                                                           }
            //                                                           ).ToList();

            return AllRoles;
        }


        public string saveRole(MODEL.RoleManageModel.RoleAddUpdateModel obj)
        {
            string msg = string.Empty;
            int Id;
            using (var con = new SqlConnection(ConnectionString))
            {

                Id = con.Query<int>("Usp_InsertRole", new
                {
                    Name = obj.RoleModel.RoleName,
                    Description = obj.RoleModel.RoleDescription,
                    CreatedBy = obj.UserId,
                    ModifiedBy = obj.UserId,
                    CompanyId = obj.CompanyId,
                    Priority = obj.RoleModel.RolePriority,
                    RoleVisibleToVendor = obj.RoleModel.RoleVisibleToVendor,
                    Type = obj.RoleModel.Type


                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                if (Id == 1)
                {
                    if (obj.RoleModel.Type == 1)
                    {
                        msg = "RoleAddedSuccess";
                    }
                    else  if(obj.RoleModel.Type==2)
                    {
                        msg = "DesignationAddedSuccess";
                    }
                    else if(obj.RoleModel.Type == 3)
                    {
                        msg = "BothAddedSuccess";
                    }
                }
                else if (Id == -1)
                {
                    if (obj.RoleModel.Type == 1)
                    {
                        msg = "DuplicatePriority";
                    }
                    else if (obj.RoleModel.Type == 2)
                    {
                        msg = "DuplicateDesignationPriority";
                    }
                    else if (obj.RoleModel.Type == 3)
                    {
                        msg = "DuplicateRecordPriority";
                    }
                }
                else if (Id == -2)
                {
                    if (obj.RoleModel.Type == 1)
                    {
                        msg = "DuplicateRole";
                    }
                    else if (obj.RoleModel.Type == 2)
                    {
                        msg = "DuplicateDesignation";
                    }
                    else if (obj.RoleModel.Type == 3)
                    {
                        msg = "DuplicateRecord";
                    }

                }


            }


            return msg;
        }

        /// <summary>
        /// Saving New Role
        /// </summary>
        /// this function wiht linq now rename by 1
        /// 
        public string saveRole1(MODEL.RoleManageModel.RoleAddUpdateModel obj)
        {
            string msg = string.Empty;

            var isRoleExist = (from rol in dbcontext.Roles
                               where rol.Name == obj.RoleModel.RoleName && rol.IsDeleted == false
                               && (rol.CompanyId == obj.CompanyId || rol.CompanyId == 0)
                               select rol.RoleId).SingleOrDefault();

            if (isRoleExist < 1)
            {
                var isPriorityExist = (from rol in dbcontext.Roles
                                       where (rol.CompanyId == obj.CompanyId || rol.CompanyId == 0)
                                       && rol.Priority == obj.RoleModel.RolePriority
                                       && rol.IsDeleted == false
                                       select rol).FirstOrDefault();

                if (isPriorityExist != null)
                {
                    msg = "DuplicatePriority";
                }
                else
                {
                    TIGHTEN.ENTITY.Role role = new TIGHTEN.ENTITY.Role
                    {
                        Name = obj.RoleModel.RoleName,
                        Description = obj.RoleModel.RoleDescription,
                        isActive = true,
                        CreatedBy = obj.UserId,
                        CreatedDate = DateTime.Now,
                        ModifiedBy = obj.UserId,
                        ModifiedDate = DateTime.Now,
                        CompanyId = obj.CompanyId,
                        Priority = obj.RoleModel.RolePriority
                    };

                    dbcontext.Roles.Add(role);
                    dbcontext.SaveChanges();

                    msg = "RoleAddedSuccess";
                }

            }
            else
            {
                msg = "DuplicateRole";
            }

            return msg;
        }



        public string updateRole(MODEL.RoleManageModel.RoleAddUpdateModel obj)
        {
            string msg = string.Empty;
            int Id;
            using (var con = new SqlConnection(ConnectionString))
            {

                Id = con.Query<int>("Usp_UpdateRole", new
                {
                    RoleId=obj.RoleModel.RoleId,
                    Name = obj.RoleModel.RoleName,
                    Description = obj.RoleModel.RoleDescription,
                    ModifiedBy = obj.UserId,
                    CompanyId = obj.CompanyId,
                    Priority = obj.RoleModel.RolePriority,
                    Type=obj.RoleModel.Type

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }

            if (Id == 1)
            {
                if(obj.RoleModel.Type==1)
                {
                    msg = "RoleUpdatedSuccess";
                }
                else if (obj.RoleModel.Type == 2)
                {
                    msg = "DesignationUpdatedSuccess";
                }
                else if (obj.RoleModel.Type == 3)
                {
                    msg = "RecordUpdatedSuccess";
                }
            }
            else if (Id == -1)
            {
                if (obj.RoleModel.Type == 1)
                {
                    msg = "DuplicatePriority";
                }
                else if (obj.RoleModel.Type == 2)
                {
                    msg = "DuplicateDesignationPriority";
                }
                else if (obj.RoleModel.Type == 3)
                {
                    msg = "DuplicateRecordPriority";
                }
            }
            else if (Id == -2)
            {
                if (obj.RoleModel.Type == 1)
                {
                    msg = "DuplicateRole";
                }
                else if (obj.RoleModel.Type == 2)
                {
                    msg = "DuplicateDesignation";
                }
                else if (obj.RoleModel.Type == 3)
                {
                    msg = "DuplicateRecord";
                }
            }

            return msg;

        }

        /// <summary>
        /// This function updates the existing Team
        /// </summary>
        ///
         //this function wiht linq now rename by 1
        public string updateRole1(MODEL.RoleManageModel.RoleAddUpdateModel obj)
        {
            string msg = string.Empty;

            var role = (from rol in dbcontext.Roles
                        where rol.Name == obj.RoleModel.RoleName && rol.IsDeleted == false
                        && (rol.CompanyId == obj.CompanyId || rol.CompanyId == 0)
                        && rol.RoleId != obj.RoleModel.RoleId
                        select rol).FirstOrDefault();

            if (role == null)
            {
                var isPriorityExist = (from rl in dbcontext.Roles
                                       where (rl.CompanyId == obj.CompanyId || rl.CompanyId == 0)
                                       && rl.Priority == obj.RoleModel.RolePriority
                                       && rl.RoleId != obj.RoleModel.RoleId
                                       && rl.IsDeleted == false
                                       select rl).FirstOrDefault();

                if (isPriorityExist != null)
                {
                    msg = "DuplicatePriority";
                }
                else
                {
                    TIGHTEN.ENTITY.Role rol = (from rl in dbcontext.Roles
                                               where rl.RoleId == obj.RoleModel.RoleId
                                               && rl.IsDeleted == false
                                               select rl).FirstOrDefault();

                    rol.Name = obj.RoleModel.RoleName;
                    rol.Description = obj.RoleModel.RoleDescription;
                    rol.ModifiedBy = obj.UserId;
                    rol.ModifiedDate = DateTime.Now;
                    rol.CompanyId = obj.CompanyId;
                    rol.Priority = obj.RoleModel.RolePriority;

                    dbcontext.SaveChanges();

                    msg = "RoleUpdatedSuccess";
                }
            }
            else
            {
                msg = "DuplicateRole";
            }

            return msg;

        }



        /// <summary>
        /// This function deletes the Particular Role using Linq
        /// </summary>
        /// <param name="RoleId">It is the Unique Id of Role</param>
        public void deleteRole1(int RoleId)
        {


            TIGHTEN.ENTITY.Role roleEntity = (from m in dbcontext.Roles where m.RoleId == RoleId && m.IsDeleted == false select m).FirstOrDefault();

            //dbcontext.Roles.Remove(roleEntity);
            roleEntity.IsDeleted = true;
            dbcontext.SaveChanges();

        }

        public string activeInactiveRoles(int RoleId, int CompanyId)
        {
            string msg = string.Empty;
            int Id;
            using (var con = new SqlConnection(ConnectionString))
            {

                Id = con.Query<int>("usp_ActiveInactiveRoles", new
                {

                    RoleId = RoleId,
                    CompanyId = CompanyId
                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }

            if (Id == 0)
            {
                msg = "Status updated scuccessfully.";
            }
            else
            {

                msg = "Error occurred try again.";
            }
            return msg;

        }

        /// <summary>
        /// This function deletes the Particular Role 
        /// /// </summary>
        /// <param name="RoleId">It is the Unique Id of Role</param>
        public string deleteRole(int RoleId,int CompanyId)
        {


            string msg = string.Empty;
            int Id;
            using (var con = new SqlConnection(ConnectionString))
            {

                Id = con.Query<int>("usp_DeleteRoles", new
                {

                    RoleId = RoleId,
                    CompanyId = CompanyId
                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }

            if (Id > 0)
            {
                msg = "Record deleted successfully";
            }
            else
            {

                msg = "Error occurred try again.";
            }
            return msg;

        }

    }
}
