﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL.ParentAccount;
using TIGHTEN.UTILITIES;
using TIGHTEN.DATA;
using System.Data;
using TIGHTEN.MODEL;
using static TIGHTEN.MODEL.ParentAccount.ParentAccountModel;
using System.Net.Http;
using Dapper;
using System.Data.SqlClient;
namespace TIGHTEN.DATA.ParentAccount
{
    public class ParentAccountDAL : BaseClass
    {
        public List<ParentAccountModel> GetParentAccountList(int CompanyId)
        {

            List<ParentAccountModel> objAcc;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT Id,[Name],CompanyId,(select top 1 name from companies where id=ParentAcountDetail.CompanyId) CompanyName,VendorId,(select top 1 name from companies where id=ParentAcountDetail.VendorId) VendorCompanyName,Remarks,PersonName,PersonPhNo,Personemail,TotalCost, CreatedDate,IsActive FROM ParentAcountDetail  where CompanyId = @CompanyId and IsDeleted<>1";
                objAcc = con.Query<ParentAccountModel>(query, new
                {
                    CompanyId = CompanyId,

                }).ToList();

            }

            return objAcc;



        }

        public ParentAccountModel GetParentAccountById(int Id)
        {

            ParentAccountModel objAcc;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT Id,[Name],CompanyId,(select top 1 name from companies where id=ParentAcountDetail.CompanyId) CompanyName,VendorId,(select top 1 name from companies where id=ParentAcountDetail.VendorId) VendorCompanyName,Remarks,PersonName,PersonPhNo,Personemail,TotalCost, CreatedDate,IsActive FROM ParentAcountDetail  where Id = @id and IsDeleted<>1";
                objAcc = con.Query<ParentAccountModel>(query, new
                {
                    id = Id,

                }).FirstOrDefault();

            }

            return objAcc;



        }

        public string SaveNewParentAccount(ParentAccountModel modal)
        {
            string msg = "";
            ParentAccountModel objCompanysend = new ParentAccountModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_SaveNewParentAccount",
                    new
                    {
                        Name = modal.Name,
                        VendorId = modal.VendorId,
                        CompanyId=modal.CompanyId,
                        PersonName=modal.PersonName,
                        PersonPhNo=modal.PersonPhNo,
                        PersonEmail=modal.PersonEmail,
                        TotalCost=modal.TotalCost,
                        Remarks=modal.Remarks
                    }, commandType: CommandType.StoredProcedure);

                objCompanysend = Result.Read<ParentAccountModel>().FirstOrDefault();
                if (objCompanysend.Id > 0)
                {
                    msg = "Parent Account is successfully saved";
                }
                else if (objCompanysend.Id == -1)
                {
                    msg = "Parent Account is already exist";
                }

            }

            return msg;
        }

        public string deleteParentAccountDetail(int Id)
        {
            string msg = string.Empty;
            int ResponseId;

            using (var con = new SqlConnection(ConnectionString))

            {

                ResponseId = con.Query<int>("Usp_deleteParentAccountDetail", new
                {

                    Id = Id,


                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }
            if (ResponseId == 1)
            {
                msg = "ParentAccDetailDeletedSuccessfully";
            }
            return msg;
        }

        public string updateParentAccountDetail(ParentAccountModel saveObj)
        {
            string msg = string.Empty;
            int ResponseId;
            string userIds = "";
            ParentAccountModel responseObj = new ParentAccountModel();
            using (var con = new SqlConnection(ConnectionString))

            {

                ResponseId = con.Query<int>("Usp_UpdateParentAccountDetail", new
                {

                    Id = saveObj.Id,
                    Name = saveObj.Name,
                    PersonName = saveObj.PersonName,
                    PersonPhNo = saveObj.PersonPhNo,
                    PersonEmail = saveObj.PersonEmail,
                    TotalCost = saveObj.TotalCost,
                    Remarks = saveObj.Remarks

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                //responseObj = con.Query<ParentAccountModel>("Usp_GetAccountDetailForMailOnAssignPOC", new
                //{
                //    Id = saveObj.Id
                //}, commandType: CommandType.StoredProcedure

                // ).FirstOrDefault();
            }
            if (ResponseId >= 1)
            {
                msg = "ParentAccDetailUpdatedSuccessfully";
               


            }
            else if (ResponseId == -1)
            {
                msg = "ParentAccountNotExists";
            }
            return msg;
        }

        public List<ParentAccountModel> GetParentAccountForVendorList(int VendorId)
        {

            List<ParentAccountModel> objAcc;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT Id,[Name],CompanyId,(select top 1 name from companies where id=ParentAcountDetail.CompanyId) CompanyName,VendorId,(select top 1 name from companies where id=ParentAcountDetail.VendorId) VendorCompanyName,Remarks,PersonName,PersonPhNo,Personemail,TotalCost, CreatedDate,IsActive FROM ParentAcountDetail  where VendorId = @VendorId and IsDeleted<>1";
                objAcc = con.Query<ParentAccountModel>(query, new
                {
                    VendorId = VendorId,

                }).ToList();

            }

            return objAcc;



        }
    }
}
