﻿using TIGHTEN.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TIGHTEN.ENTITY;
using System.Web.Mvc;
using TIGHTEN.UTILITIES;

namespace TIGHTEN.DATA
{
    public class UserOnlineData : Common
    {

        public List<UserOnline> GetOnlineUser(String CompanyId)
        {
            return (from m in dbcontext.UserOnlines where (DateTime.Now - m.LastOnlineTime).TotalMinutes == 10 select m).ToList();
        }

        public List<UserOnlineModel> GetAllOnlineUser()
        {
            return (from t0 in dbcontext.UserOnlines
                    join t1 in dbcontext.UserDetails on t0.UserId equals t1.UserId
                    where t1.IsDeleted == false
                    select new UserOnlineModel
                    {
                        Id = t0.Id,
                        UserId = t0.UserId,
                        UserName = t1.UserName,
                        Email = t1.Email,
                        FirstName = t1.FirstName,
                        LastName = t1.LastName,
                        Gender = t1.Gender,
                        ProfilePhoto = t1.ProfilePhoto,
                        DateTimeDifference = (DateTime.Now - t0.LastOnlineTime).TotalMinutes,
                        LastOnlineTime = t0.LastOnlineTime
                    }).ToList();
        }

        public void AddUpdateOnlineUser(String UserId)
        {
            UserOnline User = (from t in dbcontext.UserOnlines where t.UserId == UserId select t).FirstOrDefault();
            if (User == null)
            {
                User = new UserOnline();
                User.UserId = UserId;
                User.LastOnlineTime = DateTime.Now;
                dbcontext.UserOnlines.Add(User);
            }
            else
            {
                User.LastOnlineTime = DateTime.Now;
            }
            dbcontext.SaveChanges();
        }

        

    }
}
