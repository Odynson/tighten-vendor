﻿using TIGHTEN.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TIGHTEN.ENTITY;
using System.Web.Mvc;
using TIGHTEN.UTILITIES;
using Dapper;
using System.Data.SqlClient;


namespace TIGHTEN.DATA
{
    public class UserMessagesData : BaseClass
    {
        AppContext dbcontext;

        public List<UserMessagesModel> GetAllUserMessages()
        {
            //return (from t in dbcontext.UserMessage
            //        select new UserMessagesModel
            //        {
            //            Id = t.Id,
            //            SenderId = t.SenderId,
            //            ReceiverId = t.ReceiverId,
            //            Message = t.Message,
            //            IsRead = t.IsRead,
            //            CreatedDateTime = t.CreatedDateTime
            //        }).ToList();

            List<UserMessagesModel> mod;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select t.Id,t.SenderId,t.ReceiverId,t.Message,t.IsRead,t.CreatedDateTime form UserMessages t ";
                mod = con.Query<UserMessagesModel>(sqlquery).ToList();
            }

            return mod;
        }

        public List<UserMessagesModel> GetUserMessages(string reciverId, string senderId)
        {
            //List<UserMessagesModel> UserSenderId = (from t in dbcontext.UserMessage
            //                                        join r in dbcontext.UserDetails on t.ReceiverId equals r.UserId
            //                                        join s in dbcontext.UserDetails on t.SenderId equals s.UserId
            //                                        where t.ReceiverId == reciverId && t.SenderId == senderId && r.IsDeleted == false && s.IsDeleted == false
            //                                        select new UserMessagesModel
            //                                        {
            //                                            Id = t.Id,
            //                                            SenderId = t.SenderId,
            //                                            SenderName = s.FirstName,
            //                                            SenderProfileImage = s.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + s.ProfilePhoto,
            //                                            ReceiverId = t.ReceiverId,
            //                                            ReceiverName = r.FirstName,
            //                                            ReceiverProfileImage = r.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + r.ProfilePhoto,
            //                                            Message = t.Message,
            //                                            IsRead = t.IsRead,
            //                                            IsSender = true,
            //                                            CreatedDateTime = t.CreatedDateTime
            //                                        }).ToList();

                                        


            //List<UserMessagesModel> UserReceiverId = (from t in dbcontext.UserMessage
            //                                          join r in dbcontext.UserDetails on t.ReceiverId equals r.UserId
            //                                          join s in dbcontext.UserDetails on t.SenderId equals s.UserId
            //                                          where t.ReceiverId == senderId && t.SenderId == reciverId && r.IsDeleted == false && s.IsDeleted == false
            //                                          select new UserMessagesModel
            //                                          {
            //                                              Id = t.Id,
            //                                              SenderId = t.SenderId,
            //                                              SenderName = s.FirstName,
            //                                              SenderProfileImage = s.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + s.ProfilePhoto,
            //                                              ReceiverId = t.ReceiverId,
            //                                              ReceiverName = r.FirstName,
            //                                              ReceiverProfileImage = r.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + r.ProfilePhoto,
            //                                              Message = t.Message,
            //                                              IsRead = t.IsRead,
            //                                              IsSender = true,
            //                                              CreatedDateTime = t.CreatedDateTime
            //                                          }).ToList();

            List<UserMessagesModel> UserSenderId;
            List<UserMessagesModel> UserReceiverId;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetUserMessages", new { reciverId = reciverId, senderId = senderId },commandType:System.Data.CommandType.StoredProcedure);
                UserSenderId = result.Read<UserMessagesModel>().ToList();
                UserReceiverId = result.Read<UserMessagesModel>().ToList();
            }


            var UserMessage = UserSenderId.Union(UserReceiverId).OrderBy(x => x.CreatedDateTime).ToList();

            UpdateMessageIsReadStatus(reciverId, senderId);
            return UserMessage;
        }

        public List<UserLeftDisplayModel> GetSenderUsers(string UserId)
        {
            List<UserLeftDisplayModel> UserSender = (from t in dbcontext.UserMessage
                                                     join s in dbcontext.UserDetails on t.SenderId equals s.UserId
                                                     where t.ReceiverId == UserId && s.IsDeleted == false
                                                     select new UserLeftDisplayModel
                                                     {
                                                         SenderId = t.SenderId,
                                                         SenderName = s.FirstName,
                                                         SenderProfileImage = s.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + s.ProfilePhoto,
                                                         CreatedDateTime = dbcontext.UserMessage.Where(x => x.SenderId == t.SenderId || x.ReceiverId == t.SenderId).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault().CreatedDateTime,
                                                         UnreadCount = dbcontext.UserMessage.Where(x => x.SenderId == t.SenderId && x.IsRead == false).Count()

                                                     }).Distinct().ToList();
            List<UserLeftDisplayModel> UserSender1 = (from t in dbcontext.UserMessage
                                                      join r in dbcontext.UserDetails on t.ReceiverId equals r.UserId
                                                      where t.SenderId == UserId && r.IsDeleted == false
                                                      select new UserLeftDisplayModel
                                                      {
                                                          SenderId = t.ReceiverId,
                                                          SenderName = r.FirstName,
                                                          SenderProfileImage = r.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + r.ProfilePhoto,
                                                          CreatedDateTime = dbcontext.UserMessage.Where(x => x.SenderId == t.ReceiverId || x.ReceiverId == t.ReceiverId).OrderByDescending(x => x.CreatedDateTime).FirstOrDefault().CreatedDateTime,
                                                          UnreadCount = dbcontext.UserMessage.Where(x => x.SenderId == t.ReceiverId && x.IsRead == false).Count()
                                                      }).Distinct().ToList();

            List<UserLeftDisplayModel> UserSender12 = new List<UserLeftDisplayModel>();
            foreach (var s in UserSender1)
            {
                if (!UserSender.Select(x => x.SenderId).Contains(s.SenderId))
                {
                    UserSender12.Add(s);
                }

            }

            return UserSender.Union(UserSender12).OrderByDescending(x => x.CreatedDateTime).Distinct().ToList();
        }

        public int GetMessagesCount(string UserId)
        {
            //return dbcontext.UserMessage.Where(x => x.ReceiverId == UserId && x.IsRead == false).Count();
            int MessagesCount = 0;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlQuery = "select count(Id) from UserMessages where ReceiverId = @UserId and IsRead = 0";
                MessagesCount = con.Query<int>(sqlQuery,new { UserId= UserId }).Single();
            }

            return MessagesCount;
        }

        public void AddUserMessages(TIGHTEN.MODEL.UserMessagesModel UserId)
        {
            //UserMessages UserMsg = new UserMessages();
            //UserMsg.SenderId = UserId.SenderId;
            //UserMsg.ReceiverId = UserId.ReceiverId;
            //UserMsg.Message = UserId.Message;
            //UserMsg.IsRead = false;
            //UserMsg.CreatedDateTime = DateTime.UtcNow;
            //dbcontext.UserMessage.Add(UserMsg);
            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"insert into UserMessages (SenderId,ReceiverId,Message,IsRead,CreatedDateTime ) values (@SenderId,@ReceiverId,@Message,0,GETDATE() ) ";
                con.Execute(sqlquery, new { SenderId = UserId.SenderId, ReceiverId = UserId.ReceiverId, Message = UserId.Message });
            }
                
        }

        public List<UserMessagesModel> GetUpdateMessageIsReadStatus(MODEL.UserMessagesModel msg)
        {
            //List<UserMessages> User = (from t in dbcontext.UserMessage
            //                           where t.SenderId == msg.ReceiverId && t.ReceiverId == msg.SenderId && t.IsRead == false
            //                           select t).ToList();

            

            //var mod = (from t in User
            //           join r in dbcontext.UserDetails on t.ReceiverId equals r.UserId
            //           join s in dbcontext.UserDetails on t.SenderId equals s.UserId
            //           where r.IsDeleted == false && s.IsDeleted == false
            //           select new UserMessagesModel
            //           {
            //               Id = t.Id,
            //               SenderId = t.SenderId,
            //               SenderName = s.FirstName,
            //               SenderProfileImage = s.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + s.ProfilePhoto,
            //               ReceiverId = t.ReceiverId,
            //               ReceiverName = r.FirstName,
            //               ReceiverProfileImage = r.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + r.ProfilePhoto,
            //               Message = t.Message,
            //               IsRead = t.IsRead,
            //               IsSender = true,
            //               CreatedDateTime = t.CreatedDateTime
            //           }).ToList();

            

            //foreach (var item in User)
            //{
            //    item.IsRead = true;
            //    dbcontext.SaveChanges();
            //}

            List<UserMessagesModel> mod;
            string sqlquery = @"update UserMessages set IsRead = 1 where SenderId = @ReceiverId and ReceiverId = @SenderId and IsRead = 0 ";
            using (var con = new SqlConnection(ConnectionString))
            {
                mod = con.Query<UserMessagesModel>("usp_GetUpdateMessageIsReadStatus", new { reciverId = msg.SenderId, senderId = msg.ReceiverId }, commandType: System.Data.CommandType.StoredProcedure).ToList();
                con.Execute(sqlquery, new { ReceiverId = msg.SenderId, SenderId = msg.ReceiverId });
            }

            return mod;
        }

        public void UpdateMessageIsReadStatus(string reciverId, string senderId)
        {
            //List<UserMessages> User = (from t in dbcontext.UserMessage
            //                           where t.SenderId == reciverId && t.ReceiverId == senderId && t.IsRead == false
            //                           select t).ToList();
            //foreach (var item in User)
            //{
            //    item.IsRead = true;
            //    dbcontext.SaveChanges();
            //}

            string sqlquery = @"update UserMessages set IsRead = 1 where SenderId = @reciverId and ReceiverId = @senderId and IsRead = 0";
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute(sqlquery, new { reciverId = reciverId, senderId = senderId });
            }

        }

        public List<UserLeftDisplayModel> GetAllSenders(string SenderId, int CompanyId, string UId)
        {

            //var IsUserFreelancer = (from usr in dbcontext.UserDetails
            //                        where usr.UserId == SenderId && usr.IsDeleted == false
            //                        select usr.IsFreelancer).FirstOrDefault();

            //var TeamId = (from tem in dbcontext.Teams
            //              join temusr in dbcontext.TeamUsers on tem.Id equals temusr.TeamId
            //              where temusr.UserId == SenderId
            //              && tem.IsDefault == false
            //              select tem.Id).ToList();


            //List<UserLeftDisplayModel> UserSender = (from usrdet in dbcontext.UserDetails
            //                                         join usrcomrol in dbcontext.UserCompanyRoles on usrdet.UserId equals usrcomrol.UserId
            //                                         join comp in dbcontext.Companies on usrcomrol.CompanyId equals comp.Id
            //                                         //join TeamUsr in dbcontext.TeamUsers on usrcomrol.UserId equals TeamUsr.UserId
            //                                         //join team in dbcontext.Teams on TeamUsr.TeamId equals team.Id
            //                                         //join prjctusr in dbcontext.ProjectUsers on TeamUsr.UserId equals prjctusr.UserId
            //                                         //join prjct in dbcontext.Projects on prjctusr.ProjectId equals prjct.Id
            //                                         where usrcomrol.UserId != SenderId && usrdet.IsDeleted == false
            //                                         && usrdet.EmailConfirmed == true
            //                                         //&& TeamId.Contains(team.Id)
            //                                         && (IsUserFreelancer == true ? usrcomrol.CompanyId == usrcomrol.CompanyId : usrcomrol.CompanyId == CompanyId)
            //                                         select new UserLeftDisplayModel
            //                                         {
            //                                             CompanyName = comp.Name,
            //                                             SenderId = usrdet.UserId,
            //                                             SenderName = usrdet.FirstName,
            //                                             SenderEmail =  usrdet.Email,
            //                                             SenderProfileImage = usrdet.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + usrdet.ProfilePhoto,

            //                                             //CreatedDateTime = dbcontext.UserMessage.Where(x => x.SenderId == usrdet.UserId && x.ReceiverId == SenderId).OrderByDescending(x => (x.CreatedDateTime == null ? DateTime.Now :x.CreatedDateTime)).FirstOrDefault().CreatedDateTime,
            //                                             UnreadCount = dbcontext.UserMessage.Where(x => x.SenderId == usrdet.UserId && x.ReceiverId == SenderId && x.IsRead == false).Count()

            //                                         }).Distinct().ToList();


            List<UserLeftDisplayModel> UserSender;
            using (var con = new SqlConnection(ConnectionString))
            {
                UserSender = con.Query<UserLeftDisplayModel>("usp_GetAllSenders", new { SenderId = SenderId, CompanyId= CompanyId }, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }


            List<UserLeftDisplayModel> UserSenderToReturn = new List<UserLeftDisplayModel>();
            List<UserLeftDisplayModel> UserSenderToReturnWithoutUnreadCount = new List<UserLeftDisplayModel>();

            foreach (var item in UserSender)
            {
                //    var CreatedDate = (from usr_msg in dbcontext.UserMessage
                //                       where (usr_msg.SenderId == item.SenderId && usr_msg.ReceiverId == SenderId) 
                //                       || (usr_msg.SenderId == SenderId && usr_msg.ReceiverId == item.SenderId)
                //                       orderby usr_msg.CreatedDateTime descending
                //                       select usr_msg.CreatedDateTime).ToList();

                List<DateTime> CreatedDate ;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"select usr_msg.CreatedDateTime from UserMessages usr_msg where (usr_msg.SenderId = @item_SenderId and usr_msg.ReceiverId = @SenderId) or (usr_msg.SenderId = @SenderId and usr_msg.ReceiverId = @item_SenderId)
                                        order by usr_msg.CreatedDateTime desc ";
                    CreatedDate = con.Query<DateTime>(sqlquery, new { item_SenderId = item.SenderId, SenderId = SenderId }).ToList();
                }

                if (CreatedDate.Count() > 0)
                {
                    item.CreatedDateTime = CreatedDate[0];
                }
                else {
                    item.CreatedDateTime = null;
                }

                if (item.UnreadCount > 0)
                {
                    UserSenderToReturn.Add(item);
                }
                else
                {
                    UserSenderToReturnWithoutUnreadCount.Add(item);
                }

            }

            UserSenderToReturnWithoutUnreadCount = UserSenderToReturnWithoutUnreadCount.OrderByDescending(x => x.CreatedDateTime).ToList();
            UserSenderToReturn = UserSenderToReturn.OrderByDescending(y => y.CreatedDateTime).ToList();

            foreach (var useritem in UserSenderToReturnWithoutUnreadCount)
            {
                UserSenderToReturn.Add(useritem);
            }

            //if (UId != "0")
            //{
            //    UserLeftDisplayModel ListItem = UserSenderToReturn.Where(x => x.SenderId == UId).SingleOrDefault();
            //    UserSenderToReturn.Remove(ListItem);
            //    UserSenderToReturn.Insert(0, ListItem);
            //}

            return UserSenderToReturn;
        }



    }
}
