﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;

namespace TIGHTEN.DATA
{
    public class ClientFeedback
    {
        AppContext dbcontext;

        /// <summary>
        /// This function fetches all Feedbacks which are associated to the logged in user
        /// </summary>
        /// <param name="UserId">It is the unique id of logged in User</param>
        /// <returns>List of Client Feedbacks associated to the user</returns>
        public List<ClientFeedbackModel.ClientFeedbackDetailModel> getClientFeedbacks(string UserId)
        {
            dbcontext = new AppContext();

            UserDetail userEntity = (from m in dbcontext.UserDetails where m.UserId == UserId && m.IsDeleted == false select m).SingleOrDefault();

            List<ClientFeedbackModel.ClientFeedbackDetailModel> feedbackObject = (from m in dbcontext.Companies
                                                                                  join m1 in dbcontext.Teams on m.Id equals m1.CompanyId
                                                                                  join m2 in dbcontext.TeamUsers on m1.Id equals m2.TeamId
                                                                                  join m3 in dbcontext.Projects on m1.Id equals m3.TeamId
                                                                                  join m4 in dbcontext.ProjectUsers on m3.Id equals m4.ProjectId
                                                                                  join m5 in dbcontext.Sections on m3.Id equals m5.ProjectId
                                                                                  join m6 in dbcontext.ToDos on m5.Id equals m6.SectionId
                                                                                  join m7 in dbcontext.ClientFeedback on m6.Id equals m7.TodoId
                                                                                  join m8 in dbcontext.UserDetails on m7.CreatedBy equals m8.UserId

                                                                                  where m.Id == userEntity.CompanyId && m2.UserId == UserId && m4.UserId == UserId && m7.IsTodoFeedback == true
                                                                                  && m8.IsDeleted == false && m1.IsDeleted == false && m3.IsDeleted == false
                                                                                  && m6.IsDeleted == false
                                                                                  select new ClientFeedbackModel.ClientFeedbackDetailModel
                                                                                  {
                                                                                      ClientName = m8.FirstName + " " + m8.LastName,
                                                                                      CreatedDate = m7.CreatedDate,
                                                                                      ClientProfilePhoto = m8.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m8.ProfilePhoto,
                                                                                      ClientUserId = m7.CreatedBy,
                                                                                      Feedback = m7.Feedback,
                                                                                      IsTodoFeedback = true,
                                                                                      ProjectId = 0,
                                                                                      ProjectName = "",
                                                                                      Rating = m7.Rating,
                                                                                      TodoId = m6.Id,
                                                                                      TodoName = m6.Name
                                                                                  }
                                                                                    ).Union(from m in dbcontext.Companies
                                                                                            join m1 in dbcontext.Teams on m.Id equals m1.CompanyId
                                                                                            join m2 in dbcontext.TeamUsers on m1.Id equals m2.TeamId
                                                                                            join m3 in dbcontext.Projects on m1.Id equals m3.TeamId
                                                                                            join m4 in dbcontext.ProjectUsers on m3.Id equals m4.ProjectId
                                                                                            join m7 in dbcontext.ClientFeedback on m3.Id equals m7.ProjectId
                                                                                            join m8 in dbcontext.UserDetails on m7.CreatedBy equals m8.UserId

                                                                                            where m.Id == userEntity.CompanyId && m2.UserId == UserId && m4.UserId == UserId && m7.IsTodoFeedback == false
                                                                                            && m8.IsDeleted == false && m1.IsDeleted == false && m3.IsDeleted == false
                                                                                            select new ClientFeedbackModel.ClientFeedbackDetailModel
                                                                                            {
                                                                                                ClientName = m8.FirstName + " " + m8.LastName,
                                                                                                CreatedDate = m7.CreatedDate,
                                                                                                ClientProfilePhoto = m8.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m8.ProfilePhoto,
                                                                                                ClientUserId = m7.CreatedBy,
                                                                                                Feedback = m7.Feedback,
                                                                                                IsTodoFeedback = false,
                                                                                                ProjectId = m3.Id,
                                                                                                ProjectName = m3.Name,
                                                                                                Rating = m7.Rating,
                                                                                                TodoId = 0,
                                                                                                TodoName = ""
                                                                                            }
                                                                                   ).OrderByDescending(x => x.CreatedDate).ToList();

            return feedbackObject;




        }






        /// <summary>
        /// It saves the Client Feedback
        /// </summary>
        /// <param name="UserId">Unique id of user</param>
        /// <param name="model">it consists the values to be saved</param>
        public void SaveClientFeedback(string UserId, ClientFeedbackModel.ClientFeedbackAddModel model)
        {
            dbcontext = new AppContext();

            TIGHTEN.ENTITY.ClientFeedback clientFeedbackEntity = new TIGHTEN.ENTITY.ClientFeedback
            {
                TodoId = model.TodoId,
                ProjectId = model.ProjectId,
                Feedback = model.Feedback,
                Rating = model.Rating,
                IsTodoFeedback = model.IsTodoFeedback,
                CreatedBy = UserId,
                CreatedDate = DateTime.Now
            };

            dbcontext.ClientFeedback.Add(clientFeedbackEntity);
            dbcontext.SaveChanges();

        }

    }
}
