﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using Dapper;



namespace TIGHTEN.DATA
{
    public class Notifications : BaseClass
    {
        //AppContext dbcontext;

        public List<NotificationModel.NotificationDetailModel> GetNotifications(string Userid)
        {

            //List<NotificationModel.NotificationDetailModel> notObject = (from m in dbcontext.Notifications
            //                                                             join m1 in dbcontext.Teams on m.TeamId equals m1.Id
            //                                                             join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId

            //                                                             where m.NotifyingUserId == Userid && m.Type == 1
            //                                                             && m2.IsDeleted == false && m1.IsDeleted == false
            //                                                             select new NotificationModel.NotificationDetailModel
            //                                                             {
            //                                                                 Id = m.Id,
            //                                                                 ConcernedSectionId = m1.Id,
            //                                                                 ConcernedSectionName = m1.TeamName,
            //                                                                 FilePath = "#",
            //                                                                 CreatorName = (m2.LastName == " " || m2.LastName == null ? m2.FirstName : m2.FirstName + " " + m2.LastName ) ,
            //                                                                 CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                                 CreatorUserId = m2.UserId,
            //                                                                 IsRead = m.IsRead,
            //                                                                 Name = m.Name,
            //                                                                 Type = 1,
            //                                                                 CreatedDate = m.CreatedDate
            //                                                             }


            //                                                               ).Union

            //                                                                    (from m in dbcontext.Notifications
            //                                                                       join m1 in dbcontext.Projects on m.ProjectId equals m1.Id
            //                                                                       join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId

            //                                                                       where m.NotifyingUserId == Userid && m.Type == 2
            //                                                                       && m2.IsDeleted == false && m1.IsDeleted == false
            //                                                                       select new NotificationModel.NotificationDetailModel
            //                                                                       {
            //                                                                           Id = m.Id,
            //                                                                           ConcernedSectionId = m1.Id,
            //                                                                           ConcernedSectionName = m1.Name,
            //                                                                           FilePath = "#",
            //                                                                           CreatorName = (m2.LastName == " " || m2.LastName == null ? m2.FirstName : m2.FirstName + " " + m2.LastName),
            //                                                                           CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                                           CreatorUserId = m2.UserId,
            //                                                                           IsRead = m.IsRead,
            //                                                                           Name = m.Name,
            //                                                                           Type = 2,
            //                                                                           CreatedDate = m.CreatedDate
            //                                                                       }



            //                                                                       ).Union(from m in dbcontext.Notifications
            //                                                                               join m1 in dbcontext.ToDos on m.TodoId equals m1.Id
            //                                                                               join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId

            //                                                                               where m.NotifyingUserId == Userid && m.Type == 3
            //                                                                               && m2.IsDeleted == false && m1.IsDeleted == false
            //                                                                               select new NotificationModel.NotificationDetailModel
            //                                                                               {
            //                                                                                   Id = m.Id,
            //                                                                                   ConcernedSectionId = m1.Id,
            //                                                                                   ConcernedSectionName = m1.Name,
            //                                                                                   FilePath = "#",
            //                                                                                   CreatorName = (m2.LastName == " " || m2.LastName == null ? m2.FirstName : m2.FirstName + " " + m2.LastName),
            //                                                                                   CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                                                   CreatorUserId = m2.UserId,
            //                                                                                   IsRead = m.IsRead,
            //                                                                                   Name = m.Name,
            //                                                                                   Type = 3,
            //                                                                                   CreatedDate = m.CreatedDate
            //                                                                               }




            //                                                                                ).Union(from m in dbcontext.Notifications
            //                                                                                        join m1 in dbcontext.ToDoAttachments on m.AttachmentId equals m1.Id
            //                                                                                        join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId

            //                                                                                        where m.NotifyingUserId == Userid && m.Type == 4
            //                                                                                        && m2.IsDeleted == false
            //                                                                                        select new NotificationModel.NotificationDetailModel
            //                                                                                        {
            //                                                                                            Id = m.Id,
            //                                                                                            ConcernedSectionId = m1.Id,
            //                                                                                            ConcernedSectionName = m1.OriginalName,
            //                                                                                            FilePath = m1.Path,
            //                                                                                            CreatorName = (m2.LastName == " " || m2.LastName == null ? m2.FirstName : m2.FirstName + " " + m2.LastName),
            //                                                                                            CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                                                            CreatorUserId = m2.UserId,
            //                                                                                            IsRead = m.IsRead,
            //                                                                                            Name = m.Name,
            //                                                                                            Type = 4,
            //                                                                                            CreatedDate = m.CreatedDate
            //                                                                                        }


            //                                                                                        ).Union(from m in dbcontext.Notifications
            //                                                                                                 join m1 in dbcontext.ToDoComments on m.CommentId equals m1.Id
            //                                                                                                 join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId

            //                                                                                                 where m.NotifyingUserId == Userid && m.Type == 5
            //                                                                                                 && m2.IsDeleted == false
            //                                                                                                 select new NotificationModel.NotificationDetailModel
            //                                                                                                 {
            //                                                                                                     Id = m.Id,
            //                                                                                                     ConcernedSectionId = m1.Id,
            //                                                                                                     ConcernedSectionName = "",
            //                                                                                                     FilePath = "",
            //                                                                                                     CreatorName = (m2.LastName == " " || m2.LastName == null ? m2.FirstName : m2.FirstName + " " + m2.LastName),
            //                                                                                                     CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                                                                     CreatorUserId = m2.UserId,
            //                                                                                                     IsRead = m.IsRead,
            //                                                                                                     Name = m.Name,
            //                                                                                                     Type = 5,
            //                                                                                                     CreatedDate = m.CreatedDate
            //                                                                                                 }



            //                                                                                            ).Union(from m in dbcontext.Notifications
            //                                                                                                          join m1 in dbcontext.CalendarEvents on m.CalendarEventId equals m1.Id
            //                                                                                                          join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId

            //                                                                                                          where m.NotifyingUserId == Userid && m.Type == 6
            //                                                                                                          && m2.IsDeleted == false
            //                                                                                                          select new NotificationModel.NotificationDetailModel
            //                                                                                                          {
            //                                                                                                              Id = m.Id,
            //                                                                                                              ConcernedSectionId = m1.Id,
            //                                                                                                              ConcernedSectionName = m1.Title,
            //                                                                                                              FilePath = m1.Url,
            //                                                                                                              CreatorName = (m2.LastName == " " || m2.LastName == null ? m2.FirstName : m2.FirstName + " " + m2.LastName),
            //                                                                                                              CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                                                                              CreatorUserId = m2.UserId,
            //                                                                                                              IsRead = m.IsRead,
            //                                                                                                              Name = m.Name,
            //                                                                                                              Type = 6,
            //                                                                                                              CreatedDate = m.CreatedDate
            //                                                                                                          }





            //                                                                                                        ).Union(from m in dbcontext.Notifications
            //                                                                                                                   join m1 in dbcontext.UserDetails on m.CreatedBy equals m1.UserId
            //                                                                                                                   join m2 in dbcontext.Projects on m.ProjectId equals m2.Id
            //                                                                                                                   where m.NotifyingUserId == Userid && m.Type == 10
            //                                                                                                                   && m1.IsDeleted == false && m2.IsDeleted == false
            //                                                                                                                   select new NotificationModel.NotificationDetailModel
            //                                                                                                                   {
            //                                                                                                                       Id = m.Id,
            //                                                                                                                       ConcernedSectionId = m2.Id ,
            //                                                                                                                       ConcernedSectionName = m2.Name,
            //                                                                                                                       FilePath = "",
            //                                                                                                                       CreatorName = (m1.LastName == " " || m1.LastName == null ? m1.FirstName : m1.FirstName + " " + m1.LastName),
            //                                                                                                                       CreatorProfilePhoto = m1.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m1.ProfilePhoto,
            //                                                                                                                       CreatorUserId = m1.UserId,
            //                                                                                                                       IsRead = m.IsRead,
            //                                                                                                                       Name = m.Name,
            //                                                                                                                       Type = 10,
            //                                                                                                                       CreatedDate = m.CreatedDate
            //                                                                                                                   }).OrderByDescending(x => x.CreatedDate).ToList();


            List<NotificationModel.NotificationDetailModel> notObject;
            using (var con = new SqlConnection(ConnectionString))
            {
                notObject = con.Query<NotificationModel.NotificationDetailModel>("usp_GetNotifications", new { Userid = Userid }, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }

            return notObject;

            //var data = (from m in dbcontext.Notifications
            //            join m2 in dbcontext.UserDetails on m.CreatedBy equals m2.UserId
            //            join m3 in dbcontext.ToDos on m.TodoId equals m3.Id
            //            where m.NotifyingUserId == Userid
            //            orderby m.CreatedDate descending
            //            let ProfilePhoto = (m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m2.ProfilePhoto)
            //            let Filter = m.IsRead
            //            select new { m.Id, m.TodoId, m.Name, m2.FirstName, m2.LastName, ProfilePhoto, m.IsRead, m.CreatedDate, Filter }

            //              ).ToList();

            //return data;


        }

        public void SaveNotification(TodoModel.Notifications obj, string Userid)
        {

            // Flag is 1 when we want to send notification to multiple users for example all followers
            //if (obj.Flag == 1)
            //{

            //    var objFollowers = (from m in dbcontext.Followers where m.TodoId == obj.TodoId  select m).ToList();
            //    var objAssignee = (from m in dbcontext.ToDos where m.Id == obj.TodoId && m.TaskType == 5 && m.IsDeleted == false select m).SingleOrDefault();
            //    var objAssigneeInFollowersOrNot = (from l in objFollowers where l.UserId == objAssignee.AssigneeId select l).SingleOrDefault();
            //    foreach (var k in objFollowers)
            //    {

            //        Notification objNotification = new Notification
            //        {
            //            Name = obj.Name,
            //            TodoId = obj.TodoId,
            //            CreatedBy = Userid,
            //            CreatedDate = DateTime.Now.ToUniversalTime(),
            //            NotifyingUserId = k.UserId,
            //            IsRead = false,


            //        };

            //        dbcontext.Notifications.Add(objNotification);
            //        dbcontext.SaveChanges();
            //    }

            //    if (objAssigneeInFollowersOrNot == null)
            //    {
            //        Notification objNot = new Notification
            //        {
            //            Name = obj.Name,
            //            TodoId = obj.TodoId,
            //            CreatedBy = Userid,
            //            CreatedDate = DateTime.Now.ToUniversalTime(),
            //            NotifyingUserId = objAssignee.AssigneeId,
            //            IsRead = false
            //        };
            //        dbcontext.Notifications.Add(objNot);
            //        dbcontext.SaveChanges();
            //    }
            //}
            //else
            //{

            //    Notification objNotification = new Notification
            //    {
            //        Name = obj.Name,
            //        TodoId = obj.TodoId,
            //        CreatedBy = Userid,
            //        CreatedDate = DateTime.Now.ToUniversalTime(),
            //        NotifyingUserId = obj.NotifyingUserId,
            //        IsRead = false

            //    };

            //    dbcontext.Notifications.Add(objNotification);
            //    dbcontext.SaveChanges();

            //}


            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("usp_InsertNotification", new { Flag = obj.Flag , TodoId = obj.TodoId, Name = obj.Name, Userid = Userid, NotifyingUserId = obj.NotifyingUserId }, commandType: System.Data.CommandType.StoredProcedure);
            }

        }

        public void UpdateNotification(int NotificationId)
        {

            //Notification objNotification = (from m in dbcontext.Notifications where m.Id == NotificationId select m).SingleOrDefault();

            //objNotification.IsRead = true;

            //dbcontext.SaveChanges();

            
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update Notifications set IsRead = 1 where Id = @NotificationId";
                con.Execute(sqlquery, new { NotificationId = NotificationId });
            }

        }

        public string resetNotification(string UserId)
        {
            //    (from notification in dbcontext.Notifications
            //     where notification.NotifyingUserId == UserId
            //     select notification).ToList().ForEach(n =>n.IsRead = true);

            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update Notifications set IsRead = 1 where NotifyingUserId = @UserId";
                con.Execute(sqlquery, new { UserId = UserId });
            }

            return "";
        }


    }
}
