﻿using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.UTILITIES;
using System.Threading;
using System.Web;
using System.Xml.Linq;
using System.IO;
using System.Data.Entity;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using static TIGHTEN.MODEL.ProjectModel;
using TIGHTEN.MODEL.Vendor;

namespace TIGHTEN.DATA
{
    public static class Extensions
    {
        /// <summary>
        /// This extension converts an enumerable set to a Dapper TVP
        /// </summary>
        /// <typeparam name="T">type of enumerbale</typeparam>
        /// <param name="enumerable">list of values</param>
        /// <param name="typeName">database type name</param>
        /// <param name="orderedColumnNames">if more than one column in a TVP, 
        /// columns order must mtach order of columns in TVP</param>
        /// <returns>a custom query parameter</returns>
        public static SqlMapper.ICustomQueryParameter AsTableValuedParameter<T>(this IEnumerable<T> enumerable, string typeName, IEnumerable<string> orderedColumnNames = null)
        {
            var dataTable = new DataTable();
            if (typeof(T).IsValueType || typeof(T).FullName.Equals("System.String"))
            {
                dataTable.Columns.Add(orderedColumnNames == null ?
                    "NONAME" : orderedColumnNames.First(), typeof(T));
                foreach (T obj in enumerable)
                {
                    dataTable.Rows.Add(obj);
                }
            }
            else
            {
                PropertyInfo[] properties = typeof(T).GetProperties
                    (BindingFlags.Public | BindingFlags.Instance);
                PropertyInfo[] readableProperties = properties.Where
                    (w => w.CanRead).ToArray();
                if (readableProperties.Length > 1 && orderedColumnNames == null)
                    throw new ArgumentException("Ordered list of column names must be provided when TVP contains more than one column");

                var columnNames = (orderedColumnNames ??
                    readableProperties.Select(s => s.Name)).ToArray();
                foreach (string name in columnNames)
                {
                    dataTable.Columns.Add(name, readableProperties.Single
                        (s => s.Name.Equals(name)).PropertyType);
                }

                foreach (T obj in enumerable)
                {
                    dataTable.Rows.Add(
                        columnNames.Select(s => readableProperties.Single
                            (s2 => s2.Name.Equals(s)).GetValue(obj))
                            .ToArray());
                }
            }
            return dataTable.AsTableValuedParameter(typeName);
        }
    }


    public class Projects : BaseClass
    {
        AppContext dbcontext;

        /// <summary>
        /// Getting User's Teams with projects in which user is Team Member and Project Member For MyTodos Section
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>List of User's Teams with Projects in it as an array</returns>
        public List<MyTodosModel.Teams> getMyProjectsWithTeams(string UserId)
        {
            dbcontext = new AppContext();
            var myTodosResult = (from m in dbcontext.Teams
                                 join m1 in dbcontext.TeamUsers on m.Id equals m1.TeamId
                                 where m1.UserId == UserId && m.IsDeleted == false
                                 select new
                                 {
                                     TeamId = m.Id,
                                     TeamName = m.TeamName,
                                     Projects = from p in dbcontext.Projects
                                                join p1 in dbcontext.ProjectUsers on p.Id equals p1.ProjectId

                                                where p1.UserId == UserId && p.TeamId == m.Id && p.IsDeleted == false
                                                select new MyTodosModel.Projects
                                                {
                                                    ProjectId = p.Id,
                                                    ProjectName = p.Name,
                                                }
                                 });

            List<MyTodosModel.Teams> myTodos = myTodosResult.AsEnumerable().Select(item =>
                                                           new MyTodosModel.Teams()
                                                           {
                                                               TeamId = item.TeamId,
                                                               TeamName = item.TeamName,
                                                               Projects = item.Projects.ToList()
                                                           }).ToList();
            return myTodos;
        }

        /// <summary>
        /// Getting List of Projects for a particular Team with its Project Members in which The user is a project Member
        /// </summary>
        /// <param name="userId">It is the unique id of logged in user</param>
        /// <param name="TeamId">It is the unique id of Team</param>
        /// <returns>returns the list of projects with projects members in it as an array</returns>
        public List<ProjectModel.ProjectWithMembers> getProjects(String userId, int TeamId, int CompanyId, bool IsFreelancer)
        {
            dbcontext = new AppContext();

            //var team = (from t1 in dbcontext.Teams
            //            where t1.Id == TeamId && t1.IsDeleted == false
            //            select t1).FirstOrDefault();

            TIGHTEN.ENTITY.Team team;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from Teams t1  where t1.Id = @TeamId and t1.IsDeleted = 0";
                team = con.Query<TIGHTEN.ENTITY.Team>(sqlquery, new { TeamId = TeamId }).FirstOrDefault();
            }

            if (team != null)
            {
                if (IsFreelancer && team.IsDefault == true)
                {
                    CompanyId = 0;
                }
            }

            //var GetUserRole = (from usr in dbcontext.UserDetails
            //                   join usrcmpnyrole in dbcontext.UserCompanyRoles on usr.UserId equals usrcmpnyrole.UserId
            //                   where usr.UserId == userId && usr.IsDeleted == false
            //                   && (CompanyId == 0 ? usrcmpnyrole.CompanyId == usrcmpnyrole.CompanyId : usrcmpnyrole.CompanyId == CompanyId)
            //                   select usrcmpnyrole.RoleId).FirstOrDefault();

            int UserRole = 0;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select usrcmpnyrole.RoleId from UserDetails usr inner join UserCompanyRoles as usrcmpnyrole on usr.UserId = usrcmpnyrole.UserId where usr.UserId = @userId 
                                    and usr.IsDeleted = 0 and usrcmpnyrole.CompanyId = case when @CompanyId = 0 then usrcmpnyrole.CompanyId  else @CompanyId  end ";
                UserRole = con.Query<int>(sqlquery, new { CompanyId = CompanyId, userId = userId }).FirstOrDefault();
            }


            // Getting User's Projects
            List<ProjectModel.ProjectWithMembers> projectMod;
            using (var con = new SqlConnection(ConnectionString))
            {
                var projects = con.QueryMultiple("usp_GetProjects", new { userId = userId, CompanyId = CompanyId, TeamId = TeamId, UserRole = UserRole }, commandType: CommandType.StoredProcedure);

                projectMod = projects.Read<ProjectModel.ProjectWithMembers>().ToList();
                if (projectMod != null)
                {
                    var ProjectMembers = projects.Read<ProjectModel.ProjectMembers>().ToList();

                    foreach (var item in projectMod)
                    {
                        item.ProjectMembers = ProjectMembers.Where(x => x.ProjectId == item.ProjectId).ToList();
                    }
                }
            }


            //// to list inside this is giving error so I have returened the anonymous type and then convert it into particular type
            //var projectQueryResult = (from data in
            //                         (from m1 in dbcontext.Projects
            //                          join n1 in dbcontext.ProjectUsers on m1.Id equals n1.ProjectId
            //                          where (GetUserRole == 1 ? n1.UserId == n1.UserId : (n1.UserId == userId || m1.CreatedBy == userId))
            //                          && (TeamId == 0 ? m1.TeamId == m1.TeamId : m1.TeamId == TeamId)
            //                          && (CompanyId == 0 ? m1.CompanyId == m1.CompanyId : m1.CompanyId == CompanyId)
            //                          && m1.IsDeleted == false
            //                          orderby m1.CreatedDate descending
            //                          select new { m1, n1 }
            //                          )
            //                          group data by new { data.m1.Id, data.m1.Name, data.m1.Description, data.m1.CreatedDate } into GroupedData
            //                          select new
            //                          {
            //                              ProjectName = GroupedData.Key.Name,
            //                              ProjectDescription = GroupedData.Key.Description,
            //                              ProjectId = GroupedData.Key.Id,
            //                              CreatedDate = GroupedData.Key.CreatedDate,
            //                              ProjectMembers = from m3 in dbcontext.ProjectUsers
            //                                               join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
            //                                               join usrcmpnyrole in dbcontext.UserCompanyRoles on m2.UserId equals usrcmpnyrole.UserId
            //                                               join usrrole in dbcontext.Roles on usrcmpnyrole.RoleId equals usrrole.RoleId
            //                                               where m3.ProjectId == GroupedData.Key.Id && m2.IsDeleted == false
            //                                               && (CompanyId == 0 ? usrcmpnyrole.CompanyId == usrcmpnyrole.CompanyId : usrcmpnyrole.CompanyId == CompanyId)
            //                                               && usrrole.IsDeleted == false  //&& (GetUserRole == 3 ? m2.UserId == userId : m2.UserId == m2.UserId )
            //                                               select new ProjectModel.ProjectMembers
            //                                               {
            //                                                   MemberEmail = m2.Email,
            //                                                   MemberName = m2.LastName == null ? m2.FirstName : m2.FirstName + " " + m2.LastName,
            //                                                   MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                   MemberUserId = m2.UserId,
            //                                                   MemberPhoneNo = m2.PhoneNo,
            //                                                   MemberDesignation = usrrole.Name,
            //                                                   Value = m2.UserId,
            //                                                   Text = m2.FirstName + " ( " + m2.Email + " )"
            //                                               }
            //                          });




            //// converting the anonymous type to the user defined type                  
            //List<ProjectModel.ProjectWithMembers> projectMod = projectQueryResult.AsEnumerable().Select(item =>
            //                                             new ProjectModel.ProjectWithMembers()
            //                                             {
            //                                                 ProjectId = item.ProjectId,
            //                                                 ProjectName = item.ProjectName,
            //                                                 CreatedDate = item.CreatedDate,
            //                                                 ProjectMembers = item.ProjectMembers.ToList()
            //                                             }).ToList();


            //if (GetUserRole == 1)
            //{
            //    List<int> ProjectIdList = new List<int>();

            //    foreach (var item in projectMod)
            //    {
            //        ProjectIdList.Add(item.ProjectId);
            //    }

            //    var compModList_ForAdmin = (from Gdata in
            //                               (from m1 in dbcontext.Projects
            //                                where !ProjectIdList.Contains(m1.Id)
            //                                && (TeamId == 0 ? m1.TeamId == m1.TeamId : m1.TeamId == TeamId)
            //                                && (CompanyId == 0 ? m1.CompanyId == m1.CompanyId : m1.CompanyId == CompanyId)
            //                                && m1.IsDeleted == false
            //                                orderby m1.CreatedDate descending
            //                                select new { m1 }
            //                                )

            //                                group Gdata by new { Gdata.m1.Id, Gdata.m1.Name, Gdata.m1.Description, Gdata.m1.CreatedDate } into GroupedData
            //                                select new

            //                                {
            //                                    ProjectName = GroupedData.Key.Name,
            //                                    ProjectDescription = GroupedData.Key.Description,
            //                                    ProjectId = GroupedData.Key.Id,
            //                                    CreatedDate = GroupedData.Key.CreatedDate,
            //                                    ProjectMembers = from m3 in dbcontext.ProjectUsers
            //                                                     join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
            //                                                     join usrcmpnyrole in dbcontext.UserCompanyRoles on m2.UserId equals usrcmpnyrole.UserId
            //                                                     join usrrole in dbcontext.Roles on usrcmpnyrole.RoleId equals usrrole.RoleId
            //                                                     where m3.ProjectId == GroupedData.Key.Id && m2.IsDeleted == false
            //                                                     && (CompanyId == 0 ? usrcmpnyrole.CompanyId == usrcmpnyrole.CompanyId : usrcmpnyrole.CompanyId == CompanyId)
            //                                                     && usrrole.IsDeleted == false  //&& (GetUserRole == 3 ? m2.UserId == userId : m2.UserId == m2.UserId )
            //                                                     select new ProjectModel.ProjectMembers
            //                                                     {
            //                                                         MemberEmail = m2.Email,
            //                                                         MemberName = m2.FirstName,
            //                                                         MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                         MemberUserId = m2.UserId,
            //                                                         MemberPhoneNo = m2.PhoneNo,
            //                                                         MemberDesignation = usrrole.Name,
            //                                                         Value = m2.UserId,
            //                                                         Text = m2.FirstName + " ( " + m2.Email + " )"
            //                                                     }
            //                                });

            //    // converting the anonymous type to the user defined type                  
            //    List<ProjectModel.ProjectWithMembers> compMod_ForAdmin = compModList_ForAdmin.AsEnumerable().Select(item =>
            //                                                          new ProjectModel.ProjectWithMembers()
            //                                                          {
            //                                                              ProjectId = item.ProjectId,
            //                                                              ProjectName = item.ProjectName,
            //                                                              CreatedDate = item.CreatedDate,
            //                                                              ProjectMembers = item.ProjectMembers.ToList()
            //                                                          }).ToList();


            //    foreach (var item in compMod_ForAdmin)
            //    {
            //        projectMod.Add(item);
            //    }

            //    projectMod.OrderBy(x => x.CreatedDate);

            //}


            return projectMod;
        }

        /// <summary>
        /// Getting User Project By Specific ProjectId
        /// </summary>
        /// <param name="ProjectId">It is unique id of Project</param>
        /// <returns>  Single  Project Model  </returns>
        public ProjectModel.ProjectWithMembers GetProject(int ProjectId)
        {
            //dbcontext = new AppContext();
            //var projectModResult = (from m1 in dbcontext.Projects
            //                        where m1.Id == ProjectId && m1.IsDeleted == false
            //                        select new
            //                        {
            //                            ProjectId = m1.Id,
            //                            ProjectName = m1.Name,
            //                            ProjectDescription = m1.Description,
            //                            Accesslevel = m1.AccessLevel.HasValue ? m1.AccessLevel.Value : 0,
            //                            EstimatedHours = m1.EstimatedHours.HasValue ? m1.EstimatedHours.Value : 0,
            //                            EstimatedBudget = m1.EstimatedBudget.HasValue ? m1.EstimatedBudget.Value : 0,
            //                            FileName = m1.FileName,
            //                            ActualFileName = m1.ActualFileName,
            //                            FilePath = m1.FilePath == null ? "Uploads/Default/profile.png" : "Uploads/Attachments/Mail_Attachments/" + m1.FilePath,
            //                            AttachmentDescription = m1.AttachmentDescription,

            //                            ProjectMembers = (from m3 in dbcontext.ProjectUsers
            //                                              join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
            //                                              where m3.ProjectId == m1.Id && m2.IsDeleted == false
            //                                              select new ProjectModel.ProjectMembers
            //                                              {
            //                                                  MemberEmail = m2.Email,
            //                                                  MemberName = m2.FirstName,
            //                                                  MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                  MemberUserId = m2.UserId,
            //                                                  Rate = m3.UserRate.HasValue ? m3.UserRate.Value : 0,
            //                                                  IsFreelancer = m2.IsFreelancer
            //                                              })


            //                        });




            //ProjectModel.ProjectWithMembers projectMod = projectModResult.AsEnumerable().Select(item =>
            //                                               new ProjectModel.ProjectWithMembers()
            //                                               {
            //                                                   ProjectId = item.ProjectId,
            //                                                   ProjectName = item.ProjectName,
            //                                                   ProjectDescription = item.ProjectDescription,
            //                                                   Accesslevel = item.Accesslevel,
            //                                                   projectHours = item.EstimatedHours,
            //                                                   projectBudget = item.EstimatedBudget,
            //                                                   FileName = item.FileName,
            //                                                   ActualFileName = item.ActualFileName,
            //                                                   FilePath = item.FilePath,
            //                                                   AttachmentDescription = item.AttachmentDescription,
            //                                                   ProjectMembers = item.ProjectMembers.ToList()
            //                                               }).SingleOrDefault();


            ProjectModel.ProjectWithMembers projectMod;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetProject", new { ProjectId = ProjectId }, commandType: CommandType.StoredProcedure);
                projectMod = result.Read<ProjectModel.ProjectWithMembers>().SingleOrDefault();

                if (projectMod != null)
                {
                    var Members = result.Read<ProjectModel.ProjectMembers>().ToList();

                    projectMod.ProjectMembers = Members.Where(x => x.ProjectId == projectMod.ProjectId).ToList();
                }
            }


            return projectMod;
        }

        /// <summary>
        /// Getting All Projects in which User is involved/ProjectMember irrespective of Teams
        /// </summary>
        /// <param name="userId">It is unique id of user</param>
        /// <returns>List of Projects in which User is project Member</returns>
        public List<ProjectModel.ProjectWithMembers> getUserProjects(string userId, int CompanyId)
        {
            dbcontext = new AppContext();

            var user = (from usr in dbcontext.UserCompanyRoles
                        where usr.UserId == userId
                        && usr.CompanyId == CompanyId
                        select usr).SingleOrDefault();

            var projectModResult = (from data in
                                   (from m1 in dbcontext.Projects
                                    join m in dbcontext.ProjectUsers on m1.Id equals m.ProjectId
                                    where
                                    //&& m1.CompanyId == CompanyId
                                    (CompanyId == 0 ? m1.CompanyId == m1.CompanyId : m1.CompanyId == CompanyId)
                                    && (user.RoleId == 1 || user.RoleId == 2 ? m.UserId == m.UserId : m.UserId == userId)
                                     && m1.IsDeleted == false
                                    orderby m1.CreatedDate descending
                                    select new { m1 }
                                    )
                                    group data by new { data.m1.Id, data.m1.Name, data.m1.Description } into GroupedData

                                    select new
                                    {
                                        ProjectId = GroupedData.Key.Id,
                                        ProjectName = GroupedData.Key.Name,
                                        ProjectDescription = GroupedData.Key.Description,
                                        ProjectMembers = (from m3 in dbcontext.ProjectUsers
                                                          join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
                                                          where m3.ProjectId == GroupedData.Key.Id && m2.IsDeleted == false
                                                          select new ProjectModel.ProjectMembers
                                                          {
                                                              MemberEmail = m2.Email,
                                                              MemberName = m2.FirstName,
                                                              MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
                                                              MemberUserId = m2.UserId
                                                          })
                                    });
            List<ProjectModel.ProjectWithMembers> projectMod = projectModResult.AsEnumerable().Select(item =>
                                                           new ProjectModel.ProjectWithMembers()
                                                           {
                                                               ProjectId = item.ProjectId,
                                                               ProjectName = item.ProjectName,
                                                               ProjectDescription = item.ProjectDescription,
                                                               ProjectMembers = item.ProjectMembers.ToList()
                                                           }).ToList();
            return projectMod;
        }


        /// <summary>
        /// Getting All Projects in which User is involved/ProjectMember irrespective of Teams
        /// </summary>
        /// <param name="userId">It is unique id of user</param>
        /// <returns>List of Projects in which User is project Member</returns>
        public List<ProjectModel.ProjectWithMembers> GetProjectsForSelectedTeam(int TeamId, int CompanyId)
        {
            dbcontext = new AppContext();

            List<ProjectModel.ProjectWithMembers> projectModResult = (from prjct in dbcontext.Projects
                                                                      where prjct.TeamId == (TeamId == 0 ? prjct.TeamId : TeamId)
                                                                      && prjct.CompanyId == CompanyId && prjct.IsDeleted == false
                                                                      select new ProjectModel.ProjectWithMembers
                                                                      {
                                                                          ProjectId = prjct.Id,
                                                                          ProjectName = prjct.Name
                                                                      }
                                                                       ).ToList();


            return projectModResult;
        }


        /// <summary>
        /// Getting Projects with all project members of a particular Team that have been Created By User i.e owned by User
        /// </summary>
        /// <param name="userId">It is unique Id of User</param>
        /// <param name="teamId">It is unique Id of Team</param>
        /// <returns>List of Projects with ProjectMembers in it as an array</returns>
        public List<ProjectModel.ProjectWithMembers> getUserOwnedProjects(string userId, int teamId, int RoleId, int CompanyId)
        {
            TIGHTEN.ENTITY.Team team;
            bool isUserFreelancer;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_isUserFreelancer = @"select usr.IsFreelancer from UserDetails usr where usr.UserId = @userId and usr.IsDeleted = 0 
                                                     select * from Teams m1 where m1.Id = @teamId and m1.IsDeleted = 0 ";
                var result = con.QueryMultiple(sqlquery_isUserFreelancer, new { userId = userId, teamId = teamId });

                isUserFreelancer = result.Read<bool>().SingleOrDefault();
                team = result.Read<TIGHTEN.ENTITY.Team>().SingleOrDefault();
            }

            List<ProjectModel.ProjectWithMembers> projectMod;
            if (isUserFreelancer && team.IsDefault)
            {
                CompanyId = 0;
            }


            using (var con = new SqlConnection(ConnectionString))
            {
                var resullt = con.QueryMultiple("usp_GetUserOwnedProjects", new { RoleId = RoleId, teamId = teamId, CompanyId = CompanyId, userId = userId }, commandType: CommandType.StoredProcedure);

                projectMod = resullt.Read<ProjectModel.ProjectWithMembers>().ToList();

                if (projectMod != null)
                {
                    var members = resullt.Read<ProjectModel.ProjectMembers>().ToList();

                    foreach (var item in projectMod)
                    {
                        item.ProjectMembers = members.Where(x => x.ProjectId == item.ProjectId).ToList();
                    }
                }

            }



            // This need to be changed after some time ,  this is a temporary fix
            // Here we have to find some other method to find Total Hours 

            #region Changable Data

            foreach (var prjct in projectMod)
            {
                int TotalMinutesForProject = 0;
                decimal totalamtForProject = 0;

                foreach (var member in prjct.ProjectMembers)
                {
                    //var todosForProjectMembers = (from t in dbcontext.ToDos
                    //                              join s in dbcontext.Sections on t.SectionId equals s.Id
                    //                              join u in dbcontext.UserDetails on t.AssigneeId equals u.UserId
                    //                              where t.AssigneeId == member.MemberUserId && u.IsDeleted == false
                    //                              && s.ProjectId == prjct.ProjectId && t.IsDeleted == false
                    //                              select new
                    //                              {
                    //                                  LoggedTime = t.LoggedTime
                    //                              }).ToList();

                    string[] todosForProjectMembers;
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlquery = @"select t.LoggedTime from ToDoes t inner join Sections s on t.SectionId = s.Id inner join UserDetails u on t.AssigneeId = u.UserId 
                                            where t.AssigneeId = '" + member.MemberUserId + "' and u.IsDeleted = 0 ";

                        todosForProjectMembers = con.Query<string>(sqlquery).ToArray();
                    }

                    int TotalMinutesForMember = 0;
                    decimal totalamtForMember = 0;

                    foreach (var item in todosForProjectMembers)
                    {
                        int LoggedTimeInMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(item);
                        TotalMinutesForMember += LoggedTimeInMinutes;
                    }

                    int TotalHoursForMember = (TotalMinutesForMember / 60);
                    totalamtForMember = (TotalHoursForMember * member.Rate);

                    int RestTotalMinutesForMember = (TotalMinutesForMember % 60);
                    if (RestTotalMinutesForMember > 0)
                    {
                        decimal totalAmtForMinutes = (Convert.ToDecimal(RestTotalMinutesForMember / 60.00) * member.Rate);
                        totalamtForMember = totalamtForMember + totalAmtForMinutes;
                    }


                    TotalMinutesForProject += TotalMinutesForMember;
                    totalamtForProject += totalamtForMember;

                }

                int TotalHoursForProject = (TotalMinutesForProject / 60);
                //totalamtForProject = (TotalHoursForProject * rate);

                int RestTotalMinutesForProject = (TotalMinutesForProject % 60);
                //if (RestTotalMinutesForProject > 0)
                //{
                //    decimal totalAmtForMinutesForProject = (Convert.ToDecimal(RestTotalMinutesForProject / 60.00) * rate);
                //    totalamtForProject = totalamtForProject + totalAmtForMinutesForProject;
                //}


                prjct.TotalSpentHours = TotalHoursForProject + "h " + RestTotalMinutesForProject + "m";
                prjct.TotalSpentMoney = Math.Round(totalamtForProject, 2);


            }

            #endregion

            return projectMod;
        }

        /// <summary>
        /// It saves the new Project with Project Members
        /// </summary>
        /// <param name="UserId">It is unique Id of User who is saving the new Project</param>
        /// <param name="model">Model consists the new values of project and Project members who will be accessing this Project</param>
        public string saveProject(string url, String UserId, ProjectModel.Project model, int companyId)
        {
            string msg = string.Empty;
            //dbcontext = new AppContext();

            //var IsUserFreelancer = (from u in dbcontext.UserDetails
            //                        where u.UserId == UserId && u.IsDeleted == false
            //                        select u).FirstOrDefault();

            //var team = (from m1 in dbcontext.Teams
            //            where m1.Id == model.TeamId && m1.IsDeleted == false
            //            select m1).SingleOrDefault();

            //if (IsUserFreelancer.IsFreelancer && team.IsDefault == true)
            //{
            //    companyId = 0;
            //}

            //var teamBudget = team.TeamBudget;

            //var projectsBudget = dbcontext.Projects.Where(x => x.CompanyId == companyId && x.TeamId == model.TeamId && x.IsDeleted == false).Select(x => x.EstimatedBudget).Sum();
            //var totalprojectsBudget = (projectsBudget == null ? 0 : projectsBudget) + model.projectBudget;
            ////  CompanyId is passed from parameters
            ////int companyId = Convert.ToInt32((from u in dbcontext.UserDetails
            ////                                 where u.UserId == UserId
            ////                                 select u.CompanyId).SingleOrDefault());

            ///*   checking project budget still under team budget or not     */
            //if (teamBudget >= totalprojectsBudget || totalprojectsBudget == null)
            //{
            //    var isProjectExist = (from prjct in dbcontext.Projects
            //                          where prjct.Name == model.ProjectName
            //                          && prjct.CompanyId == companyId && prjct.IsDeleted == false
            //                          && prjct.TeamId == model.TeamId
            //                          select prjct.Id).SingleOrDefault();

            //    /*   check wheather project is not duplicate within team   */
            //    if (isProjectExist < 1)
            //    {
            //        Project projectEntity = new Project
            //        {
            //            AccessLevel = model.AccessLevel,
            //            CompanyId = companyId,
            //            Name = model.ProjectName,
            //            Description = model.ProjectDescription,
            //            EstimatedHours = model.projectHours,
            //            EstimatedBudget = model.projectBudget,
            //            CreatedBy = UserId,
            //            CreatedDate = DateTime.Now,
            //            TeamId = model.TeamId,
            //            IsComplete = false,
            //        };

            //        if (model.TempData != null)
            //        {
            //            projectEntity.FileName = model.TempData.FileName;
            //            projectEntity.ActualFileName = model.TempData.ActualFileName;
            //            projectEntity.FilePath = model.TempData.FilePath;
            //            projectEntity.AttachmentDescription = model.TempData.AttachmentDescription;
            //        }


            //        // Save Project Details */
            //        dbcontext.Projects.Add(projectEntity);
            //        dbcontext.SaveChanges();

            //        Section sectionEntity = new Section
            //        {
            //            SectionName = "No Section",
            //            ProjectId = projectEntity.Id,
            //            CreatedBy = UserId,
            //            CreatedDate = DateTime.Now,
            //            ModifiedDate = DateTime.Now,
            //            ModifiedBy = UserId,
            //            IsDefault = true
            //        };
            //        dbcontext.Sections.Add(sectionEntity);
            //        dbcontext.SaveChanges();








            //        #region New Add Project Method For Internal Users
            //        /* if ProjectMembers exist */
            //        /* loop through all ProjectMembers in internalUserDetails to save necessary details regarding new project */
            //        foreach (var internalUserItem in model.internalUserDetails)
            //        {
            //            string Members_UserId = internalUserItem.UserId;
            //            bool SaveNotification = false;

            //            ProjectUser projectUsersEntity = new ProjectUser
            //            {
            //                UserId = Members_UserId,
            //                CreatedBy = UserId,
            //                CreatedDate = DateTime.Now,
            //                ProjectId = projectEntity.Id,
            //                RoleId = internalUserItem.RoleId
            //            };

            //            dbcontext.ProjectUsers.Add(projectUsersEntity);
            //            dbcontext.SaveChanges();

            //            if (projectUsersEntity.Id > 0)
            //            {
            //                SaveNotification = true;
            //            }


            //            if (SaveNotification == true)
            //            {
            //                /* Saving notification for newly added Team for team members */
            //                Notification objNotification = new Notification
            //                {
            //                    Name = "has created a New Project:",
            //                    CreatedBy = UserId,
            //                    CreatedDate = DateTime.Now,
            //                    IsRead = false,
            //                    Type = 2,
            //                    NotifyingUserId = Members_UserId,
            //                    ProjectId = projectEntity.Id
            //                };

            //                dbcontext.Notifications.Add(objNotification);
            //                dbcontext.SaveChanges();

            //                #region Adding ProjectId in UserSettingConfig for each project member

            //                /* getting all Settings for the user  */

            //                UserSettingConfig usrsetting = (from usrset in dbcontext.UserSettingConfigs
            //                                                join set in dbcontext.SettingConfigs on usrset.NotificationId equals set.Id
            //                                                where set.IsActive == true
            //                                                && usrset.CompanyId == companyId
            //                                                && usrset.UserId == Members_UserId
            //                                                && set.NotificationName == "Project Update"
            //                                                select usrset).FirstOrDefault();

            //                if (usrsetting != null)
            //                {
            //                    if (usrsetting.ProjectId != null && usrsetting.ProjectId != string.Empty)
            //                    {
            //                        usrsetting.ProjectId += "," + projectEntity.Id.ToString();
            //                    }
            //                    else
            //                    {
            //                        usrsetting.ProjectId = projectEntity.Id.ToString();
            //                    }

            //                    dbcontext.SaveChanges();
            //                }
            //                #endregion
            //            }
            //        }


            //        #endregion

            //        #region New Add Project Method For Freelancers

            //        /* loop through all ProjectMembers which are freelancers to save necessary details regarding new project */
            //        foreach (var item in model.freelancerDetails)
            //        { 
            //            string Members_UserId = item.FreelancerUserId;
            //            bool SaveNotification = false;

            //            if (team.IsDefault)
            //            {
            //                ProjectUser projectUsersEntity = new ProjectUser
            //                {
            //                    UserId = Members_UserId,
            //                    CreatedBy = UserId,
            //                    CreatedDate = DateTime.Now,
            //                    ProjectId = projectEntity.Id,
            //                    RoleId = item.FreelancerRoleId
            //                };

            //                dbcontext.ProjectUsers.Add(projectUsersEntity);
            //                dbcontext.SaveChanges();

            //                if (projectUsersEntity.Id > 0)
            //                {
            //                    SaveNotification = true;
            //                }
            //            }

            //            //var freelancerdata = (from fusr in dbcontext.UserDetails
            //            //                      join usrcm in dbcontext.UserCompanyRoles on fusr.UserId equals usrcm.UserId
            //            //                      join rol in dbcontext.Roles on usrcm.RoleId equals rol.RoleId
            //            //                      where fusr.UserId == item.FreelancerUserId
            //            //                      select new
            //            //                      {
            //            //                          Email = fusr.Email,
            //            //                          RoleId = rol.RoleId
            //            //                      }).FirstOrDefault();

            //            FreelancerInvitation FreelancerInv = (from freelncr in dbcontext.FreelancerInvitations
            //                                                  where freelncr.FreelancerEmail == item.FreelancerEmail
            //                                                  && freelncr.CompanyId == companyId
            //                                                  && freelncr.ProjectId == projectEntity.Id
            //                                                  && freelncr.IskeyActive == true
            //                                                  select freelncr).SingleOrDefault();

            //            if (FreelancerInv == null)
            //            {
            //                string key = UTILITIES.EmailUtility.GetUniqueKey(16);

            //                FreelancerInvitation obj = new FreelancerInvitation
            //                {
            //                    CompanyId = companyId,
            //                    SentBy = UserId,
            //                    ProjectId = projectEntity.Id,
            //                    RoleId = item.FreelancerRoleId,
            //                    Activationkey = key,
            //                    IskeyActive = true,
            //                    SentDate = DateTime.Now,
            //                    IsAccepted = false,
            //                    FreelancerEmail = item.FreelancerEmail,
            //                    //WeeklyLimit = model.WeeklyLimit,
            //                    IsJobInvitation = false,
            //                    FreelancerRate = item.FreelancerOfferedPrice,
            //                    //FileName = item.FileName,
            //                    //ActualFileName = item.ActualFileName,
            //                    //FilePath = item.FilePath,
            //                    //AttachmentDescription = model.TempData.AttachmentDescription,
            //                    Responsibilities = item.FreelancerResponsibilities,

            //                };

            //                dbcontext.FreelancerInvitations.Add(obj);

            //                //foreach (var freelncritem in item.AllAttachments)
            //                //{
            //                //    ProjectInvitationAttachment invObj = new ProjectInvitationAttachment
            //                //    {
            //                //        UserId = item.FreelancerUserId,
            //                //        ProjectId = projectEntity.Id,
            //                //        ActualFileName = freelncritem.ActualFileName,
            //                //        FileName = freelncritem.FileName,
            //                //        FilePath = freelncritem.FilePath,
            //                //        CreatedBy = UserId,
            //                //        CreatedDate = DateTime.Now
            //                //    };
            //                //    dbcontext.ProjectInvitationAttachments.Add(invObj);
            //                //}

            //                //dbcontext.SaveChanges();

            //                //if (obj.Id > 0)
            //                //{
            //                //    item.Key = key;
            //                //    SaveNotification = true;
            //                //}
            //            }


            //            //if (SaveNotification == true)
            //            //{
            //            //    /* Saving notification for newly added Team for team members */
            //            //    Notification objNotification = new Notification
            //            //    {
            //            //        Name = "has Invited you for project :",
            //            //        CreatedBy = UserId,
            //            //        CreatedDate = DateTime.Now,
            //            //        IsRead = false,
            //            //        Type = 10,
            //            //        NotifyingUserId = Members_UserId,
            //            //        ProjectId = projectEntity.Id
            //            //    };

            //            //    dbcontext.Notifications.Add(objNotification);
            //            //    dbcontext.SaveChanges();
            //            //}
            //        }





            //        #endregion


            //        #region Sending Mail

            //        if (team.IsDefault != true)
            //        {
            //            HttpContext ctx = HttpContext.Current;
            //            Thread childref = new Thread(new ThreadStart(() =>
            //            {
            //                HttpContext.Current = ctx;
            //                ThreadForSaveProject(UserId, model, companyId, projectEntity.Id, url);
            //            }
            //            ));
            //            childref.Start();

            //        }

            //        #endregion


            //        msg = "ProjectAddedSuccess";
            //    }
            //    else
            //    {
            //        msg = "DuplicateProject";
            //    }
            //}
            //else
            //{
            //    msg = "TeamProjectsBudgetExceed";
            //}





            using (var con = new SqlConnection(ConnectionString))
            {
                bool IsDataInTemp = model.TempData == null ? false : true;
                string AllInternalUsers = string.Join(",", model.internalUserDetails.Select(x => x.UserId));


                msg = con.Query<string>("usp_SaveProject", new
                {
                    UserId = UserId,
                    TeamId = model.TeamId,
                    CompanyId = companyId,
                    projectBudget = model.projectBudget,
                    ProjectName = model.ProjectName,
                    AccessLevel = model.AccessLevel,
                    ProjectDescription = model.ProjectDescription,
                    projectHours = model.projectHours,
                    IsDataInTemp = IsDataInTemp,
                    FileName = model.TempData == null ? null : model.TempData.FileName,
                    ActualFileName = model.TempData == null ? null : model.TempData.ActualFileName,
                    FilePath = model.TempData == null ? null : model.TempData.FilePath,
                    AttachmentDescription = model.TempData == null ? null : model.TempData.AttachmentDescription,
                    AllInternalUsers = AllInternalUsers
                }, commandType: CommandType.StoredProcedure

                ).FirstOrDefault();

                if (msg != "DuplicateProject" && msg != "TeamProjectsBudgetExceed")
                {
                    int CurrentProjectId = Convert.ToInt32(msg);

                    // Inserting internal Users in below section
                    List<ProjectUser> ProjectUserList = model.internalUserDetails.AsEnumerable().Select(x =>
                                                         new ProjectUser()
                                                         {
                                                             UserId = x.UserId,
                                                             CreatedBy = UserId,
                                                             CreatedDate = DateTime.Now,
                                                             ProjectId = CurrentProjectId,
                                                             RoleId = x.RoleId
                                                         }
                                                            ).ToList();

                    string sqlquery_internalUser = @"insert into ProjectUsers (UserId,CreatedBy,CreatedDate,ProjectId,RoleId ) values(@UserId,@CreatedBy,@CreatedDate,@ProjectId,@RoleId) ";
                    con.Execute(sqlquery_internalUser, ProjectUserList);


                    // Inserting Freelancers in below section

                    List<ProjectModel.FreelancerToInsertInProjectModel> freelancerDetails = model.freelancerDetails.AsEnumerable().Select(x =>
                                                                                              new ProjectModel.FreelancerToInsertInProjectModel()
                                                                                              {
                                                                                                  FreelancerUserId = x.FreelancerUserId,
                                                                                                  UserId = UserId,
                                                                                                  ProjectId = CurrentProjectId,
                                                                                                  FreelancerRoleId = x.FreelancerRoleId,
                                                                                                  FreelancerEmail = x.FreelancerEmail,
                                                                                                  companyId = companyId,
                                                                                                  keyy = UTILITIES.EmailUtility.GetUniqueKey(16),
                                                                                                  FreelancerOfferedPrice = x.FreelancerOfferedPrice,
                                                                                                  FreelancerResponsibilities = x.FreelancerResponsibilities,
                                                                                                  TeamId = model.TeamId

                                                                                              }).ToList();

                    con.Execute("usp_InsertFreelancerAfterProjectInsert", new
                    {
                        freelancers = freelancerDetails.AsTableValuedParameter("freelancerDetails", new[] { "FreelancerUserId", "UserId", "ProjectId", "FreelancerRoleId", "FreelancerEmail", "companyId", "keyy", "FreelancerOfferedPrice", "FreelancerResponsibilities", "TeamId" })

                    }, commandType: CommandType.StoredProcedure);



                    List<Notification> Notificationss = model.freelancerDetails.AsEnumerable().Select(x =>
                                                        new Notification()
                                                        {
                                                            Name = "has Invited you for project :",
                                                            CreatedBy = UserId,
                                                            CreatedDate = DateTime.Now,
                                                            IsRead = false,
                                                            Type = 10,
                                                            NotifyingUserId = x.FreelancerUserId,
                                                            ProjectId = CurrentProjectId
                                                        }).ToList();

                    string sqlquery_freelancerNotification = @"insert into Notifications (Name,CreatedBy,CreatedDate,IsRead,Type,NotifyingUserId,ProjectId ) values(@Name,@CreatedBy,@CreatedDate,@IsRead,@Type,@NotifyingUserId,@ProjectId) ";
                    con.Execute(sqlquery_freelancerNotification, Notificationss);


                    List<ProjectInvitationAttachment> invObjList = new List<ProjectInvitationAttachment>();
                    foreach (var freelancerItem in model.freelancerDetails)
                    {
                        foreach (var att in freelancerItem.AllAttachments)
                        {
                            ProjectInvitationAttachment objj = new ProjectInvitationAttachment();
                            objj.UserId = freelancerItem.FreelancerUserId;
                            objj.ProjectId = CurrentProjectId;
                            objj.ActualFileName = att.ActualFileName;
                            objj.FileName = att.FileName;
                            objj.FilePath = att.FilePath;
                            objj.CreatedBy = UserId;
                            objj.CreatedDate = DateTime.Now;

                            invObjList.Add(objj);
                        }
                    }

                    string sqlquery_ProjectInvitationAttachment = @"insert into ProjectInvitationAttachments (UserId,ProjectId,ActualFileName,FileName,FilePath,CreatedBy,CreatedDate ) values(@UserId,@ProjectId,@ActualFileName,@FileName,@FilePath,@CreatedBy,@CreatedDate) ";
                    con.Execute(sqlquery_ProjectInvitationAttachment, invObjList);


                    #region Sending Mail

                    bool IsTeamDefault = false;
                    string sqlquery_IsTeamDefault = @"select IsDefault from Teams where Id = @TeamId and IsDeleted = 0";
                    IsTeamDefault = con.Query<bool>(sqlquery_IsTeamDefault, new { TeamId = model.TeamId }).Single();

                    if (IsTeamDefault == true)
                    {
                        HttpContext ctx = HttpContext.Current;
                        Thread childref = new Thread(new ThreadStart(() =>
                        {
                            HttpContext.Current = ctx;
                            ThreadForSaveProject(UserId, model, companyId, CurrentProjectId, url);
                        }
                        ));
                        childref.Start();

                    }

                    #endregion


                    msg = "ProjectAddedSuccess";
                }

            }




            return msg;
        }



        //Sending mail in thread on Project creation to all project members 
        private void ThreadForSaveProject(String UserId, ProjectModel.Project model, int companyId, int ProjectId, string url)
        {
            #region New for ProjectMembers as Internal Users

            foreach (var item in model.internalUserDetails)
            {
                //var TmMember = (from u in dbcontext.UserDetails
                //                where u.UserId == item.UserId && u.IsDeleted == false
                //                select u).FirstOrDefault();


                //string OnRole = (from r in dbcontext.Roles
                //                 where r.RoleId == item.RoleId
                //                 && r.IsDeleted == false
                //                 select r.Name).FirstOrDefault();

                UserDetail TmMember;
                string OnRole;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"select * from UserDetails u where u.UserId = @UserId and u.IsDeleted = 0 
                                    select r.Name from Roles r where r.RoleId = @RoleId and r.IsDeleted = 0  ";
                    var result = con.QueryMultiple(sqlquery, new { UserId = item.UserId, RoleId = item.RoleId });
                    TmMember = result.Read<UserDetail>().FirstOrDefault();
                    OnRole = result.Read<string>().FirstOrDefault();

                }

                //if (!TmMember.IsFreelancer)
                //{
                bool IsSettingActivated = false;
                /*  This function is used for checking wheather mail setting is activated for assignee or not*/
                if (!string.IsNullOrEmpty(item.UserId))
                {
                    //IsSettingActivated = (from usrSetting in dbcontext.UserSettingConfigs
                    //                      join dfltSet in dbcontext.SettingConfigs on usrSetting.NotificationId equals dfltSet.Id
                    //                      where dfltSet.NotificationName == "Project Creation"
                    //                      && usrSetting.UserId == item.UserId && usrSetting.CompanyId == companyId
                    //                      select usrSetting.IsActive).FirstOrDefault();

                    /*  below procedure is used for checking wheather mail setting is activated for assignee or not*/
                    int SettingActivated;
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        SettingActivated = con.Query<int>("usp_IsUserEmailSettingActivated", new { CompanyId = companyId, UserId = item.UserId, NotificationName = "Project Creation" }, commandType: CommandType.StoredProcedure).Single();
                    }

                    if (SettingActivated == 0)
                        IsSettingActivated = false;
                    else
                        IsSettingActivated = true;

                }

                if (IsSettingActivated)
                {
                    //UserDetail CurrentUser = (from crntusr in dbcontext.UserDetails
                    //                          where crntusr.UserId == UserId && crntusr.IsDeleted == false
                    //                          select crntusr).FirstOrDefault();

                    UserDetail CurrentUser;
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlquery = @"select * from UserDetails crntusr where crntusr.UserId = @UserId and crntusr.IsDeleted = 0 ";
                        CurrentUser = con.Query<UserDetail>(sqlquery, new { UserId = UserId }).FirstOrDefault();
                    }


                    #region Mail-Body
                    String emailbody = null;
                    string Subject = string.Empty;

                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                    var MailModule = from r in xdoc.Descendants("MailModule").
                                     Where(r => (string)r.Attribute("id") == "SaveProject")
                                         //select r;
                                     select new
                                     {
                                         mailbody = r.Element("Body").Value,
                                         subject = r.Element("Subject").Value
                                     };

                    foreach (var Mailitem in MailModule)
                    {
                        emailbody = Mailitem.mailbody;
                        Subject = Mailitem.subject;
                    }

                    emailbody = emailbody.Replace("@@Receiver_FirstName", TmMember.FirstName).Replace("@@ProjectName", model.ProjectName).Replace("@@CreatedBy_FirstName", CurrentUser.FirstName);
                    emailbody = emailbody.Replace("@@RoleName", OnRole);
                    Subject = Subject.Replace("@@ProjectName", model.ProjectName);

                    #endregion

                    //String emailbody = "<p> Hello " + TmMember.FirstName + " <br/>";
                    //emailbody += " This mail is sent to inform you that a new project named as " + model.ProjectName + " has been created by " + CurrentUser.FirstName + "<br/>";
                    //emailbody += " and you are added as a project member for this project <br/>";

                    //EmailUtility.SendMailInThread(TmMember.Email, Subject, emailbody);

                    List<EmailUtility.ProjectFileModel> FilePathList = new List<EmailUtility.ProjectFileModel>();

                    if (model.TempData != null)
                    {
                        if (model.TempData.FilePath != string.Empty && model.TempData.FilePath != null)
                        {
                            EmailUtility.ProjectFileModel obj = new EmailUtility.ProjectFileModel();
                            obj.FilePath = model.TempData.FilePath;
                            obj.ActualFileName = model.TempData.ActualFileName;

                            FilePathList.Add(obj);

                            EmailUtility.SendMailInThreadWithAttachment(TmMember.Email, Subject, emailbody, FilePathList);
                        }
                        else
                        {
                            EmailUtility.SendMailInThread(TmMember.Email, Subject, emailbody);
                        }
                    }
                    else
                    {
                        EmailUtility.SendMailInThread(TmMember.Email, Subject, emailbody);
                    }

                }
            }


            #endregion

            #region New for ProjectMembers as Freelancer

            foreach (var item in model.freelancerDetails)
            {
                //var TmMember = (from u in dbcontext.UserDetails
                //                where u.UserId == item.FreelancerUserId && u.IsDeleted == false
                //                select u).FirstOrDefault();

                UserDetail TmMember;
                ProjectModel.ParentCompanyInfoForFreelancerModel ParentCompany;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"selcet * from UserDetails u where u.UserId = @FreelancerUserId and u.IsDeleted = 0  ";
                    TmMember = con.Query<UserDetail>(sqlquery, new { FreelancerUserId = item.FreelancerUserId }).FirstOrDefault();

                    string sqlquery_ParentCompany = @"select e.FirstName as UserName,ec.Name as CompanyName,p.Name as ProjectName, p.Description as ProjectDescription,p.EstimatedHours as ProjectEstimatedHours  
                                                       from UserDetails e inner join UserCompanyRoles usrcom on e.UserId = usrcom.UserId inner join Companies ec on usrcom.CompanyId = ec.Id 
                                                       inner join Projects p on ec.Id = p.CompanyId where e.UserId = @UserId and e.IsDeleted = 0 and usrcom.CompanyId = @companyId and p.IsDeleted = 0
                                                       and p.Id = @ProjectId 
                                                       ";
                    ParentCompany = con.Query<ProjectModel.ParentCompanyInfoForFreelancerModel>(sqlquery_ParentCompany, new { UserId = UserId, companyId = companyId, ProjectId = ProjectId }).FirstOrDefault();
                }

                //ProjectModel.ParentCompanyInfoForFreelancerModel ParentCompany = (from e in dbcontext.UserDetails
                //                                                                  join usrcom in dbcontext.UserCompanyRoles on e.UserId equals usrcom.UserId
                //                                                                  join ec in dbcontext.Companies on usrcom.CompanyId equals ec.Id
                //                                                                  join p in dbcontext.Projects on ec.Id equals p.CompanyId
                //                                                                  where e.UserId == UserId && e.IsDeleted == false
                //                                                                  && usrcom.CompanyId == companyId && p.IsDeleted == false
                //                                                                  && p.Id == ProjectId
                //                                                                  select new ProjectModel.ParentCompanyInfoForFreelancerModel
                //                                                                  {
                //                                                                      UserName = e.FirstName,
                //                                                                      CompanyName = ec.Name,
                //                                                                      ProjectName = p.Name,
                //                                                                      ProjectDescription = p.Description,
                //                                                                      ProjectEstimatedHours = p.EstimatedHours
                //                                                                  }
                //                                                              ).FirstOrDefault();





                if (ParentCompany != null)
                {
                    string ParentUserName = ParentCompany.UserName;
                    string ParentProjectName = ParentCompany.ProjectName;
                    string ParentCompanyName = ParentCompany.CompanyName;

                    string SendInvitationTo = "SendInvitationToRegisteredFreelancerFromProjectScreen";

                    //var freelncr = (from rol in dbcontext.Roles
                    //                where rol.RoleId == item.FreelancerRoleId
                    //                && rol.IsDeleted == false
                    //                select new
                    //                {
                    //                    RoleName = rol.Name
                    //                }).FirstOrDefault();

                    string freelncr_RoleName;
                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlquery = @"select rol.Name from Roles rol where rol.RoleId = @FreelancerRoleId and rol.IsDeleted = 0";
                        freelncr_RoleName = con.Query<string>(sqlquery, new { FreelancerRoleId = item.FreelancerRoleId }).FirstOrDefault();
                    }


                    #region Mail-Body

                    var callbackUrl = url + "#/invitation/" + ProjectId;
                    //String emailbody = "<p>" + ParentUserName + " wants to add you to " + ParentCompanyName + "'s list of contractors on tighten, please follow the link to create your account today</p>";
                    ////emailbody += "<p>Please confirm your account by clicking this link </p>";
                    //emailbody += "<a href=\"" + callbackUrl + "\">link</a>";


                    String emailbody = null;
                    string Subject = string.Empty;

                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                    var MailModule = from r in xdoc.Descendants("MailModule").
                                     Where(r => (string)r.Attribute("id") == SendInvitationTo)
                                         //select r;
                                     select new
                                     {
                                         mailbody = r.Element("Body").Value,
                                         subject = r.Element("Subject").Value
                                     };

                    foreach (var Mailitem in MailModule)
                    {
                        emailbody = Mailitem.mailbody;
                        Subject = Mailitem.subject;
                    }

                    emailbody = emailbody.Replace("@@ReceiverName", TmMember.UserName).Replace("@@ParentUserName", ParentUserName).Replace("@@ParentCompanyName", ParentCompanyName).Replace("@@ParentProject", ParentProjectName).Replace("@@callbackUrl", callbackUrl);
                    emailbody = emailbody.Replace("@@ProjectSummary", ParentCompany.ProjectDescription).Replace("@@FreelancerRate", item.FreelancerOfferedPrice.ToString()).Replace("@@EstimatedHours", ParentCompany.ProjectEstimatedHours.ToString());
                    emailbody = emailbody.Replace("@@FreelancerRole", freelncr_RoleName).Replace("@@FreelancerResponsibilities", item.FreelancerResponsibilities);

                    Subject = Subject.Replace("@@ParentProject", ParentProjectName);

                    #endregion

                    List<EmailUtility.ProjectFileModel> FilePathList = new List<EmailUtility.ProjectFileModel>();

                    if (model.TempData != null)
                    {
                        if (model.TempData.FilePath != string.Empty && model.TempData.FilePath != null)
                        {
                            EmailUtility.ProjectFileModel obj = new EmailUtility.ProjectFileModel();
                            obj.FilePath = model.TempData.FilePath;
                            obj.ActualFileName = model.TempData.ActualFileName;

                            FilePathList.Add(obj);
                        }
                    }

                    foreach (var frlncrItem in item.AllAttachments)
                    {
                        EmailUtility.ProjectFileModel obj = new EmailUtility.ProjectFileModel();
                        obj.FilePath = frlncrItem.FilePath;
                        obj.ActualFileName = frlncrItem.ActualFileName;

                        FilePathList.Add(obj);
                    }


                    if (FilePathList.Count > 0)
                    {
                        EmailUtility.SendMailInThreadWithAttachment(TmMember.Email, Subject, emailbody, FilePathList);
                    }
                    else
                    {
                        EmailUtility.SendMailInThread(TmMember.Email, Subject, emailbody);
                    }
                }
            }

            #endregion

        }

        private void SendInvitationForProjectToFreelancer(string url, String UserId, string[] ProjectMembers, int companyId, int ProjectId)
        {
            ProjectModel.ParentCompanyInfoForFreelancerModel ParentCompany = (from e in dbcontext.UserDetails
                                                                              join usrcom in dbcontext.UserCompanyRoles on e.UserId equals usrcom.UserId
                                                                              join ec in dbcontext.Companies on usrcom.CompanyId equals ec.Id
                                                                              join p in dbcontext.Projects on ec.Id equals p.CompanyId
                                                                              where e.UserId == UserId && e.IsDeleted == false
                                                                              && usrcom.CompanyId == companyId && p.IsDeleted == false
                                                                              && p.Id == ProjectId
                                                                              select new ProjectModel.ParentCompanyInfoForFreelancerModel
                                                                              {
                                                                                  UserName = e.FirstName,
                                                                                  CompanyName = ec.Name,
                                                                                  ProjectName = p.Name
                                                                                  //UserRoleId = usrcom.RoleId
                                                                              }
                                                                              ).FirstOrDefault();

            if (ParentCompany != null)
            {
                string ParentUserName = ParentCompany.UserName;
                string ParentProjectName = ParentCompany.ProjectName;
                string ParentCompanyName = ParentCompany.CompanyName;


                string SendInvitationTo = "SendInvitationToRegisteredFreelancerFromProjectScreen";

                foreach (var item in ProjectMembers)
                {
                    ProjectModel.ParentCompanyInfoForFreelancerModel usr = (from u in dbcontext.UserDetails
                                                                            join uc in dbcontext.UserCompanyRoles on u.UserId equals uc.UserId
                                                                            where u.UserId == item && u.IsDeleted == false
                                                                            && uc.CompanyId == companyId
                                                                            select new ProjectModel.ParentCompanyInfoForFreelancerModel
                                                                            {
                                                                                IsFreelancer = u.IsFreelancer,
                                                                                Email = u.Email,
                                                                                RoleId = uc.RoleId,
                                                                                UserName = u.FirstName
                                                                            }
                                                                           ).FirstOrDefault();

                    if (usr.IsFreelancer)
                    {
                        FreelancerInvitation freelancer = (from freelncr in dbcontext.FreelancerInvitations
                                                           where freelncr.FreelancerEmail == usr.Email
                                                           && freelncr.CompanyId == companyId
                                                           && freelncr.ProjectId == ProjectId
                                                           && freelncr.IskeyActive == true
                                                           select freelncr).SingleOrDefault();

                        if (freelancer == null)
                        {


                            #region Mail-Body
                            string key = "";

                            var callbackUrl = url + "#/invitation" + key;
                            //String emailbody = "<p>" + ParentUserName + " wants to add you to " + ParentCompanyName + "'s list of contractors on tighten, please follow the link to create your account today</p>";
                            ////emailbody += "<p>Please confirm your account by clicking this link </p>";
                            //emailbody += "<a href=\"" + callbackUrl + "\">link</a>";


                            String emailbody = null;
                            string Subject = string.Empty;

                            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                            var MailModule = from r in xdoc.Descendants("MailModule").
                                             Where(r => (string)r.Attribute("id") == SendInvitationTo)
                                                 //select r;
                                             select new
                                             {
                                                 mailbody = r.Element("Body").Value,
                                                 subject = r.Element("Subject").Value
                                             };

                            foreach (var Mailitem in MailModule)
                            {
                                emailbody = Mailitem.mailbody;
                                Subject = Mailitem.subject;
                            }

                            emailbody = emailbody.Replace("@@ReceiverName", usr.UserName).Replace("@@ParentUserName", ParentUserName).Replace("@@ParentCompanyName", ParentCompanyName).Replace("@@ParentProject", ParentProjectName).Replace("@@callbackUrl", callbackUrl);

                            #endregion


                            EmailUtility.SendMailInThread(usr.Email, "New Invitation ", emailbody);

                        }
                    }
                }
            }

        }


        /// <summary>
        /// It updates the Existing Project Fields
        /// </summary>
        /// <param name="UserId">It is unique id of User who is Updating The Project</param>
        /// <param name="model">Model consists the updated values of existing project and its project members</param>
        public string updateProject(string url, String UserId, ProjectModel.Project model, int CompanyId)
        {
            string msg = string.Empty;

            #region Old code with entityframework
            //dbcontext = new AppContext();

            //var IsUserFreelancer = (from u in dbcontext.UserDetails
            //                        where u.UserId == UserId && u.IsDeleted == false
            //                        select u).FirstOrDefault();



            //var team = (from m1 in dbcontext.Teams
            //            where m1.Id == model.TeamId && m1.IsDeleted == false
            //            select m1).SingleOrDefault();


            //if (IsUserFreelancer.IsFreelancer && team.IsDefault == true)
            //{
            //    CompanyId = 0;
            //}
            //var teamBudget = team.TeamBudget;


            //var projectsBudget = dbcontext.Projects.Where(x => x.CompanyId == CompanyId && x.TeamId == model.TeamId && x.Id != model.ProjectId && x.IsDeleted == false).Select(x => x.EstimatedBudget).Sum();
            //var totalprojectsBudget = (projectsBudget == null ? 0 : projectsBudget) + model.projectBudget;
            ////  CompanyId is passed from parameters
            ////int companyId = Convert.ToInt32((from u in dbcontext.UserDetails
            ////                                 where u.UserId == UserId
            ////                                 select u.CompanyId).SingleOrDefault());
            //if (teamBudget >= totalprojectsBudget || totalprojectsBudget == null)
            //{
            //    var isProjectExist = (from prjct in dbcontext.Projects
            //                          where prjct.Id != model.ProjectId && prjct.IsDeleted == false
            //                          && prjct.Name == model.ProjectName
            //                          && prjct.CompanyId == CompanyId
            //                          && prjct.TeamId == model.TeamId
            //                          select prjct.Id).SingleOrDefault();


            //    if (isProjectExist < 1)
            //    {

            //        Project projectEntity = (from e in dbcontext.Projects
            //                                 where e.Id == model.ProjectId && e.IsDeleted == false
            //                                 select e).SingleOrDefault();

            //        projectEntity.AccessLevel = model.AccessLevel;
            //        projectEntity.Name = model.ProjectName;
            //        projectEntity.Description = model.ProjectDescription;
            //        projectEntity.EstimatedHours = model.projectHours;
            //        projectEntity.EstimatedBudget = model.projectBudget;
            //        projectEntity.ModifiedDate = DateTime.Now;

            //        if (model.TempData != null)
            //        {
            //            projectEntity.FileName = model.TempData.FileName;
            //            projectEntity.ActualFileName = model.TempData.ActualFileName;
            //            projectEntity.FilePath = model.TempData.FilePath;
            //            projectEntity.AttachmentDescription = model.TempData.AttachmentDescription;
            //        }



            //        //Updating Internal Users Role
            //        foreach (var item in model.internalUserDetails)
            //        {
            //            var prjtcusr = (from pu in dbcontext.ProjectUsers
            //                            where pu.ProjectId == model.ProjectId
            //                            && pu.UserId == item.UserId
            //                            select pu).FirstOrDefault();
            //            if (prjtcusr != null)
            //            {
            //                prjtcusr.RoleId = item.RoleId;
            //                dbcontext.Entry<ENTITY.ProjectUser>(prjtcusr).State = EntityState.Modified;
            //            }
            //        }

            //        dbcontext.SaveChanges();

            //        string[] existingProjectMembers = (from m in dbcontext.ProjectUsers where m.ProjectId == model.ProjectId select m.UserId).ToArray();

            //        string[] FreelancersArray = model.freelancerDetails.Select(i => i.FreelancerUserId).ToArray();

            //        string[] InternalUsers = model.internalUserDetails.Select(i => i.UserId).ToArray();

            //        string[] existingUsersToBeDeleted = existingProjectMembers.Except(InternalUsers).ToArray();
            //        existingUsersToBeDeleted = existingUsersToBeDeleted.Except(FreelancersArray).ToArray();

            //        string[] newUsersToBeAdded_Internal = InternalUsers.Except(existingProjectMembers).ToArray();
            //        string[] FreelancersArrayToAdd = model.freelancerDetails.Where(x => x.IsInvitationSent == false).Select(i => i.FreelancerUserId).ToArray();
            //        string[] newUsersToBeAdded_Freelancer = FreelancersArrayToAdd.Except(existingProjectMembers).ToArray();

            //        var newUsersToBeAdded = new string[newUsersToBeAdded_Internal.Length + newUsersToBeAdded_Freelancer.Length];
            //        newUsersToBeAdded_Internal.CopyTo(newUsersToBeAdded, 0);
            //        newUsersToBeAdded_Freelancer.CopyTo(newUsersToBeAdded, newUsersToBeAdded_Internal.Length);


            //        #region existingUsersToBeDeleted

            //        if (existingUsersToBeDeleted != null)
            //        {
            //            for (int i = 0; i < existingUsersToBeDeleted.Length; i++)
            //            {
            //                string UserIdToBeDeleted = existingUsersToBeDeleted[i];
            //                ProjectUser ProjectuserToBeDeletedEntity = (from m in dbcontext.ProjectUsers where m.UserId == UserIdToBeDeleted && m.ProjectId == model.ProjectId select m).SingleOrDefault();

            //                dbcontext.ProjectUsers.Remove(ProjectuserToBeDeletedEntity);
            //                dbcontext.SaveChanges();

            //                /* Saving notification for newly added Team for team members */
            //                Notification objNotification = new Notification
            //                {
            //                    Name = "has removed you from",
            //                    CreatedBy = UserId,
            //                    CreatedDate = DateTime.Now,
            //                    IsRead = false,
            //                    Type = 2,
            //                    NotifyingUserId = existingUsersToBeDeleted[i],
            //                    ProjectId = model.ProjectId
            //                };

            //                dbcontext.Notifications.Add(objNotification);


            //                #region Deleting default values from SettingConfigs for deleted users
            //                /* getting all Settings for the user  */
            //                UserSettingConfig usrsetting = (from usrset in dbcontext.UserSettingConfigs
            //                                                join set in dbcontext.SettingConfigs on usrset.NotificationId equals set.Id
            //                                                where usrset.UserId == UserIdToBeDeleted
            //                                                && usrset.CompanyId == CompanyId
            //                                                && set.NotificationName == "Project Update"
            //                                                select usrset).FirstOrDefault();

            //                string ProjectsAfterDeletion = string.Empty;
            //                int[] AllProjectIds = null;
            //                if (usrsetting.ProjectId != null && usrsetting.ProjectId != string.Empty)
            //                {
            //                    AllProjectIds = usrsetting.ProjectId.Split(',').Select(str => int.Parse(str)).ToArray();
            //                    foreach (var item in AllProjectIds)
            //                    {
            //                        if (item != model.ProjectId)
            //                        {
            //                            if (ProjectsAfterDeletion == string.Empty)
            //                            {
            //                                ProjectsAfterDeletion = item.ToString();
            //                            }
            //                            else
            //                            {
            //                                ProjectsAfterDeletion += "," + item.ToString();
            //                            }
            //                        }
            //                    }
            //                    usrsetting.ProjectId = ProjectsAfterDeletion;
            //                }

            //                #endregion

            //                dbcontext.SaveChanges();
            //            }
            //        }

            //        #endregion


            //        #region newUsersToBeAdded 
            //        if (newUsersToBeAdded != null)
            //        {
            //            // To Add Freelancers
            //            //SendInvitationForProjectToFreelancer(url, UserId, newUsersToBeAdded, CompanyId, model.ProjectId);

            //            string Members_UserId = string.Empty;
            //            string NotificationName = "has added you in";
            //            int NotificationType = 2;
            //            bool SaveNotification = false;

            //            for (int i = 0; i < newUsersToBeAdded.Length; i++)
            //            {
            //                Members_UserId = newUsersToBeAdded[i];

            //                var CurrentProjectMembers = (from u in dbcontext.UserDetails
            //                                             join ucr in dbcontext.UserCompanyRoles on u.UserId equals ucr.UserId
            //                                             where u.UserId == Members_UserId && u.IsDeleted == false
            //                                             select new
            //                                             {
            //                                                 IsFreelancer = u.IsFreelancer,
            //                                                 Email = u.Email,
            //                                                 RoleId = ucr.RoleId
            //                                             }).FirstOrDefault();

            //                int CurrentProjectMembersRole = 0;

            //                if (CurrentProjectMembers.IsFreelancer)
            //                {
            //                    CurrentProjectMembersRole = (from r in model.freelancerDetails
            //                                                 where r.FreelancerUserId == Members_UserId
            //                                                 select r.FreelancerRoleId).FirstOrDefault();
            //                }
            //                else
            //                {
            //                    CurrentProjectMembersRole = (from r in model.internalUserDetails
            //                                                 where r.UserId == Members_UserId
            //                                                 select r.RoleId).FirstOrDefault();
            //                }

            //                /*  ProjectMembers  is freelancer or not   */
            //                if (!CurrentProjectMembers.IsFreelancer || (CurrentProjectMembers.IsFreelancer == true && team.IsDefault == true))
            //                {

            //                    ProjectUser ProjectuserToBeAddedEntity = new ProjectUser
            //                    {
            //                        UserId = newUsersToBeAdded[i],
            //                        CreatedBy = UserId,
            //                        CreatedDate = DateTime.Now,
            //                        ProjectId = model.ProjectId,
            //                        RoleId = CurrentProjectMembersRole
            //                    };

            //                    dbcontext.ProjectUsers.Add(ProjectuserToBeAddedEntity);
            //                    dbcontext.SaveChanges();

            //                    if (ProjectuserToBeAddedEntity.Id > 0)
            //                    {
            //                        SaveNotification = true;
            //                    }
            //                }
            //                else
            //                {

            //                    NotificationName = "has Invited you for project :";
            //                    NotificationType = 10;

            //                    FreelancerInvitation FreelancerInv = (from freelncr in dbcontext.FreelancerInvitations
            //                                                          where freelncr.FreelancerEmail == CurrentProjectMembers.Email
            //                                                          && freelncr.CompanyId == CompanyId
            //                                                          && freelncr.ProjectId == projectEntity.Id
            //                                                          && freelncr.IskeyActive == true
            //                                                          select freelncr).SingleOrDefault();

            //                    if (FreelancerInv == null)
            //                    {
            //                        string key = UTILITIES.EmailUtility.GetUniqueKey(16);

            //                        var item = (from u in model.freelancerDetails
            //                                    where u.FreelancerUserId == Members_UserId
            //                                    select u).FirstOrDefault();


            //                        FreelancerInvitation obj = new FreelancerInvitation
            //                        {
            //                            CompanyId = CompanyId,
            //                            SentBy = UserId,
            //                            ProjectId = projectEntity.Id,
            //                            RoleId = item.FreelancerRoleId,
            //                            Activationkey = key,
            //                            IskeyActive = true,
            //                            SentDate = DateTime.Now,
            //                            IsAccepted = false,
            //                            FreelancerEmail = CurrentProjectMembers.Email,
            //                            //WeeklyLimit = model.WeeklyLimit,
            //                            IsJobInvitation = false,
            //                            FreelancerRate = item.FreelancerOfferedPrice,
            //                            //FileName = item.FileName,
            //                            //ActualFileName = item.ActualFileName,
            //                            //FilePath = item.FilePath,
            //                            //AttachmentDescription = model.TempData.AttachmentDescription,
            //                            Responsibilities = item.FreelancerResponsibilities,

            //                        };
            //                        dbcontext.FreelancerInvitations.Add(obj);

            //                        foreach (var frlncrItem in item.AllAttachments)
            //                        {
            //                            ProjectInvitationAttachment InvObj = new ProjectInvitationAttachment
            //                            {
            //                                UserId = Members_UserId,
            //                                ProjectId = projectEntity.Id,
            //                                FileName = frlncrItem.FileName,
            //                                ActualFileName = frlncrItem.ActualFileName,
            //                                FilePath = frlncrItem.FilePath,
            //                                CreatedBy = UserId,
            //                                CreatedDate = DateTime.Now
            //                            };
            //                            dbcontext.ProjectInvitationAttachments.Add(InvObj);
            //                        }



            //                        dbcontext.SaveChanges();

            //                        if (obj.Id > 0)
            //                        {
            //                            foreach (var Listitem in model.freelancerDetails)
            //                            {
            //                                if (Listitem.FreelancerUserId == Members_UserId)
            //                                {
            //                                    Listitem.Key = key;
            //                                }
            //                            }

            //                            SaveNotification = true;
            //                        }
            //                    }
            //                }


            //                if (SaveNotification == true)
            //                {
            //                    /* Saving notification for newly added Team for team members */
            //                    Notification objNotification = new Notification
            //                    {
            //                        Name = NotificationName,
            //                        CreatedBy = UserId,
            //                        CreatedDate = DateTime.Now,
            //                        IsRead = false,
            //                        Type = NotificationType,
            //                        NotifyingUserId = newUsersToBeAdded[i],
            //                        ProjectId = model.ProjectId
            //                    };

            //                    dbcontext.Notifications.Add(objNotification);


            //                    #region Adding ProjectId in UserSettingConfig for each project member

            //                    /* getting all Settings for the user  */

            //                    UserSettingConfig usrsetting = (from usrset in dbcontext.UserSettingConfigs
            //                                                    join set in dbcontext.SettingConfigs on usrset.NotificationId equals set.Id
            //                                                    where set.IsActive == true
            //                                                    && usrset.CompanyId == CompanyId
            //                                                    && usrset.UserId == Members_UserId
            //                                                    && set.NotificationName == "Project Update"
            //                                                    select usrset).FirstOrDefault();

            //                    if (usrsetting.ProjectId != null && usrsetting.ProjectId != string.Empty)
            //                    {
            //                        usrsetting.ProjectId += "," + projectEntity.Id.ToString();
            //                    }
            //                    else
            //                    {
            //                        usrsetting.ProjectId = projectEntity.Id.ToString();
            //                    }

            //                    //dbcontext.SaveChanges();
            //                    #endregion


            //                    dbcontext.SaveChanges();
            //                }
            //            }
            //        }

            //        #endregion  



            //        if (team.IsDefault != true)
            //        {
            //            HttpContext ctx = HttpContext.Current;
            //            Thread childref = new Thread(new ThreadStart(() =>
            //            {
            //                HttpContext.Current = ctx;
            //                ThreadForUpdateProject(UserId, model, CompanyId, existingProjectMembers, existingUsersToBeDeleted, newUsersToBeAdded, url);
            //            }
            //            ));
            //            childref.Start();

            //        }


            //        msg = "ProjectUpdatedSuccess";
            //    }
            //    else
            //    {
            //        msg = "DuplicateProject";
            //    }

            //}
            //else
            //{
            //    msg = "TeamProjectsBudgetExceed";
            //}

            #endregion

            List<ProjectModel.InternalUserDetailsModal> InternalUsersList = new List<ProjectModel.InternalUserDetailsModal>();
            foreach (var item in model.internalUserDetails)
            {
                ProjectModel.InternalUserDetailsModal obj = new ProjectModel.InternalUserDetailsModal
                {
                    InternalUserUserId = item.UserId,
                    InternalUserRoleId = item.RoleId
                };
                InternalUsersList.Add(obj);
            }

            List<ProjectModel.FreelancerDetailsModal> FreelancersList = new List<ProjectModel.FreelancerDetailsModal>();
            foreach (var item in model.freelancerDetails)
            {
                ProjectModel.FreelancerDetailsModal obj = new ProjectModel.FreelancerDetailsModal
                {
                    FreelancerUserUserId = item.FreelancerUserId,
                    FreelancerRoleId = item.FreelancerRoleId,
                    FreelancerOfferedPrice = item.FreelancerOfferedPrice,
                    FreelancerResponsibilities = item.FreelancerResponsibilities
                };
                FreelancersList.Add(obj);
            }


            string[] existingProjectMembers;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select m.UserId from ProjectUsers m where m.ProjectId = @ProjectId ";
                existingProjectMembers = con.Query<string>(sqlquery, new { ProjectId = model.ProjectId }).ToArray();
            }

            string[] FreelancersArray = model.freelancerDetails.Select(i => i.FreelancerUserId).ToArray();

            string[] InternalUsers = model.internalUserDetails.Select(i => i.UserId).ToArray();

            string[] existingUsersToBeDeleted = existingProjectMembers.Except(InternalUsers).ToArray();
            existingUsersToBeDeleted = existingUsersToBeDeleted.Except(FreelancersArray).ToArray();

            string[] newUsersToBeAdded_Internal = InternalUsers.Except(existingProjectMembers).ToArray();
            string[] FreelancersArrayToAdd = model.freelancerDetails.Where(x => x.IsInvitationSent == false).Select(i => i.FreelancerUserId).ToArray();
            string[] newUsersToBeAdded_Freelancer = FreelancersArrayToAdd.Except(existingProjectMembers).ToArray();

            var newUsersToBeAdded = new string[newUsersToBeAdded_Internal.Length + newUsersToBeAdded_Freelancer.Length];
            newUsersToBeAdded_Internal.CopyTo(newUsersToBeAdded, 0);
            newUsersToBeAdded_Freelancer.CopyTo(newUsersToBeAdded, newUsersToBeAdded_Internal.Length);


            List<ProjectModel.UsersToBeAddedOrDeletedModal> ExistingUsersToBeDeletedList = new List<ProjectModel.UsersToBeAddedOrDeletedModal>();

            for (int i = 0; i < existingUsersToBeDeleted.Length; i++)
            {
                ProjectModel.UsersToBeAddedOrDeletedModal obj = new ProjectModel.UsersToBeAddedOrDeletedModal
                {
                    MemberUserIdToBeAdded = existingUsersToBeDeleted[i],
                    keyy = UTILITIES.EmailUtility.GetUniqueKey(16)
                };

                ExistingUsersToBeDeletedList.Add(obj);
            }




            List<ProjectModel.UsersToBeAddedOrDeletedModal> NewUsersToBeAddedList = new List<ProjectModel.UsersToBeAddedOrDeletedModal>();

            for (int i = 0; i < newUsersToBeAdded.Length; i++)
            {
                ProjectModel.UsersToBeAddedOrDeletedModal obj = new ProjectModel.UsersToBeAddedOrDeletedModal
                {
                    MemberUserIdToBeAdded = newUsersToBeAdded[i],
                    keyy = UTILITIES.EmailUtility.GetUniqueKey(16)
                };

                NewUsersToBeAddedList.Add(obj);
            }




            using (var con = new SqlConnection(ConnectionString))
            {
                bool IsTempDataNull = true;
                string FileName = string.Empty;
                string ActualFileName = string.Empty;
                string FilePath = string.Empty;
                string AttachmentDescription = string.Empty;

                if (model.TempData != null)
                {
                    IsTempDataNull = false;
                    FileName = model.TempData.FileName;
                    ActualFileName = model.TempData.ActualFileName;
                    FilePath = model.TempData.FilePath;
                    AttachmentDescription = model.TempData.AttachmentDescription;
                }


                msg = con.Query<string>("usp_ProjectUpdate", new
                {
                    UserId = UserId,
                    TeamId = model.TeamId,
                    CompanyId = CompanyId,
                    ProjectId = model.ProjectId,
                    projectBudget = model.projectBudget,
                    ProjectName = model.ProjectName,
                    AccessLevel = model.AccessLevel,
                    ProjectDescription = model.ProjectDescription,
                    projectHours = model.projectHours,
                    IsTempDataNull = IsTempDataNull,
                    FileName = FileName,
                    ActualFileName = ActualFileName,
                    FilePath = FilePath,
                    AttachmentDescription = AttachmentDescription,
                    InternalUsersList = InternalUsersList.AsTableValuedParameter("InternalUserDetailsForProjectUpdate", new[] { "InternalUserUserId", "InternalUserRoleId" }),
                    FreelancersList = FreelancersList.AsTableValuedParameter("FreelancerDetailsForProjectUpdate", new[] { "FreelancerUserUserId", "FreelancerRoleId", "FreelancerOfferedPrice", "FreelancerResponsibilities" }),
                    ExistingUsersToBeDeleted = ExistingUsersToBeDeletedList.AsTableValuedParameter("ExistingUsersToBeDeletedForProjectUpdate", new[] { "MemberUserIdToBeAdded", "keyy" }),
                    NewUsersToBeAdded = NewUsersToBeAddedList.AsTableValuedParameter("ExistingUsersToBeDeletedForProjectUpdate", new[] { "MemberUserIdToBeAdded", "keyy" })
                }, commandType: CommandType.StoredProcedure).Single();



                List<ProjectInvitationAttachment> invObjList = new List<ProjectInvitationAttachment>();
                foreach (var freelancerItem in model.freelancerDetails)
                {
                    foreach (var att in freelancerItem.AllAttachments)
                    {
                        ProjectInvitationAttachment objj = new ProjectInvitationAttachment();
                        objj.UserId = freelancerItem.FreelancerUserId;
                        objj.ProjectId = model.ProjectId;
                        objj.ActualFileName = att.ActualFileName;
                        objj.FileName = att.FileName;
                        objj.FilePath = att.FilePath;
                        objj.CreatedBy = UserId;
                        objj.CreatedDate = DateTime.Now;

                        invObjList.Add(objj);
                    }
                }

                string sqlquery_ProjectInvitationAttachment = @"insert into ProjectInvitationAttachments (UserId,ProjectId,ActualFileName,FileName,FilePath,CreatedBy,CreatedDate ) values(@UserId,@ProjectId,@ActualFileName,@FileName,@FilePath,@CreatedBy,@CreatedDate) ";
                con.Execute(sqlquery_ProjectInvitationAttachment, invObjList);

            }


            if (msg == "ProjectUpdatedSuccess")
            {
                //var team = (from m1 in dbcontext.Teams
                //            where m1.Id == model.TeamId && m1.IsDeleted == false
                //            select m1).SingleOrDefault();

                bool IsTeamDefault = false;
                string sqlquery = @"select * from Teams m1 where m1.Id = @TeamId and m1.IsDeleted = 0 ";
                using (var con = new SqlConnection(ConnectionString))
                {
                    IsTeamDefault = con.Query<bool>(sqlquery, new { TeamId = model.TeamId }).SingleOrDefault();
                }

                if (IsTeamDefault != true)
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForUpdateProject(UserId, model, CompanyId, existingProjectMembers, existingUsersToBeDeleted, newUsersToBeAdded, url);
                    }
                    ));
                    childref.Start();
                }
            }


            return msg;
        }


        //Sending mail in thread on Project update to all project members 
        private void ThreadForUpdateProject(String UserId, ProjectModel.Project model, int companyId, string[] existingProjectMembers, string[] existingUsersToBeDeleted, string[] newUsersToBeAdded, string url)
        {
            string SendInvitationTo = "UpdateProject";


            #region For all Existing and Deleted Users
            foreach (var item in existingProjectMembers)
            {
                bool IsSettingActivated = false;

                foreach (var deletedUsers_item in existingUsersToBeDeleted)
                {
                    if (deletedUsers_item == item)
                    {
                        SendInvitationTo = "DeletedUsersFromProject";
                        break;
                    }
                }

                /*  This function is used for checking wheather mail setting is activated for assignee or not*/
                if (!string.IsNullOrEmpty(item))
                {
                    string UsrActiveProjects = (from usrSetting in dbcontext.UserSettingConfigs
                                                join dfltSet in dbcontext.SettingConfigs on usrSetting.NotificationId equals dfltSet.Id
                                                where dfltSet.NotificationName == "Project Update"
                                                && usrSetting.UserId == item && usrSetting.CompanyId == companyId
                                                select usrSetting.ProjectId).FirstOrDefault();

                    int[] AllProjectIds = null;
                    if (UsrActiveProjects != null && UsrActiveProjects != string.Empty)
                    {
                        AllProjectIds = UsrActiveProjects.Split(',').Select(str => int.Parse(str)).ToArray();

                        foreach (var prjctItem in AllProjectIds)
                        {
                            if (prjctItem == model.ProjectId)
                            {
                                IsSettingActivated = true;
                            }
                        }
                    }
                }


                if (IsSettingActivated)
                {
                    UserDetail CurrentUser = (from crntusr in dbcontext.UserDetails
                                              where crntusr.UserId == UserId && crntusr.IsDeleted == false
                                              select crntusr).FirstOrDefault();

                    UserDetail TmMember = (from membr in dbcontext.UserDetails
                                           where membr.UserId == item && membr.IsDeleted == false
                                           select membr).FirstOrDefault();

                    //String emailbody = "<p> Hello " + TmMember.FirstName + " <br/>";
                    //emailbody += " This mail is sent to inform you that a project named as " + model.ProjectName + " has been updated by " + CurrentUser.FirstName + "<br/>";

                    #region Mail-Body

                    String emailbody = null;
                    string Subject = string.Empty;

                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                    var MailModule = from r in xdoc.Descendants("MailModule").
                                     Where(r => (string)r.Attribute("id") == SendInvitationTo)
                                         //select r;
                                     select new
                                     {
                                         mailbody = r.Element("Body").Value,
                                         subject = r.Element("Subject").Value
                                     };

                    foreach (var Mailitem in MailModule)
                    {
                        emailbody = Mailitem.mailbody;
                        Subject = Mailitem.subject;
                    }

                    emailbody = emailbody.Replace("@@Receiver_FirstName", TmMember.FirstName).Replace("@@ProjectName", model.ProjectName).Replace("@@UpdatedBy_FirstName", CurrentUser.FirstName);

                    #endregion

                    List<EmailUtility.ProjectFileModel> FilePathList = new List<EmailUtility.ProjectFileModel>();

                    if (model.TempData != null)
                    {
                        if (model.TempData.FilePath != string.Empty && model.TempData.FilePath != null)
                        {
                            EmailUtility.ProjectFileModel obj = new EmailUtility.ProjectFileModel();
                            obj.FilePath = model.TempData.FilePath;
                            obj.ActualFileName = model.TempData.ActualFileName;

                            FilePathList.Add(obj);
                        }
                    }

                    if (FilePathList.Count > 0)
                    {
                        EmailUtility.SendMailInThreadWithAttachment(TmMember.Email, "Project Updated", emailbody, FilePathList);
                    }
                    else
                    {
                        EmailUtility.SendMailInThread(TmMember.Email, "Project Updated", emailbody);
                    }





                }
                //}

            }

            #endregion


            #region For all New Users
            foreach (var NewUsersItem in newUsersToBeAdded)
            {
                bool IsSettingActivated = false;
                SendInvitationTo = "NewUsersInProject";

                UserDetail TmMember = (from membr in dbcontext.UserDetails
                                       where membr.UserId == NewUsersItem && membr.IsDeleted == false
                                       select membr).FirstOrDefault();

                if (!TmMember.IsFreelancer)
                {

                    /*  This function is used for checking wheather mail setting is activated for assignee or not*/
                    if (!string.IsNullOrEmpty(NewUsersItem))
                    {
                        string UsrActiveProjects = (from usrSetting in dbcontext.UserSettingConfigs
                                                    join dfltSet in dbcontext.SettingConfigs on usrSetting.NotificationId equals dfltSet.Id
                                                    where dfltSet.NotificationName == "Project Update"
                                                    && usrSetting.UserId == NewUsersItem && usrSetting.CompanyId == companyId
                                                    select usrSetting.ProjectId).FirstOrDefault();

                        int[] AllProjectIds = null;
                        if (UsrActiveProjects != null && UsrActiveProjects != string.Empty)
                        {
                            AllProjectIds = UsrActiveProjects.Split(',').Select(str => int.Parse(str)).ToArray();

                            foreach (var prjctItem in AllProjectIds)
                            {
                                if (prjctItem == model.ProjectId)
                                {
                                    IsSettingActivated = true;
                                }
                            }
                        }
                    }


                    if (IsSettingActivated)
                    {
                        UserDetail CurrentUser = (from crntusr in dbcontext.UserDetails
                                                  where crntusr.UserId == UserId && crntusr.IsDeleted == false
                                                  select crntusr).FirstOrDefault();



                        //String emailbody = "<p> Hello " + TmMember.FirstName + " <br/>";
                        //emailbody += " This mail is sent to inform you that a project named as " + model.ProjectName + " has been updated by " + CurrentUser.FirstName + "<br/>";

                        #region Mail-Body

                        String emailbody = null;
                        string Subject = string.Empty;

                        XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                        var MailModule = from r in xdoc.Descendants("MailModule").
                                         Where(r => (string)r.Attribute("id") == SendInvitationTo)
                                             //select r;
                                         select new
                                         {
                                             mailbody = r.Element("Body").Value,
                                             subject = r.Element("Subject").Value
                                         };

                        foreach (var Mailitem in MailModule)
                        {
                            emailbody = Mailitem.mailbody;
                            Subject = Mailitem.subject;
                        }

                        emailbody = emailbody.Replace("@@Receiver_FirstName", TmMember.FirstName).Replace("@@ProjectName", model.ProjectName).Replace("@@UpdatedBy_FirstName", CurrentUser.FirstName);

                        #endregion


                        List<EmailUtility.ProjectFileModel> FilePathList = new List<EmailUtility.ProjectFileModel>();

                        if (model.TempData != null)
                        {
                            if (model.TempData.FilePath != string.Empty && model.TempData.FilePath != null)
                            {
                                EmailUtility.ProjectFileModel obj = new EmailUtility.ProjectFileModel();
                                obj.FilePath = model.TempData.FilePath;
                                obj.ActualFileName = model.TempData.ActualFileName;

                                FilePathList.Add(obj);
                            }
                        }

                        if (FilePathList.Count > 0)
                        {
                            EmailUtility.SendMailInThreadWithAttachment(TmMember.Email, "Project Updated", emailbody, FilePathList);
                        }
                        else
                        {
                            EmailUtility.SendMailInThread(TmMember.Email, "Project Updated", emailbody);
                        }


                    }

                }
                else
                {

                    ProjectModel.ParentCompanyInfoForFreelancerModel ParentCompany = (from e in dbcontext.UserDetails
                                                                                      join usrcom in dbcontext.UserCompanyRoles on e.UserId equals usrcom.UserId
                                                                                      join ec in dbcontext.Companies on usrcom.CompanyId equals ec.Id
                                                                                      join p in dbcontext.Projects on ec.Id equals p.CompanyId
                                                                                      where e.UserId == UserId && e.IsDeleted == false
                                                                                      && usrcom.CompanyId == companyId && p.IsDeleted == false
                                                                                      && p.Id == model.ProjectId
                                                                                      select new ProjectModel.ParentCompanyInfoForFreelancerModel
                                                                                      {
                                                                                          UserName = e.FirstName,
                                                                                          CompanyName = ec.Name,
                                                                                          ProjectName = p.Name,
                                                                                          ProjectDescription = p.Description,
                                                                                          ProjectEstimatedHours = p.EstimatedHours
                                                                                          //UserRoleId = usrcom.RoleId
                                                                                      }
                                                                              ).FirstOrDefault();

                    if (ParentCompany != null)
                    {
                        string ParentUserName = ParentCompany.UserName;
                        string ParentProjectName = ParentCompany.ProjectName;
                        string ParentCompanyName = ParentCompany.CompanyName;

                        SendInvitationTo = "SendInvitationToRegisteredFreelancerFromProjectScreen";

                        var FKey = (from usr in model.freelancerDetails
                                    where usr.FreelancerUserId == NewUsersItem
                                    select usr).FirstOrDefault();


                        #region Mail-Body

                        var callbackUrl = url + "#/confirmfreelancer/" + FKey.Key;
                        //String emailbody = "<p>" + ParentUserName + " wants to add you to " + ParentCompanyName + "'s list of contractors on tighten, please follow the link to create your account today</p>";
                        ////emailbody += "<p>Please confirm your account by clicking this link </p>";
                        //emailbody += "<a href=\"" + callbackUrl + "\">link</a>";


                        String emailbody = null;
                        string Subject = string.Empty;

                        XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                        var MailModule = from r in xdoc.Descendants("MailModule").
                                         Where(r => (string)r.Attribute("id") == SendInvitationTo)
                                             //select r;
                                         select new
                                         {
                                             mailbody = r.Element("Body").Value,
                                             subject = r.Element("Subject").Value
                                         };

                        foreach (var Mailitem in MailModule)
                        {
                            emailbody = Mailitem.mailbody;
                            Subject = Mailitem.subject;
                        }


                        emailbody = emailbody.Replace("@@ReceiverName", TmMember.UserName).Replace("@@ParentUserName", ParentUserName).Replace("@@ParentCompanyName", ParentCompanyName).Replace("@@ParentProject", ParentProjectName).Replace("@@callbackUrl", callbackUrl);
                        emailbody = emailbody.Replace("@@ProjectSummary", ParentCompany.ProjectDescription).Replace("@@FreelancerRate", FKey.FreelancerOfferedPrice.ToString()).Replace("@@EstimatedHours", ParentCompany.ProjectEstimatedHours.ToString());
                        emailbody = emailbody.Replace("@@FreelancerRole", FKey.FreelancerRole).Replace("@@FreelancerResponsibilities", FKey.FreelancerResponsibilities);

                        #endregion

                        List<EmailUtility.ProjectFileModel> FilePathList = new List<EmailUtility.ProjectFileModel>();

                        if (model.TempData != null)
                        {
                            if (model.TempData.FilePath != string.Empty && model.TempData.FilePath != null)
                            {
                                EmailUtility.ProjectFileModel obj = new EmailUtility.ProjectFileModel();
                                obj.FilePath = model.TempData.FilePath;
                                obj.ActualFileName = model.TempData.ActualFileName;

                                FilePathList.Add(obj);
                            }
                        }

                        foreach (var frlncrItem in FKey.AllAttachments)
                        {
                            EmailUtility.ProjectFileModel obj = new EmailUtility.ProjectFileModel();
                            obj.FilePath = frlncrItem.FilePath;
                            obj.ActualFileName = frlncrItem.ActualFileName;

                            FilePathList.Add(obj);
                        }

                        if (FilePathList.Count > 0)
                        {
                            EmailUtility.SendMailInThreadWithAttachment(TmMember.Email, "New Invitation ", emailbody, FilePathList);
                        }
                        else
                        {
                            EmailUtility.SendMailInThread(TmMember.Email, "New Invitation ", emailbody);
                        }


                    }
                }
            }

            #endregion




        }



        /// <summary>
        /// It deletes the existing Project from Projects table
        /// </summary>
        /// <param name="Id">It is unique id of Project that will be Deleted</param>
        public string deleteProject(int Id, int CompanyId, bool IsFreelancer)
        {
            #region OLD CODE before dapper
            //dbcontext = new AppContext();
            //string msg = string.Empty;
            //var AllInvoice = (from invdet in dbcontext.InvoiceDetails
            //                  join tod in dbcontext.ToDos on invdet.TodoId equals tod.Id
            //                  join sec in dbcontext.Sections on tod.SectionId equals sec.Id
            //                  join p in dbcontext.Projects on sec.ProjectId equals p.Id
            //                  where p.Id == Id && p.IsDeleted == false && tod.IsDeleted == false
            //                  select invdet.Id).ToList();



            //if (AllInvoice.Count() > 0)
            //{
            //    msg = "InvoicePresentInProject";
            //}
            //else
            //{
            //    var team = (from p1 in dbcontext.Projects
            //                join t1 in dbcontext.Teams on p1.TeamId equals t1.Id
            //                where p1.Id == Id && t1.IsDeleted == false && p1.IsDeleted == false
            //                select t1).FirstOrDefault();

            //    if (team != null)
            //    {
            //        if (IsFreelancer && team.IsDefault == true)
            //        {
            //            CompanyId = 0;
            //        }
            //    }

            //    string[] ProjectUsers = (from usr in dbcontext.Projects
            //                             join usrprjct in dbcontext.ProjectUsers on usr.Id equals usrprjct.ProjectId
            //                             where usr.Id == Id && usr.IsDeleted == false
            //                             && usr.CompanyId == CompanyId
            //                             select usrprjct.UserId).ToArray();

            //    foreach (var item2 in ProjectUsers)
            //    {

            //        UserSettingConfig UsrSetList = (from usrsett in dbcontext.UserSettingConfigs
            //                                        where usrsett.CompanyId == CompanyId
            //                                        && usrsett.NotificationId == 5
            //                                        && usrsett.UserId == item2
            //                                        && usrsett.IsActive == true
            //                                        select usrsett
            //                                        ).FirstOrDefault();


            //        if (UsrSetList != null)
            //        {
            //            if (UsrSetList.ProjectId != null)
            //            {
            //                string[] AllProjectId = UsrSetList.ProjectId.Split(',');
            //                string RemainingProjects = string.Empty;

            //                foreach (var item in AllProjectId)
            //                {
            //                    if (item != Id.ToString())
            //                    {
            //                        if (RemainingProjects == string.Empty)
            //                        {
            //                            RemainingProjects = item;
            //                        }
            //                        else
            //                        {
            //                            RemainingProjects += "," + item;
            //                        }
            //                    }
            //                }

            //                UsrSetList.ProjectId = RemainingProjects;

            //            }
            //        }

            //        var freelncrInfo = (from inv in dbcontext.FreelancerInvitations
            //                            join usr in dbcontext.UserDetails on inv.FreelancerEmail equals usr.Email
            //                            where usr.UserId == item2 && inv.ProjectId == Id && usr.IsDeleted == false
            //                            select inv).ToList();

            //        foreach (var flncritem in freelncrInfo)
            //        {
            //            dbcontext.FreelancerInvitations.Remove(flncritem);
            //        }

            //    }


            //    var freelncrInfoForPendingInvitation = (from inv in dbcontext.FreelancerInvitations
            //                                            where inv.ProjectId == Id && inv.IskeyActive == true
            //                                            select inv).ToList();

            //    foreach (var flncritem in freelncrInfoForPendingInvitation)
            //    {
            //        dbcontext.FreelancerInvitations.Remove(flncritem);
            //    }



            //    Project projectentity = (from e in dbcontext.Projects
            //                             where e.Id == Id && e.IsDeleted == false
            //                             select e).SingleOrDefault();

            //    /* Delete Project Attachment*/
            //    if (projectentity.FilePath != null)
            //    {
            //        if (File.Exists(projectentity.FilePath))
            //        {
            //            File.Delete(projectentity.FilePath);
            //        }
            //    }
            //    /* Delete freelancers Attachment for this particular projects*/
            //    var Attachments = (from prjctInvAtchmnt in dbcontext.ProjectInvitationAttachments
            //                       where prjctInvAtchmnt.ProjectId == Id
            //                       select prjctInvAtchmnt).ToList();
            //    foreach (var item in Attachments)
            //    {
            //        if (item.FilePath != null)
            //        {
            //            if (File.Exists(item.FilePath))
            //            {
            //                File.Delete(item.FilePath);
            //            }
            //        }
            //    }
            //    dbcontext.ProjectInvitationAttachments.RemoveRange(Attachments);

            //    /* Delete Project */
            //    //dbcontext.Projects.Remove(projectentity);
            //    projectentity.IsDeleted = true;

            //    dbcontext.SaveChanges();

            //    //Deleting All Todos comes under this Project 


            //    msg = "ProjectDeletedSuccessfully";
            //}

            #endregion

            string msg = string.Empty;

            using (var con = new SqlConnection(ConnectionString))
            {
                msg = con.Query<string>("usp_Projectdelete", new { CompanyId = CompanyId, IsFreelancer = IsFreelancer, Id = Id }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (msg == "ProjectDeletedSuccessfully")
                {
                    ENTITY.Project projectentity;
                    string sqlquery_projectentity = @"select * from Projects e where e.Id = @Id ";
                    projectentity = con.Query<ENTITY.Project>(sqlquery_projectentity, new { Id = Id }).SingleOrDefault();

                    /* Delete Project Attachment*/
                    if (projectentity.FilePath != null)
                    {
                        if (File.Exists(projectentity.FilePath))
                        {
                            File.Delete(projectentity.FilePath);
                        }
                    }

                    /* Delete freelancers Attachment for this particular projects*/
                    List<ProjectInvitationAttachment> Attachments;
                    string sqlquery_Attachments = @"select * from ProjectInvitationAttachments prjctInvAtchmnt where prjctInvAtchmnt.ProjectId = @Id  ";
                    Attachments = con.Query<ProjectInvitationAttachment>(sqlquery_Attachments, new { Id = Id }).ToList();

                    foreach (var item in Attachments)
                    {
                        if (item.FilePath != null)
                        {
                            if (File.Exists(item.FilePath))
                            {
                                File.Delete(item.FilePath);
                            }
                        }
                    }
                }

            }



            return msg;
        }

        /// <summary>
        /// It mark as complete the existing Project from Projects table
        /// </summary>
        /// <param name="Id">It is unique id of Project that will be marked as complete</param>
        public string ProjectComplete(int Id)
        {
            dbcontext = new AppContext();

            bool ProjectEligibleToMarkComplete = false;

            var AllProjectUsers = (from usr in dbcontext.UserDetails
                                   join prjctusr in dbcontext.ProjectUsers on usr.UserId equals prjctusr.UserId
                                   where prjctusr.ProjectId == Id && usr.IsDeleted == false
                                   && usr.IsFreelancer == true
                                   select usr.UserId).ToList();

            var UserIdInCTest = (from test in dbcontext.ClientTestimonials
                                 where test.ProjectId == Id
                                 select test.UserId).ToList();


            foreach (var item in AllProjectUsers)
            {
                if (UserIdInCTest.Contains(item))
                {
                    ProjectEligibleToMarkComplete = true;
                }
                else
                {
                    ProjectEligibleToMarkComplete = false;
                    break;
                }
            }

            if (ProjectEligibleToMarkComplete)
            {
                ENTITY.Project projectentity = (from e in dbcontext.Projects
                                                where e.Id == Id && e.IsDeleted == false
                                                select e).SingleOrDefault();

                projectentity.IsComplete = true;
                dbcontext.SaveChanges();

                return "ProjectCompleted";
            }
            else
            {
                return "ProjectNotCompleted";
            }
        }

        /// <summary>
        /// It insert feedback for Project in  ClientTeatimonial table
        /// </summary>
        public void InsertFeedback(MODEL.ClientTestimonialModel model)
        {
            dbcontext = new AppContext();
            ENTITY.Project projectentity = (from e in dbcontext.Projects
                                            where e.Id == model.ProjectId && e.IsDeleted == false
                                            select e).SingleOrDefault();


            ClientTestimonial obj = new ClientTestimonial();

            foreach (var item in model.selectedFreelancersForProjects)
            {

                ClientTestimonial CTestimonial = (from CTest in dbcontext.ClientTestimonials
                                                  where CTest.UserId == item.FreelancerUserId
                                                  && CTest.ProjectId == model.ProjectId
                                                  select CTest).SingleOrDefault();

                if (CTestimonial == null)
                {
                    obj.ProjectId = model.ProjectId;
                    obj.UserId = item.FreelancerUserId;
                    obj.CompanyId = projectentity.CompanyId;
                    obj.Rating = model.Rating;
                    obj.Feedback = model.Feedback;
                    obj.AdminId = model.UserId;
                    obj.IsSelected = false;

                    dbcontext.ClientTestimonials.Add(obj);
                }
                else
                {
                    CTestimonial.ProjectId = model.ProjectId;
                    CTestimonial.UserId = item.FreelancerUserId;
                    CTestimonial.CompanyId = projectentity.CompanyId;
                    CTestimonial.Rating = model.Rating;
                    CTestimonial.Feedback = model.Feedback;
                    CTestimonial.AdminId = model.UserId;

                }

                dbcontext.SaveChanges();
            }
        }

        /// <summary>
        /// Getting All Projects in which User is involved/ProjectMember irrespective of Teams
        /// </summary>
        /// <param name="userId">It is unique id of user</param>
        /// <returns>List of Projects in which User is project Member</returns>
        public List<ProjectModel.ProjectWithForFinancialManager> getAllProjectsForFinancialManager(int CompanyId)
        {
            dbcontext = new AppContext();

            var projectModResult = (from data in
                                   (from m1 in dbcontext.Projects
                                    join m in dbcontext.ProjectUsers on m1.Id equals m.ProjectId
                                    where
                                    //&& m1.CompanyId == CompanyId
                                    (CompanyId == 0 ? m1.CompanyId == m1.CompanyId : m1.CompanyId == CompanyId)
                                    && m1.IsDeleted == false
                                    orderby m1.CreatedDate descending
                                    select new { m1 }
                                    )
                                    group data by new { data.m1.Id, data.m1.Name, data.m1.Description, data.m1.EstimatedBudget } into GroupedData

                                    select new
                                    {
                                        ProjectId = GroupedData.Key.Id,
                                        ProjectName = GroupedData.Key.Name,
                                        ProjectDescription = GroupedData.Key.Description,
                                        EstimatedBudget = GroupedData.Key.EstimatedBudget

                                    });
            List<ProjectModel.ProjectWithForFinancialManager> projectMod = projectModResult.AsEnumerable().Select(item =>
                                                           new ProjectModel.ProjectWithForFinancialManager()
                                                           {
                                                               ProjectId = item.ProjectId,
                                                               ProjectName = item.ProjectName,
                                                               AllottedBudget = item.EstimatedBudget,

                                                           }).ToList();
            return projectMod;
        }



        /// <summary>
        /// Getting All Projects in which User is involved/ProjectMember irrespective of Teams
        /// </summary>
        /// <param name="userId">It is unique id of user</param>
        /// <returns>List of Projects in which User is project Member</returns>
        public List<ProjectModel.ProjectWithForFinancialManager> GetAllProjectsOfCompany(int CompanyId, int TeamId)
        {
            //dbcontext = new AppContext();

            //var projectModResult = (from data in
            //                       (from m1 in dbcontext.Projects
            //                        join tm in dbcontext.Teams on m1.TeamId equals tm.Id
            //                        where
            //                        //&& m1.CompanyId == CompanyId
            //                        (CompanyId == 0 ? m1.CompanyId == m1.CompanyId : m1.CompanyId == CompanyId)
            //                        && (TeamId == 0 ? tm.Id == tm.Id : tm.Id == TeamId)
            //                        && tm.IsDeleted == false && m1.IsDeleted == false
            //                        orderby m1.CreatedDate descending
            //                       select new { m1 }
            //                        )
            //                        group data by new { data.m1.Id, data.m1.Name, data.m1.Description, data.m1.EstimatedBudget } into GroupedData

            //                        select new
            //                        {
            //                            ProjectId = GroupedData.Key.Id,
            //                            ProjectName = GroupedData.Key.Name,
            //                            ProjectDescription = GroupedData.Key.Description,
            //                            EstimatedBudget = GroupedData.Key.EstimatedBudget

            //                        });



            //List<ProjectModel.ProjectWithForFinancialManager> projectMod = projectModResult.AsEnumerable().Select(item =>
            //                                               new ProjectModel.ProjectWithForFinancialManager()
            //                                               {
            //                                                   ProjectId = item.ProjectId,
            //                                                   ProjectName = item.ProjectName,
            //                                                   AllottedBudget = item.EstimatedBudget,

            //                                               }).ToList();

            List<ProjectModel.ProjectWithForFinancialManager> projectMod;
            using (var con = new SqlConnection(ConnectionString))
            {
                projectMod = con.Query<ProjectModel.ProjectWithForFinancialManager>("usp_GetAllProjectsOfCompany", new { CompanyId = CompanyId, TeamId = TeamId }, commandType: CommandType.StoredProcedure).ToList();
            }


            return projectMod;
        }


        /// <summary>
        /// Getting All Projects in which User is involved/ProjectMember irrespective of Teams
        /// </summary>
        /// <param name="userId">It is unique id of user</param>
        /// <returns>List of Projects in which User is project Member</returns>

        public List<MODEL.ProjectModel.AllFreelancersForCompanyModel> GetAllFreelancerOfProject(int ProjectId)
        {
            //dbcontext = new AppContext();

            //List<MODEL.ProjectModel.AllFreelancersForCompanyModel> list = (from prjct in dbcontext.Projects
            //                                                               join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId
            //                                                               join usr in dbcontext.UserDetails on prjctusr.UserId equals usr.UserId
            //                                                               where prjct.Id == ProjectId && usr.IsDeleted == false
            //                                                               && usr.IsFreelancer == true && prjct.IsDeleted == false
            //                                                               select new MODEL.ProjectModel.AllFreelancersForCompanyModel
            //                                                               {
            //                                                                   UserId = usr.UserId,
            //                                                                   FreelancerName = usr.FirstName
            //                                                               }
            //                                                               ).ToList();

            List<MODEL.ProjectModel.AllFreelancersForCompanyModel> list;
            using (var con = new SqlConnection(ConnectionString))
            {
                list = con.Query<MODEL.ProjectModel.AllFreelancersForCompanyModel>("usp_GetAllFreelancerOfProject", new { ProjectId = ProjectId }, commandType: CommandType.StoredProcedure).ToList();
            }


            return list;
        }




        /// <summary>
        /// Getting All Projects in which User is involved/ProjectMember irrespective of Teams
        /// </summary>
        /// <param name="userId">It is unique id of user</param>
        /// <returns>List of Projects in which User is project Member</returns>

        public string SaveProjectOptionsForFreelancer(MODEL.ProjectModel.ProjectOptionsForFreelancerModel model)
        {
            dbcontext = new AppContext();
            string msg = string.Empty;

            foreach (var item in model.FreelancerIdList)
            {
                ProjectUser prjctusr = (from Pusr in dbcontext.ProjectUsers
                                        where Pusr.ProjectId == model.ProjectId
                                        && Pusr.UserId == item
                                        select Pusr).FirstOrDefault();

                if (prjctusr != null)
                {
                    prjctusr.UserRate = model.UserRate;
                    prjctusr.WeeklyLimit = model.WeeklyLimit;

                    dbcontext.SaveChanges();
                    msg = "Project is updated successfully";
                }
            }

            return msg;
        }



        public MODEL.ProjectModel.FreelancerListingModel GetFreelancerDetails(string UserId, int projectId)
        {
            //dbcontext = new AppContext();

            //var freelancr = (from freeinv in dbcontext.FreelancerInvitations
            //                 join prjct in dbcontext.Projects on freeinv.ProjectId equals prjct.Id
            //                 join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId
            //                 join usr in dbcontext.UserDetails on prjctusr.UserId equals usr.UserId
            //                 join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
            //                 join rol in dbcontext.Roles on usrcom.RoleId equals rol.RoleId
            //                 where usr.UserId == UserId && usr.IsDeleted == false && rol.IsDeleted == false
            //                 && freeinv.ProjectId == projectId && prjct.IsDeleted == false
            //                 select new
            //                 {
            //                     FreelancerUserId = usr.UserId,
            //                     FreelancerEmail = usr.Email,
            //                     FreelancerName = usr.FirstName,
            //                     FreelancerProfilePhoto = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + usr.ProfilePhoto,
            //                     FreelancerProfileRate = usr.Rate,
            //                     FreelancerOfferedPrice = prjctusr.UserRate.HasValue ? prjctusr.UserRate.Value : 0,
            //                     FreelancerRoleId = rol.RoleId,
            //                     FreelancerRole = rol.Name,

            //                     PreviousAttachments = (from at in dbcontext.ProjectInvitationAttachments
            //                                            where at.UserId == UserId && at.ProjectId == projectId
            //                                            select new MODEL.ProjectModel.ProjectFileModel
            //                                            {
            //                                                FileName = at.FileName,
            //                                                ActualFileName = at.ActualFileName,
            //                                                FilePath = at.FilePath
            //                                            })

            //                 });





            //MODEL.ProjectModel.FreelancerListingModel freelancer = freelancr.AsEnumerable().Select(x =>
            //new MODEL.ProjectModel.FreelancerListingModel
            //{
            //    FreelancerUserId = x.FreelancerUserId,
            //    FreelancerEmail = x.FreelancerEmail,
            //    FreelancerName = x.FreelancerName,
            //    FreelancerProfilePhoto = x.FreelancerProfilePhoto,
            //    FreelancerProfileRate = x.FreelancerProfileRate,
            //    FreelancerOfferedPrice = x.FreelancerOfferedPrice,
            //    FreelancerRoleId = x.FreelancerRoleId,
            //    FreelancerRole = x.FreelancerRole,
            //    PreviousAttachments = x.PreviousAttachments.ToList()
            //}
            //).FirstOrDefault();



            MODEL.ProjectModel.FreelancerListingModel freelancer;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetFreelancerDetails", new { UserId = UserId, projectId = projectId }, commandType: CommandType.StoredProcedure);
                freelancer = result.Read<MODEL.ProjectModel.FreelancerListingModel>().FirstOrDefault();
                if (freelancer != null)
                {
                    freelancer.PreviousAttachments = result.Read<MODEL.ProjectModel.ProjectFileModel>().ToList();
                }
            }


            if (freelancer == null)
            {
                //freelancer = (from usr in dbcontext.UserDetails
                //              join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
                //              join rol in dbcontext.Roles on usrcom.RoleId equals rol.RoleId
                //              where usr.UserId == UserId && usr.IsDeleted == false
                //              && rol.IsDeleted == false
                //              select new MODEL.ProjectModel.FreelancerListingModel
                //              {
                //                  FreelancerUserId = usr.UserId,
                //                  FreelancerEmail = usr.Email,
                //                  FreelancerName = usr.FirstName,
                //                  FreelancerProfilePhoto = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + usr.ProfilePhoto,
                //                  FreelancerProfileRate = usr.Rate,
                //                  FreelancerOfferedPrice = usr.Rate,
                //                  FreelancerRoleId = rol.RoleId,
                //                  FreelancerRole = rol.Name,
                //                  Status = "Not Sent"
                //              }).FirstOrDefault();

                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"select usr.UserId as FreelancerUserId,usr.Email as FreelancerEmail,usr.FirstName as FreelancerName, case when usr.ProfilePhoto is null then 'Uploads/Default/profile.png' else 'Uploads/Profile/Icon/'+ usr.ProfilePhoto end as FreelancerProfilePhoto,
                                        usr.Rate as FreelancerProfileRate,usr.Rate as FreelancerOfferedPrice,rol.RoleId as FreelancerRoleId,  rol.Name as FreelancerRole, 'Not Sent' as  Status                             
                                        from UserDetails usr inner join UserCompanyRoles usrcom on usr.UserId = usrcom.UserId inner join Roles rol on usrcom.RoleId = rol.RoleId 
                                        where usr.UserId = @UserId and usr.IsDeleted = 0  and rol.IsDeleted = 0  ";

                    freelancer = con.Query<MODEL.ProjectModel.FreelancerListingModel>(sqlquery, new { UserId = UserId }).FirstOrDefault();
                }


                freelancer.IsInvitationSent = false;
            }
            else
            {
                freelancer.IsInvitationSent = true;
            }


            return freelancer;
        }


        public string DeleteAttachmentFileFromProject(int projectId)
        {
            //dbcontext = new AppContext();
            string msg = "File Not Found";

            //var project = (from p in dbcontext.Projects
            //               where p.Id == projectId && p.IsDeleted == false
            //               select p).FirstOrDefault();

            ENTITY.Project project;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from Projects p where p.Id = @projectId and p.IsDeleted = 0";
                project = con.Query<ENTITY.Project>(sqlquery, new { projectId = projectId }).FirstOrDefault();
            }


            string filename = project.FileName;

            if (filename != string.Empty && filename != null)
            {
                if (File.Exists(HttpContext.Current.Server.MapPath("~/Uploads/Attachments/Mail_Attachments/") + filename))
                {
                    File.Delete(HttpContext.Current.Server.MapPath("~/Uploads/Attachments/Mail_Attachments/") + filename);
                    msg = "File Deleted";
                }
            }


            //project.FileName = null;
            //project.ActualFileName = null;
            //project.FilePath = null;
            //project.AttachmentDescription = null;

            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update Projects set FileName = null,ActualFileName = null,FilePath = null,AttachmentDescription = null  where Id = @projectId and IsDeleted = 0";
                project = con.Query<ENTITY.Project>(sqlquery, new { projectId = projectId }).FirstOrDefault();
            }


            return msg;
        }


        public HttpResponseMessage DownloadAttachmentFileFromProject(int projectId)
        {
            //dbcontext = new AppContext();

            //var project = (from p in dbcontext.Projects
            //               where p.Id == projectId && p.IsDeleted == false
            //               select p).FirstOrDefault();

            ENTITY.Project project;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from Projects p  where p.Id = @projectId and p.IsDeleted = 0 ";
                project = con.Query<ENTITY.Project>(sqlquery, new { projectId = projectId }).FirstOrDefault();
            }

            var path = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/Attachments/Mail_Attachments/ede044cd.jpg");
            //var path = project.FilePath ;
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = project.ActualFileName;
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentLength = stream.Length;
            return result;

        }


        public List<ProjectModel.FreelancerListingModel> GetInvitedFreelamcersOnlyNotAccepted(int projectId)
        {
            //dbcontext = new AppContext();

            //var freelancer = (from data in
            //                  (from freeinv in dbcontext.FreelancerInvitations
            //                   join usr in dbcontext.UserDetails on freeinv.FreelancerEmail equals usr.Email
            //                   join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
            //                   join rol in dbcontext.Roles on freeinv.RoleId equals rol.RoleId
            //                   where freeinv.ProjectId == projectId && usr.IsDeleted == false
            //                   && freeinv.IsRejected != true && rol.IsDeleted == false
            //                  select new { freeinv, usr, rol }
            //                  )
            //                  group data by new
            //                  {
            //                      data.freeinv.Id,
            //                      data.freeinv.SentBy,
            //                      data.usr.UserId,
            //                      data.usr.Email,
            //                      data.usr.FirstName,
            //                      data.usr.ProfilePhoto,
            //                      data.usr.Rate,
            //                      data.freeinv.FreelancerRate,
            //                      data.rol.RoleId,
            //                      data.rol.Name,
            //                      data.freeinv.Responsibilities,
            //                      data.freeinv.IsAccepted,
            //                      data.freeinv.IsRejected
            //                  } into groupedData




            //select new
            //                  {
            //                      FreelancerInvitationId = groupedData.Key.Id,
            //                      SentBy = groupedData.Key.SentBy,
            //                      FreelancerUserId = groupedData.Key.UserId,
            //                      FreelancerEmail = groupedData.Key.Email,
            //                      FreelancerName = groupedData.Key.FirstName,
            //                      FreelancerProfilePhoto = groupedData.Key.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + groupedData.Key.ProfilePhoto,
            //                      FreelancerProfileRate = groupedData.Key.Rate,
            //                      FreelancerOfferedPrice = groupedData.Key.FreelancerRate,
            //                      FreelancerRoleId = groupedData.Key.RoleId,
            //                      FreelancerRole = groupedData.Key.Name,
            //                      FreelancerResponsibilities = groupedData.Key.Responsibilities,
            //                      IsInvitationSent = true,
            //                      Status = groupedData.Key.IsAccepted == true ? "Accepted" : groupedData.Key.IsRejected == true ? "Rejected" : "Sent",




            //                       PreviousAttachments = (from at in dbcontext.ProjectInvitationAttachments
            //                                             where at.UserId == groupedData.Key.UserId && at.ProjectId == projectId
            //                                             select new MODEL.ProjectModel.ProjectFileModel
            //                                             {

            //                                                 FileName = at.FileName,
            //                                                 ActualFileName = at.ActualFileName,
            //                                                 FilePath = at.FilePath
            //                                             })
            //                  });




            //List<ProjectModel.FreelancerListingModel> FreelancersList1 = freelancer.AsEnumerable().Select(x =>
            //new MODEL.ProjectModel.FreelancerListingModel
            //{
            //    FreelancerInvitationId = x.FreelancerInvitationId,
            //    SentBy = x.SentBy,
            //    FreelancerUserId = x.FreelancerUserId,
            //    FreelancerEmail = x.FreelancerEmail,
            //    FreelancerName = x.FreelancerName,
            //    FreelancerProfilePhoto = x.FreelancerProfilePhoto,
            //    FreelancerProfileRate = x.FreelancerProfileRate,
            //    FreelancerOfferedPrice = x.FreelancerOfferedPrice,
            //    FreelancerRoleId = x.FreelancerRoleId,
            //    FreelancerRole = x.FreelancerRole,
            //    FreelancerResponsibilities = x.FreelancerResponsibilities,
            //    IsInvitationSent = x.IsInvitationSent,
            //    Status = x.Status,
            //    PreviousAttachments = x.PreviousAttachments.ToList()
            //}
            //).ToList();


            List<ProjectModel.FreelancerListingModel> FreelancersList;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetInvitedFreelamcersOnlyNotAccepted", new { projectId = projectId }, commandType: CommandType.StoredProcedure);
                FreelancersList = result.Read<ProjectModel.FreelancerListingModel>().ToList();
                if (FreelancersList != null)
                {
                    var PreviousAttachments = result.Read<MODEL.ProjectModel.ProjectFileModel>().ToList();
                    foreach (var item in FreelancersList)
                    {
                        item.PreviousAttachments = PreviousAttachments.Where(x => x.UserId == item.FreelancerUserId).ToList();
                    }
                }
            }

            return FreelancersList;
        }


        public MODEL.ProjectModel.InternalUserDetailsModel GetInternalUserDetails(string UserId, int projectId)
        {
            //dbcontext = new AppContext();

            //MODEL.ProjectModel.InternalUserDetailsModel UserDetail = (from usr in dbcontext.UserDetails
            //                                                          join prjctusr in dbcontext.ProjectUsers on usr.UserId equals prjctusr.UserId
            //                                                          where prjctusr.ProjectId == projectId
            //                                                          && usr.UserId == UserId && usr.IsDeleted == false
            //                                                          select new MODEL.ProjectModel.InternalUserDetailsModel
            //                                                          {
            //                                                              FirstName = usr.FirstName,
            //                                                              UserId = usr.UserId,
            //                                                              ProfilePhoto = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + usr.ProfilePhoto,
            //                                                              Status = "Sent",
            //                                                              RoleId = prjctusr.RoleId
            //                                                          }).FirstOrDefault();

            MODEL.ProjectModel.InternalUserDetailsModel UserDetail;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select usr.FirstName,usr.UserId, case when usr.ProfilePhoto is null then 'Uploads/Default/profile.png' else 'Uploads/Profile/Icon/'+ usr.ProfilePhoto end as ProfilePhoto,
                                   'Sent' as Status , prjctusr.RoleId  as RoleId                         
                                    from UserDetails usr inner join ProjectUsers prjctusr on usr.UserId = prjctusr.UserId where prjctusr.ProjectId = @projectId and usr.UserId = @UserId and usr.IsDeleted = 0";

                UserDetail = con.Query<MODEL.ProjectModel.InternalUserDetailsModel>(sqlquery, new { projectId = projectId, UserId = UserId }).FirstOrDefault();
            }



            if (UserDetail == null)
            {
                //UserDetail = (from usr in dbcontext.UserDetails
                //              where usr.UserId == UserId && usr.IsDeleted == false
                //              select new MODEL.ProjectModel.InternalUserDetailsModel
                //              {
                //                  FirstName = usr.FirstName,
                //                  UserId = usr.UserId,
                //                  ProfilePhoto = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + usr.ProfilePhoto,
                //                  Status = "Not Sent",
                //                  RoleId = 0
                //              }).FirstOrDefault();

                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"select usr.FirstName,usr.UserId, case when usr.ProfilePhoto is null then 'Uploads/Default/profile.png' else 'Uploads/Profile/Icon/'+ usr.ProfilePhoto end as ProfilePhoto,
                                   'Sent' as Status , 0  as RoleId                         
                                    from UserDetails usr where usr.UserId = @UserId and usr.IsDeleted = 0";

                    UserDetail = con.Query<MODEL.ProjectModel.InternalUserDetailsModel>(sqlquery, new { UserId = UserId }).FirstOrDefault();
                }

            }



            return UserDetail;
        }

        public string GetFileName(int projectId)
        {
            //dbcontext = new AppContext();

            //var proj = (from p in dbcontext.Projects
            //            where p.Id == projectId && p.IsDeleted == false
            //            select p).FirstOrDefault();

            string FileName = string.Empty;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select p.FileName from Projects p where p.Id = @projectId and p.IsDeleted = 0";
                FileName = con.Query(sqlquery, new { projectId = projectId }).FirstOrDefault();
            }

            return FileName;
        }

        public List<vendorModel> GetProjectVendor(int CompanyId, int ProjectId)
        {

            List<vendorModel> objVendorModel;

            using (var con = new SqlConnection(ConnectionString))
            {
                //        string query = @"select UD.Id VendorId,Ud.Email EmailAddress ,CONCAT(Ud.FirstName+ ' ',Ud.LastName + ' ' + '( '+ C.Name + ' )') Name
                //                        , C.Name as VendorCompanyName,c.Name as HIredByCompany
                //                        ,Getdate() CreatedDate                                          
                //                        from CompanyVendor CV 
                //inner join UserDetails Ud on Ud.Id = CV.VendorId 
                //inner join Companies C on Ud.CompanyId = C.Id
                //                        where CV.CompanyId =  @CompanyId and Ud.RoleId <> 1";
                string query = @"select UD.Id VendorId,
                                Ud.Email EmailAddress ,CONCAT(Ud.FirstName+ ' ',Ud.LastName + ' ' + '( '+ C.Name + ' )') Name
                                , C.Name as VendorCompanyName,c.Name as HIredByCompany
                                ,Getdate() CreatedDate,Convert(bit,'False') IsAdded,CVA.VendorId VendorCompanyId,CVA.Id Id                                          
                                from CompanyVendorPOCAccount CV 
							 inner join CompanyVendorAccount CVA on CVA.Id = CV.AccId 
								and CVA.CompanyId=@CompanyId
								 and cva.IsDeleted<>1
								inner join UserDetails Ud on Ud.UserId = CV.UserId 
								inner join Companies C on Ud.CompanyId = C.Id";
                objVendorModel = con.Query<vendorModel>(query, new { CompanyId = CompanyId, ProjectId = ProjectId }).ToList();

            }

            return objVendorModel;



        }
        public int CheckProjectExist(int CompanyId, string ProjectName, int ProjectId)
        {
            int ResultId = 0;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query;
                if (ProjectId == 0)
                {
                    // query new Project creation
                    query = @"If Not Exists (select prj.Name  from Projects prj where Name = @ProjectName and  prj.CompanyId = @CompanyId)
                                Begin select ResultId = 1 end else begin select -1 end";
                }
                else
                {
                    // query edit case of project
                    query = @"If Not Exists (select prj.Name  from Projects prj where Name = @ProjectName and  prj.CompanyId = @CompanyId and prj.Id <> @ProjectId)
                                Begin select ResultId = 1 end else begin select -1 end";
                }
                ResultId = con.Query<int>(query, new { CompanyId = CompanyId, ProjectName = ProjectName, ProjectId = ProjectId }).SingleOrDefault();

            }
            return ResultId;

        }

        public List<projectQuotedModel> GetQuotedProjectList(projectQuotedModel Modal)
        {

            List<projectQuotedModel> objQuoteModel;

            using (var con = new SqlConnection(ConnectionString))
            {

                objQuoteModel = con.Query<projectQuotedModel>("Ups_GetQuotedProjectList", new
                {
                    CompanyId = Modal.CompanyId,
                    Id = Modal.Id,
                    Name = Modal.Name,
                    ProjectId = Modal.ProjectId,
                    PageIndex = Modal.PageIndex,
                    PageSize = Modal.PageSize

                }, commandType: CommandType.StoredProcedure).ToList();

            }

            return objQuoteModel;



        }

        public int SaveNewProject(ProjectModel.NewCreateProjectModel model)
        {
            string msg = string.Empty;
            string ProjectName;
            ProjectName = model.ProjectName;
            int datasave = 0;
            List<ProjectPhaseModel> objPhaseList = new List<ProjectPhaseModel>();
            List<PhaseDocumentsNameModel> objList = new List<PhaseDocumentsNameModel>();

            List<ProjectPhaseCustom> objProjectPhaseList = new List<ProjectPhaseCustom>();
            List<ProjectRiskCustom> objProjectRiskList = new List<ProjectRiskCustom>();
            int count = 1;
            foreach(var item in model.RiskList)
            {
                ProjectRiskCustom ObjItem = new ProjectRiskCustom();
                ObjItem.Id = count++;
                ObjItem.ProjectId = 0;
                ObjItem.RiskTitle = item.RiskTitle;
                ObjItem.Description = item.Description;
                ObjItem.Severity = item.Severity;
                ObjItem.Status = item.Status;
                for (int i = 0; i < item.RiskComments.Count; i++)
                {
                    ObjItem.CommentList += item.RiskComments[i].Comment + ",";
                    ObjItem.AgainstComment += item.RiskComments[i].AgainstComment==null?"0," : item.RiskComments[i].AgainstComment.ToString()+",";
                }
                objProjectRiskList.Add(ObjItem);

            }
            count = 1;
            foreach (var item in model.PhaseList)
            {

                ProjectMileStoneModel objProjectPhase = new ProjectMileStoneModel();

                ProjectPhaseCustom ObjItem = new ProjectPhaseCustom();
                ObjItem.Name = item.Name;
                ObjItem.Budget = item.Budget;
                ObjItem.Description = item.Description;
                ObjItem.Id = count;
                ObjItem.PhaseId = 0;

                StringBuilder FileNameList = new StringBuilder();
                StringBuilder ActualFileNameList = new StringBuilder();
                StringBuilder FilePathList = new StringBuilder();
                StringBuilder MileNameList = new StringBuilder();
                StringBuilder BudgetList = new StringBuilder();
                StringBuilder DescriptionList = new StringBuilder();
                StringBuilder EstimatedHourList = new StringBuilder();

                for (int i = 0; i < item.PhaseDocumentsName.Count; i++)
                {
                    ObjItem.FileNameList += item.PhaseDocumentsName[i].FileName + ",";
                    ObjItem.ActualFileNameList += item.PhaseDocumentsName[i].ActualFileName + ",";
                    ObjItem.FilePathList += item.PhaseDocumentsName[i].FilePath + ",";
                }


                for (int i = 0; i < item.MileStone.Count; i++)
                {
                    ObjItem.MileNameList += item.MileStone[i].Name + "`";
                    ObjItem.BudgetList += item.MileStone[i].Budget + "`";
                    ObjItem.DescriptionList += item.MileStone[i].Description + "`";
                    ObjItem.EstimatedHourList += item.MileStone[i].EstimatedHours + "`";
                }


                objProjectPhaseList.Add(ObjItem);
                count++;

            }
            StringBuilder VendorList = new StringBuilder();
            foreach (var Item in model.VendorList)
            {
                if (Item.IsAdded == true)
                {
                    VendorList.Append(Item.VendorId).Append(',');
                }

            }
            List<VendorCompanyMail> ObjVendorcompList = new List<VendorCompanyMail>();
            MailSendercompanyModel ObjMailSender;
            MailReceiverVendorModel ObjMailReceiver;


            using (var con = new SqlConnection(ConnectionString))
            {
                var Result = con.QueryMultiple("usp_SaveNewProject", new
                {
                    UserId = model.UserId,
                    CompanyId = model.CompanyId,
                    ProjectName = model.ProjectName,
                    ProjectDescription = model.ProjectDescription,
                    projectHours = model.ProjectHours,
                    projectBudget = model.ProjectBudget,
                    AccessLevel = model.AccessLevel,
                    FileNameList = model.FileName,
                    ActualFileNameList = model.ActualFileName,
                    FilePathList = model.FilePath,
                    VendorList = VendorList.ToString(),
                    StartDate = model.ProjectStartDate,
                    EndDate = model.ProjectEndDate,
                    IsQuoteSent = model.IsQuoteSent,
                    CustomControlData = model.CustomControlData,
                    PhaseList = objProjectPhaseList.AsTableValuedParameter("ProjectPhaseList", new[]
                    { "Id","PhaseId", "Name", "Description", "Budget",  "FileNameList", "ActualFileNameList", "FilePathList", "MileNameList",
                        "BudgetList", "DescriptionList","EstimatedHourList" }),
                    RiskList = objProjectRiskList.AsTableValuedParameter("ProjectRiskList", new[]
                    { "Id","ProjectId", "RiskTitle", "Description", "Severity",  "Status", "CommentList", "AgainstComment" }),

                }, commandType: CommandType.StoredProcedure);

                ObjMailSender = Result.Read<MailSendercompanyModel>().FirstOrDefault();
                ObjMailReceiver  = Result.Read<MailReceiverVendorModel>().FirstOrDefault();
                   




                datasave = model.IsQuoteSent == true ? 2 : 1;

                if (model.IsQuoteSent && ObjVendorcompList != null)
                {

                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForionVendorAddInProject(model.VendorList, ObjMailSender, ProjectName);
                        if (ObjMailReceiver != null)
                        {
                            ThreadForSendMailVendorAdminProfileAddInProject(ObjMailReceiver, ObjMailSender, ProjectName);
                        }
                    }

                    ));
                    childref.Start();

                }

            }


            return datasave;
        }


        // sending mail for registration
        private void ThreadForionVendorAddInProject(List<VendorForCompany> objCompanysend, MailSendercompanyModel ObjMailSender, string ProjectName)
        {

            foreach (VendorForCompany item in objCompanysend.Where(x => x.IsAdded == true))
            {
                String emailbody = null;
                string Subject = string.Empty;
                string subjectBysender = "Vendor Invitation";

                XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                var MailModule = from r in xdoc.Descendants("MailModule").
                                 Where(r => (string)r.Attribute("id") == "SendToVendorAddedInProject")
                                 select new
                                 {
                                     mailbody = r.Element("Body").Value,
                                     subject = r.Element("Subject").Value
                                 };

                foreach (var Mailitem in MailModule)
                {
                    emailbody = Mailitem.mailbody;
                    Subject = Mailitem.subject;
                }

                emailbody = emailbody.Replace("@@ReceiverName", item.Name).Replace("@@SenderName", ObjMailSender.SenderName)
                    .Replace("@@SenderCompanyName", ObjMailSender.SenderCompanyName).Replace("@@ProjectName", ProjectName).Replace("@@subjectBysender", subjectBysender);
                EmailUtility.SendMailInThread(item.EmailAddress, subjectBysender, emailbody);
            }

        }

        private void ThreadForSendMailVendorAdminProfileAddInProject(MailReceiverVendorModel ObjMailReceiver ,MailSendercompanyModel ObjMailSender, string ProjectName)
        {

        
                String emailbody = null;
                string Subject = string.Empty;
                string subjectBysender = "Vendor Invitation";

                XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                var MailModule = from r in xdoc.Descendants("MailModule").
                                 Where(r => (string)r.Attribute("id") == "SendToVendorAdminProfileAddedInProject")
                                 select new
                                 {
                                     mailbody = r.Element("Body").Value,
                                     subject = r.Element("Subject").Value
                                 };

                foreach (var Mailitem in MailModule)
                {
                    emailbody = Mailitem.mailbody;
                    Subject = Mailitem.subject;
                }

                emailbody = emailbody.Replace("@@ReceiverName", ObjMailReceiver.ReceiverName).Replace("@@SenderName", ObjMailSender.SenderName)
                    .Replace("@@SenderCompanyName", ObjMailSender.SenderCompanyName).Replace("@@ProjectName", ProjectName).Replace("@@subjectBysender", subjectBysender)
            .Replace("@@Pocs", ObjMailReceiver.Pocs);
            EmailUtility.SendMailInThread(ObjMailReceiver.Email, subjectBysender, emailbody);
            

        }


        public List<VendorQuoteModal> GetQuotedSubmitbyVendor(string ProjectId)
        {

            List<VendorQuoteModal> ObjVendorQuoteList = new List<VendorQuoteModal>();

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"SELECT Ud.Id as VendorId,  concat(Ud.FirstName +' ',  Ud.LastName) Name, p.Id ProjectId,  Budget TotalBudget,
                                    Hours TotalHours,Ud.IsActive, QuoteSenton CreatedDate FROM ProjectVendorQuote Pvq
                                    inner join projects P on p.Id = Pvq.ProjectId
                                    left join UserDetails Ud on Ud.Id = VendorId where P.Id = @ProjectId  and Pvq.IsActive = 1";

                ObjVendorQuoteList = con.Query<VendorQuoteModal>(sqlquery, new { ProjectId = ProjectId }).ToList();

            }

            return ObjVendorQuoteList;

        }


        public VendorQuoteModal GetProjectQuotedByVendor(int CompanyId, int ProjectId, int VendorId)
        {
            VendorQuoteModal ObjVendorQuote = new VendorQuoteModal();
            List<ProjectDocument> ObjProjectDocList;
            List<vendorPhaseModal> ObjvendorPhaseList;
            List<PhaseDocument> ObjPhaseDocumentList;
            List<vendorMilestone> ObjvendorMileList;
            List<ResourceModal> ObjResourceList;
            CustomControlValueModel ObjCustomControlValueList;
            List<CustomControlUIModel> ObjCustomControlUIList;
            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetQuotedProjectForCompany", new { ProjectId = ProjectId, VendorId = VendorId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                ObjVendorQuote = Result.Read<VendorQuoteModal>().FirstOrDefault();
                ObjProjectDocList = Result.Read<ProjectDocument>().ToList();
                ObjvendorPhaseList = Result.Read<vendorPhaseModal>().ToList();
                ObjPhaseDocumentList = Result.Read<PhaseDocument>().ToList();
                ObjvendorMileList = Result.Read<vendorMilestone>().ToList();
                ObjResourceList = Result.Read<ResourceModal>().ToList();
                ObjCustomControlUIList = Result.Read<CustomControlUIModel>().ToList();
                ObjCustomControlValueList = Result.Read<CustomControlValueModel>().FirstOrDefault();

                foreach (vendorPhaseModal item in ObjvendorPhaseList)
                {

                    List<PhaseDocument> ObjPhasedocList = ObjPhaseDocumentList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.PhaseDocumentList = new List<PhaseDocument>();
                    foreach (PhaseDocument item1 in ObjPhasedocList)
                    {
                        item.PhaseDocumentList.Add(item1);
                    }
                    List<vendorMilestone> PMilestoneList = ObjvendorMileList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.MileStone = new List<vendorMilestone>();
                    foreach (vendorMilestone item2 in PMilestoneList)
                    {
                        item.MileStone.Add(item2);
                    }
                    
                }


                int i = 0;
                foreach (CustomControlUIModel item3 in ObjCustomControlUIList)
                {
                    List<CustomUIDropDownModel> ObjCustDrop = new List<CustomUIDropDownModel>();
                    List<CustomUIRadioButtonModel> ObjRadioList = new List<CustomUIRadioButtonModel>();
                    if (item3.Values != null && item3.ControlType == "Dropdown")
                    {
                        string[] drop = item3.Values.Split(',');
                        int j = 1;
                        foreach (string item1 in drop)
                        {
                            CustomUIDropDownModel obj = new CustomUIDropDownModel();
                            obj.Name = item1;
                            obj.Id = j;
                            ObjCustDrop.Add(obj);
                            j++;
                        }
                    }
                    if (item3.Values != null && item3.ControlType == "Radio buttons")
                    {
                        string[] drop = item3.Values.Split(',');
                        int k = 1;
                        foreach (string item1 in drop)
                        {
                            CustomUIRadioButtonModel obj = new CustomUIRadioButtonModel();
                            obj.Name = item1;
                            obj.Id = k;
                            ObjRadioList.Add(obj);
                            k++;
                        }
                    }

                    ObjCustomControlUIList[i].FieldModel = ObjCustomControlUIList[i].FieldName.Replace(' ', '_');
                    ObjCustomControlUIList[i].CustomDropDownList = ObjCustDrop;
                    ObjCustomControlUIList[i].CustomRadioButtonList = ObjRadioList;
                    i++;

                }

            }
            ObjVendorQuote.PhaseList = ObjvendorPhaseList;
            ObjVendorQuote.ProjectDocumentList = ObjProjectDocList;
            ObjVendorQuote.ResourcesList = ObjResourceList;
            ObjVendorQuote.CustomControlUIList = ObjCustomControlUIList;
            ObjVendorQuote.CustomControlValueModel = ObjCustomControlValueList;
            return ObjVendorQuote;

        }



        public int saveReasonAcceptOrRejectQuote(ReasonAcceptRejectModal modal)
        {
            int Issave = 0;
            string sqlquery = "";
            string MailIdSendToVendorAboutProject = "";


            VendorCompanyMail objVendorCompanyMail;
            MailVendorAdminProfileModel ObjVendorAdminMailmodel = new MailVendorAdminProfileModel();
            using (var con = new SqlConnection(ConnectionString))
            {

                if (modal.IsAcceptOrReject == 1)
                {
                    sqlquery = @"Declare @userId Nvarchar(max)
                                select @userId = Ud.UserId from UserDetails Ud where Ud.Id =  @VendorId
                                 update dbo.VendorInvitations set IsApproved = 1,ReasonReject = @RejectOrAccept
                                   where ProjectId = @ProjectId and UserId = @userId 
                                    update dbo.Projects set IsAssigned = 1,IsComplete = 1 where Id = @ProjectId 
                                    update ProjectVendor set IsHired = 1,HiredOn = GetDate() where ProjectId = @ProjectId  and VendorId = @VendorId
                                 select concat(ud.FirstName +' ',ud.LastName) ReceiverName,ud.Email Email
                                ,(select concat(ud.FirstName +' ',ud.LastName) SenderName from UserDetails ud where ud.Id = @CompanyId) SenderName
                                 from UserDetails ud where ud.Id = @VendorId

                                 --- mail send to vendor admin profile
                                Declare @VendorCompanyId int
                                Declare @VendorName nvarchar(max)
                                select  @VendorCompanyId = ud.CompanyId,@VendorName = Concat(ud.FirstName + ' ',ud.LastName) 
                                from UserDetails ud where ud.Id = @VendorId

                                select @VendorName SenderName,Concat(ud.FirstName + ' ',ud.LastName) ReceiverName,ud.Email 
                                from UserDetails ud where ud.CompanyId = @VendorCompanyId and ud.RoleId = 1";
                    MailIdSendToVendorAboutProject = "SendToVendorProjectQuoteApproved";

                    var Result = con.QueryMultiple(sqlquery, new
                    { ProjectId = modal.ProjectId, CompanyId = modal.CompanyId, VendorId = modal.VendorId, RejectOrAccept = modal.RejectOrAccept });

                    objVendorCompanyMail = Result.Read<VendorCompanyMail>().FirstOrDefault();
                    ObjVendorAdminMailmodel = Result.Read<MailVendorAdminProfileModel>().FirstOrDefault();

                }
                else
                {
                    sqlquery = @"Declare @userId Nvarchar(max)
                                select @userId = Ud.UserId from UserDetails Ud where Ud.Id =  @VendorId
                                 update dbo.VendorInvitations set IsApproved = 0,ReasonReject = @RejectOrAccept
                                 where ProjectId = @ProjectId and userId = @userId
                                 
                               update ProjectVendorQuote set IsActive = 0 where ProjectId = @ProjectId and VendorId = @VendorId

                                select concat(ud.FirstName +' ',ud.LastName) ReceiverName,ud.Email Email
                                ,(select concat(ud.FirstName +' ',ud.LastName) SenderName from UserDetails ud where ud.Id = @CompanyId) SenderName
                                 from UserDetails ud where ud.Id = @VendorId";
                    MailIdSendToVendorAboutProject = "SendToVendorProjectQuoteRejected";
                    objVendorCompanyMail = con.Query<VendorCompanyMail>(sqlquery, new { ProjectId = modal.ProjectId, CompanyId = modal.CompanyId, VendorId = modal.VendorId, RejectOrAccept = modal.RejectOrAccept }).SingleOrDefault();

                }


                // send mail to Vendor 
                if (objVendorCompanyMail != null)
                {
                    Issave = 1;
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForionVendorAddInProject(objVendorCompanyMail, modal.ProjectName, modal.RejectOrAccept, MailIdSendToVendorAboutProject);

                    }


                    ));
                    childref.Start();

                }

                // send mail to Vendor Admin if exist then

                if (ObjVendorAdminMailmodel != null)
                {

                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForSendMailVendorAdimnProfile(ObjVendorAdminMailmodel, modal.ProjectName);

                    }

                    ));
                    childref.Start();


                }
            }

            return Issave;

        }

        private void ThreadForionVendorAddInProject(VendorCompanyMail objVendorCompanyMail, string Name, string RejectOrAccept, string MailIdSendToVendorAboutProject)
        {
            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == MailIdSendToVendorAboutProject)
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", objVendorCompanyMail.ReceiverName).Replace("@@SenderName", objVendorCompanyMail.SenderName)
                .Replace("@@SenderCompanyName", objVendorCompanyMail.SenderCompanyName).Replace("@@MailProjectName", Name)
                .Replace("@@Message", RejectOrAccept);
            EmailUtility.SendMailInThread(objVendorCompanyMail.Email, Subject, emailbody);


        }

        private void ThreadForSendMailVendorAdimnProfile(MailVendorAdminProfileModel objVendorCompanyMail, string ProjectName)
        {
            String emailbody = null;
            string Subject = string.Empty;
            DateTime date = DateTime.Now;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "VendorAdminProfileProjectAwardedSendMail")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", objVendorCompanyMail.ReceiverName).Replace("@@SenderName", objVendorCompanyMail.SenderName)
               .Replace("@@ProjectName", ProjectName).Replace("@@Date", date.ToString());
            EmailUtility.SendMailInThread(objVendorCompanyMail.Email, Subject, emailbody);


        }


        public ProjectModel.EditNewProjectModel GetNewProjectForEdit(int ProjectId, int CompanyId)
        {

            ProjectModel.EditNewProjectModel ObjEditProjectList = new EditNewProjectModel();
            List<newProjectDocument> ObjProjectDocList;
            List<ProjectPhaseModel> ObjPhaseList;
            List<PhaseDocumentsNameModel> ObjPhaseDocList;
            List<ProjectMileStoneModel> ObjPhaseMileStone;
            List<VendorForCompany> ObjVendorForComList;
            CustomControlValueModel ObjCustomControlValueList;
            List<CustomControlUIModel> ObjCustomControlUIList;
            List<ProjectRiskModel> ObjProjectRiskList;
            List<ProjectRiskCommentsModel> ObjProjectRiskCommentsList;

            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetNewProjectForEdit", new { ProjectId = ProjectId, CompanyId = CompanyId }, commandType: CommandType.StoredProcedure);
                ObjEditProjectList = Result.Read<ProjectModel.EditNewProjectModel>().FirstOrDefault();

                ObjProjectRiskList = Result.Read<ProjectRiskModel>().ToList();
                ObjProjectRiskCommentsList = Result.Read<ProjectRiskCommentsModel>().ToList();
                ObjProjectDocList = Result.Read<newProjectDocument>().ToList();


                ObjPhaseList = Result.Read<ProjectPhaseModel>().ToList();
                ObjPhaseDocList = Result.Read<PhaseDocumentsNameModel>().ToList();
                ObjPhaseMileStone = Result.Read<ProjectMileStoneModel>().ToList();
                ObjVendorForComList = Result.Read<VendorForCompany>().ToList();
                ObjCustomControlUIList = Result.Read<CustomControlUIModel>().ToList();
                ObjCustomControlValueList = Result.Read<CustomControlValueModel>().FirstOrDefault();
              
                foreach (ProjectPhaseModel Item in ObjPhaseList)
                {

                    List<PhaseDocumentsNameModel> ItemPhasedoc = ObjPhaseDocList.Where(x => x.PhaseId == Item.PhaseId).ToList();
                    Item.PhaseDocumentsName = new List<PhaseDocumentsNameModel>();
                    foreach (PhaseDocumentsNameModel Item1 in ItemPhasedoc){
                        Item.PhaseDocumentsName.Add(Item1);
                    }
                    Item.MileStone = new List<ProjectMileStoneModel>();
                    List<ProjectMileStoneModel> ObjProjectMstoneList = ObjPhaseMileStone.Where(y => y.PhaseId == Item.PhaseId).ToList();
                    foreach (ProjectMileStoneModel Item2 in ObjProjectMstoneList){
                        Item.MileStone.Add(Item2);
                    }

                }


                foreach (ProjectRiskModel Item in ObjProjectRiskList)
                {
                    
                    List<ProjectRiskCommentsModel> ItemRiskComment = ObjProjectRiskCommentsList.Where(x => x.RiskId == Item.Id).ToList();
                    Item.RiskComments = new List<ProjectRiskCommentsModel>();
                    foreach (ProjectRiskCommentsModel Item1 in ItemRiskComment)
                    {
                        Item.RiskComments.Add(Item1);
                    }
                }
                int i = 0;
                foreach (CustomControlUIModel item in ObjCustomControlUIList)
                {
                    List<CustomUIDropDownModel> ObjCustDrop = new List<CustomUIDropDownModel>();
                    List<CustomUIRadioButtonModel> ObjRadioList = new List<CustomUIRadioButtonModel>();
                    if (item.Values != null && item.ControlType == "Dropdown")
                    {
                        string[] drop = item.Values.Split(',');
                        int j = 1;
                        foreach (string item1 in drop)
                        {
                            CustomUIDropDownModel obj = new CustomUIDropDownModel();
                            obj.Name = item1;
                            obj.Id = j;
                            ObjCustDrop.Add(obj);
                            j++;
                        }
                    }
                    if (item.Values != null && item.ControlType == "Radio buttons")
                    {
                        string[] drop = item.Values.Split(',');
                        int k = 1;
                        foreach (string item1 in drop)
                        {
                            CustomUIRadioButtonModel obj = new CustomUIRadioButtonModel();
                            obj.Name = item1;
                            obj.Id = k;
                            ObjRadioList.Add(obj);
                            k++;
                        }
                    }

                    ObjCustomControlUIList[i].FieldModel = ObjCustomControlUIList[i].FieldName.Replace(' ', '_');
                    ObjCustomControlUIList[i].CustomDropDownList = ObjCustDrop;
                    ObjCustomControlUIList[i].CustomRadioButtonList = ObjRadioList;
                    i++;
                }
                
                ObjEditProjectList.ProjectDocList = ObjProjectDocList;
                ObjEditProjectList.PhaseList = ObjPhaseList;
                ObjEditProjectList.VendorList = ObjVendorForComList;
                ObjEditProjectList.CustomControlValueModel = ObjCustomControlValueList;
                ObjEditProjectList.CustomControlUIList = ObjCustomControlUIList;
                ObjEditProjectList.RiskList = ObjProjectRiskList;
            }

            return ObjEditProjectList;

        }


        public int EditNewProject(ProjectModel.NewCreateProjectModel model)
        {
            string msg = string.Empty;
            string ProjectName;
            ProjectName = model.ProjectName;
            int datasave = 0;
            List<ProjectPhaseModel> objPhaseList = new List<ProjectPhaseModel>();
            List<PhaseDocumentsNameModel> objList = new List<PhaseDocumentsNameModel>();

            List<ProjectMileStoneModel> ObjMistoneList = new List<ProjectMileStoneModel>();
            List<ProjectPhaseCustom> ObjProjectPhaseList = new List<ProjectPhaseCustom>();
            List<EditProjectRiskCustom> ObjProjectRiskList = new List<EditProjectRiskCustom>();

            List<EditProjectRiskCustom> objProjectRiskList = new List<EditProjectRiskCustom>();
            int count = 1;
            foreach (var item in model.RiskList)
            {
                EditProjectRiskCustom ObjItem = new EditProjectRiskCustom();
                ObjItem.Id = count++;
                ObjItem.RiskId = item.Id;
                ObjItem.ProjectId = 0;
                ObjItem.RiskTitle = item.RiskTitle;
                ObjItem.Description = item.Description;
                ObjItem.Severity = item.Severity;
                ObjItem.Status = item.Status;
                ObjItem.IsDeleted = item.IsDeleted;
                for (int i = 0; i < item.RiskComments.Count; i++)
                {
                    ObjItem.CommentList += item.RiskComments[i].Comment + ",";
                    ObjItem.AgainstComment += item.RiskComments[i].AgainstComment + ",";
                    ObjItem.CommentIsDeletedList += item.RiskComments[i].IsDeleted == true ? "1," : "0,";
                }
                objProjectRiskList.Add(ObjItem);

            }
            int Id = 1;
            foreach (var item in model.PhaseList)
            {


                ProjectPhaseCustom ObjItem = new ProjectPhaseCustom();

                ObjItem.Name = item.Name;
                ObjItem.Budget = item.Budget;
                ObjItem.Description = item.Description;
                ObjItem.PhaseId = item.PhaseId;
                ObjItem.Id = Id;
                foreach (var item1 in item.PhaseDocumentsName)
                {
                    ProjectPhaseCustom ObjItem1 = new ProjectPhaseCustom();
                    ObjItem1.Id = Id;
                    ObjItem1.PhaseId = item.PhaseId;
                    ObjItem1.PhaseAttachementId = item1.Id;
                    ObjItem1.Name = item.Name;
                    ObjItem1.Budget = item.Budget;
                    ObjItem1.Description = item.Description;
                    ObjItem1.FileNameList = item1.FileName;
                    ObjItem1.ActualFileNameList = item1.ActualFileName;
                    ObjItem1.FilePathList = item1.FilePath;
                    ObjItem1.MileNameList = "";
                    ObjItem1.BudgetList = "";
                    ObjItem1.DescriptionList = "";
                    ObjItem1.EstimatedHourList = "";
                    ObjProjectPhaseList.Add(ObjItem1);


                }
                ObjItem.FileNameList = "";
                ObjItem.ActualFileNameList = "";
                ObjItem.FilePathList = "";
                ObjItem.MileNameList = "";
                ObjItem.BudgetList = "";
                ObjItem.DescriptionList = "";
                ObjItem.EstimatedHourList = "";

                if (item.PhaseDocumentsName.Count == 0)
                {
                    ObjProjectPhaseList.Add(ObjItem);

                }


                foreach (var item2 in item.MileStone)
                {

                    ProjectMileStoneModel Obj = new ProjectMileStoneModel();
                    Obj.PhaseId = item.PhaseId;
                    Obj.Name = item2.Name;
                    Obj.Description = item2.Description;
                    Obj.EstimatedHours = 0;
                    Obj.Budget = item2.Budget;
                    Obj.MilestoneId = item2.MilestoneId;
                    Obj.ProjectId = model.ProjectId;
                    Obj.TrackId = 0;
                    Obj.Id = Id;
                    ObjMistoneList.Add(Obj);
                }

                Id++;

            }
            StringBuilder VendorListId = new StringBuilder();
            StringBuilder VendorListIsAdded = new StringBuilder();
            StringBuilder ListId = new StringBuilder();
            foreach (var Item in model.VendorList)
            {
                VendorListId.Append(Item.VendorId).Append(',');
                VendorListIsAdded.Append(Item.IsAdded).Append(',');
                ListId.Append(Item.Id).Append(',');



            }
            
            VendorCompanyMail ObjMailAfterList;
            List<VendorCompanyMail> ObjMailBeforeList = new List<VendorCompanyMail>();
            List<VendorCompanyMail> ObjCompMailNewVendorList = new List<VendorCompanyMail>();
            List<VendorCompanyMail> ObjCompMailEditProjectVendorList = new List<VendorCompanyMail>();
            List<VendorCompanyMail> ObjCompMailDeleteProjectVendorList = new List<VendorCompanyMail>();

            foreach (VendorForCompany item in model.VendorList)
            {
                VendorCompanyMail obj = new VendorCompanyMail();
                if (item.Id == 0 && item.IsAdded == true)
                {
                    obj.Id = item.Id;
                    obj.Email = item.EmailAddress;
                    obj.ReceiverName = item.Name;
                    obj.SenderCompanyName = item.CompanyName;
                    ObjCompMailNewVendorList.Add(obj);
                }
                else if (item.Id > 0 && item.IsAdded == true)
                {
                    obj.Id = item.Id;
                    obj.Email = item.EmailAddress;
                    obj.ReceiverName = item.Name;
                    obj.SenderCompanyName = item.CompanyName;
                    ObjCompMailEditProjectVendorList.Add(obj);
                }
                //delete vendor
                else if (item.Id > 0 && item.IsAdded == false)
                {
                    obj.Id = item.Id;
                    obj.Email = item.EmailAddress;
                    obj.ReceiverName = item.Name;
                    obj.SenderCompanyName = item.CompanyName;
                    ObjCompMailDeleteProjectVendorList.Add(obj);
                }

                //Delete than added
                if (item.Id > 0 && item.IsAdded == true && item.IsActive == false)
                {
                    obj.Id = item.Id;
                    obj.Email = item.EmailAddress;
                    obj.ReceiverName = item.Name;
                    obj.SenderCompanyName = item.CompanyName;
                    ObjCompMailNewVendorList.Add(obj);
                }
            }


            using (var con = new SqlConnection(ConnectionString))
            {

                ObjMailAfterList = con.Query<VendorCompanyMail>("Usp_EditSaveNewProject", new
                {
                    ProjectId = model.ProjectId,
                    UserId = model.UserId,
                    CompanyId = model.CompanyId,
                    ProjectName = model.ProjectName,
                    ProjectDescription = model.ProjectDescription,
                    projectHours = model.ProjectHours,
                    projectBudget = model.ProjectBudget,
                    AccessLevel = model.AccessLevel,
                    FileNameList = model.FileName,
                    ActualFileNameList = model.ActualFileName,
                    FilePathList = model.FilePath,
                    VendorListId = VendorListId.ToString(),
                    VendorListIsAdded = VendorListIsAdded.ToString(),
                    ListId = ListId.ToString(),
                    StartDate = model.ProjectStartDate,
                    EndDate = model.ProjectEndDate,
                    IsQuoteSent = model.IsQuoteSent,
                    CustomControlId = model.CustomControlId,
                    CustomControlData = model.CustomControlData,
                    PhaseList = ObjProjectPhaseList.AsTableValuedParameter("EditProjectPhaseList", new[]
                    { "Id","PhaseId", "PhaseAttachementId","Name", "Description", "Budget",  "FileNameList", "ActualFileNameList", "FilePathList", "MileNameList",
                        "BudgetList", "DescriptionList","EstimatedHourList" }),
                    MilestoneList = ObjMistoneList.AsTableValuedParameter("PhaseMilestoneList", new[]
                    {"TrackId","ProjectId","Id","Name","PhaseId","MilestoneId","Budget","EstimatedHours","Description"}

                    ),
                    RiskList = objProjectRiskList.AsTableValuedParameter("editProjectRiskList", new[]
                    {"Id","RiskId","ProjectId","RiskTitle","Description","Severity","Status","IsDeleted","CommentList","AgainstComment","CommentIsDeletedList"}

                    ),

                }, commandType: CommandType.StoredProcedure
                
                ).SingleOrDefault();

                datasave = model.IsQuoteSent == true ? 2 : 1;//Convert.ToInt32(ObjMailAfterList.SenderCompanyName);

                if (model.IsQuoteSent && ObjMailAfterList != null)
                {

                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForEditNewProject(ObjMailAfterList, ObjCompMailNewVendorList, ObjCompMailEditProjectVendorList, ObjCompMailDeleteProjectVendorList, ProjectName);

                    }

                    ));
                    childref.Start();

                }

            }


            return datasave;
        }

        private void ThreadForEditNewProject(VendorCompanyMail ObjMailAfterList, List<VendorCompanyMail> ObjCompMailNewVendorList, List<VendorCompanyMail> ObjCompMailEditProjectVendorList, List<VendorCompanyMail> ObjCompMailDeleteProjectVendorList, string ProjectName)
        {
            string Mailmessage = "";
            string subjectBysender = string.Empty;


            // new Vendor
            foreach (VendorCompanyMail item in ObjCompMailNewVendorList)
            {
                subjectBysender = "Vendor Invitation";
                Mailmessage = "SendToVendorAddedInProject";
                item.SenderName = ObjMailAfterList.SenderName;
                sendMailOnEditProject(item, ProjectName, Mailmessage, subjectBysender);

            }

            // vendor mail Edit Project
            foreach (VendorCompanyMail item in ObjCompMailEditProjectVendorList)
            {
                subjectBysender = "Project Edited";
                Mailmessage = "SendToOldVendorEditProjectMail";
                item.SenderName = ObjMailAfterList.SenderName;
                sendMailOnEditProject(item, ProjectName, Mailmessage, subjectBysender);

            }

            // vendor mail remove from Project
            foreach (VendorCompanyMail item in ObjCompMailDeleteProjectVendorList)
            {
                subjectBysender = "Project Withdrawn" + " | " + ProjectName;
                Mailmessage = "SendToVendorDeleteFromProjectMail";
                item.SenderName = ObjMailAfterList.SenderName;
                item.SenderCompanyName = ObjMailAfterList.SenderCompanyName;
                sendMailOnEditProject(item, ProjectName, Mailmessage, subjectBysender);

            }


        }


        public void sendMailOnEditProject(VendorCompanyMail item, string ProjectName, string Mailmessage, string subjectBysender)
        {
            String emailbody = null;
            string Subject = string.Empty;


            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == Mailmessage)
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", item.ReceiverName).Replace("@@SenderName", item.SenderName)
                .Replace("@@SenderCompanyName", item.SenderCompanyName).Replace("@@ProjectName", ProjectName)
                .Replace("@@subjectBysender", subjectBysender);
            EmailUtility.SendMailInThread(item.Email, subjectBysender, emailbody);

        }

        public int DeleteEditProjectFiles(MODEL.ProjectModel.ProjectFileModel model)
        {

            int ResultId;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query;

                if (model.ProjectId == 1)
                {
                    query = "update ProjectInvitationAttachments  set IsDeleted = 1 where  Id = @Id select 1";
                }
                else
                {
                    query = "update PhaseDocuments set IsDeleted = 1 where  Id = @Id select 1";

                }
                ResultId = con.Query<int>(query, new { Id = model.Id }).SingleOrDefault();


            }

            return ResultId;

        }


        public List<vendorModel> GetProjectAwardedForVendor(FilterProjectModal Modal)
        {
            List<vendorModel> ObjVendorLIst = new List<vendorModel>();

            using (var con = new SqlConnection(ConnectionString))
            {
                ObjVendorLIst = con.Query<vendorModel>("Usp_GetProjectAwardedforVendor", new
                {
                    VendorId = Modal.VendorId,
                    CompanyId = Modal.CompanyId,
                    Id = Modal.Id,
                    Name = Modal.Name,
                    UnpaidInvoice = Modal.UnpaidInvoice,
                    ProjectId = Modal.ProjectId,
                    pageIndex = Modal.PageIndex,
                    PageSize =  Modal.PageSize

                }, commandType: CommandType.StoredProcedure).ToList();
            }

            return ObjVendorLIst;

        }
        public List<vendorModel> GetProjectAwardedForcompanyProfile(FilterProjectModal Modal)
        {
            List<vendorModel> ObjVendorLIst = new List<vendorModel>();


            using (var con = new SqlConnection(ConnectionString))
            {

                ObjVendorLIst = con.Query<vendorModel>("Usp_GetProjectAwardedforCompanyProfile",
                    new
                    {
                        Id = Modal.Id,
                        CompanyId = Modal.CompanyId,
                        VendorCompanyId = Modal.VendorCompanyId,
                        Name = Modal.Name,
                        UnpaidInvoice = Modal.UnpaidInvoice,
                        ProjectId = Modal.ProjectId,
                        PageIndex = Modal.PageIndex,
                        PageSize = Modal.PageSize

                    }, commandType: CommandType.StoredProcedure).ToList();
            }

            return ObjVendorLIst;

        }

        public VendorQuoteModal GetProjectAwardedViewDetailCompanyProfile(int ProjectId, int VendorId,int ComapnyId)
        {
            VendorQuoteModal ObjVendorQuote = new VendorQuoteModal();
            List<ProjectDocument> ObjProjectDocList;
            List<vendorPhaseModal> ObjvendorPhaseList;
            List<PhaseDocument> ObjPhaseDocumentList;
            List<vendorMilestone> ObjvendorMileList;
            List<ResourceModal> ObjResourceList;
            List<VendorProjectProgress> ObjProjectProgress;
            CustomControlValueModel ObjCustomControlValueList;
            List<CustomControlUIModel> ObjCustomControlUIList;



            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetAwardedQuotedProjectForCompany", new { ProjectId = ProjectId, VendorId = VendorId, CompanyId = ComapnyId }, commandType: CommandType.StoredProcedure);
                ObjVendorQuote = Result.Read<VendorQuoteModal>().FirstOrDefault();
                ObjProjectDocList = Result.Read<ProjectDocument>().ToList();
                ObjvendorPhaseList = Result.Read<vendorPhaseModal>().ToList();
                ObjPhaseDocumentList = Result.Read<PhaseDocument>().ToList();
                ObjvendorMileList = Result.Read<vendorMilestone>().ToList();
                ObjResourceList = Result.Read<ResourceModal>().ToList();
                ObjProjectProgress = Result.Read<VendorProjectProgress>().ToList();
                ObjCustomControlUIList = Result.Read<CustomControlUIModel>().ToList();
                ObjCustomControlValueList = Result.Read<CustomControlValueModel>().FirstOrDefault();


                foreach (vendorPhaseModal item in ObjvendorPhaseList)
                {

                    List<PhaseDocument> ObjPhasedocList = ObjPhaseDocumentList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.PhaseDocumentList = new List<PhaseDocument>();
                    foreach (PhaseDocument item1 in ObjPhasedocList)
                    {
                        item.PhaseDocumentList.Add(item1);
                    }
                    List<vendorMilestone> PMilestoneList = ObjvendorMileList.Where(x => x.PhaseId == item.PhaseId).ToList();

                    item.MileStone = new List<vendorMilestone>();
                    foreach (vendorMilestone item2 in PMilestoneList)
                    {
                        item.MileStone.Add(item2);
                    }

                    int i = 0;
                    foreach (CustomControlUIModel item3 in ObjCustomControlUIList)
                    {
                        List<CustomUIDropDownModel> ObjCustDrop = new List<CustomUIDropDownModel>();
                        List<CustomUIRadioButtonModel> ObjRadioList = new List<CustomUIRadioButtonModel>();
                        if (item3.Values != null && item3.ControlType == "Dropdown")
                        {
                            string[] drop = item3.Values.Split(',');
                            int j = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIDropDownModel obj = new CustomUIDropDownModel();
                                obj.Name = item1;
                                obj.Id = j;
                                ObjCustDrop.Add(obj);
                                j++;
                              }
                        }
                        if (item3.Values != null && item3.ControlType == "Radio buttons")
                        {
                            string[] drop = item3.Values.Split(',');
                            int k = 1;
                            foreach (string item1 in drop)
                            {
                                CustomUIRadioButtonModel obj = new CustomUIRadioButtonModel();
                                obj.Name = item1;
                                obj.Id = k;
                                ObjRadioList.Add(obj);
                                k++;
                            }
                        }

                        ObjCustomControlUIList[i].FieldModel = ObjCustomControlUIList[i].FieldName.Replace(' ', '_');
                        ObjCustomControlUIList[i].CustomDropDownList = ObjCustDrop;
                        ObjCustomControlUIList[i].CustomRadioButtonList = ObjRadioList;
                        i++;

                    }
                }
            }
            
            ObjVendorQuote.PhaseList = ObjvendorPhaseList;
            ObjVendorQuote.ProjectDocumentList = ObjProjectDocList;
            ObjVendorQuote.ResourcesList = ObjResourceList;
            ObjVendorQuote.VendorProjectProgressList = ObjProjectProgress;
            ObjVendorQuote.CustomControlValueModel = ObjCustomControlValueList;
            ObjVendorQuote.CustomControlUIList = ObjCustomControlUIList;
            return ObjVendorQuote;

        }

        public ProjectCompleteModal GetVendorProjectforCompletion(int ProjectId, int CompanyId, int VendorId)
        {
            ProjectCompleteModal ObjCreatedProjectModal = new ProjectCompleteModal();
            List<VendorResourceModel> ObjVendorResourceList;

            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetVendorProjectforCompletion", new
                { ProjectId = ProjectId, CompanyId = CompanyId, VendorId = VendorId }, commandType: CommandType.StoredProcedure);

                ObjCreatedProjectModal = Result.Read<ProjectCompleteModal>().FirstOrDefault();
                ObjVendorResourceList = Result.Read<VendorResourceModel>().ToList();
                ObjCreatedProjectModal.VendorResourceList = ObjVendorResourceList;

            }

            return ObjCreatedProjectModal;
        }


        public ProjectCompleteModal CompletionVendorProject(ProjectCompleteModal modal)
        {
            ProjectCompleteModal ObjCreatedProjectModal = new ProjectCompleteModal();

            List<VendorResourceTypeModel> ObjVendorResourceList = new List<VendorResourceTypeModel>();

            VendorCompanyMail ObjVendorForCompany;
            foreach (VendorResourceModel item in modal.VendorResourceList)
            {
                int VendorId = 1;
                VendorResourceTypeModel ObjVendorResource = new VendorResourceTypeModel();
                ObjVendorResource.Id = item.Id;
                ObjVendorResource.VendorId = VendorId;
                ObjVendorResource.RoleId = 1;
                ObjVendorResource.FirstName = modal.UserName;
                ObjVendorResource.LastName = item.Name;
                ObjVendorResource.Profile = modal.ProfilePic;
                ObjVendorResource.HourlyRate = 0;
                ObjVendorResource.ExpectedHours = 0;
                ObjVendorResource.SpentHours = 0;   ///not in use upto the time
                ObjVendorResource.Companyfeedback = item.Companyfeedback;
                ObjVendorResource.Rating = item.Rating;
                ObjVendorResourceList.Add(ObjVendorResource);
                VendorId++;
            }

            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_CompleteVendorProject", new
                {
                    ProjectId = modal.ProjectId,
                    vendorId = modal.VendorId,
                    vendorfeedback = modal.vendorFeedback,
                    Rating = modal.Rating,

                    ProjectProposedResourceList = ObjVendorResourceList.AsTableValuedParameter("ProjectProposedResource", new[]

                    { "Id", "VendorId", "RoleId", "FirstName", "LastName", "Profile", "HourlyRate" ,"ExpectedHours", "SpentHours", "Companyfeedback","Rating" })
                }, commandType: CommandType.StoredProcedure);


                ObjVendorForCompany = Result.Read<VendorCompanyMail>().FirstOrDefault();


                if (ObjVendorForCompany != null)
                {

                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForCompleteVendorProject(ObjVendorForCompany);

                    }

                    ));
                    childref.Start();

                }

            }

            return ObjCreatedProjectModal;
        }


        private void ThreadForCompleteVendorProject(VendorCompanyMail ObjVendorForCompany)
        {

            String emailbody = null;
            string Subject = string.Empty;
            string subjectBysender = "Project Completed | " + ObjVendorForCompany.ProjectName;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendCompleteVendorProject")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", ObjVendorForCompany.ReceiverName)
                .Replace("@@SenderCompanyName", ObjVendorForCompany.SenderCompanyName).Replace("@@ProjectName", ObjVendorForCompany.ProjectName).Replace("@@subjectBysender", subjectBysender);
            EmailUtility.SendMailInThread(ObjVendorForCompany.Email, subjectBysender, emailbody);

        }


        public List<ProjectCompanyModal> CompanyListForAwardedProjectDropVendorProfile(int VendorId)
        {
            List<ProjectCompanyModal> ObjProjectCompanyModalList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select C.Id CompanyId,c.Name from Companies c inner join
                         Projects P on P.CompanyId = c.Id
                         inner join  ProjectVendor pv on pv.ProjectId = P.Id where pv.IsHired = 1 and IsRemovedFromProject = 0
                         and pv.VendorId = @VendorId  Group by c.Id,c.Name";
                ObjProjectCompanyModalList = con.Query<ProjectCompanyModal>(query, new { VendorId = VendorId }).ToList();

            }
            return ObjProjectCompanyModalList;




        }

        public List<ProjectVendorModal> GetVendorListForDrop(int CompanyId)
        {
            List<ProjectVendorModal> ObjProjectVendorModal;

            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"select Distinct C.Id VendorCompanyId, C.Name Name from 
                                 UserDetails Ud inner join ProjectVendor PV on Ud.Id =  PV.VendorId
                                 inner join Companies C  on C.Id = Ud.CompanyId
                                 inner join Projects P on P.Id = Pv.ProjectId  
                                where P.CompanyId = @CompanyId  and Pv.IsHired = 1 and IsRemovedFromProject = 0";

                ObjProjectVendorModal = con.Query<ProjectVendorModal>(query, new { CompanyId = CompanyId }).ToList();


            }

            return ObjProjectVendorModal;

        }



        public List<FilterProjectByNameModal> GetProjectName(FilterProjectByNameModal Modal)
        {
            List<FilterProjectByNameModal> ObjFilterProjectByNameModal;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = "";
                if (Modal.VendorId != 0 && Modal.RequestType == 0)  //1 for Project to quote Vendor Profie// 2 Project Awarded Project Vendor Profie
                {
                    query = @"Declare @userId Nvarchar(max)
                            select @userId = Ud.UserId from UserDetails Ud where Ud.Id = @VendorId
                            select Prj.Name Name,Prj.Id ProjectId
                            from ProjectVendor Pv Inner Join Projects Prj on Prj.Id = Pv.ProjectId
                            Inner Join Companies C on C.Id = Prj.CompanyId   inner join VendorInvitations VI on VI.ProjectId = Pv.ProjectId
                            and VI.UserId = @userId
                            left join ProjectVendorQuote PrjVQ on PrjVQ.ProjectId = Prj.Id and PrjVQ.VendorId = Pv.VendorId
                            where Pv.VendorId = @VendorId and IsHired = 0 and VI.IsActive = 1   and VI.IsDeleted = 0 and Prj.Name LIKE @Name";
                }

                else if (Modal.VendorId != 0 && Modal.RequestType == 1)  //1 project/awarded Vendor Profie// 2 project/awarded Vendor Profie
                {
                    query = @"select P.Name Name,P.Id ProjectId from Projects P
                             inner join ProjectVendor Pv on P.Id =  Pv.ProjectId inner join Companies C on C.Id = P.CompanyId
                             inner join ProjectVendorQuote Pvq on Pvq.ProjectId = P.Id and Pvq.VendorId = @VendorId
                             where  Pv.VendorId = @VendorId and IsHired = 1 and P.Name LIKE @Name";
                }

                else if (Modal.CompanyId != 0 && Modal.RequestType == 2)  // 2 quoted/project // 2 quoted/project
                {
                    query = @"select  P.Id projectId  , P.Name from Projects P where P.CompanyId = 1  and IsDeleted = 0 AND  P.Name LIKE @Name";
                }

                else if (Modal.CompanyId != 0 && Modal.RequestType == 3)  // 3 Projects Awarded List company Profile // 3 Projects Awarded List company Profile
                {
                    query = @"select   P.Id  ProjectId, P.Name Name from Projects P
                                left join ProjectVendor Pv on P.Id =  Pv.ProjectId and  Pv.IsRemovedFromProject = 0	and IsHired = 1
                                left join ProjectVendorQuote Pvq on Pvq.ProjectId = P.Id and   Pvq.IsActive = 1  and pv.VendorId = pvq.VendorId
								inner join Companies C on C.Id = Pvq.VendorId
                                where  P.CompanyId = @CompanyId  AND  P.Name LIKE @Name";
                }

                ObjFilterProjectByNameModal = con.Query<FilterProjectByNameModal>(query,
                    new
                    {
                        VendorId = Modal.VendorId,
                        Name = "%" + Modal.Name + "%",
                        CompanyId = Modal.CompanyId
                    }).ToList();

            }

            return ObjFilterProjectByNameModal;

        }


        public List<CustomControlUIModel> GetCustomControlProjectCreation(int CompanyId)
        {
            List<CustomControlUIModel> ObjCustomCont;



            using (var con = new SqlConnection(ConnectionString))
            {

                string query = @"SELECT  Id , CompanyId, FieldName, Watermark, Description, ControlType,[Values], IsActive,Description DescriptionCustomControl
                                ,IsDeleted FROM ProjectCustomizationSettings Where CompanyId = @CompanyId and IsDeleted = 0
                                and IsActive = 1 ORDER BY Priority";

                ObjCustomCont = con.Query<CustomControlUIModel>(query, new { CompanyId = CompanyId }).ToList();
                int i = 0;
                foreach (CustomControlUIModel item in ObjCustomCont)
                {
                    List<CustomUIDropDownModel> ObjCustDrop = new List<CustomUIDropDownModel>();
                    List<CustomUIRadioButtonModel> ObjRadioList = new List<CustomUIRadioButtonModel>();
                    if (item.Values != null && item.ControlType == "Dropdown")
                    {
                        string[] drop = item.Values.Split(',');
                        int j = 1;
                        foreach (string item1 in drop)
                        {
                            CustomUIDropDownModel obj = new CustomUIDropDownModel();
                            obj.Name = item1;
                            obj.Id = j;
                            ObjCustDrop.Add(obj);
                            j++;
                        }
                    }
                    if (item.Values != null && item.ControlType == "Radio buttons")
                    {
                        string[] drop = item.Values.Split(',');
                        int k = 1;
                        foreach (string item1 in drop)
                        {
                            CustomUIRadioButtonModel obj = new CustomUIRadioButtonModel();
                            obj.Name = item1;
                            obj.Id = k;
                            ObjRadioList.Add(obj);
                            k++;
                        }
                    }

                    ObjCustomCont[i].FieldModel = ObjCustomCont[i].FieldName.Replace(' ', '_');
                    ObjCustomCont[i].CustomDropDownList = ObjCustDrop;
                    ObjCustomCont[i].CustomRadioButtonList = ObjRadioList;
                    i++;
                }

            }



            return ObjCustomCont;
        }


        public List<vendorModel> getProjectVendorByCompIdId(int CompanyId, int Id)
        {

            List<vendorModel> objVendorModel;

            using (var con = new SqlConnection(ConnectionString))
            {
        
                string query = @"select UD.Id VendorId,
                                Ud.Email EmailAddress ,CONCAT(Ud.FirstName+ ' ',Ud.LastName + ' ' + '( '+ C.Name + ' )') Name
                                , C.Name as VendorCompanyName,c.Name as HIredByCompany
                                ,Getdate() CreatedDate,Convert(bit,'False') IsAdded,CVA.VendorId VendorCompanyId,CVA.Id Id                                          
                                from CompanyVendorPOCAccount CV 
							 inner join CompanyVendorAccount CVA on CVA.Id = CV.AccId 
								and CVA.CompanyId=@CompanyId and cva.id=@Id
								 and cva.IsDeleted<>1
								inner join UserDetails Ud on Ud.UserId = CV.UserId 
								inner join Companies C on Ud.CompanyId = C.Id";
                objVendorModel = con.Query<vendorModel>(query, new { CompanyId = CompanyId, Id = Id }).ToList();

            }

            return objVendorModel;



        }

        public List<vendorModel> getApprovedProjectByUserId(string UserId)
        {

            List<vendorModel> objVendorModel;

            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetProjectAwardedforUserId",
                    new
                    {
                        UserId = UserId
                    }, commandType: CommandType.StoredProcedure);

                objVendorModel = Result.Read<vendorModel>().ToList();
                


            }

            return objVendorModel;



        }

        public List<vendorModel> getApprovedBillableProjectByUserId(string UserId)
        {

            List<vendorModel> objVendorModel;

            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetProjectAwardedBillableforUserId",
                    new
                    {
                        UserId = UserId
                    }, commandType: CommandType.StoredProcedure);

                objVendorModel = Result.Read<vendorModel>().ToList();



            }

            return objVendorModel;



        }


        public List<vendorModel> GetProjectMilestonesByProjectId(int ProjectId)
        {

            List<vendorModel> objVendorModel;

            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetProjectMilestones",
                    new
                    {
                        ProjectId = ProjectId
                    }, commandType: CommandType.StoredProcedure);

                objVendorModel = Result.Read<vendorModel>().ToList();



            }

            return objVendorModel;



        }

        public List<ProjectModel.ProjectWithForFinancialManager> GetProjectsOfCompany(int CompanyId)
        {
            
            List<ProjectModel.ProjectWithForFinancialManager> projectMod;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select name ProjectName,Id ProjectId                                          
                                from projects where CompanyId=@companyId and IsDeleted<>1";
                projectMod = con.Query<ProjectModel.ProjectWithForFinancialManager>(query, new { companyId = CompanyId }).ToList();
            }
            return projectMod;
        }
    }
}
