﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
namespace TIGHTEN.DATA
{
    public class Email
    {

        public async Task SendEmail(MODEL.TodoModel.Emails obj)
        {
            if (obj.Flag == 1)
            {
                String emailbody = "<p>Hello " + "<a>" + obj.Username + "</a>" + " A new Task has been assigned to you.</p>";
                emailbody += "<p>Project by name <strong>" + obj.ProjectName + "</strong></p>";
                emailbody += "<p>Task by name <strong>" + obj.Todo + "</strong>  has been assigned to you.</p>";
                emailbody += "<p>Login to your Smart Admin account to update your progress.</p>";

                await sendAssigneeEmail(obj.EmailId, obj.Todo, emailbody, "Task Assigned");
            }

            if (obj.Flag == 2)
            {
                String emailbody = "<p>Hello " + "<a>" + obj.Username + "</a>" + " Now the task has been unassigned</p>";
                emailbody += "<p>Project by name <strong>" + obj.ProjectName + "</strong></p>";
                emailbody += "<p>Task by name <strong>" + obj.Todo + "</strong>  has been unassigned to you.</p>";
                emailbody += "<p>Login to your Smart Admin account to update your progress.</p>";

                await sendAssigneeEmail(obj.EmailId, obj.Todo, emailbody, "Task Unassigned");
            }
            if (obj.Flag == 3)
            {
                String emailbody = "<p>Hello " + "<a>" + obj.Username + "</a>" + " Now the task is being Followed</p>";
                emailbody += "<p>Project by name <strong>" + obj.ProjectName + "</strong></p>";
                emailbody += "<p>Task by name <strong>" + obj.Todo + "</strong>  has been followed by you.</p>";
                emailbody += "<p>Login to your Smart Admin account to update your progress.</p>";

                await sendAssigneeEmail(obj.EmailId, obj.Todo, emailbody, "Task Followed");
            }
            if (obj.Flag == 4)
            {
                String emailbody = "<p>Hello " + "<a>" + obj.Username + "</a>" + " Now You are not following the task</p>";
                emailbody += "<p>Project by name <strong>" + obj.ProjectName + "</strong></p>";
                emailbody += "<p>Task by name <strong>" + obj.Todo + "</strong>  has been stopped following by you.</p>";
                emailbody += "<p>Login to your Smart Admin account to update your progress.</p>";

                await sendAssigneeEmail(obj.EmailId, obj.Todo, emailbody, "Task Unfollowed");
            }

            if (obj.Flag == 5)
            {
                String emailbody = "<p>Hello " + "<a>" + obj.Username + "</a>" + " You have been tagged into a comment</p>";
                emailbody += "<p>Project by name <strong>" + obj.ProjectName + "</strong></p>";
                emailbody += "<p>Task by name <strong>" + obj.Todo + "</strong> </p>";
                emailbody += "<p>Comment is : <strong>" + obj.Comment + "</strong> </p>";
                emailbody += "<p>Login to your Smart Admin account to update your progress.</p>";

                await sendAssigneeEmail(obj.EmailId, obj.Todo, emailbody, "Tagged into a comment");
            }
        }
        public async Task sendAssigneeEmail(string destinationemail, string taskName, string emailbody, string taskstatus)
        {
            /*Send Email to user and inform about Task Assigned */
            await EmailUtility.Send(destinationemail, taskstatus, emailbody);
        }
    }
}
