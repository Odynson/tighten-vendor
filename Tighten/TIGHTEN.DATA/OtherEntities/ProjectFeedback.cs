﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.UTILITIES;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class ProjectFeedback : BaseClass
    {
        //AppContext dbcontext;

        /// <summary>
        /// Getting User's projects in which user is Member 
        /// </summary>
        /// <param name="ProjectId">It is unique Id of Project</param>
        /// <returns>List of User's Projects in it as an array</returns>
        public List<MODEL.ProjectFeedbackModel.ProjectFeedback> GetProjectByProjectId(int ProjectId, string UserId, int RoleId)
        {
            
            //var CurrentUser = (from usr in dbcontext.UserDetails
            //                   join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
            //                   join rol in dbcontext.Roles on usrcom.RoleId equals rol.RoleId
            //                   where usr.UserId == UserId && usr.IsDeleted == false
            //                    && rol.IsDeleted == false
            //                   select new
            //                   {
            //                       IsFreelancer = usr.IsFreelancer,
            //                       RolePriority = rol.Priority,
            //                       RoleName = rol.Name
            //                   }).FirstOrDefault();

            MODEL.ProjectFeedbackModel.UserModalForProjectFeedback CurrentUser;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select usr.IsFreelancer , rol.Priority as RolePriority,rol.Name as RoleName from UserDetails usr inner join UserCompanyRoles usrcom on usr.UserId = usrcom.UserId 
                                    inner join Roles rol on usrcom.RoleId = rol.RoleId where usr.UserId = @UserId and usr.IsDeleted = 0 and rol.IsDeleted = 0 ";

                CurrentUser = con.Query<MODEL.ProjectFeedbackModel.UserModalForProjectFeedback>(sqlquery, new { UserId = UserId }).FirstOrDefault();
            }


            List<MODEL.ProjectFeedbackModel.ProjectFeedback> project = new List<MODEL.ProjectFeedbackModel.ProjectFeedback>();

            if (CurrentUser != null)
            {

                //var projectResult = (from prjct in dbcontext.Projects
                //                     where prjct.Id == ProjectId && prjct.IsDeleted == false
                //                     select new
                //                     {
                //                         CompanyId = prjct.CompanyId,
                //                         ProjectId = prjct.Id,
                //                         ProjectName = prjct.Name,
                //                         ProjectDescription = prjct.Description,
                //                         ProjectEstimatedHours = prjct.EstimatedHours,
                //                         ProjectEstimatedBudget = prjct.EstimatedBudget,
                //                         ProjectRemarks = prjct.Remarks,
                //                         ProjectCompleted = prjct.IsComplete,
                            

                //                        ProjectMembers = from p in dbcontext.Projects
                //                                         join pu in dbcontext.ProjectUsers on p.Id equals pu.ProjectId
                //                                         join usr in dbcontext.UserDetails on pu.UserId equals usr.UserId
                //                                         join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
                //                                         join rol in dbcontext.Roles on usrcom.RoleId equals rol.RoleId
                //                                         where p.Id == prjct.Id && usr.IsDeleted == false
                //                                         && pu.UserId != UserId && p.IsDeleted == false
                //                                         && rol.IsDeleted == false

                //                                         select new
                //                                         {
                //                                             MemberName = usr.FirstName,
                //                                             MemberProfilePhoto = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + usr.ProfilePhoto,
                //                                             IsFreelancer = usr.IsFreelancer,
                //                                             MemberEmail = usr.Email,
                //                                             MemberUserId = usr.UserId,
                //                                             MemberRole = rol.Name,
                //                                             MemberRolePriority = rol.Priority,
                //                                             MemberRate = usr.IsFreelancer == true ? 0 : usr.Rate,
                //                                             IsFeedbackAllowed = false,
                //                                             MemberFeedback = (from fed in dbcontext.ProjectFeedbacks
                //                                                               where fed.ProjectId == p.Id
                //                                                               && fed.CreatedBy == UserId
                //                                                               && fed.UserId == usr.UserId
                //                                                               select fed.Feedback).FirstOrDefault()
                //                                                               ,


                //                                            MemberTodos = from t in dbcontext.ToDos
                //                                                          join s in dbcontext.Sections on t.SectionId equals s.Id
                //                                                          where s.ProjectId == prjct.Id && t.IsDeleted == false
                //                                                          && t.AssigneeId == usr.UserId
                //                                                          select new MODEL.ProjectFeedbackModel.Todo
                //                                                          {
                //                                                              TodoId = t.Id,
                //                                                              TodoLoggedTime = t.LoggedTime
                //                                                          }

                //                                              }
                //                         });



                //    project = projectResult.AsEnumerable().Select(item =>
                //              new MODEL.ProjectFeedbackModel.ProjectFeedback()
                //              {
                //                  CompanyId = item.CompanyId,
                //                  ProjectId = item.ProjectId,
                //                  ProjectName = item.ProjectName,
                //                  ProjectDescription = item.ProjectDescription,
                //                  ProjectEstimatedHours = item.ProjectEstimatedHours,
                //                  ProjectEstimatedBudget = item.ProjectEstimatedBudget,
                //                  ProjectRemarks = item.ProjectRemarks,
                //                  ProjectCompleted = item.ProjectCompleted,
                //                  ProjectMembers = item.ProjectMembers.AsEnumerable().Select(item2 =>
                //                                    new MODEL.ProjectFeedbackModel.ProjectFeedbackMembers()
                //                                    {
                //                                        MemberName = item2.MemberName,
                //                                        MemberProfilePhoto = item2.MemberProfilePhoto,
                //                                        IsFreelancer = item2.IsFreelancer,
                //                                        MemberEmail = item2.MemberEmail,
                //                                        MemberUserId = item2.MemberUserId,
                //                                        MemberRole = item2.MemberRole,
                //                                        MemberRolePriority = item2.MemberRolePriority,
                //                                        MemberRate = item2.MemberRate,
                //                                        IsFeedbackAllowed = item2.IsFeedbackAllowed,
                //                                        MemberFeedback = item2.MemberFeedback,
                //                                        //MemberTotalSpentHours = item2.MemberTotalSpentHours,
                //                                        MemberTodos = item2.MemberTodos.ToList()
                //                                    }).ToList()

                //              }).ToList();


                
                using (var con = new SqlConnection(ConnectionString))
                {
                    var result = con.QueryMultiple("usp_GetProjectByProjectIdForFeedback", new { }, commandType: System.Data.CommandType.StoredProcedure);

                    project = result.Read<MODEL.ProjectFeedbackModel.ProjectFeedback>().ToList();

                    if (project != null)
                    {
                        var Members = result.Read<MODEL.ProjectFeedbackModel.ProjectFeedbackMembers>().ToList();

                        var MemberTodos = result.Read<MODEL.ProjectFeedbackModel.Todo>().ToList();

                        foreach (var item in project)
                        {
                            item.ProjectMembers = Members.Where(x => x.ProjectId == item.ProjectId).ToList();

                            foreach (var ProjectMembersItem in item.ProjectMembers)
                            {
                                ProjectMembersItem.MemberTodos = MemberTodos.Where(y => y.ProjectId == ProjectMembersItem.ProjectId && y.UserId == ProjectMembersItem.MemberUserId).ToList();
                            }
                        }

                    }


                }



                    #region code to change later

                    foreach (var project_item in project)
                    {
                        int TotalMinutesForProject = 0;
                        decimal totalamtForProject = 0;

                        foreach (var member_item in project_item.ProjectMembers)
                        {

                            int TotalMinutesForMember = 0;
                            decimal totalamtForMember = 0;

                            foreach (var todo_item in member_item.MemberTodos)
                            {
                                int LoggedTimeInMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(todo_item.TodoLoggedTime);
                                TotalMinutesForMember += LoggedTimeInMinutes;
                            }

                            int TotalHoursForMember = (TotalMinutesForMember / 60);
                            totalamtForMember = (TotalHoursForMember * member_item.MemberRate);

                            int RestTotalMinutesForMember = (TotalMinutesForMember % 60);
                            if (RestTotalMinutesForMember > 0)
                            {
                                decimal totalAmtForMinutes = (Convert.ToDecimal(RestTotalMinutesForMember / 60.00) * member_item.MemberRate);
                                totalamtForMember = totalamtForMember + totalAmtForMinutes;
                            }

                            member_item.MemberSpentHours = TotalHoursForMember + "h " + RestTotalMinutesForMember + "m";

                            TotalMinutesForProject += TotalMinutesForMember;

                            if (member_item.IsFreelancer == true)
                            {
                                member_item.MemberEarnedMoney = Math.Round(totalamtForMember, 2);
                                totalamtForProject += totalamtForMember;
                            }

                            // For FeedBack Permissions
                            if (!CurrentUser.IsFreelancer && member_item.IsFreelancer)
                            {
                                member_item.IsFeedbackAllowed = true;
                            }
                            else if (!CurrentUser.IsFreelancer && !member_item.IsFreelancer && CurrentUser.RolePriority < member_item.MemberRolePriority)
                            {
                                member_item.IsFeedbackAllowed = true;
                            }
                            else if (CurrentUser.IsFreelancer && member_item.IsFreelancer && CurrentUser.RoleName == "Project Manager")
                            {
                                member_item.IsFeedbackAllowed = true;
                            }

                        }

                        int TotalHoursForProject = (TotalMinutesForProject / 60);

                        int RestTotalMinutesForProject = (TotalMinutesForProject % 60);

                        project_item.ProjectSpentHours = TotalHoursForProject + "h " + RestTotalMinutesForProject + "m";
                        project_item.ProjectSpentBudget = Math.Round(totalamtForProject, 2);

                    }

                #endregion

            }

            return project;
        }


        public int GetUserRolePriority(int ProjectId, string UserId, int RoleId)
        {
            //int UserRolePriority = (from rol in dbcontext.Roles
            //                        where rol.RoleId == RoleId
            //                        && rol.IsDeleted == false
            //                        select rol.Priority).SingleOrDefault();

            int UserRolePriority;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from Roles rol where rol.RoleId = @RoleId and rol.IsDeleted = 0";
                UserRolePriority = con.Query<int>(sqlquery, new { RoleId = RoleId }).SingleOrDefault();
            }

            return UserRolePriority;
        }


        public void AddUpdateFeedback(MODEL.ProjectFeedbackModel.ProjectFeedback model)
        {
            List<MODEL.ProjectFeedbackModel.AddUpdateFeedbackModal> ProjectMembers = model.ProjectMembers.AsEnumerable().Select(x =>
                                                                                    new MODEL.ProjectFeedbackModel.AddUpdateFeedbackModal()
                                                                                    {
                                                                                        MemberUserId = x.MemberUserId,
                                                                                        ProjectId = model.ProjectId,
                                                                                        CompanyId = model.CompanyId,
                                                                                        CurrentUser = model.CurrentUser,
                                                                                        MemberFeedback = x.MemberFeedback
                                                                                    }).ToList();

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("usp_AddUpdateFeedback", new
                {
                    ProjectMembers = ProjectMembers.AsTableValuedParameter("ProjectMembersForAddUpdateFeedback", 
                                     new[] { "MemberUserId", "ProjectId", "CompanyId", "CurrentUser", "MemberFeedback" })

                }, commandType: CommandType.StoredProcedure);
            }



            //foreach (var item in model.ProjectMembers)
            //    {
            //        ENTITY.ProjectFeedback prjctFdbk = (from profed in dbcontext.ProjectFeedbacks
            //                                            where profed.ProjectId == model.ProjectId
            //                                            && profed.CompanyId == model.CompanyId
            //                                            && profed.UserId == item.MemberUserId
            //                                            && profed.CreatedBy == model.CurrentUser
            //                                            select profed).FirstOrDefault();


            //        if (prjctFdbk != null)
            //        {
            //            if (item.MemberFeedback == null)
            //            {
            //                dbcontext.ProjectFeedbacks.Remove(prjctFdbk);
            //                dbcontext.SaveChanges();
            //            }
            //            else
            //            {
            //                prjctFdbk.Feedback = item.MemberFeedback;
            //                dbcontext.Entry<ENTITY.ProjectFeedback>(prjctFdbk).State = EntityState.Modified;
            //            }
            //        }
            //        else
            //        {
            //            if (item.MemberFeedback != null)
            //            {
            //                ENTITY.ProjectFeedback obj = new ENTITY.ProjectFeedback();
            //                obj.CompanyId = model.CompanyId;
            //                obj.CreatedBy = model.CurrentUser;
            //                obj.CreatedDate = DateTime.Now;
            //                obj.Feedback = item.MemberFeedback;
            //                obj.ModifiedDate = DateTime.Now;
            //                obj.ProjectId = model.ProjectId;
            //                obj.UserId = item.MemberUserId;

            //                dbcontext.ProjectFeedbacks.Add(obj);
            //            }
            //        }
            //    }

            //dbcontext.SaveChanges();


            //return null;
        }



        public void SaveProjectRemarks(MODEL.ProjectFeedbackModel.ProjectFeedback model)
        {

            //ENTITY.Project Project = (from prjct in dbcontext.Projects
            //                          where prjct.Id == model.ProjectId && prjct.IsDeleted == false
            //                          && prjct.CompanyId == model.CompanyId
            //                          select prjct).FirstOrDefault();


            //Project.Remarks = model.ProjectRemarks;
            //dbcontext.SaveChanges();

            
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update Projects set Remarks = @ProjectRemarks where Id = @ProjectId and IsDeleted = 0 and prjct.CompanyId = @CompanyId";
                con.Execute(sqlquery, new { ProjectRemarks = model.ProjectRemarks, ProjectId= model.ProjectId, CompanyId = model.CompanyId });
            }

        }



        public string MarkAsComplete(MODEL.ProjectFeedbackModel.ProjectFeedback model)
        {
            string msg = string.Empty;

            //var ProjectFeedback = (from prjctfed in dbcontext.ProjectFeedbacks
            //                       join usrcom in dbcontext.UserCompanyRoles on prjctfed.CreatedBy equals usrcom.UserId
            //                       where prjctfed.ProjectId == model.ProjectId
            //                       && prjctfed.CompanyId == model.CompanyId
            //                       && usrcom.RoleId < 3
            //                       select prjctfed).ToList();

            //var AllProjectUsers = (from pu in dbcontext.ProjectUsers
            //                       join usr in dbcontext.UserDetails on pu.UserId equals usr.UserId
            //                       where pu.ProjectId == model.ProjectId && usr.IsDeleted == false
            //                       && usr.IsFreelancer == true
            //                       select usr.UserId).ToList();


            List<TIGHTEN.ENTITY.ProjectFeedback> ProjectFeedbacks;
            List<UserDetail> AllProjectUsers;

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqllquery = @"select * from ProjectFeedbacks prjctfed inner join UserCompanyRoles usrcom on prjctfed.CreatedBy = usrcom.UserId  where prjctfed.ProjectId = @ProjectId  
                           and prjctfed.CompanyId = @CompanyId and usrcom.RoleId < 3 
                            select * from ProjectUsers pu inner join UserDetails usr on pu.UserId = usr.UserId where pu.ProjectId = @ProjectId and usr.IsDeleted = 0 and usr.IsFreelancer = 1";

                var result = con.QueryMultiple(sqllquery);
                ProjectFeedbacks = result.Read<TIGHTEN.ENTITY.ProjectFeedback>().ToList();
                AllProjectUsers = result.Read<UserDetail>().ToList();

                bool isCommentPresent = true;
                foreach (var ProjectUserItem in AllProjectUsers)
                {
                    bool isUserIdPresent = false;
                    foreach (var item in ProjectFeedbacks)
                    {
                        if (ProjectUserItem.UserId == item.UserId)
                        {
                            isUserIdPresent = true;
                            break;
                        }
                    }
                    if (isUserIdPresent == false)
                    {
                        isCommentPresent = false;
                        break;
                    }
                }

                if (isCommentPresent == true)
                {
                    //ENTITY.Project Project = (from prjct in dbcontext.Projects
                    //                          where prjct.Id == model.ProjectId && prjct.IsDeleted == false
                    //                          && prjct.CompanyId == model.CompanyId
                    //                          select prjct).FirstOrDefault();


                    //Project.IsComplete = true;
                    //dbcontext.SaveChanges();

                    string updateProject = @"update Projects set IsComplete = 1 where Id = @ProjectId and IsDeleted = 0 and CompanyId = @CompanyId ";
                    con.Execute(updateProject, new { ProjectId = model.ProjectId, CompanyId = model.CompanyId });

                    msg = "ProjectMarkedAsComplete";
                }
                else
                {
                    msg = "ProjectManagerNotCommented";
                }
            }

            return msg;
        }



    }
}
