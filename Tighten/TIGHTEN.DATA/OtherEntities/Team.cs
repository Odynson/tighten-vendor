﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using System.Threading;
using TIGHTEN.UTILITIES;
using System.Web;
using System.Xml.Linq;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class Team : BaseClass
    {

        AppContext dbcontext;

        /// <summary>
        /// Getting Teams with Users in which Logged in user is a team Member
        /// </summary>
        /// <param name="userId">It is the Unique Id of Logged In User</param>
        /// <returns>Returns the list of Teams with teammembers</returns>
        public List<TeamModel.TeamWithMembers> getTeams(String userId, int CompanyId)
        {
            dbcontext = new AppContext();

            //var UserRole = (from userdet in dbcontext.UserDetails
            //                join usrcom in dbcontext.UserCompanyRoles on userdet.UserId equals usrcom.UserId
            //                where userdet.UserId == userId && userdet.IsDeleted == false
            //                && usrcom.CompanyId == CompanyId
            //                select usrcom.RoleId).SingleOrDefault();

            UserDetail Usr;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select usrcom.RoleId,userdet.IsFreelancer from UserDetails userdet inner join UserCompanyRoles usrcom on userdet.UserId = usrcom.UserId  
                                  where userdet.UserId = @userId and userdet.IsDeleted = 0 and usrcom.CompanyId = @CompanyId";
                Usr = con.Query<UserDetail>(sqlquery, new { userId = userId, CompanyId = CompanyId }).FirstOrDefault();
            }

            int UserRole = Convert.ToInt32(Usr.RoleId);
            if (Usr.IsFreelancer)
            {
                CompanyId = 0;
            }

            List<TeamModel.TeamWithMembers> teamMod1 = null;
            using (var con = new SqlConnection(ConnectionString))
            {
                var AllTeams = con.QueryMultiple("usp_GetTeams", new { userId = userId, CompanyId = CompanyId, UserRole = UserRole }, commandType: CommandType.StoredProcedure);
                teamMod1 = AllTeams.Read<TeamModel.TeamWithMembers>().ToList();
                if (teamMod1 != null)
                {
                    var TeamMembers = AllTeams.Read<TeamModel.TeamMembers>().ToList();

                    foreach (var options in teamMod1)
                    {
                        options.TeamMembers = TeamMembers.Where(x => x.TeamId == options.TeamId).ToList();
                    }
                }
            }

            teamMod1 = teamMod1.OrderByDescending(x => x.SelectedTeamId).ToList();

            //// to list inside this is giving error so I have returened the anonymous type and then convert it into particular type
            //var teamMod = (from data in
            //                   (from t in dbcontext.Teams
            //                    join m in dbcontext.TeamUsers on t.Id equals m.TeamId
            //                    join p3 in dbcontext.UserDetails on m.UserId equals p3.UserId
            //                    where t.IsDefault == false && p3.IsDeleted == false && t.IsDeleted == false
            //                    //&& (user.IsFreelancer == true ?  t.IsDefault == false : t.IsDefault == t.IsDefault)
            //                    && (CompanyId == 0 ? t.CompanyId == t.CompanyId : t.CompanyId == CompanyId)
            //                    && (UserRole == 1 ? m.UserId == m.UserId : (m.UserId == userId || t.CreatedBy == userId))
            //                    //let SelectedTeamId = p3.SelectedTeamId == t.Id ? p3.SelectedTeamId : 0
            //                    //orderby SelectedTeamId descending
            //                    //select new { t, m, p3, SelectedTeamId }
            //                    select new { t, m, p3 }
            //                   )

            //               group data by new { data.t.Id, data.t.TeamName, data.t.TeamBudget }
            //                    into GroupedData

            //               select new
            //               {
            //                   SelectedTeamId = (from usrdet in dbcontext.UserDetails
            //                                     where usrdet.UserId == userId && usrdet.IsDeleted == false
            //                                     select usrdet.SelectedTeamId == GroupedData.Key.Id ? usrdet.SelectedTeamId : 0).FirstOrDefault(),
            //                   TeamId = GroupedData.Key.Id,
            //                   TeamName = GroupedData.Key.TeamName,
            //                   TeamBudget = GroupedData.Key.TeamBudget,
            //                   TeamMembers = from tu in dbcontext.TeamUsers
            //                                 join udm in dbcontext.UserDetails on tu.UserId equals udm.UserId
            //                                 where tu.TeamId == GroupedData.Key.Id && udm.IsDeleted == false
            //                                 //&& udm.IsFreelancer == false
            //                                 select new TeamModel.TeamMembers
            //                                 {
            //                                     MemberEmail = udm.Email,
            //                                     MemberName = udm.FirstName,
            //                                     MemberProfilePhoto = string.IsNullOrEmpty(udm.ProfilePhoto) ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + udm.ProfilePhoto,
            //                                     MemberUserId = udm.UserId,
            //                                     Text = udm.FirstName + " ( " + udm.Email + " )",
            //                                     Value = udm.UserId,
            //                                     IsFreelancer = udm.IsFreelancer
            //                                 }
            //               });




            //// converting the anonymous type to the user defined type
            //List<TeamModel.TeamWithMembers> teamMod1 = teamMod.AsEnumerable().Select(item =>
            //                                           new TeamModel.TeamWithMembers()
            //                                           {
            //                                               SelectedTeamId = item.SelectedTeamId.Value,
            //                                               TeamId = item.TeamId,
            //                                               TeamName = item.TeamName,
            //                                               TeamBudget = item.TeamBudget,
            //                                               TeamMembers = item.TeamMembers.ToList()
            //                                           }).ToList();


            //var teamModForPersonelTeam = (from t in dbcontext.Teams
            //                              join m in dbcontext.TeamUsers on t.Id equals m.TeamId
            //                              join p3 in dbcontext.UserDetails on m.UserId equals p3.UserId
            //                              where t.IsDefault == true && p3.IsDeleted == false && t.IsDeleted == false
            //                              //&& p3.IsFreelancer == false
            //                              && m.UserId == userId
            //                              && (user.IsFreelancer == false ? t.CompanyId == CompanyId : t.CompanyId == 0)
            //                              let SelectedTeamId = p3.SelectedTeamId == t.Id ? p3.SelectedTeamId : 0
            //                              orderby SelectedTeamId descending

            //                              select new
            //                              {
            //                                  SelectedTeamId = SelectedTeamId,
            //                                  TeamId = t.Id,
            //                                  TeamName = t.TeamName,
            //                                  TeamBudget = t.TeamBudget,
            //                                  TeamMembers = from tu in dbcontext.TeamUsers
            //                                                join udm in dbcontext.UserDetails on tu.UserId equals udm.UserId
            //                                                where tu.TeamId == t.Id && udm.IsDeleted == false
            //                                                //&& udm.IsFreelancer == false
            //                                                select new TeamModel.TeamMembers
            //                                                {
            //                                                    MemberEmail = udm.Email,
            //                                                    MemberName = udm.FirstName,
            //                                                    MemberProfilePhoto = string.IsNullOrEmpty(udm.ProfilePhoto) ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + udm.ProfilePhoto,
            //                                                    MemberUserId = udm.UserId,
            //                                                    Text = udm.FirstName + " ( " + udm.Email + " )",
            //                                                    Value = udm.UserId,
            //                                                    IsFreelancer = udm.IsFreelancer
            //                                                }
            //                              });


            //// converting the anonymous type to the user defined type
            //List<TeamModel.TeamWithMembers> teamModForPersonelTeam1 = teamModForPersonelTeam.AsEnumerable().Select(item =>
            //                                                           new TeamModel.TeamWithMembers()
            //                                                           {
            //                                                               SelectedTeamId = item.SelectedTeamId.Value,
            //                                                               TeamId = item.TeamId,
            //                                                               TeamName = item.TeamName,
            //                                                               TeamBudget = item.TeamBudget,
            //                                                               TeamMembers = item.TeamMembers.ToList()
            //                                                           }).ToList();


            //foreach (var item in teamModForPersonelTeam1)
            //{
            //    teamMod1.Insert(0, item);
            //}

            //teamMod1 = teamMod1.OrderByDescending(x => x.SelectedTeamId).ToList();



            //if (UserRole == 1)
            //{
            //    List<int> TeamIdList = new List<int>();

            //    foreach (var item in teamMod1)
            //    {
            //        TeamIdList.Add(item.TeamId);
            //    }

            //    var compModList_ForAdmin = (from data in
            //                               (from m in dbcontext.Teams
            //                                where m.IsDefault == false && m.IsDeleted == false
            //                                && !TeamIdList.Contains(m.Id)
            //                                && (CompanyId == 0 ? m.CompanyId == m.CompanyId : m.CompanyId == CompanyId)
            //                                select new { m }
            //                                )
            //                                group data by new { data.m.Id, data.m.TeamName, data.m.TeamBudget, data.m.Description, data.m.CreatedDate }
            //                               into GroupedData
            //                                select new
            //                                {
            //                                    TeamId = GroupedData.Key.Id,
            //                                    TeamName = GroupedData.Key.TeamName,
            //                                    TeamBudget = GroupedData.Key.TeamBudget,
            //                                    TeamDescription = GroupedData.Key.Description,
            //                                    CreatedDate = GroupedData.Key.CreatedDate,
            //                                    TeamMembers = (from m1 in dbcontext.TeamUsers
            //                                                   join m2 in dbcontext.UserDetails on m1.UserId equals m2.UserId
            //                                                   where m1.TeamId == GroupedData.Key.Id && m2.IsDeleted == false
            //                                                   orderby m1.CreatedDate
            //                                                   select new TeamModel.TeamMembers
            //                                                   {
            //                                                       MemberEmail = m2.Email,
            //                                                       MemberName = m2.FirstName,
            //                                                       MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                                       MemberUserId = m2.UserId
            //                                                   })

            //                                });

            //    string ad = @"  ";


            //    List<TeamModel.TeamWithMembers> compMod_ForAdmin = compModList_ForAdmin.AsEnumerable().Select(item =>
            //                                                         new TeamModel.TeamWithMembers()
            //                                                         {
            //                                                             TeamId = item.TeamId,
            //                                                             TeamName = item.TeamName,
            //                                                             TeamBudget = item.TeamBudget,
            //                                                             TeamDescription = item.TeamDescription,
            //                                                             CreatedDate = item.CreatedDate,
            //                                                             TeamMembers = item.TeamMembers.ToList()
            //                                                         }).ToList();

            //    foreach (var item in compMod_ForAdmin)
            //    {
            //        teamMod1.Add(item);
            //    }

            //    teamMod1.OrderBy(x => x.CreatedDate);

            //}


            return teamMod1;
        }


        /// <summary>
        /// Getting Teams with Users in which used is a team Member
        /// </summary>
        /// <param name="userId">It is the Unique Id of  User</param>
        /// <returns>Returns the list of Teams with teammembers</returns>
        public List<TeamModel.TeamWithMembers> getUserTeams(String userId, int CompanyId, bool isOtherUser)
        {
            dbcontext = new AppContext();

            var user = (from u in dbcontext.UserCompanyRoles
                        where u.UserId == userId
                        && u.CompanyId == CompanyId
                        select u).SingleOrDefault();

            var teamModResult = (from data in
                                 (from m1 in dbcontext.Teams
                                  join m in dbcontext.TeamUsers on m1.Id equals m.TeamId
                                  join p3 in dbcontext.UserDetails on m.UserId equals p3.UserId
                                  //where m.UserId == userId && m1.CompanyId == user.CompanyId
                                  where m1.CompanyId == CompanyId && p3.IsDeleted == false && m1.IsDeleted == false
                                  && (user.RoleId == 1 || user.RoleId == 2 ? m.UserId == m.UserId && m1.IsDefault == false : m.UserId == userId)
                                  && (isOtherUser == true ? m1.IsDefault == false : m1.IsDefault == m1.IsDefault)

                                  select new { m1 }
                                 )

                                 group data by new { data.m1.Id, data.m1.TeamName, data.m1.TeamBudget }
                                 into GroupedData

                                 select new
                                 {
                                     TeamId = GroupedData.Key.Id,
                                     TeamName = GroupedData.Key.TeamName,
                                     TeamBudget = GroupedData.Key.TeamBudget,
                                     TeamMembers = (from m3 in dbcontext.TeamUsers
                                                    join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
                                                    where m3.TeamId == GroupedData.Key.Id && m2.IsDeleted == false
                                                    select new TeamModel.TeamMembers
                                                    {
                                                        MemberEmail = m2.Email,
                                                        MemberName = m2.FirstName,
                                                        MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
                                                        MemberUserId = m2.UserId,
                                                        Text = m2.FirstName + " ( " + m2.Email + " )",
                                                        Value = m2.UserId
                                                    })
                                 });

            List<TeamModel.TeamWithMembers> teamMod = teamModResult.AsEnumerable().Select(item =>
                                                         new TeamModel.TeamWithMembers()
                                                         {
                                                             TeamId = item.TeamId,
                                                             TeamName = item.TeamName,
                                                             TeamBudget = item.TeamBudget,
                                                             TeamMembers = item.TeamMembers.ToList()
                                                         }).ToList();




            if ((isOtherUser == false && user.RoleId == 1) || (isOtherUser == false && user.RoleId == 2))
            {


                var PersonelTeamResultForAdminAndPM = (from m1 in dbcontext.Teams
                                                       join m in dbcontext.TeamUsers on m1.Id equals m.TeamId
                                                       join p3 in dbcontext.UserDetails on m.UserId equals p3.UserId
                                                       //where m.UserId == userId && m1.CompanyId == user.CompanyId
                                                       where m1.CompanyId == CompanyId && p3.IsDeleted == false && m1.IsDeleted == false
                                                       && m.UserId == userId
                                                       && m1.IsDefault == true

                                                       select new
                                                       {
                                                           TeamId = m1.Id,
                                                           TeamName = m1.TeamName,
                                                           TeamBudget = m1.TeamBudget,
                                                           TeamMembers = (from m3 in dbcontext.TeamUsers
                                                                          join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
                                                                          where m3.TeamId == m1.Id && m2.IsDeleted == false
                                                                          select new TeamModel.TeamMembers
                                                                          {
                                                                              MemberEmail = m2.Email,
                                                                              MemberName = m2.FirstName,
                                                                              MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
                                                                              MemberUserId = m2.UserId,
                                                                              Text = m2.FirstName + " ( " + m2.Email + " )",
                                                                              Value = m2.UserId
                                                                          })
                                                       });

                List<TeamModel.TeamWithMembers> teamModForAdminAndPM = PersonelTeamResultForAdminAndPM.AsEnumerable().Select(item =>
                                                             new TeamModel.TeamWithMembers()
                                                             {
                                                                 TeamId = item.TeamId,
                                                                 TeamName = item.TeamName,
                                                                 TeamBudget = item.TeamBudget,
                                                                 TeamMembers = item.TeamMembers.ToList()
                                                             }).ToList();

                teamMod.Insert(0, teamModForAdminAndPM[0]);

            }

            return teamMod;
        }

        /// <summary>
        /// Fetching Details of a Particular Team
        /// </summary>
        /// <param name="teamId">Unique of a particular team</param>
        /// <returns>return team details with team members</returns>
        public TeamModel.TeamWithMembers getTeam(int teamId)
        {
            //dbcontext = new AppContext();
            //var teamModResult = (from m1 in dbcontext.Teams
            //                     where m1.Id == teamId && m1.IsDeleted == false
            //                     select new
            //                     {
            //                         TeamId = m1.Id,
            //                         TeamName = m1.TeamName,
            //                         TeamBudget = m1.TeamBudget,
            //                         TeamDescription = m1.Description,
            //                         TeamMembers = (from m3 in dbcontext.TeamUsers
            //                                        join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
            //                                        where m3.TeamId == m1.Id && m2.IsDeleted == false
            //                                        select new TeamModel.TeamMembers
            //                                        {
            //                                            MemberEmail = m2.Email,
            //                                            MemberName = m2.FirstName + " " + (string.IsNullOrEmpty(m2.LastName) ? "" : m2.LastName),
            //                                            MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                            MemberUserId = m2.UserId
            //                                        })
            //                     });


            //TeamModel.TeamWithMembers teamMod = teamModResult.AsEnumerable().Select(item =>
            //                                               new TeamModel.TeamWithMembers()
            //                                               {
            //                                                   TeamId = item.TeamId,
            //                                                   TeamName = item.TeamName,
            //                                                   TeamBudget = item.TeamBudget,
            //                                                   TeamDescription = item.TeamDescription,
            //                                                   TeamMembers = item.TeamMembers.ToList()
            //                                               }).SingleOrDefault();

            TeamModel.TeamWithMembers teamMod;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetTeam", new { teamId = teamId }, commandType: CommandType.StoredProcedure);
                teamMod = result.Read<TeamModel.TeamWithMembers>().SingleOrDefault();
                if (teamMod != null)
                {
                    var tmMembers = result.Read<TeamModel.TeamMembers>().ToList();
                    teamMod.TeamMembers = tmMembers;
                }
            }

            return teamMod;
        }


        public TeamModel.TeamWithMembers getTeamWithDesignation(int teamId)
        {
            //dbcontext = new AppContext();
            //var teamModResult = (from m1 in dbcontext.Teams
            //                     where m1.Id == teamId && m1.IsDeleted == false
            //                     select new
            //                     {
            //                         TeamId = m1.Id,
            //                         TeamName = m1.TeamName,
            //                         TeamBudget = m1.TeamBudget,
            //                         TeamDescription = m1.Description,
            //                         TeamMembers = (from m3 in dbcontext.TeamUsers
            //                                        join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
            //                                        where m3.TeamId == m1.Id && m2.IsDeleted == false
            //                                        select new TeamModel.TeamMembers
            //                                        {
            //                                            MemberEmail = m2.Email,
            //                                            MemberName = m2.FirstName + " " + (string.IsNullOrEmpty(m2.LastName) ? "" : m2.LastName),
            //                                            MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
            //                                            MemberUserId = m2.UserId
            //                                        })
            //                     });


            //TeamModel.TeamWithMembers teamMod = teamModResult.AsEnumerable().Select(item =>
            //                                               new TeamModel.TeamWithMembers()
            //                                               {
            //                                                   TeamId = item.TeamId,
            //                                                   TeamName = item.TeamName,
            //                                                   TeamBudget = item.TeamBudget,
            //                                                   TeamDescription = item.TeamDescription,
            //                                                   TeamMembers = item.TeamMembers.ToList()
            //                                               }).SingleOrDefault();

            TeamModel.TeamWithMembers teamMod;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetTeamWithDesignation", new { teamId = teamId }, commandType: CommandType.StoredProcedure);
                teamMod = result.Read<TeamModel.TeamWithMembers>().SingleOrDefault();
                if (teamMod != null)
                {
                    var tmMembers = result.Read<TeamModel.TeamMembers>().ToList();
                    teamMod.TeamMembers = tmMembers;
                }
            }

            return teamMod;
        }


        /// <summary>
        /// Saving New Team with it Team Members
        /// </summary>
        /// <param name="UserId">Unique id of logged in User</param>
        /// <param name="model">It consists the values of all fields to be saved</param>
        public string saveTeam(String UserId, TeamModel.Team model, int companyId)

        {
            string msg = "";

            StringBuilder UserIdList = new StringBuilder();
            StringBuilder UserRoleIdList = new StringBuilder();
            StringBuilder DesignationIdList = new StringBuilder();
            StringBuilder IsTeamLeadList = new StringBuilder();

            for (int i = 0; i < model.TeamMembers.Length; i++)
            {
                UserIdList.Append(model.TeamMembers[i].MemberUserId).Append(",");
                UserRoleIdList.Append(model.TeamMembers[i].MemberRoleId).Append(",");
                DesignationIdList.Append(model.TeamMembers[i].MemberDesignationId).Append(",");
                IsTeamLeadList.Append(model.TeamMembers[i].IsTeamLead).Append(",");
            }

            int Id;
            using (var con = new SqlConnection(ConnectionString))

            {
                Id = con.Query<int>("Usp_InsertTeam", new
                {
                    TeamId = 0,
                    UserId = UserId,
                    TeamName = model.TeamName,
                    TeamBudget = model.TeamBudget,
                    Description = model.TeamDescription,
                    companyId = companyId,
                    UserIdList = Convert.ToString(UserIdList),
                    UserRoleIdList = Convert.ToString(UserRoleIdList),
                    DesignationIdList = Convert.ToString(DesignationIdList),
                    IsTeamLeadList = Convert.ToString(IsTeamLeadList),
                    ParentTeamId = model.ParentTeamId,
                }, commandType: CommandType.StoredProcedure).SingleOrDefault();


            }


            if (Id > 0)
            {
                dbcontext = new AppContext();

                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForsaveTeam(UserId, model, companyId);
                }
                ));
                childref.Start();

            }

            if (Id != -2)
            {
                msg = "TeamAddedSuccess";
            }
            else if (Id == -2)
            {
                msg = "DuplicateTeam";
            }


            return msg;

            //    string msg = string.Empty;
            //    dbcontext = new AppContext();
            //           var isTeamExist = (from tm in dbcontext.Teams
            //                       where tm.TeamName == model.TeamName && tm.IsDeleted == false
            //                       && tm.CompanyId == companyId
            //                       select tm.Id).SingleOrDefault();

            //    if (isTeamExist < 1)
            //    {
            //        TIGHTEN.ENTITY.Team team = new TIGHTEN.ENTITY.Team
            //        {
            //            TeamName = model.TeamName,
            //            TeamBudget = model.TeamBudget,
            //            Description = model.TeamDescription,
            //            CreatedBy = UserId,
            //            CreatedDate = DateTime.Now,
            //            CompanyId = companyId,
            //            ModifiedBy = UserId,
            //            ModifiedDate = DateTime.Now,
            //            IsDefault = false
            //        };

            /* Save New Team */
            //dbcontext.Teams.Add(team);
            //dbcontext.SaveChanges();

            //if (model.TeamMembers != null)
            //{
            //    for (int i = 0; i < model.TeamMembers.Length; i++)
            //    {
            //        TeamUser teamuserss = new TeamUser
            //        {
            //            UserId = model.TeamMembers[i],
            //            CreatedBy = UserId,
            //            CreatedDate = DateTime.Now,
            //            TeamId = team.Id
            //        };

            //        dbcontext.TeamUsers.Add(teamuserss);
            //        dbcontext.SaveChanges();

            //        /* Saving notification for newly added Team for team members */
            //        Notification objNotification = new Notification
            //        {
            //            Name = "has created a New Team:",
            //            CreatedBy = UserId,
            //            CreatedDate = DateTime.Now,
            //            IsRead = false,
            //            Type = 1,
            //            NotifyingUserId = model.TeamMembers[i],
            //            TeamId = team.Id
            //        };

            //        dbcontext.Notifications.Add(objNotification);
            //        dbcontext.SaveChanges();

            //    }

            //        HttpContext ctx = HttpContext.Current;
            //        Thread childref = new Thread(new ThreadStart(() =>
            //        {
            //            HttpContext.Current = ctx;
            //            ThreadForsaveTeam(UserId, model, companyId);
            //        }
            //        ));
            //        childref.Start();

            //    }

            //    msg = "TeamAddedSuccess";
            //}
            //else
            //{
            //    msg = "DuplicateTeam";
            //}



        }

        //Sending mail on team creation to all team members in thread
        private void ThreadForsaveTeam(String UserId, TeamModel.Team model, int companyId)
        {
            bool IsSettingActivated = false;
            string sqlquery;
            string sqlquery1;
            string sqlquery2;
            UserDetail CurrentUser;
            UserDetail TmMember;
            foreach (var item in model.TeamMembers)
            {

                /*  This function is used for checking wheather mail setting is activated for assignee or not*/
                // if (!string.IsNullOrEmpty(item))
                if (!string.IsNullOrEmpty(item.MemberUserId))
                {

                    sqlquery = @"select usrSetting.IsActive from UserSettingConfigs usrSetting inner join SettingConfigs dfltSet on 
                                        usrSetting.NotificationId = dfltSet.Id where dfltSet.NotificationName = 'Team Update' 
                                        and usrSetting.UserId = @item and usrSetting.CompanyId = @companyId";




                    using (var con = new SqlConnection(ConnectionString))
                    {
                        IsSettingActivated = con.Query<bool>(sqlquery, new { companyId = companyId, item = item.MemberUserId }).FirstOrDefault();

                    }




                    //IsSettingActivated = (from usrSetting in dbcontext.UserSettingConfigs
                    //                      join dfltSet in dbcontext.SettingConfigs on usrSetting.NotificationId equals dfltSet.Id
                    //                      where dfltSet.NotificationName == "Team Creation"
                    //                      && usrSetting.UserId == item && usrSetting.CompanyId == companyId
                    //                      select usrSetting.IsActive).FirstOrDefault();

                }


                if (IsSettingActivated)
                {



                    sqlquery1 = @"select   [Id],[UserId],[Email],[FirstName],[LastName],[Gender],[IsSubscribed],[CompanyId],[RoleId],[ParentUserId]
                                  ,[ProfilePhoto],[SelectedTeamId],[SendNotificationEmail],[Address],[PhoneNo],[Department],[Designation],[Experience]
                                  ,[Expertise],[DateOfBirth],[AboutMe],[EmailConfirmed],[Password],[Token],[EmailConfirmationCode],[Rate],[IsFreelancer]
                                  ,[UserName],[Availabilty],[Tags],[FavProjects],[IsProfilePublic],[CreditCard],[ExpiryMonth],[ExpiryYear],[CvvNumber]
                                  ,[CardHolderName],[AllowReckringPayment],[StripeUserId],[IsStripeAccountVerified],[EmailConfirmationCodeCreatedDate]
                                  ,[IsDeleted]from UserDetails membr where membr.UserId = @UserId and membr.IsDeleted = 0";

                    sqlquery2 = @"select [Id],[UserId],[Email],[FirstName],[LastName],[Gender],[IsSubscribed],[CompanyId],[RoleId],[ParentUserId]
                                  ,[ProfilePhoto],[SelectedTeamId],[SendNotificationEmail],[Address],[PhoneNo],[Department],[Designation],[Experience]
                                  ,[Expertise],[DateOfBirth],[AboutMe],[EmailConfirmed],[Password],[Token],[EmailConfirmationCode],[Rate],[IsFreelancer]
                                  ,[UserName],[Availabilty],[Tags],[FavProjects],[IsProfilePublic],[CreditCard],[ExpiryMonth],[ExpiryYear],[CvvNumber]
                                  ,[CardHolderName],[AllowReckringPayment],[StripeUserId],[IsStripeAccountVerified],[EmailConfirmationCodeCreatedDate]
                                  ,[IsDeleted] from UserDetails crntusr  where  crntusr.UserId = @UserId and crntusr.IsDeleted = 0";


                    using (var con = new SqlConnection(ConnectionString))
                    {
                        CurrentUser = con.Query<UserDetail>(sqlquery1, new { companyId = companyId, UserId = UserId }).FirstOrDefault();

                        TmMember = con.Query<UserDetail>(sqlquery2, new { companyId = companyId, UserId = item.MemberUserId }).FirstOrDefault();



                    }

                    String emailbody = null;
                    string Subject = string.Empty;

                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                    var MailModule = from r in xdoc.Descendants("MailModule").
                                     Where(r => (string)r.Attribute("id") == "saveTeam")
                                     select new
                                     {
                                         mailbody = r.Element("Body").Value,
                                         subject = r.Element("Subject").Value
                                     };

                    foreach (var Mailitem in MailModule)
                    {
                        emailbody = Mailitem.mailbody;
                        Subject = Mailitem.subject;
                    }

                    emailbody = emailbody.Replace("@@TmMember_FirstName", TmMember.FirstName).Replace("@@TeamName", model.TeamName).Replace("@@CurrentUser_FirstName", CurrentUser.FirstName);


                    EmailUtility.SendMailInThread(TmMember.Email, Subject, emailbody);
                }
            }
        }



        /// <summary>
        /// This function updates the existing Team
        /// </summary>
        /// <param name="UserId">Unique id of logged in user</param>
        /// <param name="model">It consists the updated values of Team</param>
        public string updateTeam(String UserId, TeamModel.Team model, int CompanyId)
        {
            int Id;

            string msg = string.Empty;
            StringBuilder UserIdList = new StringBuilder();
            StringBuilder UserRoleIdList = new StringBuilder();
            StringBuilder DesignationIdList = new StringBuilder();
            StringBuilder IsTeamLeadList = new StringBuilder();

            for (int i = 0; i < model.TeamMembers.Length; i++)
            {
                UserIdList.Append(model.TeamMembers[i].MemberUserId).Append(",");
                UserRoleIdList.Append(model.TeamMembers[i].MemberRoleId).Append(",");
                DesignationIdList.Append(model.TeamMembers[i].MemberDesignationId).Append(",");
                IsTeamLeadList.Append(model.TeamMembers[i].IsTeamLead).Append(",");

            }


            using (var con = new SqlConnection(ConnectionString))

            {
                Id = con.Query<int>("Usp_updateTeam", new
                {
                    TeamId = model.TeamId,
                    UserId = UserId,
                    companyId = CompanyId,
                    TeamName = model.TeamName,
                    TeamBudget = model.TeamBudget,
                    Description = model.TeamDescription,
                    UserIdList = Convert.ToString(UserIdList),
                    UserRoleIdList = Convert.ToString(UserRoleIdList),
                    DesignationIdList = Convert.ToString(DesignationIdList),
                    IsTeamLeadList = Convert.ToString(IsTeamLeadList),
                    ParentTeamId = model.ParentTeamId,

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();


            }

            string[] existingTeamMembers;
            using (var con = new SqlConnection(ConnectionString))
            {

                string query = "select m.UserId from TeamUsers m where m.TeamId =" + model.TeamId + "";


                existingTeamMembers = con.Query<string>(query, new { TeamId = model.TeamId }).ToArray();

            }



            HttpContext ctx = HttpContext.Current;
            Thread childref = new Thread(new ThreadStart(() =>
            {
                HttpContext.Current = ctx;
                ThreadForUpdateTeam(UserId, existingTeamMembers, CompanyId, model.TeamName);
            }
            ));
            childref.Start();




            if (Id == -1)
            {
                msg = "TeamProjectsBudgetExceed";
            }
            else if (Id == -2)
            {
                msg = "DuplicateTeam";

            }

            else
            {
                msg = "Team Updated Successfully !";

            }

            return msg;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////





            //var isTeamExist = (from tm in dbcontext.Teams
            //                   where tm.TeamName == model.TeamName && tm.IsDeleted == false
            //                   && tm.Id != model.TeamId
            //                   && tm.CompanyId == CompanyId
            //                   select tm.Id).SingleOrDefault();

            //if (isTeamExist < 1)
            //{

            //    decimal? AllProjectsBudgetUnderTeam = (from pr in dbcontext.Projects
            //                                           where pr.TeamId == model.TeamId
            //                                           && pr.IsDeleted == false
            //                                           select pr.EstimatedBudget).Sum();

            //    if (AllProjectsBudgetUnderTeam > model.TeamBudget)
            //    {
            //        msg = "TeamProjectsBudgetExceed";
            //        return msg;
            //    }









            //    string[] existingTeamMembers = (from m in dbcontext.TeamUsers where m.TeamId == model.TeamId select m.UserId).ToArray();

            //    string[] existingUsersToBeDeleted = existingTeamMembers.Except(model.TeamMembers).ToArray();

            //    string[] newUsersToBeAdded = model.TeamMembers.Except(existingTeamMembers).ToArray();

            //    if (existingUsersToBeDeleted != null)
            //    {
            //        for (int i = 0; i < existingUsersToBeDeleted.Length; i++)
            //        {
            //            string UserIdToBeDeleted = existingUsersToBeDeleted[i];
            //            TeamUser teamuserToBeDeletedEntity = (from m in dbcontext.TeamUsers where m.UserId == UserIdToBeDeleted && m.TeamId == model.TeamId select m).SingleOrDefault();
            //            dbcontext.TeamUsers.Remove(teamuserToBeDeletedEntity);

            //            List<ProjectUser> projectuserToBeDeletedEntity = (from prjctUsr in dbcontext.ProjectUsers
            //                                                              join prjct in dbcontext.Projects on prjctUsr.ProjectId equals prjct.Id
            //                                                              where prjctUsr.UserId == UserIdToBeDeleted && prjct.TeamId == model.TeamId
            //                                                              && prjct.IsDeleted == false
            //                                                              select prjctUsr).ToList();

            //            #region Deleting default values from UserSettingConfigs for deleted users
            //            /* getting all Settings for the user  */
            //            foreach (var ItemProjectuserToBeDeleted in projectuserToBeDeletedEntity)
            //            {
            //                if (ItemProjectuserToBeDeleted != null)
            //                {
            //                    List<UserSettingConfig> setting = (from usrset in dbcontext.UserSettingConfigs
            //                                                       where usrset.UserId == UserIdToBeDeleted
            //                                                       && usrset.CompanyId == CompanyId
            //                                                       && usrset.ProjectId == ItemProjectuserToBeDeleted.ProjectId.ToString()
            //                                                       select usrset).ToList();

            //                    dbcontext.UserSettingConfigs.RemoveRange(setting);
            //                    dbcontext.ProjectUsers.Remove(ItemProjectuserToBeDeleted);
            //                }
            //            }
            //            #endregion
            //            dbcontext.SaveChanges();

            //            /* Saving notification for newly added Team for team members */
            //            Notification objNotification = new Notification
            //            {
            //                Name = "has removed you from",
            //                CreatedBy = UserId,
            //                CreatedDate = DateTime.Now,
            //                IsRead = false,
            //                Type = 1,
            //                NotifyingUserId = existingUsersToBeDeleted[i],
            //                TeamId = model.TeamId
            //            };

            //            dbcontext.Notifications.Add(objNotification);
            //            dbcontext.SaveChanges();

            //        }
            //    }

            //    if (newUsersToBeAdded != null)
            //    {
            //        for (int i = 0; i < newUsersToBeAdded.Length; i++)
            //        {
            //            TeamUser teamuserToBeAddedEntity = new TeamUser
            //            {
            //                UserId = newUsersToBeAdded[i],
            //                CreatedBy = UserId,
            //                CreatedDate = DateTime.Now,
            //                TeamId = model.TeamId
            //            };

            //            dbcontext.TeamUsers.Add(teamuserToBeAddedEntity);
            //            dbcontext.SaveChanges();

            //            /* Saving notification for newly added Team for team members */
            //            Notification objNotification = new Notification
            //            {
            //                Name = "has added you in",
            //                CreatedBy = UserId,
            //                CreatedDate = DateTime.Now,
            //                IsRead = false,
            //                Type = 1,
            //                NotifyingUserId = newUsersToBeAdded[i],
            //                TeamId = model.TeamId
            //            };

            //            dbcontext.Notifications.Add(objNotification);
            //            dbcontext.SaveChanges();
            //        }
            //    }


            //    if (model.TeamId > 0)
            //    {
            //        TIGHTEN.ENTITY.Team teamentity = (from e in dbcontext.Teams
            //                                          where e.Id == model.TeamId && e.IsDeleted == false
            //                                          select e).SingleOrDefault();

            //        teamentity.TeamName = model.TeamName;
            //        teamentity.TeamBudget = model.TeamBudget;
            //        teamentity.Description = model.TeamDescription;
            //        teamentity.ModifiedDate = DateTime.Now;
            //        dbcontext.SaveChanges();

            //        HttpContext ctx = HttpContext.Current;
            //        Thread childref = new Thread(new ThreadStart(() =>
            //        {
            //            HttpContext.Current = ctx;
            //            ThreadForUpdateTeam(UserId, existingTeamMembers, CompanyId, model.TeamName);
            //        }
            //        ));
            //        childref.Start();

            //        msg = "Team Updated Successfully !";
            //    }

            //}
            //else
            //{
            //    msg = "DuplicateTeam";
            //}

            //return msg;
        }


        //Sending mail in thread on team Update to all team members 
        private void ThreadForUpdateTeam(String UserId, String[] model, int companyId, string TeamName)
        {
            dbcontext = new AppContext();
            foreach (var item in model)
            {
                bool IsSettingActivated = false;
                string sqlquery;
                string sqlquery1;
                string sqlquery2;
                UserDetail CurrentUser;
                UserDetail TmMember;
                /*  This function is used for checking wheather mail setting is activated for assignee or not*/
                if (!string.IsNullOrEmpty(item))
                {

                    sqlquery = @"select usrSetting.IsActive from UserSettingConfigs usrSetting inner join SettingConfigs dfltSet on 
                                        usrSetting.NotificationId = dfltSet.Id where dfltSet.NotificationName = 'Team Update' 
                                        and usrSetting.UserId = @item and usrSetting.CompanyId = @companyId";




                    using (var con = new SqlConnection(ConnectionString))
                    {
                        IsSettingActivated = con.Query<bool>(sqlquery, new { companyId = companyId, item = item }).FirstOrDefault();

                    }
                    //IsSettingActivated = (from usrSetting in dbcontext.UserSettingConfigs
                    //                      join dfltSet in dbcontext.SettingConfigs on usrSetting.NotificationId equals dfltSet.Id
                    //                      where dfltSet.NotificationName == "Team Update"
                    //                      && usrSetting.UserId == item && usrSetting.CompanyId == companyId
                    //                      select usrSetting.IsActive).FirstOrDefault();




                }


                if (IsSettingActivated)
                {

                    sqlquery1 = @"select   [Id],[UserId],[Email],[FirstName],[LastName],[Gender],[IsSubscribed],[CompanyId],[RoleId],[ParentUserId]
                                  ,[ProfilePhoto],[SelectedTeamId],[SendNotificationEmail],[Address],[PhoneNo],[Department],[Designation],[Experience]
                                  ,[Expertise],[DateOfBirth],[AboutMe],[EmailConfirmed],[Password],[Token],[EmailConfirmationCode],[Rate],[IsFreelancer]
                                  ,[UserName],[Availabilty],[Tags],[FavProjects],[IsProfilePublic],[CreditCard],[ExpiryMonth],[ExpiryYear],[CvvNumber]
                                  ,[CardHolderName],[AllowReckringPayment],[StripeUserId],[IsStripeAccountVerified],[EmailConfirmationCodeCreatedDate]
                                  ,[IsDeleted]from UserDetails membr where membr.UserId = @UserId and membr.IsDeleted = 0";

                    sqlquery2 = @"select [Id],[UserId],[Email],[FirstName],[LastName],[Gender],[IsSubscribed],[CompanyId],[RoleId],[ParentUserId]
                                  ,[ProfilePhoto],[SelectedTeamId],[SendNotificationEmail],[Address],[PhoneNo],[Department],[Designation],[Experience]
                                  ,[Expertise],[DateOfBirth],[AboutMe],[EmailConfirmed],[Password],[Token],[EmailConfirmationCode],[Rate],[IsFreelancer]
                                  ,[UserName],[Availabilty],[Tags],[FavProjects],[IsProfilePublic],[CreditCard],[ExpiryMonth],[ExpiryYear],[CvvNumber]
                                  ,[CardHolderName],[AllowReckringPayment],[StripeUserId],[IsStripeAccountVerified],[EmailConfirmationCodeCreatedDate]
                                  ,[IsDeleted] from UserDetails crntusr  where  crntusr.UserId = @UserId and crntusr.IsDeleted = 0";


                    using (var con = new SqlConnection(ConnectionString))
                    {
                        CurrentUser = con.Query<UserDetail>(sqlquery1, new { companyId = companyId, UserId = UserId }).FirstOrDefault();

                        TmMember = con.Query<UserDetail>(sqlquery2, new { companyId = companyId, UserId = UserId }).FirstOrDefault();



                    }




                    //CurrentUser = (from crntusr in dbcontext.UserDetails
                    //                         where crntusr.UserId == UserId && crntusr.IsDeleted == false
                    //                         select crntusr).FirstOrDefault();

                    //TmMember = (from membr in dbcontext.UserDetails
                    //                      where membr.UserId == item && membr.IsDeleted == false
                    //                      select membr).FirstOrDefault();

                    //String emailbody = "<p> Hello " + TmMember.FirstName + " <br/>";
                    //emailbody += " This mail is sent to inform you that a team named as " + TeamName + " has been updated by " + CurrentUser.FirstName + "<br/>";


                    String emailbody = null;
                    string Subject = string.Empty;

                    XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
                    var MailModule = from r in xdoc.Descendants("MailModule").
                                     Where(r => (string)r.Attribute("id") == "UpdateTeam")
                                         //select r;
                                     select new
                                     {
                                         mailbody = r.Element("Body").Value,
                                         subject = r.Element("Subject").Value
                                     };

                    foreach (var Mailitem in MailModule)
                    {
                        emailbody = Mailitem.mailbody;
                        Subject = Mailitem.subject;
                    }

                    emailbody = emailbody.Replace("@@TmMember_FirstName", TmMember.FirstName).Replace("@@TeamName", TeamName).Replace("@@CurrentUser_FirstName", CurrentUser.FirstName);
                    Subject = Subject.Replace("@@TeamName", TeamName);

                    EmailUtility.SendMailInThread(TmMember.Email, Subject, emailbody);
                }
            }
        }



        /// <summary>
        /// This function fetches the Teams with team members for a particular Company
        /// </summary>
        /// <param name="companyId">It is the unique Id of Company</param>
        /// <returns>It returns Teams of a particular company with team members</returns>
        /// 

        public List<TeamDataModel> getCompanyTeams(int CompanyId, string UserId, int PageIndex, int PageSizeSelected)
        {
            UserDetail Usr;

            /// //////////////////////////////////////////////////

            TotalTeamModel ObjTeamModel = new TotalTeamModel();
            List<TeamDataModel> ObjTeam = new List<TeamDataModel>();
            List<TeamMembersModel> ObjMemberList = new List<TeamMembersModel>();
            List<TeamMembersModel> ObjTopMemberList = new List<TeamMembersModel>();
            using (var con = new SqlConnection(ConnectionString))
            {
                var AllTeams = con.QueryMultiple("Usp_GetCompanyTeams", new { CompanyId = CompanyId, UserId = UserId, PageIndex = PageIndex, PageSizeSelected = PageSizeSelected }, commandType: CommandType.StoredProcedure);
                if (AllTeams != null)
                {
                    ObjTeam = AllTeams.Read<TeamDataModel>().ToList();
                    ObjMemberList = AllTeams.Read<TeamMembersModel>().ToList();
                    ObjTopMemberList = AllTeams.Read<TeamMembersModel>().ToList();
                    List<TeamDataModel> Obj = new List<TeamDataModel>();
                    ObjTeam = NestedTeamcal(ObjTeam, ObjMemberList, ObjTopMemberList, 0);
                    foreach (var item in ObjTeam)
                    {

                        item.TopTeamMembersList = ObjTopMemberList.Where(x => x.TeamId == item.TeamId).ToList();
                    }
                }

            }
            return ObjTeam;
        }

        public List<TeamChartModel> getCompanyTeamsChart(int CompanyId, string UserId, int PageIndex, int PageSizeSelected,int ParentTeamId)
        {
            UserDetail Usr;

            /// //////////////////////////////////////////////////

            TotalTeamModel ObjTeamModel = new TotalTeamModel();
            List<TeamDataModel> ObjTeam = new List<TeamDataModel>();
            List<TeamChartModel> ObjChartTeam = new List<TeamChartModel>();
            List<TeamMembersModel> ObjMemberList = new List<TeamMembersModel>();
            List<TeamMembersModel> ObjTopMemberList = new List<TeamMembersModel>();
            using (var con = new SqlConnection(ConnectionString))
            {
                var AllTeams = con.QueryMultiple("Usp_GetCompanyTeams", new { CompanyId = CompanyId, UserId = UserId, PageIndex = PageIndex, PageSizeSelected = PageSizeSelected }, commandType: CommandType.StoredProcedure);

                ObjTeam = AllTeams.Read<TeamDataModel>().ToList();
                ObjChartTeam = NestedTeamchartcal(ObjTeam, ParentTeamId);

            }
            return ObjChartTeam;
        }


        List<TeamMembersModel> ObjTempTopMemberList;

        public List<TeamDataModel> NestedTeamcal(List<TeamDataModel> ObjTeam, List<TeamMembersModel> ObjMemberList, List<TeamMembersModel> ObjTopMemberList, int ParentTeamId)
        {
            List<TeamDataModel> Obj = new List<TeamDataModel>();
            var teamsList = ObjTeam.Where(y1 => y1.ParentTeamId.Equals(ParentTeamId)).ToList();

        

            foreach (var option1 in teamsList)
            {
                if (ParentTeamId == 0)
                {
                    ObjTempTopMemberList = ObjTopMemberList.Where(x => x.TeamId == option1.TeamId).ToList();

                }

                Obj.Add(new TeamDataModel()
                {
                    TeamId = option1.TeamId,
                    TeamName = option1.TeamName,
                    TeamBudget = option1.TeamBudget,
                    TeamDescription = option1.TeamDescription,
                    TeamCreatedDate = option1.TeamCreatedDate,
                    ParentId = option1.ParentId,
                    ParentTeamId = option1.ParentTeamId,
                    SubTeamList = NestedTeamcal(ObjTeam, ObjMemberList, ObjTopMemberList, option1.TeamId),
                    TeamMembersList = ObjMemberList.Where(x1 => x1.TeamId == option1.TeamId).ToList(),
                    TopTeamMembersList = ObjTempTopMemberList,

                });


            }
            return Obj;
        }

        public List<TeamChartModel> NestedTeamchartcal(List<TeamDataModel> ObjTeam, int ParentTeamId)
        {
            List<TeamChartModel> Obj = new List<TeamChartModel>();
            var teamsList = ObjTeam.Where(y1 => y1.ParentTeamId.Equals(ParentTeamId)).ToList();



            foreach (var option1 in teamsList)
            {
                if (ParentTeamId == 0)
                {
                  //  ObjChartTempTopMemberList = ObjTopMemberList.Where(x => x.TeamId == option1.TeamId).ToList();

                }

                Obj.Add(new TeamChartModel()
                {
                    title = option1.TeamId.ToString(),
                    name = option1.TeamName,
                    children = NestedTeamchartcal(ObjTeam, option1.TeamId),

                });


            }
            return Obj;
        }



        /*this mehod ues with dapper*/
        //public List<TeamModel.TeamWithMembers> getCompanyTeams(int CompanyId, string UserId)
        //{
        //    dbcontext = new AppContext();
        //    var UserRole = (from user in dbcontext.UserDetails
        //                    join usrcom in dbcontext.UserCompanyRoles on user.UserId equals usrcom.UserId
        //                    where user.UserId == UserId && user.IsDeleted == false
        //                    && usrcom.CompanyId == CompanyId
        //                    select usrcom.RoleId).SingleOrDefault();

        //    //List to hold all the team listing
        //    List<TeamModel.TeamWithMembers> compMod = new List<TeamModel.TeamWithMembers>();

        //    if (UserRole != 1)
        //    {
        //        var compModList = (from data in
        //                           (from m in dbcontext.Teams
        //                            join tu in dbcontext.TeamUsers on m.Id equals tu.TeamId into tmu
        //                            from tmusr in tmu.DefaultIfEmpty()
        //                            where m.IsDefault == false && m.IsDeleted == false
        //                            && (CompanyId == 0 ? m.CompanyId == m.CompanyId : m.CompanyId == CompanyId)
        //                            && (tmusr.UserId == UserId || m.CreatedBy == UserId)
        //                            select new { m, tmusr }
        //                            )
        //                           group data by new { data.m.Id, data.m.TeamName, data.m.TeamBudget, data.m.Description, data.m.CreatedDate }
        //                           into GroupedData
        //                           select new
        //                           {
        //                               TeamId = GroupedData.Key.Id,
        //                               TeamName = GroupedData.Key.TeamName,
        //                               TeamBudget = GroupedData.Key.TeamBudget,
        //                               TeamDescription = GroupedData.Key.Description,
        //                               CreatedDate = GroupedData.Key.CreatedDate,
        //                               TeamMembers = (from m1 in dbcontext.TeamUsers
        //                                              join m2 in dbcontext.UserDetails on m1.UserId equals m2.UserId
        //                                              where m1.TeamId == GroupedData.Key.Id && m2.IsDeleted == false
        //                                              orderby m1.CreatedDate
        //                                              select new TeamModel.TeamMembers
        //                                              {
        //                                                  MemberEmail = m2.Email,
        //                                                  MemberName = m2.FirstName,
        //                                                  MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
        //                                                  MemberUserId = m2.UserId
        //                                              })
        //                           });


        //        compMod = compModList.AsEnumerable().Select(item =>
        //                       new TeamModel.TeamWithMembers()
        //                       {
        //                           TeamId = item.TeamId,
        //                           TeamName = item.TeamName,
        //                           TeamBudget = item.TeamBudget,
        //                           TeamDescription = item.TeamDescription,
        //                           CreatedDate = item.CreatedDate,
        //                           TeamMembers = item.TeamMembers.ToList()
        //                       }).ToList();
        //    }
        //    else
        //    {
        //        var compModList_ForAdmin = (from data in
        //                                   (from m in dbcontext.Teams
        //                                    where m.IsDefault == false && m.IsDeleted == false

        //                                    && (CompanyId == 0 ? m.CompanyId == m.CompanyId : m.CompanyId == CompanyId)
        //                                    select new { m }
        //                                    )
        //                                    group data by new { data.m.Id, data.m.TeamName, data.m.TeamBudget, data.m.Description, data.m.CreatedDate }
        //                                   into GroupedData
        //                                    select new
        //                                    {
        //                                        TeamId = GroupedData.Key.Id,
        //                                        TeamName = GroupedData.Key.TeamName,
        //                                        TeamBudget = GroupedData.Key.TeamBudget,
        //                                        TeamDescription = GroupedData.Key.Description,
        //                                        CreatedDate = GroupedData.Key.CreatedDate,
        //                                        TeamMembers = (from m1 in dbcontext.TeamUsers
        //                                                       join m2 in dbcontext.UserDetails on m1.UserId equals m2.UserId
        //                                                       where m1.TeamId == GroupedData.Key.Id && m2.IsDeleted == false
        //                                                       orderby m1.CreatedDate
        //                                                       select new TeamModel.TeamMembers
        //                                                       {
        //                                                           MemberEmail = m2.Email,
        //                                                           MemberName = m2.FirstName,
        //                                                           MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
        //                                                           MemberUserId = m2.UserId
        //                                                       })
        //                                    });

        //        compMod = compModList_ForAdmin.AsEnumerable().Select(item =>
        //                  new TeamModel.TeamWithMembers()
        //                  {
        //                      TeamId = item.TeamId,
        //                      TeamName = item.TeamName,
        //                      TeamBudget = item.TeamBudget,
        //                      TeamDescription = item.TeamDescription,
        //                      CreatedDate = item.CreatedDate,
        //                      TeamMembers = item.TeamMembers.ToList()
        //                  }).ToList();

        //    }

        //    compMod.OrderBy(x => x.CreatedDate);

        //    return compMod;
        //}


        //public List<TeamModel.TeamFinancialManager> getAllTeamsForFinancialManager(int CompanyId, string UserId)
        //{
        //    dbcontext = new AppContext();
        //    var UserRole = (from user in dbcontext.UserDetails
        //                    join usrcom in dbcontext.UserCompanyRoles on user.UserId equals usrcom.UserId
        //                    where user.UserId == UserId && user.IsDeleted == false
        //                    && usrcom.CompanyId == CompanyId
        //                    select usrcom.RoleId).SingleOrDefault();


        //    return compMod;
        //}
        public List<TeamModel.TeamFinancialManager> getAllTeamsForFinancialManager(int CompanyId, string UserId)
        {


            List<TeamModel.TeamFinancialManager> compMod;
            List<TeamModel.ProjectFinancialManager> teamMod1 = null;
            List<TeamModel.ProjectFinancialManager> teamMod2 = null;
            using (var con = new SqlConnection(ConnectionString))
            {
                var AllTeams = con.QueryMultiple("Usp_getAllTeamsForFinancialManager", new { CompanyId = CompanyId, UserId = UserId, }, commandType: CommandType.StoredProcedure);
                compMod = AllTeams.Read<TeamModel.TeamFinancialManager>().ToList();

                teamMod1 = AllTeams.Read<TeamModel.ProjectFinancialManager>().ToList();

                teamMod2 = AllTeams.Read<TeamModel.ProjectFinancialManager>().ToList();

                for (int i = 0; i < teamMod2.Count; i++)
                {

                    compMod[i].ProjectDetails = teamMod2.Where(x => x.ProjectId == teamMod1[i].ProjectId && x.TeamId == teamMod1[i].TeamId).ToList();
                }

                for (int i = 0; i < teamMod1.Count; i++)
                {
                    // outer loop

                    compMod[i].ProjectDetails = teamMod2.Where(x => x.ProjectId == teamMod1[i].ProjectId && x.TeamId == teamMod1[i].TeamId).ToList();
                    compMod[i].Indicator = (teamMod1[i].SpentBudget != 0 ? teamMod1[i].SpentBudget : 0) > teamMod1[i].SpentBudget ? false : true;
                    compMod[i].SpentBudget = teamMod1[i].SpentBudget != 0 ? teamMod1[i].SpentBudget : 0;
                    compMod[i].Percentage = Math.Round(Convert.ToDecimal((teamMod1[i].SpentBudget > 0 ? (teamMod1[i].SpentBudget * 100) / compMod[i].TeamBudget : 0)), 2);
                    compMod[i].AllottedBudget = teamMod1[i].AllottedBudget;
                    // inner loop
                    //teamMod1[i].SpentBudget = (teamMod1[i].SpentBudget != null ? teamMod1[i].SpentBudget : 0);
                    //teamMod1[i].Percentage = Math.Round(Convert.ToDecimal((teamMod1[i].SpentBudget != null ? (teamMod1[i].SpentBudget * 100) / teamMod1[i].AllottedBudget : 0)), 2);
                    //teamMod1[i].PercentageFromTeam = Math.Round(Convert.ToDecimal((teamMod1[i].SpentBudget != null ? (teamMod1[i].SpentBudget * 100) / compMod[i].TeamBudget : 0)), 2);
                    //teamMod1[i].Indicator = teamMod1[i].SpentBudget > teamMod1[i].AllottedBudget ? false : true;


                }



                if (teamMod1 != null)
                {

                    foreach (var options in compMod)
                    {
                        TeamModel.ProjectFinancialManager obj = new TeamModel.ProjectFinancialManager();
                        int i = 0;
                        options.ProjectDetails = teamMod1.Where(x => x.TeamId == options.TeamId)
                                                .Select(x => new TeamModel.ProjectFinancialManager()
                                                {
                                                    ProjectId = x.ProjectId,
                                                    ProjectName = x.ProjectName,
                                                    ProjectDescription = x.ProjectDescription,
                                                    AllottedBudget = x.AllottedBudget,
                                                    SpentBudget = (x.SpentBudget != null ? x.SpentBudget : 0),
                                                    Percentage = Math.Round(Convert.ToDecimal((x.SpentBudget != null ? (x.SpentBudget * 100) / x.AllottedBudget : 0)), 2),
                                                    PercentageFromTeam = Math.Round(Convert.ToDecimal((x.SpentBudget != null ? (x.SpentBudget * 100) / options.TeamBudget : 0)), 2),
                                                    Indicator = x.SpentBudget > x.AllottedBudget ? false : true,

                                                }).ToList();






                    }
                }


            }

            return compMod;
        }

        //public List<TeamModel.TeamFinancialManager> getAllTeamsForFinancialManager1(int CompanyId, string UserId)
        //{
        //    dbcontext = new AppContext();
        //    var UserRole = (from user in dbcontext.UserDetails
        //                    join usrcom in dbcontext.UserCompanyRoles on user.UserId equals usrcom.UserId
        //                    where user.UserId == UserId && user.IsDeleted == false
        //                    && usrcom.CompanyId == CompanyId
        //                    select usrcom.RoleId).SingleOrDefault();

        //    var compModList = (from data in
        //                       (from m in dbcontext.Teams
        //                        join tu in dbcontext.TeamUsers on m.Id equals tu.TeamId
        //                        where m.IsDefault == false && m.IsDeleted == false
        //                        && (CompanyId == 0 ? m.CompanyId == m.CompanyId : m.CompanyId == CompanyId)
        //                        && (UserRole == 5 || UserRole == 2 ? tu.UserId == tu.UserId : tu.UserId == UserId)
        //                        select new { m, tu }
        //                        )
        //                       group data by new { data.m.Id, data.m.TeamName, data.m.TeamBudget, data.m.Description }
        //                       into GroupedData
        //                       select new
        //                       {
        //                           TeamId = GroupedData.Key.Id,
        //                           TeamName = GroupedData.Key.TeamName,
        //                           TeamBudget = GroupedData.Key.TeamBudget,
        //                           TeamDescription = GroupedData.Key.Description,
        //                           SpentBudget = (from m1 in dbcontext.Invoices
        //                                          join m2 in dbcontext.InvoiceDetails on m1.Id equals m2.InvoiceId
        //                                          join m3 in dbcontext.ToDos on m2.TodoId equals m3.Id
        //                                          join m4 in dbcontext.Sections on m3.SectionId equals m4.Id
        //                                          join m5 in dbcontext.Projects on m4.ProjectId equals m5.Id
        //                                          where m5.TeamId == GroupedData.Key.Id && m1.InvoiceApprovedStatus == true
        //                                          && m5.IsDeleted == false && m3.IsDeleted == false
        //                                          select m1.TotalAmount).ToList(),
        //                           ProjectDetails = (from t1 in dbcontext.Projects
        //                                             where t1.TeamId == GroupedData.Key.Id && t1.IsDeleted == false
        //                                             orderby t1.CreatedDate
        //                                             select new TeamModel.ProjectFinancialManager
        //                                             {
        //                                                 ProjectId = t1.Id,
        //                                                 ProjectName = t1.Name,
        //                                                 ProjectDescription = t1.Description,
        //                                                 AllottedBudget = t1.EstimatedBudget,
        //                                                 SpentBudget = (from m1 in dbcontext.Invoices
        //                                                                join m2 in dbcontext.InvoiceDetails on m1.Id equals m2.InvoiceId
        //                                                                join m3 in dbcontext.ToDos on m2.TodoId equals m3.Id
        //                                                                join m4 in dbcontext.Sections on m3.SectionId equals m4.Id
        //                                                                join m5 in dbcontext.Projects on m4.ProjectId equals m5.Id
        //                                                                where m5.Id == t1.Id && m1.InvoiceApprovedStatus == true
        //                                                                && m5.IsDeleted == false && m3.IsDeleted == false
        //                                                                select m1.TotalAmount).ToList().Sum()
        //                                             }).ToList()
        //                       });

        //    List<TeamModel.TeamFinancialManager> compMod = compModList.AsEnumerable().Select(item =>
        //                                                  new TeamModel.TeamFinancialManager()
        //                                                  {
        //                                                      TeamId = item.TeamId,
        //                                                      TeamName = item.TeamName,
        //                                                      AllottedBudget = item.TeamBudget,
        //                                                      SpentBudget = item.SpentBudget.Count > 0 ? item.SpentBudget.Sum() : 0,
        //                                                      Percentage = Math.Round(Convert.ToDecimal((item.SpentBudget.Count > 0 ? (item.SpentBudget.Sum() * 100) / item.TeamBudget : 0)), 2),
        //                                                      TeamDescription = item.TeamDescription,
        //                                                      ProjectDetails = item.ProjectDetails.Count > 0 ? item.ProjectDetails.Select(t1 => new TeamModel.ProjectFinancialManager
        //                                                      {
        //                                                          ProjectId = t1.ProjectId,
        //                                                          ProjectName = t1.ProjectName,
        //                                                          ProjectDescription = t1.ProjectDescription,
        //                                                          AllottedBudget = t1.AllottedBudget,
        //                                                          SpentBudget = (t1.SpentBudget != null ? t1.SpentBudget : 0),
        //                                                          Percentage = Math.Round(Convert.ToDecimal((t1.SpentBudget != null ? (t1.SpentBudget * 100) / t1.AllottedBudget : 0)), 2),
        //                                                          PercentageFromTeam = Math.Round(Convert.ToDecimal((t1.SpentBudget != null ? (t1.SpentBudget * 100) / item.TeamBudget : 0)), 2),
        //                                                          Indicator = t1.SpentBudget > t1.AllottedBudget ? false : true
        //                                                      }).ToList() : null,
        //                                                      Indicator = (item.SpentBudget.Count > 0 ? item.SpentBudget.Sum() : 0) > item.TeamBudget ? false : true
        //                                                  }).ToList();
        //    return compMod;
        //}


        /// <summary>
        /// This function deletes the Particular Team
        /// </summary>
        /// <param name="TeamId">It is the Unique Id of Team</param>
        public string deleteTeam(int TeamId)
        {
            string msg = string.Empty;
            int Id;

            using (var con = new SqlConnection(ConnectionString))

            {

                Id = con.Query<int>("Usp_deleteTeam", new
                {

                    TeamId = TeamId,


                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }

            if (Id == -1)
            {
                msg = "ProjectPresentInTeam";
            }
            if (Id == 1)
            {
                msg = "TeamDeletedSuccessfully";
            }
            //dbcontext = new AppContext();
            //string msg = string.Empty;
            //var AllProjects = (from p in dbcontext.Projects
            //                   where p.TeamId == TeamId && p.IsDeleted == false
            //                   select p.Id).ToList();

            //if (AllProjects.Count() > 0)
            //{
            //    msg = "ProjectPresentInTeam";
            //}
            //else
            //{
            //    TIGHTEN.ENTITY.Team teamEntity = (from m in dbcontext.Teams where m.Id == TeamId && m.IsDeleted == false select m).SingleOrDefault();

            //    teamEntity.IsDeleted = true;
            //    dbcontext.SaveChanges();
            //    msg = "TeamDeletedSuccessfully";
            //}

            return msg;
        }

        public List<UserRoleModel> GetAllDesignation()
        {

            List<UserRoleModel> AllDesignationList;


            using (var con = new SqlConnection(ConnectionString))
            {
                string query = "Select RoleId ,Name from Roles where  Type in(2,3) and IsActive=1 and IsDeleted=0";
                AllDesignationList = con.Query<UserRoleModel>(query).ToList();
            }

            return AllDesignationList;
        }

        public List<TeamModel.TeamWithMembers> GetAllTeams(int CompanyId, string UserId)
        {
            List<TeamModel.TeamWithMembers> AllTeamsList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select m.Id as TeamId ,m.TeamName as TeamName, m.TeamBudget as TeamBudget,m.[Description] as TeamDescription,m.ParentId,m.ParentTeamId
                from Teams m left join TeamUsers tu  on m.Id = tu.TeamId 
                where m.IsDefault = 0 and m.IsDeleted = @CompanyId or m.IsDeleted = 0 and m.CompanyId = case when @CompanyId = 0 then m.CompanyId else @CompanyId end
                and( tu.UserId = @UserId or m.CreatedBy =  @UserId ) AND m.IsDefault=0 and m.ParentTeamId=0
                group by m.Id,m.TeamName, m.TeamBudget, m.[Description],m.ParentId,m.ParentTeamId";

                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId, UserId = UserId });
                AllTeamsList = Result.Read<TeamModel.TeamWithMembers>().ToList();
            }

            return AllTeamsList;
        }


        public List<UserRoleModel> GetTeamMembersByTeamId(int CompanyId, int TeamId)
        {
            List<UserRoleModel> TeamMembersByTeamIdList;
            using (var con = new SqlConnection(ConnectionString))
            {
                //               string query = @"select m2.TeamId, m2.Email as MemberEmail,m2.FirstName as MemberName,m1.IsActive IsActive, 
                //case when m2.ProfilePhoto is null then 'Uploads/Default/profile.png' else 'Uploads/Profile/Icon/'+m2.ProfilePhoto end
                //as MemberProfilePhoto, m2.UserId as MemberUserId,m1.RoleId as MemberRoleId   ,R.Name AS Text,m1.DesignationId AS MemberDesignationId,R.IsRole
                //from TeamUsers m1 inner join UserDetails m2 on m1.UserId = m2.UserId 
                //LEFT JOIN Roles R on R.RoleId=m1.DesignationId 
                //LEFT JOIN Teams T on T.Id=m1.TeamId 
                //where m1.TeamId=@TeamId AND T.CompanyId=@CompanyId
                //and m2.IsDeleted = 0 and m1.IsActive = 1 and m2.IsDeleted = 0 order by m1.CreatedDate";


                string query = @"select m2.FirstName +' '+ m2.LastName as Name,m1.RoleId, m2.UserId from TeamUsers m1 inner join UserDetails m2 on m1.UserId = m2.UserId 
                LEFT JOIN Roles R on R.RoleId=m1.DesignationId 
                LEFT JOIN Teams T on T.Id=m1.TeamId 
                where m1.TeamId=@TeamId AND T.CompanyId=@CompanyId
                and m2.IsDeleted = 0 and m1.IsActive = 1 and m2.IsDeleted = 0 order by m1.CreatedDate";

                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId, TeamId = TeamId });
                TeamMembersByTeamIdList = Result.Read<UserRoleModel>().ToList();
            }

            return TeamMembersByTeamIdList;
        }

        public string getCompanyName(int CompanyId)
        {

            /// //////////////////////////////////////////////////

            CompanyModal returnObj = new CompanyModal();
            string CompanyName = "";
            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @"select Id as CompanyId ,Name
                from Companies 
                where id = @CompanyId";

                var Result = con.QueryMultiple(query, new { CompanyId = CompanyId });
                returnObj = Result.Read<CompanyModal>().FirstOrDefault();
                if (returnObj != null)
                {
                    CompanyName = returnObj.Name;
                }
            }
            return CompanyName;
        }

    }
}
