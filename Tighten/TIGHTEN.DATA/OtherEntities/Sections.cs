﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class Sections : BaseClass
    {
        AppContext dbcontext;

        //public object GetSections(int ProjectId)
        //{

        //    return (from m in dbcontext.Sections where m.ProjectId == ProjectId orderby m.DefaultStatus descending select m).ToList();
        //}


        /// <summary>
        /// Getting Sections For a particular Project
        /// </summary>
        /// <param name="ProjectId">It is Unique id of the project</param>
        /// <returns>It returns the sections as a list</returns>
        public List<TodoModel.SectionData> GetSectionForParticularProject(int ProjectId)
        {

            List<TodoModel.SectionData> sectionObject = (from m in dbcontext.Sections
                                                         where m.ProjectId == ProjectId

                                                         select new TodoModel.SectionData
                                                         {
                                                             Id = m.Id,
                                                             ProjectId = ProjectId,
                                                             SectionName = m.SectionName
                                                         }
                                                           ).ToList();

            return sectionObject;

        }

        public object GetUpdatableSections(int ProjectId)
        {

            return (from m in dbcontext.Sections where m.ProjectId == ProjectId && m.IsDefault != true select m).ToList();
        }

        public int SaveSection(string UserId, SectionModel.SectionAddModel obj)
        {
            //Section sec = new Section
            //{
            //    ProjectId = obj.ProjectId,
            //    SectionName = "New Section",
            //    CreatedBy = UserId,
            //    ModifiedBy = UserId,
            //    CreatedDate = DateTime.Now,
            //    ModifiedDate = DateTime.Now,
            //    IsDefault = false
            //};

            ///*Save Section*/
            //dbcontext.Sections.Add(sec);
            //dbcontext.SaveChanges();
            //return sec.Id;
            int SectionId = 0 ;
            using (var con = new SqlConnection(ConnectionString))
            {
                SectionId = con.Query<int>("usp_SectionInsert", new { ProjectId = obj.ProjectId, UserId = UserId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return SectionId;
        }

        public string UpdateSection(string UserId, SectionModel.SectionUpdateModel obj)
        {
            string msg = string.Empty;

            //var isSectionExist = (from sc in dbcontext.Sections
            //                      where sc.Id == obj.SectionId
            //                      && sc.ProjectId == obj.ProjectId
            //                      select sc.Id).SingleOrDefault();

            //if (isSectionExist > 0)
            //{
            //    Section sec = (from m in dbcontext.Sections where m.Id == obj.SectionId select m).SingleOrDefault();

            //    sec.SectionName = obj.SectionName;
            //    sec.ModifiedBy = UserId;
            //    sec.ModifiedDate = DateTime.Now;

            //    dbcontext.SaveChanges();
            //    msg = "SectionUpdatedSuccess";
            //}
            //else
            //{
            //    msg = "SectionNotExist"; 
            //}

            using (var con = new SqlConnection(ConnectionString))
            {
                int count = con.Execute("usp_SectionUpdate",new { SectionName  = obj.SectionName , UserId = UserId , SectionId = obj.SectionId, ProjectId = obj.ProjectId },commandType:CommandType.StoredProcedure);
                msg = "SectionUpdatedSuccess";
            }

            return msg;
        }

        public void UpdateSectionOrder(List<TodoModel.Sections> obj)
        {

            for (int i = 0; i < obj.Count; i++)
            {
                int sectionId = obj[i].Id;
                Section sectionObject = (from m in dbcontext.Sections where m.Id == sectionId select m).SingleOrDefault();
                sectionObject.ListOrder = i;
                dbcontext.SaveChanges();
            }
        }


        public string DeleteSection(int Id)
        {
            string msg = string.Empty;
            //Section sec = (from m in dbcontext.Sections where m.Id == Id select m).SingleOrDefault();

            //var allSections = from allSec in dbcontext.Sections
            //                  where allSec.ProjectId == sec.ProjectId
            //                  select allSec;

            //if (allSections.Count() > 1)
            //{
            //    var TodoInSection = (from t in dbcontext.ToDos
            //                         where t.SectionId == Id
            //                         && t.IsDeleted == false
            //                         select t).ToList();

            //    if (TodoInSection.Count() > 0)
            //    {
            //        msg = "Can't delete !! Todos are still there in this section";
            //    }
            //    else
            //    {
            //        dbcontext.Sections.Remove(sec);
            //        dbcontext.SaveChanges();
            //        msg = "section deleted";
            //    }
            //}

            using (var con = new SqlConnection(ConnectionString))
            {
                msg = con.Query<string>("usp_SectionDelete", new { SectionId = Id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return msg;
        }


        public void UpdateTodosListOrderOrSectionInSection(List<TodoModel.TodosData> obj)
        {

            for (int i = 0; i < obj.Count; i++)
            {
                int todoId = obj[i].TodoId;
                int sectionId = obj[i].SectionId;
                ToDo todoObject = (from m in dbcontext.ToDos where m.Id == todoId && m.IsDeleted == false select m).SingleOrDefault();
                todoObject.ListOrder = i;
                todoObject.SectionId = sectionId;
                dbcontext.SaveChanges();

                if (obj[i].ActiveTodoId  == todoId)
                {
                    var SectionName = (from secname in dbcontext.Sections
                                       where secname.Id == sectionId
                                       select secname.SectionName).SingleOrDefault();
                    /* Saving Notification for Todo */
                    TodoNotifications objtodoNotifications = new TodoNotifications
                    {
                        TodoId = obj[i].TodoId,
                        Description = "has moved this todo to "+ SectionName + " section",
                        CreatedBy = obj[i].ActiveUserId,
                        CreatedDate = DateTime.Now,
                        Type = 1
                    };

                    /* Save TodoNotification */
                    dbcontext.TodoNotifications.Add(objtodoNotifications);
                    dbcontext.SaveChanges();
                }
                
            }

        }
    }
}
