﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using Dapper;
using System.Data;

namespace TIGHTEN.DATA
{
    public class Feedback : BaseClass
    {
        AppContext dbcontext;

        /// <summary>
        /// Getting User's projects in which user is Member 
        /// </summary>
        /// <param name="ProjectId">It is unique Id of Project</param>
        /// <returns>List of User's Projects in it as an array</returns>
        public List<MODEL.FeedbackModel.Feedback> GetProjects(int CompanyId, string UserId)
        {
        //    var AllProjectList = (from prjct in dbcontext.Projects
        //                          join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId
        //                          where prjct.CompanyId == (CompanyId == 0 ? prjct.CompanyId : CompanyId)
        //                          && prjctusr.UserId == UserId && prjct.IsDeleted == false
        //                          select new
        //                          {
        //                              ProjectId = prjct.Id,
        //                              ProjectName = prjct.Name,
        //                              ProjectDescription = prjct.Description,
        //                              ProjectRemarks = prjct.Remarks,
        //                              CompanyId = prjct.CompanyId,
        //                              FeedbackOfProject = (from fed in dbcontext.ProjectFeedbacks
        //                                                   join usr in dbcontext.UserDetails on fed.CreatedBy equals usr.UserId
        //                                                   join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
        //                                                   join rol in dbcontext.Roles on usrcom.RoleId equals rol.RoleId
        //                                                   where fed.ProjectId == prjct.Id && usr.IsDeleted == false
        //                                                   && fed.UserId == UserId && fed.Feedback != null
        //                                                   && rol.IsDeleted == false //&& rol.RoleId > 2 
        //                                                   select new MODEL.FeedbackModel.FeedbackOfProject
        //                                                   {
        //                                                       Id = fed.Id,
        //                                                       //ProjectId = fed.ProjectId,
        //                                                       //ProjectName = prjct.Name,
        //                                                       //ProjectDescription = prjct.Description,
        //                                                       MemberName = usr.FirstName,
        //                                                       MemberRole = rol.Name,
        //                                                       MemberFeedback = fed.Feedback,
        //                                                       IsSelected = fed.IsSelected
        //                                                   })
                                                          
        //}) ;




            //List<MODEL.FeedbackModel.Feedback> ProjectList1 = AllProjectList.AsEnumerable().Select(x =>
            //new MODEL.FeedbackModel.Feedback
            //{
            //    ProjectId = x.ProjectId,
            //    ProjectName = x.ProjectName,
            //    ProjectDescription = x.ProjectDescription,
            //    ProjectRemarks = x.ProjectRemarks,
            //    CompanyId = x.CompanyId,
            //    FeedbackOfProject = x.FeedbackOfProject.ToList()

            //}).ToList();


            List<MODEL.FeedbackModel.Feedback> ProjectList;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetProjectsForFeedback", new { UserId = UserId, CompanyId = CompanyId }, commandType: System.Data.CommandType.StoredProcedure);

                ProjectList = result.Read<MODEL.FeedbackModel.Feedback>().ToList();
                if (ProjectList != null)
                {
                    var FeedbackOfProject = result.Read<MODEL.FeedbackModel.FeedbackOfProject>().ToList();
                    foreach (var item in ProjectList)
                    {
                        item.FeedbackOfProject = FeedbackOfProject.Where(x => x.ProjectId == item.ProjectId).ToList();
                    }
                }
            }

            return ProjectList;

        }



        public List<MODEL.FeedbackModel.FeedbackOfProject> ShowAllFeedbacksOfProject(int ProjectId, string UserId)
        {
            //List<MODEL.FeedbackModel.FeedbackOfProject> ProjectListWithFeedback = (from fed in dbcontext.ProjectFeedbacks
            //                                                                       join prjct in dbcontext.Projects on fed.ProjectId equals prjct.Id
            //                                                                       join usr in dbcontext.UserDetails on fed.CreatedBy equals usr.UserId
            //                                                                       join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
            //                                                                       join rol in dbcontext.Roles on usrcom.RoleId equals rol.RoleId
            //                                                                       where fed.ProjectId == ProjectId && usr.IsDeleted == false
            //                                                                       && fed.UserId == UserId && fed.Feedback != null
            //                                                                       && prjct.IsDeleted == false && rol.IsDeleted == false  //&& rol.RoleId > 2 
            //                                                                       select new MODEL.FeedbackModel.FeedbackOfProject
            //                                                                       {
            //                                                                            Id = fed.Id,
            //                                                                            ProjectId = fed.ProjectId,
            //                                                                            ProjectName = prjct.Name,
            //                                                                            ProjectDescription = prjct.Description,
            //                                                                            MemberName = usr.FirstName,
            //                                                                            MemberRole = rol.Name,
            //                                                                            MemberFeedback = fed.Feedback,
            //                                                                            IsSelected = fed.IsSelected
            //                                                                       }).ToList();


            List<MODEL.FeedbackModel.FeedbackOfProject> ProjectListWithFeedback;
            using (var con = new SqlConnection(ConnectionString))
            {
                ProjectListWithFeedback = con.Query<MODEL.FeedbackModel.FeedbackOfProject>("usp_GetAllFeedbacksOfProject", new { ProjectId = ProjectId, UserId = UserId }, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }


            return ProjectListWithFeedback;
        }



        public void ShowOnProfile(MODEL.FeedbackModel.ShowOnProfileModel model)
        {
            //foreach (var item in model.FeedbackOfProject)
            //{
            //    var fdback = (from f in dbcontext.ProjectFeedbacks
            //                    where f.Id == item.Id
            //                    select f).FirstOrDefault();

            //    if (fdback != null)
            //    {
            //        fdback.IsSelected = item.IsSelected;
            //        dbcontext.Entry(fdback).State = System.Data.Entity.EntityState.Modified;
            //    }

            //}
            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("usp_SetFeedbacksOfProjectToShowOnProfile", new
                {
                    FeedbackOfProject = model.FeedbackOfProject.AsTableValuedParameter("FeedbacksOfProjectToShowOnProfile", new[] { "Id", "IsSelected" })

                }, commandType: CommandType.StoredProcedure);
            }

        }



    }
}
