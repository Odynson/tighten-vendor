﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class Followers : BaseClass
    {
        AppContext dbcontext;

        public List<FollowerModel.FollowerDetailsModel> getFollowers(int ToDoId)
        {
            //List<FollowerModel.FollowerDetailsModel> followersModel = (from p in dbcontext.Followers
            //                                                           join p2 in dbcontext.UserDetails on p.UserId equals p2.UserId
            //                                                           where p.TodoId == ToDoId && p2.IsDeleted == false
            //                                                           let DisplayPhoto = (p2.ProfilePhoto == null ? "False" : "True")
            //                                                           let ProfilePhoto = (p2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + p2.ProfilePhoto)
                                                                      
            //                                                           select new FollowerModel.FollowerDetailsModel
            //                                                           {
            //                                                               FollowerName = p2.FirstName + " " + p2.LastName,
            //                                                               FollowerProfilePhoto = ProfilePhoto,
            //                                                               FollowerEmail = p2.Email,
            //                                                               Value = p.UserId,
            //                                                               Text = p2.FirstName + " " + p2.LastName + " ( " + p2.Email + " )"

            //                                                           }).ToList();


            List<FollowerModel.FollowerDetailsModel> followersModel;
            using (var con = new SqlConnection(ConnectionString))
            {
                followersModel = con.Query<FollowerModel.FollowerDetailsModel>("usp_GetFollowers", new { ToDoId = ToDoId }, commandType: CommandType.StoredProcedure).ToList();
            }


            return followersModel;
        }

        public object getUsers(int ToDoId, string UserId)
        {
            var result1 = (from e in dbcontext.UserDetails where e.UserId == UserId && e.IsDeleted == false select e)
                      .SingleOrDefault();
            var CompanyId = result1.CompanyId;
            var result2 = (from vs in dbcontext.UserDetails
                           //join p3 in dbcontext.Users on vs.UserId equals p3.Id
                           where vs.CompanyId == CompanyId && !(from s in dbcontext.Followers where s.TodoId == ToDoId select s.UserId).Contains(vs.UserId)
                           && vs.IsDeleted == false
                           let DisplayPhoto = (vs.ProfilePhoto == null ? "False" : "True")
                           let ProfilePhoto = (vs.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + vs.ProfilePhoto)

                           select new { vs.FirstName, vs.Id, vs.LastName, DisplayPhoto, ProfilePhoto, vs.UserId, vs.Email }).ToList();
            return result2;
        }

        //public object getMyFollowedToDos(string UserId)
        //{
        //    UserDetail userdetail = (from m in dbcontext.UserDetails where m.UserId == UserId select m).FirstOrDefault();

        //    return (from e in dbcontext.ToDos
        //            join e1 in dbcontext.Projects on e.ProjectId equals e1.Id
        //            join e4 in dbcontext.Followers on e.Id equals e4.TodoId
        //            join e5 in dbcontext.Sections on e.SectionId equals e5.Id
        //            join e2 in dbcontext.UserDetails on e.AssigneeId equals e2.UserId
        //            into b
        //            where e4.UserId == UserId && e1.TeamId == userdetail.SelectedTeamId && e.IsDone == false
        //            let Display = (e.CreatedBy == UserId ? "False" : "True")
        //            let ProjectName = e1.Name

        //            from e3 in b.DefaultIfEmpty()
        //            let ProfilePhoto = (e3.ProfilePhoto == null ? null : "Uploads/Profile/Thumbnail/" + e3.ProfilePhoto)
        //            let DisplayPhoto = (e3.ProfilePhoto == null ? "False" : "True")
        //            let Assigned = (e.AssigneeId == null ? "False" : "True")
        //            select new { e.Id, e.Name, ProjectName, e3.FirstName, e3.LastName, ProfilePhoto, DisplayPhoto, Assigned, Display, e5.SectionName, e.InProgress, e.ProjectId }).ToList();
        //}

        public void saveFollower(int ToDoId, string FollowerUserId, string UserId)
        {
            Follower follower = new Follower();
            {
                follower.TodoId = ToDoId;
                follower.UserId = FollowerUserId;
                follower.CreatedBy = UserId;
                follower.CreatedDate = DateTime.Now.ToUniversalTime();

            };

            /* Save ToDoAttachments */
            dbcontext.Followers.Add(follower);
            dbcontext.SaveChanges();

        }



        public void deleteFollower(int Id)
        {

            Follower follower = (from e in dbcontext.Followers
                                 where e.Id == Id
                                 select e).SingleOrDefault();

            /* Delete Follower */

            dbcontext.Followers.Remove(follower);

            dbcontext.SaveChanges();
        }



        /// <summary>
        /// IT updated the Followers of a particular Todo
        /// </summary>
        /// <param name="UserId">It is the userid of logged in user</param>
        /// <param name="model">It consists the TodoId and Udpated values of new Followers</param>
        public void UpdateFollowers(string UserId, FollowerModel.FollowerUpdateModel model, int CompanyId)
        {

            //string[] existingFollowers = (from m in dbcontext.Followers where m.TodoId == model.TodoId select m.UserId).ToArray();

            string[] existingFollowers;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select m.UserId from Followers m where m.TodoId = @TodoId ";
                existingFollowers = con.Query<string>(sqlquery, new { TodoId = model.TodoId }).ToArray();
            }

            string[] existingFollowersToBeDeleted = existingFollowers.Except(model.Followers).ToArray();

            string[] newFollowersToBeAdded = model.Followers.Except(existingFollowers).ToArray();

            List<FollowerModel.FollowersUpdateModel> ExistingFollowersToBeDeletedList = new List<FollowerModel.FollowersUpdateModel>();
            List<FollowerModel.FollowersUpdateModel> NewFollowersToBeAddedList = new List<FollowerModel.FollowersUpdateModel>();


            for (int i = 0; i < existingFollowersToBeDeleted.Length; i++)
            {
                FollowerModel.FollowersUpdateModel obj = new FollowerModel.FollowersUpdateModel
                {
                    FollowerUserId = existingFollowersToBeDeleted[i],
                    CompanyId = CompanyId,
                    TodoId = model.TodoId,
                    UserId = UserId
                };

                ExistingFollowersToBeDeletedList.Add(obj);
            }

            for (int i = 0; i < newFollowersToBeAdded.Length; i++)
            {
                FollowerModel.FollowersUpdateModel obj = new FollowerModel.FollowersUpdateModel
                {
                    FollowerUserId = newFollowersToBeAdded[i],
                    CompanyId = CompanyId,
                    TodoId = model.TodoId,
                    UserId = UserId
                };

                NewFollowersToBeAddedList.Add(obj);
            }



            using (var con = new SqlConnection(ConnectionString))
            {
                con.Execute("usp_FollowersUpdate", new
                {
                    existingFollowersToBeDeleted = ExistingFollowersToBeDeletedList.AsTableValuedParameter("FollowersUpdate", new[] { "FollowerUserId", "TodoId", "CompanyId", "UserId" }),
                    newFollowersToBeAdded = NewFollowersToBeAddedList.AsTableValuedParameter("FollowersUpdate", new[] { "FollowerUserId", "TodoId", "CompanyId", "UserId" })

                }, commandType: CommandType.StoredProcedure);
            }





            //if (existingFollowersToBeDeleted != null)
            //{

            //    string existingFollowersToBeDeletedString = string.Join(",", existingFollowersToBeDeleted.Select(x => "'" + x + "'"));

            //    using (var con = new SqlConnection(ConnectionString))
            //    {
            //        string[] asd = con.Query<string>("usp_FollowersUpdate", new { existingFollowersToBeDeletedString = existingFollowersToBeDeletedString }, commandType: CommandType.StoredProcedure).ToArray();
            //    }




            //    for (int i = 0; i < existingFollowersToBeDeleted.Length; i++)
            //        {
            //            string UserIdToBeDeleted = existingFollowersToBeDeleted[i];
            //            Follower followerToBeDeletedEntity = (from m in dbcontext.Followers
            //                                                  where m.UserId == UserIdToBeDeleted && m.TodoId == model.TodoId
            //                                                  select m).SingleOrDefault();

            //            dbcontext.Followers.Remove(followerToBeDeletedEntity);
            //            dbcontext.SaveChanges();

            //            /* Saving Notification for Todo */
            //            TodoNotifications objtodoNotifications = new TodoNotifications
            //            {
            //                TodoId = model.TodoId,
            //                Description = "has removed this Follower.",
            //                CreatedBy = UserId,
            //                CreatedDate = DateTime.Now,
            //                Type = 3,
            //                ConcernedUserId = existingFollowersToBeDeleted[i]
            //            };

            //            /* Save TodoNotification */
            //            dbcontext.TodoNotifications.Add(objtodoNotifications);


            //            #region Deleting default values from SettingConfigs for deleted followers
            //            /* getting all Settings for the user  */
            //            UserSettingConfig usrsetting = (from usrset in dbcontext.UserSettingConfigs
            //                                            join set in dbcontext.SettingConfigs on usrset.NotificationId equals set.Id
            //                                            where usrset.UserId == UserIdToBeDeleted
            //                                            && usrset.CompanyId == CompanyId
            //                                            && set.NotificationName == "Todo Update"
            //                                            select usrset).FirstOrDefault();

            //            string TodoAfterDeletion = string.Empty;
            //            int[] AllTodos = null;
            //            if (usrsetting.TodoId != null && usrsetting.TodoId != string.Empty)
            //            {
            //                AllTodos = usrsetting.TodoId.Split(',').Select(str => int.Parse(str)).ToArray();
            //                foreach (var item in AllTodos)
            //                {
            //                    if (item != model.TodoId)
            //                    {
            //                        if (TodoAfterDeletion == string.Empty)
            //                        {
            //                            TodoAfterDeletion = item.ToString();
            //                        }
            //                        else
            //                        {
            //                            TodoAfterDeletion += "," + item.ToString();
            //                        }
            //                    }
            //                }
            //                usrsetting.TodoId = TodoAfterDeletion;
            //            }

            //            #endregion
            //            dbcontext.SaveChanges();


            //        }
            //}

            //if (newFollowersToBeAdded != null)
            //{
            //    string newFollowersToBeAddedString = string.Join(",", newFollowersToBeAdded.Select(x => "'" + x + "'"));

            //    using (var con = new SqlConnection(ConnectionString))
            //    {
            //        con.Execute("usp_FollowersUpdate", new { existingFollowersString = newFollowersToBeAddedString, UserId = UserId , TodoId = model.TodoId }, commandType: CommandType.StoredProcedure);
            //    }



            //    for (int i = 0; i < newFollowersToBeAdded.Length; i++)
            //    {

            //        Follower followerToBeAddedEntity = new Follower
            //        {
            //            UserId = newFollowersToBeAdded[i],
            //            CreatedBy = UserId,
            //            CreatedDate = DateTime.Now,
            //            TodoId = model.TodoId

            //        };

            //        dbcontext.Followers.Add(followerToBeAddedEntity);
            //        dbcontext.SaveChanges();


            //        /* Saving Notification for Todo */
            //        TodoNotifications objtodoNotifications = new TodoNotifications
            //        {
            //            TodoId = model.TodoId,
            //            Description = "has added a new Follower.",
            //            CreatedBy = UserId,
            //            CreatedDate = DateTime.Now,
            //            Type = 3,
            //            ConcernedUserId = newFollowersToBeAdded[i]
            //        };

            //        /* Save TodoNotification */
            //        dbcontext.TodoNotifications.Add(objtodoNotifications);

            //        #region Adding TodoId in UserSettingConfig for each Follower

            //        /* getting all Settings for the user  */
            //        string Members_UserId = newFollowersToBeAdded[i];

            //        UserSettingConfig usrsetting = (from usrset in dbcontext.UserSettingConfigs
            //                                        join set in dbcontext.SettingConfigs on usrset.NotificationId equals set.Id
            //                                        where set.IsActive == true
            //                                        && usrset.CompanyId == CompanyId
            //                                        && usrset.UserId == Members_UserId
            //                                        && set.NotificationName == "Todo Update"
            //                                        select usrset).FirstOrDefault();

            //        if (usrsetting.TodoId != null && usrsetting.TodoId != string.Empty)
            //        {
            //            usrsetting.TodoId += "," + model.TodoId.ToString();
            //        }
            //        else
            //        {
            //            usrsetting.TodoId = model.TodoId.ToString();
            //        }

            //        //dbcontext.SaveChanges();
            //        #endregion

            //        dbcontext.SaveChanges();
            //    }
            //}



        }




    }
}
