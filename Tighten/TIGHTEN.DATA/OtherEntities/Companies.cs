﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{



    public class Companies : BaseClass
    {

        /// <summary>
        /// Declaration of AppContext Class
        /// </summary>
        AppContext dbContext;

        /// <summary>
        /// It is a function for Updating Company Logo 
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged In User</param>
        /// <param name="Logo">Logoname of newly updated Logo</param>
        /// <param name="CompanyId">Unique Id of Company</param>
        /// <returns>Previous Logo of Company which we will delete</returns>
        public string UpdateCompanyLogo(string UserId, string Logo, int CompanyId)
        {

            string PreviousLogo;
            using (var con = new SqlConnection(ConnectionString))
            {

                PreviousLogo = con.Query<string>("Usp_UpdateCompanyLogo", new { CompanyId = CompanyId, UserId  = UserId , Logo = Logo }, commandType: CommandType.StoredProcedure).SingleOrDefault();


                return PreviousLogo;
            }







            //dbContext = new AppContext();

            //string PreviousLogo = string.Empty;
            //Company compEntity = (from m in dbContext.Companies where m.Id == CompanyId select m).SingleOrDefault();
            //PreviousLogo = compEntity.Logo;
            //compEntity.Logo = Logo;
            //compEntity.ModifiedBy = UserId;
            //compEntity.ModifiedDate = DateTime.Now;
            //dbContext.SaveChanges();

            //return PreviousLogo;
        }


        /// <summary>
        /// It is a function for Getting Particular Company's Details
        /// </summary>
        /// <param name="CompanyId">It is the unique Id of Company</param>
        /// <returns> List of Company Details like name, description, email etc</returns>
        public object GetCompanyDetails(int CompanyId)
        {
            List<CompanyModel> compMod1;
            using (var con = new SqlConnection(ConnectionString))
            {

                compMod1 = con.Query<CompanyModel>("Usp_GetCompanyProfileDetail", new { CompanyId = CompanyId }, commandType: CommandType.StoredProcedure).ToList();


                return compMod1;
            }









            dbContext = new AppContext();

            //List<CompanyModel> compMod = (from m in dbContext.Companies
            //                              join m1 in dbContext.UserDetails on m.CreatedBy equals m1.UserId
                                     
            //                              where m.Id == CompanyId && m1.IsDeleted == false 
            //                              select new CompanyModel
            //                              {
            //                                  CompanyId = m.Id,
            //                                  CompanyAddress = m.Address,
            //                                  CompanyDescription = m.Description,
            //                                  CompanyEmail = m.Email,
            //                                  CompanyLogo = "Uploads/CompanyLogos/" + (string.IsNullOrEmpty(m.Logo) ? "" : m.Logo),
            //                                  CompanyName = m.Name,
            //                                  CompanyWebsite = m.Website,
            //                                  CompanyActiveSince = m.CreatedDate,
            //                                  CompanyAdmin = m1.FirstName,
            //                                  CompanyFax = m.Fax,
            //                                  CompanyPhoneNo = m.PhoneNo,
                 

            //                                  CompanyProjectsCount = (from p in dbContext.Projects
            //                                                          join t in dbContext.Teams on p.TeamId equals t.Id
            //                                                          where p.CompanyId == m.Id && p.IsDeleted == false
            //                                                          && t.IsDefault == false && t.IsDeleted == false
            //                                                          select p).Count(),

            //                                  CompanyTeamsCount = (from t in dbContext.Teams
            //                                                       where t.CompanyId == m.Id && t.IsDefault == false
            //                                                       && t.IsDeleted == false
            //                                                       select t).Count()

            //                              }

            //                                ).ToList();

            //return compMod;

        }


        /// <summary>
        /// It is a function for Clearing/Removing Logo of Particular Company
        /// </summary>
        /// <param name="CompanyId">It is the unique Id of the Company</param>
        public void ClearCompanyLogo(int CompanyId)
        {
            dbContext = new AppContext();

            Company compEntity = (from m in dbContext.Companies where m.Id == CompanyId select m).SingleOrDefault();
            compEntity.Logo = null;

            dbContext.SaveChanges();

        }


        /// <summary>
        /// This function Updates the Company Details other than Logo
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void EditCompanyDetails(string UserId, CompanyModel mod)
        {
            dbContext = new AppContext();

            Company compEntity = (from m in dbContext.Companies where m.Id == mod.CompanyId select m).SingleOrDefault();

            compEntity.Name = mod.CompanyName;
            compEntity.ModifiedDate = DateTime.Now;
            compEntity.Website = mod.CompanyWebsite;
            compEntity.Address = mod.CompanyAddress;
            compEntity.Description = mod.CompanyDescription;
            compEntity.Email = mod.CompanyEmail;
            compEntity.ModifiedBy = UserId;
            compEntity.Fax = mod.CompanyFax;
            compEntity.PhoneNo = mod.CompanyPhoneNo;

            dbContext.SaveChanges();
        }



        /// <summary>
        /// This function Updates the Company Name
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyName(NewCompanyModel mod)
        {



            int objResult1;
            using (var con = new SqlConnection(ConnectionString))
            {
                objResult1 = con.Query<int>("Usp_UpdateCompanyProfile", new
                {
                    CompanyName = mod.Company.CompanyName,
                    CompanyPhoneNo = mod.Company.CompanyPhoneNo,
                    CompanyFax = mod.Company.CompanyFax,
                    CompanyEmail = mod.Company.CompanyEmail,
                    CompanyAddress = mod.Company.CompanyAddress,
                    CompanyWebsite = mod.Company.CompanyWebsite,
                    CompanyDescription = mod.Company.CompanyDescription,
                    CompanyId = mod.Company.CompanyId

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }



            //dbContext = new AppContext();

            //Company compEntity = (from m in dbContext.Companies where m.Id == mod.Company.CompanyId select m).SingleOrDefault();

            //compEntity.Name = mod.Company.CompanyName;
            //compEntity.ModifiedDate = DateTime.Now;
            //compEntity.ModifiedBy = mod.UserId;

            //dbContext.SaveChanges();
        }


        /// <summary>
        /// This function Updates the Company Web Address
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyWebAddress(NewCompanyModel mod)
        {




            int objResult1;

            if (mod.Company.CompanyEmail != null)
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    objResult1 = con.Query<int>("Usp_UpdateCompanyProfile", new
                    {
                        CompanyName = mod.Company.CompanyName,
                        CompanyPhoneNo = mod.Company.CompanyPhoneNo,
                        CompanyFax = mod.Company.CompanyFax,
                        CompanyEmail = mod.Company.CompanyEmail,
                        CompanyAddress = mod.Company.CompanyAddress,
                        CompanyWebsite = mod.Company.CompanyWebsite,
                        CompanyDescription = mod.Company.CompanyDescription,
                        CompanyId = mod.Company.CompanyId

                    }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                }

            }

            //dbContext = new AppContext();

            //Company compEntity = (from m in dbContext.Companies where m.Id == mod.Company.CompanyId select m).SingleOrDefault();

            //compEntity.Website = mod.Company.CompanyWebsite;
            //compEntity.ModifiedDate = DateTime.Now;
            //compEntity.ModifiedBy = mod.UserId;

            //dbContext.SaveChanges();
        }


        /// <summary>
        /// This function Updates the Company Phone No
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyPhoneNo(NewCompanyModel mod)
        {
            int objResult1;
            using (var con = new SqlConnection(ConnectionString))
            {
                objResult1 = con.Query<int>("Usp_UpdateCompanyProfile", new
                {
                    CompanyName = mod.Company.CompanyName,
                    CompanyPhoneNo = mod.Company.CompanyPhoneNo,
                    CompanyFax = mod.Company.CompanyFax,
                    CompanyEmail = mod.Company.CompanyEmail,
                    CompanyAddress = mod.Company.CompanyAddress,
                    CompanyWebsite = mod.Company.CompanyWebsite,
                    CompanyDescription = mod.Company.CompanyDescription,
                    CompanyId = mod.Company.CompanyId

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }





            //dbContext = new AppContext();

            //Company compEntity = (from m in dbContext.Companies where m.Id == mod.Company.CompanyId select m).SingleOrDefault();

            //compEntity.PhoneNo = mod.Company.CompanyPhoneNo;
            //compEntity.ModifiedDate = DateTime.Now;
            //compEntity.ModifiedBy = mod.UserId;

            //dbContext.SaveChanges();
        }


        /// <summary>
        /// This function Updates the Company Fax
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyFax(NewCompanyModel mod)
        {

            int objResult1;
            using (var con = new SqlConnection(ConnectionString))
            {
                objResult1 = con.Query<int>("Usp_UpdateCompanyProfile", new
                {
                    CompanyName = mod.Company.CompanyName,
                    CompanyPhoneNo = mod.Company.CompanyPhoneNo,
                    CompanyFax = mod.Company.CompanyFax,
                    CompanyEmail = mod.Company.CompanyEmail,
                    CompanyAddress = mod.Company.CompanyAddress,
                    CompanyWebsite = mod.Company.CompanyWebsite,
                    CompanyDescription = mod.Company.CompanyDescription,
                    CompanyId = mod.Company.CompanyId

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }





            //dbContext = new AppContext();

            //Company compEntity = (from m in dbContext.Companies where m.Id == mod.Company.CompanyId select m).SingleOrDefault();

            //compEntity.Fax = mod.Company.CompanyFax;
            //compEntity.ModifiedDate = DateTime.Now;
            //compEntity.ModifiedBy = mod.UserId;

            //dbContext.SaveChanges();
        }


        /// <summary>
        /// This function Updates the Company Email
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyEmail(NewCompanyModel mod)
        {

            int objResult1;
            using (var con = new SqlConnection(ConnectionString))
            {
                objResult1 = con.Query<int>("Usp_UpdateCompanyProfile", new
                {
                    CompanyName = mod.Company.CompanyName,
                    CompanyPhoneNo = mod.Company.CompanyPhoneNo,
                    CompanyFax = mod.Company.CompanyFax,
                    CompanyEmail = mod.Company.CompanyEmail,
                    CompanyAddress = mod.Company.CompanyAddress,
                    CompanyWebsite = mod.Company.CompanyWebsite,
                    CompanyDescription = mod.Company.CompanyDescription,
                    CompanyId = mod.Company.CompanyId

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }


            //dbContext = new AppContext();

            //Company compEntity = (from m in dbContext.Companies where m.Id == mod.Company.CompanyId select m).SingleOrDefault();

            //compEntity.Email = mod.Company.CompanyEmail;
            //compEntity.ModifiedDate = DateTime.Now;
            //compEntity.ModifiedBy = mod.UserId;

            //dbContext.SaveChanges();
        }


        /// <summary>
        /// This function Updates the Company Address
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyAddress(NewCompanyModel mod)
        {

            int objResult1;
            using (var con = new SqlConnection(ConnectionString))
            {
                objResult1 = con.Query<int>("Usp_UpdateCompanyProfile", new
                {
                    CompanyName = mod.Company.CompanyName,
                    CompanyPhoneNo = mod.Company.CompanyPhoneNo,
                    CompanyFax = mod.Company.CompanyFax,
                    CompanyEmail = mod.Company.CompanyEmail,
                    CompanyAddress = mod.Company.CompanyAddress,
                    CompanyWebsite = mod.Company.CompanyWebsite,
                    CompanyDescription = mod.Company.CompanyDescription,
                    CompanyId = mod.Company.CompanyId

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }



            //dbContext = new AppContext();

            //Company compEntity = (from m in dbContext.Companies where m.Id == mod.Company.CompanyId select m).SingleOrDefault();

            //compEntity.Address = mod.Company.CompanyAddress;
            //compEntity.ModifiedDate = DateTime.Now;
            //compEntity.ModifiedBy = mod.UserId;

            //dbContext.SaveChanges();
        }


        /// <summary>
        /// This function Updates the Company About Organization
        /// </summary>
        /// <param name="UserId">It is the Unique Id of Logged in User  who updated the Company Details</param>
        /// <param name="mod">It is the Model THat contains all the Updated Values</param>
        public void UpdateCompanyDescription(NewCompanyModel mod)
        {
            int objResult1;
            using (var con = new SqlConnection(ConnectionString))
            {
                objResult1 = con.Query<int>("Usp_UpdateCompanyProfile", new
                {
                    CompanyName = mod.Company.CompanyName,
                    CompanyPhoneNo = mod.Company.CompanyPhoneNo,
                    CompanyFax = mod.Company.CompanyFax,
                    CompanyEmail = mod.Company.CompanyEmail,
                    CompanyAddress = mod.Company.CompanyAddress,
                    CompanyWebsite = mod.Company.CompanyWebsite,
                    CompanyDescription = mod.Company.CompanyDescription,
                    CompanyId = mod.Company.CompanyId

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }

        }

        //dbContext = new AppContext();

        //Company compEntity = (from m in dbContext.Companies where m.Id == mod.Company.CompanyId select m).SingleOrDefault();

        //compEntity.Description = mod.Company.CompanyDescription;
        //compEntity.ModifiedDate = DateTime.Now;
        //compEntity.ModifiedBy = mod.UserId;

        //dbContext.SaveChanges();
    }


    
}
