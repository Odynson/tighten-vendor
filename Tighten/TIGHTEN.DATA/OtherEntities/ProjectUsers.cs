﻿using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.DATA
{
    public class ProjectUsers : Common
    {

        /// <summary>
        /// Getting ProjectMembers For a particular project who can access this project
        /// </summary>
        /// <param name="projectId">It is unique id of Project</param>
        /// <returns>Return the list of Project Members associated with project</returns>
        public List<ProjectModel.ProjectMembers> getProjectMembers(int projectId)
        {
            List<ProjectModel.ProjectMembers> projectMembersObject = (from m1 in dbcontext.ProjectUsers
                                                                      join m2 in dbcontext.UserDetails on m1.UserId equals m2.UserId
                                                                      where m1.ProjectId == projectId && m2.IsDeleted == false

                                                                      select new ProjectModel.ProjectMembers
                                                                      {
                                                                          MemberEmail = m2.Email,
                                                                          MemberName = m2.FirstName + " " + m2.LastName,
                                                                          MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m2.ProfilePhoto,
                                                                          MemberUserId = m2.UserId,
                                                                          Text = m2.FirstName + " " + m2.LastName + " ( " + m2.Email + " )",
                                                                          Value = m2.UserId
                                                                      }

                                                                        ).ToList();

            return projectMembersObject;

        }

        public object getNonProjectUsers(int projectId)
        {
            int teamId = Convert.ToInt32((from j in dbcontext.Projects where j.Id == projectId && j.IsDeleted == false select j.TeamId).SingleOrDefault());
            var nonprojectusers = (from m in dbcontext.TeamUsers
                                   join m3 in dbcontext.UserDetails on m.UserId equals m3.UserId
                                   where m.TeamId == teamId && m3.IsDeleted == false
                                   let Display = (m3.ProfilePhoto == null ? "False" : "True")
                                   let ProfilePhoto = (m3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m3.ProfilePhoto)
                                   select new { m.UserId, m3.FirstName, m3.LastName, m3.Email, Display, ProfilePhoto }
                                  ).Except(from m2 in dbcontext.ProjectUsers
                                           join m3 in dbcontext.UserDetails on m2.UserId equals m3.UserId
                                           where m2.ProjectId == projectId && m3.IsDeleted == false
                                           let Display = (m3.ProfilePhoto == null ? "False" : "True")
                                           let ProfilePhoto = (m3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m3.ProfilePhoto)
                                           select new { m3.UserId, m3.FirstName, m3.LastName, m3.Email, Display, ProfilePhoto }).ToList();
            return nonprojectusers;

        }

        public void AddProjectUser(String UserId, TodoModel.ProjectUsers model)
        {

            ProjectUser projectuser = new ProjectUser
            {
                UserId = model.UserId,
                ProjectId = model.ProjectId,
                CreatedBy = UserId,
                CreatedDate = DateTime.Now.ToUniversalTime()

            };
            dbcontext.ProjectUsers.Add(projectuser);
            dbcontext.SaveChanges();


        }

        public void DeleteProjectUser(int Id)
        {
            ProjectUser projectuser = (from m in dbcontext.ProjectUsers where m.Id == Id select m).SingleOrDefault();

            dbcontext.ProjectUsers.Remove(projectuser);
            dbcontext.SaveChanges();


        }

    }
}
