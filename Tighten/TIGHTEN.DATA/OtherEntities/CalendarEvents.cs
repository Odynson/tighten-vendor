﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;

namespace TIGHTEN.DATA
{

    public class CalendarEvents
    {
        /// <summary>
        /// Declaration of AppContext Class
        /// </summary>
        AppContext dbContext;


        /// <summary>
        /// It is a Function for getting Calendar Events for a particular Team
        /// </summary>
        /// <param name="TeamId">It is the TeamId for which we are getting Calendar Events</param>
        /// <returns>It returns the Calendar Events for a particular team</returns>
        public List<TodoModel.CalendarEvents> GetCalendarEvents(int CompanyId, string UserId, int TeamId, int ProjectId)
        {
            dbContext = new AppContext();

            //UserDetail userEntity = (from m in dbContext.UserDetails where m.UserId == UserId select m).SingleOrDefault();

            //List<TodoModel.CalendarEvents> cal = (from c in dbContext.Companies
            //                                      join t in dbContext.Teams on c.Id equals t.CompanyId
            //                                      join u in dbContext.TeamUsers on t.Id equals u.TeamId
            //                                      join m in dbContext.CalendarEvents on t.Id equals m.TeamId
            //                                      join m2 in dbContext.UserDetails on m.CreatedBy equals m2.UserId
            //                                      where c.Id == userEntity.CompanyId && u.UserId == UserId
            //                                      select new TodoModel.CalendarEvents
            //                                      {
            //                                          Id = m.Id,
            //                                          TodoId = 0,
            //                                          Title = m.Title,
            //                                          Start = m.StartDate,
            //                                          End = m.EndDate,
            //                                          AllDay = m.AllDay,
            //                                          Url = m.Url,
            //                                          ClassName = "eventInCalendar",
            //                                          Type = "Event",
            //                                          Editable = true,
            //                                          Description = m.Description,
            //                                          CreatorName = m2.FirstName + " " + m2.LastName,
            //                                          CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m2.ProfilePhoto,
            //                                          CreatedDate = m.CreatedDate
            //                                      }

            //                                  ).Union(
            //                                  from c in dbContext.Companies
            //                                  join m3 in dbContext.Teams on c.Id equals m3.CompanyId
            //                                  join m2 in dbContext.Projects on m3.Id equals m2.TeamId
            //                                  join m1 in dbContext.Sections on m2.Id equals m1.ProjectId
            //                                  join m in dbContext.ToDos on m1.Id equals m.SectionId

            //                                  where m.CreatedBy == userId || m.AssigneeId == userId
            //                                  select new TodoModel.CalendarEvents
            //                                  {
            //                                      Id = 0,
            //                                      TodoId = m.Id,
            //                                      Title = m.Name,
            //                                      Start = m.DeadlineDate,
            //                                      End = m.DeadlineDate,
            //                                      AllDay = true,
            //                                      Url = "",
            //                                      ClassName = m.IsDone ? "completeTodoInCalendar" : (m.InProgress ? "inProgressTodoInCalendar" : "pendingTodoInCalendar"),
            //                                      Type = "Task",
            //                                      Editable = false,
            //                                      Description = m.Description,
            //                                      CreatorName = "",
            //                                      CreatorProfilePhoto = "",
            //                                      CreatedDate = m.CreatedDate
            //                                  }).ToList();




            List<TodoModel.CalendarEvents> cal = (from clndrevnt in dbContext.CalendarEvents 
                                                  select new TodoModel.CalendarEvents
                                                  {
                                                      _id = clndrevnt.Id,
                                                      Title = clndrevnt.Title,
                                                      Start = clndrevnt.StartDate,
                                                      End = clndrevnt.EndDate,
                                                      AllDay = clndrevnt.AllDay,
                                                      Url = clndrevnt.Url,
                                                  }

                                            ).ToList();




            //List<TodoModel.CalendarEvents> cal = (from cmpny in dbContext.Companies
            //                                      join tm in dbContext.Teams on cmpny.Id equals tm.CompanyId
            //                                      join tmusr in dbContext.TeamUsers on tm.Id equals tmusr.TeamId
            //                                      join clndrevnt in dbContext.CalendarEvents on tm.Id equals clndrevnt.TeamId
            //                                      join usrdet in dbContext.UserDetails on clndrevnt.CreatedBy equals usrdet.UserId
            //                                      //join usrcom in dbContext.UserCompanyRoles on usrdet.UserId equals usrcom.UserId
            //                                      where (CompanyId == 0 ? cmpny.Id == cmpny.Id : cmpny.Id == CompanyId )
            //                                      && (tmusr.UserId == UserId)
            //                                      && (TeamId == 0 ? tm.Id == tm.Id  : tm.Id == TeamId)
            //                                      select new TodoModel.CalendarEvents
            //                                      {
            //                                          _id = clndrevnt.Id,
            //                                          Title = clndrevnt.Title,
            //                                          Start = clndrevnt.StartDate,
            //                                          End = clndrevnt.EndDate,
            //                                          AllDay = clndrevnt.AllDay,
            //                                          Url = clndrevnt.Url,
            //                                      }

            //                                 ).ToList();


            return cal;

        }


        /// <summary>
        /// It fetches details of SIngle Event
        /// </summary>
        /// <param name="CalendarEventId">It is unique id of event</param>
        /// <returns>it returns single event </returns>
        public TodoModel.CalendarEvents GetSingleEvent(int CalendarEventId)
        {
            dbContext = new AppContext();
            TodoModel.CalendarEvents calObject = (from m in dbContext.CalendarEvents
                                                  join m2 in dbContext.UserDetails on m.CreatedBy equals m2.UserId
                                                  where m.Id == CalendarEventId && m2.IsDeleted == false

                                                  select new TodoModel.CalendarEvents
                                                  {
                                                      Id = m.Id,
                                                      Title = m.Title,
                                                      Start = m.StartDate,
                                                      End = m.EndDate,
                                                      AllDay = m.AllDay,
                                                      Url = m.Url,
                                                      Editable = true,
                                                      Description = m.Description,
                                                      CreatorName = m2.FirstName + " " + (string.IsNullOrEmpty( m2.LastName)?"": m2.LastName),
                                                      CreatorProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m2.ProfilePhoto,
                                                      CreatedDate = m.CreatedDate
                                                  }).SingleOrDefault();

            return calObject;

        }


        /// <summary>
        /// It is a Function for Saving Calendar Event for a particular Team
        /// </summary>
        /// <param name="UserId">It is the Unique Userid of logged in User</param>
        /// <param name="objModel">It is the model which consists all the properties of CalendarEvent Table fields being Saved</param>
        public string SaveCalendarEvent(MODEL.CalendarModel.CalendarEvents model)
        {
            string msg = string.Empty;
            dbContext = new AppContext();

            CalendarEvent calEntity = new CalendarEvent
            {
                TeamId = model.TeamId,
                Title = model.Title,
                Description = model.Description,
                CreatedBy = model.UserId,
                AllDay = model.AllDay,
                //StartDate = Convert.ToDateTime(model.eventDateFrom.Add(TimeSpan.Parse(model.eventTimeFrom.ToString("HH:mm")))),     //oldDateTime.Add(TimeSpan.Parse(timeString));
                //EndDate = Convert.ToDateTime(model.eventDateTo.Add(TimeSpan.Parse(model.eventTimeTo.ToString("HH:mm")))),
                StartDate = Convert.ToDateTime(model.eventDateFrom),     //oldDateTime.Add(TimeSpan.Parse(timeString));
                EndDate = Convert.ToDateTime(model.eventDateTo),
                Url = model.Url,
                CreatedDate = DateTime.Now 
                //ModifiedDate = DateTime.Now
            };

            dbContext.CalendarEvents.Add(calEntity);
            dbContext.SaveChanges();
            if (calEntity.Id > 0)
            {
                msg = "New Event Saved";
            }

            string[] TeamMembers = (from m in dbContext.Teams
                                    join m1 in dbContext.TeamUsers on m.Id equals m1.TeamId

                                    where m.Id == model.TeamId && m.IsDeleted == false
                                    select m1.UserId).ToArray();

            for (int j = 0; j < TeamMembers.Count(); j++)
            {
                /* Saving notification for newly added Team for team members */
                Notification objNotification = new Notification
                {

                    Name = "has created a New Event:",
                    CreatedBy = model.UserId,
                    CreatedDate = DateTime.Now,
                    IsRead = false,
                    Type = 6,
                    NotifyingUserId = TeamMembers[j],
                    CalendarEventId = calEntity.Id
                };

                dbContext.Notifications.Add(objNotification);
                dbContext.SaveChanges();

            }

            return msg;
        }



        /// <summary>
        /// It is a Function for Editing Calendar Event for a particular Team
        /// </summary>
        /// <param name="UserId">It is the Unique Userid of logged in User for updating ModifiedBy Field</param>
        /// <param name="objModel">It is the model which consists all the properties of CalendarEvent Table fields being Updated</param>

        public void EditCalendarEvent(TodoModel.CalendarEvents objModel)
        {
            dbContext = new AppContext();

            CalendarEvent calEntity = (from m in dbContext.CalendarEvents where m.Id == objModel.Id select m).SingleOrDefault();

            calEntity.ModifiedBy = objModel.UserId;
            calEntity.ModifiedDate = DateTime.Now;
            //calEntity.StartDate = Convert.ToDateTime(objModel.Start);
            //calEntity.EndDate = objModel.End;
            calEntity.AllDay = objModel.AllDay;
            calEntity.Title = objModel.Title;
            calEntity.Description = objModel.Description;
            calEntity.Url = objModel.Url;
            calEntity.StartDate = objModel.eventDateFrom;
            calEntity.EndDate = objModel.eventDateTo;

            dbContext.SaveChanges();

        }


        /// <summary>
        /// It is a Function for Editing Calendar Event for a particular Team
        /// </summary>
        /// <param name="UserId">It is the Unique Userid of logged in User for updating ModifiedBy Field</param>
        /// <param name="objModel">It is the model which consists all the properties of CalendarEvent Table fields being Updated</param>
        public void UpdateOnDrop(MODEL.CalendarModel.UpdateEventOnDropOrResize model)
        {
            dbContext = new AppContext();

            CalendarEvent calEntity = (from m in dbContext.CalendarEvents where m.Id == model.EventId select m).SingleOrDefault();

            calEntity.ModifiedBy = model.UserId;
            calEntity.ModifiedDate = DateTime.Now;
            calEntity.StartDate = Convert.ToDateTime(model.eventDateFrom);

            dbContext.SaveChanges();
        }


        /// <summary>
        /// It is a Function for Editing Calendar Event for a particular Team
        /// </summary>
        /// <param name="UserId">It is the Unique Userid of logged in User for updating ModifiedBy Field</param>
        /// <param name="objModel">It is the model which consists all the properties of CalendarEvent Table fields being Updated</param>
        public void UpdateOnResize(MODEL.CalendarModel.UpdateEventOnDropOrResize model)
        {
            dbContext = new AppContext();

            CalendarEvent calEntity = (from m in dbContext.CalendarEvents where m.Id == model.EventId select m).SingleOrDefault();

            calEntity.ModifiedBy = model.UserId;
            calEntity.ModifiedDate = DateTime.Now;
            calEntity.StartDate = Convert.ToDateTime(model.eventDateFrom);
            calEntity.EndDate = Convert.ToDateTime(model.eventDateTo);

            dbContext.SaveChanges();
        }


        /// <summary>
        /// It is the Function for Deleting a particular Calendar Event
        /// </summary>
        /// <param name="CalendarEventId"> It is the unique Id of CalendarEvent which will be deleted</param>
        public void DeleteCalendarEvent(int CalendarEventId)
        {
            dbContext = new AppContext();
            CalendarEvent calEntity = (from m in dbContext.CalendarEvents where m.Id == CalendarEventId select m).SingleOrDefault();
            dbContext.CalendarEvents.Remove(calEntity);
            dbContext.SaveChanges();
        }
    }
}
