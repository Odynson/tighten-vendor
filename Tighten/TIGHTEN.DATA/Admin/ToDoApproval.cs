﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;

namespace TIGHTEN.DATA
{
    public class ToDoApproval : Common
    {

        ///// <summary>
        ///// This function fetches my Todos for MyTOdos Tab
        ///// </summary>
        ///// <param name="UserId">It is unique id of logged in user</param>
        ///// <returns>returns todos in list</returns>
        ///// 
        //public List<TodoModel.TodosData> getTodos(TodoApprovalSearch search)
        //{
        //    search.From = (search.From == DateTime.MinValue ? DateTime.Now.AddDays(-7) : search.From);
        //    search.To = (search.To == DateTime.MinValue ? DateTime.Now : search.To);
        //    //Query to search based on different columns 
        //    //from o in context.Orders.include("customers")
        //    //where o.city == (city == null ? o.city : city) && o.firstname == (firstname == null ? o.firstname : firstname)
        //    //select o;
        //    List<TodoModel.TodosData> Todos = (from c in dbcontext.ToDos
        //                                       join c3 in dbcontext.UserDetails on c.AssigneeId equals c3.UserId
        //                                       where c3.CompanyId == search.CompanyId //&& c.IsDone == true//
        //                                       && c.AssigneeId == (string.IsNullOrEmpty(search.AssigneeId) ? c.AssigneeId : search.AssigneeId)
        //                                       && (c.ModifiedDate >= search.From.Date
        //                                       && c.ModifiedDate <= search.To.Date)


        //                                       orderby c.ModifiedDate descending

        //                                       let AssigneeProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
        //                                       let AssigneeName = c3.FirstName //+' ' +( string.IsNullOrEmpty( c2.LastName)?"": c2.LastName)

        //                                       select new TodoModel.TodosData
        //                                       {
        //                                           Name = c.Name,
        //                                           SectionId = c.SectionId,
        //                                           TodoId = c.Id,
        //                                           AssigneeId = c.AssigneeId,
        //                                           AssigneeProfilePhoto = AssigneeProfilePhoto,
        //                                           AssigneeName = AssigneeName,
        //                                           AssigneeEmail = c3.Email,
        //                                           //ReporterId = c3.UserId,
        //                                           //ReporterEmail = c3.Email,
        //                                           //ReporterName = ReporterName,
        //                                           //ReporterProfilePhoto = ReporterProfilePhoto,
        //                                           Description = c.Description,
        //                                           DeadlineDate = c.DeadlineDate,
        //                                           InProgress = c.InProgress,
        //                                           IsDone = c.IsDone,
        //                                           //SectionName = t1.SectionName,
        //                                           //TeamId = t0.Id,
        //                                           //TeamName = t0.TeamName,
        //                                           //ProjectId = t.Id,
        //                                           //ProjectName = t.Name,
        //                                           CreatedDate = c.CreatedDate,
        //                                           ModifiedDate = c.ModifiedDate,
        //                                           LoggedTime = c.LoggedTime,
        //                                           EstimatedTime = c.EstimatedTime,
        //                                           IsApproved = c.IsApproved,
        //                                           ApprovedBy = c.ApprovedBy,
        //                                           Reason = c.Reason,
        //                                           TodoType = (c.TaskType == 5 ? "PMS Task" : c.TaskType == 4 ? "Admin Duties" : c.TaskType == 3 ? "Meeting" : c.TaskType == 2 ? "Research & Development" : "phone Calls")
        //                                       }).ToList();

        //    return Todos;
        //}

        /// <summary>
        /// This function fetches my Todos for MyTOdos Tab
        /// </summary>
        /// <param name="UserId">It is unique id of logged in user</param>
        /// <returns>returns todos in list</returns>
        /// 
        public List<TodoApproval> getTodos(TodoApprovalSearch search)
        {
            List<TodoApproval> ListForTodos = new List<TodoApproval>();

            search.From = (search.From == DateTime.MinValue ? DateTime.Now.AddDays(-7) : search.From);
            search.To = (search.To == DateTime.MinValue ? DateTime.Now : search.To);
            //Query to search based on different columns 
            //from o in context.Orders.include("customers")
            //where o.city == (city == null ? o.city : city) && o.firstname == (firstname == null ? o.firstname : firstname)
            //select o;

            if (search.RoleId == 2)
            {
                var AllProjects = (from prjctusr in dbcontext.ProjectUsers
                                   where prjctusr.UserId == search.UserId
                                   select prjctusr.ProjectId).ToList();

                var TodosList = (from  MyData in
                                (from c in dbcontext.ToDos.AsEnumerable()
                                 join c3 in dbcontext.UserDetails on c.AssigneeId equals c3.UserId
                                 join sec in dbcontext.Sections on c.SectionId equals sec.Id
                                 join prjct in dbcontext.Projects on sec.ProjectId equals prjct.Id
                                 join prjctUsr in dbcontext.ProjectUsers on prjct.Id equals prjctUsr.ProjectId
                                 join t in dbcontext.TodoTimeLogs on c.Id equals t.TodoId

                                 where prjct.CompanyId == search.CompanyId && prjct.IsDeleted == false
                                 && AllProjects.Contains(prjct.Id)
                                 && c.AssigneeId == (string.IsNullOrEmpty(search.AssigneeId) ? c.AssigneeId : search.AssigneeId)
                                 && (c.ModifiedDate.Date >= search.From.Date
                                 && c.ModifiedDate.Date <= search.To.Date)
                                 && c.IsDone == true
                                 && c.AssigneeId != c.ReporterId
                                 && c3.IsDeleted == false && c.IsDeleted == false
                                 //&& t.Duration > 0
                                 select new {c,c3 }
                                 )
                                 
                                 group MyData by new { MyData.c.Name, MyData.c.SectionId ,
                                                       MyData.c.Id,MyData.c.AssigneeId,MyData.c3.IsFreelancer,MyData.c3.Email,MyData.c.Description,MyData.c.DeadlineDate,MyData.c.InProgress,
                                                       MyData.c.IsDone,MyData.c.CreatedBy,MyData.c.CreatedDate,MyData.c.ModifiedDate,MyData.c.LoggedTime,MyData.c.EstimatedTime,
                                                       MyData.c.IsApproved,MyData.c.ApprovedBy,MyData.c.Reason,MyData.c.TaskType,MyData.c3.ProfilePhoto,MyData.c3.FirstName
                                 } into GroupedData

                                 orderby GroupedData.Key.ModifiedDate descending

                                 let AssigneeProfilePhoto = GroupedData.Key.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + GroupedData.Key.ProfilePhoto
                                 let AssigneeName = GroupedData.Key.FirstName //+' ' +( string.IsNullOrEmpty( c2.LastName)?"": c2.LastName)

                                 select new
                                 {
                                     Name = GroupedData.Key.Name,
                                     SectionId = GroupedData.Key.SectionId,
                                     TodoId = GroupedData.Key.Id,
                                     AssigneeId = GroupedData.Key.AssigneeId,
                                     AssigneeProfilePhoto = AssigneeProfilePhoto,
                                     AssigneeName = AssigneeName,
                                     AssigneeEmail = GroupedData.Key.Email,
                                     Description = GroupedData.Key.Description,
                                     DeadlineDate = GroupedData.Key.DeadlineDate,
                                     InProgress = GroupedData.Key.InProgress,
                                     IsDone = GroupedData.Key.IsDone,
                                     CreatedBy = GroupedData.Key.CreatedBy,
                                     CreatedDate = GroupedData.Key.CreatedDate,
                                     ModifiedDate = GroupedData.Key.ModifiedDate,
                                     LoggedTime = GroupedData.Key.LoggedTime,
                                     EstimatedTime = GroupedData.Key.EstimatedTime,
                                     IsApproved = GroupedData.Key.IsApproved,
                                     ApprovedBy = GroupedData.Key.ApprovedBy,
                                     Reason = GroupedData.Key.Reason,
                                     IsFreelancer = GroupedData.Key.IsFreelancer,
                                     TodoType = (GroupedData.Key.TaskType == 5 ? "PMS Task" : GroupedData.Key.TaskType == 4 ? "Admin Duties" : GroupedData.Key.TaskType == 3 ? "Meeting" : GroupedData.Key.TaskType == 2 ? "Research & Development" : "phone Calls"),
                                     TodoDetails = (from tl in dbcontext.TodoTimeLogs
                                                    where tl.TodoId == GroupedData.Key.Id
                                                    select new TodoDetail
                                                    {
                                                        Id = tl.Id,
                                                        TodoId = tl.TodoId,
                                                        IsAutomaticLogged = tl.IsAutomaticLogged,
                                                        Duration = tl.Duration,
                                                        loggingTime = tl.loggingTime,
                                                        StartDateTime = tl.StartDateTime,
                                                        StopDateTime = tl.StopDateTime,
                                                    })
                                 });



                List<TodoApproval> Todos = TodosList.AsEnumerable().Select(item =>
                                                              new TodoApproval()
                                                              {
                                                                  Name = item.Name,
                                                                  SectionId = item.SectionId,
                                                                  TodoId = item.TodoId,
                                                                  AssigneeId = item.AssigneeId,
                                                                  AssigneeProfilePhoto = item.AssigneeProfilePhoto,
                                                                  AssigneeName = item.AssigneeName,
                                                                  AssigneeEmail = item.AssigneeEmail,
                                                                  Description = item.Description,
                                                                  DeadlineDate = item.DeadlineDate,
                                                                  InProgress = item.InProgress,
                                                                  IsDone = item.IsDone,
                                                                  CreatedBy = item.CreatedBy,
                                                                  CreatedDate = item.CreatedDate,
                                                                  ModifiedDate = item.ModifiedDate,
                                                                  LoggedTime = item.LoggedTime,
                                                                  EstimatedTime = item.EstimatedTime,
                                                                  IsApproved = item.IsApproved,
                                                                  ApprovedBy = item.ApprovedBy,
                                                                  Reason = item.Reason,
                                                                  IsFreelancer = item.IsFreelancer,
                                                                  TodoType = item.TodoType,
                                                                  TodoDetails = item.TodoDetails.ToList()
                                                              }).ToList();

                ListForTodos = Todos;
            }
            else
            {
               var TodosList = (from MyData in
                                (from c in dbcontext.ToDos.AsEnumerable()
                                 join sec in dbcontext.Sections on c.SectionId equals sec.Id
                                 join p in dbcontext.Projects on sec.ProjectId equals p.Id
                                 join pu in dbcontext.ProjectUsers on c.AssigneeId equals pu.UserId
                                 join c3 in dbcontext.UserDetails on pu.UserId equals c3.UserId
                                 join t in dbcontext.TodoTimeLogs on c.Id equals t.TodoId

                                 //join c3 in dbcontext.UserDetails on c.AssigneeId equals c3.UserId
                                 where p.CompanyId == search.CompanyId && p.IsDeleted == false   //&& c.IsDone == true//
                                 //where c3.CompanyId == search.CompanyId //&& c.IsDone == true//
                                && c.AssigneeId == (string.IsNullOrEmpty(search.AssigneeId) ? c.AssigneeId : search.AssigneeId)
                                 && (c.ModifiedDate.Date >= search.From.Date
                                 && c.ModifiedDate.Date <= search.To.Date)
                                 && c.IsDone == true
                                 && c.AssigneeId != c.ReporterId
                                 && c3.IsDeleted == false && c.IsDeleted == false
                                 //&& t.Duration > 0

                                 select new { c, c3 }
                                 )

                                group MyData by new
                                {
                                    MyData.c.Name,MyData.c.SectionId,MyData.c.Id,MyData.c.AssigneeId,MyData.c3.Email,MyData.c.Description,
                                    MyData.c.DeadlineDate,MyData.c.InProgress,MyData.c.IsDone,MyData.c.CreatedBy,MyData.c.CreatedDate,MyData.c.ModifiedDate,
                                    MyData.c.LoggedTime,MyData.c.EstimatedTime,MyData.c.IsApproved,MyData.c.ApprovedBy,MyData.c.Reason,
                                    MyData.c.TaskType,MyData.c3.ProfilePhoto,MyData.c3.FirstName,MyData.c3.IsFreelancer
                                } into GroupedData

                                orderby GroupedData.Key.ModifiedDate descending

                                 let AssigneeProfilePhoto = GroupedData.Key.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + GroupedData.Key.ProfilePhoto
                                 let AssigneeName = GroupedData.Key.FirstName //+' ' +( string.IsNullOrEmpty( c2.LastName)?"": c2.LastName)

                                 select new
                                 {
                                     Name = GroupedData.Key.Name,
                                     SectionId = GroupedData.Key.SectionId,
                                     TodoId = GroupedData.Key.Id,
                                     AssigneeId = GroupedData.Key.AssigneeId,
                                     AssigneeProfilePhoto = AssigneeProfilePhoto,
                                     AssigneeName = AssigneeName,
                                     AssigneeEmail = GroupedData.Key.Email,
                                     Description = GroupedData.Key.Description,
                                     DeadlineDate = GroupedData.Key.DeadlineDate,
                                     InProgress = GroupedData.Key.InProgress,
                                     IsDone = GroupedData.Key.IsDone,
                                     CreatedBy = GroupedData.Key.CreatedBy,
                                     CreatedDate = GroupedData.Key.CreatedDate,
                                     ModifiedDate = GroupedData.Key.ModifiedDate,
                                     LoggedTime = GroupedData.Key.LoggedTime,
                                     EstimatedTime = GroupedData.Key.EstimatedTime,
                                     IsApproved = GroupedData.Key.IsApproved,
                                     ApprovedBy = GroupedData.Key.ApprovedBy,
                                     Reason = GroupedData.Key.Reason,
                                     IsFreelancer = GroupedData.Key.IsFreelancer,
                                     TodoType = (GroupedData.Key.TaskType == 5 ? "PMS Task" : GroupedData.Key.TaskType == 4 ? "Admin Duties" : GroupedData.Key.TaskType == 3 ? "Meeting" : GroupedData.Key.TaskType == 2 ? "Research & Development" : "phone Calls"),
                                     TodoDetails = (from tl in dbcontext.TodoTimeLogs
                                                    where tl.TodoId == GroupedData.Key.Id
                                                    select new TodoDetail
                                                    {
                                                        Id = tl.Id,
                                                        TodoId = tl.TodoId,
                                                        IsAutomaticLogged = tl.IsAutomaticLogged,
                                                        Duration = tl.Duration,
                                                        loggingTime = tl.loggingTime,
                                                        StartDateTime = tl.StartDateTime,
                                                        StopDateTime = tl.StopDateTime,
                                                    })
                                 });



                List<TodoApproval> Todos = TodosList.AsEnumerable().Select(item =>
                                                              new TodoApproval()
                                                              {
                                                                  Name = item.Name,
                                                                  SectionId = item.SectionId,
                                                                  TodoId = item.TodoId,
                                                                  AssigneeId = item.AssigneeId,
                                                                  AssigneeProfilePhoto = item.AssigneeProfilePhoto,
                                                                  AssigneeName = item.AssigneeName,
                                                                  AssigneeEmail = item.AssigneeEmail,
                                                                  Description = item.Description,
                                                                  DeadlineDate = item.DeadlineDate,
                                                                  InProgress = item.InProgress,
                                                                  IsDone = item.IsDone,
                                                                  CreatedBy = item.CreatedBy,
                                                                  CreatedDate = item.CreatedDate,
                                                                  ModifiedDate = item.ModifiedDate,
                                                                  LoggedTime = item.LoggedTime,
                                                                  EstimatedTime = item.EstimatedTime,
                                                                  IsApproved = item.IsApproved,
                                                                  ApprovedBy = item.ApprovedBy,
                                                                  Reason = item.Reason,
                                                                  IsFreelancer = item.IsFreelancer,
                                                                  TodoType = item.TodoType,
                                                                  TodoDetails = item.TodoDetails.ToList()
                                                              }).ToList();

                ListForTodos = Todos;
            }

            return ListForTodos;
        }

        public int ApproveTodo(TodoApprovalUpdate approval)
        {
            var todo = dbcontext.ToDos.Where(x => x.Id == approval.TodoId && x.IsDeleted == false).SingleOrDefault();
            todo.IsApproved = approval.IsApproved;
            todo.ApprovedBy = approval.ApproverId;
            if (approval.IsApproved == false)
            {
                todo.Reason = approval.Reason;
            }
            dbcontext.SaveChanges();
            return todo.Id;
        }

        /// <summary>
        ///Return the list of the user of the particular company 
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        /// 
        public List<CompanyUserResult> CompanyUsers(int CompanyId, int RoleId, string UserId)
        {
            List<CompanyUserResult> list = new List<CompanyUserResult>();

            if (RoleId == 4)
            {

                var AllProjectsOfLoggedinUser = (from prjctuser in dbcontext.ProjectUsers
                                                 where prjctuser.UserId == UserId
                                                 select prjctuser.ProjectId).ToList();


                List<CompanyUserResult> UserListForProjectManager = (from prjctusr in dbcontext.ProjectUsers
                                                                        join usrdetail in dbcontext.UserDetails on prjctusr.UserId equals usrdetail.UserId
                                                                        where AllProjectsOfLoggedinUser.Contains(prjctusr.ProjectId)
                                                                        && usrdetail.IsDeleted == false
                                                                        group usrdetail by new { usrdetail.UserId, usrdetail.FirstName, usrdetail.LastName, usrdetail.Email } into GroupedUserDetail

                                                                        select new CompanyUserResult
                                                                        {
                                                                            Name = GroupedUserDetail.Key.FirstName + " " + (GroupedUserDetail.Key.LastName == null ? "" : GroupedUserDetail.Key.LastName) + "( " + GroupedUserDetail.Key.Email + ")",
                                                                            Value = GroupedUserDetail.Key.UserId
                                                                     }).ToList();


                //UserListForProjectManager.Insert(0, new CompanyUserResult() { Name = "All", Value = "" });
                list = UserListForProjectManager;

            }
            else
            {
                List<CompanyUserResult> CompanyUserList = (from user in dbcontext.UserDetails
                                                           join usrcom in dbcontext.UserCompanyRoles on user.UserId equals usrcom.UserId
                                                           where usrcom.CompanyId == CompanyId
                                                           && user.EmailConfirmed == true
                                                           && user.IsDeleted == false
                                                           select new CompanyUserResult
                                                           {
                                                               Name = user.FirstName + " " + (user.LastName == null ? "" : user.LastName) + "( " + user.Email + ")",
                                                               Value = user.UserId
                                                           }).ToList();

                //CompanyUserList.Insert(0, new CompanyUserResult() { Name = "All", Value = "" });
                list = CompanyUserList;

            }
            return list;
        }

        public int DeleteUserLoggedTimeSlot(int TodoDetailId)
        {
            TodoTimeLogs TodoTimeLog = (from e in dbcontext.TodoTimeLogs
                                        where e.Id == TodoDetailId
                                        select e).SingleOrDefault();

            /*  Delete ToDo  */
            dbcontext.TodoTimeLogs.Remove(TodoTimeLog);
            dbcontext.SaveChanges();
            return TodoDetailId;
        }

    }
}
