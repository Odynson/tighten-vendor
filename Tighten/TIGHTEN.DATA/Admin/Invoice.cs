﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using System.Data.Entity;
using System.Threading;
using System.Web;
using System.Xml.Linq;
using Stripe;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using TIGHTEN.MODEL.Vendor;

namespace TIGHTEN.DATA
{
    public class Invoice : BaseClass
    {
        AppContext dbcontext;

        public MODEL.UserInvoice.InvoiceModelBothApprovedandNotApproved GetInvoice(MODEL.UserInvoice.InvoiceQuery model)
        {
            MODEL.UserInvoice.InvoiceModelBothApprovedandNotApproved ReturnModel = new MODEL.UserInvoice.InvoiceModelBothApprovedandNotApproved();

            //List<MODEL.UserInvoice.InvoiceModel> invoiceList = ((from allinvoices in dbcontext.Invoices
            //                                                     join User_Detail in dbcontext.UserDetails on allinvoices.UserId equals User_Detail.UserId
            //                                                     where User_Detail.IsDeleted == false
            //                                                     && allinvoices.CompanyId == model.CompanyId
            //                                                     && DbFunctions.TruncateTime(allinvoices.InvoiceDate) >= model.FromDate
            //                                                     && DbFunctions.TruncateTime(allinvoices.InvoiceDate) <= model.ToDate
            //                                                     && (model.RoleId == 1 || model.RoleId == 5 ? allinvoices.SendTo == allinvoices.SendTo : allinvoices.SendTo == model.UserId)
            //                                                     //&& allinvoices.SendTo == model.UserId
            //                                                     && allinvoices.InvoiceApprovedStatus == (model.InvoiceStatus == 1 ? true : model.InvoiceStatus == 2 ? false : allinvoices.InvoiceApprovedStatus)
            //                                                     && User_Detail.UserId == (model.FreelancerUserId == null || model.FreelancerUserId == "0" ? User_Detail.UserId : model.FreelancerUserId)
            //                                                     orderby allinvoices.InvoiceDate
            //                                                     select new MODEL.UserInvoice.InvoiceModel
            //                                                     {
            //                                                         Id = allinvoices.Id,
            //                                                         InvoiceNumber = allinvoices.InvoiceNumber,
            //                                                         UserId = allinvoices.UserId,
            //                                                         ApprovedBy = (from u in dbcontext.UserDetails
            //                                                                       where u.UserId == allinvoices.ApprovedBy
            //                                                                       && u.IsDeleted == false
            //                                                                       select u.FirstName
            //                                                                      ).FirstOrDefault(),
            //                                                         FromDate = allinvoices.FromDate,
            //                                                         ToDate = allinvoices.ToDate,
            //                                                         SendTo = allinvoices.SendTo,
            //                                                         Message = allinvoices.Message,
            //                                                         Rate = User_Detail.Rate,
            //                                                         InvoiceDate = allinvoices.InvoiceDate,
            //                                                         TotalHours = allinvoices.TotalHours,
            //                                                         TotalAmount = allinvoices.TotalAmount,
            //                                                         InvoiceApprovedStatus = allinvoices.InvoiceApprovedStatus,
            //                                                         UserName = User_Detail.FirstName,
            //                                                         ProfilePic = User_Detail.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + User_Detail.ProfilePhoto,
            //                                                         IsPaid = allinvoices.IsPaid,
            //                                                         PaidBy = allinvoices.PaidBy,
            //                                                         IsRejected = allinvoices.IsRejected,
            //                                                         RejectedBy = allinvoices.RejectedBy,
            //                                                         RejectReason = allinvoices.RejectReason,
            //                                                         PaidByUserDetail = (from u in dbcontext.UserDetails
            //                                                                             where u.UserId == allinvoices.PaidBy
            //                                                                             select new MODEL.UserInvoice.UserDetailForInvoicePaidAndReject
            //                                                                             {
            //                                                                                 UserId = u.UserId,
            //                                                                                 UserName = u.FirstName,
            //                                                                                 UserProfilePic = u.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + u.ProfilePhoto
            //                                                                             }).FirstOrDefault(),

            //                                                         RejectedByUserDetail = (from u in dbcontext.UserDetails
            //                                                                                 where u.UserId == allinvoices.RejectedBy
            //                                                                                 select new MODEL.UserInvoice.UserDetailForInvoicePaidAndReject
            //                                                                                 {
            //                                                                                     UserId = u.UserId,
            //                                                                                     UserName = u.FirstName,
            //                                                                                     UserProfilePic = u.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + u.ProfilePhoto
            //                                                                                 }).FirstOrDefault()
            //                                                     })
            //                                                    .Union
            //                                                    (from allinvoices in dbcontext.Invoices
            //                                                     join User_Detail in dbcontext.UserDetails on allinvoices.UserId equals User_Detail.UserId
            //                                                     join frwdinv in dbcontext.InvoiceForwards on allinvoices.Id equals frwdinv.InvoiceId
            //                                                     where User_Detail.IsDeleted == false
            //                                                     && allinvoices.CompanyId == model.CompanyId
            //                                                     && DbFunctions.TruncateTime(allinvoices.InvoiceDate) >= model.FromDate
            //                                                     && DbFunctions.TruncateTime(allinvoices.InvoiceDate) <= model.ToDate
            //                                                     //&& (model.RoleId == 1 || model.RoleId == 5 ? allinvoices.SendTo == allinvoices.SendTo : allinvoices.SendTo == model.UserId)
            //                                                     //&& allinvoices.SendTo == model.UserId
            //                                                     && allinvoices.InvoiceApprovedStatus == (model.InvoiceStatus == 1 ? true : model.InvoiceStatus == 2 ? false : allinvoices.InvoiceApprovedStatus)
            //                                                     && User_Detail.UserId == (model.FreelancerUserId == null || model.FreelancerUserId == "0" ? User_Detail.UserId : model.FreelancerUserId)
            //                                                     && frwdinv.IsCurrentlyActive == true
            //                                                     orderby allinvoices.InvoiceDate

            //                                                     select new MODEL.UserInvoice.InvoiceModel
            //                                                     {
            //                                                         Id = allinvoices.Id,
            //                                                         InvoiceNumber = allinvoices.InvoiceNumber,
            //                                                         UserId = allinvoices.UserId,
            //                                                         ApprovedBy = (from u in dbcontext.UserDetails
            //                                                                       where u.UserId == allinvoices.ApprovedBy
            //                                                                       && u.IsDeleted == false
            //                                                                       select u.FirstName
            //                                                                     ).FirstOrDefault(),
            //                                                         FromDate = allinvoices.FromDate,
            //                                                         ToDate = allinvoices.ToDate,
            //                                                         SendTo = allinvoices.SendTo,
            //                                                         Message = allinvoices.Message,
            //                                                         Rate = User_Detail.Rate,
            //                                                         InvoiceDate = allinvoices.InvoiceDate,
            //                                                         TotalHours = allinvoices.TotalHours,
            //                                                         TotalAmount = allinvoices.TotalAmount,
            //                                                         InvoiceApprovedStatus = allinvoices.InvoiceApprovedStatus,
            //                                                         UserName = User_Detail.FirstName,
            //                                                         ProfilePic = User_Detail.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + User_Detail.ProfilePhoto,
            //                                                         IsPaid = allinvoices.IsPaid,
            //                                                         PaidBy = allinvoices.PaidBy,
            //                                                         IsRejected = allinvoices.IsRejected,
            //                                                         RejectedBy = allinvoices.RejectedBy,
            //                                                         RejectReason = allinvoices.RejectReason,
            //                                                         PaidByUserDetail = (from u in dbcontext.UserDetails
            //                                                                             where u.UserId == allinvoices.PaidBy
            //                                                                             select new MODEL.UserInvoice.UserDetailForInvoicePaidAndReject
            //                                                                             {
            //                                                                                 UserId = u.UserId,
            //                                                                                 UserName = u.FirstName,
            //                                                                                 UserProfilePic = u.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + u.ProfilePhoto
            //                                                                             }).FirstOrDefault(),

            //                                                         RejectedByUserDetail = (from u in dbcontext.UserDetails
            //                                                                                 where u.UserId == allinvoices.RejectedBy
            //                                                                                 select new MODEL.UserInvoice.UserDetailForInvoicePaidAndReject
            //                                                                                 {
            //                                                                                     UserId = u.UserId,
            //                                                                                     UserName = u.FirstName,
            //                                                                                     UserProfilePic = u.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + u.ProfilePhoto
            //                                                                                 }).FirstOrDefault()

            //                                                     }))
            //                                                    .ToList();



            List<MODEL.UserInvoice.InvoiceModel> invoiceList;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetAllInvoice", new { CompanyId = model.CompanyId, FromDate = model.FromDate, ToDate = model.ToDate, RoleId = model.RoleId, UserId = model.UserId, FreelancerUserId = model.FreelancerUserId, InvoiceStatus = model.InvoiceStatus }, commandType: CommandType.StoredProcedure);
                invoiceList = result.Read<MODEL.UserInvoice.InvoiceModel>().ToList();
                if (invoiceList != null)
                {
                    var users = result.Read<MODEL.UserInvoice.UserDetailForInvoicePaidAndReject>().ToList();

                    foreach (var item in invoiceList)
                    {
                        item.PaidByUserDetail = users.Where(x => x.UserId == item.PaidBy).FirstOrDefault();
                        item.RejectedByUserDetail = users.Where(x => x.UserId == item.RejectedBy).FirstOrDefault();
                    }
                }
            }


            //    List<MODEL.UserInvoice.InvoiceModel> list_Paid = new List<MODEL.UserInvoice.InvoiceModel>();
            //List<MODEL.UserInvoice.InvoiceModel> list_NotPaid = new List<MODEL.UserInvoice.InvoiceModel>();
            //List<MODEL.UserInvoice.InvoiceModel> list_Rejected = new List<MODEL.UserInvoice.InvoiceModel>();

            //foreach (var item in invoiceList)
            //{
            //    if (item.IsPaid == true)
            //    {
            //        list_Paid.Add(item);
            //    }
            //    else if (item.IsPaid == false && item.IsRejected == false)
            //    {
            //        list_NotPaid.Add(item);
            //    }
            //    else if (item.IsRejected == true)
            //    {
            //        list_Rejected.Add(item);
            //    }
            //}


            ReturnModel.list_Paid = invoiceList.Where(x => x.IsPaid == true).ToList();
            ReturnModel.list_NotPaid = invoiceList.Where(x => x.IsPaid == false && x.IsRejected == false).ToList();
            ReturnModel.list_Rejected = invoiceList.Where(x => x.IsRejected == true).ToList();

            return ReturnModel;

        }

        public MODEL.ReportersForTodoesModel.TodosForPdf_List GetInvoiceDetail(MODEL.UserInvoice.AdminInvoiceDetailsRequiredParams model, string DefaultInvoiceNumber)
        {
            MODEL.ReportersForTodoesModel.TodosForPdf_List ReturnModel = new MODEL.ReportersForTodoesModel.TodosForPdf_List();

            //var invoices = (from inv in dbcontext.Invoices
            //                where inv.Id == model.InvoiceId
            //                select inv).SingleOrDefault();

            TIGHTEN.ENTITY.Invoice invoices;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from Invoices inv where inv.Id = @InvoiceId";
                invoices = con.Query<TIGHTEN.ENTITY.Invoice>(sqlquery, new { InvoiceId = model.InvoiceId }).SingleOrDefault();
            }


            if (invoices != null)
            {
                //var FromName = (from ussr in dbcontext.UserDetails
                //                where ussr.UserId == invoices.UserId
                //                && ussr.IsDeleted == false
                //                select ussr.FirstName).SingleOrDefault();

                //var ToName = (from ussr in dbcontext.UserDetails
                //              where ussr.UserId == invoices.SendTo
                //              && ussr.IsDeleted == false
                //              select ussr.FirstName).SingleOrDefault();


                //var CompanyProfilePic = (from usr in dbcontext.Companies
                //                         where usr.Id == model.CompanyId
                //                         select new
                //                         {
                //                             CompanyUserId = usr.Id,
                //                             conpanyprofilepic = usr.Logo == null ? "Uploads/Default/nologo.png" : "Uploads/CompanyLogos/" + usr.Logo,
                //                             CompanyName = usr.Name
                //                         }).SingleOrDefault();

                InvoiceDetailReturnModel obj;
                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = @"select usr.Id as CompanyUserId,case when usr.Logo is null then 'Uploads/Default/nologo.png' else 'Uploads/CompanyLogos/'+ usr.Logo end as conpanyprofilepic,usr.Name as CompanyName ,
	                                    (select FirstName from UserDetails where UserId = @UserId and IsDeleted = 0)  as FromName,(select FirstName from UserDetails where UserId = @SendToUserId and IsDeleted = 0)  as ToName
	                                    from Companies usr where usr.Id = @CompanyId ";

                    obj = con.Query<InvoiceDetailReturnModel>(sqlquery, new { UserId = invoices.UserId, CompanyId = model.CompanyId, SendToUserId = invoices.SendTo }).SingleOrDefault();
                }


                ReturnModel.InvoiceNumber = invoices.InvoiceNumber;
                ReturnModel.IsRejected = invoices.IsRejected;
                ReturnModel.RejectReason = invoices.RejectReason;
                ReturnModel.FromName = obj.FromName;
                ReturnModel.ToName = obj.ToName;
                ReturnModel.CompanyProfilePic = obj.conpanyprofilepic;
                ReturnModel.CompanyUserId = obj.CompanyUserId.ToString();
                ReturnModel.CompanyName = obj.CompanyName;
                ReturnModel.DateIssued = invoices.InvoiceDate;
                ReturnModel.Message = invoices.Message;

            }

            //List<MODEL.ReportersForTodoesModel.TodosForPreview_List> list = (from invcdetail in dbcontext.InvoiceDetails
            //                                                                 join todoss in dbcontext.ToDos on invcdetail.TodoId equals todoss.Id
            //                                                                 join u in dbcontext.UserDetails on todoss.AssigneeId equals u.UserId
            //                                                                 join s in dbcontext.Sections on todoss.SectionId equals s.Id
            //                                                                 join p in dbcontext.Projects on s.ProjectId equals p.Id
            //                                                                 where invcdetail.InvoiceId == model.InvoiceId
            //                                                                 && todoss.IsDeleted == false
            //                                                                 && u.IsDeleted == false && p.IsDeleted == false
            //                                                                 select new MODEL.ReportersForTodoesModel.TodosForPreview_List
            //                                                                 {
            //                                                                     ProjectName = p.Name,
            //                                                                     TodoName = invcdetail.TodoName,
            //                                                                     TodoDescription = invcdetail.TodoDescription,
            //                                                                     Rate = u.Rate,
            //                                                                     hours = invcdetail.hours,
            //                                                                     Amount = invcdetail.Amount,
            //                                                                     FromDate = invcdetail.FromDate,
            //                                                                     ToDate = invcdetail.ToDate,
            //                                                                     TodoType = invcdetail.TodoType,
            //                                                                     TaskType = (invcdetail.TodoType == 1 ? "Phone Call" : invcdetail.TodoType == 2 ? " Research & Development " : invcdetail.TodoType == 3 ? "Meeting" : invcdetail.TodoType == 4 ? " Admin Duties " : "PMS Task")

            //                                                                 }).ToList();

            List<MODEL.ReportersForTodoesModel.TodosForPreview_List> list;
            using (var con = new SqlConnection(ConnectionString))
            {
                list = con.Query<MODEL.ReportersForTodoesModel.TodosForPreview_List>("usp_GetInvoiceDetail", new { InvoiceId = model.InvoiceId }, commandType: CommandType.StoredProcedure).ToList();
            }


            // This need to be changed after some time ,  this is a temporary fix
            // Here we have to find some other method to find Total Hours 

            #region Changable Data

            int TotalMinutesForTodos = 0;
            decimal rate = 0;

            foreach (var item in list)
            {
                int LoggedTimeInMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(item.hours);

                TotalMinutesForTodos += LoggedTimeInMinutes;
                rate = item.Rate;
            }


            decimal totalamt = 0;
            int TotalHours = (TotalMinutesForTodos / 60);
            totalamt = (TotalHours * rate);

            int TotalMinutes = (TotalMinutesForTodos % 60);
            if (TotalMinutes > 0)
            {
                decimal totalAmtForMinutes = (Convert.ToDecimal(TotalMinutes / 60.00) * rate);
                totalamt = totalamt + totalAmtForMinutes;
            }

            ReturnModel.TotalHoursForAllTodos = TotalHours + "h " + TotalMinutes + "m";

            ReturnModel.TotalAmountForAllTodos = Math.Round(totalamt, 2);


            #endregion

            ReturnModel.AllTodos = list;

            return ReturnModel;

        }

        public string ChangeInvoiceStatus(MODEL.UserInvoice.InvoiceStatus model, int StripeApplicationFee)
        {
            string msg = string.Empty;

            //var invoice = (from inv in dbcontext.Invoices
            //               where inv.Id == model.InvoiceId
            //               select inv).SingleOrDefault();
            //if (invoice.Id > 0)
            //{
            //    invoice.InvoiceApprovedStatus = true;
            //    invoice.ApprovedBy = model.UserId;
            //    invoice.IsPaid = true;
            //    invoice.PaidBy = model.UserId;
            //    dbcontext.SaveChanges();
            //}

            MODEL.InvoiceModel obj;
            using (var con = new SqlConnection(ConnectionString))
            {
                obj = con.Query<MODEL.InvoiceModel>("usp_ApproveInvoice", new { UserId = model.UserId, InvoiceId = model.InvoiceId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }



            #region Stripe Payment starts here 

         

            UserDetail user;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from UserDetails usr where usr.UserId = @UserId and usr.IsDeleted = 0";
                user = con.Query<UserDetail>(sqlquery, new { UserId = obj.UserId }).FirstOrDefault();
            }

            var myCharge = new StripeChargeCreateOptions();

            int Amount = Convert.ToInt32(Math.Floor(obj.TotalAmount * 100));

            int StripeCharges = Convert.ToInt32(Math.Floor(((Amount * 2.9) / 100) + 30));

            myCharge.Amount = Amount + StripeCharges;

            int ApplicationFee = ((Amount * StripeApplicationFee) / 100) + StripeCharges;

            myCharge.Currency = "usd";
            // set this if you want to
            myCharge.Description = "Invoice : Payment of $ " + (myCharge.Amount / 100) + " from " + obj.CompanyName + " to " + user.FirstName;
            myCharge.SourceTokenOrExistingSourceId = model.Token;

            // set this if you have your own application fees (you must have your application configured first within Stripe)
            myCharge.ApplicationFee = ApplicationFee;

            // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
            myCharge.Capture = true;

            if (user.StripeUserId != string.Empty)
            {
                myCharge.Destination = user.StripeUserId;

                var chargeService = new StripeChargeService();
                StripeCharge stripeCharge = chargeService.Create(myCharge);

                if (stripeCharge.Status == "succeeded")
                {

                    #region Transaction
                    //inserting values in Transaction table

                    Guid obj_Guid = Guid.NewGuid();

                    using (var con = new SqlConnection(ConnectionString))
                    {
                        con.Execute("usp_InsertTransactionForApprovedInvoices", new { EventID = obj.Id, TotalAmount = Convert.ToDecimal(obj.TotalAmount), TransactionID = obj_Guid.ToString(), TransactionBy = model.UserId, TransactionFor = user.UserId, StripeChargeId = stripeCharge.Id }, commandType: CommandType.StoredProcedure);
                    }

     
                    #endregion

                    msg = "Payment successfull";

                    /*     Sending Mail via Thread  if payment successfull    */
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForApproveInvoice(obj);
                    }
                    ));
                    childref.Start();

                }
            }

            #endregion

            return msg;
        }


        public void ThreadForApproveInvoice(MODEL.InvoiceModel mod)
        {


            UserDetail usr;
            UserDetail ApprovedByusr;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_usr = @"select * from UserDetails u where u.UserId = @UserId and u.IsDeleted = 0";
                string sqlquery_ApprovedByusr = @"select * from UserDetails Approvedusr where Approvedusr.UserId = @ApprovedBy and Approvedusr.IsDeleted = 0";

                usr = con.Query<UserDetail>(sqlquery_usr, new { UserId = mod.UserId }).FirstOrDefault();
                ApprovedByusr = con.Query<UserDetail>(sqlquery_ApprovedByusr, new { ApprovedBy = mod.ApprovedBy }).FirstOrDefault();

            }

            //string emailbody = "Dear " + usr.FirstName + " <br/>";
            //emailbody += "<p>Invoice you have sent for amount $" + mod.TotalAmount + " has been approved and paid to you by " + ApprovedByusr.FirstName + " .</p>";


            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "ApproveInvoice")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var item in MailModule)
            {
                emailbody = item.mailbody;
                Subject = item.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverFirstName", usr.FirstName).Replace("@@TotalAmount", mod.TotalAmount.ToString()).Replace("@@ApprovedByusr_FirstName", ApprovedByusr.FirstName).Replace("@@InvoiceID", mod.InvoiceNumber);
            Subject = Subject.Replace("@@InvoiceID", mod.InvoiceNumber);

            //await EmailUtility.Send(objBLL.getUserEmail(mod.UserId), "Invoice Approved and Paid", emailbody);
            EmailUtility.SendMailInThread(usr.Email, Subject, emailbody);
        }


        public List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> GetProjectMembersForUser(MODEL.UserInvoice.ProjectMembersToForwardInvoice_QueryParams model)
        {



            List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> list;
            using (var con = new SqlConnection(ConnectionString))
            {
                list = con.Query<MODEL.UserInvoice.ProjectMembersToForwardInvoice>("usp_GetProjectMembersForUser", new { UserId = model.UserId, invoiceId = model.invoiceId, }, commandType: CommandType.StoredProcedure).ToList();
            }


            return list;
        }

        public string ForwardInvoice(MODEL.UserInvoice.ForwardInvoice model)
        {
            string ReturnMsg = string.Empty;

            if (model != null)
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    ReturnMsg = con.Query<string>("usp_ForwardInvoice", new { InvoiceId = model.InvoiceId, UserId = model.UserId, ForwardTo = model.ForwardTo, CompanyId = model.CompanyId }).Single();
                }

                if (ReturnMsg == "saved")
                {
                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForForwardInvoice(model);
                    }
                    ));
                    childref.Start();
                }

                //var DuplicateInvoiceForward = from dup in dbcontext.InvoiceForwards
                //                              where dup.InvoiceId == model.InvoiceId
                //                              && dup.UserId == model.UserId
                //                              && dup.ForwardTo == model.ForwardTo
                //                              && dup.CompanyId == model.CompanyId
                //                              select dup;

                //if (DuplicateInvoiceForward.Count() == 0)
                //{

                //    var OldInvoiceForwarded = (from invfrwrd in dbcontext.InvoiceForwards
                //                               where invfrwrd.InvoiceId == model.InvoiceId
                //                               && invfrwrd.CompanyId == model.CompanyId
                //                               select invfrwrd).ToList();
                //    foreach (var item in OldInvoiceForwarded)
                //    {
                //        item.IsCurrentlyActive = false;
                //    }
                //    dbcontext.SaveChanges();

                //    ENTITY.InvoiceForward FrwrdInv = new ENTITY.InvoiceForward()
                //    {
                //        InvoiceId = model.InvoiceId,
                //        UserId = model.UserId,
                //        CompanyId = model.CompanyId,
                //        ForwardTo = model.ForwardTo,
                //        ForwardDate = DateTime.Now,
                //        IsCurrentlyActive = true
                //    };

                //    dbcontext.InvoiceForwards.Add(FrwrdInv);
                //    dbcontext.SaveChanges();

                //    if (FrwrdInv.Id > 0)
                //    {
                //        HttpContext ctx = HttpContext.Current;
                //        Thread childref = new Thread(new ThreadStart(() =>
                //        {
                //            HttpContext.Current = ctx;
                //            ThreadForForwardInvoice(model);
                //        }
                //        ));
                //        childref.Start();
                //    }

                //    ReturnMsg = "saved";
                //}


            }
            return ReturnMsg;
        }


        private void ThreadForForwardInvoice(MODEL.UserInvoice.ForwardInvoice model)
        {
            //var ForwardTo = (from ForwardUser in dbcontext.UserDetails
            //                 where ForwardUser.UserId == model.ForwardTo
            //                 && ForwardUser.IsDeleted == false
            //                 select ForwardUser).FirstOrDefault();

            //var ForwardFrom = (from ForwardfromUser in dbcontext.UserDetails
            //                   where ForwardfromUser.UserId == model.UserId
            //                   && ForwardfromUser.IsDeleted == false
            //                   select ForwardfromUser).FirstOrDefault();

            UserDetail ForwardTo;
            UserDetail ForwardFrom;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_ForwardTo = @"select * from UserDetails ForwardUser where ForwardUser.UserId = @ForwardTo and ForwardUser.IsDeleted = 0";
                string sqlquery_ForwardFrom = @"select * from UserDetails ForwardfromUser where ForwardfromUser.UserId = @UserId and ForwardfromUser.IsDeleted = 0";

                ForwardTo = con.Query<UserDetail>(sqlquery_ForwardTo, new { ForwardTo = model.ForwardTo }).FirstOrDefault();
                ForwardFrom = con.Query<UserDetail>(sqlquery_ForwardFrom, new { UserId = model.UserId }).FirstOrDefault();

            }

            //String emailbody = "<p> Hello " + ForwardTo.FirstName + " <br/>";
            //emailbody += " Invoice is forwarded to you by "+ ForwardFrom.FirstName + " for approval :<br/>";

            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "ForwardInvoice")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var item in MailModule)
            {
                emailbody = item.mailbody;
                Subject = item.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverFirstName", ForwardTo.FirstName).Replace("@@ForwardFrom_FirstName", ForwardFrom.FirstName);


            /*Send Email to user and inform about Invoice Forwarded */
            EmailUtility.SendMailInThread(ForwardTo.Email, Subject, emailbody);

        }

        public List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> getAllFreelancerForAdmin(int CompanyId)
        {
            //List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> list = (from Usr_Dtls in dbcontext.UserDetails
            //                                                               join CmpnyRole in dbcontext.UserCompanyRoles on Usr_Dtls.UserId equals CmpnyRole.UserId
            //                                                               where CmpnyRole.CompanyId == CompanyId
            //                                                               && Usr_Dtls.IsDeleted == false
            //                                                               && Usr_Dtls.IsFreelancer == true
            //                                                               select new MODEL.UserInvoice.ProjectMembersToForwardInvoice
            //                                                               {
            //                                                                   NameWithEmail = Usr_Dtls.FirstName,
            //                                                                   UserId = Usr_Dtls.UserId
            //                                                               }).ToList();

            List<MODEL.UserInvoice.ProjectMembersToForwardInvoice> list;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select Usr_Dtls.FirstName as NameWithEmail,Usr_Dtls.UserId as UserId from UserDetails Usr_Dtls inner join UserCompanyRoles CmpnyRole on Usr_Dtls.UserId = CmpnyRole.UserId 
                                where CmpnyRole.CompanyId = @CompanyId and Usr_Dtls.IsDeleted = 0 and Usr_Dtls.IsFreelancer = 1";
                list = con.Query<MODEL.UserInvoice.ProjectMembersToForwardInvoice>(sqlquery, new { CompanyId = CompanyId }).ToList();
            }
            return list;
        }


        public MODEL.UserInvoice.UserCardPaymentModel GetCardDetailForPayment(int PaymentDetailId, int CompanyId, int InvoiceId, int StripeApplicationFee)
        {
            //MODEL.UserInvoice.UserCardPaymentModel list = (from pay in dbcontext.PaymentDetails
            //                                               where pay.CompanyId == CompanyId
            //                                               && pay.IsDefault == true
            //                                               //&& pay.Id == PaymentDetailId
            //                                               && pay.IsDeleted == false
            //                                               select new MODEL.UserInvoice.UserCardPaymentModel
            //                                               {
            //                                                   Card = pay.CreditCardNumber,
            //                                                   Month = pay.ExpiryMonth,
            //                                                   Year = pay.ExpiryYear,
            //                                                   Cvv = pay.CvvNumber
            //                                               }
            //                                               ).FirstOrDefault();

            MODEL.UserInvoice.UserCardPaymentModel list;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select pay.CreditCardNumber as Card,pay.ExpiryMonth as Month, pay.ExpiryYear as Year,pay.CvvNumber as Cvv from PaymentDetails pay where pay.CompanyId = @CompanyId and pay.IsDefault = 1 and pay.IsDeleted = 0";
                list = con.Query<MODEL.UserInvoice.UserCardPaymentModel>(sqlquery, new { CompanyId = CompanyId }).FirstOrDefault();
            }

            if (InvoiceId == 0)
            {
                return list;
            }

            //var invoice = (from inv in dbcontext.Invoices
            //               where inv.Id == InvoiceId
            //               select inv).SingleOrDefault();

            TIGHTEN.ENTITY.Invoice invoice;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select * from Invoices inv where inv.Id = @InvoiceId";
                invoice = con.Query<TIGHTEN.ENTITY.Invoice>(sqlquery, new { InvoiceId = InvoiceId }).FirstOrDefault();
            }

            decimal Amount = Math.Round(invoice.TotalAmount, 2);
            decimal StripeCharges = Convert.ToDecimal(Math.Round((Convert.ToDouble(Amount) * 0.029) + 0.30, 2));
            decimal ApplicationFee = Math.Round((Amount * StripeApplicationFee) / 100, 2);
            decimal NetAmount = Math.Round(Amount + StripeCharges + ApplicationFee, 2);

            if (list != null)
            {
                list.FreelancerInvoiceAmount = (Amount).ToString();
                list.StripeFees = StripeCharges.ToString();
                list.TightenCharges = ApplicationFee.ToString();
                list.NetAmountForAdmin = NetAmount.ToString();
            }

            return list;
        }



        public List<MODEL.PaymentDetailModel> GetAllCardsForPaymentDetail(int CompanyId)
        {
            //List<MODEL.PaymentDetailModel> PaymentDetailList = (from pay in dbcontext.PaymentDetails
            //                                                    where pay.CompanyId == CompanyId
            //                                                    && pay.IsDeleted == false
            //                                                    select new MODEL.PaymentDetailModel
            //                                                    {
            //                                                        Id = pay.Id,
            //                                                        CardName = pay.CardName,
            //                                                        CardDescription = pay.CardDescription,
            //                                                        CreditCardNumber = pay.CreditCardNumber,
            //                                                        IsSelected = false
            //                                                    }
            //                                                     ).ToList();

            List<MODEL.PaymentDetailModel> PaymentDetailList;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select pay.Id,pay.CardName,pay.CardDescription,pay.CreditCardNumber,0 as IsSelected from PaymentDetails pay where pay.CompanyId = @CompanyId and pay.IsDeleted = 0";
                PaymentDetailList = con.Query<MODEL.PaymentDetailModel>(sqlquery).ToList();
            }


            return PaymentDetailList;
        }


        public string ChangeInvoiceApprovedStatus(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            string msg = string.Empty;

            //var invoice = (from inv in dbcontext.Invoices
            //               where inv.Id == model.InvoiceId
            //               select inv).SingleOrDefault();
            //if (invoice.Id > 0)
            //{
            //    invoice.InvoiceApprovedStatus = true;
            //    invoice.ApprovedBy = model.UserId;
            //    dbcontext.SaveChanges();
            //    msg = "Invoice Approved Status Changed";
            //}

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update Invoices set InvoiceApprovedStatus = 1,ApprovedBy = @UserId where Id = @InvoiceId";
                int count = con.Query<int>(sqlquery, new { UserId = model.UserId, InvoiceId = model.InvoiceId }).Single();
                if (count > 0)
                {
                    msg = "Invoice Approved Status Changed";
                }
            }

            return msg;
        }

        public string RejectInvoice(MODEL.UserInvoice.InvoiceRejectModel model)
        {
            string msg = string.Empty;

            //var invoice = (from inv in dbcontext.Invoices
            //               where inv.Id == model.InvoiceId
            //               select inv).SingleOrDefault();
            //if (invoice.Id > 0)
            //{
            //    invoice.IsRejected = true;
            //    invoice.RejectReason = model.Reason;
            //    invoice.RejectedBy = model.UserId;
            //    dbcontext.SaveChanges();
            //    msg = "Invoice is rejected";

            //    // Rearrange Todos for generating the rejected invoice again

            //    var invoicedetails = (from InvDet in dbcontext.InvoiceDetails
            //                          where InvDet.InvoiceId == invoice.Id
            //                          select InvDet).ToList();




            //    foreach (var item in invoicedetails)
            //    {
            //        var todo = (from t in dbcontext.ToDos
            //                    where t.Id == item.TodoId
            //                    select t).FirstOrDefault();

            //        todo.IsAvailableForInvoice = true;
            //        dbcontext.Entry<ToDo>(todo).State = EntityState.Modified;

            //        string[] TodoTimeLogArray = null;
            //        if (item.TodoTimeLogIds != string.Empty && item.TodoTimeLogIds != null)
            //        {
            //            TodoTimeLogArray = item.TodoTimeLogIds.Split(',');
            //        }

            //        foreach (var arrayItem in TodoTimeLogArray)
            //        {
            //            int TimeLogId = Convert.ToInt32(arrayItem);

            //            var TimeLog = (from tlog in dbcontext.TodoTimeLogs
            //                           where tlog.Id == TimeLogId
            //                           select tlog).FirstOrDefault();

            //            if (TimeLog != null)
            //            {
            //                TimeLog.IsInvoiceGenerated = false;
            //                dbcontext.Entry<TodoTimeLogs>(TimeLog).State = EntityState.Modified;
            //            }
            //        }

            //    }

            //    dbcontext.SaveChanges();

            //}



            List<TIGHTEN.ENTITY.InvoiceDetail> invoicedetails;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update Invoices set IsRejected = 1,RejectReason = @Reason,RejectedBy = @UserId where Id = @InvoiceId";
                int count = con.Execute(sqlquery, new { Reason = model.Reason, UserId = model.UserId, InvoiceId = model.InvoiceId });
                if (count > 0)
                {
                    msg = "Invoice is rejected";
                }


                string sqlquery_InvoiceDetail = @"select * from InvoiceDetails InvDet where InvDet.InvoiceId = @InvoiceId";
                invoicedetails = con.Query<TIGHTEN.ENTITY.InvoiceDetail>(sqlquery_InvoiceDetail, new { InvoiceId = model.InvoiceId }).ToList();

                List<MODEL.UserInvoice.InvoiceRejectDetailsModel> list = new List<MODEL.UserInvoice.InvoiceRejectDetailsModel>();

                foreach (var item in invoicedetails)
                {
                    MODEL.UserInvoice.InvoiceRejectDetailsModel obj = new MODEL.UserInvoice.InvoiceRejectDetailsModel
                    {
                        TodoId = item.TodoId,
                        TodoTimeLogIds = item.TodoTimeLogIds
                    };

                    list.Add(obj);
                }

                con.Execute("usp_RejectInvoice", new
                {
                    InvoiceDetails = list.AsTableValuedParameter("InvoiceRejectDetails", new[] { "TodoId", "TodoTimeLogIds" })
                }, commandType: CommandType.StoredProcedure);

            }


            return msg;
        }


        public int SaveInvoiceByVendor(InvoiceModel model)
        {
     
            int Id = 0;
            InvoiceMail ObjIvoiceMail;


            using (var con = new SqlConnection(ConnectionString))
            {
                ObjIvoiceMail = con.Query<InvoiceMail>("Usp_SaveVedorInvoice", new
                {
                    InvoiceNumber = model.InvoiceNumber.ToString(),
                    UserId = model.UserId,
                    SendTo = model.SendTo,
                    CompanyId = model.CompanyId,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    TotalHours = model.TotalHours,
                    TotalAmount = model.TotalAmount,
                    Message = model.Message,
                    ProjectId = model.ProjectId,
                    VendorId = model.VendorId,
                    PhaseId = model.PhaseId,
                    MilestoneId = model.MilestoneId.Length > 0 ? model.MilestoneId.Substring(0, model.MilestoneId.Length - 1) : null,
                },commandType:CommandType.StoredProcedure).FirstOrDefault();
            }
            if (ObjIvoiceMail != null)
            {
                Id = 1;
                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForsendMailOnInvoiceRaiseByVendor(ObjIvoiceMail);

                }

                ));
                childref.Start();

            }

            return Id ;
        }
        public void ThreadForsendMailOnInvoiceRaiseByVendor(InvoiceMail item)
        {
            String emailbody = null;
            string Subject = string.Empty;

            string subjectBysender = item.InvoiceNo + "|" + item.ProjectName + "|" + item.SenderName;
           


                        XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "SendInvoiceRaaiseMailByVendor")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@ReceiverName", item.ReceiverName).Replace("@@SenderName", item.SenderName)
                .Replace("@@SenderCompanyName", item.SenderCompanyName).Replace("@@ProjectName", item.ProjectName)
                .Replace("@@subjectBysender", subjectBysender).Replace("@@InvoiceNo",item.InvoiceNo)
                .Replace("@@TotalAmount", item.TotalAmount).Replace("@@TotalHours", item.TotalHours);
            EmailUtility.SendMailInThread(item.Email, subjectBysender, emailbody);

        }



    }
}
