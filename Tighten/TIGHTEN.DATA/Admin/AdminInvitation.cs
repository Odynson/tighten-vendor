﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class AdminInvitation : BaseClass
    {

        public List<MODEL.AllSentRequestForAdminModel> getAllSentRequest(int CompanyId,int PageIndex,int PageSizeSelected)
        {

            //List<MODEL.AllSentRequestForAdminModel> AllRequest = (from inv in dbcontext.FreelancerInvitations
            //                                                      join usrcom in dbcontext.Companies on inv.CompanyId equals usrcom.Id
            //                                                      where usrcom.Id == CompanyId

            //                                                      select new MODEL.AllSentRequestForAdminModel
            //                                                      {
            //                                                           FreelancerInvitationId = inv.Id,
            //                                                           FreelancerEmail = inv.FreelancerEmail,
            //                                                           CompanyId = inv.CompanyId,
            //                                                           SentBy = inv.SentBy,
            //                                                           ProjectId = inv.ProjectId,
            //                                                           UserRate = inv.UserRate.HasValue ? inv.UserRate.Value : 0,
            //                                                           RoleId = inv.RoleId,
            //                                                           Activationkey = inv.Activationkey,
            //                                                           IskeyActive = inv.IskeyActive,
            //                                                           SentDate = inv.SentDate,
            //                                                           IsAccepted = inv.IsAccepted,
            //                                                           IsRejected = inv.IsRejected.HasValue ? inv.IsRejected.Value : false,
            //                                                           WeeklyLimit = inv.WeeklyLimit.HasValue ? inv.WeeklyLimit.Value : 0,
            //                                                           JobTitle = inv.JobTitle,
            //                                                           JobDescription = inv.JobDescription,
            //                                                           Remarks = inv.Remarks,
            //                                                           IsJobInvitation = inv.IsJobInvitation.HasValue ? inv.IsJobInvitation.Value : false,

            //                                                          ProjectName = (from prjctt in dbcontext.Projects
            //                                                                          where prjctt.Id == inv.ProjectId 
            //                                                                          && prjctt.IsDeleted == false
            //                                                                          select prjctt.Name).FirstOrDefault(),
            //                                                           RoleName =    (from rol in dbcontext.Roles
            //                                                                          where rol.RoleId == inv.RoleId
            //                                                                          && rol.IsDeleted == false
            //                                                                          select rol.Name
            //                                                                          ).FirstOrDefault(),

            //                                                          SenderName = (from ussr in dbcontext.UserDetails
            //                                                                         where ussr.UserId == inv.SentBy
            //                                                                         && ussr.IsDeleted == false
            //                                                                         select ussr.FirstName).FirstOrDefault(),
            //                                                          FreelancerUserId = (from ussrr in dbcontext.UserDetails
            //                                                                              where ussrr.Email == inv.FreelancerEmail
            //                                                                              && ussrr.IsDeleted == false
            //                                                                              select ussrr.UserId).FirstOrDefault(),
            //                                                          FreelancerProfilePhoto = (from ussrr in dbcontext.UserDetails
            //                                                                                    where ussrr.Email == inv.FreelancerEmail
            //                                                                                    && ussrr.IsDeleted == false
            //                                                                                    select ussrr.ProfilePhoto).FirstOrDefault()


            //                                                      }).ToList();

            List<MODEL.AllSentRequestForAdminModel> AllRequest;
            using (var con = new SqlConnection(ConnectionString))
            {
                AllRequest = con.Query<MODEL.AllSentRequestForAdminModel>("usp_GetAllSentRequestForAdmin", new { CompanyId = CompanyId,PageIndex=PageIndex,PageSizeSelected=PageSizeSelected }, commandType: CommandType.StoredProcedure).ToList();
            }


            return AllRequest;
        }





    }
}
