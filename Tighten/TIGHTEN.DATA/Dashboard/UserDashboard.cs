﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;
using System.Data;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class UserDashboard :Common
    {
        AppContext dbcontext;

        /// <summary>
        /// It gets the upcoming Todos of the user logged in. Whole deadline date is greater than present date
        /// </summary>
        /// <param name="UserId">It is the unique id of logged in user</param>
        /// <returns>Returns the list of Todos</returns>
        public List<TodoModel.TodosData> getUserUpcomingTodos(string UserId)
        {
            dbcontext = new AppContext();
            DateTime now = DateTime.Today.AddDays(7);
            List<TodoModel.TodosData> userUpcomingTodos = (from t0 in dbcontext.Teams
                                                           join t in dbcontext.Projects on t0.Id equals t.TeamId
                                                           join t1 in dbcontext.Sections on t.Id equals t1.ProjectId
                                                           join c in dbcontext.ToDos on t1.Id equals c.SectionId
                                                           join c3 in dbcontext.UserDetails on c.CreatedBy equals c3.UserId
                                                           join c1 in dbcontext.UserDetails on c.AssigneeId equals c1.UserId
                                                           where c.AssigneeId == UserId && c.TaskType==5  
                                                           && ((c.DeadlineDate>=DateTime.Today) &&(c.DeadlineDate <= now))
                                                           && t0.IsDeleted == false  && c3.IsDeleted == false && c1.IsDeleted == false
                                                           && t.IsDeleted == false && c.IsDeleted == false
                                                           orderby c.DeadlineDate

                                                           let AssigneeProfilePhoto = c1.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c1.ProfilePhoto

                                                           let ReporterProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
                                                           let AssigneeName = c1.FirstName + " " + c1.LastName
                                                           let ReporterName = c3.FirstName + " " + c3.LastName
                                                           select new TodoModel.TodosData
                                                           {
                                                               Name = c.Name,
                                                               SectionId = c.SectionId,
                                                               TodoId = c.Id,
                                                               AssigneeId = c.AssigneeId,
                                                               AssigneeProfilePhoto = AssigneeProfilePhoto,
                                                               AssigneeName = AssigneeName,
                                                               AssigneeEmail = c1.Email,
                                                               ReporterId = c3.UserId,
                                                               ReporterEmail = c3.Email,
                                                               ReporterName = ReporterName,
                                                               ReporterProfilePhoto = ReporterProfilePhoto,
                                                               Description = c.Description,
                                                               DeadlineDate = c.DeadlineDate,
                                                               InProgress = c.InProgress,
                                                               IsDone = c.IsDone,
                                                               SectionName = t1.SectionName,
                                                               TeamId = t0.Id,
                                                               TeamName = t0.TeamName,
                                                               ProjectId = t.Id,
                                                               ProjectName = t.Name,
                                                               CreatedDate = c.CreatedDate,
                                                               ModifiedDate = c.ModifiedDate

                                                           }).ToList();


            return userUpcomingTodos;
        }



        /// It is a Function for getting Calendar Events for Logged In
        /// </summary>
        /// <param name="UserId">It is the unique id of user</param>
        /// <returns>It returns the Calendar Events for a particular User</returns>
        public List<TodoModel.CalendarEvents> GetUserUpcomingEvents(string UserId)
        {
            dbcontext = new AppContext();
            DateTime now = DateTime.Now.AddDays(7);
            List<TodoModel.CalendarEvents> cal = (from m in dbcontext.Teams
                                                  join m1 in dbcontext.TeamUsers on m.Id equals m1.TeamId
                                                  join m2 in dbcontext.CalendarEvents on m.Id equals m2.TeamId
                                                  join m3 in dbcontext.UserDetails on m2.CreatedBy equals m3.UserId
                                                  where m1.UserId == UserId && ((m2.EndDate>= DateTime.Now) && (m2.EndDate <= now))
                                                  && m3.IsDeleted == false && m.IsDeleted == false
                                                  let CreatorName = m3.FirstName + " " + m3.LastName
                                                  let CreatorProfilePhoto = m3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m3.ProfilePhoto

                                                  select new TodoModel.CalendarEvents
                                                  {
                                                      Id = m2.Id,
                                                      Title = m2.Title,
                                                      Start = m2.StartDate,
                                                      End = m2.EndDate,
                                                      AllDay = m2.AllDay,
                                                      Url = m2.Url,
                                                      Description = m.Description,
                                                      CreatorName = CreatorName,
                                                      CreatorProfilePhoto = CreatorProfilePhoto
                                                  }

                                              ).ToList();

            return cal;

        }


        /// <summary>
        /// It gets the User T-Line with all fields
        /// </summary>
        /// <param name="UserId">It is the unique Id of User</param>
        /// <returns>It returns the T-Line Model</returns>
        public T_LineModel GetUserTLine(string UserId)
        {
            dbcontext = new AppContext();

            int teamsCount = (from m in dbcontext.TeamUsers where m.UserId == UserId select m).Count();

            int projectsCount = (from m in dbcontext.ProjectUsers where m.UserId == UserId select m).Count();

            int pendingTodosCount = (from m in dbcontext.ToDos where m.AssigneeId == UserId && m.IsDone == false && m.TaskType == 5 && m.IsDeleted == false select m).Count();

            int completedTodosCount = (from m in dbcontext.ToDos where m.AssigneeId == UserId && m.IsDone == true && m.TaskType == 5 && m.IsDeleted == false select m).Count();

            int beforeHandTodosCount = (from m in dbcontext.ToDos where m.AssigneeId == UserId && m.IsDone == true && m.TaskType == 5 && m.DateOfCompletion < m.DeadlineDate && m.IsDeleted == false select m).Count();


            int retardedTodosCount = (from m in dbcontext.ToDos where m.AssigneeId == UserId && m.IsDone == true && m.TaskType == 5 && m.DateOfCompletion > m.DeadlineDate && m.IsDeleted == false select m).Count();

            int totalTodosCount = (from m in dbcontext.ToDos where m.AssigneeId == UserId && m.TaskType == 5 && m.IsDeleted == false select m).Count();

            T_LineModel mod = new T_LineModel();

            mod.Teams = teamsCount;
            mod.Projects = projectsCount;
            mod.PendingTasks = pendingTodosCount;
            mod.CompletedTasks = completedTodosCount;
            mod.BeforeHandTasks = beforeHandTodosCount;
            mod.RetardedTasks = retardedTodosCount;
            mod.TotalTasks = totalTodosCount;

            return mod;



            //var tlineModelObject = (from cart in dbcontext.ToDos

            //                        where cart.AssigneeId == UserId










            //                        //from t0 in dbcontext.Teams
            //                        //                    join t3 in dbcontext.TeamUsers on t0.Id equals t3.TeamId
            //                        //                    into teams


            //                        //                    join t in dbcontext.Projects on t0.Id equals t.TeamId
            //                        //                    join t4 in dbcontext.ProjectUsers on t.Id equals t4.ProjectId
            //                        //                    into projects


            //                        //                    join c1 in dbcontext.Sections on t.Id equals c1.ProjectId
            //                        //                    join c in dbcontext.ToDos on c1.Id equals c.SectionId
            //                        //                    into todos



            //                        //let Teams =cart.Sections.selectMany(b=>b.) //teams.Count(m => m.UserId == UserId)
            //                        //let Projects = cart.Sections.Count(m1 => m1.UserId == UserId)
            //                        let BeforeHandTasks = todos.Count(m => m.DateOfCompletion < m.DeadlineDate && m.AssigneeId == UserId)
            //                        let PendingTasks = todos.Count(m => m.IsDone != true && m.AssigneeId == UserId)
            //                        let CompletedTasks = todos.Count(m => m.IsDone == true && m.AssigneeId == UserId)
            //                        let RetardedTasks = todos.Count(m => m.DateOfCompletion > m.DeadlineDate && m.AssigneeId == UserId)
            //                        let TotalTasks = todos.Count(m => m.AssigneeId == UserId)

            //                        select new T_LineModel
            //                        {
            //                            Teams = cart.Sections.,
            //                            Projects = Projects,
            //                            BeforeHandTasks = BeforeHandTasks,
            //                            PendingTasks = PendingTasks,
            //                            CompletedTasks = CompletedTasks,
            //                            RetardedTasks = RetardedTasks,
            //                            TotalTasks = TotalTasks

            //                        }

            //     );

            //return tlineModelObject;

        }



        /// <summary>
        /// It gets All the Packages
        /// </summary>
        /// <returns>It returns All the Packages </returns>
        public List<MODEL.PackageModel> GetAllPackages(int CompanyId)
        {
           
            dbcontext = new AppContext();

            //int OldPackageId = (from sub in dbcontext.Subscriptions
            //                    where sub.CompanyId == CompanyId
            //                    && sub.IsActive == true
            //                    select sub.PackageID).FirstOrDefault();

            List<MODEL.PackageModel> list;

            using (var con = new SqlConnection(ConnectionString))
            {
                string query = @" declare @OldPackageId int
                                   select top 1 @OldPackageId =  ISNULL(sub.PackageID,0) from Subscriptions sub  where sub.CompanyId = @CompanyId and sub.IsActive = 1
                                   select pack.Id Id,pack.Description Description,pack.Name Name ,pack.Price Price,pack.DurationInDays DurationInDays,Discount Discount,
                                   pack.Description, case when pack.Id = 0  then 0 when pack.Id = @OldPackageId then 1 else 0 end IsActive from Packages pack";

                 list = con.Query<MODEL.PackageModel>(query, new { CompanyId = CompanyId }).ToList();


            }


                             //list = (from pack in dbcontext.Packages
                             //                select new MODEL.PackageModel
                             //                {
                             //                    Id = pack.Id,
                             //                    Description = pack.Description,
                             //                    Name = pack.Name,
                             //                    Price = pack.Price,
                             //                    DurationInDays = pack.DurationInDays,
                             //                    Discount = pack.Discount,
                             //                    IsActive = OldPackageId == 0 ? false :  pack.Id == OldPackageId ? true : false

                             //                }).ToList();



            return list;
        }


        /// <summary>
        /// Upgrading Packages
        /// </summary>
        /// <returns>It returns success msg </returns>
        public string UpgradePackage(MODEL.SubscriptionModel model)
        {
            dbcontext = new AppContext();

            var SubscriptionList = (from SubsList in dbcontext.Subscriptions
                                    where SubsList.UserId == model.UserId
                                    && SubsList.IsActive == true
                                    select SubsList
                                    ).ToList();

            foreach (var item in SubscriptionList)
            {
                item.IsActive = false;
                dbcontext.SaveChanges();
            }

            Package pack = (from p in dbcontext.Packages
                            where p.Id == model.PackageId
                            select p).SingleOrDefault();

            if (pack != null)
            {
                // Save Default Subscription  */
                TIGHTEN.ENTITY.Subscription Subscription = new TIGHTEN.ENTITY.Subscription
                {
                    PackageID = model.PackageId,
                    UserId = model.UserId,
                    CompanyId = model.CompanyId,
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(pack.DurationInDays),
                    IsActive = true
                };
                dbcontext.Subscriptions.Add(Subscription);
                var SubscriptionId = dbcontext.SaveChanges();
            }

            return "Package is Upgraded successfully !";
        }


    }
}
