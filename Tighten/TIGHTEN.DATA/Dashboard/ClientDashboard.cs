﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.MODEL;
using TIGHTEN.ENTITY;

namespace TIGHTEN.DATA
{
    public class ClientDashboard
    {
        AppContext dbcontext;

        /// <summary>
        /// It gets the upcoming Todos for the Client
        /// </summary>
        /// <param name="UserId">It is the unique id of logged in user</param>
        /// <returns>Returns the list of Todos</returns>
        public List<TodoModel.TodosData> getClientUpcomingTodos(string UserId)
        {
            
            dbcontext = new AppContext();
            List<TodoModel.TodosData> clientUpcomingTodos = (from t0 in dbcontext.Teams
                                                             join t3 in dbcontext.TeamUsers on t0.Id equals t3.TeamId
                                                             join t in dbcontext.Projects on t0.Id equals t.TeamId
                                                             join t2 in dbcontext.ProjectUsers on t.Id equals t2.ProjectId
                                                             join t1 in dbcontext.Sections on t.Id equals t1.ProjectId
                                                             join c in dbcontext.ToDos on t1.Id equals c.SectionId
                                                             join c3 in dbcontext.UserDetails on c.CreatedBy equals c3.UserId
                                                             join c1 in dbcontext.UserDetails on c.AssigneeId equals c1.UserId
                                                             into c2
                                                             where t3.UserId == UserId && t2.UserId == UserId && c.TaskType==5
                                                             && c3.IsDeleted == false && t0.IsDeleted == false && t.IsDeleted == false
                                                             && c.DeadlineDate >= DateTime.Now && c.IsDeleted == false
                                                             orderby c.DeadlineDate ascending
                                                             from c5 in c2.DefaultIfEmpty()
                                                             let AssigneeProfilePhoto = c5.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c5.ProfilePhoto
                                                             let ReporterProfilePhoto = c3.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + c3.ProfilePhoto
                                                             let AssigneeName = c5.FirstName + " " + c5.LastName
                                                             let ReporterName = c3.FirstName + " " + c3.LastName
                                                             select new TodoModel.TodosData
                                                             {
                                                                 Name = c.Name,
                                                                 SectionId = c.SectionId,
                                                                 TodoId = c.Id,
                                                                 AssigneeId = c.AssigneeId,
                                                                 AssigneeProfilePhoto = AssigneeProfilePhoto,
                                                                 AssigneeName = AssigneeName,
                                                                 AssigneeEmail = c5.Email,
                                                                 ReporterId = c3.UserId,
                                                                 ReporterEmail = c3.Email,
                                                                 ReporterName = ReporterName,
                                                                 ReporterProfilePhoto = ReporterProfilePhoto,
                                                                 Description = c.Description,
                                                                 DeadlineDate = c.DeadlineDate,
                                                                 InProgress = c.InProgress,
                                                                 IsDone = c.IsDone,
                                                                 SectionName = t1.SectionName,
                                                                 TeamId = t0.Id,
                                                                 TeamName = t0.TeamName,
                                                                 ProjectId = t.Id,
                                                                 ProjectName = t.Name,
                                                                 CreatedDate = c.CreatedDate,
                                                                 ModifiedDate = c.ModifiedDate
                                                             }).ToList();
            return clientUpcomingTodos;
        }

        /// <summary>
        /// Getting projects in which Client is Involved
        /// </summary>
        /// <param name="userId">It is the unique id of Client</param>
        /// <returns>Returns the Client Projects</returns>
        public List<ProjectModel.ProjectWithMembers> getClientProjects(String userId)
        {
            dbcontext = new AppContext();
            // to list inside this is giving error so I have returened the anonymous type and then convert it into particular type
            var projectList = (from m1 in dbcontext.Projects
                               join m8 in dbcontext.ProjectUsers on m1.Id equals m8.ProjectId
                               where m8.UserId == userId && m1.IsDeleted == false
                               orderby m1.CreatedDate descending
                               select new
                               {
                                   ProjectName = m1.Name,
                                   ProjectDescription = m1.Description,
                                   ProjectId = m1.Id,
                                   ProjectMembers = from m3 in dbcontext.ProjectUsers
                                                    join m2 in dbcontext.UserDetails on m3.UserId equals m2.UserId
                                                    where m3.ProjectId == m1.Id
                                                    && m2.IsDeleted == false
                                                    select new ProjectModel.ProjectMembers
                                                    {
                                                        MemberEmail = m2.Email,
                                                        MemberName = m2.FirstName,
                                                        MemberProfilePhoto = m2.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Icon/" + m2.ProfilePhoto,
                                                        MemberUserId = m2.UserId,
                                                        Value = m2.UserId,
                                                        Text = m2.FirstName + " ( " + m2.Email + " )"
                                                    }
                               });

            List<ProjectModel.ProjectWithMembers> projectMod = projectList.AsEnumerable().Select(item =>
                                                        new ProjectModel.ProjectWithMembers()
                                                        {
                                                            ProjectId = item.ProjectId,
                                                            ProjectName = item.ProjectName,
                                                            ProjectDescription = item.ProjectDescription,
                                                            ProjectMembers = item.ProjectMembers.ToList()
                                                        }).ToList();
            return projectMod;
        }

        /// <summary>
        /// Get Users of those projects in which Client Is Involved
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public List<UserModel> getConcernedUsersOfClient(String UserId)
        {
            dbcontext = new AppContext();
            List<UserModel> result = (from m in dbcontext.Teams
                                      join m1 in dbcontext.TeamUsers on m.Id equals m1.TeamId
                                      join m2 in dbcontext.Projects on m.Id equals m2.TeamId
                                      join m3 in dbcontext.ProjectUsers on m2.Id equals m3.ProjectId
                                      join m4 in dbcontext.ProjectUsers on m3.ProjectId equals m4.ProjectId
                                      //join m5 in dbcontext.UserDetails on m4.UserId equals m5.UserId
                                      into users
                                      where m1.UserId == UserId && m3.UserId == UserId 
                                      && m.IsDeleted == false && m2.IsDeleted == false
                                      from m6 in users.DefaultIfEmpty()
                                      join m5 in dbcontext.UserDetails on m6.UserId equals m5.UserId
                                      where m5.IsDeleted == false
                                      select new UserModel
                                      {
                                          userId = m5.UserId,
                                          firstName = m5.FirstName,
                                          lastName = m5.LastName,
                                          email = m5.Email,
                                          profilePhoto = (m5.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + m5.ProfilePhoto),
                                          Designation = m5.Designation

                                      }).ToList();
            return result;
        }

    }
}
