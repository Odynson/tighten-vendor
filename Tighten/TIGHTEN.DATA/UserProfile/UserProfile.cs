﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.MODEL;
using TIGHTEN.UTILITIES;
using System.Data.SqlClient;
using System.Data.SqlClient;
using System.Data;
using Dapper;

namespace TIGHTEN.DATA
{
    public class UserProfile : BaseClass
    {

        AppContext dbcontext;


        /// <summary>
        /// Getting User's Educational Details for User Profile From UserEducationalDetail Table
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Educational Details as an array</returns>
        public List<MODEL.UserProfileModel.UserEducationDetail> getUserEducationDetail(string UserId)
        {
            //Usp_GetUserEducationDetail

            List<MODEL.UserProfileModel.UserEducationDetail> ObjList;
            List<MODEL.UserProfileModel.UserEducationalStrem> objEduStream;
            using (var con = new SqlConnection(ConnectionString ))
            {


                var result = con.QueryMultiple("Usp_GetUserEducationDetail", new { UserId = UserId }, commandType: CommandType.StoredProcedure);


                ObjList = result.Read<MODEL.UserProfileModel.UserEducationDetail>().ToList();




                if (ObjList != null)
                {
                    objEduStream = result.Read<MODEL.UserProfileModel.UserEducationalStrem>().ToList();
                    foreach (var item in ObjList)
                    {
                        item.EducationalStream = objEduStream.Where(x => x.IdEduDetail == item.Id).ToList();

                    }
                }


            }

            //                           ObjList = (from usredu in dbcontext.UserEducationDetails
            //                                                            where usredu.UserId == UserId 
            //                                                            && usredu.IsDeleted == false
            //                                                            select new MODEL.UserProfileModel.UserEducationDetail
            //                                                            {
            //                                                                Id = usredu.Id,
            //                                                                UserId = usredu.UserId,
            //                                                                School = usredu.School,
            //                                                                DateAttendedFrom = usredu.DateAttendedFrom,
            //                                                                DateAttendedTo = usredu.DateAttendedTo,
            //                                                                Degree = usredu.Degree,
            //                                                                CommaSeperatedEducationalStream = usredu.EducationalStream,
            //                                                                Grade = usredu.Grade,
            //                                                                ActivitiesAndSocieties = usredu.ActivitiesAndSocieties,
            //                                                                Description = usredu.Description
            //                                                            }).ToList();

            //foreach (var item in ObjList)
            //{
            //    if (item.CommaSeperatedEducationalStream != string.Empty && item.CommaSeperatedEducationalStream != null)
            //    {
            //        int[] StreamIds = item.CommaSeperatedEducationalStream.Split(',').Select(str => int.Parse(str)).ToArray();

            //        List<MODEL.UserProfileModel.UserEducationalStrem > streams = (from st in dbcontext.EducationalStreams
            //                                                                       where StreamIds.Contains(st.Id)
            //                                                                       select new MODEL.UserProfileModel.UserEducationalStrem
            //                                                                       {
            //                                                                           Id = st.Id,
            //                                                                           Description = st.Description,
            //                                                                           EducationalStreamName = st.Name,
            //                                                                           IsActive = st.IsActive
            //                                                                       }).ToList();

            //        item.EducationalStream = streams;




            return ObjList;
        }





        /// <summary>
        /// Getting User's Professional Details for User Profile From UserProfessionalDetail Table
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Professional Details as an array</returns>
        public List<MODEL.UserProfileModel.UserProfessionalDetail> getUserProfessionalDetail(string UserId)
        {

            List<MODEL.UserProfileModel.UserProfessionalDetail> ObjList;


            using (var con = new SqlConnection(ConnectionString))
            {

                ObjList = con.Query<MODEL.UserProfileModel.UserProfessionalDetail>("Usp_UserProfessionalDetails",
                new { UserId = UserId }, commandType: CommandType.StoredProcedure).ToList();


            }

            //ObjList = (from usrprofes in dbcontext.UserProfessionalDetails
            //           where usrprofes.UserId == UserId && usrprofes.IsDeleted == false
            //           select new MODEL.UserProfileModel.UserProfessionalDetail
            //           {
            //               Id = usrprofes.Id,
            //               UserId = usrprofes.UserId,
            //               CompanyName = usrprofes.CompanyName,
            //               From = usrprofes.From,
            //               To = usrprofes.To,
            //               Title = usrprofes.Title,
            //               Location = usrprofes.Location,
            //               Description = usrprofes.Description,

            //           }).ToList();

            return ObjList;
        }






        /// <summary>
        /// Save User's Professional Details for User Profile in UserProfessionalDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Professional Details as its properties </param>
        /// <returns>It returns Id of new User's Professional Details entry in UserProfessionalDetail Table </returns>
        /// 
        public int saveUserProfessionalDetail(MODEL.UserProfileModel.UserProfessionalDetail model)
        {

            DateTime FromDate = DateTime.ParseExact(model.FromDate,
                                   "dd/MM/yyyy HH:mm:ss",
                                   CultureInfo.InvariantCulture);

            DateTime ToDate = DateTime.ParseExact(model.ToDate,
                                  "dd/MM/yyyy HH:mm:ss",
                                  CultureInfo.InvariantCulture);


            int Id;

            using (var con = new SqlConnection(ConnectionString))
            {

                Id = con.Query<int>("Usp_InsertUpdateProfessionalDetail", new
                {
                    Id = model.Id,
                    CompanyName = model.CompanyName,
                    Title = model.Title,
                    From = FromDate,
                    To = ToDate,
                    Location = model.Location,
                    Description = model.Description,
                    UserId = model.UserId,
                    IsInsertOrUpdate = 1
                }, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }

            //UserProfessionalDetail obj = new UserProfessionalDetail
            //{
            //    CompanyName = model.CompanyName,
            //    Title = model.Title,
            //    From = FromDate,
            //    To = ToDate,
            //    Location = model.Location,
            //    Description = model.Description,
            //    UserId = model.UserId,
            //};

            //DateTime from12 = DateTime.Now.ToUniversalTime();

            //dbcontext.UserProfessionalDetails.Add(obj);
            //dbcontext.SaveChanges();

            return Id;
        }




        /// <summary>
        /// Update User's Professional Details for User Profile in UserProfessionalDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Professional Details as its properties </param>
        /// <returns>It returns Id of new User's Professional Details entry from UserProfessionalDetail Table </returns>
        /// 
        public int updateUserProfessionalDetail(MODEL.UserProfileModel.UserProfessionalDetail model)
        {
            int Id;
            using (var con = new SqlConnection(ConnectionString))
            {

                Id = con.Query<int>("Usp_InsertUpdateProfessionalDetail", new
                {
                    Id = model.Id,
                    CompanyName = model.CompanyName,
                    Title = model.Title,
                    From = model.From,
                    To = model.To,
                    Location = model.Location,
                    Description = model.Description,
                    UserId = model.UserId,
                    IsInsertOrUpdate = 2
                }, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }



            //var isUserProfessionalDetailExist = (from usrpro in dbcontext.UserProfessionalDetails
            //                                     where usrpro.Id == model.Id && usrpro.IsDeleted == false
            //                                     select usrpro).SingleOrDefault();

            //isUserProfessionalDetailExist.CompanyName = model.CompanyName;
            //isUserProfessionalDetailExist.From = model.From;
            //isUserProfessionalDetailExist.To = model.To;
            //isUserProfessionalDetailExist.Title = model.Title;
            //isUserProfessionalDetailExist.Location = model.Location;
            //isUserProfessionalDetailExist.Description = model.Description;

            //dbcontext.SaveChanges();

            return Id;
        }





        /// <summary>
        /// Save User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Education Details as its properties </param>
        /// <returns>It returns Id of new User's Education Details entry in UserEducationDetail Table </returns>
        /// 
        public string saveUserEducationDetail(MODEL.UserProfileModel.UserEducationDetail model)
        {

            using (var con = new SqlConnection(ConnectionString))
            {

                con.Query("Ups_InsertUpdateEduDetails", new
                {
                    Id = model.Id,
                    School = model.School,
                    DateAttendedFrom = model.DateAttendedFrom,
                    DateAttendedTo = model.DateAttendedTo,
                    Degree = model.Degree,
                    CommaSeperatedEducationalStream = model.CommaSeperatedEducationalStream,
                    Grade = model.Grade,
                    ActivitiesAndSocieties = model.ActivitiesAndSocieties,
                    Description = model.Description,
                    UserId = model.UserId,
                    IsInsertOrUpdate = 1
                }, commandType: CommandType.StoredProcedure);
            }


            //UserEducationDetail obj = new UserEducationDetail
            //{
            //    School = model.School,
            //    DateAttendedFrom = model.DateAttendedFrom,
            //    DateAttendedTo = model.DateAttendedTo,
            //    Degree = model.Degree,
            //    EducationalStream = model.CommaSeperatedEducationalStream,
            //    Grade = model.Grade,
            //    ActivitiesAndSocieties = model.ActivitiesAndSocieties,
            //    Description = model.Description,
            //    UserId = model.UserId,
            //};

            //dbcontext.UserEducationDetails.Add(obj);
            //dbcontext.SaveChanges();

            return "New record for Education Detail is saved Successfully !";
        }




        /// <summary>
        /// Update User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Education Details as its properties </param>
        /// <returns>It returns Id of new User's Education Details entry from UserEducationDetail Table </returns>
        /// 
        public int updateUserEducationDetail(MODEL.UserProfileModel.UserEducationDetail model)
        {
            int Id;

            using (var con = new SqlConnection(ConnectionString))
            {

                Id = con.Query<int>("Ups_InsertUpdateEduDetails", new
                {
                    Id = model.Id,
                    School = model.School,
                    DateAttendedFrom = model.DateAttendedFrom,
                    DateAttendedTo = model.DateAttendedTo,
                    Degree = model.Degree,
                    CommaSeperatedEducationalStream = model.CommaSeperatedEducationalStream,
                    Grade = model.Grade,
                    ActivitiesAndSocieties = model.ActivitiesAndSocieties,
                    Description = model.Description,
                    UserId = model.UserId,
                    IsInsertOrUpdate = 2
                }, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }

            //var isUserEducationDetailExist = (from usrpro in dbcontext.UserEducationDetails
            //                                  where usrpro.Id == model.Id
            //                                  && usrpro.IsDeleted == false
            //                                  select usrpro).SingleOrDefault();


            //isUserEducationDetailExist.School = model.School;
            //isUserEducationDetailExist.DateAttendedFrom = model.DateAttendedFrom;
            //isUserEducationDetailExist.DateAttendedTo = model.DateAttendedTo;
            //isUserEducationDetailExist.Degree = model.Degree;
            //isUserEducationDetailExist.EducationalStream = model.CommaSeperatedEducationalStream;
            //isUserEducationDetailExist.Grade = model.Grade;
            //isUserEducationDetailExist.ActivitiesAndSocieties = model.ActivitiesAndSocieties;
            //isUserEducationDetailExist.Description = model.Description;


            dbcontext.SaveChanges();

            return Id;
        }



        /// <summary>
        /// Delete User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <param name="EducationDetailObjectId">It is an object which contain info for Education Details as its properties </param>
        /// <returns>It returns the confirmation message for User's Education Details entry from UserEducationDetail Table </returns>
        /// 
        public void deleteUserEducationDetail(int EducationDetailObjectId)
        {
            string query = @"update UserEducationDetails set IsDeleted = 1 where Id = @Id";


            using (var con = new SqlConnection(ConnectionString))
            {

                con.Query(query, new { Id = EducationDetailObjectId });

            }



            //        var UserEducationDetailss = (from usrpro in dbcontext.UserEducationDetails
            //                             where usrpro.Id == EducationDetailObjectId
            //                             && usrpro.IsDeleted == false
            //                             select usrpro).SingleOrDefault();

            ////dbcontext.UserEducationDetails.Remove(UserEducationDetail);
            //UserEducationDetailss.IsDeleted = true;

            //dbcontext.SaveChanges();
        }




        /// <summary>
        /// Delete User's Professional Details for User Profile in UserProfessionalDetail Table
        /// </summary>
        /// <param name="ProfessionalDetailObjectId">It is an object which contain info for Professional Details as its properties </param>
        /// <returns>It returns the confirmation message for User's Professional Details entry from UserProfessionalDetail Table </returns>
        /// 
        public void deleteUserProfessionalDetail(int ProfessionalDetailObjectId)
        {

            string query = @"update UserProfessionalDetails set IsDeleted = 1 where Id = @Id";


            using (var con = new SqlConnection(ConnectionString))
            {

                con.Query(query, new { Id = ProfessionalDetailObjectId });

            }




            //var UserProfessionalDetailss = (from usrpro in dbcontext.UserProfessionalDetails
            //                                where usrpro.Id == ProfessionalDetailObjectId
            //                                && usrpro.IsDeleted == false
            //                                select usrpro).SingleOrDefault();


            ////dbcontext.UserProfessionalDetails.Remove(UserProfessionalDetail);
            //UserProfessionalDetailss.IsDeleted = true;
            //dbcontext.SaveChanges();

        }





        /// <summary>
        /// Update User's Education Details for User Profile in UserEducationDetail Table
        /// </summary>
        /// <param name="model">It is an object which contain info for Education Details as its properties </param>
        /// <returns>It returns Id of new User's Education Details entry from UserEducationDetail Table </returns>
        /// 
        public string updateUserHourlyRate(string UserId, decimal newHourlyRate)
        {


          string query = "update UserDetails  set  Rate = @newHourlyRate  where UserId = @UserId";
            using (var con = new SqlConnection(ConnectionString))
            {


                con.Query(query, new { UserId = UserId, newHourlyRate = newHourlyRate });

            }



            //    var user = (from usr in dbcontext.UserDetails
            //                where usr.UserId == UserId && usr.IsDeleted == false
            //                select usr).SingleOrDefault();
            //user.Rate = newHourlyRate;
            //dbcontext.SaveChanges();

            return "Hourly Rate is updated Successfully !";
        }



        /// <summary>
        ///  Update User's Availabilty for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User </param>
        /// <returns>It returns the new Availabilty </returns>
        public string updateUserAvailabilty(string UserId, decimal AvailableHours)
        {



            string query = "update UserDetails  set  Availabilty = @AvailableHours  where UserId = @UserId  and IsDeleted = 0";
            using (var con = new SqlConnection(ConnectionString))
            {


                con.Query(query, new { UserId = UserId, AvailableHours = AvailableHours });

            }




            var user = (from usr in dbcontext.UserDetails
                        where usr.UserId == UserId && usr.IsDeleted == false
                        select usr).SingleOrDefault();
            user.Availabilty = AvailableHours;
            dbcontext.SaveChanges();

            return "User Availability is updated Successfully !";
        }




        /// <summary>
        /// Getting User's Favourite Projects for User Profile From UserDetails Table
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Favourite Projects as an array</returns>
        public ProjectModel.UserFavouriteProjects getUserFavouriteProjects(string UserId)
        {

            ProjectModel.UserFavouriteProjects UserFavouriteProjects = new ProjectModel.UserFavouriteProjects();
            List<ProjectModel.Project> userProjectsForAllOrgs;
            List<ProjectModel.Project> userFavProjects;


            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("USp_GetUserFavouriteProjects", new { UserId = UserId }, commandType: CommandType.StoredProcedure);

                userProjectsForAllOrgs = Result.Read<ProjectModel.Project>().ToList();
                userFavProjects = Result.Read<ProjectModel.Project>().ToList();


             }





            //ProjectModel.UserFavouriteProjects UserFavouriteProjects = new ProjectModel.UserFavouriteProjects();

            //userProjectsForAllOrgs = (from p1 in dbcontext.Projects
            //                          join pu1 in dbcontext.ProjectUsers on p1.Id equals pu1.ProjectId
            //                          where pu1.UserId == UserId && p1.IsComplete == false
            //                          && p1.IsDeleted == false
            //                          select new ProjectModel.Project
            //                          {
            //                              ProjectId = p1.Id,
            //                              ProjectName = p1.Name,
            //                              ProjectDescription = p1.Description
            //                          }).ToList();




            //var FavProjects = (from usr in dbcontext.UserDetails where usr.UserId == UserId && usr.IsDeleted == false select usr.FavProjects).SingleOrDefault();

            //if (FavProjects != null && FavProjects != "")
            //{

            //    int[] FavProjectsArray = FavProjects.Split(',').Select(str => int.Parse(str)).ToArray();

            //    userFavProjects = (from m1 in dbcontext.Projects
            //                       where FavProjectsArray.Contains(m1.Id)
            //                       && m1.IsDeleted == false
            //                       select new ProjectModel.Project
            //                       {
            //                           ProjectId = m1.Id,
            //                           ProjectName = m1.Name,
            //                           ProjectDescription = m1.Description

            //                       }).ToList();

            //}


            UserFavouriteProjects.userProjectsForAllOrgs = userProjectsForAllOrgs;
            UserFavouriteProjects.userFavProjects = userFavProjects;

            return UserFavouriteProjects;
        }




        /// <summary>
        /// Getting User's Availabilty for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Availabilty</returns>
        public decimal getUserAvailabilty(string UserId)
        {

            var user = (from usr in dbcontext.UserDetails
                        where usr.UserId == UserId && usr.IsDeleted == false
                        select usr.Availabilty).SingleOrDefault();


            return user.HasValue ? user.Value : 0;

        }




        /// <summary>
        /// Getting User's Favourite Projects for User Profile From UserDetails Table
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Favourite Projects as an array</returns>
        public string saveUserFavouriteProjects(MODEL.UserProfileModel.SaveFavoriteProjectsModel model)
        {
            string favProjects = string.Empty;
            foreach (var item in model.FavouriteProjectsId)
            {
                if (favProjects == string.Empty)
                {
                    favProjects = item.ProjectId.ToString();
                }
                else
                {
                    favProjects += "," + item.ProjectId.ToString();
                }
            }

            var user = (from usr in dbcontext.UserDetails
                        where usr.UserId == model.UserId && usr.IsDeleted == false
                        select usr).SingleOrDefault();

            user.FavProjects = favProjects;
            dbcontext.SaveChanges();

            return "Favorite Projects are saved Successfully !";
        }

        public UserProfileModel.UserStatistics getUserStatistics(string UserId, string CurrentUserId)
        {

         

            int TotalProjectsCount;
            string[] HoursForAllOrg;

            List<MODEL.UserProfileModel.UserOrganization> UserOrganization;
            List<MODEL.UserProfileModel.TotalHours> TotalHoursListForOrg;
            List<MODEL.UserProfileModel.UserProject> UserProjects;
            List<MODEL.UserProfileModel.UserIds> UserIdOfTeamUsers;
            List<MODEL.UserProfileModel.TotalHours> TotalHoursListForProject;

            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("Usp_GetUserStatistics", new { UserId = UserId, }, commandType: CommandType.StoredProcedure);
                {

                    TotalProjectsCount = result.Read<int>().FirstOrDefault();
                    HoursForAllOrg = result.Read<string>().ToArray();
                    UserOrganization = result.Read<MODEL.UserProfileModel.UserOrganization>().ToList();
                    TotalHoursListForOrg = result.Read<MODEL.UserProfileModel.TotalHours>().ToList();
                    UserProjects = result.Read<MODEL.UserProfileModel.UserProject>().ToList();
                    UserIdOfTeamUsers = result.Read<MODEL.UserProfileModel.UserIds>().ToList();
                    TotalHoursListForProject = result.Read<MODEL.UserProfileModel.TotalHours>().ToList();

                }


                foreach (var Option in UserOrganization)
                {
                    Option.TotalHoursListForOrg = TotalHoursListForOrg.Where(x => x.CompanyId == Option.CompanyId).ToList();

                }

                foreach (var option in UserProjects)
                {
                    option.UserIdOfTeamUsers = UserIdOfTeamUsers.Where(x => x.ProjectId == option.ProjectId).ToList();
                    option.TotalHoursListForProject = TotalHoursListForProject.Where(x => x.ProjectId == option.ProjectId).ToList();

                }



            }



            #region Calculate TotalHoursForAllOrg

            int TotalMinutesForTodos = 0;
            foreach (var item in HoursForAllOrg)
            {
                int LoggedTimeInMinutes = Utility.ConvertTimeLogStringToFormatedMinutes(item);

                TotalMinutesForTodos += LoggedTimeInMinutes;
            }

            int TotalHours = (TotalMinutesForTodos / 60);
            int TotalMinutes = (TotalMinutesForTodos % 60);

            if (TotalMinutes > 30)
            {
                TotalHours += 1;
            }


            //string TotalHoursForAllOrg = TotalHours +"h "+ TotalMinutes + "m" ;
            string TotalHoursForAllOrg = TotalHours + "h";

            #endregion



            foreach (var UserOrganizationItem in UserOrganization)
            {
                #region Calculate TotalHoursForOrg

                int TotalMinutesFromStringForOrgTodos = 0;
                foreach (var TotalHoursListForOrgItem in UserOrganizationItem.TotalHoursListForOrg)
                {
                    int LoggedTimeInMinutesForOrgTodos = Utility.ConvertTimeLogStringToFormatedMinutes(TotalHoursListForOrgItem.hours);

                    TotalMinutesFromStringForOrgTodos += LoggedTimeInMinutesForOrgTodos;
                }

                int TotalHoursForOrgTodos = (TotalMinutesFromStringForOrgTodos / 60);
                int TotalMinutesForOrgTodos = (TotalMinutesFromStringForOrgTodos % 60);

                //string TotalLoggedHoursForOrgTodos = TotalHoursForOrgTodos + "h " + TotalMinutesForOrgTodos + "m";
                if (TotalMinutesForOrgTodos > 30)
                {
                    TotalHoursForOrgTodos += 1;
                }
                string TotalLoggedHoursForOrgTodos = TotalHoursForOrgTodos + "h";

                UserOrganizationItem.TotalHoursForOrg = TotalLoggedHoursForOrgTodos;

                #endregion

            }




            foreach (var ProjectListItem in UserProjects)
            {

                #region Calculate TotalHoursForProject

                int TotalMinutesFromStringForProjectTodos = 0;
                foreach (var TotalHoursListForProjectItem in ProjectListItem.TotalHoursListForProject)
                {
                    int LoggedTimeInMinutesForProjectTodos = Utility.ConvertTimeLogStringToFormatedMinutes(TotalHoursListForProjectItem.hours);

                    TotalMinutesFromStringForProjectTodos += LoggedTimeInMinutesForProjectTodos;
                }

                int TotalHoursForProjectTodos = (TotalMinutesFromStringForProjectTodos / 60);
                int TotalMinutesForProjectTodos = (TotalMinutesFromStringForProjectTodos % 60);
                if (TotalMinutesForProjectTodos > 30)
                {
                    TotalHoursForProjectTodos += 1;
                }
                //string TotalLoggedHoursForProjectTodos = TotalHoursForProjectTodos + "h " + TotalMinutesForProjectTodos + "m";
                string TotalLoggedHoursForProjectTodos = TotalHoursForProjectTodos + "h";

                ProjectListItem.TotalHoursForProject = TotalLoggedHoursForProjectTodos;


                bool IsTeamMember = false;
                foreach (var TeamMemberItem in ProjectListItem.UserIdOfTeamUsers)
                {
                    if (TeamMemberItem.UserId == CurrentUserId)
                    {
                        IsTeamMember = true;
                        break;
                    }
                }

                ProjectListItem.IsTeamMember = IsTeamMember;
                #endregion

            }




            MODEL.UserProfileModel.UserStatistics ReturnModel = new UserProfileModel.UserStatistics();
            ReturnModel.TotalProjectsCount = TotalProjectsCount;
            ReturnModel.TotalHoursForAllOrg = TotalHoursForAllOrg;
            ReturnModel.UserOrg = UserOrganization;
            ReturnModel.UserProjects = UserProjects;



            return ReturnModel;
        }




        /// <summary>
        /// Getting User's Statistics for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Statistics  as an array</returns>
       


        public MODEL.UserProfileModel.TodoEfficiencyMeter getUserEfficiency(string UserId, string CurrentUserId)
        {
           
            List<MODEL.UserProfileModel.TodoEfficiency> TodoEfficiencyList;
            List<MODEL.UserProfileModel.TodoEfficiency> ProjectList1;
            List<MODEL.UserProfileModel.TodoEfficiency> ProjectTodos;
            List<MODEL.UserProfileModel.UserOrganizationForEfficiency> UserOrganization;
         //   List<MODEL.UserProfileModel.TodoEfficiency> EfficiencyList;
          
            List<MODEL.UserProfileModel.TodoEfficiency> TodoEfficiencyListForOrg;
            List<MODEL.UserProfileModel.UserProjectForEfficiency> UserProjects;
            List<MODEL.UserProfileModel.UserIds> UserIdOfTeamUsers;
            List<MODEL.UserProfileModel.TodoEfficiency> TotalEfficiencyListForProject;

            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("Usp_GetUserEfficiency", new
                { UserId = UserId }, commandType: CommandType.StoredProcedure);

                TodoEfficiencyList = result.Read<MODEL.UserProfileModel.TodoEfficiency>().ToList();
                ProjectList1 = result.Read<MODEL.UserProfileModel.TodoEfficiency>().ToList(); // for match and use
                ProjectTodos = result.Read<MODEL.UserProfileModel.TodoEfficiency>().ToList(); // for match and use
                //EfficiencyList = result.Read<MODEL.UserProfileModel.TodoEfficiency>().ToList();
              
                UserOrganization = result.Read<MODEL.UserProfileModel.UserOrganizationForEfficiency>().ToList();
                TodoEfficiencyListForOrg = result.Read<MODEL.UserProfileModel.TodoEfficiency>().ToList();
                UserProjects = result.Read<MODEL.UserProfileModel.UserProjectForEfficiency>().ToList();
                UserIdOfTeamUsers = result.Read<MODEL.UserProfileModel.UserIds>().ToList();
                TotalEfficiencyListForProject = result.Read<MODEL.UserProfileModel.TodoEfficiency>().ToList();
                
            }



            List<MODEL.UserProfileModel.ProjectEfficiency> ProjectList = new List<UserProfileModel.ProjectEfficiency>();

            if(UserOrganization != null)
            {
                foreach (var item in UserOrganization)
                    item.TodoEfficiencyListForOrg = TodoEfficiencyListForOrg;


            }

            if(UserProjects != null)
            {
                foreach(var item in UserProjects)
                {

                    item.TotalEfficiencyListForProject = TotalEfficiencyListForProject.Where(x => x.ProjectId == item.ProjectId).ToList();
                    item.UserIdOfTeamUsers = UserIdOfTeamUsers.Where(x => x.ProjectId == item.ProjectId).ToList();
                }


            }

            #region -------------------TodoEfficiencyList
            decimal TotalEfficiency = 0;
            int EfficiencyCount = 0;
            decimal NetEfficiency = 0;

            foreach (var item in TodoEfficiencyList)
            {
                decimal TodoEstimatedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoEstimatedTime);

                decimal TodoLoggedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoLoggedTime);

                if (TodoLoggedTime > 0)
                {
                    decimal Efficiency = (TodoEstimatedTime / TodoLoggedTime) * 100;

                    TotalEfficiency += Efficiency;
                    EfficiencyCount += 1;
                }
            }

            if (EfficiencyCount > 0)
            {
                NetEfficiency = Math.Round(TotalEfficiency / EfficiencyCount, 2);
            }

            #endregion

            #region ------------------------ProjectList


            foreach (var option in ProjectList)
            {
                //option.ProjectTodos = EfficiencyList.Where(x => x.ProjectIdTodoEff == option.ProjectId).ToList();

            }


            int ProjectCount = 0;

            foreach (var item in ProjectList)
            {
                int EstimatedTime = 0;
                int LoggedTime = 0;
           
                foreach (var TodoItem in item.ProjectTodos)
                {
                    int TodoEstimatedTime = Utility.ConvertTimeLogStringToFormatedMinutes(TodoItem.TodoEstimatedTime);
                    int TodoLoggedTime = Utility.ConvertTimeLogStringToFormatedMinutes(TodoItem.TodoLoggedTime);
                    EstimatedTime += TodoEstimatedTime;
                    LoggedTime += TodoLoggedTime;
                }

                if (EstimatedTime >= LoggedTime)
                {
                    ProjectCount += 1;
                }
            }

            #endregion

            #region --UserOrganization --TodoEfficiencyListForOrg


      
            UserOrganization[0].TodoEfficiencyListForOrg = TodoEfficiencyListForOrg;


            #endregion

            #region 



            foreach (var option in UserProjects)
            {
                option.UserIdOfTeamUsers = UserIdOfTeamUsers.Where(x => x.ProjectId == option.ProjectId).ToList();
                option.TotalEfficiencyListForProject = TotalEfficiencyListForProject.Where(x => x.ProjectId == option.ProjectId).ToList();


            }
            
            #endregion

            foreach (var UserOrganizationItem in UserOrganization)
            {
                #region Calculate TotalEfficiencyForOrg

                decimal TotalEfficiencyForOrgTodos = 0;
                int EfficiencyCountForOrgTodos = 0;
                decimal NetEfficiencyForOrgTodos = 0;

                foreach (var item in UserOrganizationItem.TodoEfficiencyListForOrg)
                {
                    decimal TodoEstimatedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoEstimatedTime);

                    decimal TodoLoggedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoLoggedTime);

                    if (TodoLoggedTime > 0)
                    {
                        decimal Efficiency = (TodoEstimatedTime / TodoLoggedTime) * 100;

                        TotalEfficiencyForOrgTodos += Efficiency;
                        EfficiencyCountForOrgTodos += 1;
                    }
                }

                if (EfficiencyCountForOrgTodos > 0)
                {
                    NetEfficiencyForOrgTodos = Math.Round(TotalEfficiencyForOrgTodos / EfficiencyCountForOrgTodos, 2);
                }


                UserOrganizationItem.TotalEfficiencyForOrg = NetEfficiencyForOrgTodos;

                #endregion

            }

            foreach (var ProjectListItem in UserProjects)
            {

                #region Calculate TotalEfficiencyForProject

                decimal TotalEfficiencyForProject = 0;
                int EfficiencyCountForProject = 0;
                decimal NetEfficiencyForProject = 0;

                foreach (var item in ProjectListItem.TotalEfficiencyListForProject)
                {
                    decimal TodoEstimatedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoEstimatedTime);

                    decimal TodoLoggedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoLoggedTime);

                    if (TodoLoggedTime > 0)
                    {
                        decimal Efficiency = (TodoEstimatedTime / TodoLoggedTime) * 100;

                        TotalEfficiencyForProject += Efficiency;
                        EfficiencyCountForProject += 1;
                    }

                }

                bool IsTeamMember = false;
                foreach (var TeamMemberItem in ProjectListItem.UserIdOfTeamUsers)
                {
                    if (TeamMemberItem.UserId == CurrentUserId)
                    {
                        IsTeamMember = true;
                        break;
                    }
                }

                ProjectListItem.IsTeamMember = IsTeamMember;


                if (EfficiencyCountForProject > 0)
                {
                    NetEfficiencyForProject = Math.Round(TotalEfficiencyForProject / EfficiencyCountForProject, 2);
                }

                ProjectListItem.TotalEfficiencyForProject = NetEfficiencyForProject;

                #endregion


            }

            MODEL.UserProfileModel.TodoEfficiencyMeter ReturnModel = new UserProfileModel.TodoEfficiencyMeter();
            ReturnModel.ProjectsCountCompletedInTimeForAllOrgs = ProjectCount;
            ReturnModel.TodoEfficiencyInPercentForAllOrgs = NetEfficiency;
            ReturnModel.UserOrg = UserOrganization;
            ReturnModel.UserProjects = UserProjects;

            return ReturnModel;
        }





        /// <summary>
        /// Getting User's Efficiency for User Profile  
        /// </summary>
        #region public MODEL.UserProfileModel.TodoEfficiencyMeter getUserEfficiency1
        public MODEL.UserProfileModel.TodoEfficiencyMeter getUserEfficiency1(string UserId, string CurrentUserId)
        {
            // testing purpose writing string
            AppContext dbcontext = new AppContext();
            List<MODEL.UserProfileModel.TodoEfficiency> TodoEfficiencyList = (from t in dbcontext.ToDos
                                                                              where t.AssigneeId == UserId && t.IsDeleted == false
                                                                              select new MODEL.UserProfileModel.TodoEfficiency
                                                                              {
                                                                                  TodoEstimatedTime = t.EstimatedTime,
                                                                                  TodoLoggedTime = t.LoggedTime
                                                                              }).ToList();
            decimal TotalEfficiency = 0;
            int EfficiencyCount = 0;
            decimal NetEfficiency = 0;

            foreach (var item in TodoEfficiencyList)
            {
                decimal TodoEstimatedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoEstimatedTime);

                decimal TodoLoggedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoLoggedTime);

                if (TodoLoggedTime > 0)
                {
                    decimal Efficiency = (TodoEstimatedTime / TodoLoggedTime) * 100;

                    TotalEfficiency += Efficiency;
                    EfficiencyCount += 1;
                }
            }

            if (EfficiencyCount > 0)
            {
                NetEfficiency = Math.Round(TotalEfficiency / EfficiencyCount, 2);
            }


            #region ProjectsCountCompletedInTime

            var projects = (from prjct in dbcontext.Projects
                            join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId
                            where prjctusr.UserId == UserId && prjct.IsDeleted == false
                            //    && prjct.IsComplete == true
                            select new
                            {
                                ProjectId = prjct.Id,
                                ProjectTodos = (from t in dbcontext.ToDos
                                                join s in dbcontext.Sections on t.SectionId equals s.Id
                                                where s.ProjectId == prjct.Id && t.IsDeleted == false
                                                && t.AssigneeId == UserId
                                                select new MODEL.UserProfileModel.TodoEfficiency
                                                {
                                                    TodoEstimatedTime = t.EstimatedTime,
                                                    TodoLoggedTime = t.LoggedTime
                                                })
                            }).ToList();

            List<MODEL.UserProfileModel.ProjectEfficiency> ProjectList = projects.AsEnumerable().Select(x =>
                new MODEL.UserProfileModel.ProjectEfficiency
                {
                    ProjectId = x.ProjectId,
                    ProjectTodos = x.ProjectTodos.ToList()
                }
                ).ToList();

            int ProjectCount = 0;

            foreach (var item in ProjectList)
            {
                int EstimatedTime = 0;
                int LoggedTime = 0;

                foreach (var TodoItem in item.ProjectTodos)
                {
                    int TodoEstimatedTime = Utility.ConvertTimeLogStringToFormatedMinutes(TodoItem.TodoEstimatedTime);
                    int TodoLoggedTime = Utility.ConvertTimeLogStringToFormatedMinutes(TodoItem.TodoLoggedTime);
                    EstimatedTime += TodoEstimatedTime;
                    LoggedTime += TodoLoggedTime;
                }

                if (EstimatedTime >= LoggedTime)
                {
                    ProjectCount += 1;
                }
            }

            #endregion 


            var TotalOrgsForUser = (from usrdetail in dbcontext.UserDetails
                                    join ucRole in dbcontext.UserCompanyRoles on usrdetail.CompanyId equals ucRole.CompanyId
                                    join cmp in dbcontext.Companies on ucRole.CompanyId equals cmp.Id
                                    where ucRole.UserId == UserId && usrdetail.IsDeleted == false
                                    //&& usrdetail.UserId != UserId
                                     && usrdetail.ParentUserId == null
                                    select new
                                    {
                                        TodoEfficiencyListForOrg = /*(from Membrs in*/
                                                                     (from t1 in dbcontext.ToDos
                                                                      join sec_2 in dbcontext.Sections on t1.SectionId equals sec_2.Id
                                                                      join prjct_2 in dbcontext.Projects on sec_2.ProjectId equals prjct_2.Id
                                                                      //join prjctusr_2 in dbcontext.ProjectUsers on prjct_2.Id equals prjctusr_2.ProjectId
                                                                      join usrdet_2 in dbcontext.UserDetails on t1.AssigneeId equals usrdet_2.UserId
                                                                      where prjct_2.CompanyId == cmp.Id && usrdet_2.IsDeleted == false
                                                                      && t1.AssigneeId == UserId && prjct_2.IsDeleted == false
                                                                      && t1.IsDeleted == false
                                                                      select new MODEL.UserProfileModel.TodoEfficiency
                                                                      {
                                                                          TodoEstimatedTime = t1.EstimatedTime,
                                                                          TodoLoggedTime = t1.LoggedTime
                                                                      }),
                                        //group Membrs by new { Membrs.t1.EstimatedTime, Membrs.t1.LoggedTime } into TodoMembrs
                                        //select new MODEL.UserProfileModel.TodoEfficiency
                                        //{
                                        //    TodoEstimatedTime = TodoMembrs.Key.EstimatedTime,
                                        //    TodoLoggedTime = TodoMembrs.Key.LoggedTime
                                        //}),

                                        CompanyId = cmp.Id,
                                        CompanyName = cmp.Name,
                                        CompanyProfilePhoto = cmp.Logo == null ? "Uploads/Default/nologo.png" : "Uploads/CompanyLogos/" + cmp.Logo

                                    }).ToList();


            List<MODEL.UserProfileModel.UserOrganizationForEfficiency> UserOrganization = TotalOrgsForUser.AsEnumerable().Select(x =>
            new MODEL.UserProfileModel.UserOrganizationForEfficiency
            {
                CompanyId = x.CompanyId,
                CompanyName = x.CompanyName,
                CompanyProfilePhoto = x.CompanyProfilePhoto,
                TodoEfficiencyListForOrg = x.TodoEfficiencyListForOrg.ToList()
            }).ToList();


            int[] CmpId = UserOrganization.Select(str => str.CompanyId).ToArray();



            var AllProjectList = (from p2 in dbcontext.Projects
                                  join pu2 in dbcontext.ProjectUsers on p2.Id equals pu2.ProjectId
                                  where CmpId.Contains(p2.CompanyId) && p2.IsDeleted == false
                                  && pu2.UserId == UserId
                                  select new
                                  {
                                      ProjectName = p2.Name,
                                      ProjectAccessLevel = p2.AccessLevel.HasValue ? p2.AccessLevel.Value : 0,
                                      UserIdOfTeamUsers = (from prjctusrfortem in dbcontext.ProjectUsers
                                                           where prjctusrfortem.ProjectId == p2.Id
                                                           select new MODEL.UserProfileModel.UserIds
                                                           {
                                                               UserId = prjctusrfortem.UserId
                                                           }
                                                           ),
                                      CompanyId = p2.CompanyId,
                                      TotalEfficiencyListForProject = (from t3 in dbcontext.ToDos
                                                                       join s3 in dbcontext.Sections on t3.SectionId equals s3.Id
                                                                       where t3.AssigneeId == UserId && t3.IsDeleted == false
                                                                       && s3.ProjectId == p2.Id
                                                                       select new MODEL.UserProfileModel.TodoEfficiency
                                                                       {
                                                                           TodoEstimatedTime = t3.EstimatedTime,
                                                                           TodoLoggedTime = t3.LoggedTime
                                                                       })

                                  }).ToList();




            List<MODEL.UserProfileModel.UserProjectForEfficiency> UserProjects = AllProjectList.AsEnumerable().Select(y =>
            new MODEL.UserProfileModel.UserProjectForEfficiency
            {
                UserIdOfTeamUsers = y.UserIdOfTeamUsers.ToList(),
                ProjectName = y.ProjectName,
                ProjectAccessLevel = y.ProjectAccessLevel,
                CompanyId = y.CompanyId,
                TotalEfficiencyListForProject = y.TotalEfficiencyListForProject.ToList()
            }).ToList();




            foreach (var UserOrganizationItem in UserOrganization)
            {
                #region Calculate TotalEfficiencyForOrg

                decimal TotalEfficiencyForOrgTodos = 0;
                int EfficiencyCountForOrgTodos = 0;
                decimal NetEfficiencyForOrgTodos = 0;

                foreach (var item in UserOrganizationItem.TodoEfficiencyListForOrg)
                {
                    decimal TodoEstimatedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoEstimatedTime);

                    decimal TodoLoggedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoLoggedTime);

                    if (TodoLoggedTime > 0)
                    {
                        decimal Efficiency = (TodoEstimatedTime / TodoLoggedTime) * 100;

                        TotalEfficiencyForOrgTodos += Efficiency;
                        EfficiencyCountForOrgTodos += 1;
                    }
                }

                if (EfficiencyCountForOrgTodos > 0)
                {
                    NetEfficiencyForOrgTodos = Math.Round(TotalEfficiencyForOrgTodos / EfficiencyCountForOrgTodos, 2);
                }


                UserOrganizationItem.TotalEfficiencyForOrg = NetEfficiencyForOrgTodos;

                #endregion

            }




            foreach (var ProjectListItem in UserProjects)
            {

                #region Calculate TotalEfficiencyForProject

                decimal TotalEfficiencyForProject = 0;
                int EfficiencyCountForProject = 0;
                decimal NetEfficiencyForProject = 0;

                foreach (var item in ProjectListItem.TotalEfficiencyListForProject)
                {
                    decimal TodoEstimatedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoEstimatedTime);

                    decimal TodoLoggedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoLoggedTime);

                    if (TodoLoggedTime > 0)
                    {
                        decimal Efficiency = (TodoEstimatedTime / TodoLoggedTime) * 100;

                        TotalEfficiencyForProject += Efficiency;
                        EfficiencyCountForProject += 1;
                    }

                }

                bool IsTeamMember = false;
                foreach (var TeamMemberItem in ProjectListItem.UserIdOfTeamUsers)
                {
                    if (TeamMemberItem.UserId == CurrentUserId)
                    {
                        IsTeamMember = true;
                        break;
                    }
                }

                ProjectListItem.IsTeamMember = IsTeamMember;


                if (EfficiencyCountForProject > 0)
                {
                    NetEfficiencyForProject = Math.Round(TotalEfficiencyForProject / EfficiencyCountForProject, 2);
                }

                ProjectListItem.TotalEfficiencyForProject = NetEfficiencyForProject;

                #endregion


            }





            MODEL.UserProfileModel.TodoEfficiencyMeter ReturnModel = new UserProfileModel.TodoEfficiencyMeter();
            ReturnModel.ProjectsCountCompletedInTimeForAllOrgs = ProjectCount;
            ReturnModel.TodoEfficiencyInPercentForAllOrgs = NetEfficiency;
            ReturnModel.UserOrg = UserOrganization;
            ReturnModel.UserProjects = UserProjects;

            return ReturnModel;
        }

        #endregion


        /// <summary>
        /// Getting User's Efficiency for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Efficiency as an array</returns>
        public MODEL.UserProfileModel.TechnologyList getUserEfficiencyForStrongAndWeekTechnology(string UserId)
        {

            List<MODEL.UserProfileModel.TodoEfficiency> TodoEfficiencyList;
            List<MODEL.UserProfileModel.TagsList> GetAllMasterTagId;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("Usp_GetUserEfficiencyForStrongAndWeekTechnology", new { UserId = UserId }, commandType: CommandType.StoredProcedure);


                TodoEfficiencyList = result.Read<MODEL.UserProfileModel.TodoEfficiency>().ToList();
                GetAllMasterTagId = result.Read<MODEL.UserProfileModel.TagsList>().ToList();



            }






            //List<MODEL.UserProfileModel.TodoEfficiency> TodoEfficiencyList = (from t in dbcontext.ToDos
            //                                                              where t.AssigneeId == UserId && t.IsDeleted == false
            //                                                              select new MODEL.UserProfileModel.TodoEfficiency
            //                                                              {
            //                                                                  TodoId = t.Id,
            //                                                                  TodoEstimatedTime = t.EstimatedTime,
            //                                                                  TodoLoggedTime = t.LoggedTime,
            //                                                                  TodoTagIds = t.TodoTaskTypeId
            //                                                              }).ToList();

            List<MODEL.UserProfileModel.TodoEfficiencyWithTags> TodoEfficiencyWithTags = new List<UserProfileModel.TodoEfficiencyWithTags>();

            foreach (var item in TodoEfficiencyList)
            {
                MODEL.UserProfileModel.TodoEfficiencyWithTags TagModel = new UserProfileModel.TodoEfficiencyWithTags();

                decimal TodoEstimatedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoEstimatedTime);
                decimal TodoLoggedTime = Utility.ConvertTimeLogStringToFormatedMinutes(item.TodoLoggedTime);
                if (TodoLoggedTime > 0)
                {
                    decimal Efficiency = (TodoEstimatedTime / TodoLoggedTime) * 100;

                    string alltagids = item.TodoTagIds;
                    int[] UserOwnedTags = null;

                    if (alltagids != null && alltagids != "")
                    {
                        UserOwnedTags = alltagids.Split(',').Select(str => int.Parse(str)).ToArray();
                    }

                    if (UserOwnedTags != null)
                    {
                        foreach (var tagitem in UserOwnedTags)
                        {
                            TagModel.TagId = tagitem;
                            TagModel.TodoEfficiency = Efficiency;
                            TagModel.TodoId = item.TodoId;
                        }
                    }

                    TodoEfficiencyWithTags.Add(TagModel);
                }
            }

            //var GetAllMasterTagId = (from ta1 in dbcontext.Tags
            //                         select ta1).ToList();

            List<MODEL.UserProfileModel.Technology> StrongWeekTechnologyList = new List<UserProfileModel.Technology>();

            foreach (var item in GetAllMasterTagId)
            {
                MODEL.UserProfileModel.Technology StrongWeekTechnology = new UserProfileModel.Technology();

                var tag = from ta in TodoEfficiencyWithTags
                          where ta.TagId == item.Id
                          select ta;

                decimal TotalEfficiency = 0;
                int EfficiencyCount = 0;

                foreach (var effItem in tag)
                {
                    TotalEfficiency += effItem.TodoEfficiency;
                    EfficiencyCount += 1;
                }

                if (EfficiencyCount > 0)
                {
                    decimal NetEfficiency = Math.Round(TotalEfficiency / EfficiencyCount, 2);

                    StrongWeekTechnology.TagName = item.TagName;
                    StrongWeekTechnology.Efficiency = NetEfficiency;
                    StrongWeekTechnologyList.Add(StrongWeekTechnology);
                }

            }

            StrongWeekTechnologyList = StrongWeekTechnologyList.AsEnumerable().OrderByDescending(x => x.Efficiency).ToList();

            MODEL.UserProfileModel.TechnologyList TechnologyList = new UserProfileModel.TechnologyList();

            List<MODEL.UserProfileModel.Technology> StrongTech = new List<MODEL.UserProfileModel.Technology>();
            List<MODEL.UserProfileModel.Technology> weekTech = new List<MODEL.UserProfileModel.Technology>();

            int listCount = 0;
            for (int i = 0; i < StrongWeekTechnologyList.Count(); i++)
            {
                MODEL.UserProfileModel.Technology StrongTechnology = new UserProfileModel.Technology();

                StrongTechnology.TagName = StrongWeekTechnologyList[i].TagName;
                StrongTechnology.Efficiency = StrongWeekTechnologyList[i].Efficiency;
                StrongTech.Add(StrongTechnology);
                listCount += 1;
                if (listCount > 1)
                {
                    break;
                }

            }

            if (StrongWeekTechnologyList.Count() > 2)
            {

                int listCountforweek = 0;
                for (int i = StrongWeekTechnologyList.Count() - 1; i >= 0; i--)
                {
                    MODEL.UserProfileModel.Technology StrongTechnology = new UserProfileModel.Technology();

                    StrongTechnology.TagName = StrongWeekTechnologyList[i].TagName;
                    StrongTechnology.Efficiency = StrongWeekTechnologyList[i].Efficiency;
                    weekTech.Add(StrongTechnology);
                    listCountforweek += 1;
                    if (listCountforweek > 1)
                    {
                        break;
                    }

                }

            }

            TechnologyList.Strong = StrongTech;
            TechnologyList.week = weekTech;

            return TechnologyList;
        }







        /// <summary>
        /// Getting User's Avg Rate for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Avg Rate as an array</returns>
        public decimal getUserAvgRate(string UserId)
        {




  

            List<MODEL.UserProfileModel.UserProjectRate> userproject;
            using (var con = new SqlConnection(ConnectionString))
            {

                userproject = con.Query<MODEL.UserProfileModel.UserProjectRate>("Ups_GetUserAvgRate", new { UserId = UserId }, commandType: CommandType.StoredProcedure).ToList();

            }







            //var userproject1 = (from prjct in dbcontext.Projects
            //                    join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId
            //                    where prjctusr.UserId == UserId && prjct.IsDeleted == false
            //                    select new MODEL.UserProfileModel.UserProjectRate
            //                    {
            //                        ProjectId = prjct.Id,
            //                        userRateForProject = prjctusr.UserRate.HasValue ? prjctusr.UserRate.Value : 0
            //                    }).ToList();

            decimal NetTotalHours = 0;
            int ProjectCount = 0;
            decimal userRate = 0;

            foreach (var item in userproject)
            {
                userRate += item.userRateForProject;
                ProjectCount += 1;
            }

            if (userproject.Count() > 0)
            {
                NetTotalHours = Math.Round(userRate / ProjectCount, 2);
            }


            return NetTotalHours;
        }






        /// <summary>
        /// Getting User's Tags for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Tags as an array</returns>
        public MODEL.UserProfileModel.UserTags getUserTags(string UserId)
        {
            MODEL.UserProfileModel.UserTags TagsList = new UserProfileModel.UserTags();

            List<MODEL.UserProfileModel.Tags> allTagsObject = (from tag in dbcontext.Tags
                                                                   //where tag.TagType == true
                                                               select new MODEL.UserProfileModel.Tags
                                                               {
                                                                   TagId = tag.Id,
                                                                   TagName = tag.TagName
                                                               }).ToList();

            List<MODEL.UserProfileModel.Tags> userTags = new List<UserProfileModel.Tags>();

            var user = (from usr in dbcontext.UserDetails
                        where usr.UserId == UserId && usr.IsDeleted == false
                        select usr.Tags).SingleOrDefault();


            if (user != null && user != string.Empty)
            {
                int[] UserOwnedTags = user.Split(',').Select(str => int.Parse(str)).ToArray();

                userTags = (from tag in dbcontext.Tags
                            where UserOwnedTags.Contains(tag.Id)
                            select new MODEL.UserProfileModel.Tags
                            {
                                TagId = tag.Id,
                                TagName = tag.TagName
                            }).ToList();

            }

            TagsList.allSkillSetTagsObject = allTagsObject;
            TagsList.SkillSetTags = userTags;

            return TagsList;
        }





        /// <summary>
        /// Getting User's Skill Set Tags for User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Skill Set Tags as an array</returns>
        public MODEL.UserProfileModel.UserTags getUserSkillSetTags(string UserId)
        {
            MODEL.UserProfileModel.UserTags TagsList = new UserProfileModel.UserTags();
            List<MODEL.UserProfileModel.Tags> allSkillSetTagsObject;
            List<MODEL.UserProfileModel.Tags> allBusinessDomainObject;
            List<MODEL.UserProfileModel.Tags> SkillSetTags;
            List<MODEL.UserProfileModel.Tags> BusinessDomainExpertiseTags;

            using (var con = new SqlConnection(ConnectionString))
            {

                var result = con.QueryMultiple("Usp_GetUserSkillSetTags", new { UserId = UserId }, commandType: CommandType.StoredProcedure);

                allSkillSetTagsObject = result.Read<MODEL.UserProfileModel.Tags>().ToList();
                allBusinessDomainObject = allSkillSetTagsObject;
                SkillSetTags = result.Read<MODEL.UserProfileModel.Tags>().ToList();
                BusinessDomainExpertiseTags = SkillSetTags;

                if (allSkillSetTagsObject != null)
                {
                    allSkillSetTagsObject = allSkillSetTagsObject.Where(x => x.IsSkillSet == true && (x.IsActive == true || x.IsActive == null)).ToList();
                    allBusinessDomainObject = allBusinessDomainObject.Where(x => x.IsSkillSet == false && (x.IsActive == true || x.IsActive == null)).ToList();
                }

                if (SkillSetTags != null)
                {
                    SkillSetTags = SkillSetTags.Where(x => x.TagType == true && (x.IsActive == true || x.IsActive == null)).ToList();
                    BusinessDomainExpertiseTags = BusinessDomainExpertiseTags.Where(x => x.TagType == false && (x.IsActive != false || x.IsActive == null)).ToList();

                }

            }



            TagsList.allSkillSetTagsObject = allSkillSetTagsObject;
            TagsList.allBusinessDomainObject = allBusinessDomainObject;
            TagsList.SkillSetTags = SkillSetTags;
            TagsList.BusinessDomainExpertiseTags = BusinessDomainExpertiseTags;

            return TagsList;
        }




        // This function is getUserSkillSetTags1  with entity
      public MODEL.UserProfileModel.UserTags getUserSkillSetTags1(string UserId)
            {
                    MODEL.UserProfileModel.UserTags TagsList = new UserProfileModel.UserTags();

                List<MODEL.UserProfileModel.Tags> allSkillSetTagsObject = (from tag in dbcontext.Tags
                                                                           where tag.TagType == true
                                                                           && (tag.IsActive != false || tag.IsActive == null)
                                                                           select new MODEL.UserProfileModel.Tags
                                                                           {
                                                                               TagId = tag.Id,
                                                                               TagName = tag.TagName,
                                                                               IsSkillSet = tag.TagType,
                                                                               TagDescription = tag.TagDescription
                                                                           }).ToList();


                List<MODEL.UserProfileModel.Tags> allBusinessDomainObject =   (from tag in dbcontext.Tags
                                                                               where tag.TagType == false
                                                                               && (tag.IsActive != false || tag.IsActive == null)
                                                                               select new MODEL.UserProfileModel.Tags
                                                                               {
                                                                                   TagId = tag.Id,
                                                                                   TagName = tag.TagName,
                                                                                   IsSkillSet = tag.TagType,
                                                                                   TagDescription = tag.TagDescription
                                                                               }).ToList();


                List<MODEL.UserProfileModel.Tags> SkillSetTags = new List<UserProfileModel.Tags>();

                var user = (from usr in dbcontext.UserDetails
                            where usr.UserId == UserId && usr.IsDeleted == false
                            select usr.Tags).SingleOrDefault();


                if (user != null && user != string.Empty)
                {
                    int[] UserOwnedTags = user.Split(',').Select(str => int.Parse(str)).ToArray();

                    SkillSetTags = (from tag in dbcontext.Tags
                                    where UserOwnedTags.Contains(tag.Id)
                                    where tag.TagType == true
                                     && (tag.IsActive != false || tag.IsActive == null)
                                    select new MODEL.UserProfileModel.Tags
                                    {
                                        TagId = tag.Id,
                                        TagName = tag.TagName,
                                        IsSkillSet = tag.TagType,
                                        TagDescription = tag.TagDescription
                                    }).ToList();

                }

                List<MODEL.UserProfileModel.Tags> BusinessDomainExpertiseTags = new List<UserProfileModel.Tags>();

                var userForBusinessDomainExpertise =   (from usr in dbcontext.UserDetails
                                                        where usr.UserId == UserId && usr.IsDeleted == false
                                                        select usr.Tags).SingleOrDefault();


                if (userForBusinessDomainExpertise != null && userForBusinessDomainExpertise != string.Empty)
                {
                    int[] UserOwnedTagsForBDE = userForBusinessDomainExpertise.Split(',').Select(str => int.Parse(str)).ToArray();

                    BusinessDomainExpertiseTags = (from tag in dbcontext.Tags
                                                    where UserOwnedTagsForBDE.Contains(tag.Id)
                                                    where tag.TagType == false
                                                     && (tag.IsActive != false || tag.IsActive == null)
                                                   select new MODEL.UserProfileModel.Tags
                                                    {
                                                        TagId = tag.Id,
                                                        TagName = tag.TagName,
                                                        IsSkillSet = tag.TagType,
                                                        TagDescription = tag.TagDescription
                                                    }).ToList();

                }


                TagsList.allSkillSetTagsObject = allSkillSetTagsObject;
                TagsList.allBusinessDomainObject = allBusinessDomainObject;
                TagsList.SkillSetTags = SkillSetTags;
                TagsList.BusinessDomainExpertiseTags = BusinessDomainExpertiseTags;

                return TagsList;
            }

            


        /// <summary>
        /// Getting User's Tags for Professional Details in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Tags for Professional Details as an array</returns>
        public MODEL.UserProfileModel.UserTagsForProfessionalDetail getTagsForProfessionalDetails(string UserId)
        {
            MODEL.UserProfileModel.UserTagsForProfessionalDetail TagsList = new UserProfileModel.UserTagsForProfessionalDetail();

            List<MODEL.UserProfileModel.Tags> BusinessDomainExpertise = (from tag in dbcontext.Tags
                                                                         where tag.TagType == false
                                                                         && (tag.IsActive != false || tag.IsActive == null)
                                                                         select new MODEL.UserProfileModel.Tags
                                                                         {
                                                                             TagId = tag.Id,
                                                                             TagName = tag.TagName
                                                                         }).ToList();


            List<MODEL.UserProfileModel.Tags> TechnicalSkillSet = (from tag in dbcontext.Tags
                                                                   where tag.TagType == true
                                                                   && (tag.IsActive != false || tag.IsActive == null)
                                                                   select new MODEL.UserProfileModel.Tags
                                                                   {
                                                                       TagId = tag.Id,
                                                                       TagName = tag.TagName
                                                                   }).ToList();


            TagsList.BusinessDomainExpertise = BusinessDomainExpertise;
            TagsList.TechnicalSkillSet = TechnicalSkillSet;

            return TagsList;
        }




        /// <summary>
        /// Getting User's Tags for User Profile From Tags Table
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Tags as an array</returns>

        public string saveUserTags(MODEL.UserProfileModel.SaveTags model)
        {
            string User;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select u1.Tags from UserDetails u1 where u1.UserId = @UserId and u1.IsDeleted = 0 ";
                User = con.Query<string>(sqlquery, new { UserId = model.UserId }).FirstOrDefault();

                string UserTags = string.Empty;

                //if (User != null && User != string.Empty)
                //{

                int[] UserSkillSetTag;
                int[] UserBDETag;
                if (User != null)
                {


                    string sqlquery_UserSkillSetTag = @"select * from Tags t1 where t1.TagType = 1 and t1.Id in (" + User + ") ";
                    UserSkillSetTag = con.Query<int>(sqlquery_UserSkillSetTag).ToArray();

                    string sqlquery_UserBDETag = @"select * from Tags t1 where t1.TagType = 0 and t1.Id in (" + User + ") ";
                    UserBDETag = con.Query<int>(sqlquery_UserBDETag).ToArray();

                    if (model.TagType == "SkillSetTags")
                    {
                        foreach (var item in UserBDETag)
                        {
                            if (UserTags == string.Empty)
                            {
                                UserTags = item.ToString();
                            }
                            else
                            {
                                UserTags += "," + item.ToString();
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in UserSkillSetTag)
                        {
                            if (UserTags == string.Empty)
                            {
                                UserTags = item.ToString();
                            }
                            else
                            {
                                UserTags += "," + item.ToString();
                            }
                        }
                    }
                }

                foreach (var item in model.Tags)
                {
                    if (UserTags == string.Empty)
                    {
                        UserTags = item.TagId.ToString();
                    }
                    else
                    {
                        UserTags += "," + item.TagId.ToString();
                    }
                }
                //}

                string sqlquery_update = @"update UserDetails set Tags = @UserTags where UserId = @UserId and IsDeleted = 0 ";
                con.Execute(sqlquery_update, new { UserId = model.UserId, UserTags = UserTags });

            }


            return "Tags are saved Successfully !";
        }



        public string saveUserTags12(MODEL.UserProfileModel.SaveTags model)
        {
            List<string> modelTagIds = new List<string>();
            foreach (var TagId in model.Tags)
            {
                modelTagIds.Add(TagId.TagId.ToString());
            }

            var User = (from u1 in dbcontext.UserDetails
                        where u1.UserId == model.UserId && u1.IsDeleted == false
                        select u1.Tags).SingleOrDefault();

            string UserTags = string.Empty;

            if (User != null && User != string.Empty)
            {

                int[] UserOwnedTags = User.Split(',').Select(str => int.Parse(str)).ToArray();

                var UserSkillSetTag = (from t1 in dbcontext.Tags
                                       where UserOwnedTags.Contains(t1.Id)
                                       && t1.TagType == true
                                       select t1.Id).ToList();

                var UserBDETag = (from t1 in dbcontext.Tags
                                  where UserOwnedTags.Contains(t1.Id)
                                  && t1.TagType == false
                                  select t1.Id).ToList();


                foreach (var TagId in model.Tags)
                {
                    modelTagIds.Add(TagId.TagId.ToString());
                }

                if (modelTagIds != null && modelTagIds.Count() > 0)
                {

                    int[] newUserOwnedTags = modelTagIds.Select(str => int.Parse(str)).ToArray();

                    bool isTrue = false;
                    bool isfalse = false;

                    var TagForModel = from t2 in dbcontext.Tags
                                      where newUserOwnedTags.Contains(t2.Id)
                                      select t2;

                    foreach (var item in TagForModel)
                    {
                        if (item.TagType == true)
                        {
                            isTrue = true;
                            break;
                        }
                    }


                    foreach (var item in TagForModel)
                    {
                        if (item.TagType == false)
                        {
                            isfalse = true;
                            break;
                        }
                    }


                    if (isTrue == false)
                    {
                        foreach (var item in UserSkillSetTag)
                        {
                            if (UserTags == string.Empty)
                            {
                                UserTags = item.ToString();
                            }
                            else
                            {
                                UserTags += "," + item.ToString();
                            }
                        }
                    }


                    if (isfalse == false)
                    {
                        foreach (var item in UserBDETag)
                        {
                            if (UserTags == string.Empty)
                            {
                                UserTags = item.ToString();
                            }
                            else
                            {
                                UserTags += "," + item.ToString();
                            }
                        }
                    }

                }
            }
            foreach (var item in modelTagIds)
            {
                if (UserTags == string.Empty)
                {
                    UserTags = item;
                }
                else
                {
                    UserTags += "," + item;
                }
            }

            var user1 = (from usr in dbcontext.UserDetails
                         where usr.UserId == model.UserId && usr.IsDeleted == false
                         select usr).SingleOrDefault();

            user1.Tags = UserTags;
            dbcontext.SaveChanges();

            return "Tags are saved Successfully !";
        }


        public string saveUserTags123(MODEL.UserProfileModel.SaveTags model)
        {
            List<string> modelTagIds = new List<string>();
            foreach (var TagId in model.Tags)
            {
                modelTagIds.Add(TagId.TagId.ToString());
            }



            using (var con = new SqlConnection(ConnectionString))
            {
                var User = con.Query<string>("select e.Tags from UserDetails e where e.UserId = @UserId and IsDeleted = 0 ",
                    new { UserId = model.UserId }).SingleOrDefault();



                //var User = (from u1 in dbcontext.UserDetails
                //            where u1.UserId == model.UserId && u1.IsDeleted == false
                //            select u1.Tags).SingleOrDefault();

                string UserTags = string.Empty;

                if (User != null && User != string.Empty)
                {

                    int[] UserOwnedTags = User.Split(',').Select(str => int.Parse(str)).ToArray();

                    var UserSkillSetTag = con.Query<string>("select Id TagId from Tags t1 where t1.TagType =1 and t1.Id in (" + User + ") ",
                    new { UserId = model.UserId }).ToList();

                    var UserBDETag = con.Query<string>("select Id TagId from Tags t1 where t1.TagType =0 and t1.Id in (" + User + ") ",
                          new { UserId = model.UserId }).ToList();


                    //var UserSkillSetTag = (from t1 in dbcontext.Tags
                    //                       where UserOwnedTags.Contains(t1.Id)
                    //                       && t1.TagType == true
                    //                       select t1.Id).ToList();

                    //var UserBDETag = (from t1 in dbcontext.Tags
                    //                  where UserOwnedTags.Contains(t1.Id)
                    //                  && t1.TagType == false
                    //                  select t1.Id).ToList();


                    foreach (var TagId in model.Tags)
                    {
                        modelTagIds.Add(TagId.TagId.ToString());
                    }

                    if (modelTagIds != null && modelTagIds.Count() > 0)
                    {

                        int[] newUserOwnedTags = modelTagIds.Select(str => int.Parse(str)).ToArray();

                        bool isTrue = false;
                        bool isfalse = false;

                        var TagForModel = from t2 in dbcontext.Tags
                                          where newUserOwnedTags.Contains(t2.Id)
                                          select t2;

                        foreach (var item in TagForModel)
                        {
                            if (item.TagType == true)
                            {
                                isTrue = true;
                                break;
                            }
                        }


                        foreach (var item in TagForModel)
                        {
                            if (item.TagType == false)
                            {
                                isfalse = true;
                                break;
                            }
                        }


                        if (isTrue == false)
                        {
                            foreach (var item in UserSkillSetTag)
                            {
                                if (UserTags == string.Empty)
                                {
                                    UserTags = item.ToString();
                                }
                                else
                                {
                                    UserTags += "," + item.ToString();
                                }
                            }
                        }


                        if (isfalse == false)
                        {
                            foreach (var item in UserBDETag)
                            {
                                if (UserTags == string.Empty)
                                {
                                    UserTags = item.ToString();
                                }
                                else
                                {
                                    UserTags += "," + item.ToString();
                                }
                            }
                        }

                    }
                }
                foreach (var item in modelTagIds)
                {
                    if (UserTags == string.Empty)
                    {
                        UserTags = item;
                    }
                    else
                    {
                        UserTags += "," + item;
                    }
                }

                con.Query("update UserDetails set Tags = @UserTags where IsDeleted = 0 and UserId = @UserId",
                        new { UserTags = UserTags, UserId = model.UserId });



                //var user1 = (from usr in dbcontext.UserDetails
                //             where usr.UserId == model.UserId && usr.IsDeleted == false
                //             select usr).SingleOrDefault();

                //user1.Tags = UserTags;
                //dbcontext.SaveChanges();
            }
            return "Tags are saved Successfully !";

        }



        /// <summary>
        /// Getting User's Team And Project Count in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Team And Project Count </returns>
        public MODEL.UserProfileModel.UserTeamAndProjectCount getUserTeamAndProjectCount(string UserId)
        {
            MODEL.UserProfileModel.UserTeamAndProjectCount TeamAndProjectCount = new UserProfileModel.UserTeamAndProjectCount();



            using (var con = new SqlConnection(ConnectionString))
            {



                TeamAndProjectCount = con.Query<MODEL.UserProfileModel.UserTeamAndProjectCount>("Usp_getUserTeamAndProjectCount",
                new { UserId = UserId }, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }

            //    var teamCount = (from t in dbcontext.Teams
            //                     join tu in dbcontext.TeamUsers on t.Id equals tu.TeamId
            //                     where tu.UserId == UserId && t.IsDeleted == false
            //                     && t.IsDefault == false
            //                     select t.Id).Count();

            //var projectCount = (from p in dbcontext.Projects
            //                    join pu in dbcontext.ProjectUsers on p.Id equals pu.ProjectId
            //                    join t in dbcontext.Teams on p.TeamId equals t.Id
            //                    where pu.UserId == UserId && t.IsDeleted == false
            //                    && t.IsDefault == false
            //                    select p.Id).Count();


            //TeamAndProjectCount.TeamCount = objCount.TeamCount;
            //TeamAndProjectCount.ProjectCount = objCount.projectCount;

            //return TeamAndProjectCount;

            return TeamAndProjectCount;
        }




        /// <summary>
        /// Getting User's Testimonials in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Testimonials </returns>
        public MODEL.UserProfileModel.UserTestimonials getUserTestimonials(string UserId)
        {
            MODEL.UserProfileModel.UserTestimonials TestimonialList = new UserProfileModel.UserTestimonials();

            List<MODEL.UserProfileModel.Testimonials> AllTestimonialList = (from ct in dbcontext.ClientTestimonials
                                                                            where ct.UserId == UserId
                                                                            //&& ct.IsSelected == false
                                                                            select new MODEL.UserProfileModel.Testimonials
                                                                            {
                                                                                TestimonialId = ct.Id,
                                                                                AdminName = (from usr in dbcontext.UserDetails
                                                                                             where usr.UserId == ct.AdminId && usr.IsDeleted == false
                                                                                             select usr.FirstName).FirstOrDefault(),
                                                                                CompanyName = (from Comp in dbcontext.Companies
                                                                                               where Comp.Id == ct.CompanyId
                                                                                               select Comp.Name).FirstOrDefault(),
                                                                                Testimonial = ct.Feedback
                                                                            }).ToList();



            List<MODEL.UserProfileModel.Testimonials> UserTestimonialList = (from ct in dbcontext.ClientTestimonials
                                                                             where ct.UserId == UserId
                                                                             && ct.IsSelected == true
                                                                             select new MODEL.UserProfileModel.Testimonials
                                                                             {
                                                                                 TestimonialId = ct.Id,
                                                                                 AdminName = (from usr in dbcontext.UserDetails
                                                                                              where usr.UserId == ct.AdminId && usr.IsDeleted == false
                                                                                              select usr.FirstName).FirstOrDefault(),
                                                                                 CompanyName = (from Comp in dbcontext.Companies
                                                                                                where Comp.Id == ct.CompanyId
                                                                                                select Comp.Name).FirstOrDefault(),
                                                                                 Testimonial = ct.Feedback
                                                                             }).ToList();


            TestimonialList.AllTestimonialList = AllTestimonialList;
            TestimonialList.UserTestimonialList = UserTestimonialList;

            //TestimonialList = TestimonialList.Select()

            return TestimonialList;
        }




        /// <summary>
        /// Getting User's Feedbacks in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Feedbacks </returns>
        public List<MODEL.UserProfileModel.UserFeedbacks> getUserFeedbacks(string UserId)
        {


            List<MODEL.UserProfileModel.UserFeedbacks> List;
            List<MODEL.UserProfileModel.MemberFeedback> MemberFeedback;


            using (var con = new SqlConnection(ConnectionString))
            {

                var Result = con.QueryMultiple("Usp_GetMemberFeedback", new { UserId = UserId }, commandType: CommandType.StoredProcedure);

                List = Result.Read<MODEL.UserProfileModel.UserFeedbacks>().ToList();
                MemberFeedback = Result.Read<MODEL.UserProfileModel.MemberFeedback>().ToList();


                foreach (var option in List)
                {
                    option.MemberFeedback = MemberFeedback.Where(x => x.ProjectId == option.ProjectId).ToList();

                }


            }






            //var FeedbackList = (from prjct in dbcontext.Projects
            //                    join prjctusr in dbcontext.ProjectUsers on prjct.Id equals prjctusr.ProjectId
            //                    join com in dbcontext.Companies on prjct.CompanyId equals com.Id
            //                    where prjctusr.UserId == UserId && prjct.IsDeleted == false
            //                    //&&  prjct.IsComplete == true
            //                    select new
            //                    {
            //                        ProjectId = prjct.Id,
            //                        ProjectName = prjct.Name,
            //                        CompanyName = com.Name,
            //                        ProjectDescription = prjct.Description,
            //                        ProjectRemarks = (from pf in dbcontext.ProjectFeedbacks
            //                                          join ucr in dbcontext.UserCompanyRoles on pf.CreatedBy equals ucr.UserId
            //                                          where pf.ProjectId == prjct.Id
            //                                          && pf.UserId == UserId
            //                                          && ucr.RoleId < 3
            //                                          select pf.Feedback
            //                                         ).FirstOrDefault(),
            //                        CreatedDate = prjct.CreatedDate,
            //                        MemberFeedback = from fed in dbcontext.ProjectFeedbacks
            //                                         join usr in dbcontext.UserDetails on fed.CreatedBy equals usr.UserId
            //                                         join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
            //                                         join Rol in dbcontext.Roles on usrcom.RoleId equals Rol.RoleId
            //                                         where fed.UserId == UserId && usr.IsDeleted == false
            //                                         && fed.ProjectId == prjct.Id && Rol.IsDeleted == false
            //                                         && fed.IsSelected == true
            //                                         && Rol.RoleId > 2
            //                                         select new MODEL.UserProfileModel.MemberFeedback
            //                                         {
            //                                             MemberUserId = fed.CreatedBy,
            //                                             MemberName = usr.FirstName,
            //                                             Feedback = fed.Feedback,
            //                                             MemberRole = Rol.Name
            //                                         }
            //                    }
            //                  ).ToList();


            // List = FeedbackList.AsEnumerable().Select(x =>
            //new MODEL.UserProfileModel.UserFeedbacks
            //{
            //    ProjectId = x.ProjectId,
            //    CompanyName = x.CompanyName,
            //    ProjectName = x.ProjectName,
            //    ProjectDescription = x.ProjectDescription,
            //    ProjectRemarks = x.ProjectRemarks,
            //    CreatedDate = x.CreatedDate,
            //    MemberFeedback = x.MemberFeedback.ToList()
            //}
            //).ToList();


            List<MODEL.UserProfileModel.UserFeedbacks> FinalList = new List<UserProfileModel.UserFeedbacks>();

            foreach (var item in List)
            {
                if (item.ProjectRemarks != string.Empty && item.ProjectRemarks != null)
                {
                    FinalList.Add(item);
                }
            }

            return FinalList;
        }




        /// <summary>
        /// Getting Other User's Profile Status in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns Other User's Profile Status </returns>
        public bool getOtherUserProfileStatus(string UserId)
        {


            string query;

            query = @"select Id,UserId,Email,FirstName,LastName,Gender,IsSubscribed,CompanyId,RoleId,ParentUserId,ProfilePhoto,SelectedTeamId
                  ,SendNotificationEmail,Address,PhoneNo,Department,Designation,Experience,Expertise,DateOfBirth,AboutMe,EmailConfirmed
                  ,Password,Token,EmailConfirmationCode,Rate,IsFreelancer,UserName,Availabilty,Tags,FavProjects,IsProfilePublic,CreditCard
                  ,ExpiryMonth,ExpiryYear,CvvNumber,CardHolderName,AllowReckringPayment,StripeUserId,IsStripeAccountVerified,EmailConfirmationCodeCreatedDate
                  ,IsDeleted from UserDetails usr where usr.UserId = @UserId and usr.IsDeleted = 0";
            UserDetail user;
            using (var con = new SqlConnection(ConnectionString))
            {
                user = con.Query<UserDetail>(query, new { UserId = UserId }).SingleOrDefault();
            }

            //user = (from usr in dbcontext.UserDetails
            //        where usr.UserId == UserId && usr.IsDeleted == false
            //        select usr).FirstOrDefault();

            return user.IsProfilePublic.HasValue ? user.IsProfilePublic.Value : false;
        }


        /// <summary>
        /// Getting User's Testimonials in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's Testimonials </returns>
        public string saveUserTestimonials(MODEL.UserProfileModel.SaveTestimonial model)
        {
            List<ClientTestimonial> Selectedtestimonial = (from testimo in dbcontext.ClientTestimonials
                                                           where testimo.UserId == model.UserId
                                                           && testimo.IsSelected == true
                                                           select testimo).ToList();

            foreach (var testItem in Selectedtestimonial)
            {
                testItem.IsSelected = false;
                dbcontext.SaveChanges();
            }

            foreach (var item in model.Testimonial)
            {

                ClientTestimonial testimonial = (from test in dbcontext.ClientTestimonials
                                                 where test.Id == item.TestimonialId
                                                 select test).FirstOrDefault();

                testimonial.IsSelected = true;
                dbcontext.SaveChanges();

            }

            return "Tstimonial saved successfully";
        }



        /// <summary>
        /// Change User's ProfileStatus in User Profile  
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns User's ProfileStatus </returns>
        public bool changeUserProfile(MODEL.UserProfileModel.USerProfileStatusModel model)
        {

            
            


            //var user = (from usr in dbcontext.UserDetails
            //            where usr.UserId == model.UserId && usr.IsDeleted == false
            //            select usr).FirstOrDefault();

            //if (model.UserProfileStatus == true)
            //{
            //    user.IsProfilePublic = false;
            //}
            //else
            //{
            //    user.IsProfilePublic = true;
            //}

            using (var con = new SqlConnection(ConnectionString))
            {


                con.Query("Usp_changeUserProfile", new { IsProfilePublic = model.UserProfileStatus, UserId = model.UserId }, commandType: CommandType.StoredProcedure);
            }

            dbcontext.SaveChanges();
            return !model.UserProfileStatus;
        }



        /// <summary>
        /// Getting Other User's Educational Streams
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns Other User's Educational Streams </returns>
        public MODEL.UserProfileModel.UserEducationalStremModel getEducationalStreams(string UserId)
        {
            List<MODEL.UserProfileModel.UserEducationalStrem> allEducationalStreamObject;


            using (var con = new SqlConnection(ConnectionString))
            {

                allEducationalStreamObject = con.Query<MODEL.UserProfileModel.UserEducationalStrem>("Usp_GetEducationalStreams", new { UserId = UserId }, commandType: CommandType.StoredProcedure).ToList();

            }


            ////allEducationalStreamObject = (from edu in dbcontext.EducationalStreams
            //                              where edu.IsActive == true
            //                              select new MODEL.UserProfileModel.UserEducationalStrem
            //                              {
            //                                  Id = edu.Id,
            //                                  EducationalStreamName = edu.Name,
            //                                  Description = edu.Description
            //                              }).ToList();


            List<MODEL.UserProfileModel.UserEducationalStrem> EducationalStream = new List<UserProfileModel.UserEducationalStrem>();


            MODEL.UserProfileModel.UserEducationalStremModel model = new UserProfileModel.UserEducationalStremModel();
            model.allEducationalStreamObject = allEducationalStreamObject;
            model.EducationalStream = EducationalStream;

            return model;
        }



        /// <summary>
        /// Getting Other User's Email-Id
        /// </summary>
        /// <param name="UserId">It is unique Id of User</param>
        /// <returns>It returns Other User's Email-Id </returns>
        public string getUserEmailFromUserId(string UserId)
        {
            string CurrentEmailId = (from usr in dbcontext.UserDetails
                                     where usr.UserId == UserId && usr.IsDeleted == false
                                     select usr.Email).FirstOrDefault();

            return CurrentEmailId;
        }





    }
}
