﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using Dapper;
using System.Data.SqlClient;

namespace TIGHTEN.DATA
{
    public class Invitation : BaseClass
    {
        AppContext dbcontext;

        public List<MODEL.InvitationNotificationModal> GetInvitationNotification(string UserId, string InvitaitonStatus)
        {

            //var newInvNotification = (from inv in dbcontext.FreelancerInvitations
            //                          join usr in dbcontext.UserDetails on inv.FreelancerEmail equals usr.Email
            //                          where usr.UserId == UserId && usr.IsDeleted == false
            //                          && (InvitaitonStatus == "2" ? (inv.IskeyActive == false && inv.IsAccepted == true) : InvitaitonStatus == "3" ? (inv.IskeyActive == false && inv.IsAccepted == false) : InvitaitonStatus == "4" ? (inv.IskeyActive == true) : (inv.IsAccepted == inv.IsAccepted))
            //                          select new MODEL.InvitationNotificationModal
            //                          {
            //                              FreelancerInvitationId = inv.Id,
            //                              EmailForFreelancer = inv.FreelancerEmail,
            //                              SentBy = inv.SentBy,
            //                              ProjectId = inv.ProjectId,
            //                              UserRate = inv.UserRate.HasValue ? inv.UserRate.Value : 0,
            //                              RoleId = inv.RoleId,
            //                              IsJobInvitation = inv.IsJobInvitation.HasValue ? inv.IsJobInvitation.Value : false,
            //                              WeeklyLimit = inv.WeeklyLimit.HasValue ? inv.WeeklyLimit.Value : 0,
            //                              JobTitle = inv.JobTitle,
            //                              JobDescription = inv.JobDescription,
            //                              FreelancerRate = inv.FreelancerRate,
            //                              IskeyActive = inv.IskeyActive,
            //                              IsAccepted = inv.IsAccepted,
            //                              IsRejected = inv.IsRejected,
            //                              AttachmentDescription = inv.AttachmentDescription,
            //                              Responsibilities = inv.Responsibilities,
            //                              FreelancerName = usr.LastName == null ? usr.FirstName : usr.FirstName + " " + usr.LastName,
            //                              FreelancerProfilePic = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + usr.ProfilePhoto,
            //                              FreelancerProfileRate = usr.Rate,

            //                              CompanyDetail = (from obj in
            //                                          (from cmp in dbcontext.Companies
            //                                           join usrdtforcomp in dbcontext.UserDetails on cmp.Id equals usrdtforcomp.CompanyId
            //                                           join uccr in dbcontext.UserCompanyRoles on cmp.Id equals uccr.CompanyId
            //                                           where uccr.UserId == inv.SentBy
            //                                           && usrdtforcomp.IsDeleted == false

            //                                           select new { cmp, usrdtforcomp, uccr }
            //                                           )
            //                                               group obj by new
            //                                               {
            //                                                   //Name = obj.cmp.Name,CompId = obj.cmp.Id,CompUserId = obj.usrdtforcomp.UserId,CompProfilePic = obj.usrdtforcomp.ProfilePhoto
            //                                                   Name = obj.cmp.Name,
            //                                                   CompId = obj.cmp.Id,
            //                                                   CompUserId = obj.cmp.Id,
            //                                                   CompProfilePic = obj.cmp.Logo
            //                                               } into objGrouped
            //                                               select new MODEL.CompanyModalForInvitaion
            //                                               {
            //                                                   CompanyName = objGrouped.Key.Name,
            //                                                   CompanyProfilePic = objGrouped.Key.CompProfilePic == null ? "Uploads/Default/nologo.png" : "Uploads/CompanyLogos/" + objGrouped.Key.CompProfilePic,
            //                                                   CompanyUserId = objGrouped.Key.CompUserId.ToString(),
            //                                                   CompanyId = objGrouped.Key.CompId,
            //                                               }).FirstOrDefault(),

            //                             ProjectName = (from prjctt in dbcontext.Projects
            //                                             where prjctt.Id == inv.ProjectId
            //                                             && prjctt.IsDeleted == false
            //                                             select prjctt.Name).FirstOrDefault()
            //                                             ,
            //                              Role = (
            //                                              from rol in dbcontext.Roles
            //                                              where rol.RoleId == inv.RoleId
            //                                              && rol.IsDeleted == false
            //                                              select rol.Name
            //                                             ).FirstOrDefault(),
            //                              SenderName = (from ussr in dbcontext.UserDetails
            //                                            where ussr.UserId == inv.SentBy
            //                                            && ussr.IsDeleted == false
            //                                            select ussr.FirstName).FirstOrDefault(),

            //                              SenderProfilePic = (from ussr in dbcontext.UserDetails
            //                                                  where ussr.UserId == inv.SentBy
            //                                                  && ussr.IsDeleted == false
            //                                                  select ussr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + ussr.ProfilePhoto).FirstOrDefault(),


            //                          }).ToList();




            List<MODEL.InvitationNotificationModal> InvNotification;
            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetInvitationNotification", new { UserId = UserId, InvitaitonStatus = InvitaitonStatus }, commandType: System.Data.CommandType.StoredProcedure);

                InvNotification = result.Read<MODEL.InvitationNotificationModal>().ToList();
                if (InvNotification != null)
                {
                    var company = result.Read<MODEL.CompanyModalForInvitaion>().ToList();

                    foreach (var item in InvNotification)
                    {
                        item.CompanyDetail = company.Where(x => x.UsersIdForCompany == item.SentBy).FirstOrDefault();
                    }
                }
            }


            return InvNotification;
        }


        public string AcceptInvitation(MODEL.InvitationNotificationModal model)
        {
            string msg = string.Empty;

            //var usrcmpnyrol = from ucr in dbcontext.UserCompanyRoles
            //                  where ucr.CompanyId == model.CompanyDetail.CompanyId
            //                  && ucr.RoleId == model.RoleId
            //                  && ucr.UserId == model.FreelancerUserId
            //                  select ucr;


            //if (usrcmpnyrol.Count() == 0)
            //{
            //    // Save userCompanyRole
            //    UserCompanyRole objUserRole = new UserCompanyRole();
            //    objUserRole.CompanyId = model.CompanyDetail.CompanyId;
            //    objUserRole.RoleId = model.RoleId;
            //    objUserRole.UserId = model.FreelancerUserId;
            //    objUserRole.CreatedDate = DateTime.Now;
            //    dbcontext.UserCompanyRoles.Add(objUserRole);

            //    if (objUserRole.Id > 0 && model.ProjectId > 0)
            //    {
            //        msg = "invitaionAccepted";
            //    }
            //}

            //#region Saving default value in SettingConfigs


            //UserSettingConfig usrsetting = (from usrset in dbcontext.UserSettingConfigs
            //                                join set in dbcontext.SettingConfigs on usrset.NotificationId equals set.Id
            //                                where set.IsActive == true
            //                                && usrset.CompanyId == model.CompanyDetail.CompanyId
            //                                && usrset.UserId == model.FreelancerUserId
            //                                && set.NotificationName == "Project Update"
            //                                select usrset).FirstOrDefault();

                        

            //if (usrsetting != null)
            //{
            //    if (usrsetting.ProjectId != null && usrsetting.ProjectId != string.Empty)
            //    {
            //        usrsetting.ProjectId += "," + model.ProjectId.ToString();
            //    }
            //    else
            //    {
            //        usrsetting.ProjectId = model.ProjectId.ToString();
            //    }
            //}
            //else
            //{

            //    /* getting all Settings for the user  */
            //    List<SettingConfig> setting = (from set in dbcontext.SettingConfigs
            //                                   where set.IsActive == true
            //                                   && set.NotificationName != "Subscription Expiry"
            //                                   && set.NotificationName != "Invoice Raise"
            //                                   select set).ToList();

            //    foreach (SettingConfig item in setting)
            //    {
            //        string CustomProjectId = null;
            //        if (item.NotificationName == "Project Update")
            //        {
            //            CustomProjectId = model.ProjectId.ToString();
            //        }
            //        /* Inserting default values for UserSettingConfig table   */
            //        UserSettingConfig obj = new UserSettingConfig
            //        {
            //            CompanyId = model.CompanyDetail.CompanyId,
            //            ProjectId = CustomProjectId,
            //            UserId = model.FreelancerUserId,
            //            NotificationId = item.Id,
            //            IsActive = true
            //        };

            //        dbcontext.UserSettingConfigs.Add(obj);
            //    }

            //}
            //dbcontext.SaveChanges();

            //#endregion

            //if (model.ProjectId > 0)
            //{
            //    var FreelancerTeamId = (from prct in dbcontext.Projects
            //                            where prct.Id == model.ProjectId
            //                            && prct.IsDeleted == false
            //                            select prct.TeamId).SingleOrDefault();

            //    var tmusr = from tm in dbcontext.TeamUsers
            //                where tm.UserId == model.FreelancerUserId
            //                && tm.TeamId == FreelancerTeamId
            //                select tm;

            //    if (tmusr.Count() == 0)
            //    {
            //        //Adding the default user in the team
            //        TeamUser teamusersDefault = new TeamUser
            //        {
            //            UserId = model.FreelancerUserId,
            //            CreatedBy = model.FreelancerUserId,
            //            CreatedDate = DateTime.Now,
            //            TeamId = FreelancerTeamId
            //        };
            //        dbcontext.TeamUsers.Add(teamusersDefault);
            //        var teamUserId = dbcontext.SaveChanges();
            //    }

            //    //Adding the Freelancer in the Project
            //    ProjectUser prjctUsr = new ProjectUser
            //    {
            //        UserId = model.FreelancerUserId,
            //        CreatedBy = model.FreelancerUserId,
            //        CreatedDate = DateTime.Now,
            //        ProjectId = model.ProjectId,
            //        UserRate = model.FreelancerRate,
            //        WeeklyLimit = model.WeeklyLimit,
            //        InvitationId = model.FreelancerInvitationId
            //    };

            //    dbcontext.ProjectUsers.Add(prjctUsr);
            //    dbcontext.SaveChanges();

            //    if (prjctUsr.Id > 0)
            //    {
            //        msg = "invitaionAccepted";
            //    }
            //}


            //var freelancerinvitation = (from invitation in dbcontext.FreelancerInvitations
            //                            where invitation.Id == model.FreelancerInvitationId
            //                            select invitation).SingleOrDefault();

            //freelancerinvitation.IsAccepted = true;
            //freelancerinvitation.IskeyActive = false;
            //dbcontext.SaveChanges();

            using (var con = new SqlConnection(ConnectionString))
            {
                msg = con.Query<string>("usp_AcceptInvitation", new { CompanyId = model.CompanyDetail.CompanyId, RoleId = model.RoleId, FreelancerUserId = model.FreelancerUserId, ProjectId = model.ProjectId, FreelancerRate = model.FreelancerRate, WeeklyLimit = model.WeeklyLimit, FreelancerInvitationId = model.FreelancerInvitationId }, commandType: System.Data.CommandType.StoredProcedure).Single();
            }

            if (msg == "invitaionAccepted")
            {
                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForAcceptProject(model);
                }
                ));
                childref.Start();
            }
             
            return msg;
        }



        public string RejectInvitation(MODEL.InvitationNotificationModal model)
        {

            //var freelancerinvitation = (from invitation in dbcontext.FreelancerInvitations
            //                            where invitation.Id == model.FreelancerInvitationId
            //                            select invitation).SingleOrDefault();

            //if (freelancerinvitation != null)
            //{
            //    freelancerinvitation.IsRejected = true;
            //    freelancerinvitation.IskeyActive = false;
            //    freelancerinvitation.Remarks = model.RejectionReason;

            //    dbcontext.SaveChanges();


            //    HttpContext ctx = HttpContext.Current;
            //    Thread childref = new Thread(new ThreadStart(() =>
            //    {
            //        HttpContext.Current = ctx;
            //        ThreadForRejection(model);
            //    }
            //    ));
            //    childref.Start();

            //    return "Invitation is rejected Successfully !";
            //}
            //else
            //{
            //    return "Some Error Occured !";
            //}

            string msg = "Some Error Occured !";

            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"update FreelancerInvitations set IsRejected = 1, IskeyActive = 0, Remarks = @RejectionReason where Id = @FreelancerInvitationId  ";
                con.Execute(sqlquery, new { RejectionReason = model.RejectionReason, FreelancerInvitationId = model.FreelancerInvitationId });

                HttpContext ctx = HttpContext.Current;
                Thread childref = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = ctx;
                    ThreadForRejection(model);
                }
                ));
                childref.Start();

                msg = "Invitation is rejected Successfully !";
            }

            return msg;

        }


        private void ThreadForAcceptProject(MODEL.InvitationNotificationModal model)
        {
            //var receiver = (from r in dbcontext.UserDetails
            //                join ucr in dbcontext.UserCompanyRoles on r.UserId equals ucr.UserId
            //                join c in dbcontext.Companies on ucr.CompanyId equals c.Id
            //                where r.UserId == model.SentBy
            //                && r.IsDeleted == false
            //                select new {
            //                    FirstName = r.FirstName,
            //                    Email = r.Email,
            //                    CompanyName = c.Name
            //                }).FirstOrDefault();

            //var project = (from p in dbcontext.Projects
            //               where p.Id == model.ProjectId
            //               && p.IsDeleted == false
            //               select p).FirstOrDefault();

            //var freelancer = (from f in dbcontext.UserDetails
            //                  where f.UserId == model.FreelancerUserId
            //                  && f.IsDeleted == false
            //                  select f).FirstOrDefault();

            MODEL.FreelancerInfoToConfirmLink receiver;
            Project project;
            UserDetail freelancer;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select r.FirstName,r.Email,c.Name as CompanyName from UserDetails r inner join UserCompanyRoles ucr on r.UserId = ucr.UserId inner join Companies c on ucr.CompanyId = c.Id 
                                    where r.UserId = @SentBy and r.IsDeleted = 0 
                                    select * from Projects p where p.Id = @ProjectId and p.IsDeleted = 0                                                          
                                    select * from UserDetails f where f.UserId = @FreelancerUserId and f.IsDeleted = 0        ";

                var result = con.QueryMultiple(sqlquery, new { SentBy = model.SentBy, ProjectId = model.ProjectId, FreelancerUserId = model.FreelancerUserId });

                receiver = result.Read<MODEL.FreelancerInfoToConfirmLink>().FirstOrDefault();
                project = result.Read<Project>().FirstOrDefault();
                freelancer = result.Read<UserDetail>().FirstOrDefault();

            }

            #region Mail-Body
            String emailbody = null;
            string Subject = string.Empty;
            string SendMailfor = "FreelancerAcceptOrganizationInvitation";
            if (model.ProjectId > 0)
            {
                SendMailfor = "FreelancerAcceptInvitation";
            }
            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == SendMailfor)
                                 //select r;
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@FreelancerName", freelancer.FirstName).Replace("@@ReceiverName", receiver.FirstName);
            Subject = Subject.Replace("@@FreelancerName", freelancer.FirstName).Replace("@@ReceiverName", receiver.FirstName);

            if (model.ProjectId > 0)
            {
                emailbody = emailbody.Replace("@@ProjectName", project.Name);
                Subject = Subject.Replace("@@ProjectName", project.Name);
            }
            else
            {
                emailbody = emailbody.Replace("@@CompanyName", receiver.CompanyName);
                Subject = Subject.Replace("@@CompanyName", receiver.CompanyName);
            }

            #endregion

            EmailUtility.SendMailInThread(receiver.Email, Subject, emailbody);
        }



        private void ThreadForRejection(MODEL.InvitationNotificationModal model)
        {
            //var receiver = (from r in dbcontext.UserDetails
            //                join ucr in dbcontext.UserCompanyRoles on r.UserId equals ucr.UserId
            //                join c in dbcontext.Companies on ucr.CompanyId equals c.Id
            //                where r.UserId == model.SentBy
            //                && r.IsDeleted == false
            //                select new
            //                {
            //                    FirstName = r.FirstName,
            //                    Email = r.Email,
            //                    CompanyName = c.Name
            //                }).FirstOrDefault();

            //var project = (from p in dbcontext.Projects
            //               where p.Id == model.ProjectId
            //               && p.IsDeleted == false
            //               select p).FirstOrDefault();

            //var freelancer = (from f in dbcontext.UserDetails
            //                  where f.UserId == model.FreelancerUserId
            //                  && f.IsDeleted == false
            //                  select f).FirstOrDefault();

            MODEL.FreelancerInfoToConfirmLink receiver;
            Project project;
            UserDetail freelancer;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = @"select r.FirstName,r.Email,c.Name as CompanyName from UserDetails r inner join UserCompanyRoles ucr on r.UserId = ucr.UserId inner join Companies c on ucr.CompanyId = c.Id 
                                    where r.UserId = @SentBy and r.IsDeleted = 0 
                                    select * from Projects p where p.Id = @ProjectId and p.IsDeleted = 0                                                          
                                    select * from UserDetails f where f.UserId = @FreelancerUserId and f.IsDeleted = 0        ";

                var result = con.QueryMultiple(sqlquery, new { SentBy = model.SentBy, ProjectId = model.ProjectId, FreelancerUserId = model.FreelancerUserId });

                receiver = result.Read<MODEL.FreelancerInfoToConfirmLink>().FirstOrDefault();
                project = result.Read<Project>().FirstOrDefault();
                freelancer = result.Read<UserDetail>().FirstOrDefault();

            }


            #region Mail-Body
            String emailbody = null;
            string Subject = string.Empty;
            string SendMailfor = "FreelancerRejectOrganizationInvitation";
            if (model.ProjectId > 0)
            {
                SendMailfor = "FreelancerRejectInvitation";
            }
            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == SendMailfor)
                                 //select r;
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@FreelancerName", freelancer.FirstName).Replace("@@ReceiverName", receiver.FirstName);
            Subject = Subject.Replace("@@FreelancerName", freelancer.FirstName).Replace("@@ReceiverName", receiver.FirstName);

            if (model.ProjectId > 0)
            {
                emailbody = emailbody.Replace("@@ProjectName", project.Name);
                Subject = Subject.Replace("@@ProjectName", project.Name);
            }
            else
            {
                emailbody = emailbody.Replace("@@CompanyName", receiver.CompanyName);
                Subject = Subject.Replace("@@CompanyName", receiver.CompanyName);
            }
            #endregion

            EmailUtility.SendMailInThread(receiver.Email, Subject, emailbody);
        }



        public List<MODEL.InvitationMessagesModal> GetUserMessage(int FreelancerInvitationId, string UserId)
        {
            //List<MODEL.InvitationMessagesModal> AllMessages = (from msgs in dbcontext.InvitationMessages
            //                                                   join usr in dbcontext.UserDetails on msgs.SentBy equals usr.UserId
            //                                                   where msgs.InvitationId == FreelancerInvitationId
            //                                                   && usr.IsDeleted == false
            //                                                   //&& msgs.UserId == UserId
            //                                                   orderby msgs.CreatedDate

            //                                                   select new MODEL.InvitationMessagesModal
            //                                                   {
            //                                                       Id = msgs.Id,
            //                                                       InvitationId = msgs.InvitationId,
            //                                                       UserId = msgs.UserId,
            //                                                       SentBy = msgs.SentBy,
            //                                                       SenderName = usr.FirstName,
            //                                                       SenderProfilePic = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + usr.ProfilePhoto,
            //                                                       Message = msgs.Message,
            //                                                       IsRead = msgs.IsRead,
            //                                                       CreatedDate = msgs.CreatedDate
            //                                                   }).ToList();


            List<MODEL.InvitationMessagesModal> AllMessages;
            using (var con = new SqlConnection(ConnectionString))
            {
                AllMessages = con.Query<MODEL.InvitationMessagesModal>("usp_GetUserMessageForJobOffer", new { FreelancerInvitationId = FreelancerInvitationId }, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }

            return AllMessages;
        }


        public string SendMessgae(MODEL.InvitationMessagesModal model,string url)
        {
            string msg = string.Empty;

            if (model.Message != "")
            {
                //InvitationMessage MsgObj = new InvitationMessage();
                //MsgObj.InvitationId = model.InvitationId;
                //MsgObj.UserId = model.UserId;
                //MsgObj.SentBy = model.SentBy;
                //MsgObj.Message = model.Message;
                //MsgObj.IsRead = false;
                //MsgObj.CreatedDate = DateTime.Now;


                //    dbcontext.InvitationMessages.Add(MsgObj);
                //dbcontext.SaveChanges();

                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Execute("usp_SendMessgaeForJobOffer", new { InvitationId = model.InvitationId, UserId = model.UserId, SentBy = model.SentBy, Message = model.Message },commandType:System.Data.CommandType.StoredProcedure);

                    HttpContext ctx = HttpContext.Current;
                    Thread childref = new Thread(new ThreadStart(() =>
                    {
                        HttpContext.Current = ctx;
                        ThreadForInvitationMessages(model, url);
                    }
                    ));
                    childref.Start();

                    msg = "Message Saved";
                }
            }
            else
            {
                msg = "Some error occured";
            }

            return msg;
        }



        //Sending mail on new Invitation Messages to receiver
        private void ThreadForInvitationMessages(MODEL.InvitationMessagesModal model,string url)
        {

            //UserDetail Sender    = (from crntusr in dbcontext.UserDetails
            //                           where crntusr.UserId == model.SentBy
            //                           && crntusr.IsDeleted == false
            //                        select crntusr).FirstOrDefault();

            //UserDetail Receiver = (from membr in dbcontext.UserDetails
            //                       where membr.UserId == model.UserId
            //                       && membr.IsDeleted == false
            //                       select membr).FirstOrDefault();

            UserDetail Sender;
            UserDetail Receiver;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery_Sender = @"select * from UserDetails crntusr where crntusr.UserId = @SentBy and crntusr.IsDeleted = 0   ";
                string sqlquery_Receiver = @"select * from UserDetails membr where membr.UserId = @UserId and membr.IsDeleted = 0   ";

                Sender = con.Query<UserDetail>(sqlquery_Sender, new { SentBy = model.SentBy }).FirstOrDefault();
                Receiver = con.Query<UserDetail>(sqlquery_Receiver, new { UserId = model.UserId }).FirstOrDefault();
            }

            var callbackUrl = url + "#/invitation/" + 0;
            String emailbody = null;
            string Subject = string.Empty;

            XDocument xdoc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/MailHelper.xml"));//xml doc used for xml parsing
            var MailModule = from r in xdoc.Descendants("MailModule").
                             Where(r => (string)r.Attribute("id") == "InvitationMessageEmail")
                             select new
                             {
                                 mailbody = r.Element("Body").Value,
                                 subject = r.Element("Subject").Value
                             };

            foreach (var Mailitem in MailModule)
            {
                emailbody = Mailitem.mailbody;
                Subject = Mailitem.subject;
            }

            emailbody = emailbody.Replace("@@SenderName", Sender.FirstName).Replace("@@ReceiverName", Receiver.FirstName).Replace("@@message", model.Message).Replace("@@callbackUrl", callbackUrl);
            Subject = Subject.Replace("@@SenderName", Sender.FirstName);

            EmailUtility.SendMailInThread(Receiver.Email, Subject, emailbody);


        }



        public MODEL.InvitationFreelancerInfoModal GetFreelancerInfo(int InvitationId)
        {
            //var Freelancers = from freeinv in dbcontext.FreelancerInvitations
            //                  join usr in dbcontext.UserDetails on freeinv.FreelancerEmail equals usr.Email
            //                  join usrcom in dbcontext.UserCompanyRoles on usr.UserId equals usrcom.UserId
            //                  join rol in dbcontext.Roles on usrcom.RoleId equals rol.RoleId
            //                  //join att in dbcontext.ProjectInvitationAttachments on usr.UserId equals att.UserId
            //                  where freeinv.Id == InvitationId && usr.IsDeleted == false && rol.IsDeleted == false

            //                  select new
            //                  {
            //                      UserId = usr.UserId,
            //                      RoleId = usr.RoleId,
            //                      RoleName = rol.Name,
            //                      RateOffered = freeinv.FreelancerRate,
            //                      Notes = freeinv.Responsibilities,
            //                      FreelancerName = usr.LastName == null ? usr.FirstName : usr.FirstName + " " + usr.LastName,
            //                      FreelancerProfilePic = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + usr.ProfilePhoto,
            //                      FreelancerProfileRate = usr.Rate,

            //                      Attachments = from freeatt in dbcontext.ProjectInvitationAttachments
            //                                    where freeatt.ProjectId == freeinv.ProjectId
            //                                    && freeatt.UserId == usr.UserId
            //                                    select new MODEL.InvitationAttachmentModel
            //                                    {
            //                                        FileName = freeatt.FileName,
            //                                        ActualFileName = freeatt.ActualFileName,
            //                                        FilePath = freeatt.FilePath
            //                                    }
            //                  };



            //MODEL.InvitationFreelancerInfoModal AllFreelancers = Freelancers.AsEnumerable().Select(x =>
            //new MODEL.InvitationFreelancerInfoModal
            //{
            //    UserId = x.UserId,
            //    RoleId = x.RoleId,
            //    RoleName = x.RoleName,
            //    RateOffered = x.RateOffered,
            //    Notes = x.Notes,
            //    FreelancerName = x.FreelancerName,
            //    FreelancerProfilePic =x.FreelancerProfilePic,
            //    FreelancerProfileRate = x.FreelancerProfileRate,
            //    Attachments = x.Attachments.ToList()
            //}
            //).FirstOrDefault();


            MODEL.InvitationFreelancerInfoModal AllFreelancers;

            using (var con = new SqlConnection(ConnectionString))
            {
                var result = con.QueryMultiple("usp_GetFreelancerInfoForJobOffer", new { InvitationId = InvitationId }, commandType: System.Data.CommandType.StoredProcedure);

                AllFreelancers = result.Read<MODEL.InvitationFreelancerInfoModal>().FirstOrDefault();
                if (AllFreelancers != null)
                {
                    var Attachments = result.Read<MODEL.InvitationAttachmentModel>().ToList();

                    AllFreelancers.Attachments = Attachments.Where(x => x.ProjectId == AllFreelancers.ProjectId && x.UserId == AllFreelancers.UserId).ToList();
                }
            }

            return AllFreelancers;
        }






    }
}
