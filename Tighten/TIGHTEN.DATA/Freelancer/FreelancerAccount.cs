﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace TIGHTEN.DATA
{
    public class FreelancerAccount : BaseClass
    {
        AppContext dbcontext;

        public MODEL.FreelancerInvitationModel getFreelancerInfoForRegistration(string key)
        {
            //MODEL.FreelancerInvitationModel freelancerInfo = (from freelancer in dbcontext.FreelancerInvitations
            //                                                  where freelancer.Activationkey == key
            //                                                  select new MODEL.FreelancerInvitationModel
            //                                                  {
            //                                                      EmailForFreelancer = freelancer.FreelancerEmail,
            //                                                      SentBy = freelancer.SentBy,
            //                                                      CompanyId = freelancer.CompanyId,
            //                                                      ProjectId = freelancer.ProjectId,
            //                                                      UserRate = freelancer.UserRate.HasValue ? freelancer.UserRate.Value : 0,
            //                                                      RoleId = freelancer.RoleId,
            //                                                      Activationkey = freelancer.Activationkey,
            //                                                      IskeyActive = freelancer.IskeyActive,
            //                                                      SentDate = freelancer.SentDate,
            //                                                      IsAccepted = freelancer.IsAccepted,
            //                                                      IsFreelancerUserIdExist = (from usr in dbcontext.UserDetails
            //                                                                                 where usr.Email == freelancer.FreelancerEmail
            //                                                                                 && usr.IsDeleted == false
            //                                                                                 select usr.UserId).FirstOrDefault()
            //                                                  }).SingleOrDefault();

            MODEL.FreelancerInvitationModel freelancerInfo;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlQuery = "select freelancer.FreelancerEmail as EmailForFreelancer,freelancer.SentBy, freelancer.CompanyId, freelancer.ProjectId,freelancer.UserRate, freelancer.RoleId, " +
                                  "freelancer.Activationkey, freelancer.IskeyActive, freelancer.SentDate, freelancer.IsAccepted, (select usr.UserId from UserDetails as usr where usr.Email = freelancer.FreelancerEmail and usr.IsDeleted = 0) as IsFreelancerUserIdExist " +
                                  "from FreelancerInvitations as freelancer  where freelancer.Activationkey = @key";
                freelancerInfo = con.Query<MODEL.FreelancerInvitationModel>(sqlQuery,new { key = key }).SingleOrDefault();
            }


            return freelancerInfo;
        }

        public string RegisterFreelancer(MODEL.RegisterBindingModel model, MODEL.FreelancerInvitationModel freelancerInfo, string pictureUrl)
        {
            string msg = string.Empty;
            string FinalFilename = string.Empty;

            if (pictureUrl != null && pictureUrl != "")
            {
                // Saving Freelancer Profile Pic From LinkedIn
                using (WebClient webClient = new WebClient())
                {
                    byte[] data = webClient.DownloadData(pictureUrl);
                    Guid guid = Guid.NewGuid();
                    FinalFilename = guid.ToString().Substring(0, 8) + ".png";
                    
                    using (MemoryStream mem = new MemoryStream(data))
                    {
                        using (var yourImage = System.Drawing.Image.FromStream(mem))
                        {
                            // If you want it as Png
                            yourImage.Save(HttpContext.Current.Server.MapPath("~/Uploads/Profile/Original/") + FinalFilename, System.Drawing.Imaging.ImageFormat.Png);
                        }

                        var thumbnailPath = HttpContext.Current.Server.MapPath("~/Uploads/Profile/Thumbnail/" + FinalFilename);
                        var iconPath = HttpContext.Current.Server.MapPath("~/Uploads/Profile/Icon/" + FinalFilename); ;

                        GenerateThumbnails(0.07, mem, thumbnailPath);
                        GenerateIcon(0.07, mem, iconPath);
                    }
                }
            }


            DateTime ValidSentDate = DateTime.Now.AddDays(-7);

            #region Old code using EntityFramework
                //var freelancer = (from frlncr in dbcontext.FreelancerInvitations
                //                  where frlncr.Activationkey == freelancerInfo.Activationkey
                //                  && frlncr.IskeyActive == true
                //                  && frlncr.SentDate >= ValidSentDate
                //                  select frlncr).SingleOrDefault();

                //if (freelancer != null)
                //{
                //    /* Save User Details */
                //    UserDetail userdetail = new UserDetail
                //    {
                //        Email = model.Email,
                //        Password = model.Password,
                //        FirstName = model.FirstName,
                //        LastName = model.LastName,
                //        UserId = Guid.NewGuid().ToString(),
                //        EmailConfirmed = true,
                //        //ProfilePhoto = "qwe.Png",
                //        IsSubscribed = false,
                //        //LastName = model.LastName,
                //        Token = string.Empty,
                //        EmailConfirmationCode = EmailUtility.GetUniqueKey(8),
                //        Rate = model.Rate == 0 ? 10 : model.Rate,
                //        IsFreelancer = model.isFreelancer,
                //        UserName = model.UserName,
                //        IsProfilePublic = true
                //    };

                //    if (pictureUrl != null && pictureUrl != "")
                //    {
                //        userdetail.ProfilePhoto = FinalFilename;
                //    }


                //    // Save userCompanyRole
                //    UserCompanyRole objUserRole = new UserCompanyRole();
                //    objUserRole.CompanyId = freelancerInfo.CompanyId;
                //    objUserRole.RoleId = freelancerInfo.RoleId;
                //    objUserRole.UserId = userdetail.UserId;
                //    objUserRole.CreatedDate = DateTime.Now;
                //    dbcontext.UserCompanyRoles.Add(objUserRole);

                //    #region Stripe
                //    // Saving StripeAccount Settings and info in StripeAccountDetail table
                //    StripeAccountDetail accdetail = new StripeAccountDetail
                //    {
                //        UserId = userdetail.UserId,
                //        Email = model.Email,
                //        Managed = true
                //    };
                //    dbcontext.StripeAccountDetails.Add(accdetail);

                //    var account = new StripeAccountCreateOptions();
                //    account.Email = model.Email;
                //    account.Managed = true;
                //    var accountService = new StripeAccountService();
                //    StripeAccount response = accountService.Create(account);

                //    userdetail.StripeUserId = response.Id;
                //    userdetail.IsStripeAccountVerified = false;

                //    #endregion

                //    // Save Default Team  */
                //    TIGHTEN.ENTITY.Team team = new TIGHTEN.ENTITY.Team
                //    {
                //        TeamName = "My Personal",
                //        Description = "This is my personal team",
                //        CreatedBy = userdetail.UserId,
                //        CreatedDate = DateTime.Now,
                //        CompanyId = 0,
                //        IsDefault = true
                //    };
                //    dbcontext.Teams.Add(team);
                //    var teamId = dbcontext.SaveChanges();

                //    //Adding the default user in the team
                //    TeamUser teamusersDefault = new TeamUser
                //    {
                //        UserId = userdetail.UserId,
                //        CreatedBy = userdetail.UserId,
                //        CreatedDate = DateTime.Now,
                //        TeamId = team.Id
                //    };
                //    dbcontext.TeamUsers.Add(teamusersDefault);
                //    var teamUserId = dbcontext.SaveChanges();

                //    userdetail.SelectedTeamId = team.Id;
                //    userdetail.CompanyId = 0;
                //    /* Save User Details */
                //    dbcontext.UserDetails.Add(userdetail);


                //    #region Inserting default values for UserSettingConfig table
                //    /* getting all Settings for the user  */
                //    List<SettingConfig> setting = (from set in dbcontext.SettingConfigs
                //                                   where set.IsActive == true
                //                                   && set.NotificationName != "Subscription Expiry"
                //                                   && set.NotificationName != "Invoice Raise"
                //                                   select set).ToList();


                //    foreach (SettingConfig item in setting)
                //    {
                //        /* Inserting default values for UserSettingConfig table   */
                //        UserSettingConfig obj = new UserSettingConfig
                //        {
                //            CompanyId = freelancerInfo.CompanyId,
                //            UserId = userdetail.UserId,
                //            NotificationId = item.Id,
                //            IsActive = true
                //        };
                //        dbcontext.UserSettingConfigs.Add(obj);

                //    }

                //    dbcontext.SaveChanges();
                //    #endregion

                //    if (freelancerInfo.ProjectId != 0)
                //    {
                //        //Adding the Freelancer in the Project
                //        ProjectUser prjctUsr = new ProjectUser
                //        {
                //            UserId = userdetail.UserId,
                //            CreatedBy = userdetail.UserId,
                //            CreatedDate = DateTime.Now,
                //            ProjectId = freelancerInfo.ProjectId,
                //            UserRate = freelancerInfo.UserRate
                //        };

                //        dbcontext.ProjectUsers.Add(prjctUsr);
                //        //dbcontext.SaveChanges();

                //        var TeamIdForFreelancer = (from pr in dbcontext.Projects
                //                                   where pr.Id == freelancerInfo.ProjectId
                //                                   && pr.IsDeleted == false
                //                                   select pr.TeamId).SingleOrDefault();

                //        //Adding the Freelancer in the team
                //        TeamUser teamusers = new TeamUser
                //        {
                //            UserId = userdetail.UserId,
                //            CreatedBy = userdetail.UserId,
                //            CreatedDate = DateTime.Now,
                //            TeamId = TeamIdForFreelancer
                //        };

                //        dbcontext.TeamUsers.Add(teamusers);

                //        #region Adding ProjectId in UserSettingConfig
                //        /* getting all Settings for the user  */
                //        UserSettingConfig usrsetting = (from usrset in dbcontext.UserSettingConfigs
                //                                        join set in dbcontext.SettingConfigs on usrset.NotificationId equals set.Id
                //                                        where set.IsActive == true
                //                                        && usrset.CompanyId == freelancerInfo.CompanyId
                //                                        && usrset.UserId == userdetail.UserId
                //                                        && set.NotificationName == "Project Update"
                //                                        select usrset).FirstOrDefault();

                //        if (usrsetting.ProjectId != null && usrsetting.ProjectId != string.Empty)
                //        {
                //            usrsetting.ProjectId += "," + freelancerInfo.ProjectId.ToString();
                //        }
                //        else
                //        {
                //            usrsetting.ProjectId = freelancerInfo.ProjectId.ToString();
                //        }

                //        #endregion

                //        dbcontext.SaveChanges();

                //    }

                //    freelancer.IskeyActive = false;
                //    freelancer.IsAccepted = true;
                //    dbcontext.SaveChanges();
                //    msg = "freelancerRegistrationSuccess";

                //}
                //else
                //{
                //    msg = "error";
                //}

            #endregion

            string UserId = Guid.NewGuid().ToString();

            using (var con = new SqlConnection(ConnectionString))
            {
                msg = con.Query<string>("usp_RegisterFreelancer", new {
                    @FreelancerInvitations_Activationkey = freelancerInfo.Activationkey,
                    @FreelancerInvitations_ValidSentDate = ValidSentDate,
                    @FreelancerInvitations_CompanyId = freelancerInfo.CompanyId,
                    @FreelancerInvitations_RoleId = freelancerInfo.RoleId,
                    @FreelancerInvitations_ProjectId = freelancerInfo.ProjectId,
                    @FreelancerInvitations_UserRate = freelancerInfo.UserRate,
                    @CurrentDate = DateTime.Now,
                    @Email = model.Email,
                    @Password = model.Password, FirstName = model.FirstName, LastName = model.LastName, UserId = UserId, 
                    EmailConfirmationCode = EmailUtility.GetUniqueKey(8),
                    Rate = model.Rate == 0 ? 10 : model.Rate,
                    UserName = model.UserName,
                    ProfilePhoto = FinalFilename
                }, commandType: CommandType.StoredProcedure).Single();
            }

            if (msg == "freelancerRegistrationSuccess")
            {
                var account = new StripeAccountCreateOptions();
                account.Email = model.Email;
                account.Country = "US";
                account.FromRecipient = "";
                account.Type = "Business Account";
                var accountService = new StripeAccountService();
                StripeAccount response = accountService.Create(account);

                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = "update userdetails set StripeUserId = @StripeUserId,IsStripeAccountVerified = 0 where UserId = @UserId ";
                    con.Execute(sqlquery, new { UserId = UserId, StripeUserId = response.Id });
                }
                    
            }

            return msg;                                              
        }                                                                
                                                                        

        private void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(100);//(image.Width * scaleFactor);
                var newHeight = (int)(100);//(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }


        private void GenerateIcon(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(50);//(image.Width * scaleFactor);
                var newHeight = (int)(50);//(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }


        public List<MODEL.FreelancerInfoToConfirmLink> getFreelancerInfo(string UserName, int CompanyId)
        {
            //List<MODEL.FreelancerInfoToConfirmLink> freelancer = (from frlncr in dbcontext.UserDetails
            //                                                      where frlncr.UserName.Contains(UserName)
            //                                                      && frlncr.IsDeleted == false
            //                                                      select new MODEL.FreelancerInfoToConfirmLink
            //                                                      {
            //                                                          FirstName = frlncr.FirstName,
            //                                                          LastName = frlncr.LastName,
            //                                                          Email = frlncr.Email,
            //                                                          ProfilePic = frlncr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + frlncr.ProfilePhoto,
            //                                                          LastRoleInCompany = (from freelancerInv in dbcontext.FreelancerInvitations
            //                                                                               where freelancerInv.CompanyId == CompanyId
            //                                                                               select freelancerInv.RoleId).FirstOrDefault()
            //                                                      }).ToList();

            List<MODEL.FreelancerInfoToConfirmLink> freelancer;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlQuery = "select frlncr.FirstName,frlncr.LastName,frlncr.Email,case when frlncr.ProfilePhoto IS NULL then 'Uploads/Default/profile.png' else 'Uploads/Profile/Thumbnail/' + frlncr.ProfilePhoto  end as ProfilePic, " +
                                  "(select freelancerInv.RoleId from FreelancerInvitations as freelancerInv where freelancerInv.CompanyId = @CompanyId) as LastRoleInCompany from UserDetails as frlncr "  +
                                  "where contains(frlncr.UserName,UserName) and frlncr.IsDeleted = 0";
                freelancer = con.Query<MODEL.FreelancerInfoToConfirmLink>(sqlQuery,new { CompanyId = CompanyId }).ToList();
            }


            return freelancer;
        }


        public List<MODEL.FreelancerInfoToConfirmLink> GetAllFreelancer()
        {
            //List<MODEL.FreelancerInfoToConfirmLink> freelancer = (from user_detail in dbcontext.UserDetails
            //                                                      where user_detail.IsFreelancer == true
            //                                                      && user_detail.IsDeleted == false
            //                                                      select new MODEL.FreelancerInfoToConfirmLink
            //                                                      {
            //                                                          UserName = user_detail.UserName,
            //                                                          FirstName = user_detail.FirstName,
            //                                                          LastName = user_detail.LastName,
            //                                                          Email = user_detail.Email,
            //                                                          ProfilePic = user_detail.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + user_detail.ProfilePhoto,
            //                                                      }).ToList();

            List<MODEL.FreelancerInfoToConfirmLink> freelancer;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlQuery = "select user_detail.UserName,user_detail.FirstName,user_detail.LastName,user_detail.Email,case when user_detail.ProfilePhoto IS NULL then 'Uploads/Default/profile.png' else 'Uploads/Profile/Thumbnail/' + user_detail.ProfilePhoto end as ProfilePic " +
                                  "from UserDetails as user_detail where user_detail.IsFreelancer =1 and user_detail.IsDeleted = 0";
                freelancer = con.Query<MODEL.FreelancerInfoToConfirmLink>(sqlQuery).ToList();
            }

            return freelancer;
        }


        public List<MODEL.CompaniesForFreelancer> getCompaniesForFreelancer(string UserId)
        {
            //List<MODEL.CompaniesForFreelancer> GetCompanyList1 = (from ucRole in dbcontext.UserCompanyRoles
            //                                                     join cmp in dbcontext.Companies on ucRole.CompanyId equals cmp.Id
            //                                                     where ucRole.UserId == UserId
            //                                                     select new MODEL.CompaniesForFreelancer
            //                                                     {
            //                                                         CompanyId = ucRole.CompanyId.Value,
            //                                                         CompanyName = cmp.Name
            //                                                     }).ToList();

            List<MODEL.CompaniesForFreelancer> GetCompanyList;
            using (var con = new SqlConnection(ConnectionString))
            {
                GetCompanyList = con.Query<MODEL.CompaniesForFreelancer>("usp_getCompaniesForFreelancer", new { UserId= UserId },commandType : CommandType.StoredProcedure).ToList();
            }


            return GetCompanyList;
        }

        public MODEL.FreelancerModel GetFreelancerRoleForCompany(string UserId, int CompanyId)
        {

            //MODEL.FreelancerModel Freelancer1 = (from usrdetail in dbcontext.UserDetails
            //                                    join usrcmpnyrole in dbcontext.UserCompanyRoles on usrdetail.UserId equals usrcmpnyrole.UserId
            //                                    where usrdetail.UserId == UserId && usrcmpnyrole.CompanyId == CompanyId
            //                                    && usrdetail.IsDeleted == false
            //                                    select new MODEL.FreelancerModel
            //                                    {
            //                                        Email = usrdetail.Email,
            //                                        RoleId = usrcmpnyrole.RoleId.Value

            //                                    }).FirstOrDefault();

            MODEL.FreelancerModel Freelancer;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlQuery = "select usrdetail.Email,usrcmpnyrole.RoleId from UserDetails as usrdetail inner join UserCompanyRoles as usrcmpnyrole on usrdetail.UserId = usrcmpnyrole.UserId " +
                                  "where usrdetail.UserId = @UserId and usrcmpnyrole.CompanyId = @CompanyId and usrdetail.IsDeleted = 0";
                Freelancer = con.Query<MODEL.FreelancerModel>(sqlQuery, new { UserId = UserId, CompanyId= CompanyId }).FirstOrDefault();
            }

            return Freelancer;
        }


        public MODEL.confirmEmailOrHandleModel confirmEmailOrHandle(MODEL.FreelancerInfoToSendInvitationModel model)
        {
            //MODEL.confirmEmailOrHandleModel Freelancer = (from usr in dbcontext.UserDetails
            //                                              join usrcomrol in dbcontext.UserCompanyRoles on usr.UserId equals usrcomrol.UserId
            //                                              join com in dbcontext.Companies on usrcomrol.CompanyId equals com.Id
            //                                              where (model.chkInvitationType == "email" ? usr.Email == model.EmailOrHandle : usr.UserName == model.EmailOrHandle)
            //                                              && usr.IsFreelancer == true && usr.IsDeleted == false
            //                                              && com.Id == model.CompanyId
            //                                              select new MODEL.confirmEmailOrHandleModel
            //                                              {
            //                                                  FirstName = usr.FirstName,
            //                                                  LastName = usr.LastName,
            //                                                  Email = usr.Email,
            //                                                  InvitationStatus = "Already associated with " + com.Name,
            //                                                  isEmailOrHandleActive = false,
            //                                                  ProfilePic = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + usr.ProfilePhoto,
            //                                              }).FirstOrDefault();

            MODEL.confirmEmailOrHandleModel Freelancer;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlquery = "select usr.FirstName,usr.LastName,usr.Email,'Already associated with ' + com.Name as InvitationStatus,0 as isEmailOrHandleActive, " +
                                  "case when usr.ProfilePhoto IS NULL then 'Uploads/Default/profile.png' else 'Uploads/Profile/Thumbnail/' + usr.ProfilePhoto end as ProfilePic " +
                                  "from UserDetails as usr inner join UserCompanyRoles as usrcomrol on usr.UserId = usrcomrol.UserId inner join Companies as com on usrcomrol.CompanyId = com.Id " +
                                  "where usr.Email = case when @chkInvitationType = 'email' then @EmailOrHandle else usr.Email end and  usr.UserName = case when @chkInvitationType <> 'email' then @EmailOrHandle else usr.UserName  end " +
                                  "and usr.IsFreelancer = 1 and usr.IsDeleted = 0 and com.Id = @CompanyId ";
                Freelancer = con.Query<MODEL.confirmEmailOrHandleModel>(sqlquery, new { CompanyId = model.CompanyId, EmailOrHandle = model.EmailOrHandle, chkInvitationType = model.chkInvitationType }).FirstOrDefault();
            }


                if (Freelancer == null)
                {
                    //Freelancer = (from freeInv in dbcontext.FreelancerInvitations
                    //              where freeInv.FreelancerEmail == model.EmailOrHandle
                    //              && freeInv.ProjectId == 0
                    //              && freeInv.CompanyId == model.CompanyId
                    //              && freeInv.IskeyActive == true
                    //              select new MODEL.confirmEmailOrHandleModel
                    //              {
                    //                  Email = freeInv.FreelancerEmail,
                    //                  isEmailOrHandleActive = false,
                    //                  ProfilePic = "Uploads/Default/profile.png",
                    //                  InvitationStatus = "Invitation Already Sent"
                    //              }).FirstOrDefault();


                using (var con = new SqlConnection(ConnectionString))
                {
                    string sqlquery = "select freeInv.FreelancerEmail as Email,0 as isEmailOrHandleActive,'Uploads/Default/profile.png' as ProfilePic,'Invitation Already Sent' as InvitationStatus " +
                                       "from FreelancerInvitations freeInv where freeInv.FreelancerEmail = @EmailOrHandle and freeInv.ProjectId = 0 " +
                                       "and freeInv.CompanyId = @CompanyId and freeInv.IskeyActive = 1";
                    Freelancer = con.Query<MODEL.confirmEmailOrHandleModel>(sqlquery,new { EmailOrHandle= model.EmailOrHandle, CompanyId = model.CompanyId }).FirstOrDefault();
                }



                    if (Freelancer == null)
                    {

                    //string CompanyName = (from c in dbcontext.Companies where c.Id == model.CompanyId select c.Name).FirstOrDefault();

                    //string CompanyName;
                    //using (var con = new SqlConnection(ConnectionString))
                    //{
                    //    string sqlquery = "select Name from Companies where Id = @CompanyId";
                    //    CompanyName = con.Query<string>(sqlquery, new { CompanyId = model.CompanyId }).FirstOrDefault();
                    //}

                        //Freelancer = (from usr in dbcontext.UserDetails
                        //              where (model.chkInvitationType == "email" ? usr.Email == model.EmailOrHandle : usr.UserName == model.EmailOrHandle)
                        //              && usr.IsFreelancer == true && usr.IsDeleted == false
                        //              select new MODEL.confirmEmailOrHandleModel
                        //              {
                        //                  FirstName = usr.FirstName,
                        //                  LastName = usr.LastName,
                        //                  Email = usr.Email,
                        //                  isEmailOrHandleActive = true,
                        //                  InvitationStatus = "Member of Tighten, but not associated with " + CompanyName + " yet",
                        //                  ProfilePic = usr.ProfilePhoto == null ? "Uploads/Default/profile.png" : "Uploads/Profile/Thumbnail/" + usr.ProfilePhoto,
                        //              }).FirstOrDefault();

                    using (var con = new SqlConnection(ConnectionString))
                    {
                        string sqlquery = "select usr.FirstName,usr.LastName,usr.Email,1 as isEmailOrHandleActive,'Member of Tighten, but not associated with ' + (select Name from Companies where Id = @CompanyId) + ' yet ' as InvitationStatus, " +
                                          "case when usr.ProfilePhoto IS NULL then 'Uploads/Default/profile.png' else 'Uploads/Profile/Thumbnail/' + usr.ProfilePhoto end as  ProfilePic " +
                                          "from UserDetails as usr where usr.Email = case when @chkInvitationType = 'email' then @EmailOrHandle else usr.Email end and usr.UserName = case when @chkInvitationType <> 'email' then @EmailOrHandle else usr.UserName end " +
                                          "and usr.IsFreelancer = 1 and usr.IsDeleted = 0";
                        Freelancer = con.Query<MODEL.confirmEmailOrHandleModel>(sqlquery, new { EmailOrHandle = model.EmailOrHandle, chkInvitationType = model.chkInvitationType, CompanyId= model.CompanyId }).FirstOrDefault();
                    }

                }

                }

            if (Freelancer == null)
            {
                Freelancer = new MODEL.confirmEmailOrHandleModel()
                {
                    isEmailOrHandleActive = true,
                    InvitationStatus = "This person is not on Tighten yet",
                    ProfilePic = "Uploads/Profile/Thumbnail/"
                };
            }

            return Freelancer;
        }


        public MODEL.FreelancerForProjectModel GetAllFreelancerInProject(int ProjectId)
        {
            MODEL.FreelancerForProjectModel ReturnModel = new MODEL.FreelancerForProjectModel();

            //List<MODEL.FreelancerForProject> allFreelancersForProjects = (from usr in dbcontext.UserDetails
            //                                                              join prjctusr in dbcontext.ProjectUsers on usr.UserId equals prjctusr.UserId
            //                                                              where prjctusr.ProjectId == ProjectId
            //                                                              && usr.IsFreelancer == true && usr.IsDeleted == false
            //                                                              select new MODEL.FreelancerForProject
            //                                                              {
            //                                                                  FreelancerName = usr.FirstName,
            //                                                                  FreelancerEmail = usr.Email,
            //                                                                  FreelancerUserId = usr.UserId
            //                                                              }).ToList();

            List<MODEL.FreelancerForProject> allFreelancersForProjects;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlQuery = "select usr.FirstName as FreelancerName,usr.Email as FreelancerEmail,usr.UserId as FreelancerUserId from UserDetails as usr" +
                                  "inner join ProjectUsers prjctusr on usr.UserId = prjctusr.UserId where prjctusr.ProjectId = @ProjectId and usr.IsFreelancer = 1 and usr.IsDeleted = 0";
                allFreelancersForProjects = con.Query<MODEL.FreelancerForProject>(sqlQuery,new { ProjectId = ProjectId }).ToList();
            }


            //List<MODEL.FreelancerForProject> selectedFreelancersForProjects = (from usrdet in dbcontext.UserDetails
            //                                                                   join clienttest in dbcontext.ClientTestimonials on usrdet.UserId equals clienttest.UserId
            //                                                                   join prjctusr in dbcontext.ProjectUsers on clienttest.UserId equals prjctusr.UserId
            //                                                                   where prjctusr.ProjectId == ProjectId
            //                                                                   && usrdet.IsFreelancer == true && usrdet.IsDeleted == false
            //                                                                   select new MODEL.FreelancerForProject
            //                                                                   {
            //                                                                       FreelancerName = usrdet.FirstName,
            //                                                                       FreelancerEmail = usrdet.Email,
            //                                                                       FreelancerUserId = usrdet.UserId
            //                                                                   }).ToList();

            List<MODEL.FreelancerForProject> selectedFreelancersForProjects;
            using (var con = new SqlConnection(ConnectionString))
            {
                string sqlQuery = "select usrdet.FirstName as FreelancerName,usrdet.Email as FreelancerEmail,usrdet.UserId as FreelancerUserId from UserDetails as usrdet" +
                                  "inner join ClientTestimonials as clienttest on usrdet.UserId = clienttest.UserId" +
                                  "inner join ProjectUsers as prjctusr on clienttest.UserId = prjctusr.UserId where prjctusr.ProjectId = @ProjectId and usrdet.IsFreelancer = 1 and usrdet.IsDeleted = 0";
                selectedFreelancersForProjects = con.Query<MODEL.FreelancerForProject>(sqlQuery, new { ProjectId = ProjectId }).ToList();
            }


            ReturnModel.allFreelancersForProjects = allFreelancersForProjects;
            ReturnModel.selectedFreelancersForProjects = selectedFreelancersForProjects;

            return ReturnModel;

        }





    }

}
