﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using Dapper;

namespace TIGHTEN.DATA
{
    public class PaymentDetail : BaseClass
    {
       

        public List<MODEL.PaymentDetailModel> GetPaymentDetail(int CompanyId)
        {
            List<MODEL.PaymentDetailModel> PaymentDetaillist;


            using (var con = new SqlConnection(ConnectionString))
            {

                PaymentDetaillist = con.Query<MODEL.PaymentDetailModel>("Usp_GetPaymentDetail", new
                {
                    CompanyId = CompanyId,

                }, commandType: CommandType.StoredProcedure).ToList();
            }




            //PaymentDetaillist = (from pay in dbcontext.PaymentDetails
            //                                                    where pay.CompanyId == CompanyId
            //                                                    && pay.IsDeleted == false
            //                                                    select new MODEL.PaymentDetailModel
            //                                                    {
            //                                                        Id = pay.Id,
            //                                                        CompanyId = pay.CompanyId,
            //                                                        CardName = pay.CardName,
            //                                                        CardDescription = pay.CardDescription,
            //                                                        CreditCardNumber = pay.CreditCardNumber,
            //                                                        ExpiryMonth = pay.ExpiryMonth,
            //                                                        ExpiryYear = pay.ExpiryYear,
            //                                                        CvvNumber = pay.CvvNumber,
            //                                                        IsDefault = pay.IsDefault

            //                                                    }
            //                                                    ).ToList();



            return PaymentDetaillist;
        }



        // code with store procedure
        public string insertPaymentDetail(MODEL.PaymentDetailModel model)
        {
            string msg = string.Empty;
            int Id ;
            using (var con = new SqlConnection(ConnectionString))

            {
                Id = con.Query<int>("Usp_InsertPaymentDetailCompany", new
                {
                    CompanyId = model.CompanyId,
                    CardName = model.CardName,
                    CardDescription = model.CardDescription,
                    CreditCardNumber = model.CreditCardNumber,
                    ExpiryMonth = model.ExpiryMonth,
                    ExpiryYear = model.ExpiryYear,
                    CvvNumber = model.CvvNumber,
                    CreatedBy = model.UserId,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = model.UserId,
                    ModifiedDate = DateTime.Now,
                    //IsActive = true,
                    IsDefault = model.IsDefault


                }, commandType: CommandType.StoredProcedure).SingleOrDefault();


            }

            if (Id == 1)
            {
                msg = "PaymentDetailAddedSuccess";
            }

            else if(Id == -1) 
            {
                msg = "DuplicateCardName";
            }
            else if(Id == -2) {

         
                msg = "DuplicatePaymentDetail";

            }

            return msg;
    
        }



        public int updatePaymentDetail(MODEL.PaymentDetailModel model)
        {
            string msg = string.Empty;

            int Id;
            using (var con = new SqlConnection(ConnectionString))

            {
                Id = con.Query<int>("Usp_UpdatePaymentDetailCompany", new
                {
             
                    Id              =      model.Id,
                    CompanyId = model.CompanyId,
                    CardName = model.CardName,
                    CardDescription = model.CardDescription,
                    CreditCardNumber = model.CreditCardNumber,
                    ExpiryMonth = model.ExpiryMonth,
                    ExpiryYear = model.ExpiryYear,
                    CvvNumber = model.CvvNumber,
                    CreatedBy = model.UserId,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = model.UserId,
                    ModifiedDate = DateTime.Now,
                    //IsActive = true,
                    IsDefault = model.IsDefault

                }, commandType: CommandType.StoredProcedure).SingleOrDefault();


            }
            return Id;

            //var isPaymentDetailExist = (from pay in dbcontext.PaymentDetails
            //                            where pay.CreditCardNumber == model.CreditCardNumber
            //                            && pay.CompanyId == model.CompanyId
            //                            && pay.Id != model.Id
            //                            && pay.IsDeleted == false
            //                            select pay.Id).ToList();

            //if (isPaymentDetailExist.Count() > 0)
            //{
            //    msg = "DuplicatePaymentDetail";
            //}
            //else
            //{
            //    var isCardNameExist = (from pay in dbcontext.PaymentDetails
            //                           where pay.CardName == model.CardName
            //                           && pay.CompanyId == model.CompanyId
            //                           && pay.Id != model.Id
            //                           && pay.IsDeleted == false
            //                           select pay.Id).ToList();

            //    if (isCardNameExist.Count() > 0)
            //    {
            //        msg = "DuplicateCardName";
            //    }
            //    else
            //    {
            //        if (model.IsDefault == true)
            //        {
            //            var AllPayment = (from p in dbcontext.PaymentDetails
            //                              where p.CompanyId == model.CompanyId
            //                              && p.IsDeleted == false
            //                              select p).ToList();

            //            foreach (var item in AllPayment)
            //            {
            //                item.IsDefault = false;
            //            }

            //            dbcontext.SaveChanges();
            //        }

            //        var PaymentDetail = (from pay in dbcontext.PaymentDetails
            //                             where pay.Id == model.Id
            //                             && pay.CompanyId == model.CompanyId
            //                             && pay.IsDeleted == false
            //                             select pay).FirstOrDefault();


            //        PaymentDetail.CompanyId = model.CompanyId;
            //        PaymentDetail.CardName = model.CardName;
            //        PaymentDetail.CardDescription = model.CardDescription;
            //        PaymentDetail.CreditCardNumber = model.CreditCardNumber;
            //        PaymentDetail.ExpiryMonth = model.ExpiryMonth;
            //        PaymentDetail.ExpiryYear = model.ExpiryYear;
            //        PaymentDetail.CvvNumber = model.CvvNumber;
            //        PaymentDetail.ModifiedBy = model.UserId;
            //        PaymentDetail.ModifiedDate = DateTime.Now;
            //        PaymentDetail.IsActive = true;
            //        PaymentDetail.IsDefault = model.IsDefault;

            //        dbcontext.SaveChanges();

            //        msg = "PaymentDetailUpdatedSuccess";

            //    }
            //}


        }



        public void deletePaymentDetail(int Id)
        {

            //TIGHTEN.ENTITY.PaymentDetail paymentDetailEntity = (from m in dbcontext.PaymentDetails
            //                                                    where m.Id == Id && m.IsDeleted == false
            //                                                    select m).SingleOrDefault();

            //paymentDetailEntity.IsDeleted = false;
            //dbcontext.SaveChanges();

            string msg = string.Empty;
            using (var con = new SqlConnection(ConnectionString))
            {
                
                
                string query = "update PaymentDetails set IsDeleted = 1 where Id = @Id";
                msg = con.Query<string>(query, new
                {

                    Id = Id,
                }).SingleOrDefault();


            }
          

        }



    }
}
