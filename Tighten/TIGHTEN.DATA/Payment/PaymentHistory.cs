﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIGHTEN.ENTITY;
using TIGHTEN.UTILITIES;
using Dapper;

namespace TIGHTEN.DATA
{
    public class PaymentHistory : BaseClass 
    {
        AppContext dbcontext;

        public List<MODEL.PaymentHistoryModel> GetPaymentHistory(string UserId, bool isFreelancer, int TeamId, int CompanyId, string subscriptionType, int ProjectId, string FreelancerUserId)
        {

            List<MODEL.PaymentHistoryModel> Transactionlist = new List<MODEL.PaymentHistoryModel>();

            {
                using (var con = new SqlConnection(ConnectionString))
                {

                    var result = con.QueryMultiple("usp_GetPaymentHistory", new 
                    { UserId = UserId, isFreelancer = isFreelancer, TeamId = TeamId,
                     CompanyId = CompanyId, subscriptionType = subscriptionType, ProjectId = ProjectId, FreelancerUserId = FreelancerUserId }, 
                    commandType: System.Data.CommandType.StoredProcedure);

                    Transactionlist = result.Read<MODEL.PaymentHistoryModel>().ToList();

                }

               
            }
             return Transactionlist;
        }

        // This method using with  Entity 
        public List<MODEL.PaymentHistoryModel> GetPaymentHistory1(string UserId, bool isFreelancer, int TeamId, int CompanyId, string subscriptionType, int ProjectId, string FreelancerUserId)
        {

            List<MODEL.PaymentHistoryModel> Transactionlist = new List<MODEL.PaymentHistoryModel>();

            if (isFreelancer == true)
            {
                Transactionlist = (from tran in dbcontext.Transactions
                                   join usr in dbcontext.UserDetails on tran.TransactionBy equals usr.UserId
                                   where usr.CompanyId == (CompanyId == 0 ? usr.CompanyId : CompanyId)
                                   && tran.TransactionFor == UserId && usr.IsDeleted == false
                                   select new MODEL.PaymentHistoryModel
                                   {
                                       Id = tran.Id,
                                       AmountPaid = tran.AmountPaid,
                                       TransactionDate = tran.TransactionDate,
                                       TransactionType = tran.TransactionType,
                                       TransactionID = tran.TransactionID,
                                   }
                                  ).ToList();

            }
            else
            {
                if (subscriptionType != "Invoice")
                {

                    Transactionlist = (from tran in dbcontext.Transactions
                                       join usr in dbcontext.UserDetails on tran.TransactionFor equals usr.UserId
                                       join usrcomrol in dbcontext.UserCompanyRoles on usr.UserId equals usrcomrol.UserId
                                       where usrcomrol.CompanyId == CompanyId && usr.IsDeleted == false
                                       //&& tran.TransactionBy == UserId
                                       && (subscriptionType == "All" ? tran.TransactionType == tran.TransactionType : tran.TransactionType == subscriptionType)
                                       select new MODEL.PaymentHistoryModel
                                       {
                                           Id = tran.Id,
                                           AmountPaid = tran.AmountPaid,
                                           TransactionDate = tran.TransactionDate,
                                           TransactionType = tran.TransactionType,
                                           TransactionID = tran.TransactionID,
                                           TransactionBy = usr.FirstName,
                                           TransactionMadeFor = (from u in dbcontext.UserDetails
                                                                 where u.UserId == tran.TransactionFor && u.IsDeleted == false
                                                                 select u.FirstName).FirstOrDefault()
                                       }
                                      ).ToList();
                }
                else
                {

                    Transactionlist = (from AllData in 
                                      (from tran in dbcontext.Transactions
                                       join inv in dbcontext.Invoices on tran.EventID equals inv.Id
                                       join invdet in dbcontext.InvoiceDetails on inv.Id equals invdet.InvoiceId
                                       join tod in dbcontext.ToDos on invdet.TodoId equals tod.Id
                                       join sec in dbcontext.Sections on tod.SectionId equals sec.Id
                                       join prjct in dbcontext.Projects on sec.ProjectId equals prjct.Id
                                       join tm in dbcontext.Teams on prjct.TeamId equals tm.Id

                                       where inv.CompanyId == CompanyId && tm.IsDeleted == false /*&& tran.TransactionBy == UserId*/
                                       && tran.TransactionType == "Invoice" && prjct.IsDeleted == false
                                       && inv.UserId == (FreelancerUserId == "0" ? inv.UserId : FreelancerUserId)
                                       && prjct.Id == (ProjectId == 0 ? prjct.Id : ProjectId) 
                                       && tm.Id == (TeamId == 0 ? tm.Id : TeamId) && tod.IsDeleted == false
                                       select new { tran}
                                       )
                                       group AllData by new {AllData.tran.Id, AllData.tran.AmountPaid, AllData.tran.TransactionDate, AllData.tran.TransactionType, AllData.tran.TransactionID,  AllData.tran.TransactionFor, AllData.tran.TransactionBy } into GroupedData
                                       select new MODEL.PaymentHistoryModel
                                       {
                                           Id = GroupedData.Key.Id,
                                           AmountPaid = GroupedData.Key.AmountPaid,
                                           TransactionDate = GroupedData.Key.TransactionDate,
                                           TransactionType = GroupedData.Key.TransactionType,
                                           TransactionID = GroupedData.Key.TransactionID,
                                           TransactionBy = (from u in dbcontext.UserDetails
                                                            where u.UserId == GroupedData.Key.TransactionBy
                                                            && u.IsDeleted == false
                                                            select u.FirstName).FirstOrDefault(),
                                           TransactionMadeFor = (from u in dbcontext.UserDetails
                                                                 where u.UserId == GroupedData.Key.TransactionFor
                                                                 && u.IsDeleted == false
                                                                 select u.FirstName).FirstOrDefault()
                                       }
                                      ).ToList();
                }
            }

            return Transactionlist;
        }




        public List<MODEL.ProjectModel.AllFreelancersForCompanyModel> GetFreelancersForSelectedProject(int ProjectId, int CompanyId)
        {
            List<MODEL.ProjectModel.AllFreelancersForCompanyModel> list;

            using (var con = new SqlConnection(ConnectionString))
            {
                list = con.Query<MODEL.ProjectModel.AllFreelancersForCompanyModel>("Usp_GetFreelancersForSelectedProject",
                new
                {
                    ProjectId = ProjectId,
                    CompanyId = CompanyId
                },
                commandType: System.Data.CommandType.StoredProcedure).ToList(); 

            }





                //List<MODEL.ProjectModel.AllFreelancersForCompanyModel> list = (from usr in dbcontext.UserDetails
                //                                                           join prjctusr in dbcontext.ProjectUsers on usr.UserId equals prjctusr.UserId
                //                                                           join prjct in dbcontext.Projects on prjctusr.ProjectId equals prjct.Id
                //                                                           where prjct.CompanyId == CompanyId && prjct.IsDeleted == false
                //                                                           && prjct.Id == (ProjectId == 0 ? prjct.Id : ProjectId)
                //                                                           && usr.IsFreelancer == true && usr.IsDeleted == false
                //                                                           select new MODEL.ProjectModel.AllFreelancersForCompanyModel
                //                                                           {
                //                                                               UserId = usr.UserId,
                //                                                               FreelancerName = usr.FirstName
                //                                                           }
                //                                                           ).ToList();


            return list;
        }




    }
}
