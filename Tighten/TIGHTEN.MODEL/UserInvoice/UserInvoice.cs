﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class UserInvoice
    {

        public class InvoiceModel
        {
            public int Id { get; set; }
            public int CompanyId { get; set; }
            public string InvoiceNumber { get; set; }
            public string UserId { get; set; }
            public string ApprovedBy { get; set; }
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public string SendTo { get; set; }
            public string Message { get; set; }
            public DateTime InvoiceDate { get; set; }
            public string TotalHours { get; set; }
            public decimal TotalAmount { get; set; }
            public bool InvoiceApprovedStatus { get; set; }
            public string UserName { get; set; }
            public string TodoName { get; set; }
            public string ProfilePic { get; set; }
            public string TodoDescription { get; set; }
            public decimal Rate { get; set; }
            public bool IsInvoiceForward { get; set; }
            public bool IsActiveInvoiceForward { get; set; }
            public bool IsInvoiceForwardForDetails { get; set; }
            public bool IsPaid { get; set; }
            public string PaidBy { get; set; }
            public bool IsRejected { get; set; }
            public string RejectedBy { get; set; }
            public string RejectReason { get; set; }

            public UserDetailForInvoicePaidAndReject PaidByUserDetail { get; set; }
            public UserDetailForInvoicePaidAndReject RejectedByUserDetail { get; set; }
        }

        public class UserDetailForInvoicePaidAndReject
        {
            public string UserName { get; set; }
            public string UserId { get; set; }
            public string UserProfilePic { get; set; }
        }

        public class InvoiceModelBothApprovedandNotApproved
        {
            public List<InvoiceModel> list_Paid { get; set; }
            public List<InvoiceModel> list_NotPaid { get; set; }
            public List<InvoiceModel> list_Rejected { get; set; }
        }
        public class InvoiceDetailModel
        {
            public string TodoName { get; set; }
            public string TodoDescription { get; set; }
            public string hours { get; set; }
            public decimal Amount { get; set; }
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public int TodoType { get; set; }
             
        }

        public class InvoiceStatus
        {
            public int InvoiceId { get; set; }
            public string UserId { get; set; }
            public string Token { get; set; }
            public string Index { get; set; }

        }

        public class InvoiceRejectModel
        {
            public int InvoiceId { get; set; }
            public string UserId { get; set; }
            public string Reason { get; set; }
        }

        public class InvoiceQuery
        {
            public string UserId { get; set; }
            public int CompanyId { get; set; }
            public int InvoiceStatus { get; set; }
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public int InvoiceType { get; set; }
            public string FreelancerUserId { get; set; }
            public int RoleId { get; set; }

        }


        public class AdminInvoiceDetailsRequiredParams
        {
            public int InvoiceId { get; set; }
            public string UserId { get; set; }
            public int CompanyId { get; set; }
            public bool IsInvoiceForwardForDetails { get; set; }
        }


        public class ProjectMembersToForwardInvoice
        {
            public string UserId { get; set; }

            public string NameWithEmail { get; set; }
        }


        public class UserCardPaymentModel
        {
            public string Card { get; set; }

            public string Month { get; set; }

            public string Year { get; set; }

            public string Cvv { get; set; }

            public string FreelancerInvoiceAmount { get; set; }

            public string vendorInvoiceAmount { get; set; }

            public string StripeFees { get; set; }

            public string TightenCharges { get; set; }

            public string NetAmountForAdmin { get; set; }

        }


        public class ProjectMembersToForwardInvoice_QueryParams
        {
            public int invoiceId { get; set; }

            public string UserId { get; set; }

        }


        public class ForwardInvoice
        {
            public int InvoiceId { get; set; }
            public string ForwardTo { get; set; }
            public string UserId { get; set; }
            public int CompanyId { get; set; }

        }


        public class InvoiceRejectDetailsModel
        {
            public int TodoId { get; set; }
            public string TodoTimeLogIds { get; set; }
        }




    }


}
