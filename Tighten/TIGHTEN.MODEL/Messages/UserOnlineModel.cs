﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class UserOnlineModel
    {


        public int Id { get; set; }
        public string UserId { get; set; }
        /* Email is the email address of the user for eg: abc@abccompany.com  */
        public string UserName { get; set; }
        public string Email { get; set; }
        /*  FirstName is the first name of the user for eg : abc */
        public string FirstName { get; set; }
        /* LastName is the last name of the user for eg: def */
        public string LastName { get; set; }
        /* Gender is the gender/sex of the user for eg: if male Gender is 1, if female Gender is 2 , if dont prefer to answer it is 3 */
        public int? Gender { get; set; }
        public string ProfilePhoto { get; set; }
        public double DateTimeDifference { get; set; }
        public DateTime LastOnlineTime { get; set; }
    }
}
