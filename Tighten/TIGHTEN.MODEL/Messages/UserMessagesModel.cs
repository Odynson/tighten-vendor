﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class UserMessagesModel
    {
        public int Id { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string SenderProfileImage { get; set; }
        public string ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverProfileImage { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
        public bool IsSender { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }


    public class UserLeftDisplayModel
    {
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
        public string SenderProfileImage { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public int UnreadCount { get; set; }
        public string CompanyName { get; set; }
    }
}
