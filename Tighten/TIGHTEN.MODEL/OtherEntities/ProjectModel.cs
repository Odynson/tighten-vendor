﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TIGHTEN.MODEL
{
    public class ProjectModel
    {
        public class Project
        {
            public int ProjectId { get; set; }

            public int TeamId { get; set; }
            public int AccessLevel { get; set; }
            public string ProjectName { get; set; }
            public int projectHours { get; set; }
            public decimal projectBudget { get; set; }
            public string ProjectDescription { get; set; }
            public string[] ProjectMembers { get; set; }
            public List<FreelancerListingModel> freelancerDetails { get; set; }
            public List<InternalUserDetailsModel> internalUserDetails { get; set; }
            public int isTicked { get; set; }

            public TempDataModel TempData { get; set; }
        }


        public class ProjectWithMembers
        {
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public string TeamName { get; set; }
            public int Accesslevel { get; set; }
            public string ProjectDescription { get; set; }
            public int projectHours { get; set; }
            public decimal projectBudget { get; set; }
            public string TotalSpentHours { get; set; }
            public decimal TotalSpentMoney { get; set; }
            public bool IsComplete { get; set; }
            public DateTime CreatedDate { get; set; }
            public string FilePath { get; set; }
            public string FileName { get; set; }
            public string ActualFileName { get; set; }
            public string AttachmentDescription { get; set; }
            public List<ProjectMembers> ProjectMembers { get; set; }
        }


        public class ProjectMembers
        {
            public int ProjectId { get; set; }
            public string MemberName { get; set; }

            public string MemberEmail { get; set; }

            public string MemberProfilePhoto { get; set; }

            public string MemberUserId { get; set; }

            public string Value { get; set; }

            public string Text { get; set; }
            public string MemberPhoneNo { get; set; }
            public string MemberDesignation { get; set; }
            public string hours { get; set; }
            public decimal Rate { get; set; }

            public bool IsFreelancer { get; set; }
        }

        public class NewModelForProject
        {
            public Project ProjectModel { get; set; }
            public string UserId { get; set; }
            public int CompanyId { get; set; }

        }


        public class TempDataModel
        {
            public decimal FreelancerRate { get; set; }
            public string ProjectAttachments { get; set; }
            public string AttachmentDescription { get; set; }

            public string FileName { get; set; }
            public string ActualFileName { get; set; }
            public string FilePath { get; set; }

            public string FreelancerResponsibilities { get; set; }
        }


        public class UserFavouriteProjects
        {
            public List<ProjectModel.Project> userProjectsForAllOrgs { get; set; }
            public List<ProjectModel.Project> userFavProjects { get; set; }

        }



        public class ProjectWithForFinancialManager
        {
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public Nullable<decimal> AllottedBudget { get; set; }
            public Nullable<decimal> SpentBudget { get; set; }
            public Nullable<decimal> Percentage { get; set; }
            public bool Indicator { get; set; }
        }

        public class AllFreelancersForCompanyModel
        {
            public string UserId { get; set; }
            public string FreelancerName { get; set; }

        }


        public class ProjectOptionsForFreelancerModel
        {
            public int ProjectId { get; set; }
            public int UserRate { get; set; }
            public int WeeklyLimit { get; set; }
            public string[] FreelancerIdList { get; set; }

        }

        public class ParentCompanyInfoForFreelancerModel
        {
            public string UserName { get; set; }
            public string ProjectName { get; set; }
            public string ProjectDescription { get; set; }
            public int? ProjectEstimatedHours { get; set; }
            public string CompanyName { get; set; }
            public int? RoleId { get; set; }

            public bool IsFreelancer { get; set; }
            public string Email { get; set; }
        }


        public class FreelancerAndKeyModel
        {
            public string UserId { get; set; }
            public string Key { get; set; }
        }


        public class ProjectFileModel
        {
            public string FilePath { get; set; }
            public string FileName { get; set; }
            public string ActualFileName { get; set; }

            public int Id { get; set; }

            public int ProjectId { get; set; }
            public string UserId { get; set; }
        }

        public class AttachmentFileModel
        {
            public string FileName { get; set; }
            public string ActualFileName { get; set; }
            public string FilePath { get; set; }
            public int Index { get; set; }
        }


        public class FreelancerListingModel
        {
            public int FreelancerInvitationId { get; set; }
            public string SentBy { get; set; }
            public string FreelancerUserId { get; set; }
            public string FreelancerEmail { get; set; }
            public string FreelancerName { get; set; }
            public string FreelancerProfilePhoto { get; set; }
            public decimal FreelancerProfileRate { get; set; }
            public decimal FreelancerOfferedPrice { get; set; }
            public int FreelancerRoleId { get; set; }
            public string FreelancerRole { get; set; }
            public string FreelancerDocument { get; set; }
            public string FreelancerResponsibilities { get; set; }

            public string FileName { get; set; }
            public string ActualFileName { get; set; }
            public string FilePath { get; set; }

            public string Key { get; set; }
            public bool IsInvitationSent { get; set; }
            public string Status { get; set; }

            public List<ProjectFileModel> PreviousAttachments { get; set; }
            public List<ProjectFileModel> AllAttachments { get; set; }
        }


        public class InternalUserDetailsModel
        {
            public string UserId { get; set; }
            public string ProfilePhoto { get; set; }
            public string FirstName { get; set; }
            public int RoleId { get; set; }
            public string Status { get; set; }
        }



        public class FreelancerToInsertInProjectModel
        {
            public string FreelancerUserId { get; set; }
            public string UserId { get; set; }
            public int ProjectId { get; set; }
            public int FreelancerRoleId { get; set; }
            public string FreelancerEmail { get; set; }

            public int companyId { get; set; }
            public string keyy { get; set; }
            public decimal FreelancerOfferedPrice { get; set; }
            public string FreelancerResponsibilities { get; set; }

            public int TeamId { get; set; }
        }


        public class InternalUserDetailsModal
        {
            public string InternalUserUserId { get; set; }
            public int InternalUserRoleId { get; set; }
        }


        public class FreelancerDetailsModal
        {
            public string FreelancerUserUserId { get; set; }
            public int FreelancerRoleId { get; set; }

            public decimal FreelancerOfferedPrice { get; set; }
            public string FreelancerResponsibilities { get; set; }
        }


        public class UsersToBeAddedOrDeletedModal
        {
            public string MemberUserIdToBeAdded { get; set; }
            public string keyy { get; set; }
        }

        public class NewCreateProjectModel
        {
            public int ProjectId { get; set; }
            public string UserId { get; set; }

            public int CompanyId { get; set; }
            public int AccessLevel { get; set; }
            public string ProjectName { get; set; }

            public string ProjectDescription { get; set; }
            public int ProjectHours { get; set; }
            public decimal ProjectBudget { get; set; }
            public string ActualFileName { get; set; }
            public string FileName { get; set; }
            public String FilePath { get; set; }
            public bool IsQuoteSent { get; set; }

            public string ProjectStartDate { get; set; }
            public string ProjectEndDate { get; set; }

            public bool? IsProjectUpdated { get; set; }

            public string CustomControlData { get; set; }

            public int CustomControlId { get; set; }

            public List<VendorForCompany> VendorList { get; set; }
            public List<ProjectPhaseModel> PhaseList { get; set; }

            public List<newProjectDocument> ProjectDocList { get; set; }

            public List<ProjectRiskModel> RiskList { get; set; }

        }



        public class ProjectRiskModel
        {
            public int Id { get; set; }
            public string ProjectId { get; set; }

            public string RiskTitle { get; set; }
            public string Description { get; set; }
            public bool IsDeleted { get; set; }

            public string Createdon { get; set; }
            public string Severity { get; set; }
            public string Status { get; set; }
            public int NoOfComments { get; set; }
            public List<ProjectRiskCommentsModel> RiskComments { get; set; }

        }
        public class ProjectRiskCommentsModel
        {
            public int Id { get; set; }
            public int ProjectId { get; set; }
            public string UserId { get; set; }

            public int RiskId { get; set; }
            public string Comment { get; set; }

            public bool IsDeleted { get; set; }

            public string Createdon { get; set; }
            public int AgainstComment { get; set; }
            public bool IsAdded { get; set; }
            public string UserName { get; set; }

        }



        public class EditNewProjectModel
        {
            public int ProjectId { get; set; }
            public string UserId { get; set; }

            public int CompanyId { get; set; }
            public int AccessLevel { get; set; }
            public string ProjectName { get; set; }

            public string ProjectDescription { get; set; }
            public int ProjectHours { get; set; }
            public decimal ProjectBudget { get; set; }
            public string ActualFileName { get; set; }
            public string FileName { get; set; }
            public String FilePath { get; set; }
            public bool IsQuoteSent { get; set; }

            public string ProjectStartDate { get; set; }
            public string ProjectEndDate { get; set; }

            public bool? IsProjectUpdated { get; set; }
          
  
            public List<VendorForCompany> VendorList { get; set; }
            public List<ProjectPhaseModel> PhaseList { get; set; }

            public List<newProjectDocument> ProjectDocList { get; set; }

            public CustomControlValueModel CustomControlValueModel { get; set; }

            public List<CustomControlUIModel> CustomControlUIList { get; set; }
            public List<ProjectRiskModel> RiskList { get; set; }

        }


        public class CustomControlValueModel
        {
            public int Id { get; set; }

            public string CustomControlData { get; set; }


        }
        
        public class newProjectDocument
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string ActualFileName { get; set; }
            public string FileName { get; set; }
            public String FilePath { get; set; }


        }


        public class VendorForCompany
        {
            public int Id { get; set; }

            public string CompanyName { get; set; }

            public string Name { get; set; }

            public string EmailAddress { get; set; }

            public int VendorId { get; set; }

            public long PhoneNO { get; set; }

            public int PorjectCount { get; set; }

            public bool IsHired { get; set; }
            public DateTime Invitedon { get; set; }
            public bool IsAdded { get; set; }

            public bool? IsActive { get; set; }

            public int VendorCompanyId { get; set; }
        }

        public class ProjectPhaseModel
        {
            public int PhaseId { get; set; }
            public int ProjectId { get; set; }
            public string Name { get; set; }
            public string Budget { get; set; }
            public string Description { get; set; }
            public string OverAllDescription { get; set; }
            public DateTime? Createdon { get; set; }


            public List<ProjectMileStoneModel> MileStone { get; set; }
            public List<PhaseDocumentsNameModel> PhaseDocumentsName { get; set; }
        }

        public class PhaseDocumentsNameModel
        {
            public int PhaseId { get; set; }
            public int Id { get; set; }
            public string FileName { get; set; }
            public string ActualFileName { get; set; }
            public String FilePath { get; set; }

        }

        public class ProjectMileStoneModel
        {
            public int Id { get; set; }
            public int PhaseId { get; set; }
            public int ProjectId { get; set; }
            public int MilestoneId { get; set; }
            public string Name { get; set; }
            public int Budget { get; set; }
            public int EstimatedHours { get; set; }
            public string Description { get; set; }
            public DateTime? Createdon { get; set; }
            public int TrackId { get; set; }

        }


        public class ProjectPhaseCustom
        {
            public string Name { get; set; }
            public string Budget { get; set; }
            public string Description { get; set; }
            public int Id { get; set; }
            public int PhaseId { get; set; }
            public int PhaseAttachementId { get; set; }
            public string FileNameList { get; set; }
            public string ActualFileNameList { get; set; }
            public string FilePathList { get; set; }
            public string MileNameList { get; set; }
            public string BudgetList { get; set; }
            public string DescriptionList { get; set; }
            public string EstimatedHourList { get; set; }

        }


        public class ProjectRiskCustom
        {
            public int Id { get; set; }
            public int ProjectId { get; set; }
            public string RiskTitle { get; set; }
            public string Description { get; set; }
            public string Severity { get; set; }
            public string Status { get; set; }            
            public string CommentList { get; set; }
            public string AgainstComment { get; set; }
        }
        public class EditProjectRiskCustom
        {
            public string RiskTitle { get; set; }
            public string Severity { get; set; }
            public string Status { get; set; }
            public string Description { get; set; }
            public int RiskId { get; set; }
            public int Id { get; set; }
            public bool IsDeleted { get; set; }
            public int ProjectId { get; set; }
            public string CommentList { get; set; }
            public string AgainstComment { get; set; }
            public string CommentIsDeletedList { get; set; }
        }

        public class ReasonAcceptRejectModal

        {
            public int IsAcceptOrReject { get; set; }

            public string RejectOrAccept { get; set; }

            public int ProjectId { get; set; }

            public int VendorId { get; set; }

            public string ProjectName { get; set; }

            public int CompanyId { get; set; }
        }

        public class ProjectCompleteModal
        {

            public int ProjectId { get; set; }

            public int VendorId { get; set; }

            public string ProjectName { get; set; }

            public string CompanyName { get; set; }

            public string UserName { get; set; }

            public DateTime? ProjectStartDate { get; set; }


            public DateTime? ProjectEndDate { get; set; }

            public int BudgetHours { get; set; }

            public int ActualHours { get; set; }

            public int BudgetAmount { get; set; }

            public int ActualAmount { get; set; }

            public string ProfilePic { get; set; }

            public int Invoice { get; set; }

            public string vendorFeedback { get; set; }

            public decimal? Rating { get; set; }

            public int? IsComplete { get; set; }

            public List<VendorResourceModel> VendorResourceList { get; set; }


        }

        public class VendorResourceModel
        {
            public int Id { get; set; }

            public int VendorId { get; set; }


            public string Name { get; set; }

            public string Designation { get; set; }

            public int BudgetedHours { get; set; }

            public int ActualHours { get; set; }

            public string Companyfeedback { get; set; }


            public decimal Rating { get; set; }


        }

        public class VendorResourceTypeModel
        {

            public int Id { get; set; }

            public int VendorId { get; set; }

            public int RoleId { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string Profile { get; set; }
            public int ExpectedHours { get; set; }

            public int SpentHours { get; set; }

            public int HourlyRate { get; set; }

            public decimal Rating { get; set; }

            public string Companyfeedback { get; set; }

        }

        public class FilterProjectModal
        {


            public int Id { get; set; }

            public int ProjectId { get; set; }

            public int VendorCompanyId { get; set; }

            public string Name { get; set; }

            public int CompanyId { get; set; }

            public bool UnpaidInvoice { get; set; }

            public int VendorId { get; set; }


            public int PageIndex { get; set; }
            public int PageSize { get; set; }

        }


        public class ProjectCompanyModal
        {
            public int CompanyId { get; set; }

            public string Name { get; set; }
        }


        public class ProjectVendorModal
        {
            public int VendorCompanyId { get; set; }

            public string Name { get; set; }
        }


        public class FilterProjectByNameModal
        {


            public int Id { get; set; }

            public int VendorId { get; set; }

            public int ProjectId { get; set; }

            public string Name { get; set; }

            public int CompanyId { get; set; }

            public int RequestType { get; set; }

        }


        public class CustomControlUIModel
        {

            public int CustomControlId { get; set; }

            public string FieldName { get; set; }

            public string Watermark { get; set; }

            public string ControlType { get; set; }

            public List<string> controlList { get; set; }

            public string Values { get; set; }

            
            public string RadioButton { get; set; }

            public string DescriptionCustomControl { get; set; }

            public string FieldModel { get; set; }

            public List<CustomUIDropDownModel> CustomDropDownList { get; set; }

            public List<CustomUIRadioButtonModel> CustomRadioButtonList { get; set; }


        }



        public class CustomUIDropDownModel
        {
            public int Id { get; set; }

            public string Name { get; set; }

        }



        public class CustomUIRadioButtonModel
        {
            public int Id { get; set; }

            public string Name { get; set; }

        }



    }
}
