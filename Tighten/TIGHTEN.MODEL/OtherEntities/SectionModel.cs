﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class SectionModel
    {

        public class SectionAddModel
        {
            [Required]
            public int ProjectId { get; set; }

            //[Required]
            public string SectionName { get; set; }

        }

        public class NewSectionAddModel
        {
            public SectionAddModel SectionAddModel { get; set; }
            public string UserId { get; set; }

        }

        public class SectionUpdateModel
        {
            [Required]
            public int SectionId { get; set; }
            [Required]
            public int ProjectId { get; set; }

            [Required]
            public string SectionName { get; set; }

        }

        public class NewSectionUpdateModel
        {
            public SectionUpdateModel SectionUpdateModel { get; set; }
            public string UserId { get; set; }

        }
    }
}
