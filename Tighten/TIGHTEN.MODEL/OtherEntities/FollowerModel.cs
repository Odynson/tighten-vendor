﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class FollowerModel
    {
        public class FollowerDetailsModel
        {
            public string FollowerName { get; set; }

            public string FollowerEmail { get; set; }

            public string FollowerProfilePhoto { get; set; }

            public string Value { get; set; }

            public string Text { get; set; }


        }

        public class FollowerUpdateModel
        {
            [Required]
            public int TodoId { get; set; }

            public string[] Followers { get; set; }
        }

        public class NewFollowerUpdateModel
        {
            public FollowerUpdateModel FollowerUpdateModel { get; set; }

            public string UserId { get; set; }
            public int CompanyId { get; set; }
        }


        public class FollowersUpdateModel
        {
            public string FollowerUserId { get; set; }
            public int TodoId { get; set; }

            public string UserId { get; set; }
            public int CompanyId { get; set; }

        }



    }
}
