﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public  class CalendarModel
    {


        public class CalendarEvents
        {
            public int Id { get; set; }
            public int TeamId { get; set; }
            public int ProjectId { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string Url { get; set; }
            public bool AllDay { get; set; }
            public DateTime eventDateFrom { get; set; }
            public DateTime eventTimeFrom { get; set; }
            public DateTime eventDateTo { get; set; }
            public DateTime eventTimeTo { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime ModifiedDate { get; set; }
            public string CreatedBy { get; set; }
            public string ModifiedBy { get; set; }
            public string UserId { get; set; }

        }


        public class UpdateEventOnDropOrResize
        {
            public int EventId { get; set; }
            public DateTime eventDateFrom { get; set; }
            public DateTime eventDateTo { get; set; }
            public string UserId { get; set; }

        }


    }
}
