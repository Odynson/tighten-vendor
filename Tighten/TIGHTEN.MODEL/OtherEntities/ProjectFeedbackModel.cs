﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL 
{
    public class ProjectFeedbackModel
    {

        public class ProjectFeedback
        {
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public string ProjectDescription { get; set; }
            public int? ProjectEstimatedHours { get; set; }
            public decimal? ProjectEstimatedBudget { get; set; }
            public string ProjectSpentHours { get; set; }
            public decimal ProjectSpentBudget { get; set; }
            public string ProjectRemarks { get; set; }

            public int CompanyId { get; set; }
            public string CurrentUser { get; set; }

            public bool ProjectCompleted { get; set; }

            public List<ProjectFeedbackMembers> ProjectMembers { get; set; }

        }


        public class ProjectFeedbackMembers
        {
            public bool IsFeedbackAllowed { get; set; }

            public string MemberName { get; set; }

            public bool IsFreelancer { get; set; }

            public string MemberEmail { get; set; }

            public string MemberProfilePhoto { get; set; }

            public string MemberUserId { get; set; }

            public string MemberFeedback { get; set; }

            public string MemberRole { get; set; }

            public int MemberRolePriority { get; set; }

            public List<Todo> MemberTodos { get; set; }

            public string MemberSpentHours { get; set; }
            public decimal MemberEarnedMoney { get; set; }

            public decimal MemberRate { get; set; }

            public int ProjectId { get; set; }
        }

        public class Todo
        {
            public int TodoId { get; set; }
            public string TodoLoggedTime { get; set; }
            public string UserId { get; set; }
            public int ProjectId { get; set; }
        }

        public class UserModalForProjectFeedback
        {
            public bool IsFreelancer { get; set; }
            public int RolePriority { get; set; }
            public string RoleName { get; set; }
        }


        public class AddUpdateFeedbackModal
        {
            public string MemberUserId { get; set; }
            public int ProjectId { get; set; }
            public int CompanyId { get; set; }
            public string CurrentUser { get; set; }
            public string MemberFeedback { get; set; }

        }



    }

}
