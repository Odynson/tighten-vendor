﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class FeedbackModel
    {
        public class Feedback
        {
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public string ProjectDescription { get; set; }
            public string ProjectRemarks { get; set; }
            public int CompanyId { get; set; }
            public List<FeedbackModel.FeedbackOfProject> FeedbackOfProject { get; set; }
        }


        public class FeedbackOfProject
        {
            public int Id { get; set; }
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public string ProjectDescription { get; set; }
            public string MemberName { get; set; }
            public string MemberRole { get; set; }
            public string MemberFeedback { get; set; }
            public bool IsSelected { get; set; }
        }



        public class ShowOnProfileModel
        {
            public List<FeedbackOfProject> FeedbackOfProject { get; set; }
        }


    }
}
