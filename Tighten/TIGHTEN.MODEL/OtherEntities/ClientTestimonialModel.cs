﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL 
{
    public class ClientTestimonialModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string UserId { get; set; }
        public int Rating { get; set; }
        public int CompanyId { get; set; }
        public string Feedback { get; set; }
        public List<FreelancerInfoForFeedback> selectedFreelancersForProjects { get; set; }
    }

    public class FreelancerInfoForFeedback
    {
        public string FreelancerEmail { get; set; }
        public string FreelancerName { get; set; }
        public string FreelancerUserId { get; set; }

    }


}
