﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TIGHTEN.MODEL.TeamModel;

namespace TIGHTEN.MODEL
{
    public class TeamModel
    {

        public class Team
        {
            public int TeamId { get; set; }
            public string TeamName { get; set; }
            public long TeamBudget { get; set; }
            public string TeamDescription { get; set; }
            public int ParentId { get; set; }
            public int ParentTeamId { get; set; }
            //public string[] TeamMembers { get; set; }
            public TeamMembers[] TeamMembers { get; set; }



        }


        public class TeamWithMembers
        {
            public int TeamId { get; set; }
            public int SelectedTeamId { get; set; }
            public string TeamName { get; set; }
            public long TeamBudget { get; set; }
            public string TeamDescription { get; set; }
            public DateTime CreatedDate { get; set; }
            public List<TeamMembers> TeamMembers { get; set; }
            public int ParentId { get; set; }
            public int ParentTeamId { get; set; }
            public int IsParent { get; set; }
            public int TotalPageCount { get; set; }
            public List<SubTeam> SubTeam { get; set; }
            public int DesignationId { get; set; }
            public string DesignationName { get; set; }
        }
        public class SubTeam
        {
            public int TeamId { get; set; }
            public string TeamName { get; set; }
            public long TeamBudget { get; set; }
            public string TeamDescription { get; set; }
            public DateTime CreatedDate { get; set; }
            public int ParentId { get; set; }
            public int ParentTeamId { get; set; }
            public int IsParent { get; set; }

        }
        public class TeamMembers
        {
            public int TeamId { get; set; }
            public string MemberName { get; set; }
            public string MemberEmail { get; set; }
            public string MemberProfilePhoto { get; set; }
            public string MemberUserId { get; set; }
            public string Value { get; set; }
            public string Text { get; set; }
            public bool IsFreelancer { get; set; }
            public int MemberRoleId { get; set; }
            public int MemberDesignationId { get; set; } //JobProfileId
            //public bool IsRole { get; set; }
            public bool IsTeamLead { get; set; }
            public int DesignationId { get; set; }
            public string DesignationName { get; set; }
            public int NoOfTeamManage { get; set; }
        }

        public class TeamFinancialManager
        {
            public int TeamId { get; set; }
            public long TeamBudget { get; set; }
            public string TeamName { get; set; }
            public string TeamDescription { get; set; }
            public Nullable<decimal> AllottedBudget { get; set; }
            public Nullable<decimal> SpentBudget { get; set; }
            public Nullable<decimal> Percentage { get; set; }
            public bool Indicator { get; set; }
            public List<ProjectFinancialManager> ProjectDetails { get; set; }
        }


        public class ProjectFinancialManager
        {
            public int ProjectId { get; set; }
            public int TeamId { get; set; }
            public string ProjectName { get; set; }
            public string ProjectDescription { get; set; }
            public Nullable<decimal> AllottedBudget { get; set; }
            public Nullable<decimal> SpentBudget { get; set; }
            public Nullable<decimal> Percentage { get; set; }
            public Nullable<decimal> PercentageFromTeam { get; set; }
            public bool Indicator { get; set; }
        }



    }

    public class NewModelForTeam
    {
        public TeamModel.Team TeamModel { get; set; }
        public string UserId { get; set; }
        public int CompanyId { get; set; }
    }

    public class TotalTeamModel
    {
     public   List<TeamDataModel> TeamList { get; set; }
     
    }


    public class TeamDataModel
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public long TeamBudget { get; set; }
        public string TeamDescription { get; set; }
        public int ParentId { get; set; }
        public int ParentTeamId { get; set; }
        public DateTime? TeamCreatedDate { get; set; }
        public List<TeamDataModel> SubTeamList { get; set; }
        public List<TeamMembersModel> TeamMembersList { get; set; }
        public List<TeamMembersModel> TopTeamMembersList { get; set; }
        public List<TeamMembersModel> TempTopTeamMembersList { get; set; }

        public int  MainTeamIndex { get; set; }
    }


    public class TeamMembersModel
    {
        public int TeamId { get; set; }
        public string MemberName { get; set; }
        public string MemberEmail { get; set; }
        public string MemberProfilePhoto { get; set; }
        public string MemberUserId { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
        public bool IsFreelancer { get; set; }
        public int MemberRoleId { get; set; }
        public int MemberDesignationId { get; set; } //JobProfileId
                                                     //public bool IsRole { get; set; }
        public bool IsTeamLead { get; set; }


    }
    public class TeamChartModel
    {
        public string name { get; set; }
        public string title { get; set; }
        public List<TeamChartModel> children { get; set; }
    }
}
