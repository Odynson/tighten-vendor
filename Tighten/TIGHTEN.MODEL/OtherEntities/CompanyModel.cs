﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class CompanyModel
    {
        public int Id { get; set; }
        [Required]
        public int CompanyId { get; set; }

        public string CompanyLogo { get; set; }
        [Required]
        public string CompanyName { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyDescription { get; set; }
        public string CompanyWebsite { get; set; }
        public string CompanyAddress { get; set; }

        public string CompanyPhoneNo { get; set; }

        public string CompanyFax { get; set; }

        public string CompanyAdmin { get; set; }

        public DateTime CompanyActiveSince { get; set; }

        public int CompanyTeamsCount { get; set; }

        public int CompanyProjectsCount { get; set; }
        public bool CompanyIsVendorFirmRegistered { get; set; }
        public bool VendorCompanyIsVendorFirmRegistered { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public List<Vendor.VendorPOCModel> VendorPOCList { get; set; }

        public int VendorId { get; set; }
        public string CreatedDate { get; set; }
        public string ProjectCount { get; set; }
        public string AccountCount { get; set; }
    }

    public class NewCompanyModel {
        public CompanyModel Company { get; set; }
        public string UserId { get; set; }
    }
}
