﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class ClientFeedbackModel
    {
        public class ClientFeedbackAddModel
        {

            public int? TodoId { get; set; }
            public int? ProjectId { get; set; }

            public string Feedback { get; set; }

            public int Rating { get; set; }

            public bool IsTodoFeedback { get; set; }

        }

        public class ClientFeedbackDetailModel
        {


            public string Feedback { get; set; }

            public int Rating { get; set; }

            public bool IsTodoFeedback { get; set; }

            public int ProjectId { get; set; }
            public string ProjectName { get; set; }

            public int TodoId { get; set; }

            public string TodoName { get; set; }

            public string ClientUserId { get; set; }
            public string ClientName { get; set; }

            public string ClientProfilePhoto { get; set; }

            public DateTime CreatedDate { get; set; }
        }




    }
}
