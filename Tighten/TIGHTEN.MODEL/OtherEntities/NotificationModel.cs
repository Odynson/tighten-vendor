﻿using System;

namespace TIGHTEN.MODEL
{
    public class NotificationModel
    {

        public class NotificationAddModel
        {
            public int? TeamId { get; set; }

            public int? ProjectId { get; set; }

            public int? CalendarEventId { get; set; }

            public int? TodoId { get; set; }

            public int? CommentId { get; set; }

            public int? AttachmentId { get; set; }

            public string Name { get; set; }


        }

        public class NotificationDetailModel
        {
            public int Id { get; set; }
            public int ConcernedSectionId { get; set; }

            public string ConcernedSectionName { get; set; }

            public string FilePath { get; set; }

            public int Type { get; set; }

            public string Name { get; set; }

            public string CreatorName { get; set; }
            public string CreatorUserId { get; set; }

            public string CreatorProfilePhoto { get; set; }

            public bool IsRead { get; set; }

            public DateTime CreatedDate { get; set; }


        }


    }
}
