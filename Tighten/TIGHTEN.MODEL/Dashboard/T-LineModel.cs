﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
   public class T_LineModel
    {

       public int TotalTasks { get; set; }

       public int PendingTasks { get; set; }

       public int BeforeHandTasks { get; set; }

       public int CompletedTasks { get; set; }

       public int RetardedTasks { get; set; }

       public int ClientsFeedback { get; set; }

       public int Teams { get; set; }

       public int Projects { get; set; }
    }
}
