﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL 
{
    public class PackageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public int DurationInDays { get; set; }
        public bool IsActive { get; set; }
        public bool Ispromotional { get; set; }
        public decimal Discount { get; set; }
    }
}
