﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class SubscriptionModel
    {
        public int Id { get; set; }
        public int PackageId { get; set; }
        public int CompanyId { get; set; }
        public string UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public decimal Discount { get; set; }
        public string CouponCode { get; set; }
    }

    public class PaymentModel
    {
        public int Id { get; set; }
        public int PackageId { get; set; }
        public int CompanyId { get; set; }
        public string UserId { get; set; }
        public string CreditCard { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string CvvNumber { get; set; }
        public string CardName { get; set; }
        public string AllowReckringPayment { get; set; }
        public int PaymentType { get; set; }
    }


    public class CardModel{
        public string CreditCard { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string CvvNumber { get; set; }
        public string CardHolderName { get; set; }
        public bool? AllowReckringPayment { get; set; }
        public bool IsSelected { get; set; }
    }




    public class SubsctriptionPaymentModel
    {
        public string Token { get; set; }
        public int Amount { get; set; }

        public int CompanyId { get; set; }

    }


}
