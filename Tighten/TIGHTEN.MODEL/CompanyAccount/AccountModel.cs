﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.CompanyAccount
{
    public class AccountModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TeamId { get; set; }
        public int CompanyId { get; set; }
        public int VendorId { get; set; }
        public string Description { get; set; }
        public string CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
         
        public List<AccountPOCModel> AccountPOCList { get; set; }
        public List<AccountDocumentsModel> AccountDocumentList { get; set; }

        public string CompanyName { get; set; }
        public string VendorCompanyName { get; set; }
        public string TeamName { get; set; }
        public bool IsRegisteredVendor { get; set; }
        public string POCNames { get; set; }
        public string UserId { get; set; }
        public bool IsAdded { get; set; }
    }
    public class AccountPOCModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int AccId { get; set; }
        public string CreatedDate { get; set; }
        public bool IsDeleted { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string pocFullName { get; set; }
    }

    public class AccountDocumentsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AccId { get; set; }
        public string Path { get; set; }
        public string ActualFileName { get; set; }
        public string CreatedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class AccountMail
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string EmailConfirmationCode { get; set; }
        public string VendorCompanyName { get; set; }
        public string SenderCompanyName { get; set; }
        public string TeamName { get; set; }
        public string Email { get; set; }
        public string CompanyPersonName { get; set; }
    }
}
