﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.ParentAccount
{
    public class ParentAccountModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public int CompanyId { get; set; }
        public int VendorId { get; set; }
        public string Remarks { get; set; }

        public string PersonName { get; set; }
        public string PersonPhNo { get; set; }
        public string PersonEmail { get; set; }
        public decimal TotalCost { get; set; }
        public string CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public string CompanyName { get; set; }
        public string VendorCompanyName { get; set; }
    }
}
