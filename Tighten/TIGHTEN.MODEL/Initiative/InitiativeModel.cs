﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.Initiative
{
    public class InitiativeModel
    {
        public int Id { get; set; }
        public int enterpriseCompanyID { get; set; }
        public string initiative_title { get; set; }
        public string description { get; set; }
        public string createdby { get; set; }
        public string createddate { get; set; }
        public string modifiedby { get; set; }
        public string modifieddate { get; set; }
        public bool isactive { get; set; }
        public bool isdeleted { get; set; }
        public string status { get; set; }
        public string contactperson { get; set; }
        public string contactpersonno { get; set; }

        public int noOfVendors { get; set; }
        public decimal totalAmountSpent { get; set; }
        public int totalHrs { get; set; }

        public string ActualFileName { get; set; }
        public string FileName { get; set;}
        public string FilePath { get; set; }
        public string DocIdList { get; set; }
        
        public string DocIsDeletedList { get; set; }
        public string DocListIsAdded { get; set; }

        public string VendorProjectIdList { get; set; }
        public string vendorids { get; set; }
        public string projectids { get; set; }
        public string contactpersons { get; set; }
        public string contactpersonphno { get; set; }

        public List<initiativeVendorsModel> initiativeVendors { get; set; }
        public List<newVendorProjectDocument> vendorProjectDocumentList { get; set; }
    }
    public class initiativeVendorsModel
    {
        public int Id { get; set; }
        public int initiativeid { get; set; }
        public int vendorId { get; set; }
        public string joineddate { get; set; }  
        public int projectid { get; set; }
        public string description { get; set; }
        public string contactpersonno { get; set; }
        public string contactperson { get; set; }
        public bool isDeleted { get; set; }
        public bool IsAdded { get; set; }

        public string vendorName { get; set; }
        public string projectName { get; set; }
        public int hrsBilled { get; set; }
        public decimal AmtSpent { get; set; }
    }
    public class newVendorProjectDocument
    {
        public int Id { get; set; }
        public int InitiativeId { get; set; }
        public string Name { get; set; }
        public string ActualFileName { get; set; }
        public string FileName { get; set; }
        public String FilePath { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public bool IsAdded { get; set; }
    }
}
