﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class SideMenuOptionRolewiseModel
    {
        public bool IsCompany { get; set; }
        public bool IsUserProfile { get; set; }
        public bool IsTeam { get; set; }
        public bool IsProject { get; set; }
        public bool IsFinancialTeam { get; set; }
        public bool IsTaskApprovalUser { get; set; }
        public bool IsTaskApprovalAdmin { get; set; }
        public bool IsAccount { get; set; }
        public bool IsRole { get; set; }
        public bool IsPayment { get; set; }
        public bool IsFeedback { get; set; }
        public bool IsVendor { get; set; }
        public bool IsVendor1 { get; set; }
        public bool IsProfilePic { get; set; }
        public bool IsProfileLink { get; set; }
        public bool IsCompanyName { get; set; }
        public bool IsMyInvitaiton { get; set; }
        public bool IsProjectToQuote { get; set; }
        public bool IsVendorInvoice { get; set; }
        public bool IsProjectAwarded { get; set; }
        public bool IsVendorInvoiceSetting { get; set; }
        public bool IsVenorUserManagement { get; set; }
        public bool Settings { get; set; }
        public bool IsVendorGeneralUser { get; set; }
        public bool IsAccountManager { get; set; }
        public double RatingVendorGeneralUser { get; set; }


        public List<SideMenuOptionRolewiseModel_Child> TeamOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> ProjectOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> FinancialTeamOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> TaskApprovalUserOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> TaskApprovalAdminOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> AccountOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> RoleOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> PaymentOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> FeedbackOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> VendorOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> ProfileLinkOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> ProfilePicOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> CompanyNameOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> MyInvitationsVendorOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> ProjectstoQuoteVendorOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> ProjectAwardedVendorOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> InvoiceVendorOptions { get; set; }

        public List<SideMenuOptionRolewiseModel_Child> SettingOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> AccountManagerOptions { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> VendorAccountSettingOptions { get; set; }

        public List<SideMenuOptionRolewiseModel_Child> SettingVendorAdminProfileOptions { get; set; }


        public int Role { get; set; }
    }

    public class SideMenuOptionRolewiseModel_Child
    {
        public string MenuName { get; set; }
        public string SubMenuName { get; set; }
        public string URL { get; set; }
        public int ParentId { get; set; }
    }

    public class SideMenuOption
    {
        public string MenuName { get; set; }
        public int UserRole { get; set; }
        public string URL { get; set; }
        public List<SideMenuOptionRolewiseModel_Child> SubMenuList { get; set; }
        public int MenuId { get; set; }
    }




}
