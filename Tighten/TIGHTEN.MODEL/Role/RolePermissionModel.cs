﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class RolePermissionModel
    {

        public class InternalPermissionsForCurrentUserModel
        {
            public bool CanEditCompanyProfile { get; set; }
            public bool CanAddNewTeam { get; set; }
            public bool CanUpdateTeam { get; set; }
            public bool CanAddNewProject { get; set; }
            public bool CanUpdateProject { get; set; }
            public bool CanApproveInvoice { get; set; }

        }


        public class AllRolesForCompanyModel
        {
            public int RoleId { get; set; }
            public string RoleName { get; set; }

        }


        public class PermissionForSelectedRole
        {
            public int MenuId { get; set; }
            public string MenuName { get; set; }
            public string MenuDescription { get; set; }
            public string Url { get; set; }

            public int RoleId { get; set; }
            public int CompanyId { get; set; }
            public bool? IsActive { get; set; }
            public int ParentId { get; set; }
            public bool? IsInternalPermissionPresent { get; set; }
            public List<MODEL.RolePermissionModel.AllInternalPermissionsModel> InternalPermissions { get; set; }
        }



        public class SavePermissionForSelectedRole
        {
            public List<PermissionForSelectedRole> PermissionsList { get; set; }

        }




        public class AllInternalPermissionsModel
        {
            public int PermissionId { get; set; }
            public string PermissionName { get; set; }
            public bool? IsPermissionActive { get; set; }
            public bool IsAdminLevelPermission { get; set; }
            public int RoleId { get; set; }
            public int CompanyId { get; set; }
            public int MenuId { get; set; }
        }

        public class InternalPermissionsMOdalSpecific
        {

            public int Id { get; set; }
            public int ParentId { get; set; }
            public string PermissionName { get; set; }
            public int MenuId { get; set; }
            public int RoleId { get; set; }
            public int CompanyId { get; set; }

        }

        public class SaveInternalPermissionsModel
        {
            public List<AllInternalPermissionsModel> PermissionsList { get; set; }
        }



    }
}
