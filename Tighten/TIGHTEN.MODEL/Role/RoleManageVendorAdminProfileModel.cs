﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.RoleManageVendorAdminProfile
{
  public  class RoleManageVendorAdminProfileModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public bool isActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int CompanyId { get; set; }
        public int RolePriority { get; set; }
        public bool RoleVisibleToVendor { get; set; }
    }


    public class RoleAddUpdateModel
    {
        public RoleManageModel.AllRolesForCompany RoleModel { get; set; }
        public string UserId { get; set; }
        public int VendorCompanyId { get; set; }
    }

}
