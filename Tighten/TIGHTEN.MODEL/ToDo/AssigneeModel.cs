﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
   public class AssigneeModel
    {

       public class AssigneeUpdateModel {
           [Required]
           public int TodoId { get; set; }
           public string AssigneeId { get; set; }
       }
        public class NewAssigneeUpdateModel
        {
            public AssigneeUpdateModel AssigneeUpdateModel { get; set; }
            public string UserId { get; set; }
        }

    }
}
