﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class AttachmentModel
    {
        public class AttachmentDetailsModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string OriginalName { get; set; }
            public string CreatorName { get; set; }
            public string CreatorUserId { get; set; }
            public string CreatorProfilePhoto { get; set; }
            public DateTime CreatedDate { get; set; }

            public string SourceImage { get; set; }

            public string Path { get; set; }

            public int TodoId { get; set; }

            public string TodoName { get; set; }


        }


    }
}
