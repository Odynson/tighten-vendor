﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class ToDoAttachmentsForAllApis
    {
        public int ToDoId { get; set; }
        public string UserId { get; set; }
        public BoxModel boxModel { get; set; }
        public DropBoxModel DropBoxModel { get; set; }
        public GoogleDriveModel GoogleDriveModel { get; set; }

    }

    public class BoxModel
    {
        public string name { get; set; }
        public string url { get; set; }
    }

    public class DropBoxModel
    {
        public string name { get; set; }
        public string link { get; set; }
    }

    public class GoogleDriveModel
    {
        public string name { get; set; }
        public string url { get; set; }
    }

}
