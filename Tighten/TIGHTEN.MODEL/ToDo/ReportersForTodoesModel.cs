﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class ReportersForTodoesModel
    {
        public class ReportersDetail
        {

            public string UserId { get; set; }
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string ProfilePhoto { get; set; }

        }


        public class ReportersEmailData
        {
            //public List<ReportersDetail> AllReporterDetails { get; set; }
            public DateTime FromDate { get; set; }

            public DateTime ToDate { get; set; }

            public string Message { get; set; }

            public string Reporters { get; set; }

            public string UserId { get; set; }
            public int CompanyId { get; set; }
            public int ProjectId { get; set; }

            public TodosForPdf_List AllTodosForPreview { get; set; }

        }



        public class USerDataToSendEmail
        {
            //public List<ReportersDetail> AllReporterDetails { get; set; }
            public string UserName { get; set; }
            public decimal UserRate { get; set; }
            public int TaskCompleted { get; set; }

            public List<TodoLoggedTime> LoggedTime { get; set; }


        }

        public class TodoLoggedTime
        {
            public string Logged_Time { get; set; }
        }



        public class SendMailForApprovalStatus
        {

            public string UserEmail { get; set; }

            public string UserFirstName { get; set; }

            public string TodoName { get; set; }

            public string TodoLoggedTime { get; set; }

            public string ApproverName { get; set; }

            public string Reason { get; set; }

            public bool Approved { get; set; }


        }


        public class TodosForPreview_RequiredParams
        {
            public DateTime fromDate { get; set; }
            public DateTime toDate { get; set; }
            public string userId { get; set; }
            public int CompanyId { get; set; }
            public int ProjectId { get; set; }
            public string ReportersID { get; set; }
            public string message { get; set; }

        }

        
        public class TodosForPdf_List
        {
            public string TotalHoursForAllTodos { get; set; }
            public decimal TotalAmountForAllTodos { get; set; }   //

            public List<TodosForPreview_List> AllTodos { get; set; }   //

            public string CompanyProfilePic { get; set; }
            public string CompanyUserId { get; set; }
            public string CompanyName { get; set; }
            public string InvoiceNumber { get; set; }
            public string RejectReason { get; set; }
            public DateTime DateIssued { get; set; }
            public string Message { get; set; }
            public string FromName { get; set; }
            public string ToName { get; set; }
            public bool IsRejected { get; set; }
            
        }
        public class TodosForPreview_List
        {
            public string ProjectName { get; set; }
            public string TaskType { get; set; }

            public int TodoId { get; set; }
            public string TodoName { get; set; }
            public string TodoDescription { get; set; }
            public string hours { get; set; }
            public List<TodoTimeLogsForPreview_List> HoursDetail { get; set; }
            public decimal Rate { get; set; }
            public decimal Amount { get; set; }   // 
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public int TodoType { get; set; }

        }



        public class TodoTimeLogsForPreview_List
        {
            public int TodoTimeLogsId { get; set; }
            public int TodoId { get; set; }
            public int DurationInMinutes { get; set; }

        }



        public class SaveInvoiceDetailsModal
        {
            public int TodoId { get; set; }
            public string TodoDescription { get; set; }
            public string TodoName { get; set; }
            public string hours { get; set; }
            public decimal Amount { get; set; }
            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public int TodoType { get; set; }
            public string TodoTimeLogId { get; set; }


        }



    }
}
