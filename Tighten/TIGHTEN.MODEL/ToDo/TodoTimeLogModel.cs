﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class TodoTimeLogModel
    {
        public int? TodoId { get; set; }
        public string TodoName { get; set; }
        public string Description { get; set; }
        public string EstimatedTime { get; set; }
        public string LoggedTime { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime? StopDateTime { get; set; }
        public DateTime loggingTime { get; set; }
        public string StartBy { get; set; }
        public string StopBy { get; set; }
        public int CurrentSessionTime { get; set; }
    }


    public class TodoDetailForNotification
    {
        public string TodoName { get; set; }
        public string TodoDescription { get; set; }
        public string AssignedByName { get; set; }
        public string AssignedToName { get; set; }
        public string LoggedTime { get; set; }

        public AssignedToForNotification AssignedTo { get; set; }
        public AssignedByForNotification AssignedBy { get; set; }

        public List<FollowersForNotification> Followers { get; set; }
    }

    public class FollowersForNotification
    {
        public string FollowersName { get; set; }
        public string FollowersUserId { get; set; }
        public string ProfilePic { get; set; }
	}

    public class AssignedToForNotification
    {
        public string AssignedToName { get; set; }
        public string AssignedToUserId { get; set; }
        public string AssignedToProfilePic { get; set; }
    }

    public class AssignedByForNotification
    {
        public string AssignedByName { get; set; }
        public string AssignedByUserId { get; set; }
        public string AssignedByProfilePic { get; set; }
    }

    public class TaskTypeForCreatingTodo
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }


    public class BothTaskTypeForSelectize
    {
        public List<TaskTypeForCreatingTodo> AllTaskTypes { get; set; }
        public List<TaskTypeForCreatingTodo> TodoTaskType { get; set; }
    }

}
