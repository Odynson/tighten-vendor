﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class TodoApproval
    {
        public int TodoId { get; set; }
        public string Name { get; set; }
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string SectionName { get; set; }
        public int SectionId { get; set; }
        public string AssigneeProfilePhoto { get; set; }
        public string AssigneeId { get; set; }
        public string AssigneeName { get; set; }
        public string AssigneeEmail { get; set; }
        public string ReporterProfilePhoto { get; set; }
        public string ReporterId { get; set; }
        public string ReporterName { get; set; }
        public string ReporterEmail { get; set; }
        public string Description { get; set; }
        public DateTime? DeadlineDate { get; set; }
        public bool InProgress { get; set; }
        public bool IsDone { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string EstimatedTime { get; set; }
        public string LoggedTime { get; set; }
        public string TodoType { get; set; }
        public bool IsApproved { get; set; }
        public string ApprovedBy { get; set; }
        public string Reason { get; set; }
        public bool IsFreelancer { get; set; }
        public List<TodoDetail> TodoDetails { get; set; }
    }
    public class TodoDetail
    {
        public int Id { get; set; }
        public int? TodoId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime? StopDateTime { get; set; }
        public int Duration { get; set; }
        public bool IsAutomaticLogged { get; set; }
        public DateTime loggingTime { get; set; }
        public string StartBy { get; set; }
        public string StopBy { get; set; }
    }
    public class TodoApprovalSearch
    {
        public int CompanyId { get; set; }
        public string AssigneeId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public bool IsApproved { get; set; }
        public int ProjectId { get; set; }
        public int TaskStatus { get; set; }
        public int TaskType { get; set; }
        public int RoleId { get; set; }
        public string UserId { get; set; }
    }

    public class TodoApprovalUpdate
    {
        public int TodoId { get; set; }
        public string ApproverId { get; set; }
        public bool IsApproved { get; set; }
        public string Reason { get; set; }
    }
    // This is to return the result of all the Users in that company
    public class CompanyUserResult
    {
        public string Value { get; set; }
        public string Name { get; set; }
    }


    public class ProjectResult
    {
        public int Value { get; set; }
        public string Name { get; set; }
    }

    public class ProjectResultForAllCompanies
    {
        public List<ProjectResult> ProjectList { get; set; }
        public string CompanyName { get; set; }
    }
}
