﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TIGHTEN.MODEL
{
    public class TodoModel
    {
        public class SectionData
        {

            public int Id { get; set; }
            public string SectionName { get; set; }
            public int SectionListOrder { get; set; }
            public string SectionCreator { get; set; }
            public string Text { get; set; }
            public int Value { get; set; }
            public int ProjectId { get; set; }
            public int TeamId { get; set; }
            public string ProjectName { get; set; }
            public string TeamName { get; set; }
            public List<TodosData> Todos { get; set; }

        }

        public class TodosData
        {
            public int TodoId { get; set; }
            public int TodoListOrder { get; set; }
            public string Name { get; set; }
            public int TeamId { get; set; }
            public string TeamName { get; set; }
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public string SectionName { get; set; }
            public int SectionId { get; set; }
            public string AssigneeProfilePhoto { get; set; }
            public string AssigneeId { get; set; }
            public string AssigneeName { get; set; }

            public string AssigneeEmail { get; set; }

            public string ReporterProfilePhoto { get; set; }

            public string ReporterId { get; set; }

            public string ReporterName { get; set; }

            public string ReporterEmail { get; set; }

            public string Description { get; set; }

            public DateTime? DeadlineDate { get; set; }

            public bool InProgress { get; set; }

            public bool IsDone { get; set; }

            public DateTime CreatedDate { get; set; }
            public string CreatedBy { get; set; }

            public DateTime ModifiedDate { get; set; }
            public string EstimatedTime { get; set; }
            public string LoggedTime { get; set; }
            public string TodoType { get; set; }
            public bool IsApproved{ get; set; }
            public string ApprovedBy { get; set; }
            public string Reason { get; set; }
            public string ActiveUserId { get; set; }
            public int ActiveTodoId { get; set; }
            public string TaskType { get; set; }
            public string TaskTypeId { get; set; }
        }
        public class Comment
        {
            public int Id { get; set; }
            public string Description { get; set; }

            public string CreatedBy { get; set; }

            public DateTime CreatedDate { get; set; }
            public string CreatorProfilePhoto { get; set; }

            public bool IsOwner { get; set; }

            public int CommentId { get; set; }

            public List<FileTags> FilesTagged { get; set; }

            public List<UsersTags> UsersTagged { get; set; }

        }

        public class FileTags
        {
            public int CommentId { get; set; }

            public string OriginalName { get; set; }

            public string FilePath { get; set; }

            public string FileName { get; set; }

            public bool IsGif { get; set; }

        }

        public class UsersTags
        {
            public int CommentId { get; set; }

            public string UserId { get; set; }

            public string FullName { get; set; }

        }

        public class Team
        {

            public int Id { get; set; }
            public string TeamName { get; set; }
            public string Description { get; set; }

            public string Action { get; set; }

            public string UserId { get; set; }

            public int TeamId { get; set; }

            public string[] TeamMembers { get; set; }
        }
        public class Project
        {

            public int Id { get; set; }

            public int TeamId { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }

            public bool IsPrivate { get; set; }
        }

        public class ProjectUsers
        {

            public int Id { get; set; }

            public int ProjectId { get; set; }
            public string UserId { get; set; }

        }


        public class ToDo
        {
            public int Id { get; set; }
            [Required]
            public string Name { get; set; }
            public int ProjectId { get; set; }

            public int SectionId { get; set; }

            public int ListOrder { get; set; }
            public string Description { get; set; }
            public string CreatedBy { get; set; }
            public string ModifiedBy { get; set; }
            public bool selfAssigned { get; set; }
            public string AssigneeId { get; set; }

            public DateTime DeadlineDate { get; set; }

            public int updateFlag { get; set; }

            public string IsDone { get; set; }

            public bool InProgress { get; set; }

        }

        public class TodoNotifications
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int TodoId { get; set; }

            public string UserId { get; set; }

            public string Action { get; set; }

        }


        public class SubToDo
        {
            public int Id { get; set; }
            public int ToDoId { get; set; }
            public string Name { get; set; }

            public bool IsDone { get; set; }
        }

        public class ToDoComment
        {
            public int Id { get; set; }
            public int ToDoId { get; set; }
            public string Description { get; set; }
            public string CreatedBy { get; set; }

            public string[] UserTag { get; set; }
            public string[] FileTag { get; set; }

            public string[] OriginalFileName { get; set; }
            public string[] FilePath { get; set; }

            public bool[] IsGif { get; set; }
        }

        public class NewToDoComment
        {
            public ToDoComment ToDoComment { get; set; }
            public string UserId { get; set; }
        }

        public class ToDoAttachments
        {
            public int Id { get; set; }

            public int ToDoId { get; set; }
            public string Name { get; set; }

            public string OriginalName { get; set; }
            public string Path { get; set; }

            public string SourceImage { get; set; }
        }

        public class Followers
        {
            public int ToDoId { get; set; }
            public string UserId { get; set; }
        }

        public class Sections
        {
            public int Id { get; set; }
            [Required]
            public string SectionName { get; set; }
            [Required]
            public int ProjectId { get; set; }

            public string CreatedBy { get; set; }
            public string ModifiedBy { get; set; }


        }

        public class Emails
        {
            public string EmailId { get; set; }
            public string ProjectName { get; set; }
            public string Todo { get; set; }
            public int Flag { get; set; }

            public string Username { get; set; }

            public string Comment { get; set; }

        }

        public class Notifications
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int TodoId { get; set; }

            public string NotifyingUserId { get; set; }

            public int Flag { get; set; }

        }

        public class CalendarEvents
        {
            public int _id { get; set; }
            public int Id { get; set; }
            public string UserId { get; set; }
            public int TodoId { get; set; }

            [Required]
            public int TeamId { get; set; }

            public string Title { get; set; }

            public string Description { get; set; }

            public string Url { get; set; }

            public bool AllDay { get; set; }

            public DateTime? Start { get; set; }

            public DateTime? End { get; set; }

            public string ClassName { get; set; }

            public string Type { get; set; }

            public bool Editable { get; set; }

            public string CreatorName { get; set; }

            public string CreatorProfilePhoto { get; set; }

            public DateTime CreatedDate { get; set; }
            public DateTime eventDateFrom { get; set; }
            public DateTime eventDateTo { get; set; }

        }



    }
}
