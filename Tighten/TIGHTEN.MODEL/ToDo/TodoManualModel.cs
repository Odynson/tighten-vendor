﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class TodoManualModel
    {
        public class TodoAddModel
        {
            public int TodoId { get; set; }
            [Required]
            [Display(Name = "Todo Summary")]
            public string Name { get; set; }
            [Required]
            public int TaskType { get; set; }
            //[Required]
            [Display(Name = "Todo Description")]
            public string Description { get; set; }
            //[Required]
            [Display(Name = "Due Date")]
            public DateTime? DeadlineDate { get; set; }

            //[Required]
            [Display(Name = "Section")]
            public int? SectionId { get; set; }


            [Display(Name = "Assignee")]
            public string AssigneeId { get; set; }
            public string ReporterId { get; set; }
            public string ReporterName { get; set; }
            public string ReporterEmail { get; set; }
            //[Required]
            // This is the estimated time for the todo
            public string EstimatedTime { get; set; }
            public bool InProgress { get; set; }
            public bool IsDone { get; set; }
            [Required]
            [Display(Name = "Start Date")]
            public DateTime? StartDate { get; set; }
            [Required]
            [Display(Name = "End Date")]
            public DateTime? StopDate { get; set; }
            public string CreatedBy { get; set; }
            public string UserId { get; set; }
            [Required]
            [Display(Name = "Logged Time")]
            public string LoggedTime { get; set; }
        }
    }
}
