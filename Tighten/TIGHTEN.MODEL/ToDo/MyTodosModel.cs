﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class MyTodosModel
    {

        public class Teams
        {
            public int TeamId { get; set; }
            public string TeamName { get; set; }
            public List<Projects> Projects { get; set; }
        }

        public class Projects
        {

            public int ProjectId { get; set; }

            public string ProjectName { get; set; }



        }



        public class Todos
        {
            public int TodoId { get; set; }
            public string Name { get; set; }

            public string TeamName { get; set; }

            public int ProjectId { get; set; }

            public string ProjectName { get; set; }

            public string SectionName { get; set; }

            public int SectionId { get; set; }

            public string AssigneeProfilePhoto { get; set; }

            public string AssigneeId { get; set; }

            public string AssigneeName { get; set; }

            public string AssigneeEmail { get; set; }

            public string ReporterProfilePhoto { get; set; }

            public string ReporterId { get; set; }

            public string ReporterName { get; set; }

            public string ReporterEmail { get; set; }

            public string Description { get; set; }

            public DateTime DeadlineDate { get; set; }

            public bool InProgress { get; set; }

            public bool IsDone { get; set; }

            public DateTime CreatedDate { get; set; }

            public DateTime ModifiedDate { get; set; }

            public List<ProjectMembers> ProjectMembers { get; set; }

            public List<Sections> Sections { get; set; }

        }

        public class ProjectMembers
        {

            public string MemberName { get; set; }

            public string MemberEmail { get; set; }

            public string MemberProfilePhoto { get; set; }

            public string MemberUserId { get; set; }

            public string Value { get; set; }

            public string Text { get; set; }

        }

        public class Sections
        {

            public int Id { get; set; }
            public string SectionName { get; set; }

            public string SectionCreator { get; set; }
            public int ProjectId { get; set; }

            public int TeamId { get; set; }


        }


    }
}
