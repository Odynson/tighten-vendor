﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class TodosModel
    {

        public class TodoAddModel
        {
            //[Required]
            [Display(Name = "Todo Summary")]
            public string Name { get; set; }

            [Display(Name = "Type")]
            public string addType { get; set; }

            [Display(Name = "Todo List Order")]
            public int TodoListOrder { get; set; }

            [Display(Name = "Todo Description")]
            public string Description { get; set; }

            public DateTime? DeadlineDate { get; set; }

            [Required]
            [Display(Name = "Section")]
            public int? SectionId { get; set; }

            [Display(Name = "Assignee")]
            public string AssigneeId { get; set; }

            //[Required]
            [Display(Name = "Reporter")]
            public string ReporterId { get; set; }

            // This is the estimated time for the todo
            public string EstimatedTime { get; set; }
            public List<TodoTags> TaskTypeId { get; set; }
        }


        public class TodoTags
        {
            public int Id { get; set; }
            public string Text { get; set; }
        }


        public class TodoUpdateModel
        {
            [Required]
            [Display(Name = "Todo Id")]
            public int TodoId { get; set; }

            //[Required]
            [Display(Name = "Todo Summary")]
            public string Name { get; set; }

            [Display(Name = "Todo Description")]
            public string Description { get; set; }

            public DateTime? DeadlineDate { get; set; }
            // This is the estimated time for the todo
            public string EstimatedTime { get; set; }

            [Required]
            [Display(Name = "Section")]
            public int SectionId { get; set; }

            [Display(Name = "Assignee")]
            public string AssigneeId { get; set; }

            //[Required]
            [Display(Name = "Reporter")]
            public string ReporterId { get; set; }

            public bool InProgress { get; set; }
            public bool PreviouslyInProgress { get; set; }
            
            public bool IsDone { get; set; }
            public bool IsEdit { get; set; }
            public bool IsAvailableForInvoice { get; set; }
            public List<TodoTags> TaskTypeId { get; set; }
        }

        public class ReporterUpdateModel
        {
            [Required]
            public int TodoId { get; set; }
            public string ReporterId { get; set; }
        }
        public class NewReporterUpdateModel
        {
            public ReporterUpdateModel ReporterUpdateModel { get; set; }
            public string UserId { get; set; }
        }

        public class TodoDetailModel
        {

            public int TodoId { get; set; }

            public string Name { get; set; }

            public string Description { get; set; }

            public DateTime DeadlineDate { get; set; }

            public bool InProgress { get; set; }

            public bool IsDone { get; set; }

            public string TeamName { get; set; }

            public string ProjectName { get; set; }

            public string SectionName { get; set; }

            public string AssigneeId { get; set; }

            public string AssigneeProfilePhoto { get; set; }

            public string AssigneeEmailId { get; set; }

            public string AssigneeName { get; set; }

            public string ReporterId { get; set; }

            public string ReporterProfilePhoto { get; set; }

            public string ReporterEmailId { get; set; }

            public string ReporterName { get; set; }

            public DateTime CreatedDate { get; set; }

            public DateTime ModifiedDate { get; set; }



        }

        public class NewTodoAddModel
        {
            public TodoAddModel TodoAddModel { get; set; }
            public string userId { get; set; }
            public int CompanyId { get; set; }
            public int ProjectId { get; set; }
        }
        public class NewTodoUpdateModel
        {
            public TodoUpdateModel TodoUpdateModel { get; set; }
            public string userId { get; set; }
            public bool isEdit { get; set; }
            public int CompanyId { get; set; }
            public int ProjectId { get; set; }
        }

        public class MailReceiverInfo
        {
            public string TodoName { get; set; }
            public int ReceiverRole { get; set; }

            public string ReceiverUserId { get; set; }
            public string ReceiverName { get; set; }
            public string ReceiverEmail { get; set; }

        }

    }
}
