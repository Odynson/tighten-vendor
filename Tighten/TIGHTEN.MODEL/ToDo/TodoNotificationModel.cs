﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class TodoNotificationModel
    {




        public class TodoNotificationDetailModel
        {
            public int Id { get; set; }
            public int ConcernedSectionId { get; set; }

            public string ConcernedSectionName { get; set; }

            public string FollowerName { get; set; }
            public string FollowerProfilePhoto { get; set; }

            public string FilePath { get; set; }

            public int Type { get; set; }

            public string Description { get; set; }

            public string CreatorName { get; set; }
            public string CreatorUserId { get; set; }
            public string CreatorProfilePhoto { get; set; }


            public DateTime CreatedDate { get; set; }


        }


    }
}
