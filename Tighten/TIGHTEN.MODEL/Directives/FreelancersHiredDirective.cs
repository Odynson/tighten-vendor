﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class FreelancersHiredDirective
    {
        public string FreelancerUserId { get; set; }
        public string FreelancerName { get; set; }
        public List<FreelancersProject> ProjectList { get; set; }
        public decimal HourlyRate { get; set; }
    }

    public class FreelancersProject
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public string UserId { get; set; }
    }

}
