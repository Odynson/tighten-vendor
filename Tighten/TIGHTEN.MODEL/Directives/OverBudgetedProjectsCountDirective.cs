﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class OverBudgetedProjectsCountDirective
    {

        public decimal? ProjectBudget { get; set; }
        public bool IsTodoBudgetExceedsProjectBudget { get; set; }

        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public int Accesslevel { get; set; }
        public string ProjectDescription { get; set; }
        public int projectHours { get; set; }
        public string TotalSpentHours { get; set; }
        public decimal TotalSpentMoney { get; set; }
        public bool IsComplete { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<ProjectModel.ProjectMembers> ProjectMembers { get; set; }

    }

}
