﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.WorkLog
{
    public class WorkLogDetailsModel
    {
        public int Id { get; set; }
        public string WorkLogTitle { get; set; }
        public string WorkLogDescription { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedUserId { get; set; }
        public string BillableStatus { get; set; }
        public int ProjectId { get; set; }
        public int PhaseId { get; set; }
        public int PhaseMilestoneId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsInvoiceGen { get; set; }
        public string LastEntryUpdateDate { get; set; }
        public List<WorkLogEnteriesModel> WorkLogEnteries { get; set; }

        public string ProjectName { get; set; }
        public string PhaseMilestoneName { get; set; }
        public List<Vendor.vendorModel> WorkLogVendorList { get; set; }
        public List<Vendor.vendorModel> ProjectMilestones { get; set; }
        public decimal TotalHrs { get; set; }

    }

    public class WorkLogEnteriesModel
    {
        public int Id { get; set; }
        public int WorkLogId { get; set; }
        public string Description { get; set; }
        public decimal TimeSpent { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedUserId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
