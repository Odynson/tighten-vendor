﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class AddNewUserModel
    {
        [Required]
        //[Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string UserRole { get; set; }
        //[Required]
        public int RoleId { get; set; }
        public decimal Rate { get; set; }

    }
    public class NewAddNewUserModel {
        public AddNewUserModel NewUserModel { get; set; }
        public string UserId { get; set; }
    }
}
