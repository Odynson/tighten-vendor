﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TIGHTEN.MODEL
{
    public class ChangePasswordModel
    {
        [Required]
        public string CurrentPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        public string ConfirmNewPassword { get; set; }

    }

    public class NewChangePasswordModel
    {
        public ChangePasswordModel Model{ get; set; }
        public string UserId { get; set; }
    }
}
