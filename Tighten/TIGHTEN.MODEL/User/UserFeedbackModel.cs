﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class UserFeedbackModel
    {

        public class UserFeedbackAddModel
        {
            [Required]
            public string Title { get; set; }

            public string Description { get; set; }

            public bool IsPrivate { get; set; }

            public int Priority { get; set; }

            [Required]
            public int CompanyId { get; set; }

        }

        public class NewUserFeedbackAddModel
        {
            public UserFeedbackAddModel UserFeedbackAddModel { get; set; }
            public string UserId { get; set; }
        }

        public class UserFeedbackDetailsModel
        {
            public int UserFeedbackId { get; set; }
            public string Title { get; set; }

            public string Description { get; set; }

            public bool IsPrivate { get; set; }

            public bool HasVoted { get; set; }
            public int Priority { get; set; }

            public string CreatorName { get; set; }
            public string CreatorUserId { get; set; }
            public string CreatorEmail { get; set; }

            public string CreatorProfilePhoto { get; set; }

            public DateTime CreatedDate { get; set; }
            public List<UserFeedbackVotersDetailsModel> Voters { get; set; }

        }

        public class UserFeedbackVoterUpdateModel
        {
            public int UserFeedbackId { get; set; }
            public bool Vote { get; set; }

        }
        public class NewUserFeedbackVoterUpdateModel
        {
            public UserFeedbackVoterUpdateModel UserFeedbackVoterUpdateModel { get; set; }
            public string UserId { get; set; }

        }

        //public class UserFeedbackVotersDetailsModel
        //{
        //    public string VoterName { get; set; }

        //    public string VoterEmail { get; set; }

        //    public string VoterProfilePhoto { get; set; }

        //}

        public class UserFeedbackVotersDetailsModel
        {
            public int UserFeedbackId { get; set; }
            public string VoterName { get; set; }

            public string VoterEmail { get; set; }

            public string VoterProfilePhoto { get; set; }

        }




        public class UserFeedbackCommentAddModel
        {
            public int UserFeedbackId { get; set; }

            [Required]
            public string Comment { get; set; }

        }
        public class NewUserFeedbackCommentAddModel
        {
            public UserFeedbackCommentAddModel UserFeedbackCommentAddModel { get; set; }
            public string UserId { get; set; }

        }

        public class UserFeedbackComments
        {
            public int CommentId { get; set; }
            public string Comment { get; set; }

            public string CreatorName { get; set; }

            public string CreatorUserId { get; set; }

            public string CreatorProfilePhoto { get; set; }

            public DateTime CreatedDate { get; set; }

        }


    }
}
