﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TIGHTEN.MODEL
{
    //ViewModels =/= Models, VM is a modifiable version of the model (these ViewModels need to be in the Web layer)
    // Models used as parameters to AccountController actions.
    #region AccountBindingModels
    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
        public string UserName { get; set; }
    }

    public class Login
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public bool IsLoginWithLinedIn { get; set; }

        public bool RememberMe { get; set; }
    }


    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }


    public class SetPasswordViewModel
    {

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

    }

    public class FreelancerInfoForRegistration
    {
        public string UserName { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {

        public RegisterBindingModel()
        {
            //do anything before variable assignment

            //assign initial values
            TokenCompare = "nathan@tighten.io";

            //do anything after variable assignment
        }

        //[Required]
        //[Display(Name = "User Name")]
        //public string UserName { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public int Gender { get; set; }

        [Display(Name = "Activation Date")]
        public string ActivationDate { get; set; }

        [Display(Name = "Is Subscribed")]
        public bool IsSubscribed { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [Display(Name = "Company Email")]
        public string CompanyEmail { get; set; }

        [Required]
        [Display(Name = "Company Phone Number")]
        public string CompanyPhoneNo { get; set; }


        [Display(Name = "Company Fax")]
        public string CompanyFax { get; set; }

        [Required]
        [Display(Name = "Company Website")]
        public string CompanyWebsite { get; set; }

        [Required]
        [Display(Name = "Company Address")]
        public string CompanyAddress { get; set; }

        [Required]
        [Display(Name = "Company Description")]
        public string CompanyDescription { get; set; }


        public string TokenCompare { get; set; }



        [Required]
        [Display(Name = "Registration Token")]
        [Compare("TokenCompare", ErrorMessage = "Token is invalid")]
        public string Token { get; set; }
        public int RoleId { get; set; }
        public decimal Rate { get; set; }
        public bool isFreelancer { get; set; }
        public string UserName { get; set; }
        public string CreditCard { get; set; }
        public string CvvNumber { get; set; }

    }

    public class RegisterExternalBindingModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string UserName { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        // Wheather this is for set password or reset
        public string formType { get; set; }

        public string UserId { get; set; }
        public string Code { get; set; }
        public string EmailId { get; set; }
    }





    public class VendorCompanyInvitationModal
    {
        public int CompanyId { get; set; }

        public string EmailId { get; set; }

        public string UserId { get; set; }

        public string userCode { get; set; }


    }

    public class RegistrationMessageModa
    {
        public int Id { get; set; }

        public string message { get; set; }

        public string EmailId { get; set; }

    }




    public class VendorCompanyRegistrationMailModal
    {

        public int VendorCompanyId { get; set; }

        public bool IsAlreadyRegistered { get; set; }

        public string ReceiverName { get; set; }

        public string SenderCompanyName { get; set; }

        public string UserId { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }


    }



    public class VendorCompanyRegistrationModal
    {

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public string Password { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Website { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

    }

    #endregion

    // Models returned by AccountController actions.
    #region AccountViewModels
    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string UserName { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string UserName { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }




    #endregion
}