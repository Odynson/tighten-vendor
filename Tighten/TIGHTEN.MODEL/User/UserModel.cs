﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class UserModel
    {
        [Required]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string lastName { get; set; }

        public int gender { get; set; }

        [Display(Name = "Is Subscribed")]
        public bool isSubscribed { get; set; }

        public string userId { get; set; }
        public string DuplicateUserId { get; set; }

        public string profilePhoto { get; set; }

        public string originalProfilePhoto { get; set; }

        public int updateflag { get; set; }

        public int CompanyId { get; set; }
         
        public string CompanyName { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyWebsite { get; set; }

        public int TeamId { get; set; }

        public string TeamName { get; set; }
        public bool SendNotificationEmail { get; set; }

        public string Address { get; set; }

        public string PhoneNo { get; set; }


        public string Department { get; set; }

        public string Designation { get; set; }

        public string Experience { get; set; }

        public string Expertise { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string AboutMe { get; set; }

    

        public int NotificationCount { get; set; }

        public int InboxCount { get; set; }

        public string IsAdminLoggedIn { get; set; }

        public string ActiveTeamCreatorPhoto { get; set; }

        public string ActiveTeamCreatorName { get; set; }

        public int RoleId { get; set; }
        public string EmailConfirmationCode { get; set; }
        public string Password { get; set; }

        public string Token { get; set; }

        public Nullable<DateTime> SubscriptionsEndDate { get; set; }
        public bool IsSubscriptionsEnds { get; set; }

        public bool isAccountActive { get; set; }
        public bool isFreelancer { get; set; }
        public decimal Rate { get; set; }
        public decimal? Availabilty { get; set; }
        public bool? isProfilePublic { get; set; }
        public bool IsVendor { get; set; }
        public bool IsActive { get; set; }
        public bool Indefinitely { get; set; }
        public bool ValidUntil { get; set; }
        public DateTime ValidUntilDate { get; set; }

        public int  VendorId { get; set; }
    }

    public class NewUserModel
    {
       
        public UserModel Model { get; set; }
        public string UserId { get; set; }
    }


    public class CompanyAndFreelancerModel
    {
        public string CompanyName { get; set; }
        public string FreelancerName { get; set; }
    }

    public class CompanyInfoToInsertUser
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string UserName { get; set; }
    }


}
