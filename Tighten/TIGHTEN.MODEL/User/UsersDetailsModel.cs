﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class UsersDetailsModel
    {
        public string UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string Address { get; set; }

        public string PhoneNo { get; set; }

        public string ProfilePhoto { get; set; }

        public string Email { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public string Value { get; set; }

        public string Text { get; set; }

        public string UserRole { get; set; }
        public int RoleId { get; set; }
        public bool IsFreelancer { get; set; }
        public bool IsActiveFreelancer { get; set; }

    }
}
