﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
   public class CloudFilesModel
    {

       public class DropBoxFilesModel {

           public string Name { get; set; }

           public string Link { get; set; }
       
       
       }

       public class BoxFilesModel
       {

           public string Name { get; set; }

           public string Url { get; set; }


       }


       public class GoogleDriveFilesModel
       {
           public string Name { get; set; }

           public string Url { get; set; }
       
       
       }


    }
}
