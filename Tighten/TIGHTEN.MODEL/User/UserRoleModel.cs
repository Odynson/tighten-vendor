﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
  public  class UserRoleModel
    {
        
        //public string UserRole { get; set; }
        public int RoleId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        
    }

    public class NewUserRoleModel
    {
        public UserRoleModel UserRoleModel { get; set; }
        public string UserId { get; set; }
    }
}
