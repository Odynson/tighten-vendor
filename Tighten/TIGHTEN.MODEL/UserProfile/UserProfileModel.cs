﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class UserProfileModel
    {

        public class UserEducationDetail
        {
            public int Id { get; set; }
            public string UserId { get; set; }
            public string School { get; set; }
            public string DateAttendedFrom { get; set; }
            public string DateAttendedTo { get; set; }
            public string Degree { get; set; }
            public string CommaSeperatedEducationalStream { get; set; }
            public List<UserEducationalStrem> EducationalStream { get; set; }
            public string Grade { get; set; }
            public string ActivitiesAndSocieties { get; set; }
            public string Description { get; set; }
        }


        public class UserProfessionalDetail
        {
            public int Id { get; set; }
            public string UserId { get; set; }
            public string CompanyName { get; set; }
            public DateTime From { get; set; }
            public DateTime To { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string Title { get; set; }
            public string Location { get; set; }
            public string Description { get; set; }
        }


        public class UserOrganization
        {
            public List<TotalHours> TotalHoursListForOrg { get; set; }
            public string TotalHoursForOrg { get; set; }
            public int CompanyId { get; set; }
            public string CompanyName { get; set; }
            public string CompanyProfilePhoto { get; set; }
            public List<UserProject> ProjectList { get; set; }
        }


        public class UserProject
        {
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public int CompanyId { get; set; }
            public int ProjectAccessLevel { get; set; }
            public string TotalHoursForProject { get; set; }
            public List<TotalHours> TotalHoursListForProject { get; set; }
            public List<UserIds> UserIdOfTeamUsers { get; set; }
            public bool IsTeamMember { get; set; }
        }

        public class UserIds
        {
            public int ProjectId { get; set; }
            public string UserId { get; set; }
        }

        public class TotalHours
        {
            public int ProjectId { get; set; }
            public string hours { get; set; }
            public int CompanyId { get; set; }
        }


        public class projectCountAndHour
        {

            public int TotalProjectsCount { get; set; }

            public List<string> HoursForAllOrg { get; set; }
        }




        public class UserStatistics
        {
            public List<UserOrganization> UserOrg { get; set; }
            public List<UserProject> UserProjects { get; set; }
            public string TotalHoursForAllOrg { get; set; }
            public int TotalProjectsCount { get; set; }
        }


        public class UserOrganizationForEfficiency
        {
            public List<TodoEfficiency> TodoEfficiencyListForOrg { get; set; }
            public decimal TotalEfficiencyForOrg { get; set; }
            public int CompanyId { get; set; }
            public string CompanyName { get; set; }
            public string CompanyProfilePhoto { get; set; }
            public List<UserProjectForEfficiency> ProjectList { get; set; }
        }


        public class UserProjectForEfficiency
        {
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public int CompanyId { get; set; }
            public int ProjectAccessLevel { get; set; }
            public decimal TotalEfficiencyForProject { get; set; }
            public List<TodoEfficiency> TotalEfficiencyListForProject { get; set; }
            public List<UserIds> UserIdOfTeamUsers { get; set; }
            public bool IsTeamMember { get; set; }
        }



        public class SaveFavoriteProjectsModel
        {
            public List<ProjectModel.Project> FavouriteProjectsId { get; set; }
            public string UserId { get; set; }
        }

      
        public class SaveTags
        {
            public List<Tags> Tags { get; set; }
            public string UserId { get; set; }
            public string TagType { get; set; }
        }


        public class Tags
        {
            public int TagId{ get; set; }
            public string TagName { get; set; }
            public bool IsSkillSet { get; set; }
            public string TagDescription { get; set; }
            public bool? TagType { get; set; }
            public bool? IsActive { get; set; }
            public List<Tags> TagList { get; set; }
        }



        public class UserTags
        {
            public List<Tags> allSkillSetTagsObject { get; set; }
            public List<Tags> allBusinessDomainObject { get; set; }
            public List<Tags> SkillSetTags { get; set; }
            public List<Tags> BusinessDomainExpertiseTags { get; set; }
            
        }


        public class UserTagsForProfessionalDetail
        {
            public List<Tags> BusinessDomainExpertise { get; set; }
            public List<Tags> TechnicalSkillSet { get; set; }
        }


        public class UserTeamAndProjectCount
        {
            public int TeamCount { get; set; }
            public int ProjectCount { get; set; }
        }


        public class UserTestimonials
        {
            public List<Testimonials> UserTestimonialList { get; set; }
            public List<Testimonials> AllTestimonialList { get; set; }
        }


        public class Testimonials
        {
            public int TestimonialId { get; set; }
            public string AdminName { get; set; }
            public string CompanyName { get; set; }
            public string Testimonial { get; set; }
            public string UserId { get; set; }
        }


        public class MemberFeedback
        {
            public int ProjectId { get; set; }
            public string MemberUserId { get; set; }
            public string MemberName { get; set; }
            public string Feedback { get; set; }
            public string MemberRole { get; set; }
        }


        public class UserFeedbacks
        {
            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public string CompanyName { get; set; }
            public string ProjectDescription { get; set; }
            public string ProjectRemarks { get; set; }
            public List<MemberFeedback> MemberFeedback { get; set; }
            public DateTime CreatedDate { get; set; }
        }


        public class SaveTestimonial
        {
            public List<Testimonials> Testimonial { get; set; }
            public string UserId { get; set; }
        }


        public class USerProfileStatusModel
        {
            public string UserId { get; set; }
            public bool UserProfileStatus { get; set; }
        }


        public class TodoEfficiency
        {
            public int Id { get; set; }

            public int CompanyId { get; set; }
            public int ProjectId { get; set; }

            public string LoggedTime { get; set; }

            public string TodoEstimatedTime{ get; set; }

            public string TodoLoggedTime { get; set; }

            public decimal TodoEfficiencyInPercent { get; set; }

            public int ProjectsCountCompletedInTime { get; set; }

            public string TodoTagIds { get; set; }
            public int TodoId { get; set; }

        }


        public class TagsList
        {
            public int Id { get; set; }

            public string TagName { get; set; }

            public string TagDescription { get; set; }

            public bool? TagType { get; set; }

            public bool? IsActive { get; set; }

            public int Priority { get; set; }


        }




        public class TodoEfficiencyWithTags
        {
            public decimal TodoEfficiency { get; set; }
            public int TagId { get; set; }
            public int TodoId { get; set; }
            public string TagName{ get; set; }
        }

        public class Technology
        {
            public decimal Efficiency { get; set; }
            public string TagName { get; set; }
        }

        public class TechnologyList
        {
            public List<Technology> Strong { get; set; }
            public List<Technology> week { get; set; }
        }

        public class TodoEfficiencyMeter
        {
            public decimal TodoEfficiencyInPercentForAllOrgs { get; set; }
            public int ProjectsCountCompletedInTimeForAllOrgs { get; set; }
            public List<UserOrganizationForEfficiency> UserOrg { get; set; }
            public List<UserProjectForEfficiency> UserProjects { get; set; }

        }


        public class TodoEfficiencyMeterForStrongAndWeekTech
        {
            public List<Technology> Technology { get; set; }
            public int ProjectsCountCompletedInTime { get; set; }
        }


        public class ProjectEfficiency
        {

            public int ProjectId { get; set; }
            public string ProjectName { get; set; }
            public List<TodoEfficiency> ProjectTodos { get; set; }

        }


        public class UserProjectRate
        {
            public int ProjectId { get; set; }
            public decimal userRateForProject { get; set; }
        }


        public class UserEducationalStrem
        {
            public int Id { get; set; }
            public int IdEduDetail { get; set; }
            public string EducationalStreamName { get; set; }
            public string Description { get; set; }
            public bool IsActive { get; set; }

        }

        public class UserEducationalStremModel
        {
            public List<UserEducationalStrem> allEducationalStreamObject { get; set; }
            public List<UserEducationalStrem> EducationalStream { get; set; }
        }

        public class TeamAndProjectCount
        {
            public int TeamCount { get; set; }
            public int ProjectCount { get; set; }
        }



    }
}
