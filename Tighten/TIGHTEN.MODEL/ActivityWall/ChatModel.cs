﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class ChatModel
    {
        public class Media
        {
            //Isn't the ID the MediaID?
            public int ID { get; set; }

            //fields
            public string MediaName { get; set; }
            public bool ISDeleted { get; set; }
            public int SentBy { get; set; }
            public string MediaType { get; set; }

            //Relationships
            public int MessageID { get; set; }
            //what?
            public int MediaID { get; set; }
        }

        public class groupMessages
        {
            public int id { get; set; }

            [Required]
            public string Message { get; set; }
            public int Sender { get; set; }
            public DateTime SentDate { get; set; }
            public bool ISDeleted { get; set; }
            public int GroupID { get; set; }

            //Relationships
        }

        public class groupMembers
        {
            public int id { get; set; }

            //Fields
            public DateTime JoinedDate { get; set; }
            public int AddedBy { get; set; }
            //What do you mean by this record?
            public int ISGroupLeft { get; set; }

            //Relationships
            public int GroupID { get; set; }
            public int MemberID { get; set; }
        }

        public class Groups
        {
            public int GroupID { get; set; }

            //fields
            public string GroupName { get; set; }
            public string GroupDescription { get; set; }
            public DateTime CreatedDate { get; set; }
            public int TotalMembers { get; set; }
            public int IsActive { get; set; }
            public int MessagesCount { get; set; }

            //Relationships
            public int AdminID { get; set; }
        }

        public class Messages
        {
            public int MessageID { get; set; }

            //fields
            public int Reciever { get; set; }
            public DateTime Date { get; set; }
            public bool ISDeleted { get; set; }

            //relationships?
            public int Message { get; set; }
            public int Sender { get; set; }
        }


        public class Users
        {
            public int UserID { get; set; }

            //fields
            public string EmailAddress { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string ProfilePicturePath { get; set; }
            public bool ISOnline { get; set; }
            public bool ISDeleted { get; set; }
        }
    }
}
