﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class PaymentDetailModel
    {

        public int Id { get; set; }

        public int CompanyId { get; set; }

        public string CardName { get; set; }

        public string CardDescription { get; set; }

        public string CreditCardNumber { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string CvvNumber { get; set; }
        public Nullable<bool> AllowReckringPayment { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        public bool IsDefault { get; set; }

        public string UserId { get; set; }

        public bool IsSelected { get; set; }

    }


}
