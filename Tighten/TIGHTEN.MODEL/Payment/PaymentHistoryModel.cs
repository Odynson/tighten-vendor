﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL 
{
    public class PaymentHistoryModel
    {
        public int Id { get; set; }
        public decimal AmountPaid { get; set; }
        public DateTime TransactionDate { get; set; }

        public string TransactionID { get; set; }

        public int EventID { get; set; }

        public string TransactionType { get; set; }

        public string TransactionBy { get; set; }

        public string TransactionFor { get; set; }

        public string TransactionMadeFor { get; set; }
    }
}
