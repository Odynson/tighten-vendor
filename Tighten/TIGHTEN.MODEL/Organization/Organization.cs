﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class OrganizationModel
    {
        public int CompanyId { get; set; }
        public string CompanyUserId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyUserName { get; set; }
        public string CompanyProfilePic { get; set; }
        public string CompanyUserProfilePic { get; set; }
        public DateTime CompanyAssignedOnDate { get; set; }
        public RequestSentByInfo RequestSentBy { get; set; }
       
        public List<OrganizationTeamMembersModel> TeamMembers { get; set; }
        
        public List<OrganizationTeamMembersModel> ProjectMembers { get; set; }
        public List<LoggedHour> loggedInHours { get; set; }
        public decimal Rate { get; set; }
        public decimal TotalAmount { get; set; }
        public string TotalTime { get; set; }
        public string Role { get; set; }

    }

    public class OrganizationTeamMembersModel
    {
        public List<OrganizationTeamProjectMembersModel> TeamMembers { get; set; }
        public string TeamName { get; set; }
        public string TeamDescription { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<OrganizationProjectModel> Projects { get; set; }

        public int TeamId { get; set; }
    }

    public class OrganizationProjectModel
    {
        public List<OrganizationTeamProjectMembersModel> ProjectMembers { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<LoggedHour> loggedInHours { get; set; }
        public string Hours { get; set; }
        public decimal Amount { get; set; }
        public int TeamId { get; set; }
        public int ProjectId { get; set; }
    }
    public class OrganizationTeamProjectMembersModel
    {
        public string MemberUserId { get; set; }
        public string MemberName { get; set; }
        public string MemberProfilePhoto { get; set; }
        public string MemberEmail { get; set; }

        public int ProjectId { get; set; }
        public int TeamId { get; set; }
    }




    public class RequestSentByInfo
    {
        public string Name { get; set; }
        public string ProfilePic { get; set; }
        public string UserId { get; set; }
    }

    public class LoggedHour
    {
        public string LoggedInHour { get; set; }

        public int CompanyId { get; set; }
        public int ProjectId { get; set; }
    }



}
