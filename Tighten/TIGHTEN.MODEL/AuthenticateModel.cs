﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class AuthenticateModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string TimeStamp { get; set; }
        public string Token { get; set; }
    }
}
