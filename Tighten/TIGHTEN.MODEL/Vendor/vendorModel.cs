﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TIGHTEN.MODEL.ProjectModel;

namespace TIGHTEN.MODEL.Vendor
{
    public class vendorModel
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public string CompanyName { get; set; }

        public string ProjectName { get; set; }

        public string Name { get; set; }

        public int CompanyId { get; set; }

        public int VendorId { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool IsActive { get; set; }

        public string EmailAddress { get; set; }

        public string UserId { get; set; }


        public int PorjectCount { get; set; }

        public bool? IsApproved { get; set; }

        public DateTime? QuoteSenton { get; set; }

        public float Budget { get; set; }

        public float ActualBudget { get; set; }

        public int Hours { get; set; }

        public int ActualHours { get; set; }

        public string IsComplete { get; set; }

        public bool IsAdded { get; set; }

        public bool? IsRejectedByVendor { get; set; }

        public bool? IsRemovedFromProject { get; set; }

        public int CountInvoice { get; set; }

        public int TotalPageCount { get; set; }


        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string ContactNo { get; set; }

        public string Website { get; set; }

        public int VendorCompanyId { get; set; }
         
        public string PhaseMilestoneName { get; set; }

        public int MilestoneId { get; set; }
        public int PhaseId { get; set; }
    }


    public class projectQuotedModel
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int CompanyId { get; set; }

        public string Name { get; set; }

        public int EstimatedHours { get; set; }

        public int VendorCount { get; set; }

        public int QuotationCount { get; set; }

        public bool IsAssigned { get; set; }

        public int EstimatedBudget { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public int TotalPageCount { get; set; }



    }

    public class VendorInvitationModal
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public string EmaiAddress { get; set; }
        public string PhoneNo { get; set; }
        public string Website { get; set; }
        

        public string MessageTextBox { get; set; }

        public string UserId { get; set; }

        public int CompanyId { get; set; }
        public int CId { get; set; }
        public List<VendorPOCModel> VendorPOCList { get; set; }
        public List<VendorPOCModel> DeletedVendorPOCList { get; set; }
    }


    public class VendorInvitationsModal
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string CompanyName { get; set; }

        public string Invitedby { get; set; }

        public string CompanyId { get; set; }

        public int ProjectId { get; set; }

        public string Message { get; set; }

        public string InvitationType { get; set; }

        public DateTime InvitationDate { get; set; }

        public string Name { get; set; }

        public int VendorId { get; set; }

        public bool? IsApproved { get; set; }

        public string ReasonReject { get; set; }

        public int TotalPageCount { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

    }

    public class VendorCompanyMail
    {
        public string SenderCompanyName { get; set; }

        public string VendorCompany { get; set; }

        public string ReceiverName { get; set; }

        public string SenderName { get; set; }

        public string SenderEmail { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Message { get; set; }

        public string UserId { get; set; }

        public string EmailConfirmationCode { get; set; }

        public string InvitationDate { get; set; }

        public int Id { get; set; }

        public string ProjectName { get; set; }

        public int VendorCompanyId { get; set; }

    }

    public class VendorQuoteModal
    {
        public string Name { get; set; }

        public string UserId { get; set; }

        public int ProjectId { get; set; }

        public int VendorId { get; set; }

        public int TotalBudget { get; set; }

        public int TotalHours { get; set; }

        public string Description { get; set; }

        public string AllDescription { get; set; }

        public string CreatedDate { get; set; }

        public bool? IsProjectUpdated { get; set; }

        public DateTime? ProjectStartDate { get; set; }

        public DateTime? ProjectEndDate { get; set; }
        public int InvoiceNo { get; set; }

        public bool? IsActive { get; set; }

        public int? IsComplete { get; set; }



        public List<ProjectDocument> ProjectDocumentList { get; set; }

        public List<vendorPhaseModal> PhaseList { get; set; }

        public List<ResourceModal> ResourcesList { get; set; }

        public List<VendorProjectProgress> VendorProjectProgressList { get; set; }

        public CustomControlValueModel CustomControlValueModel { get; set; }

        public List<CustomControlUIModel> CustomControlUIList { get; set; }

    }


    public class ProjectDocument
    {
        public string ActualFileName { get; set; }
        public string FileName { get; set; }
        public String FilePath { get; set; }


    }


    public class vendorPhaseModal
    {

        public List<vendorMilestone> MileStone { get; set; }

        public List<PhaseDocument> PhaseDocumentList { get; set; }

        public int Id { get; set; }
        public int PhaseId { get; set; }

        public string Name { get; set; }

        public int PhaseVendorHour { get; set; }

        public int PhaseVendorBudget { get; set; }

        public string Description { get; set; }

    }


    public class PhaseDocument
    {
        public int PhaseId { get; set; }
        public string ActualFileName { get; set; }
        public string FileName { get; set; }
        public String FilePath { get; set; }


    }



    public class vendorMilestone
    {
        public int Id { get; set; }
        public int PhaseId { get; set; }

        public string Name { get; set; }

        public int MilestoneId { get; set; }

        public int MilestoneVendorBudget { get; set; }

        public int MilestoneVendorHour { get; set; }
        public string Description { get; set; }

    }


    public class ResourceModal
    {
        public int Id { get; set; }
        public int RoleId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Profile { get; set; }

        public int? HourlyRate { get; set; }

        public int? ExpectedHours { get; set; }

        public string DesignationName { get; set; }

        public decimal? PercentageFromTotal { get; set; }

        public int ProjectId { get; set; }

        public int SpentHours { get; set; }
    }



    public class VendorPhaseCustom
    {
        public int Id { get; set; }

        public int PhaseId { get; set; }

        public int Budget { get; set; }

        public int BudgetHour { get; set; }

        public string MilestoneMileIdList { get; set; }


        public string MilestoneBudgetList { get; set; }

        public string MilestoneHourList { get; set; }

    }

    public class VendorProjectProgress
    {
        public int Id { get; set; }

        public string InvoiceNumber { get; set; }

        public decimal? InvoiceCost { get; set; }

        public int? Hours { get; set; }

        public int? TotalHours { get; set; }

        public decimal? TotalCost { get; set; }

        public int? TotalPaidHour { get; set; }

        public bool? IsPaid { get; set; }

        public DateTime? InvoiceDate { get; set; }



    }


    public class VendorFilterModal
    {
        public int CompanyId { get; set; }

        public bool? IsApproved { get; set; }

        public int VendorCompanyId { get; set; }
        public int VendorId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }


        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }


    public class VendorCompanyModel
    {
        public int VendorCompanyId { get; set; }
        public string Name { get; set; }

    }


    public class VendorCompanyPocModal
    {

        public string VendorPocName { get; set; }

        public string Email { get; set; }

        public string Rating { get; set; }


        public string Project { get; set; }

        public string ProfilePhoto { get; set; }

    }

    public class RequestProjectDetailforVendorModal
    {

        public int CompanyId { get; set; }

        public int ProjectId { get; set; }

        public String UserId { get; set; }

        public int VendorId { get; set; }

    }

    public class MailVendorAdminProfileModel
    {

        public string Email { get; set; }

        public string ReceiverName { get; set; }

        public string SenderName { get; set; }

        public string ProjectName { get; set; }

        public double Amount { get; set; }



    }

    public class MailSendercompanyModel
    {

        public string SenderCompanyName { get; set; }

        public string SenderName { get; set; }
    }


    public class MailReceiverVendorModel
    {

        public string ReceiverCompanyName { get; set; }

        public string ReceiverName { get; set; }

        public string Email { get; set; }


        public string Pocs { get; set; }
    }


    public class UserInvitationModal
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Name { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string ProfilePhoto { get; set; }

        public string MessageTextBox { get; set; }

       // public UserRoleModel[] UserRoleList { get; set; }

        public int JobProfileId { get; set; }

        public string JobProfileName { get; set; }

        public bool EmailConfirmed { get; set; }

        public DateTime EmailConfirmationCodeCreatedDate { get; set; } //Date of joining of internal users

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }

        public bool Indefinitely { get; set; }

        public bool ValidUntil { get; set; }

        public DateTime ValidUntilDate { get; set; }

        public decimal Rating { get; set; }

        public int TotalPageCount { get; set; }

        public List<UserRoleModel> UserRoleList { get; set; }

    }

    public class NewUserMail
    {
        public string SenderCompanyName { get; set; }

        public string VendorCompany { get; set; }

        public string ReceiverName { get; set; }

        public string SenderName { get; set; }

        public string SenderEmail { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Message { get; set; }

        public string UserId { get; set; }

        public string EmailConfirmationCode { get; set; }

        public string InvitationDate { get; set; }

        public int Id { get; set; }

        public string ProjectName { get; set; }

        public StringBuilder RoleName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public decimal Rating { get; set; }

        public string Password { get; set; }

        public bool IsActive { get; set; }

        public bool Indefinitely { get; set; }

        public bool ValidUntil { get; set; }

        public DateTime ValidUntilDate { get; set; }


    }

    public class VendorPOCModel
    {
        public int Id { get; set; }

        public string pocFirstName { get; set; }

        public string pocLastName { get; set; }

        public string pocEmail { get; set; }

        public int CompanyId { get; set; }

        public int VendorCompanyId { get; set; }

        public string UserId { get; set; }
        public int RoleId { get; set; }

        public string pocFullName { get; set; }
    }
    public class AccountMail
    {
        public string SenderCompanyName { get; set; }

        public string VendorCompany { get; set; }

        public string ReceiverName { get; set; }

        public string SenderName { get; set; }

        public string SenderEmail { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Message { get; set; }

        public string UserId { get; set; }

        public string EmailConfirmationCode { get; set; }

        public string InvitationDate { get; set; }

        public int Id { get; set; }

        public string ProjectName { get; set; }

        public int VendorCompanyId { get; set; }

    }
}
