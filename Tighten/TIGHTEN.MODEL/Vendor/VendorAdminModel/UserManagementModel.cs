﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.Vendor.VendorAdminModel
{
    public class UserManagementModel
    {

        public List<ExistingUsersModel> ExistingUsersList { get; set; }

        public List<ResourceModel> ResourceList { get; set; }

    }


    //class for POCs invited from invite vendor screen
    public class ExistingUsersModel
    {
        public int VendorId { get; set; }

        public string VendorName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime JoinDate { get; set; }

        public string Email { get; set; }

        public decimal Rating { get; set; }

        public string RoleName { get; set; }

        public String ProfilePhoto { get; set; }

        public bool? IsActive { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public bool? Indefinitely { get; set; }
        public bool? ValidUntil { get; set; }
        public DateTime ValidUntilDate { get; set; }
        public int TotalPageCount { get; set; }
    }


    //class for resources
    public class ResourceModel
    {

        public int ResourceId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Profile { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public int TotalPageCount { get; set; }

    }



    public class ResourceStatisticsModel
    {



        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Profile { get; set; }

        public string Email { get; set; }

        public decimal Rating { get; set; }

        public decimal HorsWorked { get; set; }

        public string AllworkedRole { get; set; }

        public List<RoleVendorAdminProfileModel> RoleList { get; set; }

        public List<VendorProjectDetailModel> VendorProjectList { get; set; }
    }


    public class RoleVendorAdminProfileModel
    {

        public int RoleId { get; set; }

        public string RoleName { get; set; }

    }

    public class VendorProjectDetailModel
    {
        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

        public decimal Rating { get; set; }

        public string RoleName { get; set; }

        public int Hours { get; set; }

        public int IsComplete { get; set; }

    }



    public class ResourceRequestModel
    {

        public string Email { get; set; }
    }


    public class SaveExistingUserModel
    {

        public int VendorId { get; set; }

        public int RoleId { get; set; }

        public bool? IsActive { get; set; }
        public string Password { get; set; }
        public bool? Indefinitely { get; set; }
        public bool? ValidUntil { get; set; }
        public DateTime ValidUntilDate { get; set; }
    }


    public class SaveResourceAsVendorRequestModel
    {

        public int VendorCompanyId { get; set; }

        public int RoleId { get; set; }

        public string Email { get; set; }


        public string EmailConfirmationCode { get; set; }

        public string ReceiverName { get; set; }

        public string SenderName { get; set; }

        public string UserId { get; set; }

        public string SenderCompanyName { get; set; }



    }

    //class for internal users invited from invite new user screen
    //public class InternalUsersModel
    //{
    //    public int Id { get; set; }

    //    public string UserId { get; set; }

    //    public string Email { get; set; }

    //    public string Password { get; set; }

    //    public string FirstName { get; set; }

    //    public string LastName { get; set; }

    //    public string Name { get; set; }

    //    public int CompanyId { get; set; }

    //    public int RoleId { get; set; }

    //    public int JobProfileId { get; set; }

    //    public string JobProfileName { get; set; }

    //    public bool EmailConfirmed { get; set; }

    //    public DateTime EmailConfirmationCodeCreatedDate { get; set; } //Date of joining of internal users

    //    public bool IsDeleted { get; set; }

    //    public bool IsActive { get; set; }

    //    public bool Indefinitely { get; set; }

    //    public bool ValidUntil { get; set; }

    //    public DateTime ValidUntilDate { get; set; }

    //    public decimal Rating { get; set; }

    //    public List<UserRoleModel> UserRoleModelList { get; set; }

    //}

}