﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.Vendor.VendorAdminModel
{




    public class InvoiceVendorAdminProfileModel
    {
        public string VendorName { get; set; }

        public int InvoiceId { get; set; }

        public string InvoiceNumber { get; set; }
        public string UserId { get; set; }

        public string UserName { get; set; }

        public int CompanyId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public string SendTo { get; set; }

        public string Message { get; set; }

        public DateTime InvoiceDate { get; set; }
        public string TotalHours { get; set; }
        public decimal TotalAmount { get; set; }
        public string ApprovedBy { get; set; }
        public bool InvoiceApprovedStatus { get; set; }

        public bool IsPaid { get; set; }
        public string PaidBy { get; set; }
        public bool IsRejected { get; set; }
        public string RejectedBy { get; set; }

        public string ProjectName { get; set; }
        public string CompanyName { get; set; }
        public int ProjectId { get; set; }
        public int VendorId { get; set; }
        public int PhaseId { get; set; }
        public string MilestoneId { get; set; }

        public string Email { get; set; }
        public int TotalPageCount { get; set; }

        //public List<InvoiceToMilestoneModal> InvoiceToMilestoneList { get; set; }
        //public List<InvoiceExtraResourceModal> InvoiceToExtraResourceList { get; set; }

    }


    public class InvoiceForResourceModal
    {
        public int Id { get; set; }

        public decimal Rate { get; set; }

        public string Name { get; set; }

        public int Hours { get; set; }
    }

    public class InvoicePreviewVendorAdminProfileModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string CompanyUserName { get; set; }
        public string UserId { get; set; }
        public string CompanyProfilePic { get; set; }

        public string InvoiceNumber { get; set; }
        public string VendorCompanyName { get; set; }
        public string VendorUserName { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string TotalHours { get; set; }
        public decimal TotalAmount { get; set; }
        public string Message { get; set; }
        public bool? IsRejected { get; set; }
        public bool? IsPaid { get; set; }
        public bool InvoiceApprovedStatus { get; set; }
        public string RejectReason { get; set; }

        public string CompanyLogo { get; set; }

        public List<InvoiceForResourceModal> InvoiceForResourceList { get; set; }
        public List<InvoiceToMilestoneModal> InvoiceToMilestoneList { get; set; }
        public List<InvoiceExtraResourceModal> InvoiceToExtraResourceList { get; set; }
    }



    public class InvoiceToMilestoneModal
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Hours { get; set; }

    }

    public class InvoiceExtraResourceModal
    {

        public int Id { get; set; }

        public string Description { get; set; }

        public int Amount { get; set; }

    }


    public class InvoiceFilterAdminProfileModel
    {
        public int VendorCompanyId { get; set; }
        
        public int CompanyId { get; set; }

        public int VendorId { get; set; }

        public DateTime ? FromDate { get; set; }

        public DateTime ? ToDate { get; set; }

        public int PageSizeSelected { get; set; }
        public int PageIndex { get; set; }

    }



}
