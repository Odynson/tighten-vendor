﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.VendorAdminModal
{
  

    public class AdminProfileVendorInvitationModel
    {
        public  string VendorId { get; set; }

        public string VendorName { get; set; }

        public string CompanyName { get; set; }

        public bool? IsApproved { get; set; }

        public int CompanyId { get; set; }

        public DateTime? InvitationDate { get; set; }


        public int TotalPageCount { get; set; }


    }

    public class CompanyModel
    {
        public int CompanyId { get; set; }

        public string Name { get; set; }


    }
    public class VendorModel
    {
        public int VendorId { get; set; }

        public string Name { get; set; }


    }


    public class FilterVendorInvitationModel
    {
        public int VendorCompanyId { get; set; }

        public int CompanyId { get; set; }

        public int VendorId { get; set; }

        public int IsApprovedId { get; set; }

        public bool? IsApproved { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }
    }

    public class ProjectToQuotevendorModel
    {
        

        public int ProjectId { get; set; }

        public string CompanyName { get; set; }

        public string ProjectName { get; set; }

        public string VendorName { get; set; }

        
        public int VendorId { get; set; }

        public DateTime? CreatedDate { get; set; }

        
        public bool? IsApproved { get; set; }

        public DateTime? QuoteSenton { get; set; }

        public float Budget { get; set; }

        public int Hours { get; set; }

        public bool? IsRejectedByVendor { get; set; }

        public int TotalPageCount { get; set; }

    }


    public class ProjectToQuoteFilterModel
    {

        public int ProjectId { get; set; }

        public int VendorId { get; set; }

        public int CompanyId { get; set; }

        public int StatusId { get; set; }

        public int VendorCompanyId { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }



    }


    public class AutoCompleteProjectToQuoteModel
    {

        public int ProjectId { get; set; }

        public String ProjectName { get; set; }

    }


    public class ProjectToQuoteViewDetailRequestModel
    {

        public int ProjectId { get; set; }

        public int VendorId { get; set; }

    }

    public class VendorQuoteViewDetailForAdminProfileModal
    {
        public string Name { get; set; }

        public string UserId { get; set; }

        public int ProjectId { get; set; }

        public int VendorId { get; set; }

        public int TotalBudget { get; set; }

        public int TotalHours { get; set; }

        public string Description { get; set; }

        public string AllDescription { get; set; }

        public string CreatedDate { get; set; }

        public bool? IsProjectUpdated { get; set; }

        public DateTime? ProjectStartDate { get; set; }

        public DateTime? ProjectEndDate { get; set; }
        public int InvoiceNo { get; set; }

        public bool? IsActive { get; set; }

        public int? IsComplete { get; set; }

        public List<ProjectDocument> ProjectDocumentList { get; set; }

        public List<vendorPhaseModal> PhaseList { get; set; }

        public List<ResourceModal> ResourcesList { get; set; }

        public List<VendorProjectProgress> VendorProjectProgressList { get; set; }

        public   VendorAdminCustomControlValueModel CustomControlValueModel { get;set; }

        public List<VendorAdminProfileCustomControlUIModel> CustomControlUIList { get; set; }
    }



    public class ResourceModal
    {
        public int Id { get; set; }
        public int RoleId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Profile { get; set; }

        public int? HourlyRate { get; set; }

        public int? ExpectedHours { get; set; }

        public string DesignationName { get; set; }

        public decimal? PercentageFromTotal { get; set; }

        public int ProjectId { get; set; }

        public int SpentHours { get; set; }
    }


    public class ProjectDocument
    {
        public string ActualFileName { get; set; }
        public string FileName { get; set; }
        public String FilePath { get; set; }


    }


    public class vendorPhaseModal
    {

        public List<vendorMilestone> MileStone { get; set; }

        public List<PhaseDocument> PhaseDocumentList { get; set; }

        public int Id { get; set; }
        public int PhaseId { get; set; }

        public string Name { get; set; }

        public int PhaseVendorHour { get; set; }

        public int PhaseVendorBudget { get; set; }

        public string Description { get; set; }

    }


    public class PhaseDocument
    {
        public int PhaseId { get; set; }
        public string ActualFileName { get; set; }
        public string FileName { get; set; }
        public String FilePath { get; set; }


    }



    public class vendorMilestone
    {
        public int Id { get; set; }
        public int PhaseId { get; set; }

        public string Name { get; set; }

        public int MilestoneId { get; set; }

        public int MilestoneVendorBudget { get; set; }

        public int MilestoneVendorHour { get; set; }
        public string Description { get; set; }

    }


    public class VendorProjectProgress
    {
        public int Id { get; set; }

        public string InvoiceNumber { get; set; }

        public decimal? InvoiceCost { get; set; }

        public int? Hours { get; set; }

        public int? TotalHours { get; set; }

        public decimal? TotalCost { get; set; }

        public int? TotalPaidHour { get; set; }

        public bool? IsPaid { get; set; }

        public DateTime? InvoiceDate { get; set; }



    }

    public class AWardedProjectAdminProfileModel
    {
        public int Id { get; set; }

        public string VendorName { get; set; }

        public int ProjectId { get; set; }

        public string CompanyName { get; set; }

        public string ProjectName { get; set; }

        public string Name { get; set; }

        public int CompanyId { get; set; }

        public int VendorCompanyId { get; set; }

        public int VendorId { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool IsActive { get; set; }

        public string EmailAddress { get; set; }

        public string UserId { get; set; }


        public int PorjectCount { get; set; }

        public bool? IsApproved { get; set; }

        public DateTime? QuoteSenton { get; set; }

        public float Budget { get; set; }

        public float ActualBudget { get; set; }

        public int Hours { get; set; }

        public int ActualHours { get; set; }

        public string IsComplete { get; set; }

        public bool IsAdded { get; set; }

        public bool? IsRejectedByVendor { get; set; }

        public bool? IsRemovedFromProject { get; set; }

        public int CountInvoice { get; set; }

        public int TotalPageCount { get; set; }

        public decimal Percentage { get; set; }




    }

    public class AwardedProjectListRequestModel
    {

        public int ProjectId { get; set; }

        public int VendorId { get; set; }

        public int StatusId { get; set; }

        public int CompanyId { get; set; }


        public bool UnpaidInvoice { get; set; }

        public int VendorCompanyId { get; set; }

        public int Pagesize { get; set; }

        public int PageIndex { get; set; }


    }

    public class ProjectAwardedViewDetailModel
    {
        public string Name { get; set; }

        public string UserId { get; set; }

        public int ProjectId { get; set; }

        public int VendorId { get; set; }

        public int CompanyId { get; set; }

        public int TotalBudget { get; set; }

        public int TotalHours { get; set; }

        public string Description { get; set; }

        public string AllDescription { get; set; }

        public string CreatedDate { get; set; }

        public bool? IsProjectUpdated { get; set; }

        public DateTime? ProjectStartDate { get; set; }

        public DateTime? ProjectEndDate { get; set; }
        public int InvoiceNo { get; set; }

        public bool? IsActive { get; set; }

        public int? IsComplete { get; set; }

        public List<ProjectDocument> ProjectDocumentList { get; set; }

        public List<vendorPhaseModal> PhaseList { get; set; }

        public List<ResourceModal> ResourcesList { get; set; }

        public List<VendorProjectProgress> VendorProjectProgressList { get; set; }

        public VendorAdminCustomControlValueModel CustomControlValueModel { get; set; }

        public List<VendorAdminProfileCustomControlUIModel> CustomControlUIList { get; set; }


    }



    public class ProjectCompleteFeedBackModal
    {

        public int ProjectId { get; set; }

        public int VendorId { get; set; }

        public string ProjectName { get; set; }

        public string CompanyName { get; set; }

        public string UserName { get; set; }

        public DateTime? ProjectStartDate { get; set; }


        public DateTime? ProjectEndDate { get; set; }

        public int BudgetHours { get; set; }

        public int ActualHours { get; set; }

        public int BudgetAmount { get; set; }

        public int ActualAmount { get; set; }

        public string ProfilePic { get; set; }

        public int Invoice { get; set; }

        public string vendorFeedback { get; set; }

        public decimal? Rating { get; set; }

        public int? IsComplete { get; set; }

        public List<VendorResourceForFeedBackModel> VendorResourceList { get; set; }


    }

    public class VendorResourceForFeedBackModel
    {
        public int Id { get; set; }

        public int VendorId { get; set; }

        public string Name { get; set; }

        public string Designation { get; set; }

        public int BudgetedHours { get; set; }

        public int ActualHours { get; set; }

        public string Companyfeedback { get; set; }

        public decimal Rating { get; set; }


    }

    public class VendorAdminProfileCustomControlUIModel
    {

        public int CustomControlId { get; set; }

        public string FieldName { get; set; }

        public string Watermark { get; set; }

        public string ControlType { get; set; }

        public List<string> controlList { get; set; }

        public string Values { get; set; }


        public string RadioButton { get; set; }

        public string DescriptionCustomControl { get; set; }

        public string FieldModel { get; set; }

        public List<CustomUIDropDownModel> CustomDropDownList { get; set; }

        public List<CustomUIRadioButtonModel> CustomRadioButtonList { get; set; }


    }






    public class CustomUIDropDownModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }



    public class CustomUIRadioButtonModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }


    public class VendorAdminCustomControlValueModel
    {
        public int Id { get; set; }

        public string CustomControlData { get; set; }


    }

}
