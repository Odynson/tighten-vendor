﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.VendorGeneralProfile
{
    public class VendorGeneralProfileModel
    {

    }

    public class ProjectVendorGeneralProfile
    {
        public int ProjectId { get; set; }

        public string ProjectName { get; set; }

        public DateTime? ProjectStartDate { get; set; }

        public double HoursAllotted { get; set; }


        public double HoursSpent { get; set; }

        public string RoleAllotted { get; set; }


        public string VendorPoc { get; set; }

        public decimal Rating { get; set; }

        public int TotalPageCount { get; set; }

    }


    public class FilterProjectListVendorGeneralProfile
    {

        public string VendorGeneralUserId { get; set; }

        public string ProjectName { get; set; }

        public int ProjectId { get; set; }

        public int ProjectStatus { get; set; }


        public int PageIndex { get; set; }

        public int PageSizeSelected { get; set; }

    }


    public class ProjectModel
    {
        public string ProjectName { get; set; }

        public int ProjectId { get; set; }


    }


    public class WorkDiaryModel
    {

        public int ProjectId { get; set; }

        public string VendorGeneralUserId { get; set; }

        public string Note { get; set; }

        public DateTime NoteDate { get; set; }

        public int Hour { get; set; }



    }

    public class VendorGeneralProfProjectAwardedViewDetailModel
    {
        public string Name { get; set; }

        public string UserId { get; set; }

        public int ProjectId { get; set; }

        public int VendorId { get; set; }

        public int CompanyId { get; set; }

        public int TotalBudget { get; set; }

        public int TotalHours { get; set; }

        public string Description { get; set; }

        public string AllDescription { get; set; }

        public string CreatedDate { get; set; }

        public bool? IsProjectUpdated { get; set; }

        public DateTime? ProjectStartDate { get; set; }

        public DateTime? ProjectEndDate { get; set; }
        public int InvoiceNo { get; set; }

        public bool? IsActive { get; set; }

        public int? IsComplete { get; set; }


        public List<VendorGeneralProfProjectDocument> ProjectDocumentList { get; set; }

        public List<VendorGeneralProfVendorPhaseModal> PhaseList { get; set; }

        public List<VendorProjectProgress> VendorProjectProgressList { get; set; }

        public VendorGeneralProfCustomControlValueModel CustomControlValueModel { get; set; }

        public List<VendorGeneralProfCustomControlUIModel> CustomControlUIList { get; set; }

        public List<VendorGeneralProfResourceModal> ResourcesList { get; set; }


    }


    public class VendorGeneralProfProjectDocument
    {
        public string ActualFileName { get; set; }
        public string FileName { get; set; }
        public String FilePath { get; set; }


    }



    public class VendorGeneralProfVendorPhaseModal
    {

        public List<VendorGeneralProfMilestone> MileStone { get; set; }

        public List<VendorGeneralProfPhaseDocument> PhaseDocumentList { get; set; }

        public int Id { get; set; }
        public int PhaseId { get; set; }

        public string Name { get; set; }

        public int PhaseVendorHour { get; set; }

        public int PhaseVendorBudget { get; set; }

        public string Description { get; set; }

    }


    public class VendorGeneralProfPhaseDocument
    {
        public int PhaseId { get; set; }
        public string ActualFileName { get; set; }
        public string FileName { get; set; }
        public String FilePath { get; set; }


    }



    public class VendorGeneralProfMilestone
    {
        public int Id { get; set; }
        public int PhaseId { get; set; }

        public string Name { get; set; }

        public int MilestoneId { get; set; }

        public int MilestoneVendorBudget { get; set; }

        public int MilestoneVendorHour { get; set; }
        public string Description { get; set; }

    }


    public class VendorProjectProgress
    {
        public int Id { get; set; }

        public string InvoiceNumber { get; set; }

        public decimal? InvoiceCost { get; set; }

        public int? Hours { get; set; }

        public int? TotalHours { get; set; }

        public decimal? TotalCost { get; set; }

        public int? TotalPaidHour { get; set; }

        public bool? IsPaid { get; set; }

        public DateTime? InvoiceDate { get; set; }

    }

    public class VendorGeneralProfResourceModal
    {
        public int Id { get; set; }
        public int RoleId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Profile { get; set; }

        public int? HourlyRate { get; set; }

        public int? ExpectedHours { get; set; }

        public string DesignationName { get; set; }

        public decimal? PercentageFromTotal { get; set; }

        public int ProjectId { get; set; }

        public int SpentHours { get; set; }
    }

    public class VendorGeneralProfCustomControlUIModel
    {

        public int CustomControlId { get; set; }

        public string FieldName { get; set; }

        public string Watermark { get; set; }

        public string ControlType { get; set; }

        public List<string> controlList { get; set; }

        public string Values { get; set; }

        public string RadioButton { get; set; }

        public string DescriptionCustomControl { get; set; }

        public string FieldModel { get; set; }

        public List<VendorGeneralProfCustomUIDropDownModel> CustomDropDownList { get; set; }

        public List<VendorGeneralProfCustomUIRadioButtonModel> CustomRadioButtonList { get; set; }


    }
    public class VendorGeneralProfCustomUIDropDownModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }
    public class VendorGeneralProfCustomUIRadioButtonModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }
    public class VendorGeneralProfCustomControlValueModel
    {
        public int Id { get; set; }

        public string CustomControlData { get; set; }


    }
}
