﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class FreelancerModel
    {
        public string Email { get; set; }
        public string Link { get; set; }
        public int ProjectId { get; set; }
        public int RoleId { get; set; }
    }

    public class MailSendToFreelancerOnInvitationSent
    {
        public string emailbody { get; set; }
    }


    public class FreelancerInvitationModel
    {
        public int Id { get; set; }
        public string invitationType { get; set; }
        public string EmailOrHandle { get; set; }
        public string EmailForFreelancer { get; set; }
        public string LinkForFreelancer { get; set; }
        public string SentBy { get; set; }
        public int CompanyId { get; set; }
        public string Link { get; set; }
        public int ProjectId { get; set; }
        public List<int> ProjectIdList { get; set; }
        public int RoleId { get; set; }
        public string Activationkey { get; set; }
        public bool IskeyActive { get; set; }
        public DateTime SentDate { get; set; }
        public bool IsAccepted { get; set; }
        public string IsFreelancerUserIdExist { get; set; }
        public decimal UserRate { get; set; }
        public int WeeklyLimit { get; set; }
        public bool IsHireMe { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
    }

    public class InsertFreelancer
    {
        public RegisterBindingModel RegisterBindingModel { get; set; }
        public FreelancerInvitationModel FreelancerInvitationModel { get; set; }
        public string pictureUrl { get; set; }
    }

    public class FreelancerInfoToConfirmLink
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ProfilePic { get; set; }
        public int LastRoleInCompany { get; set; }

        public string CompanyName { get; set; }
    }


    public class confirmEmailOrHandleModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ProfilePic { get; set; }
        public string InvitationStatus { get; set; }
        public int LastRoleInCompany { get; set; }
        public bool isEmailOrHandleActive { get; set; }
        public List<PreviousProject> PreviousProjectsList { get; set; }
    }


    public class PreviousProject
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string RoleInProject { get; set; }
    }


    public class CompaniesForFreelancer
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
    }


    public class InvitationNotificationModal
    {
        public int FreelancerInvitationId { get; set; }
        public string FreelancerUserId { get; set; }
        public string EmailForFreelancer { get; set; }
        public string SentBy { get; set; }
        public int ProjectId { get; set; }
        public decimal UserRate { get; set; }
        public int WeeklyLimit { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public bool? IsJobInvitation { get; set; }
        public int RoleId { get; set; }
        public string ProjectName { get; set; }
        public string Role { get; set; }
        public string SenderName { get; set; }
        public string SenderProfilePic { get; set; }
        public bool IskeyActive { get; set; }
        public bool IsAccepted { get; set; }
        public bool? IsRejected { get; set; }
        public CompanyModalForInvitaion CompanyDetail { get; set; }
        public string RejectionReason { get; set; }

        public decimal FreelancerRate { get; set; }
        public string FileName { get; set; }
        public string ActualFileName { get; set; }
        public string FilePath { get; set; }
        public string AttachmentDescription { get; set; }
        public string Responsibilities { get; set; }

        public string FreelancerName { get; set; }
        public string FreelancerProfilePic { get; set; }
        public decimal FreelancerProfileRate { get; set; }

    }




    public class InvitationMessagesModal
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string SentBy { get; set; }
        public string SenderName { get; set; }
        public string SenderProfilePic { get; set; }
        public string ReceiverName { get; set; }
        public int InvitationId { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
        public DateTime CreatedDate { get; set; }

    }


    public class InvitationFreelancerInfoModal
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public decimal RateOffered { get; set; }
        public string Notes { get; set; }

        public string FreelancerName { get; set; }
        public string FreelancerProfilePic { get; set; }
        public decimal FreelancerProfileRate { get; set; }

        public List<InvitationAttachmentModel> Attachments { get; set; }

        public int ProjectId { get; set; }
        
    }


    public class InvitationAttachmentModel
    {
        public string FileName { get; set; }
        public string ActualFileName { get; set; }
        public string FilePath { get; set; }

        public int ProjectId { get; set; }
        public string UserId { get; set; }
    }


    public class CompanyModalForInvitaion
    {
        public string UsersIdForCompany { get; set; }
        public string CompanyName { get; set; }
        public string CompanyProfilePic { get; set; }
        public string CompanyUserId { get; set; }
        public int CompanyId { get; set; }
    }

    public class FreelancerInfoToSendInvitationModel
    {
        public string EmailOrHandle { get; set; }
        public int CompanyId { get; set; }
        public string UserId { get; set; }
        public string chkInvitationType { get; set; }
    }

    public class FreelancerForProject
    {
        public string FreelancerUserId { get; set; }
        public string FreelancerName { get; set; }
        public string FreelancerEmail { get; set; }
    }

    public class FreelancerForProjectModel
    {
        public List<FreelancerForProject> allFreelancersForProjects { get; set; }
        public List<FreelancerForProject> selectedFreelancersForProjects { get; set; }
    }


    public class AllSentRequestForAdminModel
    {
        public int FreelancerInvitationId { get; set; }
        public string FreelancerEmail { get; set; }
        public string FreelancerUserId { get; set; }
        public string FreelancerProfilePhoto { get; set; }
        public int CompanyId { get; set; }
        public string SentBy { get; set; }
        public int ProjectId { get; set; }
        public decimal UserRate { get; set; }
        public int RoleId { get; set; }
        public string Activationkey { get; set; }
        public bool IskeyActive { get; set; }
        public DateTime SentDate { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsRejected { get; set; }
        public int WeeklyLimit { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public string Remarks { get; set; }
        public bool IsJobInvitation { get; set; }

        public string SenderName { get; set; }
        public string ProjectName { get; set; }
        public string RoleName { get; set; }
        public int TotalPageCount { get; set; }

    }


    public class ParentCompanyInfo
    {
        public string UserName { get; set; }
        public string CompanyName { get; set; }
    }



}
