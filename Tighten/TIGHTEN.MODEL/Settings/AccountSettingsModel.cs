﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class AccountSettingsModel
    {
        public string UserId { get; set; }

        public string RoutingNumber { get; set; }

        public string AccountNumber { get; set; }

        public string SocialSecurityNumber { get; set; }

        public string Address { get; set; }

        public string AddressCity { get; set; }

        public string AddressState { get; set; }

        public string AddressPostalCode { get; set; }

        public string Country { get; set; }

        public DateTime DOB { get; set; }

        public string BirthDay { get; set; }
        public string BirthMonth { get; set; }
        public string BirthYear { get; set; }

    }



    public class MailSendVendorAdminProfileAccountSendingModel
    {

        public string Email { get; set; }

        public string ReceiverName { get; set; }

        public string SenderCompanyName { get; set; }

    }
    public class updatePriorityModel
    {

        public int FirstCustomControlId { get; set; }

        public int SecondCustomControlId { get; set; }

        public int FirstPriorityId{ get; set; }
        public int SecondPriorityId { get; set; }

    }
}
