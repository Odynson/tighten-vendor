﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL 
{
    public class GetUserNotificationConfigModel
    {
        public int NotificationId { get; set; }
        public string UserId { get; set; }
        public string NotificationName { get; set; }
        public string NotificationText { get; set; }
        public string NotificationDescription { get; set; }
        public int SettingSectionId { get; set; }
        public string SettingSectionName { get; set; }
        public bool IsActive { get; set; }
        public bool IsUserNotificationActive { get; set; }
        public List<SettingSectioncheck> SettingSectionList { get;set;}
        
    }

    public class SettingSectioncheck
    {

        public int SettingSectionId { get; set; }

    }


    public class EmailSettingsModelForProjectLevel
    {
        public List<ProjectSettingsModel> AllProjects { get; set; }
        public List<ProjectSettingsModel> ActiveProjects { get; set; }
        public bool IsAllProjectsSelected { get; set; }

        public string UserId { get; set; }
        public int CompanyId { get; set; }

        public string[] UserSelectedProjects { get; set; }
    }


    public class ProjectSettingsModel
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
    }



    public class EmailSettingsModelForTodoLevel
    {
        public List<TodoSettingsModel> AllTodos { get; set; }
        public List<TodoSettingsModel> ActiveTodos { get; set; }
        public bool IsAllTodosSelected { get; set; }

        public string UserId { get; set; }
        public int CompanyId { get; set; }

        public string[] UserSelectedTodos { get; set; }
    }


    public class VendorMainSettingsModel
    {

        public string UserId { get; set; }
        public int CompanyId { get; set; }
        public bool IsVendor { get; set; }
        public List<VendorAreaSettingsModel> VendorAreaList { get; set; }


    }
    public class VendorAreaSettingsModel
    {
     

        public int Id { get; set; }
        public string ServiceName { get; set; }

        public int CompanyId { get; set; }

        public int VendorServiceId { get; set; }

        public string TodoDescription { get; set; }

        public bool IsVendor { get; set; }
    }





    public class TodoSettingsModel
    {
        public int TodoId { get; set; }
        public string TodoName { get; set; }
        public string TodoDescription { get; set; }
    }



    public class ChangeSettingsModel
    {
        public string UserId { get; set; }
        public int CompanyId { get; set; }
        public int ProjectId { get; set; }
        public bool IsUserNotificationActive { get; set; }
    }



    public class GetProjectsForCurrentCompanyModel
    {
        /*  ProjectId is the unique Id of project from Projects table to which the projectUser is associated*/
        public int ProjectId { get; set; }

        /* ProjectName is the name of the project for eg. Todo Project etc */
        public string ProjectName { get; set; }

    }




}
