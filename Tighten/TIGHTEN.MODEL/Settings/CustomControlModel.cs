﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL.Settings
{
    public class CustomControlModel
    {

        public int CustomControlId { get; set; }

        public string CompanyId { get; set; }

        public string FieldName { get; set; }

        public string Watermark { get; set; }

        public string Description { get; set; }

        public string ControlType { get; set; }

        public string ControlValue { get; set; }

        public bool IsActive { get; set; }

        public int? PriorityControlValue { get; set; }


    }

    public class CustomControlDelete
    {

        public int CustomControlId { get; set; }

        public bool IsActive { get; set; }

        public int IsInActiveOrDelete { get; set; }

    }

    




}
