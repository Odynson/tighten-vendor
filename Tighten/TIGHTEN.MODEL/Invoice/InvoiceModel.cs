﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIGHTEN.MODEL
{
    public class InvoiceModel
    {
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public string UserId { get; set; }

        public String VendorName { get; set; }

        public string FirstName { get; set; }

        public string UserName { get; set; }

        public int CompanyId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public string SendTo { get; set; }

        public string Message { get; set; }

        public DateTime InvoiceDate { get; set; }
        public string TotalHours { get; set; }
        public decimal TotalAmount { get; set; }
        public string ApprovedBy { get; set; }
        public bool InvoiceApprovedStatus { get; set; }

        public bool IsPaid { get; set; }
        public string PaidBy { get; set; }
        public bool IsRejected { get; set; }
        public string RejectedBy { get; set; }

        public string ProjectName { get; set; }
        public string CompanyName { get; set; }
        public int ProjectId { get; set; }
        public int VendorId { get; set; }
        public int PhaseId { get; set; }
        public string MilestoneId { get; set; }

        public string Email { get; set; }

        public List<InvoiceForResourceModal> InvoiceForResourceList { get; set; }
        public List<InvoiceToMilestoneModal> InvoiceToMilestoneList { get; set; }
        public List<InvoiceExtraResourceModal> InvoiceToExtraResourceList { get; set; }
        public int TotalPageCount { get; set; }
        public int PageIndex { get; set; }
        public int PageSizeSelected { get; set; }

        public List<WorkLog.WorkLogDetailsModel> WorkLogList { get; set; }
    }





    public class InvoiceDetailReturnModel
    {
        public string CompanyUserId { get; set; }
        public string conpanyprofilepic { get; set; }
        public string CompanyName { get; set; }
        public string FromName { get; set; }
        public string ToName { get; set; }

    }


    public class InvoiceRaiseToProject
    {
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public string Name { get; set; }
        public string VendorCompanyName { get; set; }
        public string VendorUserName { get; set; }


        public List<InvoiceRaiseToProject> InvoiceRaiseToProjectList { get; set; }
        public List<InvoiceRaiseToCompanyModal> InvoiceRaiseToCompanyList { get; set; }
        public List<InvoiceToPhaseModal> InvoiceToPhaseList { get; set; }
        public List<InvoiceForResourceModal> InvoiceForResourceList { get; set; }


    }


    public class InvoiceForResourceModal
    {
        public int Id { get; set; }

        public decimal Rate { get; set; }

        public string Name { get; set; }

        public int Hours { get; set; }
    }




    public class InvoiceRaiseToCompanyModal
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string CompanyUserName { get; set; }
        public string UserId { get; set; }
        public string CompanyProfilePic { get; set; }

        public string CompanyLogo { get; set; }

        public string InvoiceNumber { get; set; }
        public string VendorCompanyName { get; set; }
        public string VendorUserName { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string TotalHours { get; set; }
        public decimal TotalAmount { get; set; }
        public string Message { get; set; }
        public bool? IsRejected { get; set; }
        public bool? IsPaid { get; set; }
        public bool InvoiceApprovedStatus { get; set; }
        public string RejectReason { get; set; }
        public bool? IsAccountSetByVendorPoc { get; set; }
        public List<InvoiceForResourceModal> InvoiceForResourceList { get; set; }
        public List<InvoiceToMilestoneModal> InvoiceToMilestoneList { get; set; }
        public List<InvoiceExtraResourceModal> InvoiceToExtraResourceList { get; set; }
    }

    public class InvoiceRaiseFromCompanyModal
    {
        public string Name { get; set; }
        public string CompanyUserName { get; set; }

    }


    public class InvoiceToPhaseModal
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }

    public class InvoiceToMilestoneModal
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Hours { get; set; }

    }

    public class InvoiceExtraResourceModal
    {

        public int Id { get; set; }

        public string Description { get; set; }

        public int Amount { get; set; }

    }

    public class InvoiceMail
    {

        public string SenderCompanyName { get; set; }

        public string VendorCompany { get; set; }

        public string ReceiverName { get; set; }

        public string SenderName { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string ProjectName { get; set; }

        public string InvoiceNo { get; set; }

        public string TotalHours { get; set; }

        public string TotalAmount { get; set; }

        public string Message { get; set; }


    }


    public class InvoiceListModal
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int InvoiceId { get; set; }

        public string InvoiceNo { get; set; }

        public string ProjectName { get; set; }

        public string CompanyName { get; set; }

        public DateTime Invoicedate { get; set; }

        public int Hours { get; set; }

        public decimal Amount { get; set; }

        public bool? IsPaid { get; set; }

        public bool? IsReject { get; set; }
    }



    public class CompanyModal
    {
        public int CompanyId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }


    public class VendorDetailModal
    {
        public int ProjectId { get; set; }

        public string VendorName { get; set; }

        public string ProjectName { get; set; }

        public int HourSpent { get; set; }

        public decimal Amount { get; set; }

        public int BudgetedHours { get; set; }

        public string ProfilePhoto { get; set; }

        public int IsComplete { get; set; }

        public List<VendorFeedbackModal> VendorFeedbackList { get; set; }

    }


    public class VendorFeedbackModal
    {

        public int Id { get; set; }

        public string CompanyFeedback { get; set; }

        public string Name { get; set; }


    }

    public class VendorInvoiceRequestFilterModel
    {

        public int VendorCompanyId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int CompanyId { get; set; }

        public int PageIndex { get; set; }

        public int PageSizeSelected { get; set; }

    }

    public class VendorPaymentModel
    {

        public string FirstName { get; set; }

        public int Id { get; set; }

        public string CompanyName { get; set; }

        public int CompanyId { get; set; }

        public string ApprovedBy { get; set; }


        public DateTime FromDate { get; set; }


        public bool InvoiceApprovedStatus { get; set; }

        public DateTime InvoiceDate { get; set; }

        public string InvoiceNumber { get; set; }

        public bool IsRejected { get; set; }

        public string SendTo { get; set; }

        public DateTime ToDate { get; set; }
        public decimal TotalAmount { get; set; }

        public string TotalHours { get; set; }




    }

    public class PaymentEmailModel
    {
        public int Id { get; set; }

        public string StripeUserId { get; set; }

        public string UserName { get; set; }



        public string Email { get; set; }

        public string UserId { get; set; }

        public string IsExternalBankRoutUpdated { get; set; }



    }


}
